/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { Provider } from 'react-redux';
import { Store } from '@reduxjs/toolkit';
import { ThemeWrapper } from '@opengeoweb/theme';
import { SnackbarWrapperConnect } from './SnackbarWrapperConnect';
import { snackbarActions } from '../../store';

interface TestProviderProps {
  children?: React.ReactNode;
  store: Store;
}

// custom testprovider, since ThemeStoreProvider is including SnackbarWrapperConnect already
export const TestProvider: React.FC<TestProviderProps> = ({
  children,
  store,
}) => (
  <Provider store={store}>
    <ThemeWrapper>{children}</ThemeWrapper>
  </Provider>
);

describe('src/lib/components/Snackbar', () => {
  it('should render children even if snackbar closed', () => {
    const mockState = {
      snackbar: {
        entities: {},
        ids: [],
      },
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <TestProvider store={store}>
        <SnackbarWrapperConnect>
          <div>I am a child</div>
        </SnackbarWrapperConnect>
      </TestProvider>,
    );

    expect(screen.getByText('I am a child')).toBeTruthy();
    expect(screen.queryByTestId('snackbarComponent')).toBeFalsy();
  });

  it('should render children when snackbar opened', () => {
    const mockState = {
      snackbar: {
        entities: {
          snackbar1: {
            id: 'snackbar1',
            message: 'snackbar message 1',
          },
        },
        ids: ['snackbar1'],
      },
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <TestProvider store={store}>
        <SnackbarWrapperConnect>
          <div>I am a child</div>
        </SnackbarWrapperConnect>
      </TestProvider>,
    );

    expect(screen.getByText('I am a child')).toBeTruthy();
    expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
  });

  it('should call close when clicked on the close button', () => {
    const mockState = {
      snackbar: {
        entities: {
          snackbar1: {
            id: 'snackbar1',
            message: 'snackbar message 1',
          },
        },
        ids: ['snackbar1'],
      },
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <TestProvider store={store}>
        <SnackbarWrapperConnect>
          <div>I am a child</div>
        </SnackbarWrapperConnect>
      </TestProvider>,
    );

    expect(screen.getByText('I am a child')).toBeTruthy();
    expect(screen.getByTestId('snackbarComponent')).toBeTruthy();
    const expectedAction = [snackbarActions.closeSnackbar()];
    fireEvent.click(screen.getByTestId('snackbarCloseButton'));
    expect(store.getActions()).toEqual(expectedAction);
  });
});
