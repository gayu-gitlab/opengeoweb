/* eslint-disable no-param-reassign */
/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { LayerType } from '@opengeoweb/webmap';

/**
 * Base URL of the adaguc-service where all profiles are configured
 */
export const profileAdagucServiceBaseUrl = 'https://geoservices.knmi.nl';

/** The station layer with overview of profile datasets */
export const profileStationLayer = {
  service: `${profileAdagucServiceBaseUrl}/adagucserver?dataset=ceilonet&`,
  name: 'hasceilometerWMONAME',
  style: 'WMONAME/point',
  layerType: LayerType.mapLayer,
};

export const profileDataLayer = {
  service: `${profileAdagucServiceBaseUrl}/adagucserver?dataset=ceilonet&`,
  name: 'ceilonet_06310',
  format: 'image/png',
  enabled: true,
  layerType: LayerType.mapLayer,
};
