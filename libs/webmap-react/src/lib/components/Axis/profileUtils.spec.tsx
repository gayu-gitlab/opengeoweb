/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { waitFor } from '@testing-library/react';

import {
  LayerType,
  WMBBOX,
  WMJSMap,
  WMJSMapMouseClickEvent,
  WMLayer,
  registerWMJSMap,
  registerWMLayer,
  unRegisterAllWMJSLayersAndMaps,
} from '@opengeoweb/webmap';
import axios from 'axios';
import {
  CEILONETPROFILELAYERPREFIX,
  CEILONETSTATIONLAYERNAME,
  GFIResult,
  connectProfileMapViewToProfileStationsMapById,
} from './profileUtils';

describe('components/Axis/ProfileUtils', () => {
  describe('connectProfileMapViewToProfileStationsMapById', () => {
    const x = 10;
    const y = 12;

    afterEach(() => {
      unRegisterAllWMJSLayersAndMaps();
    });
    beforeEach(() => {
      // Mock axios response
      axios.get = jest.fn().mockResolvedValueOnce({
        data: [{ data: { '0': 12345 } }] as GFIResult,
      });
    });
    it('it should still connect when no layers are added', async () => {
      const { stationMap, stationMapId } = setupStationMap();
      const { profileMap, profileMapId } = setupProfileMap();
      profileMap.removeAllLayers();
      stationMap.removeAllLayers();
      // Connect the two maps.
      connectProfileMapViewToProfileStationsMapById(profileMapId, stationMapId);
    });

    it('it should log a warning when no ceilonet station map is present', async () => {
      const stationMapId = 'notavailable';
      const { profileMapId } = setupProfileMap();

      // Simulate a mouseclick and Check if  a warning was raised
      jest.spyOn(console, 'warn').mockImplementation(() => {});
      const spy = jest.spyOn(console, 'warn');

      // Connect the two maps.
      connectProfileMapViewToProfileStationsMapById(profileMapId, stationMapId);

      expect(spy).toHaveBeenCalledWith(
        `Ceilonet stations map with id ${stationMapId} not found`,
      );
    });

    it('it should log a warning when no ceilonet profile map is present', async () => {
      const { stationMapId } = setupStationMap();
      const profileMapId = 'notavailable';

      // Simulate a mouseclick and Check if  a warning was raised
      jest.spyOn(console, 'warn').mockImplementation(() => {});
      const spy = jest.spyOn(console, 'warn');

      // Connect the two maps.
      connectProfileMapViewToProfileStationsMapById(profileMapId, stationMapId);

      expect(spy).toHaveBeenCalledWith(
        `Ceilonet profile map with id ${profileMapId} not found`,
      );
    });

    it('it should log a warning when no a ceilonet profile layer is added', async () => {
      const { stationMap, stationMapId } = setupStationMap();
      const { profileMap, profileMapId } = setupProfileMap();
      profileMap.removeAllLayers();

      // Connect the two maps.
      connectProfileMapViewToProfileStationsMapById(profileMapId, stationMapId);

      // Simulate a mouseclick and Check if  a warning was raised
      jest.spyOn(console, 'warn').mockImplementation(() => {});
      const spy = jest.spyOn(console, 'warn');

      stationMap.getListener().triggerEvent('mouseclicked', {
        map: stationMap,
        x,
        y,
      } as WMJSMapMouseClickEvent);

      expect(spy).toHaveBeenCalledWith(
        'Ceilonet profile layer not found in map with id profileMapId',
      );
    });

    it('it should log a warning when no a ceilonet station layer is added', async () => {
      const { stationMap, stationMapId } = setupStationMap();
      const { profileMapId } = setupProfileMap();
      stationMap.removeAllLayers();

      // Connect the two maps.
      connectProfileMapViewToProfileStationsMapById(profileMapId, stationMapId);

      // Simulate a mouseclick and Check if  a warning was raised
      jest.spyOn(console, 'warn').mockImplementation(() => {});
      const spy = jest.spyOn(console, 'warn');

      stationMap.getListener().triggerEvent('mouseclicked', {
        map: stationMap,
        x,
        y,
      } as WMJSMapMouseClickEvent);

      expect(spy).toHaveBeenCalledWith(
        'Ceilonet station layer with id hasceilometerWMONAME not found in map',
      );
    });

    it('when clicking a station on the station map, the layer of the profilemap should be updated', async () => {
      const { stationMap, stationMapId, stationLayer } = setupStationMap();
      const { profileMapId, profileLayer } = setupProfileMap();

      // Connect the two maps.
      connectProfileMapViewToProfileStationsMapById(profileMapId, stationMapId);

      expect(profileLayer.name).toBe('ceilonet_layer');

      // Now simulate a click on the station map

      const axiosSpy = jest.spyOn(axios, 'get');

      stationMap.getListener().triggerEvent('mouseclicked', {
        map: stationMap,
        x,
        y,
      } as WMJSMapMouseClickEvent);

      expect(axiosSpy).toHaveBeenCalledWith(
        stationMap.getWMSGetFeatureInfoRequestURL(
          stationLayer,
          x,
          y,
          'application/json',
        ),
      );
      // Check if the layername is indeed changed
      await waitFor(() => {
        expect(profileLayer.name).toBe('ceilonet_12345');
      });

      expect(stationMap.mapPin.displayMapPin).toBeTruthy();
      expect(stationMap.mapPin.disableMapPin).toBeFalsy();
    });

    it('when clicking outside the station map, the layer of the profilemap should not be updated', async () => {
      const { stationMap, stationMapId, stationLayer } = setupStationMap();
      const { profileMapId, profileLayer } = setupProfileMap();

      // Connect the two maps.
      connectProfileMapViewToProfileStationsMapById(profileMapId, stationMapId);

      expect(profileLayer.name).toBe('ceilonet_layer');

      // Now simulate a click on the station map
      jest.spyOn(console, 'warn').mockImplementation(() => {});
      const spy = jest.spyOn(console, 'warn');
      axios.get = jest
        .fn()
        .mockRejectedValueOnce('error while fetching station id');
      const axiosSpy = jest.spyOn(axios, 'get');

      stationMap.getListener().triggerEvent('mouseclicked', {
        map: stationMap,
        x,
        y,
      } as WMJSMapMouseClickEvent);

      // Check if the layername is indeed not changed
      await waitFor(() => {
        expect(spy).toHaveBeenCalledWith('error while fetching station id');
      });

      expect(axiosSpy).toHaveBeenCalledWith(
        stationMap.getWMSGetFeatureInfoRequestURL(
          stationLayer,
          x,
          y,
          'application/json',
        ),
      );

      // Check if the layername is still unchanged
      await waitFor(() => {
        expect(profileLayer.name).toBe('ceilonet_layer');
      });
    });

    it('it should not crash when the station data GFI call returns invalid information', async () => {
      const { stationMap, stationMapId, stationLayer } = setupStationMap();
      const { profileMapId, profileLayer } = setupProfileMap();

      // Connect the two maps.
      connectProfileMapViewToProfileStationsMapById(profileMapId, stationMapId);

      expect(profileLayer.name).toBe('ceilonet_layer');

      // Now simulate a click on the station map

      axios.get = jest.fn().mockResolvedValueOnce({
        data: [{ data: { '0': -1 } }] as GFIResult,
      });
      const axiosSpy = jest.spyOn(axios, 'get');

      jest.spyOn(console, 'warn').mockImplementation(() => {});
      const spy = jest.spyOn(console, 'warn');

      stationMap.getListener().triggerEvent('mouseclicked', {
        map: stationMap,
        x,
        y,
      } as WMJSMapMouseClickEvent);

      expect(axiosSpy).toHaveBeenCalledWith(
        stationMap.getWMSGetFeatureInfoRequestURL(
          stationLayer,
          x,
          y,
          'application/json',
        ),
      );

      // Check if the layername is indeed not changed
      await waitFor(() => {
        expect(profileLayer.name).toBe('ceilonet_layer');
      });

      expect(spy).toHaveBeenCalledWith('GFI result from server not recognized');
    });
  });
});

const setupStationMap = (): {
  stationMap: WMJSMap;
  stationMapId: string;
  stationLayer: WMLayer;
  stationLayerId: string;
} => {
  const stationMapId = 'stationMapId';
  const stationMapEl = document.createElement('div');
  const stationMap = new WMJSMap(stationMapEl);

  const stationLayerId = 'stationLayer';
  const stationLayer = new WMLayer({
    name: CEILONETSTATIONLAYERNAME,
    id: stationLayerId,
    layerType: LayerType.mapLayer,
    service: 'http://profilestationservice/',
  });
  stationMap.addLayer(stationLayer);
  registerWMJSMap(stationMap, stationMapId);
  registerWMLayer(stationLayer, stationLayerId);
  return { stationMap, stationMapId, stationLayer, stationLayerId };
};

const setupProfileMap = (): {
  profileMap: WMJSMap;
  profileMapId: string;
  profileLayer: WMLayer;
  profileLayerId: string;
} => {
  const profileMapId = 'profileMapId';
  const profileMapEl = document.createElement('div');
  const profileMap = new WMJSMap(profileMapEl);

  const profileLayerId = 'profileLayer';
  const profileLayer = new WMLayer({
    name: `${CEILONETPROFILELAYERPREFIX}layer`,
    id: profileLayerId,
    layerType: LayerType.mapLayer,
    service: 'http://profilesceilonetservice/',
  });
  profileMap.addLayer(profileLayer);

  profileMap.setProjection({
    srs: 'GFI:TIME_ELEVATION',
    bbox: new WMBBOX({
      left: 0,
      bottom: 0,
      right: 86400,
      top: 50,
    }),
  });
  registerWMJSMap(profileMap, profileMapId);
  registerWMLayer(profileLayer, profileLayerId);

  return { profileMap, profileMapId, profileLayer, profileLayerId };
};
