/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { WMBBOX } from '@opengeoweb/webmap';
import { ReactMapView } from '../ReactMapView';
import { ProfileAxis } from './ProfileAxis';

export const ProfileAxisStory = (): React.ReactElement => {
  const bbox = new WMBBOX({
    left: new Date('2023-12-03T00:00:00Z').getTime(),
    bottom: -500,
    right: new Date('2023-12-03T24:00:00Z').getTime(),
    top: 15000,
  });
  const extraStyle = {
    border: '1px solid grey',
    padding: '2px',
    margin: '8px',
  };
  return (
    <>
      <div style={{ height: '20vh', width: '80vw', ...extraStyle }}>
        <ProfileAxis mapId="mapId1" />
        <ReactMapView mapId="mapId1" srs="GFI:TIME_ELEVATION" bbox={bbox} />
      </div>
      <div style={{ height: '50vh', width: '80vw', ...extraStyle }}>
        <ProfileAxis mapId="mapId2" />
        <ReactMapView mapId="mapId2" srs="GFI:TIME_ELEVATION" bbox={bbox} />
      </div>
      <div style={{ height: '15vh', width: '25vw', ...extraStyle }}>
        <ProfileAxis mapId="mapId3" />
        <ReactMapView mapId="mapId3" srs="GFI:TIME_ELEVATION" bbox={bbox} />
      </div>
    </>
  );
};

export default { title: 'components/ProfileSounding' };

ProfileAxisStory.storyName = 'MapView with ProfileAxis';
