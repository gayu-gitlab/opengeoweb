/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { unRegisterAllWMJSLayersAndMaps } from '@opengeoweb/webmap';

import { getIsInsideAcceptanceTime } from './utils';

describe('src/components/ReactMapView/utils', () => {
  afterEach(() => {
    unRegisterAllWMJSLayersAndMaps();
  });

  it('getIsInsideAcceptanceTime', () => {
    expect(
      getIsInsideAcceptanceTime(
        1,
        [{ currentValue: '2023-07-19T10:00:00Z', name: 'time' }],
        [{ currentValue: '2023-07-19T10:00:00Z', name: 'time' }],
      ),
    ).toBeTruthy();

    expect(
      getIsInsideAcceptanceTime(
        1,
        [{ currentValue: '2023-07-19T10:00:00Z', name: 'time' }],
        [{ currentValue: '2023-07-19T10:01:00Z', name: 'time' }],
      ),
    ).toBeTruthy();

    expect(
      getIsInsideAcceptanceTime(
        1,
        [{ currentValue: '2023-07-19T10:00:00Z', name: 'time' }],
        [{ currentValue: '2023-07-19T10:02:00Z', name: 'time' }],
      ),
    ).toBeFalsy();

    expect(
      getIsInsideAcceptanceTime(
        undefined,
        [{ currentValue: '2023-07-19T10:00:00Z', name: 'time' }],
        [{ currentValue: '2023-07-19T10:00:00Z', name: 'time' }],
      ),
    ).toBeTruthy();

    expect(
      getIsInsideAcceptanceTime(
        1,
        [{ currentValue: '2023-07-19T10:00:00Z', name: 'time' }],
        undefined,
      ),
    ).toBeTruthy();
  });
});
