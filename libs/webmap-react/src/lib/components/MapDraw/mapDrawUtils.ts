/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as turf from '@turf/turf';
import { Position } from 'geojson';
import { Coordinate, GeoJsonFeature } from './geojsonShapes';

export type CheckHoverFeaturesResult = {
  coordinateIndexInFeature: number;
  featureIndex: number;
  feature: GeoJsonFeature;
} | null;

/* Function which calculates the distance between two points */
export const distance = (a: Coordinate, b: Coordinate): number => {
  return Math.sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
};

const checkHoverVertice = (
  feature: GeoJsonFeature,
  mouseX: number,
  mouseY: number,
  convertGeoCoordsToScreenCoords: (featureCoords: Position[]) => Coordinate[],
  ignoreCoordinateIndexInFeature?: boolean,
): {
  coordinateIndexInFeature: number;
} | null => {
  const maxDistance = 20;
  if (feature.geometry.type === 'Point') {
    const featureCoords = feature.geometry.coordinates;
    /* Get all vertexes */
    const XYCoords = convertGeoCoordsToScreenCoords([featureCoords]);
    if (
      XYCoords.length > 0 &&
      distance(XYCoords[0], { x: mouseX, y: mouseY }) < maxDistance
    ) {
      return { coordinateIndexInFeature: 0 };
    }
  }
  if (feature.geometry.type === 'MultiPoint') {
    for (
      let polygonIndex = feature.geometry.coordinates.length - 1;
      polygonIndex >= 0;
      polygonIndex -= 1
    ) {
      const featureCoords = feature.geometry.coordinates[polygonIndex];
      if (featureCoords === undefined) {
        // eslint-disable-next-line no-continue
        continue;
      }
      /* Get all vertexes */
      const XYCoords = convertGeoCoordsToScreenCoords([featureCoords]);
      if (
        XYCoords.length > 0 &&
        distance(XYCoords[0], { x: mouseX, y: mouseY }) < maxDistance
      ) {
        return {
          coordinateIndexInFeature: polygonIndex,
        };
      }
    }
  }

  if (feature.geometry.type === 'Polygon') {
    const point = [mouseX, mouseY];
    for (
      let polygonIndex = feature.geometry.coordinates.length - 1;
      polygonIndex >= 0;
      polygonIndex -= 1
    ) {
      const featureCoords = feature.geometry.coordinates[polygonIndex];
      if (featureCoords === undefined) {
        // eslint-disable-next-line no-continue
        continue;
      }
      /* Get all vertexes */
      const poly = [
        convertGeoCoordsToScreenCoords(featureCoords).map((coord) => {
          return [coord.x, coord.y];
        }),
      ];
      try {
        const isPointInPoly = turf.booleanPointInPolygon(
          turf.point(point),
          turf.polygon(poly),
        );
        if (isPointInPoly) {
          if (ignoreCoordinateIndexInFeature) {
            return {
              coordinateIndexInFeature: 0,
            };
          }

          return {
            coordinateIndexInFeature: polygonIndex,
          };
        }
      } catch (e) {
        // console.warn(e);
      }
    }
  }

  if (feature.geometry.type === 'MultiPolygon') {
    const point = [mouseX, mouseY];
    for (
      let polygonIndex = feature.geometry.coordinates.length - 1;
      polygonIndex >= 0;
      polygonIndex -= 1
    ) {
      const featureCoords = feature.geometry.coordinates[polygonIndex][0];
      if (featureCoords === undefined) {
        // eslint-disable-next-line no-continue
        continue;
      }
      const poly = [
        convertGeoCoordsToScreenCoords(featureCoords).map((coord) => {
          return [coord.x, coord.y];
        }),
      ];
      try {
        const isPointInPoly = turf.booleanPointInPolygon(
          turf.point(point),
          turf.polygon(poly),
        );
        if (isPointInPoly) {
          return {
            coordinateIndexInFeature: polygonIndex,
          };
        }
      } catch (e) {
        console.warn(e);
      }
    }
  }

  if (feature.geometry.type === 'LineString') {
    const featureCoords = feature.geometry.coordinates;
    /* Get all vertexes */
    const XYCoords = convertGeoCoordsToScreenCoords(featureCoords);
    /* Snap to the vertex closer than specified pixels */
    for (let j = 0; j < XYCoords.length; j += 1) {
      const coord = XYCoords[j];
      if (distance(coord, { x: mouseX, y: mouseY }) < maxDistance) {
        return { coordinateIndexInFeature: j };
      }
    }
  }
  return null;
};

export const checkHoverFeatures = (
  geojson: GeoJSON.FeatureCollection,
  mouseX: number,
  mouseY: number,
  convertGeoCoordsToScreenCoords: (featureCoords: Position[]) => Coordinate[],
  ignoreCoordinateIndexInFeature?: boolean,
): CheckHoverFeaturesResult => {
  for (let j = 0; j < geojson.features.length; j += 1) {
    const feature = geojson.features[j] as GeoJsonFeature;
    const hoverResult = checkHoverVertice(
      feature,
      mouseX,
      mouseY,
      convertGeoCoordsToScreenCoords,
      ignoreCoordinateIndexInFeature,
    );
    if (hoverResult != null) {
      return {
        coordinateIndexInFeature: hoverResult.coordinateIndexInFeature,
        featureIndex: j,
        feature,
      };
    }
  }
  return null;
};

export interface MapDrawDrawFunctionArgs {
  context: CanvasRenderingContext2D;
  featureIndex: number;
  coord: Coordinate;
  selected: boolean;
  isInEditMode: boolean;
  feature: GeoJsonFeature;
  mouseX: number;
  mouseY: number;
  isHovered: boolean;
}

interface DrawFunction {
  id: string;
  drawMethod: (args: MapDrawDrawFunctionArgs) => void;
}

let generatedDrawFunctionIds = 0;
const generateDrawFunctionId = (): string => {
  generatedDrawFunctionIds += 1;
  return `drawFunctionId_${generatedDrawFunctionIds}`;
};

/**
 *  DrawFunction store for re-use of drawFunctions
 */
const drawFunctionStore: DrawFunction[] = [];

export const getDrawFunctionFromStore = (
  id: string,
): DrawFunction['drawMethod'] | undefined => {
  const drawFunction = drawFunctionStore.find(
    (drawFunction) => drawFunction.id === id,
  );

  return drawFunction?.drawMethod;
};

export const registerDrawFunction = (
  drawFunction: DrawFunction['drawMethod'] = (): void => {},
): string => {
  const id = generateDrawFunctionId();
  const newFunction = { id, drawMethod: drawFunction };
  drawFunctionStore.push(newFunction);
  return id;
};

export const NEW_LINESTRING_CREATED = 'new point in LineString created';
export const NEW_FEATURE_CREATED = 'new feature created';
export const NEW_POINT_CREATED = 'new point created';
