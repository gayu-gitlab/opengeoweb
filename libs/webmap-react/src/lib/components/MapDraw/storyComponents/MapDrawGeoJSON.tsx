/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React, { useState } from 'react';
import {
  Button,
  Select,
  FormControl,
  InputLabel,
  MenuItem,
  Grid,
  Box,
  TextField,
  SelectChangeEvent,
} from '@mui/material';

import { webmapUtils } from '@opengeoweb/webmap';

import { MapView, MapViewLayer } from '../../MapView';
import { publicLayers } from '../../../layers';
import { initialBbox } from '../../../layers/defaultStorybookSettings';
import { drawPolyStoryStyles, useGeoJSON } from './storyUtils';

export const MapDrawGeoJSON: React.FC = () => {
  const {
    editModes,
    isInEditMode,
    geojson,
    setGeojson,
    geojsonText,
    setGeojsonText,
    drawMode,
    currentFeatureNrToEdit,
    setCurrentFeatureNrToEdit,
    changeDrawMode,
    setEditMode,
  } = useGeoJSON();

  const [isValid, setValidity] = useState(true);

  const geojsonState = geojson;
  const numFeatures =
    geojsonState && geojsonState.features ? geojsonState.features.length : 0;
  const featureToEditList: { key: string; value: number }[] = [];
  for (let j = 0; j < numFeatures; j += 1) {
    featureToEditList.push({ key: `${j}`, value: j });
  }

  return (
    <Box>
      <Box sx={drawPolyStoryStyles.MapDrawGeoJSONContainer}>
        <Box sx={drawPolyStoryStyles.MapDrawGeoJSONMapContainer}>
          <MapView
            mapId="mapDrawGeoJSONExample"
            srs={initialBbox.srs}
            bbox={initialBbox.bbox}
            onWMJSMount={(mapId): void => {
              const webMap = webmapUtils.getWMJSMapById(mapId);
              webMap.mapPin.hideMapPin();
            }}
          >
            <MapViewLayer {...publicLayers.baseLayer} />
            <MapViewLayer
              id="geojson-layer"
              geojson={geojsonState}
              isInEditMode={isInEditMode}
              drawMode={drawMode}
              updateGeojson={(
                updatedGeojson: GeoJSON.FeatureCollection,
              ): void => {
                setGeojson(updatedGeojson);
                setGeojsonText(JSON.stringify(geojsonState, null, 2));
              }}
              exitDrawModeCallback={(): void => {
                setEditMode(!isInEditMode);
              }}
              selectedFeatureIndex={currentFeatureNrToEdit}
            />
          </MapView>
        </Box>
        <Box sx={drawPolyStoryStyles.MapDrawGeoJSONControlsContainer}>
          <Box p={2}>
            <Grid spacing={2} container>
              <Grid item sm={6}>
                <FormControl variant="filled" style={{ minWidth: 120 }}>
                  <InputLabel id="demo-feature-type">Feature type</InputLabel>
                  <Select
                    labelId="demo-feature-type"
                    value={drawMode}
                    onChange={(event: SelectChangeEvent): void => {
                      changeDrawMode(event.target.value);
                    }}
                  >
                    {editModes.map((mode) => {
                      return (
                        <MenuItem key={mode.key} value={mode.key}>
                          {mode.value}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={6}>
                <FormControl style={{ minWidth: 120 }} variant="filled">
                  <InputLabel id="demo-feature-number">
                    Feature number
                  </InputLabel>
                  <Select
                    labelId="demo-feature-type"
                    value={`${currentFeatureNrToEdit}`}
                    onChange={(event: SelectChangeEvent): void => {
                      const featureNr = parseInt(event.target.value, 10);
                      setCurrentFeatureNrToEdit(featureNr);
                    }}
                  >
                    {featureToEditList.map((listItem) => {
                      return (
                        <MenuItem key={listItem.key} value={listItem.key}>
                          {listItem.value}
                        </MenuItem>
                      );
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item sm={6}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={(): void => {
                    setEditMode(!isInEditMode);
                  }}
                >
                  {isInEditMode ? 'Finish edit' : 'Start edit'}
                </Button>
              </Grid>
            </Grid>
          </Box>
        </Box>
        <Box sx={drawPolyStoryStyles.MapDrawGeoJSONTextAreaContainer}>
          <TextField
            sx={drawPolyStoryStyles.MapDrawGeoJSONTextArea}
            multiline
            error={!isValid}
            maxRows="18"
            label={!isValid ? 'GeoJSON not valid' : 'GeoJSON text'}
            variant="filled"
            onChange={(event: React.ChangeEvent<HTMLInputElement>): void => {
              setGeojsonText(event.target.value);
              try {
                setGeojson(JSON.parse(event.target.value));
                setValidity(true);
              } catch (e) {
                setGeojson(null!);
                setValidity(false);
              }
            }}
            value={geojsonText || JSON.stringify(geojsonState, null, 2)}
          />
        </Box>
      </Box>
    </Box>
  );
};

export default MapDrawGeoJSON;
