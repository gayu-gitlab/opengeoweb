/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { useState } from 'react';

import {
  simpleBoxGeoJSON,
  simplePointsGeojson,
  simplePolygonGeoJSON,
} from './geojsonExamples';
import { lineString } from '../geojsonShapes';

export const drawPolyStoryStyles = {
  MapDrawGeoJSONContainer: {
    display: 'grid',
    gridTemplateColumns: '70% 30%',
    gridTemplateRows: '150px 1fr',
    gridTemplateAreas: "'map controls'\n'map textarea'",
    width: '100%',
    height: '100vh',
  },
  MapDrawGeoJSONMapContainer: {
    gridArea: 'map',
    width: '100%',
    height: '100%',
  },
  MapDrawGeoJSONControlsContainer: {
    gridArea: 'controls',
    width: '100%',
    height: '100%',
  },
  MapDrawGeoJSONTextAreaContainer: {
    gridArea: 'textarea',
    width: '100%',
    height: '100%',
  },
  MapDrawGeoJSONTextArea: {
    padding: 0,
    border: 'none',
    width: '100%',
    height: '100%',
    fontSize: '10px',
  },
};

export const useGeoJSON = (): {
  editModes: { key: string; value: string }[];
  isInEditMode: boolean;
  geojson: GeoJSON.FeatureCollection;
  setGeojson: (geojson: GeoJSON.FeatureCollection) => void;
  geojsonText: string;
  setGeojsonText: (text: string) => void;
  drawMode: string;
  currentFeatureNrToEdit: number;
  setCurrentFeatureNrToEdit: (featureNumber: number) => void;
  changeDrawMode: (mode: string) => void;
  setEditMode: (on: boolean) => void;
} => {
  const editModes = [
    { key: 'POLYGON', value: 'Polygon' },
    { key: 'POINT', value: 'Point' },
    { key: 'BOX', value: 'Box' },
    { key: 'LINESTRING', value: 'LineString' },
  ];

  const [isInEditMode, setEditMode] = useState(false);
  const [geojson, setGeojson] = useState(lineString);
  const [geojsonText, setGeojsonText] = useState('');
  const [drawMode, setDrawMode] = useState('LINESTRING');
  const [currentFeatureNrToEdit, setCurrentFeatureNrToEdit] = useState(0);

  const changeDrawMode = (selectedKey: string): void => {
    setDrawMode(selectedKey);

    if (selectedKey === 'POLYGON') {
      setGeojson(simplePolygonGeoJSON);
      setGeojsonText(JSON.stringify(simplePolygonGeoJSON, null, 2));
    }
    if (selectedKey === 'BOX') {
      setGeojson(simplePolygonGeoJSON);
      setGeojsonText(JSON.stringify(simpleBoxGeoJSON, null, 2));
    }
    if (selectedKey === 'LINESTRING') {
      setGeojson(lineString);
      setGeojsonText(JSON.stringify(lineString, null, 2));
    }
    if (selectedKey === 'POINT' || selectedKey === 'MULTIPOINT') {
      setGeojson(simplePointsGeojson);
      setGeojsonText(JSON.stringify(simplePointsGeojson, null, 2));
    }
  };
  return {
    editModes,
    isInEditMode,
    geojson,
    setGeojson,
    geojsonText,
    setGeojsonText,
    drawMode,
    currentFeatureNrToEdit,
    setCurrentFeatureNrToEdit,
    changeDrawMode,
    setEditMode,
  };
};
