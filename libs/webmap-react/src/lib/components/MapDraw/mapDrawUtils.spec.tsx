/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { Position } from 'geojson';
import {
  checkHoverFeatures,
  getDrawFunctionFromStore,
  registerDrawFunction,
} from './mapDrawUtils';
import {
  simpleMultiPolygon,
  simplePointsGeojson,
  simplePolygonGeoJSON,
} from './storyComponents/geojsonExamples';
import { Coordinate } from './geojsonShapes';

/**
 * Function which scales the latlon coordinates to a fake screen coordinate system (100x lat/lon)
 * @param featureCoords
 * @returns
 */
const convertGeoCoordsToScreenCoords = (
  featureCoords: Position[],
): Coordinate[] => {
  const XYCoords: Coordinate[] = [];
  for (const featureCoord of featureCoords) {
    if (featureCoord.length < 2) {
      // eslint-disable-next-line no-continue
      continue;
    }

    const coord = {
      x: featureCoord[0] * 100,
      y: featureCoord[1] * 100,
    };
    XYCoords.push(coord);
  }
  return XYCoords;
};
describe('src/components/ReactMapView/mapDrawUtils', () => {
  describe('checkHoverFeatures', () => {
    it('checkHoverFeatures on a GeoJSON Polygon should return null when used outside of the polygons', async () => {
      const result = checkHoverFeatures(
        simplePolygonGeoJSON,
        -5 * 100,
        52 * 100,
        convertGeoCoordsToScreenCoords,
      );
      expect(result).toBeNull();
    });
    it('checkHoverFeatures on a GeoJSON Polygon should find Netherlands when using coordinate (5,52)', async () => {
      const result = checkHoverFeatures(
        simplePolygonGeoJSON,
        5 * 100,
        52 * 100,
        convertGeoCoordsToScreenCoords,
      )!;
      expect(result.coordinateIndexInFeature).toBe(0);
      expect(result.featureIndex).toBe(0);
      expect(result.feature.properties!.country).toBe('Netherlands');
    });
    it('checkHoverFeatures on a GeoJSON Polygon should find Norway when using coordinate (10,62)', async () => {
      const result = checkHoverFeatures(
        simplePolygonGeoJSON,
        10 * 100,
        62 * 100,
        convertGeoCoordsToScreenCoords,
      )!;
      expect(result.coordinateIndexInFeature).toBe(0);
      expect(result.featureIndex).toBe(2);
      expect(result.feature.properties!.country).toBe('Norway');
    });
    it('checkHoverFeatures on a GeoJSON Polygon should find Finland when using coordinate (26,63)', async () => {
      const result = checkHoverFeatures(
        simplePolygonGeoJSON,
        26 * 100,
        63 * 100,
        convertGeoCoordsToScreenCoords,
      )!;
      expect(result.coordinateIndexInFeature).toBe(0);
      expect(result.featureIndex).toBe(1);
      expect(result.feature.properties!.country).toBe('Finland');
    });
    it('checkHoverFeatures on a GeoJSON Point should return null when used outside of the points', async () => {
      const result = checkHoverFeatures(
        simplePointsGeojson,
        -50 * 100,
        52 * 100,
        convertGeoCoordsToScreenCoords,
      );
      expect(result).toBeNull();
    });
    it('checkHoverFeatures on a GeoJSON Point should find first point when using coordinate (3.16,53.12)', async () => {
      const result = checkHoverFeatures(
        simplePointsGeojson,
        3.16 * 100,
        53.12 * 100,
        convertGeoCoordsToScreenCoords,
      )!;
      expect(result.coordinateIndexInFeature).toBe(0);
      expect(result.featureIndex).toBe(0);
      expect(result.feature.properties!.name).toBe('First point');
    });
    it('checkHoverFeatures on a GeoJSON Point should find the third point when using coordinate (5.98,48.92)', async () => {
      const result = checkHoverFeatures(
        simplePointsGeojson,
        5.98 * 100,
        48.92 * 100,
        convertGeoCoordsToScreenCoords,
      )!;
      expect(result.coordinateIndexInFeature).toBe(0);
      expect(result.featureIndex).toBe(2);
      expect(result.feature.properties!.name).toBe('Third point');
    });

    it('checkHoverFeatures on a GeoJSON MultiPolygon should return null when used outside of the points', async () => {
      const result = checkHoverFeatures(
        simpleMultiPolygon,
        -50 * 100,
        52 * 100,
        convertGeoCoordsToScreenCoords,
      );
      expect(result).toBeNull();
    });
    it('checkHoverFeatures on a GeoJSON MultiPolygon should find first point when using coordinate (3.80,56.21)', async () => {
      const result = checkHoverFeatures(
        simpleMultiPolygon,
        3.8 * 100,
        56.21 * 100,
        convertGeoCoordsToScreenCoords,
      )!;
      expect(result.coordinateIndexInFeature).toBe(0);
      expect(result.featureIndex).toBe(0);
    });
    it('checkHoverFeatures on a GeoJSON MultiPolygon should find the third point when using coordinate (10.00,56.00)', async () => {
      const result = checkHoverFeatures(
        simpleMultiPolygon,
        10.0 * 100,
        56.0 * 100,
        convertGeoCoordsToScreenCoords,
      )!;
      expect(result.coordinateIndexInFeature).toBe(1);
      expect(result.featureIndex).toBe(0);
    });
  });

  describe('getDrawFunctionFromStore', () => {
    it('should return the correct function', () => {
      const drawFunction = jest.fn();
      const id = registerDrawFunction(drawFunction);
      expect(getDrawFunctionFromStore(id)).not.toBeUndefined();
      expect(getDrawFunctionFromStore(id)).toEqual(drawFunction);
    });
    it('should return undefined if function does not exist', () => {
      const id = 'test';
      expect(getDrawFunctionFromStore(id)).toBeUndefined();
    });
  });

  describe('registerDrawFunction', () => {
    it('should register a drawFunction and return the id', () => {
      const drawFunction = jest.fn();
      const id = registerDrawFunction(drawFunction);
      expect(id).toContain('drawFunctionId_');
      expect(getDrawFunctionFromStore(id)).not.toBeUndefined();
      expect(getDrawFunctionFromStore(id)).toEqual(drawFunction);
    });
  });
});
