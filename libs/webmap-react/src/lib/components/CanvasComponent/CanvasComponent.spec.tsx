/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  render,
  fireEvent,
  act,
  waitFor,
  screen,
} from '@testing-library/react';
import { CanvasComponent } from './CanvasComponent';

describe('src/components/CanvasComponent/CanvasComponent', () => {
  it('should call onWheel when using the mouse wheel', () => {
    const props = { onWheel: jest.fn() };
    render(<CanvasComponent {...props} />);
    const canvas = screen.getByRole('presentation').firstChild as Element;
    fireEvent.wheel(canvas);
    expect(props.onWheel).toHaveBeenCalled();
  });

  it('should call onCanvasClick when clicking on the canvas', () => {
    const props = { onCanvasClick: jest.fn() };
    render(<CanvasComponent {...props} />);
    const canvas = screen.getByRole('presentation').firstChild as Element;
    fireEvent.click(canvas);
    expect(props.onCanvasClick).toHaveBeenCalled();
  });

  it('should call onMouseMove when moving the mouse on the canvas', () => {
    const props = { onMouseMove: jest.fn() };
    render(<CanvasComponent {...props} />);
    const canvas = screen.getByRole('presentation').firstChild as Element;
    fireEvent.mouseMove(canvas);
    expect(props.onMouseMove).toHaveBeenCalled();
  });

  it('should call onMouseDown when pressing the mouse button down on the canvas', () => {
    const props = { onMouseDown: jest.fn() };
    render(<CanvasComponent {...props} />);
    const canvas = screen.getByRole('presentation').firstChild as Element;
    fireEvent.mouseDown(canvas);
    expect(props.onMouseDown).toHaveBeenCalled();
  });

  it('should call onMouseUp when releasing the mouse button on the canvas', () => {
    const props = { onMouseUp: jest.fn() };
    render(<CanvasComponent {...props} />);
    const canvas = screen.getByRole('presentation').firstChild as Element;
    fireEvent.mouseUp(canvas);
    expect(props.onMouseUp).toHaveBeenCalled();
  });

  it('should call onTouchStart when touching the canvas', () => {
    const props = { onTouchStart: jest.fn() };
    render(<CanvasComponent {...props} />);
    const canvas = screen.getByRole('presentation').firstChild as Element;
    fireEvent.touchStart(canvas);
    expect(props.onTouchStart).toHaveBeenCalled();
  });

  it('should call onTouchMove when swiping the canvas', () => {
    const props = { onTouchMove: jest.fn() };
    render(<CanvasComponent {...props} />);
    const canvas = screen.getByRole('presentation').firstChild as Element;
    fireEvent.touchMove(canvas);
    expect(props.onTouchMove).toHaveBeenCalled();
  });

  it('should call onTouchEnd when releasing touch on the canvas', () => {
    const props = { onTouchEnd: jest.fn() };
    render(<CanvasComponent {...props} />);
    const canvas = screen.getByRole('presentation').firstChild as Element;
    fireEvent.touchEnd(canvas);
    expect(props.onTouchEnd).toHaveBeenCalled();
  });

  it('should call onKeyDown when canvas is focussed and pressing the Escape key on the canvas', () => {
    const props = {
      onKeyUp: jest.fn(),
      onKeyDown: jest.fn(),
      isFocussed: jest.fn(),
    };
    render(<CanvasComponent {...props} />);
    const canvas = screen.getByRole('presentation').firstChild as Element;
    fireEvent.keyUp(canvas, { key: 'Tab', code: 'Tab' });
    expect(props.isFocussed).toHaveBeenCalledWith(true);
    fireEvent.keyDown(canvas, { key: 'Escape', code: 'Escape' });
    expect(props.onKeyDown).toHaveBeenCalled();
  });

  it('should not call onKeyDown when canvas is not focussed and pressing the Escape key on the canvas', () => {
    const props = { onKeyDown: jest.fn() };
    render(<CanvasComponent {...props} />);
    const canvas = screen.getByRole('presentation').firstChild as Element;
    fireEvent.keyDown(canvas, { key: 'Escape', code: 'Escape' });
    expect(props.onKeyDown).not.toHaveBeenCalled();
  });

  it('should call onKeyUp when releasing the Escape key on the canvas', () => {
    const props = { onKeyUp: jest.fn() };
    render(<CanvasComponent {...props} />);
    const canvas = screen.getByRole('presentation').firstChild as Element;
    fireEvent.keyUp(canvas, { key: 'Escape', code: 'Escape' });
    expect(props.onKeyUp).toHaveBeenCalled();
  });

  it('should call setInterval when redrawInterval is given', async () => {
    jest.useFakeTimers();
    const storedSetInterval = global['setInterval'];
    const spy = jest.spyOn(global, 'setInterval');

    const props = {
      redrawInterval: 500,
    };
    render(<CanvasComponent {...props} />);
    await waitFor(() => {
      expect(spy).toHaveBeenCalledWith(
        expect.any(Function),
        props.redrawInterval,
      );
    });
    await act(async () => {
      jest.runOnlyPendingTimers();
    });
    jest.clearAllTimers();
    jest.useRealTimers();
    global['setInterval'] = storedSetInterval;
  });

  it('should remove focus on blur', () => {
    const props = {
      isFocussed: jest.fn(),
    };
    render(
      <>
        <CanvasComponent {...props} />
        <div data-testid="dummy" />
      </>,
    );
    const canvas = screen.getByRole('presentation').firstChild as Element;
    fireEvent.blur(canvas);
    expect(props.isFocussed).toHaveBeenCalledWith(false);
  });

  it('should call provided callback function when resizing the component', () => {
    const props = { resizeCallback: jest.fn() };
    render(<CanvasComponent {...props} />);
    expect(props.resizeCallback).toHaveBeenCalledTimes(1);

    const canvasContainerDiv = screen.getByTestId('canvas-container');

    jest
      .spyOn(canvasContainerDiv, 'clientWidth', 'get')
      .mockReturnValue(1000)
      .mockReturnValueOnce(1500);
    fireEvent(window, new Event('resize'));
    fireEvent(window, new Event('resize'));
    // the last resize has same width - callback not called
    fireEvent(window, new Event('resize'));
    expect(props.resizeCallback).toHaveBeenCalledTimes(3);
  });

  it('should call provided callback function with the correct argument', () => {
    const props = { resizeCallback: jest.fn() };
    render(<CanvasComponent {...props} />);
    const canvasContainerDiv = screen.getByTestId('canvas-container');
    jest.spyOn(canvasContainerDiv, 'clientWidth', 'get').mockReturnValue(1000);
    fireEvent(window, new Event('resize'));
    expect(props.resizeCallback).toHaveBeenLastCalledWith(1000);
  });
});
