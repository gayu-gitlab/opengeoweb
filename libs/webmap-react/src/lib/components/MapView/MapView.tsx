/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useResizeDetector } from 'react-resize-detector';

import { Box } from '@mui/material';
import { WMJSMap, webmapUtils } from '@opengeoweb/webmap';

import { MapControls, ZoomControls } from '../MapControls';
import { ReactMapView, ReactMapViewProps } from '../ReactMapView';
import { MapTime } from '../MapTime';
import { ProfileAxis } from '../Axis/ProfileAxis';

export interface MapViewProps extends ReactMapViewProps {
  controls?: {
    zoomControls?: boolean;
  };
}

export const MapView: React.FC<MapViewProps> = ({
  children,
  controls = {
    zoomControls: true,
  },
  displayTimeInMap = true,
  onWMJSMount,
  // rest props
  ...props
}: MapViewProps) => {
  const { dimensions, mapId: mapIdProp } = props;

  const wmjsMapRef = React.useRef<WMJSMap | null>(null);
  const mapId = React.useRef<string>(mapIdProp).current;

  if (mapIdProp !== mapId) {
    console.warn(
      `Warning, MapId ${mapId} has been updated to ${mapIdProp} on an already mounted component. Keeping ${mapId}`,
    );
  }

  const { ref, height, width } = useResizeDetector();

  React.useEffect(() => {
    if (wmjsMapRef.current && !!width && !!height) {
      const { height: mapHeight, width: mapWidth } =
        wmjsMapRef.current.getSize();
      if (width !== mapWidth || height !== mapHeight) {
        wmjsMapRef.current.setSize(Math.ceil(width), Math.ceil(height));
      }
    }
  }, [width, height]);

  const reactMapViewProps = { ...(props as ReactMapViewProps), mapId };

  return (
    <Box
      ref={ref}
      sx={{
        display: 'grid',
        width: '100%',
        height: '100%',
        position: 'relative',
        overflow: 'hidden',
      }}
    >
      {controls && controls.zoomControls && (
        <MapControls style={{ top: 0, position: 'absolute' }}>
          <ProfileAxis mapId={mapId} />
          <ZoomControls
            onZoomIn={(): void => {
              const wmjsMap = webmapUtils.getWMJSMapById(mapId);
              wmjsMap.zoomIn(undefined!);
            }}
            onZoomOut={(): void => {
              const wmjsMap = webmapUtils.getWMJSMapById(mapId);
              wmjsMap.zoomOut();
            }}
            onZoomReset={(): void => {
              const wmjsMap = webmapUtils.getWMJSMapById(mapId);
              wmjsMap.zoomToLayer(wmjsMap.getActiveLayer());
            }}
          />
        </MapControls>
      )}
      <Box
        sx={{
          gridColumnStart: 1,
          gridRowStart: 1,
        }}
      >
        <ReactMapView
          {...reactMapViewProps}
          showLegend={false}
          displayTimeInMap={false}
          onWMJSMount={(id): void => {
            wmjsMapRef.current = webmapUtils.getWMJSMapById(id);
            if (onWMJSMount) {
              onWMJSMount(id);
            }
          }}
        >
          {children}
        </ReactMapView>
      </Box>
      {displayTimeInMap && dimensions && <MapTime dimensions={dimensions} />}
    </Box>
  );
};
