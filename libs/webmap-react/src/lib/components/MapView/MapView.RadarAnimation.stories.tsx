/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import {
  CustomDate,
  WMJSMap,
  WMLayer,
  webmapUtils,
  AnimationStep,
} from '@opengeoweb/webmap';
import { ThemeWrapper } from '@opengeoweb/theme';
import { MapView, MapViewLayer } from '.';
import { publicLayers, defaultLayers } from '../../layers';

export default {
  title: 'components/MapView',
  component: MapView,
};

export const RadarAnimation = (): React.ReactElement => {
  let currentLatestDate: string | CustomDate;
  return (
    <ThemeWrapper>
      <div style={{ height: '100vh' }}>
        <MapView
          mapId={React.useRef<string>(webmapUtils.generateMapId()).current}
        >
          <MapViewLayer {...publicLayers.baseLayer} />
          <MapViewLayer
            {...publicLayers.radarLayer}
            onLayerReady={(layer: WMLayer, webMap?: WMJSMap): void => {
              webMap!.setAnimationDelay(100);
              if (layer) {
                const timeDim = layer.getDimension('time');
                if (timeDim) {
                  const numTimeSteps = timeDim.size();
                  if (
                    timeDim.getValueForIndex(numTimeSteps - 1) !==
                    currentLatestDate
                  ) {
                    currentLatestDate = timeDim.getValueForIndex(
                      numTimeSteps - 1,
                    );
                    // var currentBeginDate = timeDim.getValueForIndex(numTimeSteps - 48);
                    const dates: { name: string; value: string }[] =
                      [] as AnimationStep[];
                    for (let j = numTimeSteps - 48; j < numTimeSteps; j += 1) {
                      dates.push({
                        name: 'time',
                        value: timeDim.getValueForIndex(j) as string,
                      });
                    }
                    webMap!.stopAnimating();
                    layer.zoomToLayer();
                    webMap!.draw(dates);
                  }
                }
              }
            }}
          />
          <MapViewLayer {...defaultLayers.overLayer} />
        </MapView>
      </div>
    </ThemeWrapper>
  );
};

RadarAnimation.storyName = 'MapView with radar animation';
