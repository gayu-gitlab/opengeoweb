/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { webmapUtils } from '@opengeoweb/webmap';
import { ThemeWrapper } from '@opengeoweb/theme';
import { defaultLayers, publicLayers } from '../../layers';
import { MapView } from './MapView';
import { LegendDialog } from '../Legend';
import { MapViewLayer } from './MapViewLayer';

export default {
  title: 'components/MapView',
  component: MapView,
};

export const MapViewRadar = (): React.ReactElement => (
  <ThemeWrapper>
    <div style={{ height: '100vh' }}>
      <LegendDialog
        layers={[publicLayers.radarLayer]}
        isOpen={true}
        mapId=""
        onClose={(): void => {}}
      />
      <MapView
        mapId={React.useRef<string>(webmapUtils.generateMapId()).current}
      >
        <MapViewLayer {...publicLayers.baseLayer} />
        <MapViewLayer
          {...publicLayers.radarLayer}
          onLayerReady={(layer): void => {
            layer.zoomToLayer();
          }}
        />
        <MapViewLayer {...defaultLayers.overLayer} />
      </MapView>
    </div>
  </ThemeWrapper>
);

MapViewRadar.storyName = 'MapView with radar data';
