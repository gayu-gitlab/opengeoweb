/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { webmapTestSettings, webmapUtils } from '@opengeoweb/webmap';
import { ThemeWrapper } from '@opengeoweb/theme';
import { LegendDialog } from './LegendDialog';

describe('src/components/Legend/LegendDialog', () => {
  it('should follow the isOpen property to determine if open or closed, clicking on the cross should call onClose prop', () => {
    const props = {
      layers: [
        webmapTestSettings.defaultReduxLayerRadarColor,
        webmapTestSettings.defaultReduxLayerRadarKNMI,
      ],
      isOpen: true,
      onClose: jest.fn(),
      mapId: 'mapId_123',
    };
    render(
      <ThemeWrapper>
        <LegendDialog {...props} />
      </ThemeWrapper>,
    );

    // legend dialog should be opened
    expect(screen.getByTestId('moveable-legend')).toBeTruthy();

    // close the legend dialog
    fireEvent.click(screen.getByTestId('closeBtn'));
    expect(props.onClose).toHaveBeenCalled();
  });

  it('should show the mapId in the title if if showMapId passed is true', async () => {
    const props = {
      layers: [
        webmapTestSettings.defaultReduxLayerRadarColor,
        webmapTestSettings.defaultReduxLayerRadarKNMI,
      ],
      isOpen: true,
      onClose: jest.fn(),
      mapId: 'mapId_123',
      showMapId: true,
    };
    render(
      <ThemeWrapper>
        <LegendDialog {...props} />
      </ThemeWrapper>,
    );

    // legend dialog should be opened
    expect(screen.getByTestId('moveable-legend')).toBeTruthy();

    expect(await screen.findByText('Legend mapId_123')).toBeTruthy();
  });

  it('should trigger onMouseDown', async () => {
    const props = {
      layers: [
        webmapTestSettings.defaultReduxLayerRadarColor,
        webmapTestSettings.defaultReduxLayerRadarKNMI,
      ],
      isOpen: true,
      onClose: jest.fn(),
      mapId: 'mapId_123',
      onMouseDown: jest.fn(),
    };
    render(
      <ThemeWrapper>
        <LegendDialog {...props} />
      </ThemeWrapper>,
    );

    // legend dialog should be opened
    expect(screen.getByTestId('moveable-legend')).toBeTruthy();

    expect(await screen.findByText('Legend')).toBeTruthy();

    fireEvent.mouseDown(screen.getByTestId('moveable-legend'));
    expect(props.onMouseDown).toHaveBeenCalled();
  });

  it('should not show the mapId in the title by default', async () => {
    const props = {
      layers: [
        webmapTestSettings.defaultReduxLayerRadarColor,
        webmapTestSettings.defaultReduxLayerRadarKNMI,
      ],
      isOpen: true,
      onClose: jest.fn(),
      mapId: 'mapId_123',
    };
    render(
      <ThemeWrapper>
        <LegendDialog {...props} />
      </ThemeWrapper>,
    );

    // legend dialog should be opened
    expect(screen.getByTestId('moveable-legend')).toBeTruthy();

    expect(await screen.findByText('Legend')).toBeTruthy();
    expect(screen.queryByText('Legend mapId_123')).toBeFalsy();
  });

  it('should show the legend of the layers in the legend dialog if there are layers loaded', () => {
    const props = {
      layers: [
        webmapTestSettings.defaultReduxLayerRadarColor,
        webmapTestSettings.defaultReduxLayerRadarKNMI,
      ],
      isOpen: true,
      onClose: jest.fn(),
      mapId: 'mapId_123',
    };
    render(
      <ThemeWrapper>
        <LegendDialog {...props} />
      </ThemeWrapper>,
    );

    expect(screen.getByTestId('LegendList')).toBeTruthy();
  });

  it('should show a legend for each layer', () => {
    const multiDimensionLayerMock = {
      ...webmapTestSettings.multiDimensionLayer,
      id: webmapTestSettings.WmMultiDimensionLayer.id,
    };
    const layers = [
      webmapTestSettings.defaultReduxLayerRadarKNMI,
      multiDimensionLayerMock,
    ];
    webmapUtils.registerWMLayer(
      webmapTestSettings.WmMultiDimensionLayer,
      webmapTestSettings.WmMultiDimensionLayer.id,
    );
    webmapUtils.registerWMLayer(
      webmapTestSettings.WmdefaultReduxLayerRadarKNMI,
      webmapTestSettings.WmdefaultReduxLayerRadarKNMI.id,
    );
    const props = {
      layers,
      isOpen: true,
      onClose: jest.fn(),
      mapId: 'mapId_123',
    };

    render(
      <ThemeWrapper>
        <LegendDialog {...props} />
      </ThemeWrapper>,
    );
    expect(screen.getAllByTestId('legend').length).toEqual(layers.length);
  });

  it('should show No layers if no layers passed', () => {
    const props = {
      layers: [],
      isOpen: true,
      onClose: jest.fn(),
      mapId: 'mapId_123',
    };

    render(
      <ThemeWrapper>
        <LegendDialog {...props} />
      </ThemeWrapper>,
    );
    expect(screen.getByTestId('NoLayers')).toBeTruthy();
  });
});
