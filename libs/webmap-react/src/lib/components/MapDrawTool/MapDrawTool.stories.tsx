/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Grid, Divider } from '@mui/material';
import { ThemeProvider } from '@opengeoweb/theme';
import { MapDrawToolOptions, useMapDrawTool } from './useMapDrawTool';
import {
  FeatureLayers,
  GeoJSONTextField,
  SelectField,
} from '../MapDraw/storyComponents';
import { StoryLayout } from '../MapDraw/storyComponents/StoryLayout';
import { ToolButton } from '../MapDraw/storyComponents/ToolButton';
import { EditModeButton } from '../MapDraw/storyComponents/EditModeButton';
import { DrawModeExitCallback } from '../MapDraw/MapDraw';
import { DrawModeValue } from './types';
import {
  basicExampleDrawOptions,
  basicExampleMultipleShapeDrawOptions,
  fillOptions,
  getToolIcon,
  opacityOptions,
  strokeWidthOptions,
} from './storyExamplesMapDrawTool';
import { getGeoJSONPropertyValue } from './utils';

export default {
  title: 'components/MapDrawTool',
};

interface BasicMapDrawToolProps {
  mapDrawOptions: MapDrawToolOptions;
}

export const BasicMapDrawTool: React.FC<BasicMapDrawToolProps> = ({
  mapDrawOptions = {},
}: BasicMapDrawToolProps) => {
  const {
    drawModes,
    isInEditMode,
    geoJSON,
    setGeoJSON,
    changeDrawMode,
    setEditMode,
    featureLayerIndex,
    setFeatureLayerIndex,
    activeTool,
    changeActiveTool,
    layers,
    deactivateTool,
    changeProperties,
    getProperties,
  } = useMapDrawTool(mapDrawOptions);

  const featureLayerGeoJSONProperties = getProperties();
  // use polygon tool shape to get default style fallback when no shapes are drawn
  const defaultPolygonShape = drawModes.find(
    (mode) => mode.value === 'POLYGON',
  );
  // geoJSON properties
  const opacity = getGeoJSONPropertyValue(
    'fill-opacity',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as number;

  const fill = getGeoJSONPropertyValue(
    'fill',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as string;

  const strokeOpacity = getGeoJSONPropertyValue(
    'stroke-opacity',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as number;

  const strokeColor = getGeoJSONPropertyValue(
    'stroke',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as string;

  const strokeWidth = getGeoJSONPropertyValue(
    'stroke-width',
    featureLayerGeoJSONProperties,
    defaultPolygonShape,
  ) as number;

  const onExitDrawMode = (reason: DrawModeExitCallback): void => {
    if (
      reason === 'escaped' ||
      (reason === 'doubleClicked' && !mapDrawOptions.shouldAllowMultipleShapes)
    ) {
      deactivateTool();
    }
  };

  const onChangeFeatureLayer = (featureNr: number): void => {
    setFeatureLayerIndex(featureNr);

    const newFeature = geoJSON.features[featureNr];
    const newMode = newFeature.geometry.type.toUpperCase() as DrawModeValue;

    changeDrawMode(newMode);
    setEditMode(true);
  };

  // change geoJSON properties
  const onChangeOpacity = (value: string): void => {
    const parseValue = parseInt(value, 10);
    const newOpacity = parseValue / 100;
    changeProperties({ 'fill-opacity': newOpacity });
  };

  const onChangeFill = (value: string): void => {
    changeProperties({ fill: value });
  };

  const onChangeStrokeOpacity = (value: string): void => {
    const parseValue = parseInt(value, 10);
    const newOpacity = parseValue / 100;
    changeProperties({ 'stroke-opacity': newOpacity });
  };

  const onChangeStroke = (value: string): void => {
    changeProperties({ stroke: value });
  };

  const onChangeStrokeWidth = (value: string): void => {
    changeProperties({ 'stroke-width': value });
  };

  return (
    <ThemeProvider>
      <StoryLayout layers={layers} onExitDrawMode={onExitDrawMode}>
        <>
          <Grid item xs={12}>
            {drawModes.map((mode) => (
              <ToolButton
                key={mode.drawModeId}
                mode={mode}
                onClick={(): void => {
                  changeActiveTool(mode);
                }}
                isSelected={activeTool === mode.drawModeId}
                icon={getToolIcon(mode.drawModeId)}
              />
            ))}

            <Divider />
          </Grid>

          <Grid item xs={12}>
            <SelectField
              label="Fill opacity"
              value={opacity * 100}
              onChangeValue={onChangeOpacity}
              options={opacityOptions}
              labelSuffix=" %"
            />

            <SelectField
              label="Fill color"
              value={fill}
              onChangeValue={onChangeFill}
              options={fillOptions}
              showValueAsColor
            />

            <SelectField
              label="Stroke opacity"
              value={strokeOpacity * 100}
              onChangeValue={onChangeStrokeOpacity}
              options={opacityOptions.filter((option) => option !== 0)}
              width="33.3%"
              labelSuffix=" %"
            />

            <SelectField
              label="Stroke color"
              value={strokeColor}
              onChangeValue={onChangeStroke}
              options={fillOptions}
              width="33.3%"
              showValueAsColor
            />

            <SelectField
              label="Stroke width"
              value={strokeWidth}
              onChangeValue={onChangeStrokeWidth}
              options={strokeWidthOptions}
              width="33.3%"
            />
          </Grid>

          <EditModeButton
            isInEditMode={isInEditMode}
            onToggleEditMode={setEditMode}
            drawMode={activeTool}
          />

          <FeatureLayers
            geojson={geoJSON}
            onChangeLayerIndex={onChangeFeatureLayer}
            activeFeatureLayerIndex={featureLayerIndex}
          />

          <GeoJSONTextField onChangeGeoJSON={setGeoJSON} geoJSON={geoJSON} />
        </>
      </StoryLayout>
    </ThemeProvider>
  );
};

export const BasicMapDrawToolShape = (): React.ReactElement => (
  <BasicMapDrawTool mapDrawOptions={basicExampleDrawOptions} />
);

export const BasicMapDrawToolWithMultipleShapes = (): React.ReactElement => (
  <BasicMapDrawTool mapDrawOptions={basicExampleMultipleShapeDrawOptions} />
);
