/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { DrawFIRLand, ArrowUp } from '@opengeoweb/theme';
import { DRAWMODE } from '../MapDraw/MapDraw';
import { defaultGeoJSONStyleProperties } from '../MapDraw/geojsonShapes';
import { intersectionFeatureNL } from '../MapDraw/storyComponents';
import { DrawMode } from './types';
import { MapDrawToolOptions, defaultModes, getIcon } from './useMapDrawTool';

export const opacityOptions = [100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 0];
export const fillOptions = [
  defaultGeoJSONStyleProperties!.fill,
  '#6e1e91',
  '#FF00FF',
  '#33ccFF',
  '#8F8',
];
export const strokeWidthOptions = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1];

export const getToolIcon = (id: string): React.ReactElement => {
  const defaultIcon = getIcon(id);
  if (defaultIcon) {
    return defaultIcon;
  }
  if (id === 'tool-fir') {
    return <DrawFIRLand />;
  }
  if (id === 'tool-custom-line') {
    return <ArrowUp />;
  }
  return <DrawFIRLand />;
};

// custom buttons
const shapeButtonNL: DrawMode = {
  drawModeId: 'tool-fir',
  value: DRAWMODE.POLYGON,
  title: 'Custom FIR NL polygon',
  shape: intersectionFeatureNL,
  isSelectable: false,
};

const customLineButton: DrawMode = {
  drawModeId: 'tool-custom-line',
  value: DRAWMODE.LINESTRING,
  title: 'Custom LineString',
  shape: {
    type: 'Feature',
    geometry: {
      type: 'LineString',
      coordinates: [
        [4.136829504703722, 50.944730810381465],
        [4.450134704406403, 53.81224530783831],
        [2.5703035061903217, 53.78911570560625],
        [5.252979278644519, 56.722490281934554],
        [8.758081200318252, 53.85846624679025],
        [6.290802752659646, 53.84691579421783],
        [6.623689527343743, 51.0556380592049],
        [4.136829504703722, 50.95706693142843],
      ],
    },
    properties: defaultGeoJSONStyleProperties,
  },
  isSelectable: false,
};

export const basicExampleDrawOptions: MapDrawToolOptions = {
  defaultDrawModes: [...defaultModes, shapeButtonNL, customLineButton],
  defaultGeoJSON: {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        properties: {
          ...defaultGeoJSONStyleProperties,
          selectionType: 'box',
        },
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [9.451665078283622, 53.21804334226515],
              [4.734608638338736, 53.21804334226515],
              [4.734608638338736, 54.57650543915101],
              [9.451665078283622, 54.57650543915101],
              [9.451665078283622, 53.21804334226515],
            ],
          ],
        },
      },
    ],
  } as GeoJSON.FeatureCollection,
};

export const basicExampleMultipleShapeDrawOptions: MapDrawToolOptions = {
  shouldAllowMultipleShapes: true,
  // add multiple tools, and update all with custom color
  defaultDrawModes: [...defaultModes, shapeButtonNL, customLineButton].map(
    (mode) => ({
      ...mode,
      shape: {
        ...mode.shape,
        properties: {
          ...(mode.shape as GeoJSON.Feature).properties,
          stroke: '#8F8',
          'stroke-width': 4,
          'stroke-opacity': 1,
          fill: '#33ccFF',
          'fill-opacity': 0.5,
        },
      },
    }),
  ),
};
