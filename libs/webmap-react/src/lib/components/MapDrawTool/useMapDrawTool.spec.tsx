/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  fireEvent,
  render,
  renderHook,
  screen,
  waitFor,
} from '@testing-library/react';

import {
  DrawPolygon,
  DrawRegion,
  Edit,
  Location,
  Delete,
} from '@opengeoweb/theme';
import { Polygon } from '@turf/turf';

import {
  MapDrawToolOptions,
  defaultBox,
  defaultDelete,
  defaultIntersectionStyleProperties,
  defaultLineString,
  defaultModes,
  defaultPoint,
  defaultPolygon,
  emptyBox,
  emptyIntersectionShape,
  emptyLineString,
  emptyPoint,
  emptyPolygon,
  getIcon,
  useMapDrawTool,
} from './useMapDrawTool';
import { emptyGeoJSON } from '../MapDraw/geojsonShapes';
import { DrawMode } from './types';
import { DRAWMODE } from '../MapDraw/MapDraw';
import {
  NEW_FEATURE_CREATED,
  NEW_LINESTRING_CREATED,
  NEW_POINT_CREATED,
} from '../MapDraw/mapDrawUtils';

const TestComponent = (): React.ReactElement => {
  const { drawModes, changeActiveTool, changeDrawMode, ...state } =
    useMapDrawTool({});

  return (
    <>
      {drawModes.map((mode) => (
        <button
          type="button"
          key={mode.value}
          onClick={(): void => changeActiveTool(mode)}
          name={mode.value}
        >
          {mode.title}
        </button>
      ))}
      <input readOnly name="state" value={JSON.stringify(state)} />
      <button
        type="button"
        onClick={(): void => changeDrawMode('')}
        name="RESET"
      >
        Reset
      </button>
    </>
  );
};

const intersectionShape: GeoJSON.FeatureCollection = {
  type: 'FeatureCollection',
  features: [
    {
      type: 'Feature',
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [5.0, 55.0],
            [4.331914, 55.332644],
            [3.368817, 55.764314],
            [2.761908, 54.379261],
            [3.15576, 52.913554],
            [2.000002, 51.500002],
            [3.370001, 51.369722],
            [3.370527, 51.36867],
            [3.362223, 51.320002],
            [3.36389, 51.313608],
            [3.373613, 51.309999],
            [3.952501, 51.214441],
            [4.397501, 51.452776],
            [5.078611, 51.391665],
            [5.848333, 51.139444],
            [5.651667, 50.824717],
            [6.011797, 50.757273],
            [5.934168, 51.036386],
            [6.222223, 51.361666],
            [5.94639, 51.811663],
            [6.405001, 51.830828],
            [7.053095, 52.237764],
            [7.031389, 52.268885],
            [7.063612, 52.346109],
            [7.065557, 52.385828],
            [7.133055, 52.888887],
            [7.14218, 52.898244],
            [7.191667, 53.3],
            [6.5, 53.666667],
            [6.500002, 55.000002],
            [5.0, 55.0],
          ],
        ],
      },
      properties: {},
    },
  ],
};

const intersectionButtonNL: DrawMode = {
  drawModeId: 'tool-6',
  value: DRAWMODE.POLYGON,
  title: 'Custom FIR NL polygon',
  shape: intersectionShape,
  isSelectable: false,
};

describe('components/MapDraw/useMapDrawTool', () => {
  it('should have default tools with correct props', () => {
    // point
    expect(defaultPoint.drawModeId).toEqual('drawtools-point');
    expect(defaultPoint.shape).toEqual(emptyPoint);
    // polygon
    expect(defaultPolygon.drawModeId).toEqual('drawtools-polygon');
    expect(defaultPolygon.shape).toEqual(emptyPolygon);
    // box
    expect(defaultBox.drawModeId).toEqual('drawtools-box');
    expect(defaultBox.shape).toEqual(emptyBox);
    // linestring
    expect(defaultLineString.drawModeId).toEqual('drawtools-linestring');
    expect(defaultLineString.shape).toEqual(emptyLineString);
    // delete
    expect(defaultDelete.drawModeId).toEqual('drawtools-delete');
    expect(defaultDelete.shape).toEqual(emptyGeoJSON);
  });

  it('should return state', () => {
    const { result } = renderHook(() => useMapDrawTool({}));
    const {
      drawModes,
      isInEditMode,
      featureLayerIndex,
      geoJSON,
      geoJSONIntersection,
      geoJSONIntersectionBounds,
      activeTool,
    } = result.current;

    expect(drawModes).toEqual(defaultModes);
    expect(activeTool).toEqual('');
    expect(isInEditMode).toEqual(false);
    expect(featureLayerIndex).toEqual(0);
    expect(geoJSON).toEqual(emptyGeoJSON);
    expect(geoJSONIntersection).toEqual(emptyIntersectionShape);
    expect(geoJSONIntersectionBounds).toBeUndefined();
  });

  it('should be able to give options', () => {
    const options: MapDrawToolOptions = {
      defaultDrawModes: [defaultPoint, intersectionButtonNL],
      shouldAllowMultipleShapes: false,
      defaultGeoJSON: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              stroke: '#8F8',
              'stroke-width': 4,
              'stroke-opacity': 1,
              fill: '#33ccFF',
              'fill-opacity': 0.5,
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [5.711880814773656, 55.12656218004421],
                  [7.663910745234145, 55.970965630799725],
                  [10.075241835802988, 52.35182632872327],
                  [6.673542618750517, 51.77828698943017],
                  [4.506215269489237, 53.442721496909385],
                  [5.711880814773656, 55.12656218004421],
                ],
              ],
            },
          },
        ],
      },
      defaultGeoJSONIntersection: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              'fill-opacity': 0.5,
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [5.711880814773656, 55.12656218004421],
                  [4.506215269489237, 53.442721496909385],
                  [5.711880814773656, 55.12656218004421],
                ],
              ],
            },
          },
        ],
      },
      defaultGeoJSONIntersectionBounds: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {},
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [6.673542618750517, 51.77828698943017],
                  [4.506215269489237, 53.442721496909385],
                  [5.711880814773656, 55.12656218004421],
                ],
              ],
            },
          },
        ],
      },
    };

    const { result } = renderHook(() => useMapDrawTool(options));
    const {
      drawModes,
      geoJSON,
      geoJSONIntersection,
      geoJSONIntersectionBounds,
    } = result.current;

    expect(drawModes).toEqual(options.defaultDrawModes);
    expect(geoJSON).toEqual(options.defaultGeoJSON);
    expect(geoJSONIntersection).toEqual(options.defaultGeoJSONIntersection);
    expect(geoJSONIntersectionBounds).toEqual(
      options.defaultGeoJSONIntersectionBounds,
    );
  });

  it('should have default edit modes', () => {
    const { result } = renderHook(() => useMapDrawTool({}));

    expect(result.current.drawModes).toEqual([
      defaultPoint,
      defaultPolygon,
      defaultBox,
      defaultLineString,
      defaultDelete,
    ]);
  });

  it('should be able to pass custom controls', () => {
    const customControls: DrawMode[] = [
      {
        value: DRAWMODE.POINT,
        title: 'Point test',
        drawModeId: 'tool-1',
        isSelectable: true,
        shape: {
          type: 'Feature',
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [5.0, 55.0],
                [5.0, 55.0],
              ],
            ],
          },
          properties: {},
        },
      },
      {
        value: DRAWMODE.POLYGON,
        title: 'Polygon test',
        drawModeId: 'tool-2',
        isSelectable: true,
        shape: {
          type: 'Feature',
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [5.0, 55.0],
                [4.331914, 55.332644],
                [7.14218, 52.898244],
                [7.191667, 53.3],
                [6.5, 53.666667],
                [6.500002, 55.000002],
                [5.0, 55.0],
              ],
            ],
          },
          properties: {},
        },
      },
    ];
    const { result } = renderHook(() =>
      useMapDrawTool({ defaultDrawModes: customControls }),
    );

    expect(result.current.drawModes).toEqual(customControls);
  });

  it('should be able to update drawModes', async () => {
    const { result } = renderHook(() => useMapDrawTool({}));

    expect(result.current.drawModes).toEqual([
      defaultPoint,
      defaultPolygon,
      defaultBox,
      defaultLineString,
      defaultDelete,
    ]);

    const newdrawModes = [defaultPolygon, defaultBox];
    await waitFor(() => result.current.setDrawModes(newdrawModes));
    expect(result.current.drawModes).toEqual(newdrawModes);
  });

  it('should change active tool for multiple shapes and keep previous shape', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({ shouldAllowMultipleShapes: true }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // point
    const pointTool = result.current.drawModes[0];
    await waitFor(() => {
      result.current.changeActiveTool(pointTool);
    });

    expect(result.current.activeTool).toEqual(pointTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(pointTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    await waitFor(() => {
      result.current.setGeoJSON({
        ...defaultPoint,
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: [5.0, 55.0],
        },
        properties: {},
      });
    });

    // polygon
    const polygonTool = result.current.drawModes[1];
    await waitFor(() => {
      result.current.changeActiveTool(polygonTool);
    });

    expect(result.current.activeTool).toEqual(polygonTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(polygonTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(2);
    expect(result.current.featureLayerIndex).toEqual(1);
    expect(result.current.isInEditMode).toEqual(true);

    await waitFor(() => {
      result.current.setGeoJSON({
        ...defaultPoint,
        type: 'Feature',
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [5.0, 55.0],
              [5.0, 55.0],
            ],
          ],
        },
        properties: {},
      });
    });

    // box
    const boxTool = result.current.drawModes[2];
    await waitFor(() => {
      result.current.changeActiveTool(boxTool);
    });

    expect(result.current.activeTool).toEqual(boxTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(boxTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(3);
    expect(result.current.featureLayerIndex).toEqual(2);
    expect(result.current.isInEditMode).toEqual(true);

    // lineString
    const lineStringTool = result.current.drawModes[3];
    await waitFor(() => {
      result.current.changeActiveTool(lineStringTool);
    });

    expect(result.current.activeTool).toEqual(lineStringTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(
      lineStringTool.shape,
    );
    expect(result.current.geoJSON.features.length).toEqual(3);
    expect(result.current.featureLayerIndex).toEqual(2);
    expect(result.current.isInEditMode).toEqual(true);

    const testLineString: GeoJSON.Feature<GeoJSON.LineString> = {
      ...defaultPoint,
      type: 'Feature',
      geometry: {
        type: 'LineString',
        coordinates: [
          [6.821858618368839, 55.35931396530716],
          [4.308460603331475, 53.987286437238204],
          [6.158423609323443, 53.26090258204719],
          [6.898408811720228, 53.648350541471274],
          [6.911167177278794, 54.78223276254279],
        ],
      },
      properties: {},
    };

    await waitFor(() => {
      result.current.setGeoJSON(testLineString);
    });

    // same shape lineString again so it resets state
    await waitFor(() => {
      result.current.changeActiveTool(lineStringTool);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON.features).toContainEqual(testLineString);
    expect(result.current.featureLayerIndex).toEqual(2);
    expect(result.current.isInEditMode).toEqual(false);

    // delete so all geoJSON is removed
    const deleteTool = result.current.drawModes[4];

    await waitFor(() => {
      result.current.changeActiveTool(deleteTool);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.geoJSON.features.length).toEqual(0);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);
  });

  it('should change active tool for multiple shapes and not add empty shapes when changing tool', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({ shouldAllowMultipleShapes: true }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // point
    const pointTool = result.current.drawModes[0];
    await waitFor(() => {
      result.current.changeActiveTool(pointTool);
    });

    expect(result.current.activeTool).toEqual(pointTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(pointTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // polygon
    const polygonTool = result.current.drawModes[1];
    await waitFor(() => {
      result.current.changeActiveTool(polygonTool);
    });

    expect(result.current.activeTool).toEqual(polygonTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(polygonTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // box
    const boxTool = result.current.drawModes[2];
    await waitFor(() => {
      result.current.changeActiveTool(boxTool);
    });

    expect(result.current.activeTool).toEqual(boxTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(boxTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // lineString
    const lineStringTool = result.current.drawModes[3];
    await waitFor(() => {
      result.current.changeActiveTool(lineStringTool);
    });

    expect(result.current.activeTool).toEqual(lineStringTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(
      lineStringTool.shape,
    );
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // same shape lineString again so it resets state
    await waitFor(() => {
      result.current.changeActiveTool(lineStringTool);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.geoJSON.features).toEqual([]);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // delete so all geoJSON is removed
    const deleteTool = result.current.drawModes[4];

    await waitFor(() => {
      result.current.changeActiveTool(deleteTool);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.geoJSON.features.length).toEqual(0);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);
  });

  it('should remove empty shape when deactivating tool', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({ shouldAllowMultipleShapes: true }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // point
    const pointTool = result.current.drawModes[0];
    await waitFor(() => {
      result.current.changeActiveTool(pointTool);
    });

    expect(result.current.activeTool).toEqual(pointTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(pointTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // deactivate point
    await waitFor(() => {
      result.current.changeActiveTool(pointTool);
    });
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.isInEditMode).toEqual(false);

    // polygon
    const polygonTool = result.current.drawModes[1];
    await waitFor(() => {
      result.current.changeActiveTool(polygonTool);
    });

    expect(result.current.activeTool).toEqual(polygonTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(polygonTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // deactivate polygon
    await waitFor(() => {
      result.current.changeActiveTool(polygonTool);
    });
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.isInEditMode).toEqual(false);

    // box
    const boxTool = result.current.drawModes[2];
    await waitFor(() => {
      result.current.changeActiveTool(boxTool);
    });

    expect(result.current.activeTool).toEqual(boxTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(boxTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // deactivate box
    await waitFor(() => {
      result.current.changeActiveTool(boxTool);
    });
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.isInEditMode).toEqual(false);

    // lineString
    const lineStringTool = result.current.drawModes[3];
    await waitFor(() => {
      result.current.changeActiveTool(lineStringTool);
    });

    expect(result.current.activeTool).toEqual(lineStringTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(
      lineStringTool.shape,
    );
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // same shape lineString again so it resets state
    await waitFor(() => {
      result.current.changeActiveTool(lineStringTool);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.geoJSON.features).toEqual([]);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // delete so all geoJSON is removed
    const deleteTool = result.current.drawModes[4];

    await waitFor(() => {
      result.current.changeActiveTool(deleteTool);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.geoJSON.features.length).toEqual(0);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);
  });

  it('should remove empty shape when but keep drawn shapes', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({ shouldAllowMultipleShapes: true }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // point
    const pointTool = result.current.drawModes[0];
    await waitFor(() => {
      result.current.changeActiveTool(pointTool);
    });

    expect(result.current.activeTool).toEqual(pointTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(pointTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    const shape = result.current.drawModes[0]
      .shape as GeoJSON.Feature<GeoJSON.Point>;
    const pointShape: GeoJSON.Feature<GeoJSON.Point> = {
      ...shape,
      geometry: {
        ...shape.geometry,
        coordinates: [9.041814225559198, 55.323036286422266],
      },
    };
    await waitFor(() => {
      result.current.setGeoJSON(pointShape);
    });

    expect(result.current.activeTool).toEqual(
      result.current.drawModes[0].drawModeId,
    );
    const expectedResult = {
      ...emptyGeoJSON,
      features: [pointShape],
    };
    expect(result.current.geoJSON).toEqual(expectedResult);
    expect(result.current.featureLayerIndex).toEqual(0);

    // deactivate point
    await waitFor(() => {
      result.current.changeActiveTool(pointTool);
    });
    expect(result.current.geoJSON).toEqual(expectedResult);
    expect(result.current.isInEditMode).toEqual(false);

    // polygon
    const polygonTool = result.current.drawModes[1];
    await waitFor(() => {
      result.current.changeActiveTool(polygonTool);
    });

    expect(result.current.activeTool).toEqual(polygonTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(polygonTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(2);
    expect(result.current.featureLayerIndex).toEqual(1);
    expect(result.current.isInEditMode).toEqual(true);

    // deactivate polygon should remove it
    await waitFor(() => {
      result.current.changeActiveTool(polygonTool);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(expectedResult);
    expect(result.current.geoJSON.features).not.toContainEqual(
      polygonTool.shape,
    );
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);
  });

  it('should change active tool and open to edit mode if geojson has the same selectionType properties', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({
        shouldAllowMultipleShapes: false,
      }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // activate point
    const pointTool = result.current.drawModes[0];
    await waitFor(() => {
      result.current.changeActiveTool(pointTool);
    });

    expect(result.current.activeTool).toEqual(pointTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(pointTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    await waitFor(() => {
      result.current.deactivateTool();
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.isInEditMode).toEqual(false);

    // activate same tool again
    await waitFor(() => {
      result.current.changeActiveTool(pointTool);
    });
    expect(result.current.activeTool).toEqual(pointTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(pointTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);
  });

  it('should reset active tool', async () => {
    const { result } = renderHook(() => useMapDrawTool({}));

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // activate point
    const pointTool = result.current.drawModes[0];
    await waitFor(() => {
      result.current.changeActiveTool(pointTool);
    });

    expect(result.current.activeTool).toEqual(pointTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(pointTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // deactivate
    await waitFor(() => {
      result.current.deactivateTool();
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.isInEditMode).toEqual(false);
  });

  it('should change active tool for single shape', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({ shouldAllowMultipleShapes: false }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);
    expect(result.current.geoJSON.features.length).toEqual(0);

    // point
    const pointTool = result.current.drawModes[0];
    await waitFor(() => {
      result.current.changeActiveTool(pointTool);
    });

    expect(result.current.activeTool).toEqual(pointTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(pointTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // polygon
    const polygonTool = result.current.drawModes[1];
    await waitFor(() => {
      result.current.changeActiveTool(polygonTool);
    });

    expect(result.current.activeTool).toEqual(polygonTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(polygonTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // box
    const boxTool = result.current.drawModes[2];
    await waitFor(() => {
      result.current.changeActiveTool(boxTool);
    });

    expect(result.current.activeTool).toEqual(boxTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(boxTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // lineString
    const lineStringTool = result.current.drawModes[3];
    await waitFor(() => {
      result.current.changeActiveTool(lineStringTool);
    });

    expect(result.current.activeTool).toEqual(lineStringTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(
      lineStringTool.shape,
    );
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // same shape lineString again so it resets state
    await waitFor(() => {
      result.current.changeActiveTool(lineStringTool);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // delete so all geoJSON is removed
    const deleteTool = result.current.drawModes[4];

    await waitFor(() => {
      result.current.changeActiveTool(deleteTool);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.geoJSON.features.length).toEqual(0);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);
  });

  it('should handle correct edit mode when changing tool when multiple shapes are not allowed', async () => {
    const isSelectableTool = defaultPolygon;
    const isNotSelectableTool = intersectionButtonNL;
    const drawModes = [isSelectableTool, isNotSelectableTool];
    const { result: singleShapeResult } = renderHook(() =>
      useMapDrawTool({
        shouldAllowMultipleShapes: false,
        defaultDrawModes: drawModes,
      }),
    );
    // test selectable tool
    expect(singleShapeResult.current.isInEditMode).toBeFalsy();
    await waitFor(() => {
      singleShapeResult.current.changeActiveTool(isSelectableTool);
    });
    expect(singleShapeResult.current.isInEditMode).toBeTruthy();
    await waitFor(() => {
      singleShapeResult.current.changeActiveTool(isSelectableTool);
    });
    expect(singleShapeResult.current.isInEditMode).toBeFalsy();
    // test non selectable tool
    await waitFor(() => {
      singleShapeResult.current.changeActiveTool(isNotSelectableTool);
    });
    expect(singleShapeResult.current.isInEditMode).toBeFalsy();
  });

  it('should handle correct edit mode when changing tool when multiple shapes are allowed', async () => {
    const isSelectableTool = defaultPolygon;
    const isNotSelectableTool = intersectionButtonNL;
    const drawModes = [isSelectableTool, isNotSelectableTool];
    const { result: singleShapeResult } = renderHook(() =>
      useMapDrawTool({
        shouldAllowMultipleShapes: true,
        defaultDrawModes: drawModes,
      }),
    );
    // test selectable tool
    expect(singleShapeResult.current.isInEditMode).toBeFalsy();
    await waitFor(() => {
      singleShapeResult.current.changeActiveTool(isSelectableTool);
    });
    expect(singleShapeResult.current.isInEditMode).toBeTruthy();
    await waitFor(() => {
      singleShapeResult.current.changeActiveTool(isSelectableTool);
    });
    expect(singleShapeResult.current.isInEditMode).toBeFalsy();
    // test non selectable tool
    await waitFor(() => {
      singleShapeResult.current.changeActiveTool(isNotSelectableTool);
    });
    expect(singleShapeResult.current.isInEditMode).toBeFalsy();
    await waitFor(() => {
      singleShapeResult.current.changeActiveTool(isNotSelectableTool);
    });
    expect(singleShapeResult.current.isInEditMode).toBeFalsy();
  });

  it('should not update featureLayerIndex when a new shape is selected and previous shape is empty', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({ shouldAllowMultipleShapes: true }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);
    expect(result.current.geoJSON.features.length).toEqual(0);

    // point
    const pointTool = result.current.drawModes[0];
    await waitFor(() => {
      result.current.changeActiveTool(pointTool);
    });

    expect(result.current.activeTool).toEqual(pointTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(pointTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    const boxTool = result.current.drawModes[2];
    await waitFor(() => {
      result.current.changeActiveTool(boxTool);
    });

    expect(result.current.activeTool).toEqual(boxTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(boxTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);
  });

  it('should not reset shape when opening last selected deactivated tool', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({ shouldAllowMultipleShapes: false }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);
    expect(result.current.geoJSON.features.length).toEqual(0);

    // point
    const pointTool = result.current.drawModes[0];
    await waitFor(() => {
      result.current.changeActiveTool(pointTool);
    });

    expect(result.current.activeTool).toEqual(pointTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(pointTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);

    // deactive tool
    await waitFor(() => {
      result.current.setActiveTool('');
    });
    await waitFor(() => {
      result.current.changeActiveTool(pointTool);
    });
    expect(result.current.activeTool).toEqual(pointTool.drawModeId);
    expect(result.current.geoJSON.features).toContainEqual(pointTool.shape);
    expect(result.current.geoJSON.features.length).toEqual(1);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(true);
  });

  it('should set geoJSON and feature for multiple shapes to featureCollection ', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({ shouldAllowMultipleShapes: true }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // point
    const shape = result.current.drawModes[0]
      .shape as GeoJSON.Feature<GeoJSON.Point>;
    const pointShape: GeoJSON.Feature<GeoJSON.Point> = {
      ...shape,
      geometry: {
        ...shape.geometry,
        coordinates: [9.041814225559198, 55.323036286422266],
      },
    };
    await waitFor(() => {
      result.current.setGeoJSON(pointShape);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual({
      ...emptyGeoJSON,
      features: [pointShape],
    });
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // add another feature
    await waitFor(() => {
      result.current.setGeoJSON(pointShape);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual({
      ...emptyGeoJSON,
      features: [pointShape, pointShape],
    });
    expect(result.current.featureLayerIndex).toEqual(0);
  });

  it('should set geoJSON and feature for single shapes to featureCollection ', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({ shouldAllowMultipleShapes: false }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // point
    const pointShape = result.current.drawModes[0].shape;
    await waitFor(() => {
      result.current.setGeoJSON(pointShape);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual({
      ...emptyGeoJSON,
      features: [pointShape],
    });
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // add another feature
    await waitFor(() => {
      result.current.setGeoJSON(pointShape);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual({
      ...emptyGeoJSON,
      features: [pointShape],
    });
    expect(result.current.featureLayerIndex).toEqual(0);
  });

  it('should set geoJSON as featureCollection ', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({ shouldAllowMultipleShapes: false }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // feature collection
    const pointShape: GeoJSON.Feature = result.current.drawModes[0]
      .shape as GeoJSON.Feature;
    const featureCollection: GeoJSON.FeatureCollection = {
      ...emptyGeoJSON,
      features: [pointShape],
    };
    await waitFor(() => {
      result.current.setGeoJSON(featureCollection);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(featureCollection);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // add another feature
    await waitFor(() => {
      result.current.setGeoJSON(featureCollection);
    });

    expect(result.current.geoJSON).toEqual(featureCollection);

    expect(result.current.featureLayerIndex).toEqual(0);
  });

  it('should update intersection when setting geoJSON', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({
        defaultGeoJSONIntersectionBounds: intersectionShape,
      }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // polygon
    const polygon: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            stroke: '#8F8',
            'stroke-width': 4,
            'stroke-opacity': 1,
            fill: '#33ccFF',
            'fill-opacity': 0.5,
            _type: 'box',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [4.9971865317142035, 50.52592311916742],
                [4.9971865317142035, 52.8606427256928],
                [8.6153686794114, 52.8606427256928],
                [8.6153686794114, 50.52592311916742],
                [4.9971865317142035, 50.52592311916742],
              ],
            ],
          },
        },
      ],
    };

    const intersectionResult = result.current.geoJSONIntersection!
      .features[0] as GeoJSON.Feature<Polygon>;

    expect(intersectionResult.properties).toEqual({});
    expect(
      (intersectionResult as GeoJSON.Feature<Polygon>).geometry.coordinates[0]
        .length,
    ).toEqual(0);

    await waitFor(() => {
      result.current.setGeoJSON(polygon);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(polygon);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    const intersectionResult2 = (
      result.current.layers.find((layer) => layer.id === 'intersection-layer')!
        .geojson as GeoJSON.FeatureCollection
    ).features[0];

    expect(intersectionResult2.properties).toEqual(
      defaultIntersectionStyleProperties,
    );
    expect(
      (intersectionResult2 as GeoJSON.Feature<Polygon>).geometry.coordinates[0]
        .length,
    ).toEqual(16);
  });

  it('should update intersection when setting geoJSON with defaultGeoJSONIntersectionProperties', async () => {
    const featurePropsIntersectionEnd = {
      stroke: '#000000',
      'stroke-width': 1.5,
      'stroke-opacity': 1,
      fill: '#6e1e91',
      'fill-opacity': 0.5,
    };
    const { result } = renderHook(() =>
      useMapDrawTool({
        defaultGeoJSONIntersectionBounds: intersectionShape,
        defaultGeoJSONIntersectionProperties: featurePropsIntersectionEnd,
      }),
    );

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    // polygon
    const polygon: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            stroke: '#8F8',
            'stroke-width': 4,
            'stroke-opacity': 1,
            fill: '#33ccFF',
            'fill-opacity': 0.5,
            _type: 'box',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [4.9971865317142035, 50.52592311916742],
                [4.9971865317142035, 52.8606427256928],
                [8.6153686794114, 52.8606427256928],
                [8.6153686794114, 50.52592311916742],
                [4.9971865317142035, 50.52592311916742],
              ],
            ],
          },
        },
      ],
    };

    const intersectionResult = result.current.geoJSONIntersection!
      .features[0] as GeoJSON.Feature<Polygon>;

    expect(intersectionResult.properties).toEqual({});
    expect(
      (intersectionResult as GeoJSON.Feature<Polygon>).geometry.coordinates[0]
        .length,
    ).toEqual(0);

    await waitFor(() => {
      result.current.setGeoJSON(polygon);
    });

    expect(result.current.activeTool).toEqual('');
    expect(result.current.geoJSON).toEqual(polygon);
    expect(result.current.featureLayerIndex).toEqual(0);
    expect(result.current.isInEditMode).toEqual(false);

    const intersectionResult2 = (
      result.current.layers.find((layer) => layer.id === 'intersection-layer')!
        .geojson as GeoJSON.FeatureCollection
    ).features[0];

    expect(intersectionResult2.properties).toEqual(featurePropsIntersectionEnd);
    expect(
      (intersectionResult2 as GeoJSON.Feature<Polygon>).geometry.coordinates[0]
        .length,
    ).toEqual(16);
  });

  it('should set draw mode', async () => {
    const { result } = renderHook(() => useMapDrawTool({}));
    expect(result.current.activeTool).toEqual('');

    // polygon
    await waitFor(() => {
      result.current.changeDrawMode(DRAWMODE.POLYGON);
    });

    expect(result.current.activeTool).toEqual(
      result.current.drawModes[1].drawModeId,
    );

    // linestring
    await waitFor(() => {
      result.current.changeDrawMode(DRAWMODE.LINESTRING);
    });

    expect(result.current.activeTool).toEqual(
      result.current.drawModes[3].drawModeId,
    );
  });

  it('should not update geoJSON when disable active tool', () => {
    render(<TestComponent />);

    const stateInput = screen.getByRole('textbox');
    expect(JSON.parse(stateInput.getAttribute('value')!).geoJSON).toEqual(
      emptyGeoJSON,
    );

    // add point
    fireEvent.click(screen.getByRole('button', { name: /POINT/i }));

    expect(
      JSON.parse(stateInput.getAttribute('value')!).geoJSON.features,
    ).toContainEqual(emptyPoint);
    expect(
      JSON.parse(stateInput.getAttribute('value')!).featureLayerIndex,
    ).toEqual(0);

    // exit draw mode
    fireEvent.click(screen.getByRole('button', { name: /RESET/i }));

    expect(
      JSON.parse(stateInput.getAttribute('value')!).geoJSON.features,
    ).toContainEqual(emptyPoint);
    expect(
      JSON.parse(stateInput.getAttribute('value')!).featureLayerIndex,
    ).toEqual(0);
  });

  it('should return correct layers', () => {
    const { result } = renderHook(() => useMapDrawTool({}));
    expect(result.current.layers).toHaveLength(1);
    expect(result.current.layers[0].id).toEqual('draw-layer');

    const { result: resultWithIntersectionLayers } = renderHook(() =>
      useMapDrawTool({
        defaultGeoJSONIntersectionBounds: intersectionShape,
      }),
    );
    expect(resultWithIntersectionLayers.current.layers).toHaveLength(3);
    expect(resultWithIntersectionLayers.current.layers[0].id).toEqual(
      'static-layer',
    );
    expect(resultWithIntersectionLayers.current.layers[1].id).toEqual(
      'intersection-layer',
    );
    expect(resultWithIntersectionLayers.current.layers[2].id).toEqual(
      'draw-layer',
    );
  });

  it('should return correct layers with custom layer ids', () => {
    const testOptions = {
      geoJSONLayerId: 'draw-layer',
      geoJSONIntersectionLayerId: 'intersection-layer',
      geoJSONIntersectionBoundsLayerId: 'static-layer',
    };
    const { result } = renderHook(() => useMapDrawTool(testOptions));
    expect(result.current.layers).toHaveLength(1);
    expect(result.current.layers[0].id).toEqual(testOptions.geoJSONLayerId);

    const { result: resultWithIntersectionLayers } = renderHook(() =>
      useMapDrawTool({
        defaultGeoJSONIntersectionBounds: intersectionShape,
      }),
    );
    expect(resultWithIntersectionLayers.current.layers).toHaveLength(3);
    expect(resultWithIntersectionLayers.current.layers[0].id).toEqual(
      testOptions.geoJSONIntersectionBoundsLayerId,
    );
    expect(resultWithIntersectionLayers.current.layers[1].id).toEqual(
      testOptions.geoJSONIntersectionLayerId,
    );
    expect(resultWithIntersectionLayers.current.layers[2].id).toEqual(
      testOptions.geoJSONLayerId,
    );
  });

  it('should return a geojson layer from getLayer', () => {
    const { result } = renderHook(() => useMapDrawTool({}));
    expect(result.current.getLayer('geoJSON', 'draw')).toEqual({
      id: 'draw',
      geojson: { type: 'FeatureCollection', features: [] },
      isInEditMode: false,
      drawMode: '',
      updateGeojson: result.current.setGeoJSON,
      selectedFeatureIndex: 0,
    });
    expect(
      result.current.getLayer('geoJSONIntersection', 'intersection'),
    ).toEqual({
      id: 'intersection',
      geojson: result.current.geoJSONIntersection,
      isInEditMode: false,
    });
    expect(
      result.current.getLayer(
        'geoJSONIntersectionBounds',
        'intersectionBounds',
      ),
    ).toEqual({
      id: 'intersectionBounds',
      geojson: result.current.geoJSONIntersectionBounds,
      isInEditMode: false,
    });

    const polygon: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            stroke: '#8F8',
            'stroke-width': 4,
            'stroke-opacity': 1,
            fill: '#33ccFF',
            'fill-opacity': 0.5,
            _type: 'box',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [4.9971865317142035, 50.52592311916742],
                [4.9971865317142035, 52.8606427256928],
                [8.6153686794114, 52.8606427256928],
                [8.6153686794114, 50.52592311916742],
                [4.9971865317142035, 50.52592311916742],
              ],
            ],
          },
        },
      ],
    };

    const polygonInterception: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          properties: {
            stroke: '#8F8',
            'stroke-width': 4,

            _type: 'box',
          },
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [8.6153686794114, 50.52592311916742],
                [4.9971865317142035, 50.52592311916742],
              ],
            ],
          },
        },
      ],
    };

    const { result: resultWithOptions } = renderHook(() =>
      useMapDrawTool({
        defaultGeoJSON: polygon,
        defaultGeoJSONIntersection: polygonInterception,
        defaultGeoJSONIntersectionBounds: intersectionShape,
      }),
    );

    expect(resultWithOptions.current.getLayer('geoJSON', 'draw')).toEqual({
      id: 'draw',
      geojson: polygon,
      isInEditMode: false,
      drawMode: '',
      updateGeojson: resultWithOptions.current.setGeoJSON,
      selectedFeatureIndex: 0,
    });
    expect(
      resultWithOptions.current.getLayer('geoJSONIntersection', 'intersection'),
    ).toEqual({
      id: 'intersection',
      geojson: polygonInterception,
      isInEditMode: false,
    });
    expect(
      resultWithOptions.current.getLayer(
        'geoJSONIntersectionBounds',
        'intersectionBounds',
      ),
    ).toEqual({
      id: 'intersectionBounds',
      geojson: intersectionShape,
      isInEditMode: false,
    });
  });

  it('should be able to change properties for geoJSON feature', async () => {
    const customControls: DrawMode[] = [
      {
        value: DRAWMODE.POINT,
        title: 'Point test',
        drawModeId: 'tool-1',
        isSelectable: true,
        shape: {
          type: 'Feature',
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [5.0, 55.0],
                [5.0, 55.0],
              ],
            ],
          },
          properties: {},
        },
      },
      {
        value: DRAWMODE.POLYGON,
        title: 'Polygon test',
        drawModeId: 'tool-2',
        isSelectable: true,
        shape: {
          type: 'Feature',
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [5.0, 55.0],
                [4.331914, 55.332644],
                [7.14218, 52.898244],
                [7.191667, 53.3],
                [6.5, 53.666667],
                [6.500002, 55.000002],
                [5.0, 55.0],
              ],
            ],
          },
          properties: {},
        },
      },
    ];
    const { result } = renderHook(() =>
      useMapDrawTool({ defaultDrawModes: customControls }),
    );

    expect(result.current.drawModes).toEqual(customControls);

    expect(
      (result.current.drawModes[0].shape as GeoJSON.Feature).properties,
    ).toEqual({});
    expect(
      (result.current.drawModes[1].shape as GeoJSON.Feature).properties,
    ).toEqual({});

    const newProperties = {
      'stroke-width': 4,
      'stroke-opacity': 1,
      fill: '#33ccFF',
      'fill-opacity': 0.5,
    };

    await waitFor(() => result.current.changeProperties(newProperties));

    expect(
      (result.current.drawModes[0].shape as GeoJSON.Feature).properties,
    ).toEqual(newProperties);
    expect(
      (result.current.drawModes[1].shape as GeoJSON.Feature).properties,
    ).toEqual(newProperties);
  });

  it('should be able to change properties for geoJSON feature collection', async () => {
    const customControls: DrawMode[] = [
      {
        value: DRAWMODE.POINT,
        title: 'Point test',
        drawModeId: 'tool-1',
        isSelectable: true,
        shape: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [
                    [5.0, 55.0],
                    [5.0, 55.0],
                  ],
                ],
              },
              properties: {},
            },
          ],
        },
      },
      {
        value: DRAWMODE.POLYGON,
        title: 'Polygon test',
        drawModeId: 'tool-2',
        isSelectable: true,
        shape: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [
                    [5.0, 55.0],
                    [4.331914, 55.332644],
                    [7.14218, 52.898244],
                    [7.191667, 53.3],
                    [6.5, 53.666667],
                    [6.500002, 55.000002],
                    [5.0, 55.0],
                  ],
                ],
              },
              properties: {},
            },
          ],
        },
      },
    ];
    const { result } = renderHook(() =>
      useMapDrawTool({ defaultDrawModes: customControls }),
    );

    expect(result.current.drawModes).toEqual(customControls);

    expect(
      (result.current.drawModes[0].shape as GeoJSON.FeatureCollection)
        .features[0].properties,
    ).toEqual({});
    expect(
      (result.current.drawModes[1].shape as GeoJSON.FeatureCollection)
        .features[0].properties,
    ).toEqual({});

    const newProperties = {
      'stroke-width': 4,
      'stroke-opacity': 1,
      fill: '#33ccFF',
      'fill-opacity': 0.5,
    };

    await waitFor(() => result.current.changeProperties(newProperties));

    expect(
      (result.current.drawModes[0].shape as GeoJSON.FeatureCollection)
        .features[0].properties,
    ).toEqual(newProperties);
    expect(
      (result.current.drawModes[1].shape as GeoJSON.FeatureCollection)
        .features[0].properties,
    ).toEqual(newProperties);
  });

  it('should be able to get properties for geoJSON ', async () => {
    const { result } = renderHook(() => useMapDrawTool({}));

    expect(result.current.getProperties()).toEqual({});

    const testJSONWithoutProperties: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [5.0, 55.0],
                [4.331914, 55.332644],
                [7.14218, 52.898244],
                [7.191667, 53.3],
                [6.5, 53.666667],
                [6.500002, 55.000002],
                [5.0, 55.0],
              ],
            ],
          },
          properties: {},
        },
      ],
    };

    await waitFor(() => {
      result.current.setGeoJSON(testJSONWithoutProperties);
    });

    expect(result.current.getProperties()).toEqual({});
    const testProperties = {
      'stroke-width': 4,
      'stroke-opacity': 1,
      fill: '#33ccFF',
      'fill-opacity': 0.5,
    };

    const testJSONWithProperties: GeoJSON.FeatureCollection = {
      type: 'FeatureCollection',
      features: [
        {
          type: 'Feature',
          geometry: {
            type: 'Polygon',
            coordinates: [
              [
                [5.0, 55.0],
                [4.331914, 55.332644],
                [7.14218, 52.898244],
                [7.191667, 53.3],
                [6.5, 53.666667],
                [6.500002, 55.000002],
                [5.0, 55.0],
              ],
            ],
          },
          properties: testProperties,
        },
      ],
    };

    await waitFor(() => {
      result.current.setGeoJSON(testJSONWithProperties);
    });

    expect(result.current.getProperties()).toEqual(testProperties);
  });

  it('should set geoJSON and move feature for Point when shouldAllowMultipleShapes = true', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({ shouldAllowMultipleShapes: true }),
    );

    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);

    const testPoint = {
      type: 'Feature',
      properties: {
        fill: '#33ccFF',
        'fill-opacity': 0.5,
        stroke: '#8F8',
        'stroke-width': 4,
        'stroke-opacity': 1,
        selectionType: 'point',
      },
      geometry: {
        type: 'Point',
        coordinates: [10.296838916063482, 54.857580549872495],
      },
    } as GeoJSON.Feature<GeoJSON.Point>;

    // feature collection with point
    const featureCollection: GeoJSON.FeatureCollection = {
      ...emptyGeoJSON,
      features: [testPoint],
    };
    await waitFor(() => {
      result.current.setGeoJSON(featureCollection);
    });

    expect(result.current.geoJSON).toEqual(featureCollection);
    expect(result.current.featureLayerIndex).toEqual(0);

    // add another point
    await waitFor(() => {
      result.current.setGeoJSON(featureCollection, NEW_POINT_CREATED);
    });

    expect(result.current.geoJSON).toEqual({
      ...featureCollection,
      features: [testPoint, testPoint],
    });

    expect(result.current.featureLayerIndex).toEqual(1);
  });

  it('should set geoJSON and move feature for Polygon when shouldAllowMultipleShapes = true', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({ shouldAllowMultipleShapes: true }),
    );

    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);

    const testPolygon = {
      type: 'Feature',
      properties: {
        fill: '#33ccFF',
        'fill-opacity': 0.5,
        stroke: '#8F8',
        'stroke-width': 4,
        'stroke-opacity': 1,
        selectionType: 'poly',
      },
      geometry: {
        type: 'Polygon',
        coordinates: [
          [
            [7.984603623737196, 54.28390264411052],
            [8.206578211800519, 53.86060510405404],
            [8.465548564541065, 52.906285336434834],
            [6.125566448706862, 53.34472716392099],
            [7.984603623737196, 54.28390264411052],
          ],
        ],
      },
    } as GeoJSON.Feature<GeoJSON.Polygon>;

    // feature collection with polygon
    const featureCollection: GeoJSON.FeatureCollection = {
      ...emptyGeoJSON,
      features: [testPolygon],
    };
    await waitFor(() => {
      result.current.setGeoJSON(featureCollection);
    });

    expect(result.current.geoJSON).toEqual(featureCollection);
    expect(result.current.featureLayerIndex).toEqual(0);

    // add another polygon
    await waitFor(() => {
      result.current.setGeoJSON(
        {
          ...featureCollection,
          features: [
            {
              ...testPolygon,
              geometry: {
                ...testPolygon.geometry,
                coordinates: [
                  ...testPolygon.geometry.coordinates,
                  ...testPolygon.geometry.coordinates,
                ],
              },
            },
          ],
        },
        NEW_FEATURE_CREATED,
      );
    });

    expect(result.current.geoJSON).toEqual({
      ...featureCollection,
      features: [testPolygon, testPolygon],
    });

    expect(result.current.featureLayerIndex).toEqual(1);
  });

  it('should set geoJSON and move feature for LineString when shouldAllowMultipleShapes = true', async () => {
    const { result } = renderHook(() =>
      useMapDrawTool({ shouldAllowMultipleShapes: true }),
    );

    expect(result.current.geoJSON).toEqual(emptyGeoJSON);
    expect(result.current.featureLayerIndex).toEqual(0);
    const lastCoords = [8.39155703518662, 53.95323023838055];

    const testLineString = {
      type: 'Feature',
      properties: {
        fill: '#33ccFF',
        'fill-opacity': 0.5,
        stroke: '#8F8',
        'stroke-width': 4,
        'stroke-opacity': 1,
        selectionType: 'linestring',
      },
      geometry: {
        type: 'LineString',
        coordinates: [
          [7.855118447366924, 54.359422791939494],
          [5.154427625929823, 53.33920519711118],
          [8.761514681958827, 53.65829856779757],
          lastCoords,
        ],
      },
    } as GeoJSON.Feature<GeoJSON.LineString>;

    // feature collection with linestring
    const featureCollection: GeoJSON.FeatureCollection = {
      ...emptyGeoJSON,
      features: [testLineString],
    };
    await waitFor(() => {
      result.current.setGeoJSON(featureCollection);
    });

    expect(result.current.geoJSON).toEqual(featureCollection);
    expect(result.current.featureLayerIndex).toEqual(0);

    // add another linestring
    await waitFor(() => {
      result.current.setGeoJSON(featureCollection, NEW_LINESTRING_CREATED);
    });

    expect(result.current.geoJSON).toEqual({
      ...featureCollection,
      features: [
        testLineString,
        {
          ...testLineString,
          geometry: {
            ...testLineString.geometry,
            coordinates: [lastCoords, lastCoords],
          },
        },
      ],
    });

    expect(result.current.featureLayerIndex).toEqual(1);
  });

  describe('getIcon', () => {
    it('should return a Location icon for "drawtools-point" drawModeId', () => {
      expect(getIcon('drawtools-point')).toEqual(<Location />);
    });

    it('should return a DrawPolygon icon for "drawtools-polygon" drawModeId', () => {
      expect(getIcon('drawtools-polygon')).toEqual(<DrawPolygon />);
    });

    it('should return a DrawRegion icon for "drawtools-box" drawModeId', () => {
      expect(getIcon('drawtools-box')).toEqual(<DrawRegion />);
    });

    it('should return an Edit icon for "drawtools-linestring" drawModeId', () => {
      expect(getIcon('drawtools-linestring')).toEqual(<Edit />);
    });

    it('should return a Delete icon for "drawtools-delete" drawModeId', () => {
      expect(getIcon('drawtools-delete')).toEqual(<Delete />);
    });

    it('should return null for unrecognized values', () => {
      expect(getIcon('DONTKNOW')).toBeNull();
    });
  });
});
