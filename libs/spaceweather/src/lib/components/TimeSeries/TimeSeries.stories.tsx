/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import TimeSeries from './TimeSeries';
import {
  StoryWrapper,
  StoryWrapperLoading,
  StoryWrapperWithNetworkError,
  StoryWrapperWithOnlyTwoErrors,
} from '../../utils/storybookUtils';

export default { title: 'components/TimeSeries' };

export const TimeSeriesDummyData: React.FC = () => {
  return (
    <StoryWrapper>
      <TimeSeries />
    </StoryWrapper>
  );
};

export const TimeSeriesWithAllErrors: React.FC = () => {
  return (
    <StoryWrapperWithNetworkError>
      <TimeSeries />
    </StoryWrapperWithNetworkError>
  );
};

export const TimeSeriesWithTwoErrors: React.FC = () => {
  return (
    <StoryWrapperWithOnlyTwoErrors>
      <TimeSeries />
    </StoryWrapperWithOnlyTwoErrors>
  );
};

export const TimeSeriesLoading: React.FC = () => {
  return (
    <StoryWrapperLoading>
      <TimeSeries />
    </StoryWrapperLoading>
  );
};
