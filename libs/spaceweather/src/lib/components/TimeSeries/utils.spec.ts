/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * */

import { DefaultLabelFormatterCallbackParams, SeriesOption } from 'echarts';
import { dateUtils } from '@opengeoweb/shared';
import { barGraphWithSeriesFixedDates } from '../../utils/DummyData';
import {
  config,
  getCurrentTimeMarkers,
  getGrid,
  getSeries,
  getTooltip,
  getTooltipValue,
  getXAxis,
  getYAxis,
} from './utils';

describe('components/Timeseries/utils', () => {
  afterEach(() => {
    jest.clearAllMocks();
    jest.restoreAllMocks();
  });
  describe('getGrid', () => {
    it('should return the grid based on the config and graphHeight', () => {
      const result = getGrid(config, 90);
      const expectedResult = [
        {
          height: '90px',
          left: '56px',
          right: '56px',
          top: '12px',
        },
        {
          height: '90px',
          left: '56px',
          right: '56px',
          top: '126px',
        },
        {
          height: '90px',
          left: '56px',
          right: '56px',
          top: '240px',
        },
        {
          height: '90px',
          left: '56px',
          right: '56px',
          top: '354px',
        },
        {
          height: '90px',
          left: '56px',
          right: '56px',
          top: '468px',
        },
        {
          height: '90px',
          left: '56px',
          right: '56px',
          top: '582px',
        },
      ];
      expect(result).toEqual(expectedResult);
    });
  });
  describe('getCurrentTimeMarkers', () => {
    it('should return a list of current time markers based on the config', () => {
      const now = new Date(`2024-01-01T14:00:00Z`);
      jest.spyOn(global, 'Date').mockReturnValue(now);

      const result = getCurrentTimeMarkers([config[0]]);
      const expectedResult = [
        {
          animation: false,
          markLine: {
            data: [{ xAxis: now }],
            label: { show: false },
            lineStyle: {
              color: '#0075a9',
              type: 'solid',
              width: 1.5,
            },
            silent: true,
            symbol: 'none',
          },
          name: 'current_time',
          type: 'line',
          xAxisIndex: 0,
          yAxisIndex: 0,
        },
      ];
      expect(result).toEqual(expectedResult);
    });
  });
  describe('getXAxis', () => {
    it('should return a list of xAxis based on the config and start and end times', () => {
      const begin = new Date(`2024-01-02T14:00:00Z`);
      const end = new Date(`2024-01-11T14:00:00Z`);

      const result = getXAxis([config[0], config[1]], begin, end);
      const expectedResult = [
        {
          axisLabel: {
            formatter: expect.any(Function),
          },
          axisLine: {
            onZero: false,
          },
          gridIndex: 0,
          max: end,
          min: begin,
          name: 'UTC time',
          nameGap: 5,
          nameLocation: 'end',
          show: false,
          type: 'time',
        },
        {
          axisLabel: {
            formatter: expect.any(Function),
          },
          axisLine: {
            onZero: false,
          },
          gridIndex: 1,
          max: end,
          min: begin,
          name: 'UTC time',
          nameGap: 5,
          nameLocation: 'end',
          type: 'time',
        },
      ];
      expect(result).toEqual(expectedResult);
    });
  });
  describe('getYAxis', () => {
    it('should return a list of yAxis based on the config', () => {
      const result = getYAxis([config[1], config[2]]);
      const expectedResult = [
        {
          axisLabel: {
            formatter: expect.any(Function),
          },
          axisLine: {
            show: true,
          },
          axisTick: {
            show: true,
          },
          gridIndex: 0,
          max: 25,
          min: -25,
          name: 'Interplanetary Magnetic Field Bt and Bz (nT)',
          nameGap: 1,
          nameTextStyle: {
            align: 'left',
          },
          splitLine: {
            lineStyle: {
              opacity: 0,
            },
          },
          type: 'value',
        },
        {
          axisLabel: {
            formatter: expect.any(Function),
          },
          axisLine: {
            show: true,
          },
          axisTick: {
            show: true,
          },
          gridIndex: 1,
          max: 0.01,
          min: 1e-9,
          name: 'X-ray Solar Flux (W/m²)',
          nameGap: 1,
          nameTextStyle: {
            align: 'left',
          },
          splitLine: {
            lineStyle: {
              opacity: 0,
            },
          },
          type: 'log',
        },
      ];
      expect(result).toEqual(expectedResult);
    });
  });

  describe('getSeries', () => {
    it('should return no series if no data', () => {
      const result = getSeries([]);
      const expectedResult: SeriesOption[] = [];
      expect(result).toEqual(expectedResult);
    });
    it('should return series based on the config', () => {
      const result = getSeries([config[0]]);
      const expectedResult = [
        {
          data: [],
          itemStyle: {
            color: '#417505',
            opacity: 0.8,
          },
          markLine: {
            animation: false,
            data: [{ yAxis: 5 }],
            label: {
              color: 'rgba(0, 0, 0, 0.4)',
              distance: [5, 1],
              formatter: 'Met Office',
              position: 'insideStartTop',
            },
            lineStyle: {
              color: 'rgba(0, 0, 0, 0.4)',
            },
            silent: true,
            symbol: 'none',
          },
          name: 'Kp Index',
          type: 'bar',
          xAxisIndex: 0,
          yAxisIndex: 0,
        },
        {
          barGap: '-100%',
          barCategoryGap: '10%',
          data: [],
          itemStyle: {
            color: '#8DB04E',
            opacity: 0.8,
          },
          markLine: {
            animation: false,
            data: [{ yAxis: 7 }],
            label: {
              color: 'rgba(0, 0, 0, 0.4)',
              distance: [5, 1],
              formatter: 'KNMI',
              position: 'insideStartTop',
            },
            lineStyle: {
              color: 'rgba(0, 0, 0, 0.4)',
            },
            silent: true,
            symbol: 'none',
          },
          name: 'Kp Index Forecast',
          type: 'bar',
          xAxisIndex: 0,
          yAxisIndex: 0,
        },
      ];
      expect(result).toEqual(expectedResult);
    });
    it('should return series with data', () => {
      const result = getSeries([barGraphWithSeriesFixedDates]);
      const expectedResult = [
        {
          data: [
            ['2022-01-05T09:00:00Z', 1],
            ['2022-01-05T12:00:00Z', 0.66],
            ['2022-01-05T15:00:00Z', 0],
            ['2022-01-05T18:00:00Z', 1],
            ['2022-01-05T21:00:00Z', 0.33],
            ['2022-01-06T00:00:00Z', 3],
          ],
          itemStyle: {
            color: '#417505',
            opacity: 0.8,
          },
          markLine: {
            animation: false,
            data: [{ yAxis: 5 }],
            label: {
              color: 'rgba(0, 0, 0, 0.4)',
              distance: [5, 1],
              formatter: 'Met Office',
              position: 'insideStartTop',
            },
            lineStyle: {
              color: 'rgba(0, 0, 0, 0.4)',
            },
            silent: true,
            symbol: 'none',
          },
          name: 'Kp Index',
          type: 'bar',
          xAxisIndex: 0,
          yAxisIndex: 0,
        },
        {
          barGap: '-100%',
          barCategoryGap: '10%',
          data: [
            ['2022-01-06T03:00:00Z', 4],
            ['2022-01-06T06:00:00Z', 1],
            ['2022-01-06T09:00:00Z', 3],
            ['2022-01-06T12:00:00Z', 7],
            ['2022-01-06T15:00:00Z', 2],
            ['2022-01-06T18:00:00Z', 1],
            ['2022-01-06T21:00:00Z', 3],
            ['2022-01-07T00:00:00Z', 3],
          ],
          itemStyle: {
            color: '#8DB04E',
            opacity: 0.8,
          },
          markLine: {
            animation: false,
            data: [{ yAxis: 7 }],
            label: {
              color: 'rgba(0, 0, 0, 0.4)',
              distance: [5, 1],
              formatter: 'KNMI',
              position: 'insideStartTop',
            },
            lineStyle: {
              color: 'rgba(0, 0, 0, 0.4)',
            },
            silent: true,
            symbol: 'none',
          },
          name: 'Kp Index Forecast',
          type: 'bar',
          xAxisIndex: 0,
          yAxisIndex: 0,
        },
      ];
      expect(result).toEqual(expectedResult);
    });
  });

  describe('getTooltipValue', () => {
    it('should give value for Kp Index Forecast for future date', () => {
      const now = new Date(`2024-01-01T15:00:00Z`);
      const future = `2024-01-01T15:00:01Z`;
      jest.spyOn(global, 'Date').mockReturnValue(now);
      jest.spyOn(dateUtils, 'isAfter').mockReturnValue(true);
      const param = {
        seriesName: 'Kp Index Forecast',
        data: [future, 2],
      } as DefaultLabelFormatterCallbackParams;
      const result = getTooltipValue(future, param);
      expect(result).toEqual('2');
    });
    it('should not give value for other param for future date', () => {
      const now = new Date(`2024-01-01T15:00:00Z`);
      const future = `2024-01-01T15:00:01Z`;
      jest.spyOn(global, 'Date').mockReturnValue(now);
      jest.spyOn(dateUtils, 'isAfter').mockReturnValue(true);
      const param = {
        seriesName: 'Kp Index',
        data: [future, 2],
      } as DefaultLabelFormatterCallbackParams;
      const result = getTooltipValue(future, param);
      expect(result).toEqual('-');
    });
    it('should give value for other param for past date', () => {
      const now = new Date(`2024-01-01T15:00:00Z`);
      const past = `2024-01-01T11:00:01Z`;
      jest.spyOn(global, 'Date').mockReturnValue(now);
      jest.spyOn(dateUtils, 'isAfter').mockReturnValue(false);
      const param = {
        seriesName: 'Kp Index',
        data: [past, 3],
      } as DefaultLabelFormatterCallbackParams;
      const result = getTooltipValue(past, param);
      expect(result).toEqual('3');
    });
    it('should reduce value for xray', () => {
      const now = new Date(`2024-01-01T15:00:00Z`);
      const past = `2024-01-01T11:00:01Z`;
      jest.spyOn(global, 'Date').mockReturnValue(now);
      const param = {
        seriesName: 'X-ray Solar Flux (W/m\u00B2)',
        data: [past, 0.0000000337648389478389],
      } as DefaultLabelFormatterCallbackParams;
      const result = getTooltipValue(past, param);
      expect(result).toEqual('3.38e-8');
    });
    it('should reduce value for solar wind density', () => {
      const now = new Date(`2024-01-01T15:00:00Z`);
      const past = `2024-01-01T11:00:01Z`;
      jest.spyOn(global, 'Date').mockReturnValue(now);
      const param = {
        seriesName: 'Solar Wind Density (1/cm\u00B3)',
        data: [past, 0.0000337648389478389],
      } as DefaultLabelFormatterCallbackParams;
      const result = getTooltipValue(past, param);
      expect(result).toEqual('0.0000338');
    });
    it('should reduce value for solar wind speed', () => {
      const now = new Date(`2024-01-01T15:00:00Z`);
      const past = `2024-01-01T11:00:01Z`;
      jest.spyOn(global, 'Date').mockReturnValue(now);
      const param = {
        seriesName: 'Solar Wind Speed (km/s)',
        data: [past, 0.0000337648389478389],
      } as DefaultLabelFormatterCallbackParams;
      const result = getTooltipValue(past, param);
      expect(result).toEqual('0.00003376');
    });
    it('should reduce value for solar wind pressure', () => {
      const now = new Date(`2024-01-01T15:00:00Z`);
      const past = `2024-01-01T11:00:01Z`;
      jest.spyOn(global, 'Date').mockReturnValue(now);
      const param = {
        seriesName: 'Solar Wind Pressure (nPa)',
        data: [past, 0.0000000337648389478389],
      } as DefaultLabelFormatterCallbackParams;
      const result = getTooltipValue(past, param);
      expect(result).toEqual('3.376e-8');
    });
    it('should handle NaN values', () => {
      const date = `2024-01-01T15:00:00Z`;
      const param = {
        seriesName: 'Kp Index',
        data: [date, NaN],
      } as DefaultLabelFormatterCallbackParams;
      const result = getTooltipValue(date, param);
      expect(result).toEqual('-');
    });

    it('should handle empty data', () => {
      const date = `2024-01-01T15:00:00Z`;
      const param = {
        seriesName: 'Kp Index',
        data: [],
      } as unknown as DefaultLabelFormatterCallbackParams;
      const result = getTooltipValue(date, param);
      expect(result).toEqual('-');
    });
  });

  describe('getTooltip', () => {
    it('should return the tooltip settings', () => {
      const result = getTooltip();
      expect(result).toEqual({
        trigger: 'axis',
        formatter: expect.any(Function),
      });
    });
  });
});
