/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { act, render, screen, waitFor } from '@testing-library/react';
import { createFakeApiInstance } from '@opengeoweb/api';
import 'jest-canvas-mock';
import TimeSeries from './TimeSeries';
import { TestWrapper } from '../../utils/testUtils';
import { SpaceWeatherApi } from '../../utils/api';
import {
  createApi as createFakeApi,
  getDummyTimeSerie,
} from '../../utils/fakeApi';
import { config } from './utils';
import { TimeseriesResponseData } from '../../types';
import * as utils from './TimeSeries.utils';

describe('components/Timeseries/TimeSeries', () => {
  it('should render a canvas element', () => {
    const { baseElement } = render(
      <TestWrapper>
        <TimeSeries />
      </TestWrapper>,
    );
    // eslint-disable-next-line testing-library/no-node-access
    const canvas = baseElement.querySelector('canvas');
    expect(canvas).toBeInstanceOf(HTMLElement);
  });

  it('should show a loading indicator while graphs are loading', async () => {
    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getTimeSeriesMultiple: () =>
        new Promise((resolve) => {
          setTimeout(() => {
            resolve([]);
          }, 1000);
        }),
    });

    render(
      <TestWrapper createApi={fakeApi}>
        <TimeSeries />
      </TestWrapper>,
    );

    await screen.findByTestId('timeline-loadingbar');
  });

  it('should fetch new streams when autoupdate is triggered and user is sleeping', async () => {
    jest.useFakeTimers();
    // make sure user is sleeping
    jest.spyOn(utils, 'useUserSleeping').mockReturnValue([true, jest.fn()]);

    const getStreams = jest.fn();

    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getStreams,
    });

    render(
      <TestWrapper createApi={fakeApi}>
        <TimeSeries />
      </TestWrapper>,
    );

    await screen.findByTestId('timeline-loadingbar');
    await waitFor(() => {
      // expect the graphs to have loaded
      expect(screen.queryByTestId('timeline-loadingbar')).toBeFalsy();
    });

    expect(getStreams).toHaveBeenCalledTimes(0);
    // jump in time to trigger autoupdate
    await act(async () => jest.advanceTimersToNextTimer());

    await waitFor(() => expect(getStreams).toHaveBeenCalledTimes(1));

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should not autoupdate when user is not sleeping', async () => {
    jest.useFakeTimers();
    // make sure user is not sleeping
    jest.spyOn(utils, 'useUserSleeping').mockReturnValue([false, jest.fn()]);

    const getStreams = jest.fn();

    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getStreams,
    });

    render(
      <TestWrapper createApi={fakeApi}>
        <TimeSeries />
      </TestWrapper>,
    );

    await screen.findByTestId('timeline-loadingbar');
    await waitFor(() => {
      // expect the graphs to have loaded
      expect(screen.queryByTestId('timeline-loadingbar')).toBeFalsy();
    });

    expect(getStreams).toHaveBeenCalledTimes(0);
    // jump in time to trigger autoupdate
    await act(async () => jest.advanceTimersToNextTimer());

    expect(getStreams).not.toHaveBeenCalled();

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should show an error for each graph', async () => {
    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getTimeSeriesMultiple: () =>
        Promise.reject(new Error('test error message')),
    });

    render(
      <TestWrapper createApi={fakeApi}>
        <TimeSeries />
      </TestWrapper>,
    );

    await waitFor(() => {
      expect(screen.getAllByRole('alert').length).toEqual(config.length);
      screen.getAllByRole('alert').forEach((alert) => {
        expect(alert.textContent).toEqual('test error message');
      });
    });
  });

  it('should load the graphs but show an error if one of multiple series in one graph fails', async () => {
    const fakeAxiosInstance = createFakeApiInstance();

    const fakeApi = (): SpaceWeatherApi => ({
      ...createFakeApi(),
      getTimeSeriesMultiple: (
        params,
      ): Promise<{ data: TimeseriesResponseData }[]> => {
        return Promise.all(
          params.map((param) => {
            if (param.stream === 'kp') {
              return Promise.reject(
                new Error('Network test error message'),
              ).catch((error) => {
                return error;
              });
            }
            return fakeAxiosInstance.get('/timeseries/data').then(() => ({
              data: getDummyTimeSerie(param.stream!, param.parameter!),
            }));
          }),
        );
      },
    });

    render(
      <TestWrapper createApi={fakeApi}>
        <TimeSeries />
      </TestWrapper>,
    );
    await screen.findByTestId('timeline-loadingbar');
    await waitFor(() => {
      // expect the graphs to have loaded
      expect(screen.queryByTestId('timeline-loadingbar')).toBeFalsy();
    });
    // But the error to be visible
    expect(screen.getByRole('alert')).toBeTruthy();
    expect(screen.getByRole('alert').textContent).toEqual(
      'Network test error message',
    );
  });
});
