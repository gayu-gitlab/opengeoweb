/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import { TimeseriesParams } from '../../types';

export type Dataset = (string | number | undefined)[][];
export type IncomingData = ChartResponseType['data'];

type GraphType = 'BAR' | 'BAND' | 'LOG' | 'AREA';

export interface GraphItem {
  id: string;
  params: TimeseriesParams[];
  graphType: GraphType;
  title: string;
  yMinValue: number;
  yMaxValue: number;
  threshold?: { title: string; value: number | string; color?: string }[];
  columns: string[] | string[][];
  tickValues?: (value: number) => number | string;
  end?: string;
  series?: Dataset[];
}

export interface ChartResponseType {
  data: {
    data: {
      source?: string;
      timestamp?: string;
      value?: number;
    }[];
    parameter?: string;
    stream?: string;
    unit?: string;
  };
}
