/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * */

import {
  createDataset,
  createSeries,
  mirrorSeries,
} from './createChartSeriesData';

const exampleIncomingData = [
  {
    timestamp: '2022-01-05T03:00:00Z',
    value: 311,
    source: 'DSCOVR',
  },
  {
    timestamp: '2022-01-05T04:00:00Z',
    value: undefined,
    source: 'DSCOVR',
  },
  {
    timestamp: '2022-01-05T05:00:00Z',
    value: 310,
    source: 'DSCOVR',
  },
];

const exampleIncomingDataResult = [
  ['2022-01-05T03:00:00Z', 311],
  ['2022-01-05T04:00:00Z', undefined],
  ['2022-01-05T05:00:00Z', 310],
];

describe('components/Timeseries/createChartSeriesData', () => {
  describe('createDataset', () => {
    it('should handle empty dataset', () => {
      const result = createDataset([]);
      expect(result).toEqual([]);
      const result1 = createDataset(undefined!);
      expect(result1).toEqual([]);
    });
    it('should create dataset', () => {
      const result = createDataset(exampleIncomingData);
      expect(result).toEqual(exampleIncomingDataResult);
    });
  });

  describe('createSeries', () => {
    it('should handle empty data', () => {
      const result = createSeries([]);
      expect(result).toEqual([]);
      const result1 = createSeries([{ data: { data: [] } }]);
      expect(result1).toEqual([[]]);
    });
    it('should create series data', () => {
      const result = createSeries([
        { data: { data: exampleIncomingData } },
        { data: { data: exampleIncomingData } },
      ]);
      expect(result).toEqual([
        exampleIncomingDataResult,
        exampleIncomingDataResult,
      ]);
    });
  });

  describe('mirrorSeries', () => {
    it('should handle empty dataset', () => {
      const result = mirrorSeries([]);
      expect(result).toEqual([]);
      const result1 = mirrorSeries(undefined!);
      expect(result1).toEqual([]);
    });
    it('should mirror dataset', () => {
      const exampleIncomingDataMirrorResult = [
        ['2022-01-05T03:00:00Z', -311],
        ['2022-01-05T04:00:00Z', undefined],
        ['2022-01-05T05:00:00Z', -310],
      ];
      const result = mirrorSeries(exampleIncomingDataResult);
      expect(result).toEqual(exampleIncomingDataMirrorResult);
    });
  });
});
