/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import { renderHook, act } from '@testing-library/react';
import {
  stateAsArray,
  createStateById,
  cancelRequests,
  createStreamDict,
  useUserSleeping,
  getGraphHeightInPx,
  useStreams,
} from './TimeSeries.utils';
import { GraphItem } from './types';
import {
  TimeseriesParams,
  StreamResponse,
  Parameters,
  Source,
  Streams,
} from '../../types';
import * as apiUtils from '../../utils/api';
import { createApi as createFakeApi } from '../../utils/fakeApi';

const apiMock = createFakeApi();

describe('src/components/TimeSeries/TimeSeries.utils', () => {
  describe('createStateById', () => {
    it('should create dict state', () => {
      const charts = [
        { id: 'chart 1', params: [] },
        { id: 'chart 2', params: [] },
      ] as unknown as GraphItem[];

      const params = [
        {
          time_stop: new Date().toUTCString(),
          time_end: new Date().toUTCString(),
        },
      ] as TimeseriesParams;

      expect(createStateById(charts, params)).toEqual({
        'chart 1': {
          isLoading: true,
          error: null,
          data: [
            {
              ...charts[0],
              series: [],
            },
          ],
        },
        'chart 2': {
          isLoading: true,
          error: null,
          data: [
            {
              ...charts[1],
              series: [],
            },
          ],
        },
      });
    });
  });

  describe('stateAsArray', () => {
    it('should create array of state object', () => {
      const charts = {
        'chart-1': {
          isLoading: false,
          data: [],
          error: null!,
        },
        'chart-2': {
          isLoading: false,
          data: [],
          error: null!,
        },
      };

      expect(stateAsArray(charts)).toEqual([
        charts['chart-1'],
        charts['chart-2'],
      ]);
    });
  });

  describe('cancelRequests', () => {
    it('should cancel requests', () => {
      const charts = [
        { params: [{ stream: 'test-s-1', parameter: 'test-p-1' }] },
        { params: [{ stream: 'test-s-2', parameter: 'test-p-2' }] },
      ] as TimeseriesParams;
      const spy = jest.spyOn(apiUtils, 'cancelRequestById');

      cancelRequests(charts as GraphItem[]);

      expect(spy).toHaveBeenCalledWith(
        apiUtils.createCancelRequestId((charts as GraphItem[])[0].params[0]),
      );
      expect(spy).toHaveBeenCalledWith(
        apiUtils.createCancelRequestId((charts as GraphItem[])[1].params[0]),
      );
    });
  });

  describe('createStreamDict', () => {
    it('should create dict object from streams', () => {
      const streams = [
        { start: '', stream: 'xray' },
        { start: '', stream: 'kp' },
      ] as StreamResponse[];

      expect(createStreamDict(streams)).toEqual({
        xray: streams[0],
        kp: streams[1],
      });
    });
  });

  describe('useUserSleeping', () => {
    it('should let user fall asleep', () => {
      jest.useFakeTimers();
      const view = renderHook(() => useUserSleeping());
      const { result, rerender } = view;

      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      expect((result as any)[0]).toBeFalsy();

      act(() => {
        jest.runAllTimers();
      });
      rerender(view);

      const [isSleeping, resetIsSleeping] = result.current;

      expect(isSleeping).toBeTruthy();

      act(() => {
        resetIsSleeping();
      });
      expect(result.current[0]).toBeFalsy();
    });
  });

  describe('getGraphHeightInPx', () => {
    it('should return 10% of the clientHeight', () => {
      jest
        .spyOn(document.documentElement, 'clientHeight', 'get')
        .mockImplementationOnce(() => 1024);

      expect(getGraphHeightInPx()).toEqual(102.4);
    });
    it('should return 90 when 10% of clientHeight is less than 90', () => {
      jest
        .spyOn(document.documentElement, 'clientHeight', 'get')
        .mockImplementationOnce(() => 899);

      expect(getGraphHeightInPx()).toEqual(90);
    });
  });

  describe('useStreams', () => {
    it('should update the stream data when end date is newer and not update the data when stream call fails', async () => {
      const firstStream = {
        stream: 'xray' as Streams,
        start: '1970-01-01T00:00:00Z',
        end: '2021-01-01T00:00:00Z',
        params: [
          { name: 'solar_x_ray_flux_long' as Parameters, unit: 'W/m2' },
          { name: 'solar_x_ray_flux_short' as Parameters, unit: 'W/m2' },
        ],
        sources: ['GOES-16' as Source],
        hasactive: false,
      };

      const secondStream = {
        stream: 'xray' as Streams,
        start: '1970-01-01T00:00:00Z',
        end: '2021-02-01T00:00:00Z',
        params: [
          { name: 'solar_x_ray_flux_long' as Parameters, unit: 'W/m2' },
          { name: 'solar_x_ray_flux_short' as Parameters, unit: 'W/m2' },
        ],
        sources: ['GOES-16' as Source],
        hasactive: false,
      };

      jest
        .spyOn(apiMock, 'getStreams')
        .mockImplementationOnce(() => {
          return new Promise((resolve) => {
            resolve({
              data: [firstStream], // first call response
            });
          });
        })
        .mockImplementationOnce(() => {
          return new Promise((resolve) => {
            resolve({
              data: [secondStream], // second call response
            });
          });
        })
        .mockImplementationOnce(() => {
          return new Promise((resolve, reject) => {
            reject(new Error('Mock error')); // third call response
          });
        });

      const { result } = renderHook(() => useStreams(apiMock));

      // eslint-disable-next-line no-unused-vars, @typescript-eslint/no-unused-vars
      const [loadedStreams, fetchStreams] = result.current;

      // first call
      await act(async () => {
        fetchStreams();
      });
      // expecting null because there is no previous call
      expect(result.current).toEqual([null, expect.any(Function)]);

      // second call
      await act(async () => {
        fetchStreams();
      });
      // expecting the updated streams to be the second stream
      expect(result.current).toEqual([
        createStreamDict([secondStream]),
        expect.any(Function),
      ]);

      // third call
      await act(async () => {
        fetchStreams();
      });
      // still expecting the second stream because the third call failed, so no new data
      expect(result.current).toEqual([
        createStreamDict([secondStream]),
        expect.any(Function),
      ]);
    });
  });
});
