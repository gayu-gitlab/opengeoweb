/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { act, render, renderHook, screen } from '@testing-library/react';
import { createStore } from '@redux-eggs/redux-toolkit';
import { MosaicNode } from 'react-mosaic-component';
import WorkspaceView, {
  WorkspaceViewProps,
  useControlActiveWindowId,
} from './WorkspaceView';
import {
  WorkspaceComponentLookupPayload,
  WorkspaceViewType,
} from '../../store/workspace/types';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { initialState, workspaceActions } from '../../store/workspace/reducer';
import { MAPPRESET_ACTIVE_TITLE } from '../../store/viewPresets/utils';

const componentsLookUp = (
  payload: WorkspaceComponentLookupPayload,
): React.ReactElement => {
  const { componentType, initialProps: props, id } = payload;
  switch (componentType) {
    case 'MyTestComponent':
      return (
        <div data-testid="MyTestComponentTestId">
          {id}
          {JSON.stringify(props)}
        </div>
      );
    default:
      return null!;
  }
};

describe('components/WorkspaceView/WorkspaceView', () => {
  const screenProps: WorkspaceViewType = {
    title: 'my screen',
    componentType: 'MyTestComponent',
    initialProps: { mapPreset: [{}], syncGroupsIds: [] },
  };
  const props: WorkspaceViewProps = {
    screenConfig: {
      id: 'preset1',
      title: 'Preset 1',
      views: {
        allIds: ['screen1'],
        byId: {
          screen1: screenProps,
        },
      },
      mosaicNode: 'screen1',
    },
    componentsLookUp,
    updateViews: jest.fn(),
    activeWindowId: 'activeWindow',
    setActiveWindowId: jest.fn(),
  };
  it('should return a view with correct title and panel id', async () => {
    const store = createStore();

    const { baseElement } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceView {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: props.screenConfig }),
      ),
    );
    expect(
      // eslint-disable-next-line testing-library/no-node-access
      baseElement.querySelector('.mosaic-window-title')!.textContent,
    ).toEqual(`${screenProps.title}${props.screenConfig.views.allIds[0]}`);
  });
  it('should return a view with new preset title and id when no title available', async () => {
    const mockProps = {
      ...props,
      screenConfig: {
        id: 'preset1',
        title: 'Preset 1',
        views: {
          allIds: ['screen1'],
          byId: {
            screen1: {
              componentType: 'MyTestComponent',
              initialProps: { mapPreset: [{}], syncGroupsIds: [] },
            },
          },
        },
        mosaicNode: 'screen1',
      },
    };
    const store = createStore();

    const { baseElement } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceView {...mockProps} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: mockProps.screenConfig }),
      ),
    );
    expect(
      // eslint-disable-next-line testing-library/no-node-access
      baseElement.querySelector('.mosaic-window-title')!.textContent,
    ).toEqual(`${MAPPRESET_ACTIVE_TITLE}${props.screenConfig.views.allIds[0]}`);
  });
  it('should show message when no views exist', async () => {
    const propsWithoutViews: WorkspaceViewProps = {
      ...props,
      screenConfig: initialState,
    };
    const store = createStore();

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceView {...propsWithoutViews} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: propsWithoutViews.screenConfig,
        }),
      ),
    );
    expect(
      screen.getByText(
        'Welcome to GeoWeb. Please use one of the presets to open up new views.',
      ),
    ).toBeTruthy();
  });

  it('should not show message when no views exist but workspace is loading', async () => {
    const propsWithoutViews: WorkspaceViewProps = {
      ...props,
      screenConfig: initialState,
      isWorkspaceLoading: true,
    };
    const store = createStore();

    const { baseElement } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceView {...propsWithoutViews} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: propsWithoutViews.screenConfig,
        }),
      ),
    );
    // eslint-disable-next-line testing-library/no-node-access
    expect(baseElement.querySelector('.geoweb-mosaic')).toBeTruthy();
    expect(
      screen.queryByText(
        'Welcome to GeoWeb. Please use one of the presets to open up new views.',
      ),
    ).toBeFalsy();
  });

  it('should not fail when id from mosaicNode does not exist yet in the views', async () => {
    const propsWithoutViewId: WorkspaceViewProps = {
      ...props,
      screenConfig: {
        id: 'preset1',
        title: 'Preset 1',
        views: {
          allIds: [],
          byId: {},
        },
        mosaicNode: 'screen1',
      },
    };
    const store = createStore();

    const { baseElement } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceView {...propsWithoutViewId} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: propsWithoutViewId.screenConfig,
        }),
      ),
    );
    expect(
      // eslint-disable-next-line testing-library/no-node-access
      baseElement.querySelector('.mosaic-window-title')!.textContent,
    ).toEqual(
      `${MAPPRESET_ACTIVE_TITLE}${propsWithoutViewId.screenConfig.mosaicNode}`,
    );
  });

  describe('useControlActiveWindowId', () => {
    it('should set initial active window when screen preset changes', () => {
      let id = '';
      let mosaicNode: MosaicNode<string>;
      const activeWindowId = undefined;
      const setActiveWindowId = jest.fn();

      const { rerender } = renderHook(() =>
        useControlActiveWindowId(
          {
            id,
            mosaicNode,
            title: 'title',
            views: { allIds: [], byId: {} },
          },
          activeWindowId,
          setActiveWindowId,
        ),
      );
      expect(setActiveWindowId).not.toBeCalled();

      id = 'id1';
      mosaicNode = 'mosaicNode';
      rerender();
      expect(setActiveWindowId).toBeCalledTimes(1);
      expect(setActiveWindowId).toBeCalledWith(mosaicNode);

      id = 'id2';
      mosaicNode = { direction: 'column', first: 'first', second: 'second' };
      rerender();
      expect(setActiveWindowId).toBeCalledTimes(2);
      expect(setActiveWindowId).toBeCalledWith(mosaicNode.first);

      id = 'id3';
      mosaicNode = {
        direction: 'column',
        first: {
          direction: 'column',
          first: 'firstFirst',
          second: 'firstSecond',
        },
        second: 'second',
      };
      rerender();
      expect(setActiveWindowId).toBeCalledTimes(3);
      expect(setActiveWindowId).toBeCalledWith('firstFirst');
    });

    it('should set new active window if current active window is not in screen config', () => {
      const activeWindowId = 'windowNotInViewsList';
      const setActiveWindowId = jest.fn();
      const viewId = 'viewId';

      renderHook(() =>
        useControlActiveWindowId(
          {
            id: '',
            mosaicNode: {
              direction: 'column',
              first: 'first',
              second: 'second',
            },
            title: 'title',
            views: { allIds: [viewId], byId: {} },
          },
          activeWindowId,
          setActiveWindowId,
        ),
      );
      expect(setActiveWindowId).toBeCalledTimes(1);
      expect(setActiveWindowId).toBeCalledWith(viewId);
    });
  });
});
