/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import {
  defaultFormOptions,
  ReactHookFormProvider,
} from '@opengeoweb/form-fields';

import MapPresetsForm from './MapPresetsForm';
import { PresetAction } from '../../store/viewPresets/types';
import { emptyMapViewPreset } from '../../store/viewPresets/utils';

describe('components/MapPresetsDialog/MapPresetsForm', () => {
  it('should work with default props', () => {
    const props = {
      formValues: {
        ...emptyMapViewPreset,
        title: 'test title',
      },
      action: PresetAction.SAVE_AS,
    };

    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <MapPresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    const input = screen.getByRole('textbox');

    expect(input.matches(':focus')).toBeTruthy();
    expect(input!.getAttribute('value')).toEqual(props.formValues.title);
    expect(screen.getByTestId('initialProps').getAttribute('value')).toEqual(
      '[object Object]',
    );
    expect(screen.getByTestId('scope').getAttribute('value')).toEqual('user');
    expect(screen.getByTestId('componentType').getAttribute('value')).toEqual(
      'Map',
    );
    expect(screen.getByTestId('keywords').getAttribute('value')).toEqual('');
  });

  it('should validate required', async () => {
    const props = {
      formValues: {
        title: 'test title',
      },
      action: PresetAction.SAVE_AS,
    };

    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <MapPresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    const input = screen.getByRole('textbox');

    expect(input.matches(':focus')).toBeTruthy();
    expect(input!.getAttribute('value')).toEqual(props.formValues.title);
    expect(screen.queryByRole('alert')).toBeFalsy();

    fireEvent.change(input!, { target: { value: '' } });
    await waitFor(() => {
      expect(screen.getByRole('alert')).toBeTruthy();
    });

    fireEvent.change(input!, { target: { value: 'new value' } });
    await waitFor(() => {
      expect(screen.queryByRole('alert')).toBeFalsy();
    });
  });

  it('should show error when title only contains spaces', async () => {
    const props = {
      formValues: {
        title: '',
      },
      action: PresetAction.SAVE_AS,
    };

    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <MapPresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    const input = screen.getByRole('textbox');
    fireEvent.change(input!, { target: { value: '  ' } });
    await waitFor(() => {
      expect(screen.getByRole('alert')).toBeTruthy();
    });
  });

  it('should show error', async () => {
    const errorMessage = 'test could not save preset';
    const props = {
      formValues: {
        title: 'test title',
      },
      error: errorMessage,
      action: PresetAction.SAVE_AS,
    };

    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <MapPresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    expect(await screen.findByText(errorMessage)).toBeTruthy();
  });

  it('should submit the form when submitting the title field', async () => {
    const props = {
      formValues: {
        title: 'test submit on enter',
      },
      onSubmit: jest.fn(),
      action: PresetAction.SAVE_AS,
    };

    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <MapPresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    const input = screen.getByRole('textbox');
    expect(input).toBeTruthy();
    fireEvent.submit(input!);
    await waitFor(() => {
      expect(props.onSubmit).toHaveBeenCalled();
    });
  });

  it('should show form for edit action', async () => {
    const props = {
      formValues: {
        title: 'test submit on enter',
      },
      onSubmit: jest.fn(),
      action: PresetAction.EDIT,
    };

    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <MapPresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    const input = screen.getByRole('textbox');
    expect(input).toBeTruthy();
  });
});
