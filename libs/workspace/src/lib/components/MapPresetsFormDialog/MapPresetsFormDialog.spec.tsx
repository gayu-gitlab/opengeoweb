/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  fireEvent,
  render,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import MapPresetsFormDialog from './MapPresetsFormDialog';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { WorkspaceWrapperProviderWithStore } from '../Providers';
import { PresetsApi } from '../../utils/api';
import { PresetAction, ViewPreset } from '../../store/viewPresets/types';
import {
  MAPPRESET_DIALOG_TITLE_SAVE_AS,
  MAPPRESET_DIALOG_TITLE_DELETE,
  emptyMapViewPreset,
  MAPPRESET_DIALOG_TITLE_EDIT,
} from '../../store/viewPresets/utils';

describe('components/MapPresetsDialog/MapPresetsFormDialog', () => {
  it('should cancel save as', async () => {
    const props = {
      isOpen: true,
      title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
      action: PresetAction.SAVE_AS,
      viewPresetId: 'Map',
      formValues: {
        title: 'Map preset',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    render(
      <WorkspaceWrapperProviderWithStore>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByText(props.title)).toBeTruthy();
    expect(await screen.findByTestId('map-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('map-preset-form')).toBeTruthy();

    // should cancel
    fireEvent.click(screen.getByTestId('confirmationDialog-cancel'));
    expect(props.onClose).toHaveBeenCalledTimes(1);
    expect(props.onSuccess).not.toHaveBeenCalled();
  });

  it('should close save as', async () => {
    const props = {
      isOpen: true,
      title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
      action: PresetAction.SAVE_AS,
      viewPresetId: 'Map',
      formValues: {
        title: 'Map preset',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    render(
      <WorkspaceWrapperProviderWithStore>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByText(props.title)).toBeTruthy();
    expect(await screen.findByTestId('map-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('map-preset-form')).toBeTruthy();

    // should close
    fireEvent.click(screen.getByTestId('customDialog-close'));
    expect(props.onClose).toHaveBeenCalledTimes(1);
    expect(props.onSuccess).not.toHaveBeenCalled();
  });

  it('should save a preset as', async () => {
    const props = {
      isOpen: true,
      title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
      action: PresetAction.SAVE_AS,
      viewPresetId: 'Map',
      formValues: {
        title: '  Map preset  ',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    const newTestId = 'new-test-id';

    const saveViewPresetAsWatcher = jest.fn(
      () =>
        new Promise((resolve) => {
          resolve(newTestId);
        }),
    );

    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        saveViewPresetAs:
          saveViewPresetAsWatcher as unknown as PresetsApi['saveViewPresetAs'],
      };
    };

    render(
      <WorkspaceWrapperProviderWithStore createApi={createApi}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByText(props.title)).toBeTruthy();
    expect(await screen.findByTestId('map-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('map-preset-form')).toBeTruthy();

    // should post
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    await waitFor(() => {
      expect(saveViewPresetAsWatcher).toHaveBeenCalledWith({
        ...emptyMapViewPreset,
        title: props.formValues.title.trim(),
        scope: 'user',
      });
    });

    await waitFor(() => {
      expect(props.onClose).toHaveBeenCalledTimes(1);
    });
    expect(props.onSuccess).toHaveBeenCalledTimes(1);
    expect(props.onSuccess).toHaveBeenCalledWith(
      PresetAction.SAVE_AS,
      newTestId,
      props.formValues.title.trim(),
    );
  });

  it('should not post empty title', async () => {
    const props = {
      isOpen: true,
      title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
      action: PresetAction.SAVE_AS,
      viewPresetId: 'Map',
      formValues: {
        title: '',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    const saveViewPresetAsWatcher = jest.fn();

    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        saveViewPresetAs: saveViewPresetAsWatcher,
      };
    };

    render(
      <WorkspaceWrapperProviderWithStore createApi={createApi}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByText(props.title)).toBeTruthy();
    const dialog = await screen.findByTestId('map-preset-dialog');
    expect(screen.getByTestId('map-preset-form')).toBeTruthy();
    expect(dialog).toBeTruthy();

    // try to post
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));
    await waitFor(() => {
      expect(screen.getByRole('alert')).toBeTruthy();
    });

    expect(saveViewPresetAsWatcher).not.toHaveBeenCalled();
    expect(props.onClose).toHaveBeenCalledTimes(0);
    expect(await screen.findByText('This field is required')).toBeTruthy();

    fireEvent.change(
      within(dialog).getByRole('textbox', { name: /Map preset name/i }),
      {
        target: {
          value: 'new dialog title',
        },
      },
    );

    await waitFor(() => {
      expect(screen.queryByRole('alert')).toBeFalsy();
    });

    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    await waitFor(() => {
      expect(saveViewPresetAsWatcher).toHaveBeenCalled();
    });

    await waitFor(() => {
      expect(props.onClose).toHaveBeenCalledTimes(1);
    });
    expect(props.onSuccess).toHaveBeenCalledTimes(1);
  });

  it('should show error', async () => {
    const props = {
      isOpen: true,
      title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
      action: PresetAction.SAVE_AS,
      viewPresetId: 'Map',
      formValues: {
        title: 'Map preset',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    const error = new Error('test error');

    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        saveViewPresetAs: (): Promise<string> => {
          return new Promise((resolve, reject) => {
            reject(error);
          });
        },
      };
    };

    render(
      <WorkspaceWrapperProviderWithStore createApi={createApi}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByText(props.title)).toBeTruthy();
    expect(await screen.findByTestId('map-preset-dialog')).toBeTruthy();
    expect(await screen.findByTestId('map-preset-form')).toBeTruthy();

    expect(screen.queryByTestId('alert-banner')).toBeFalsy();

    // should not post
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    await waitFor(async () => {
      expect(screen.getByTestId('alert-banner')).toBeTruthy();
    });
    expect(await screen.findByText(error.message)).toBeTruthy();
    expect(props.onClose).toHaveBeenCalledTimes(0);
  });

  it('should delete a preset', async () => {
    const props = {
      isOpen: true,
      title: MAPPRESET_DIALOG_TITLE_DELETE,
      action: PresetAction.DELETE,
      viewPresetId: 'Map',
      formValues: {
        title: '  Map preset  ',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    const mockDeletePreset = jest.fn();

    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        deleteViewPreset: mockDeletePreset,
      };
    };

    render(
      <WorkspaceWrapperProviderWithStore createApi={createApi}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByText(props.title)).toBeTruthy();
    expect(await screen.findByTestId('map-preset-dialog')).toBeTruthy();

    // should delete
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    await waitFor(() => {
      expect(mockDeletePreset).toHaveBeenCalledWith(props.viewPresetId);
    });

    await waitFor(() => {
      expect(props.onClose).toHaveBeenCalledTimes(1);
    });
    expect(props.onSuccess).toHaveBeenCalledTimes(1);
  });

  it('should show correct data when reopening', async () => {
    const props = {
      isOpen: true,
      title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
      action: PresetAction.SAVE_AS,
      viewPresetId: 'Map',
      formValues: {
        title: 'test title 1',
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };

    const { rerender } = render(
      <WorkspaceWrapperProviderWithStore>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByText(props.title)).toBeTruthy();

    expect(await screen.findByTestId('map-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('map-preset-form')).toBeTruthy();
    expect(
      screen
        .getByRole('textbox', { name: /Map preset name/i })
        .getAttribute('value'),
    ).toEqual(props.formValues.title);

    // should cancel
    fireEvent.click(screen.getByTestId('confirmationDialog-cancel'));
    expect(props.onClose).toHaveBeenCalledTimes(1);
    expect(props.onSuccess).not.toHaveBeenCalled();

    const newProps = { ...props, formValues: { title: 'test title 2' } };
    rerender(
      <WorkspaceWrapperProviderWithStore>
        <MapPresetsFormDialog {...newProps} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByTestId('map-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('map-preset-form')).toBeTruthy();
    expect(
      screen
        .getByRole('textbox', { name: /Map preset name/i })
        .getAttribute('value'),
    ).toEqual(newProps.formValues.title);
  });

  it('should edit a preset as', async () => {
    const props = {
      isOpen: true,
      title: MAPPRESET_DIALOG_TITLE_EDIT,
      action: PresetAction.EDIT,
      viewPresetId: 'Map1',
      formValues: {
        title: '  Map preset  ',
        initialProps: {
          mapPreset: {
            proj: {
              srs: 'EPSG:3857',
              bbox: {
                top: 12546338.93397678,
                left: -12546338.296985108,
                right: 15857562.273790747,
                bottom: 9211519.7737589478,
              },
            },
          },
        },
      },
      onClose: jest.fn(),
      onSuccess: jest.fn(),
    };
    const saveViewPresetWatcher = jest.fn(
      () =>
        new Promise<void>((resolve) => {
          resolve();
        }),
    );

    const oldPreset: ViewPreset = {
      ...emptyMapViewPreset,
      title: 'SomeOldTitle',
      scope: 'user',
      id: 'Map1',
      initialProps: {
        mapPreset: {
          proj: {
            srs: 'EPSG:3857',
            bbox: {
              top: 9211519.93397678,
              left: -12546338.296985108,
              right: 15857562.273790747,
              bottom: 1827728.7737589478,
            },
          },
        },
      },
    };
    const getViewPresetWatcher = jest.fn(
      () =>
        new Promise<{ data: ViewPreset }>((resolve) => {
          resolve({
            data: oldPreset,
          });
        }),
    );

    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        saveViewPreset:
          saveViewPresetWatcher as unknown as PresetsApi['saveViewPreset'],
        getViewPreset:
          getViewPresetWatcher as unknown as PresetsApi['getViewPreset'],
      };
    };

    render(
      <WorkspaceWrapperProviderWithStore createApi={createApi}>
        <MapPresetsFormDialog {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(await screen.findByText(props.title)).toBeTruthy();
    expect(await screen.findByTestId('map-preset-dialog')).toBeTruthy();
    expect(screen.getByTestId('map-preset-form')).toBeTruthy();

    // should post
    fireEvent.click(screen.getByTestId('confirmationDialog-confirm'));

    // New preset, bbox has changed to ensure checking that other properties besides the title are not updated
    await waitFor(() => {
      expect(saveViewPresetWatcher).toHaveBeenCalledWith(props.viewPresetId, {
        ...oldPreset,
        title: props.formValues.title.trim(),
      });
    });

    await waitFor(() => {
      expect(props.onClose).toHaveBeenCalledTimes(1);
    });
    expect(props.onSuccess).toHaveBeenCalledTimes(1);
    expect(props.onSuccess).toHaveBeenCalledWith(
      PresetAction.EDIT,
      props.viewPresetId,
      props.formValues.title.trim(),
    );
  });
});
