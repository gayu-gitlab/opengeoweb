/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import Box from '@mui/material/Box';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import React from 'react';
import { store } from '../../store';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import WorkspacePresetsFormDialog from './WorkspacePresetsFormDialog';
import {
  WorkspacePreset,
  WorkspacePresetAction,
} from '../../store/workspace/types';

export default {
  title: 'components/Workspace/WorkspaceFormDialog',
};

const emptyHandler = (): void => {};

const screenConfig: WorkspacePreset = {
  id: 'preset1',
  title: 'Radar with animating observations',
  views: {
    allIds: ['screen1', 'screen2'],
    byId: {
      screen1: {
        title: 'screen 1',
        componentType: 'MyTestComponent',
        initialProps: { mapPreset: [{}], syncGroupsIds: [] },
      },
      screen2: {
        title: 'screen 2',
        componentType: 'MyTestComponent',
        initialProps: { mapPreset: [{}], syncGroupsIds: [] },
      },
    },
  },
  abstract: 'This is some abstract on radar and animating observations',
  mosaicNode: {
    direction: 'row',
    first: 'screen1',
    second: 'screen2',
  },
};

export const FormDialogLightSaveAs = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.SAVE_AS}
          presetId="preset1"
          formValues={screenConfig}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

export const FormDialogDarkSaveAs = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore store={store} theme={darkTheme}>
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.SAVE_AS}
          presetId="preset1"
          formValues={screenConfig}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

export const FormDialogDarkDelete = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore store={store} theme={darkTheme}>
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.DELETE}
          presetId="preset1"
          formValues={screenConfig}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

export const FormDialogLightDelete = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.DELETE}
          presetId="preset1"
          formValues={screenConfig}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

export const FormDialogLightDuplicate = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.DUPLICATE}
          presetId="preset1"
          formValues={screenConfig}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

export const FormDialogDarkDuplicate = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore store={store} theme={darkTheme}>
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.DUPLICATE}
          presetId="preset1"
          formValues={screenConfig}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

export const FormDialogSaveAllLight = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore store={store}>
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.SAVE_ALL}
          presetId="preset1"
          formValues={screenConfig}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

export const FormDialogSaveAllDark = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore store={store} theme={darkTheme}>
      <Box sx={{ padding: 1, height: 2000, width: 2000 }}>
        <WorkspacePresetsFormDialog
          action={WorkspacePresetAction.SAVE_ALL}
          presetId="preset1"
          formValues={screenConfig}
          isOpen
          onFormSubmit={emptyHandler}
          onClose={emptyHandler}
          isLoading={false}
        />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};

FormDialogLightSaveAs.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/63f39dae0ef9b20f4bf4950d',
    },
  ],
};
FormDialogLightSaveAs.storyName = 'FormDialogSaveAsLight (takeSnapshot)';

FormDialogDarkSaveAs.storyName = 'FormDialogSaveAsDark (takeSnapshot)';

FormDialogLightDelete.storyName = 'FormDialogDeleteLight (takeSnapshot)';

FormDialogDarkDelete.storyName = 'FormDialogDeleteDark (takeSnapshot)';
