/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import {
  defaultFormOptions,
  ReactHookFormProvider,
} from '@opengeoweb/form-fields';

import WorkspacePresetsForm, {
  emptyWorkspacePreset,
} from './WorkspacePresetsForm';
import { WorkspacePresetAction } from '../../store/workspace/types';
import { WorkspacePresetFormValues } from '../../store/workspaceList/types';

describe('components/WorkspacePresetsDialog/WorkspacePresetsForm', () => {
  it('should work with default props', () => {
    const props = {
      formValues: {
        ...emptyWorkspacePreset,
        title: 'test title',
        abstract: 'this abstract',
      },
      action: WorkspacePresetAction.DUPLICATE,
    };

    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <WorkspacePresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    const input = screen.getAllByRole('textbox')[0];

    expect(input.matches(':focus')).toBeTruthy();
    expect(input!.getAttribute('value')).toEqual(props.formValues.title);
    expect(screen.getAllByRole('textbox')[1]!.textContent).toEqual(
      props.formValues.abstract,
    );
    expect(screen.getByTestId('scope').getAttribute('value')).toEqual('user');

    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();
  });

  it('should not show form for save as action', () => {
    const props = {
      formValues: {
        ...emptyWorkspacePreset,
        title: 'test title',
        abstract: 'this abstract',
      },
      action: WorkspacePresetAction.SAVE_AS,
    };

    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <WorkspacePresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    expect(screen.getByTestId('workspace-preset-form')).toBeTruthy();
  });

  it('should not show form for delete action', () => {
    const props = {
      formValues: {
        ...emptyWorkspacePreset,
        title: 'test title',
        abstract: 'this abstract',
      },
      action: WorkspacePresetAction.DELETE,
    };

    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <WorkspacePresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    expect(screen.queryByTestId('workspace-preset-form')).toBeFalsy();
  });

  it('should validate required', async () => {
    const props = {
      formValues: {
        title: 'test title',
      },
      action: WorkspacePresetAction.DUPLICATE,
    };

    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <WorkspacePresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    const input = screen.getAllByRole('textbox')[0];

    expect(input.matches(':focus')).toBeTruthy();
    expect(input!.getAttribute('value')).toEqual(props.formValues.title);
    expect(screen.queryByRole('alert')).toBeFalsy();

    fireEvent.change(input!, { target: { value: '' } });
    await waitFor(() => {
      expect(screen.getByRole('alert')).toBeTruthy();
    });

    fireEvent.change(input!, { target: { value: 'new value' } });
    await waitFor(() => {
      expect(screen.queryByRole('alert')).toBeFalsy();
    });
  });

  it('should show error when title only contains spaces', async () => {
    const props = {
      formValues: {
        title: '',
      },
      action: WorkspacePresetAction.DUPLICATE,
    };

    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <WorkspacePresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    const input = screen.getAllByRole('textbox')[0];
    fireEvent.change(input!, { target: { value: '  ' } });
    await waitFor(() => {
      expect(screen.getByRole('alert')).toBeTruthy();
    });
  });

  it('should show error', async () => {
    const errorMessage = 'test could not save preset';
    const props = {
      formValues: {
        title: 'test title',
      },
      error: errorMessage,
      action: WorkspacePresetAction.DUPLICATE,
    };

    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <WorkspacePresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    expect(await screen.findByText(errorMessage)).toBeTruthy();
  });

  it('should submit the form when submitting the title field', async () => {
    const props = {
      formValues: {
        title: 'test submit on enter',
      },
      onSubmit: jest.fn(),
      action: WorkspacePresetAction.DUPLICATE,
    };

    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <WorkspacePresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    const input = screen.getAllByRole('textbox')[0];
    expect(input).toBeTruthy();
    fireEvent.submit(input!);
    await waitFor(() => {
      expect(props.onSubmit).toHaveBeenCalled();
    });
  });

  it('should show input fields for Save (incl. viewpresets)', async () => {
    const props = {
      formValues: {
        title: 'test submit on enter',
      },
      onSubmit: jest.fn(),
      action: WorkspacePresetAction.SAVE_ALL,
    };

    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <WorkspacePresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    const nameInput = screen.getByRole('textbox', { name: 'Name' });
    const abstractInput = screen.getByRole('textbox', { name: 'Abstract' });

    expect(nameInput).toBeTruthy();
    expect(nameInput.getAttribute('disabled')).toBeNull();

    expect(abstractInput).toBeTruthy();
    expect(abstractInput.getAttribute('disabled')).toBeNull();
  });

  it('should show input fields as disabled for Save (incl. viewpresets) for existing user workspace', async () => {
    const props = {
      formValues: {
        title: 'test submit on enter',
        id: 'workspace-presetid',
      },
      onSubmit: jest.fn(),
      action: WorkspacePresetAction.SAVE_ALL,
    };

    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <WorkspacePresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    const nameInput = screen.getByRole('textbox', { name: 'Name' });
    const abstractInput = screen.getByRole('textbox', { name: 'Abstract' });

    expect(nameInput).toBeTruthy();
    expect(nameInput.getAttribute('disabled')).toBeDefined();
    expect(abstractInput).toBeTruthy();
    expect(abstractInput.getAttribute('disabled')).toBeDefined();
  });

  it('should not show input fields as disabled for Save (incl. viewpresets) for existing system workspace', async () => {
    const props = {
      formValues: {
        title: 'test submit on enter',
        id: 'workspace-presetid',
        scope: 'system',
      } as WorkspacePresetFormValues,
      onSubmit: jest.fn(),
      action: WorkspacePresetAction.SAVE_ALL,
    };

    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <WorkspacePresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    const nameInput = screen.getByRole('textbox', { name: 'Name' });
    const abstractInput = screen.getByRole('textbox', { name: 'Abstract' });

    expect(nameInput).toBeTruthy();
    expect(nameInput.getAttribute('disabled')).toBeNull();
    expect(abstractInput).toBeTruthy();
    expect(abstractInput.getAttribute('disabled')).toBeNull();
  });

  it('should show input fields for Edit (title and abstract)', async () => {
    const props = {
      formValues: {
        title: 'edited title',
      },
      onSubmit: jest.fn(),
      action: WorkspacePresetAction.EDIT,
    };

    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <WorkspacePresetsForm {...props} />
      </ReactHookFormProvider>,
    );

    const nameInput = screen.getByRole('textbox', { name: 'Name' });
    const abstractInput = screen.getByRole('textbox', { name: 'Abstract' });

    expect(nameInput).toBeTruthy();
    expect(nameInput.getAttribute('disabled')).toBeNull();

    expect(abstractInput).toBeTruthy();
    expect(abstractInput.getAttribute('disabled')).toBeNull();
  });
});
