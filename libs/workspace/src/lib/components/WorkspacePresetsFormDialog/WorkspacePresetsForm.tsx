/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { Box, Typography } from '@mui/material';
import {
  ReactHookFormHiddenInput,
  ReactHookFormTextField,
} from '@opengeoweb/form-fields';
import { AlertBanner } from '@opengeoweb/shared';
import React from 'react';
import {
  PresetScope,
  WorkspacePresetAction,
} from '../../store/workspace/types';
import { WorkspacePresetFormValues } from '../../store/workspaceList/types';

const validateTitle = (value: string): boolean | string => {
  if (!value) {
    return true;
  }
  return !value.trim().length ? 'This field is required' : true;
};

export const emptyWorkspacePreset = {
  scope: 'user' as PresetScope,
  title: '',
  abstract: '',
};

interface WorkspacePresetsFormProps {
  error?: string;
  onSubmit?: () => void;
  action: WorkspacePresetAction;
  formValues: WorkspacePresetFormValues;
}

const WorkspacePresetsForm: React.FC<WorkspacePresetsFormProps> = ({
  error,
  onSubmit,
  action,
  formValues,
}: WorkspacePresetsFormProps) => {
  const titleField = 'title';
  const { title, abstract, ...otherFormValues } = {
    ...emptyWorkspacePreset,
    ...formValues,
  };

  const hiddenFields = Object.keys(otherFormValues).map((name) => ({
    name,
    value: otherFormValues[name as keyof typeof otherFormValues],
  }));

  React.useEffect(() => {
    // autoFocus
    const field = document.querySelector(
      `[name="${titleField}"]`,
    ) as HTMLInputElement;
    field?.focus();
  }, []);

  const onSubmitForm = (event: React.FormEvent<HTMLFormElement>): void => {
    event.preventDefault();
    onSubmit!();
  };

  const hasFormInput =
    action === WorkspacePresetAction.DUPLICATE ||
    action === WorkspacePresetAction.SAVE_AS ||
    action === WorkspacePresetAction.SAVE_ALL ||
    action === WorkspacePresetAction.EDIT;

  // disable form inputs for 'Save (incl. viewpresets)' existing workspace preset
  const isDisabled =
    action === WorkspacePresetAction.SAVE_ALL &&
    formValues.id !== undefined &&
    formValues.id !== '' &&
    formValues.scope === 'user';

  return (
    <>
      {Boolean(error) && (
        <Box sx={{ marginBottom: 2 }}>
          <AlertBanner title={error!} />
        </Box>
      )}
      {hasFormInput && (
        <form data-testid="workspace-preset-form" onSubmit={onSubmitForm}>
          <Box sx={{ marginBottom: 2 }}>
            <ReactHookFormTextField
              name="title"
              label="Name"
              rules={{
                required: true,
                validate: {
                  validateTitle,
                },
              }}
              disabled={isDisabled}
              defaultValue={title}
            />
          </Box>
          <ReactHookFormTextField
            name="abstract"
            label="Abstract"
            rules={{
              required: false,
            }}
            multiline
            rows={3}
            disabled={isDisabled}
            defaultValue={abstract}
          />
          {hiddenFields.map(({ name, value }) => (
            <ReactHookFormHiddenInput
              key={name}
              name={name}
              defaultValue={value}
              data-testid={name}
            />
          ))}
        </form>
      )}
      {action === WorkspacePresetAction.DELETE && (
        <>
          <Typography variant="body1">
            {`Are you sure to want to delete the "${title}" workspace preset?`}
          </Typography>
          <ReactHookFormHiddenInput
            key="title"
            name="title"
            defaultValue={title}
          />
        </>
      )}
    </>
  );
};

export default WorkspacePresetsForm;
