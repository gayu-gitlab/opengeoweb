/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import FilterChip from './FilterChip';

describe('src/lib/components/WorkspaceFilter/FilterChip', () => {
  it('should render component', async () => {
    render(
      <ThemeWrapper theme={lightTheme}>
        <FilterChip id="test" label="Test" />
      </ThemeWrapper>,
    );
    expect(screen.getByRole('button')).toBeTruthy();
  });

  it('should show chip with correct styling depending on selected and disabled state', () => {
    const nonSelectedStyle = 'MuiChip-outlined';
    const selectedStyle = 'MuiChip-filled';
    const disabledStyle = 'Mui-disabled';

    render(
      <ThemeWrapper theme={lightTheme}>
        <FilterChip id="k1" key="k1" label="Default" />
        <FilterChip id="k2" key="k2" label="Selected" isSelected={true} />
        <FilterChip
          id="k3"
          key="k3"
          label="Disabled"
          isSelected={true}
          isDisabled={true}
        />
      </ThemeWrapper>,
    );

    const defaultButton = screen.getByRole('button', { name: /Default/i });
    const selectedButton = screen.getByRole('button', { name: /Selected/i });
    const disabledButton = screen.getByRole('button', { name: /Disabled/i });

    expect(defaultButton.classList).toContain(nonSelectedStyle);
    expect(defaultButton.classList).not.toContain(selectedStyle);
    expect(defaultButton.classList).not.toContain(disabledStyle);

    expect(selectedButton.classList).not.toContain(nonSelectedStyle);
    expect(selectedButton.classList).toContain(selectedStyle);
    expect(selectedButton.classList).not.toContain(disabledStyle);

    expect(disabledButton.classList).not.toContain(nonSelectedStyle);
    expect(disabledButton.classList).toContain(selectedStyle);
    expect(disabledButton.classList).toContain(disabledStyle);
  });

  it('should call handleClick when chip is selected', () => {
    const handleClick = jest.fn();
    const props = { id: 'test', label: 'Test 1', handleClick };
    render(
      <ThemeWrapper theme={lightTheme}>
        <FilterChip {...props} />
      </ThemeWrapper>,
    );
    fireEvent.click(screen.getByText(props.label));
    expect(props.handleClick).toBeCalledWith(props.id);
  });
});
