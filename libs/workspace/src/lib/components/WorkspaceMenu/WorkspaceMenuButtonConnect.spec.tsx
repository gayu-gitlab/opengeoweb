/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { createStore } from '@redux-eggs/redux-toolkit';
import { render, fireEvent, screen } from '@testing-library/react';
import { lightTheme } from '@opengeoweb/theme';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { WorkspaceMenuButtonConnect } from './WorkspaceMenuButtonConnect';

describe('WorkspaceMenuButtonConnect', () => {
  it('should render correctly upon first opening', async () => {
    const store = createStore();

    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceMenuButtonConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(await screen.findByTestId('workspaceMenuButton')).toBeTruthy();
    expect(store.getState().workspaceList.isFetching).toEqual(true);
    expect(store.getState().workspaceList.isWorkspaceListDialogOpen).toEqual(
      false,
    );
  });

  it('should be able to trigger dialog', async () => {
    const store = createStore();

    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceMenuButtonConnect />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(await screen.findByTestId('workspaceMenuButton')).toBeTruthy();
    expect(store.getState().workspaceList.isFetching).toEqual(true);
    expect(store.getState().workspaceList.isWorkspaceListDialogOpen).toEqual(
      false,
    );

    // toggle
    fireEvent.click(screen.getByTestId('workspaceMenuButton'));

    expect(store.getState().workspaceList.isWorkspaceListDialogOpen).toEqual(
      true,
    );
  });
});
