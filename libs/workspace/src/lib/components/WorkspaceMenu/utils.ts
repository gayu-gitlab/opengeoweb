/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { getHeight } from '@opengeoweb/shared';
import React from 'react';

// Gets element height incl padding, border and margins
export const useGetElementHeight = (
  ref: React.RefObject<HTMLDivElement>,
  defaultHeight: number,
): {
  height: number;
} => {
  const [height, setHeight] = React.useState<number>(defaultHeight);

  React.useLayoutEffect(() => {
    const element = ref?.current as HTMLDivElement;
    if (element) {
      setHeight(element.offsetHeight);
    }

    const observer = new ResizeObserver(() => {
      const height = getHeight(element);
      height && setHeight(height);
    });
    observer.observe(element);

    return (): void => {
      observer.disconnect();
    };
  }, [ref]);

  return { height };
};
