/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';

import { render, renderHook, screen } from '@testing-library/react';

import { useGetElementHeight } from './utils';

describe('WorkspaceMenu/utils', () => {
  describe('useGetElementHeight', () => {
    it('should return height', () => {
      const defaultHeight = 500;

      jest.spyOn(React, 'useRef').mockReturnValue({
        current: {
          offsetHeight: defaultHeight,
        },
      });
      const ref = React.useRef<HTMLDivElement>(null);
      const { result } = renderHook(() =>
        useGetElementHeight(ref, defaultHeight),
      );

      // expect start height to be equal to passed height
      expect(result.current.height).toEqual(defaultHeight);
    });

    it('should return a ref and the height of the element the ref is attached to', () => {
      const defaultHeight = 500;
      Object.defineProperty(HTMLDivElement.prototype, 'offsetHeight', {
        configurable: true,
        value: defaultHeight,
      });

      const TestComponent: React.FC<{
        divHeight: number;
      }> = ({ divHeight }: { divHeight: number }) => {
        const ref = React.useRef<HTMLDivElement>(null);
        const { height } = useGetElementHeight(ref, defaultHeight);
        return (
          <div
            ref={ref}
            style={{ height: divHeight }}
            data-testid="testComponent"
          >
            {height}
          </div>
        );
      };

      render(<TestComponent divHeight={defaultHeight} />);

      expect(screen.getByTestId('testComponent')).toBeTruthy();
      expect(screen.getByText('500')).toBeTruthy();
    });
  });
});
