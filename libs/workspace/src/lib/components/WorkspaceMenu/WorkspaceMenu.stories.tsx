/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import Box from '@mui/material/Box';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import React from 'react';
import { createStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { WorkspaceMenu } from './WorkspaceMenu';
import { workspaceListActions } from '../../store/workspaceList/reducer';
import { WorkspacePresetListItem } from '../../store/workspace/types';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { PresetsApi } from '../../utils/api';

export default {
  title: 'components/Workspace/WorkspaceMenu',
};

const workspaceList: WorkspacePresetListItem[] = [
  {
    id: 'workspaceList-1',
    scope: 'system',
    title: 'Radar',
    date: '2022-06-01T12:34:27.787192',
    viewType: 'singleWindow',
    abstract: 'radar',
  },
  {
    id: 'workspaceList-2',
    scope: 'system',
    title: 'Radar harmonie',
    date: '2022-06-01T12:34:27.787192',
    viewType: 'singleWindow',
    abstract: 'radar and harmonie',
  },
  {
    id: 'workspaceList-3',
    scope: 'system',
    title: 'Harmonie radar',
    date: '2022-06-01T12:34:27.787192',
    viewType: 'singleWindow',
    abstract: '',
  },
];

const store = createStore({
  extensions: [getSagaExtension()],
});

const createApi = (): PresetsApi => {
  return {
    ...createFakeApi(),
    getWorkspacePresets: (): Promise<{ data: WorkspacePresetListItem[] }> => {
      return new Promise((resolve) => {
        resolve({
          data: workspaceList,
        });
      });
    },
  };
};

const WorkspaceMenuDemo = (): React.ReactElement => {
  store.dispatch(
    workspaceListActions.searchFilter({
      searchQuery: 'rad',
    }),
  );
  return (
    <Box sx={{ padding: 1, width: 350, height: '100vh' }}>
      <WorkspaceMenu hideBackdrop isOpen={true} closeMenu={(): void => {}} />
    </Box>
  );
};

export const WorkspaceMenuSearchLight = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore
      theme={lightTheme}
      store={store}
      createApi={createApi}
    >
      <WorkspaceMenuDemo />
    </WorkspaceWrapperProviderWithStore>
  );
};
WorkspaceMenuSearchLight.storyName = 'WorkspaceMenuSearchLight (takeSnapshot)';

export const WorkspaceMenuSearchDark = (): React.ReactElement => {
  return (
    <WorkspaceWrapperProviderWithStore
      theme={darkTheme}
      store={store}
      createApi={createApi}
    >
      <WorkspaceMenuDemo />
    </WorkspaceWrapperProviderWithStore>
  );
};
WorkspaceMenuSearchDark.storyName = 'WorkspaceMenuSearchDark (takeSnapshot)';
