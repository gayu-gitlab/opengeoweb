/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AppStore } from '../../store/store';
import { workspaceListActions } from '../../store/workspaceList/reducer';
import { workspaceListSelectors } from '../../store/workspaceList';
import { WorkspaceMenuButton } from './WorkspaceMenuButton';

export const WorkspaceMenuButtonConnect: React.FC = () => {
  const dispatch = useDispatch();

  const setListDialogOpen = React.useCallback(
    (isListDialogOpen: boolean): void => {
      dispatch(
        workspaceListActions.toggleWorkspaceDialog({
          isWorkspaceListDialogOpen: isListDialogOpen,
        }),
      );
    },
    [dispatch],
  );

  const isWorkspaceListDialogOpen = useSelector((store: AppStore) =>
    workspaceListSelectors.getIsWorkspaceListDialogOpen(store),
  );

  return (
    <WorkspaceMenuButton
      showMenu={isWorkspaceListDialogOpen}
      onToggleDialog={setListDialogOpen}
    />
  );
};
