/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { lightTheme } from '@opengeoweb/theme';
import { WorkspaceMenu } from './WorkspaceMenu';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';

describe('WorkspaceMenu', () => {
  it('should render correctly', async () => {
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme}>
        <WorkspaceMenu isOpen={true} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(await screen.findByTestId('workspaceMenuBackdrop')).toBeTruthy();
  });

  it('should show title', () => {
    const props = {
      isOpen: true,
      onClose: jest.fn(),
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme}>
        <WorkspaceMenu {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.getByText('Workspace menu')).toBeTruthy();
  });

  it('should call closeMenu when close button is pressed', async () => {
    const props = {
      isOpen: true,
      closeMenu: jest.fn(),
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme}>
        <WorkspaceMenu {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(screen.getByTestId('closeBtn'));
    expect(props.closeMenu).toHaveBeenCalled();
  });

  it('should render the list', async () => {
    const props = {
      isOpen: true,
      closeMenu: jest.fn(),
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme}>
        <WorkspaceMenu {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('workspace-selectList')).toBeTruthy();
  });

  it('should call closeMenu when backdrop is clicked', async () => {
    const props = {
      isOpen: true,
      closeMenu: jest.fn(),
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme}>
        <WorkspaceMenu {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    fireEvent.click(screen.getByTestId('workspaceMenuBackdrop'));
    expect(props.closeMenu).toHaveBeenCalled();
  });
});
