/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import { createStore } from '@redux-eggs/redux-toolkit';
import {
  render,
  fireEvent,
  screen,
  waitFor,
  within,
  act,
} from '@testing-library/react';
import { mapSelectors, syncGroupsActions } from '@opengeoweb/store';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import {
  ERROR_RETRY,
  ERROR_TITLE,
  ViewPresetMenuListConnect,
} from './ViewPresetMenuListConnect';
import { viewPresetActions } from '../../store/viewPresets';
import { PresetAction, ViewPresetType } from '../../store/viewPresets/types';
import {
  MAPPRESET_ACTIVE_TITLE,
  MAPPRESET_DIALOG_TITLE_DELETE,
  MAPPRESET_DIALOG_TITLE_EDIT,
  MAPPRESET_DIALOG_TITLE_SAVE_AS,
} from '../../store/viewPresets/utils';
import { ViewPresetListItem } from '../../store/viewPresetsList/types';

const panelId = 'test-1';
const viewPresetsList: ViewPresetListItem[] = [
  {
    title: 'Radar custom',
    id: 'radar',
    date: '',
    scope: 'user',
  },
  {
    title: 'Harmonie',
    id: 'harm',
    date: '',
    scope: 'system',
  },
];

describe('components/ViewPresetMenu/ViewPresetMenuListConnect', () => {
  it('should render correctly', () => {
    const props = {
      panelId,
    };
    render(
      <WorkspaceWrapperProviderWithStore>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.getByTestId('viewpreset-selectList')).toBeTruthy();
  });

  it('should render viewpresets', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });

    const props = {
      panelId,
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.fetchedViewPresets({
          panelId,
          viewPresets: viewPresetsList,
          filterParams: {},
        }),
      ),
    );
    const viewPresetList = await screen.findByTestId('viewpreset-selectList');

    expect(
      within(viewPresetList).getAllByRole('listitem', {
        name: /viewpreset/i,
      }),
    ).toHaveLength(viewPresetsList.length + 1); // viewpreset length + new view preset
  });

  it('should filter view presets list when entering search query', async () => {
    const search = 'abc';

    const store = createStore({
      extensions: [getSagaExtension()],
    });
    const props = {
      panelId,
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: viewPresetsList[0].id,
          panelId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.fetchedViewPresets({
          panelId,
          viewPresets: viewPresetsList,
          filterParams: {},
        }),
      ),
    );

    const viewPresetList = await screen.findByTestId('viewpreset-selectList');

    expect(
      within(viewPresetList).getAllByRole('listitem', {
        name: /viewpreset/i,
      }),
    ).toHaveLength(viewPresetsList.length + 1);

    const textField = await screen.findByRole('textbox');

    fireEvent.change(textField, { target: { value: search } });
    fireEvent.blur(textField);

    await waitFor(() => {
      expect(
        within(viewPresetList).queryAllByRole('listitem', {
          name: /viewpreset/i,
        }),
      ).toHaveLength(0);
    });
  });

  it('should close the dialog after selecting a viewpreset', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });
    const props = {
      panelId,
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: viewPresetsList[0].id,
          panelId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: viewPresetsList[1].id,
          panelId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.fetchedViewPresets({
          panelId,
          viewPresets: viewPresetsList,
          filterParams: {},
        }),
      ),
    );
    const viewPresetList = await screen.findByTestId('viewpreset-selectList');

    expect(
      within(viewPresetList).getAllByRole('listitem', {
        name: /viewpreset/i,
      }),
    ).toHaveLength(viewPresetsList.length + 1);

    const testViewPreset = viewPresetsList[0];

    fireEvent.click(await screen.findByText(testViewPreset.title));

    await waitFor(() =>
      expect(
        store.getState().viewPresets.entities[panelId]
          .isViewPresetListDialogOpen,
      ).toEqual(false),
    );
  });

  it('should select a new viewpreset', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });
    const props = {
      panelId,
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: viewPresetsList[0].id,
          panelId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.fetchedViewPresets({
          panelId,
          viewPresets: viewPresetsList,
          filterParams: {},
        }),
      ),
    );

    const viewPresetList = await screen.findByTestId('viewpreset-selectList');

    expect(
      within(viewPresetList).getAllByRole('listitem', {
        name: /viewpreset/i,
      }),
    ).toHaveLength(viewPresetsList.length + 1);

    expect(
      within(viewPresetList).getAllByRole('listitem', {
        name: /viewpreset/i,
      })[0].textContent,
    ).toEqual(MAPPRESET_ACTIVE_TITLE);

    expect(
      within(viewPresetList).getAllByRole('listitem', {
        name: /viewpreset/i,
      })[0].classList,
    ).not.toContain('Mui-selected');

    fireEvent.click(screen.getByText(MAPPRESET_ACTIVE_TITLE));

    await waitFor(() =>
      expect(
        within(viewPresetList).getAllByRole('listitem', {
          name: /viewpreset/i,
        })[0].classList,
      ).toContain('Mui-selected'),
    );
  });

  it('should open dialog to delete a viewpreset', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });
    const props = {
      panelId,
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        viewPresetActions.fetchedViewPresets({
          panelId,
          viewPresets: viewPresetsList,
          filterParams: {},
        }),
      ),
    );
    const testViewPreset = viewPresetsList[0];

    fireEvent.click(await screen.findByTestId('viewpreset-listDeleteButton'));

    expect(store.getState().viewPresets.viewPresetDialog).toEqual({
      title: MAPPRESET_DIALOG_TITLE_DELETE,
      action: PresetAction.DELETE,
      viewPresetId: testViewPreset.id,
      panelId: props.panelId,
      formValues: { title: testViewPreset.title },
    });
  });

  it('should open dialog to duplicate a viewpreset', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });
    const props = {
      panelId,
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.fetchedViewPresets({
          panelId,
          viewPresets: viewPresetsList,
          filterParams: {},
        }),
      ),
    );
    const testViewPreset = viewPresetsList[0];

    // select radar custom
    fireEvent.click(screen.queryAllByTestId('viewpreset-listOptionsButton')[1]);
    fireEvent.click(await screen.findByText('Duplicate'));

    expect(store.getState().viewPresets.viewPresetDialog).toEqual({
      title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
      action: PresetAction.SAVE_AS,
      viewPresetId: testViewPreset.id,
      panelId: props.panelId,
      formValues: {
        title: testViewPreset.title,
        initialProps: {
          mapPreset: mapSelectors.getMapPreset(store.getState(), props.panelId),
          syncGroupsIds: [],
        },
      },
    });
  });

  it('should open dialog to duplicate a viewpreset with related syncgroups', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });
    const props = {
      panelId,
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.fetchedViewPresets({
          panelId,
          viewPresets: viewPresetsList,
          filterParams: {},
        }),
      ),
    );
    await act(() =>
      store.dispatch(
        syncGroupsActions.syncGroupAddGroup({
          groupId: 'group1',
          title: 'Group 1 for time',
          type: 'SYNCGROUPS_TYPE_SETTIME',
        }),
      ),
    );

    await act(() =>
      store.dispatch(
        syncGroupsActions.syncGroupAddTarget({
          groupId: 'group1',
          targetId: panelId,
        }),
      ),
    );
    const testViewPreset = viewPresetsList[0];

    // select radar custom
    fireEvent.click(screen.queryAllByTestId('viewpreset-listOptionsButton')[1]);
    fireEvent.click(await screen.findByText('Duplicate'));

    expect(store.getState().viewPresets.viewPresetDialog).toEqual({
      title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
      action: PresetAction.SAVE_AS,
      viewPresetId: testViewPreset.id,
      panelId: props.panelId,
      formValues: {
        title: testViewPreset.title,
        initialProps: {
          mapPreset: mapSelectors.getMapPreset(store.getState(), props.panelId),
          syncGroupsIds: ['group1'],
        },
      },
    });
  });

  it('should be able to edit a viewpreset', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });
    const props = {
      panelId,
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.fetchedViewPresets({
          panelId,
          viewPresets: viewPresetsList,
          filterParams: {},
        }),
      ),
    );
    const testViewPreset = viewPresetsList[0];

    // select radar custom
    fireEvent.click(screen.queryAllByTestId('viewpreset-listOptionsButton')[1]);
    fireEvent.click(await screen.findByText('Edit (name)'));

    expect(store.getState().viewPresets.viewPresetDialog).toEqual({
      title: MAPPRESET_DIALOG_TITLE_EDIT,
      action: PresetAction.EDIT,
      viewPresetId: testViewPreset.id,
      panelId: props.panelId,
      formValues: {
        title: testViewPreset.title,
      },
    });
  });

  it('should show the view preset list as loading', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });
    const props = {
      panelId,
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: viewPresetsList[0].id,
          panelId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.fetchViewPresets({
          panelId,
          filterParams: {},
        }),
      ),
    );

    expect(screen.getByTestId('loading-bar')).toBeTruthy();
    expect(screen.queryByText(ERROR_TITLE)).toBeFalsy();
  });

  it('should show error and be able to try again', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });
    const props = {
      panelId,
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: viewPresetsList[0].id,
          panelId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.errorViewPreset({
          panelId,
          error: {
            message: 'can not load list',
            type: ViewPresetType.PRESET_LIST,
          },
        }),
      ),
    );

    expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    expect(screen.getByText(ERROR_TITLE)).toBeTruthy();

    fireEvent.click(screen.getByText(ERROR_RETRY.toUpperCase()));

    await waitFor(() => expect(screen.queryByText(ERROR_TITLE)).toBeFalsy());
    expect(screen.getByTestId('loading-bar')).toBeTruthy();
  });

  it('should filter view presets list when toggling filter chips', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });
    const props = {
      panelId,
    };
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <ViewPresetMenuListConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: viewPresetsList[0].id,
          panelId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.fetchedViewPresets({
          panelId,
          viewPresets: viewPresetsList,
          filterParams: {},
        }),
      ),
    );

    const viewPresetList = await screen.findByTestId('viewpreset-selectList');

    expect(
      within(viewPresetList).getAllByRole('listitem', {
        name: /viewpreset/i,
      }),
    ).toHaveLength(viewPresetsList.length + 1);

    fireEvent.click(screen.getByText('My presets'));

    await waitFor(() => {
      expect(
        within(viewPresetList).getAllByRole('listitem', {
          name: /viewpreset/i,
        }),
      ).toHaveLength(1);
    });

    fireEvent.click(screen.getByText('All'));

    await waitFor(() => {
      expect(
        within(viewPresetList).getAllByRole('listitem', {
          name: /viewpreset/i,
        }),
      ).toHaveLength(viewPresetsList.length + 1);
    });
  });
});
