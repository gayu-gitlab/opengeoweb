/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { shallowEqual, useDispatch, useSelector, useStore } from 'react-redux';
import { mapSelectors, syncGroupsSelectors } from '@opengeoweb/store';
import { CustomIconButton, renderCounter } from '@opengeoweb/shared';
import { Preset } from '@opengeoweb/theme';
import { useTheme } from '@mui/material';
import { WorkspaceOptions } from '../WorkspaceOptions/WorkspaceOptions';
import {
  viewPresetActions,
  viewPresetSelectors,
} from '../../store/viewPresets';
import * as workspaceSelectors from '../../store/workspace/selectors';
import { AppStore } from '../../store/store';

import { PresetAction, ViewPresetDialog } from '../../store/viewPresets/types';
import {
  emptyMapViewPreset,
  MAPPRESET_ACTIVE_TITLE,
  MAPPRESET_DIALOG_TITLE_DELETE,
  MAPPRESET_DIALOG_TITLE_EDIT,
  MAPPRESET_DIALOG_TITLE_SAVE_AS,
} from '../../store/viewPresets/utils';

const VIEW_PRESET_MENU_TOOLTIP = 'View presets menu';

export interface ToggleViewPresetDialogProps {
  panelId: string;
  isViewPresetDialogOpen: boolean;
}

interface WorkspaceViewTitleConnectProps {
  panelId: string;
  isViewPresetDialogOpen: boolean;
}

export const NEW_TITLE_SUFFIX = '(not saved)';
export const EXISTING_TITLE_SUFFIX = '(modified)';

export const getPresetTitle = (
  title: string,
  hasChanges: boolean,
  isNew: boolean,
): string => {
  if (hasChanges && isNew) {
    return `${title} ${NEW_TITLE_SUFFIX}`;
  }
  if (hasChanges && !isNew) {
    return `${title} ${EXISTING_TITLE_SUFFIX}`;
  }
  return title;
};

const WorkspaceViewTitleConnectNoMemo: React.FC<
  WorkspaceViewTitleConnectProps
> = ({ panelId, isViewPresetDialogOpen }: WorkspaceViewTitleConnectProps) => {
  const dispatch = useDispatch();
  const theme = useTheme();
  const store = useStore<AppStore>();

  const workspaceView = useSelector((store: AppStore) => {
    const { title, scope } =
      workspaceSelectors.getViewById(store, panelId) ?? {};
    return { title, scope };
  }, shallowEqual);

  const hasMapPresetChanges = useSelector((store: AppStore) =>
    viewPresetSelectors.getViewPresetHasChanges(store, panelId),
  );

  const activeMapPresetId = useSelector((store: AppStore) =>
    viewPresetSelectors.getViewPresetActiveId(store, panelId),
  );
  const relatedSyncGroupIds = useSelector((store: AppStore) =>
    syncGroupsSelectors.getAllTargetGroupsForSource(store, panelId),
  );

  const openViewPresetsDialog = React.useCallback(
    (viewPresetDialogOptions: ViewPresetDialog) => {
      dispatch(
        viewPresetActions.openViewPresetDialog({
          viewPresetDialog: viewPresetDialogOptions,
        }),
      );
    },
    [dispatch],
  );

  const onSave = (): void => {
    dispatch(
      viewPresetActions.saveViewPreset({
        panelId,
        viewPresetId: activeMapPresetId!,
        viewPreset: {
          ...emptyMapViewPreset,
          title,
          initialProps: {
            mapPreset: mapSelectors.getMapPreset(store.getState(), panelId),
            syncGroupsIds: relatedSyncGroupIds,
          },
        },
      }),
    );
  };

  const onDelete = (): void => {
    openViewPresetsDialog({
      title: MAPPRESET_DIALOG_TITLE_DELETE,
      action: PresetAction.DELETE,
      viewPresetId: activeMapPresetId!,
      panelId,
      formValues: { title },
    });
  };
  const onSaveAs = (): void => {
    const formTitle = activeMapPresetId ? title : '';
    openViewPresetsDialog({
      title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
      action: PresetAction.SAVE_AS,
      viewPresetId: activeMapPresetId!,
      panelId,
      formValues: {
        title: formTitle,
        initialProps: {
          mapPreset: mapSelectors.getMapPreset(store.getState(), panelId),
          syncGroupsIds: relatedSyncGroupIds,
        },
      },
    });
  };

  const onEdit = (): void => {
    const formTitle = activeMapPresetId ? title : '';
    openViewPresetsDialog({
      title: MAPPRESET_DIALOG_TITLE_EDIT,
      action: PresetAction.EDIT,
      panelId,
      viewPresetId: activeMapPresetId!,
      formValues: {
        title: formTitle,
        initialProps: {
          mapPreset: mapSelectors.getMapPreset(store.getState(), panelId),
          syncGroupsIds: relatedSyncGroupIds,
        },
      },
    });
  };

  const toggleViewPresetDialog = React.useCallback(
    ({
      panelId,
      isViewPresetDialogOpen,
    }: ToggleViewPresetDialogProps): void => {
      dispatch(
        viewPresetActions.toggleViewPresetListDialog({
          panelId,
          isViewPresetListDialogOpen: isViewPresetDialogOpen,
        }),
      );
    },
    [dispatch],
  );

  const isNewActive = activeMapPresetId === '';
  const title = workspaceView.title || MAPPRESET_ACTIVE_TITLE;
  const viewScope = workspaceView.scope;

  const fullTitle = getPresetTitle(title, hasMapPresetChanges!, isNewActive);

  renderCounter.count(WorkspaceViewTitleConnectNoMemo.name);

  return (
    <>
      <CustomIconButton
        data-testid="open-viewpresets"
        sx={{
          marginLeft: -1,
          marginRight: 1,
          '&&': {
            backgroundColor: !isViewPresetDialogOpen
              ? 'transparent'
              : undefined,
            '&:hover': {
              backgroundColor: 'geowebColors.workspace.tabButtonHover',
            },
            '.MuiSvgIcon-root > path': {
              fill: isViewPresetDialogOpen
                ? theme.palette.geowebColors.iconButtons.tool.active.color
                : undefined,
            },
          },
        }}
        variant="tool"
        onClick={(): void => {
          toggleViewPresetDialog({
            panelId,
            isViewPresetDialogOpen: !isViewPresetDialogOpen,
          });
        }}
        tooltipTitle={VIEW_PRESET_MENU_TOOLTIP}
        isSelected={isViewPresetDialogOpen}
      >
        <Preset />
      </CustomIconButton>
      <WorkspaceOptions
        panelId={panelId}
        title={fullTitle}
        hasChanges={hasMapPresetChanges || isNewActive}
        handleSave={viewScope === 'user' ? onSave : undefined}
        handleDelete={viewScope === 'user' ? onDelete : undefined}
        handleSaveAs={onSaveAs}
        handleEdit={viewScope === 'user' ? onEdit : undefined}
        id="viewpreset-options-toolbutton"
      />
    </>
  );
};

export const WorkspaceViewTitleConnect = React.memo(
  WorkspaceViewTitleConnectNoMemo,
);
