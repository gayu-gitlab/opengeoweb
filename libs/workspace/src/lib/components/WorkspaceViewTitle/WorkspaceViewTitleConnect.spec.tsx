/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  fireEvent,
  render,
  waitFor,
  screen,
  act,
} from '@testing-library/react';
import { createStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import {
  EXISTING_TITLE_SUFFIX,
  NEW_TITLE_SUFFIX,
  WorkspaceViewTitleConnect,
  getPresetTitle,
} from './WorkspaceViewTitleConnect';
import { createApi as createFakeApi } from '../../utils/fakeApi';

import { WorkspaceWrapperProviderWithStore } from '../Providers';
import { PresetsApi } from '../../utils/api';
import { PresetAction } from '../../store/viewPresets/types';
import { viewPresetActions } from '../../store/viewPresets';
import {
  MAPPRESET_ACTIVE_TITLE,
  MAPPRESET_DIALOG_TITLE_DELETE,
  MAPPRESET_DIALOG_TITLE_EDIT,
  MAPPRESET_DIALOG_TITLE_SAVE_AS,
} from '../../store/viewPresets/utils';
import { workspaceActions } from '../../store/workspace/reducer';
import { WorkspacePreset } from '../../store/workspace/types';

describe('src/components/WorkspaceViewTitle/WorkspaceViewTitleConnect', () => {
  describe('getPresetTitle', () => {
    it('should return same title if no changes for new preset', () => {
      expect(getPresetTitle('test', false, true)).toEqual('test');
    });

    it('should return title for changed new preset', () => {
      expect(getPresetTitle('test', true, true)).toEqual(
        `test ${NEW_TITLE_SUFFIX}`,
      );
    });
    it('should return title for changed existing preset', () => {
      expect(getPresetTitle('test', true, false)).toEqual(
        `test ${EXISTING_TITLE_SUFFIX}`,
      );
    });
  });

  const mapId = 'mapId_123';
  const workspacePreset: WorkspacePreset = {
    id: 'preset1',
    title: 'Preset 1',
    views: {
      allIds: [mapId, 'preset-2'],
      byId: {
        [mapId]: {
          id: 'test-screen-1',
          title: 'Observation',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          scope: 'user',
        },
      },
    },
    mosaicNode: mapId,
  };

  it('should save an existing preset when choosing Save', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });

    const mockSave = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        saveViewPreset: mockSave,
      };
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <WorkspaceViewTitleConnect
          panelId={mapId}
          isViewPresetDialogOpen={false}
        />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: mapId,
          panelId: mapId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(workspaceActions.setPreset({ workspacePreset })),
    );
    await screen.findByLabelText('Observation');
    fireEvent.click(await screen.findByTestId('viewpreset-options-toolbutton'));

    await waitFor(() => {
      expect(screen.getByText('Save')).toBeTruthy();
    });

    fireEvent.click(screen.getByText('Save'), { exact: true });

    await screen.findByText('Map preset "Observation" successfully saved');

    await waitFor(() => {
      expect(mockSave).toHaveBeenCalledWith(mapId, expect.any(Object));
    });
  });

  it('should open viewpresetdialog when choosing delete', async () => {
    const store = createStore();

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewTitleConnect
          panelId={mapId}
          isViewPresetDialogOpen={false}
        />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: mapId,
          panelId: mapId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(workspaceActions.setPreset({ workspacePreset })),
    );
    await screen.findByLabelText('Observation');
    fireEvent.click(screen.getByTestId('viewpreset-options-toolbutton'));

    fireEvent.click(await screen.findByText('Delete'), { exact: true });

    await waitFor(() =>
      expect(store.getState().viewPresets.viewPresetDialog).toEqual({
        title: MAPPRESET_DIALOG_TITLE_DELETE,
        action: PresetAction.DELETE,
        viewPresetId: mapId,
        panelId: mapId,
        formValues: { title: workspacePreset.views.byId![mapId].title },
      }),
    );
  });

  it('should open viewpresetdialog when choosing save as', async () => {
    const store = createStore();

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewTitleConnect
          panelId={mapId}
          isViewPresetDialogOpen={false}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: mapId,
          panelId: mapId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(workspaceActions.setPreset({ workspacePreset })),
    );
    await screen.findByLabelText('Observation');
    fireEvent.click(screen.getByTestId('viewpreset-options-toolbutton'));

    await waitFor(() => {
      expect(screen.getByText('Save as')).toBeTruthy();
    });

    fireEvent.click(screen.getByText('Save as'), { exact: true });

    await waitFor(() =>
      expect(store.getState().viewPresets.viewPresetDialog).toEqual({
        title: MAPPRESET_DIALOG_TITLE_SAVE_AS,
        action: PresetAction.SAVE_AS,
        viewPresetId: mapId,
        panelId: mapId,
        formValues: {
          title: workspacePreset.views.byId![mapId].title,
          initialProps: { mapPreset: expect.any(Object), syncGroupsIds: [] },
        },
      }),
    );
  });

  it('show new title if no title can be found', async () => {
    render(
      <WorkspaceWrapperProviderWithStore>
        <WorkspaceViewTitleConnect
          panelId={mapId}
          isViewPresetDialogOpen={false}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByText(MAPPRESET_ACTIVE_TITLE)).toBeTruthy();
  });

  it('should edit an existing preset when choosing edit', async () => {
    const store = createStore();

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceViewTitleConnect
          panelId={mapId}
          isViewPresetDialogOpen={false}
        />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.registerViewPreset({
          viewPresetId: mapId,
          panelId: mapId,
        }),
      ),
    );
    await act(() =>
      store.dispatch(workspaceActions.setPreset({ workspacePreset })),
    );
    await screen.findByLabelText('Observation');
    fireEvent.click(screen.getByTestId('viewpreset-options-toolbutton'));

    await waitFor(() => {
      expect(screen.getByText('Edit (name)')).toBeTruthy();
    });

    fireEvent.click(screen.getByText('Edit (name)'), { exact: true });

    await waitFor(() =>
      expect(store.getState().viewPresets.viewPresetDialog).toEqual({
        title: MAPPRESET_DIALOG_TITLE_EDIT,
        action: PresetAction.EDIT,
        viewPresetId: mapId,
        panelId: mapId,
        formValues: {
          title: workspacePreset.views.byId![mapId].title,
          initialProps: { mapPreset: expect.any(Object), syncGroupsIds: [] },
        },
      }),
    );
  });
});
