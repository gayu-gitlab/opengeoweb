/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import {
  ThemeProviderProps,
  ThemeWrapper,
  lightTheme,
} from '@opengeoweb/theme';
import { Provider } from 'react-redux';
import { Store } from '@reduxjs/toolkit';
import { ConfirmationServiceProvider, withEggs } from '@opengeoweb/shared';
import { timeSeriesModuleConfig } from '@opengeoweb/timeseries';
import { ApiProvider } from '@opengeoweb/api';
import {
  AuthenticationDefaultStateProps,
  AuthenticationProvider,
} from '@opengeoweb/authentication';
import { SnackbarWrapperConnect } from '@opengeoweb/snackbar';
import { coreModuleConfig } from '@opengeoweb/store';
import workspaceModuleConfig from '../../store/config';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { API_NAME } from '../../utils/api';
import { store as defaultStore } from '../../store';

const WorkspaceThemeProvider: React.FC<ThemeProviderProps> = ({
  children,
  theme = lightTheme,
}: ThemeProviderProps) => <ThemeWrapper theme={theme}>{children}</ThemeWrapper>;

const WorkspaceWrapperProvider: React.FC<ThemeProviderProps> = withEggs([
  ...coreModuleConfig,
  timeSeriesModuleConfig,
  workspaceModuleConfig,
])(({ theme, children }: ThemeProviderProps) => (
  <WorkspaceThemeProvider theme={theme}>{children}</WorkspaceThemeProvider>
));

interface WorkspaceWrapperProviderWithStoreProps extends ThemeProviderProps {
  store?: Store;
  createApi?: () => void;
  auth?: AuthenticationDefaultStateProps;
}

export const defaultAuthConfig = {
  isLoggedIn: true,
  auth: {
    username: 'user.name',
    token: '1223344',
    refresh_token: '33455214',
  },
  onLogin: (): void => null!,
  onSetAuth: (): void => null!,
  sessionStorageProvider: null!,
};

export const WorkspaceWrapperProviderWithStore: React.FC<
  WorkspaceWrapperProviderWithStoreProps
> = ({
  children,
  store = defaultStore,
  createApi = createFakeApi,
  theme,
  auth = defaultAuthConfig,
}: WorkspaceWrapperProviderWithStoreProps) => (
  <AuthenticationProvider value={auth}>
    <ApiProvider createApi={createApi} name={API_NAME}>
      <Provider store={store}>
        <WorkspaceWrapperProvider theme={theme}>
          <SnackbarWrapperConnect>
            <ConfirmationServiceProvider>
              {children as React.ReactElement}
            </ConfirmationServiceProvider>
          </SnackbarWrapperConnect>
        </WorkspaceWrapperProvider>
      </Provider>
    </ApiProvider>
  </AuthenticationProvider>
);
