/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render, screen } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import { configureStore } from '@reduxjs/toolkit';
import * as authenticationModule from '@opengeoweb/authentication';
import { Provider } from 'react-redux';
import userEvent from '@testing-library/user-event';
import createSagaMiddleware from 'redux-saga';
import { ApiProvider } from '@opengeoweb/api';
import {
  workspaceActions,
  reducer as workspaceReducer,
} from '../../store/workspace/reducer';
import {
  workspaceListActions,
  reducer as workspaceListReducer,
} from '../../store/workspaceList/reducer';
import { WorkspaceTabConnect } from './WorkspaceTabConnect';
import {
  WorkspacePreset,
  WorkspacePresetListItem,
} from '../../store/workspace/types';
import workspaceListSaga from '../../store/workspaceList/sagas';
import workspaceSaga from '../../store/workspace/sagas';
import { API_NAME, PresetsApi } from '../../utils/api';
import { createApi as createFakeApi } from '../../utils/fakeApi';

describe('workspace/components/WorkspaceTabConnect', () => {
  const mosaicNode = {
    direction: 'row',
    first: 'screen1',
    second: 'screen2',
  } as const;
  const workpsacePreset: WorkspacePreset = {
    id: 'preset1',
    title: 'Preset 1',
    scope: 'user',
    views: {
      allIds: ['screen1', 'screen2'],
      byId: {
        screen1: {
          title: 'screen 1',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
        screen2: {
          title: 'screen 2',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
      },
    },
    mosaicNode,
    viewType: 'multiWindow',
  };

  const { views, ...workspaceListItem } = workpsacePreset;

  const sagaMiddleware = createSagaMiddleware();

  const createApi = (): PresetsApi => {
    return {
      ...createFakeApi(),
      getWorkspacePresets: (): Promise<{ data: WorkspacePresetListItem[] }> => {
        return new Promise((resolve) => {
          resolve({ data: [workspaceListItem as WorkspacePresetListItem] });
        });
      },
      deleteWorkspacePreset: (): Promise<void> => {
        return new Promise((resolve) => {
          resolve();
        });
      },
    };
  };

  // Mock being logged in as otherwise the tab header is disabled
  const mock = jest.spyOn(authenticationModule, 'useAuthenticationContext');
  mock.mockImplementation(
    (): authenticationModule.AuthenticationContextProps => {
      return {
        isLoggedIn: true,
        onLogin: jest.fn(),
        auth: null,
        onSetAuth: jest.fn(),
        sessionStorageProvider:
          {} as authenticationModule.SessionStorageProvider,
        authConfig: {} as authenticationModule.AuthenticationConfig,
      };
    },
  );

  const store = configureStore({
    reducer: {
      workspace: workspaceReducer,
      workspaceList: workspaceListReducer,
    },
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(sagaMiddleware),
  });

  sagaMiddleware.run(workspaceListSaga);
  sagaMiddleware.run(workspaceSaga);

  it('should display correct workspace title after deleting from the tab header', async () => {
    store.dispatch(
      workspaceActions.setPreset({ workspacePreset: workpsacePreset }),
    );
    store.dispatch(
      workspaceListActions.toggleWorkspaceDialog({
        isWorkspaceListDialogOpen: true,
      }),
    );
    const props = {
      workspaceId: workpsacePreset.id || '',
    };

    render(
      <ThemeWrapper>
        <ApiProvider createApi={createApi} name={API_NAME}>
          <Provider store={store}>
            <WorkspaceTabConnect {...props} />
          </Provider>
        </ApiProvider>
      </ThemeWrapper>,
    );

    const user = userEvent.setup();

    // Should show the workspace menu list
    expect(screen.getByText('Workspace menu')).toBeTruthy();
    expect(
      (await screen.findByTestId('workspaceMenuBackdrop'))
        .getAttribute('style')!
        .includes('opacity: 1'),
    );

    // Check correct preset is displayed in the tab header and in the list
    expect((await screen.findAllByText('Preset 1')).length).toBe(2);

    // Close the workspace menu and click on the Tab to show the options
    await user.click(screen.getByRole('button', { name: 'workspace menu' }));
    expect(
      (await screen.findByTestId('workspaceMenuBackdrop'))
        .getAttribute('style')!
        .includes('opacity: 0'),
    );
    await user.click(screen.getByRole('button', { name: 'Preset 1' }));
    await user.click(screen.getByRole('menuitem', { name: 'Delete' }));
    // Confirm deletion
    await user.click(screen.getByRole('button', { name: 'Delete' }));

    // Ensure the tab header has been correctly updated
    expect((await screen.findAllByText('Preset 1')).length).toBe(1);
    expect(screen.getByText('New workspace (modified)')).toBeTruthy();
  });

  it('should display correct workspace title after deleting opened preset from the list', async () => {
    store.dispatch(
      workspaceActions.setPreset({ workspacePreset: workpsacePreset }),
    );
    store.dispatch(
      workspaceListActions.toggleWorkspaceDialog({
        isWorkspaceListDialogOpen: true,
      }),
    );
    const props = {
      workspaceId: workpsacePreset.id || '',
    };

    render(
      <ThemeWrapper>
        <ApiProvider createApi={createApi} name={API_NAME}>
          <Provider store={store}>
            <WorkspaceTabConnect {...props} />
          </Provider>
        </ApiProvider>
      </ThemeWrapper>,
    );

    const user = userEvent.setup();

    // Should show the workspace menu list
    expect(screen.getByText('Workspace menu')).toBeTruthy();
    expect(
      (await screen.findByTestId('workspaceMenuBackdrop'))
        .getAttribute('style')!
        .includes('opacity: 1'),
    );

    // Check correct preset is displayed in the tab header and in the list
    expect((await screen.findAllByText('Preset 1')).length).toBe(2);

    // Click the delete option in the menu list
    await user.click(screen.getByTestId('workspace-listDeleteButton'));
    // Confirm deletion
    await user.click(screen.getByRole('button', { name: 'Delete' }));

    // Ensure the tab header has been correctly updated
    expect((await screen.findAllByText('Preset 1')).length).toBe(1);
    expect(screen.getByText('New workspace (modified)')).toBeTruthy();
  });
});
