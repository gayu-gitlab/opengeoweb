/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { WorkspaceTab } from './WorkspaceTab';
import { AppStore } from '../../store/store';
import * as workspaceSelectors from '../../store/workspace/selectors';
import WorkspacePresetsFormDialog from '../WorkspacePresetsFormDialog/WorkspacePresetsFormDialog';
import * as workspaceListSelectors from '../../store/workspaceList/selectors';
import { workspaceListActions } from '../../store/workspaceList/reducer';
import {
  WorkspacePreset,
  WorkspacePresetAction,
} from '../../store/workspace/types';

interface WorkspaceTabConnectProps {
  workspaceId: string;
}

export const WorkspaceTabConnect: React.FC<WorkspaceTabConnectProps> = ({
  workspaceId,
}: WorkspaceTabConnectProps) => {
  const dispatch = useDispatch();

  const title = useSelector((store: AppStore) =>
    workspaceSelectors.getSelectedWorkspaceTitle(store),
  );
  const dialogDetails = useSelector((store: AppStore) =>
    workspaceListSelectors.getWorkspaceActionDialogDetails(store),
  );

  const closeWorkspaceActionDialog = React.useCallback(() => {
    dispatch(workspaceListActions.closeWorkspaceActionDialogOptions());
  }, [dispatch]);

  const onSubmitForm = (
    presetAction: WorkspacePresetAction,
    presetId: string,
    data: WorkspacePreset,
  ): void => {
    dispatch(
      workspaceListActions.submitFormWorkspaceActionDialogOptions({
        presetAction,
        presetId,
        data,
      }),
    );
  };

  const presetId = typeof dialogDetails === 'object' && dialogDetails.presetId;
  const action = typeof dialogDetails === 'object' && dialogDetails.action;
  const formValues =
    typeof dialogDetails === 'object' && dialogDetails.formValues;
  const isFetching =
    (typeof dialogDetails === 'object' && dialogDetails.isFetching) || false;

  const error =
    typeof dialogDetails === 'object' && dialogDetails.error?.message;

  return (
    <>
      <WorkspaceTab activeTab title={title} workspaceId={workspaceId} />
      {Boolean(dialogDetails) && (
        <WorkspacePresetsFormDialog
          isOpen={Boolean(dialogDetails)}
          onClose={closeWorkspaceActionDialog}
          presetId={presetId || undefined!}
          action={action || undefined!}
          formValues={formValues || undefined!}
          onFormSubmit={onSubmitForm}
          isLoading={isFetching}
          error={error || undefined!}
        />
      )}
    </>
  );
};
