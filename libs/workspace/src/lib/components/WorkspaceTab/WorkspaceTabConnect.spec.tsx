/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
} from '@testing-library/react';
import { lightTheme } from '@opengeoweb/theme';
import { createStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import { WorkspaceTabConnect } from './WorkspaceTabConnect';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import {
  WorkspacePreset,
  WorkspacePresetAction,
} from '../../store/workspace/types';
import { workspaceListActions } from '../../store/workspaceList/reducer';
import { workspaceActions } from '../../store/workspace/reducer';
import { PresetsApi } from '../../utils/api';
import { createApi as createFakeApi } from '../../utils/fakeApi';

describe('workspace/components/WorkspaceTabConnect', () => {
  const mosaicNode = {
    direction: 'row',
    first: 'screen1',
    second: 'screen2',
  } as const;
  const screenConfig: WorkspacePreset = {
    id: 'preset1',
    title: 'Preset 1',
    views: {
      allIds: ['screen1', 'screen2'],
      byId: {
        screen1: {
          title: 'screen 1',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
        screen2: {
          title: 'screen 2',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
      },
    },
    mosaicNode,
  };
  it('should render the title, workspacemenu should be hidden', async () => {
    const store = createStore();
    const props = {
      workspaceId: 'test-1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceTabConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );
    await screen.findByText('Preset 1');
    expect(
      (await screen.findByTestId('workspaceMenuBackdrop'))
        .getAttribute('style')!
        .includes('visibility: hidden'),
    ).toBeTruthy();
  });

  const screenConfigEmptyView: WorkspacePreset = {
    id: 'emptyMap',
    title: 'Empty map',
    views: {
      allIds: ['emptyMap'],
      byId: {
        emptyMapView: {
          title: 'Empty map',
          componentType: 'map',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
      },
    },
    mosaicNode,
  };

  it('should not include the action dialog if no options set in store', async () => {
    const store = createStore();
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceTabConnect workspaceId="workspaceList-1" />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfigEmptyView }),
      ),
    );
    await act(() =>
      store.dispatch(
        workspaceListActions.fetchedWorkspaceList({
          workspaceList: [
            {
              id: 'workspaceList-1',
              scope: 'system',
              title: 'Workspace list item',
              date: '2022-06-01T12:34:27.787192',
              viewType: 'singleWindow',
              abstract: '',
            },
          ],
        }),
      ),
    );
    expect(screen.queryByTestId('workspace-preset-dialog')).toBeFalsy();
  });

  it('should include the action dialog if options set in store, closing should dispatch a call', async () => {
    const store = createStore();
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceTabConnect workspaceId="workspaceList-1" />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfigEmptyView }),
      ),
    );
    await act(() =>
      store.dispatch(
        workspaceListActions.fetchedWorkspaceList({
          workspaceList: [
            {
              id: 'workspaceList-1',
              scope: 'system',
              title: 'Workspace list item',
              date: '2022-06-01T12:34:27.787192',
              viewType: 'singleWindow',
              abstract: '',
            },
          ],
        }),
      ),
    );
    await act(() =>
      store.dispatch(
        workspaceListActions.openWorkspaceActionDialogOptions({
          action: WorkspacePresetAction.DELETE,
          presetId: 'workspaceList-1',
          formValues: {
            title: 'Workspace list item',
          },
        }),
      ),
    );
    expect(store.getState().workspaceList.workspaceActionDialog).toBeDefined();
    expect(screen.getByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(
      screen.getByText(
        `Are you sure to want to delete the "Workspace list item" workspace preset?`,
      ),
    ).toBeTruthy();

    // Click close button
    fireEvent.click(screen.getByTestId('customDialog-close'));

    expect(screen.queryByTestId('workspace-preset-dialog')).toBeFalsy();

    expect(
      store.getState().workspaceList.workspaceActionDialog,
    ).not.toBeDefined();
  });

  it('should handle onSubmitForm', async () => {
    const testDialog = {
      action: WorkspacePresetAction.DELETE,
      presetId: 'workspaceList-1',
      formValues: {
        title: 'Workspace list item',
        abstract: 'some abs',
      },
    };

    const store = createStore({
      extensions: [getSagaExtension({})],
    });

    const deleteWorkspacePresetWatcher = jest.fn();

    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        deleteWorkspacePreset: deleteWorkspacePresetWatcher,
      };
    };

    render(
      <WorkspaceWrapperProviderWithStore
        theme={lightTheme}
        store={store}
        createApi={createApi}
      >
        <WorkspaceTabConnect workspaceId="workspaceList-1" />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfigEmptyView }),
      ),
    );
    await act(() =>
      store.dispatch(
        workspaceListActions.fetchedWorkspaceList({
          workspaceList: [
            {
              id: 'workspaceList-1',
              scope: 'system',
              title: 'Workspace list item',
              date: '2022-06-01T12:34:27.787192',
              viewType: 'singleWindow',
              abstract: '',
            },
          ],
        }),
      ),
    );
    await act(() =>
      store.dispatch(
        workspaceListActions.openWorkspaceActionDialogOptions({
          ...testDialog,
        }),
      ),
    );
    expect(store.getState().workspaceList.workspaceActionDialog).toBeDefined();
    expect(screen.getByTestId('workspace-preset-dialog')).toBeTruthy();
    expect(
      screen.getByText(
        `Are you sure to want to delete the "${testDialog.formValues.title}" workspace preset?`,
      ),
    ).toBeTruthy();

    fireEvent.click(screen.getByText('Delete'));

    await waitFor(() => {
      expect(deleteWorkspacePresetWatcher).toHaveBeenCalled();
    });
  });
});
