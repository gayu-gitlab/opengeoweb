/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Box } from '@mui/material';
import { store } from '../../store';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { WorkspaceSelectListConnect } from './WorkspaceSelectListConnect';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { PresetsApi } from '../../utils/api';
import { WorkspacePresetListItem } from '../../store/workspace/types';
import { useFirstTimeError } from '../../storyUtils/useFirstTimeError';

export default {
  title: 'components/Workspace/WorkspaceSelectList',
};

export const WorkspaceSelectListFetchError = (): React.ReactElement => {
  const { fetch } = useFirstTimeError();
  const createFakeApiWithError = (): PresetsApi => {
    const currentFakeApi = createFakeApi();
    return {
      ...currentFakeApi,
      getWorkspacePresets: (): Promise<{
        data: WorkspacePresetListItem[];
      }> => {
        return fetch(currentFakeApi.getWorkspacePresets());
      },
    };
  };
  return (
    <WorkspaceWrapperProviderWithStore
      store={store}
      createApi={createFakeApiWithError}
    >
      <Box sx={{ width: '320px' }}>
        <WorkspaceSelectListConnect />
      </Box>
    </WorkspaceWrapperProviderWithStore>
  );
};
