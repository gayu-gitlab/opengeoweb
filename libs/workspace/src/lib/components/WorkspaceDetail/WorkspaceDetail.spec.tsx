/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import {
  render,
  fireEvent,
  screen,
  waitFor,
  act,
} from '@testing-library/react';
import { createStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import { routerActions } from '@opengeoweb/store';
import { WorkspaceDetail } from './WorkspaceDetail';
import { componentsLookUp } from '../../storyUtils/componentsLookUp';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { workspaceActions } from '../../store/workspace/reducer';
import {
  ViewPresetListItem,
  WorkspaceError,
  WorkspaceErrorType,
} from '../../store/workspace/types';
import { emptyMapWorkspace } from '../../store/workspaceList/utils';
import { viewPresetActions } from '../../store/viewPresets';
import { PresetsApi } from '../../utils/api';
import { createApi as createFakeApi } from '../../utils/fakeApi';

const panelId = 'test-1';
const viewPresetsList: ViewPresetListItem[] = [
  {
    title: 'Radar custom',
    id: 'radar',
    date: '',
    scope: 'user',
  },
  {
    title: 'Harmonie',
    id: 'harm',
    date: '',
    scope: 'system',
  },
];

describe('WorkspaceDetail', () => {
  it('should render correctly', async () => {
    const testChildren = 'some children';
    render(
      <WorkspaceWrapperProviderWithStore>
        <WorkspaceDetail componentsLookUp={componentsLookUp}>
          <p>{testChildren}</p>
        </WorkspaceDetail>
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('workspace')).toBeTruthy();
    expect(screen.getByText(testChildren)).toBeTruthy();
  });

  it('should fetch initial view presets', async () => {
    const spy = jest.spyOn(viewPresetActions, 'fetchInitialViewPresets');

    render(
      <WorkspaceWrapperProviderWithStore>
        <WorkspaceDetail componentsLookUp={componentsLookUp}>
          <p>some children</p>
        </WorkspaceDetail>
      </WorkspaceWrapperProviderWithStore>,
    );
    await waitFor(() => expect(spy).toHaveBeenCalled());
    spy.mockRestore();
  });

  it('should show loading bar when isLoading and keep WorkspacePage visible', async () => {
    const store = createStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceDetail componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.fetchWorkspace({
          workspaceId: '',
        }),
      ),
    );
    expect(screen.getByTestId('loading-bar')).toBeTruthy();
    expect(screen.getByTestId('WorkspacePage')).toBeTruthy();
  });

  it('should render empty map preset when no workspace id', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });
    const spy = jest.spyOn(routerActions, 'navigateToUrl');

    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceDetail componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await waitFor(() =>
      expect(store.getState().workspace.title).toEqual(emptyMapWorkspace.title),
    );
    await waitFor(() =>
      expect(store.getState().workspace.mosaicNode).toEqual(
        emptyMapWorkspace.mosaicNode,
      ),
    );
    await waitFor(() =>
      expect(store.getState().workspace.views).toEqual(emptyMapWorkspace.views),
    );

    await waitFor(() => expect(spy).toHaveBeenCalledWith({ url: '/' }));
  });

  it('should start fetching workspace', async () => {
    const testWorkspaceId = 'test';

    render(
      <WorkspaceWrapperProviderWithStore>
        <WorkspaceDetail
          workspaceId={testWorkspaceId}
          componentsLookUp={componentsLookUp}
        />
      </WorkspaceWrapperProviderWithStore>,
    );

    expect(screen.getByTestId('loading-bar')).toBeTruthy();
  });

  it('should show not found error', async () => {
    const testError: WorkspaceError = {
      message: 'test error',
      type: WorkspaceErrorType.NOT_FOUND,
    };

    const store = createStore({
      extensions: [getSagaExtension()],
    });
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceDetail workspaceId="" componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.errorWorkspace({
          workspaceId: '',
          error: testError,
        }),
      ),
    );
    expect(await screen.findByText(`Failed to fetch workspace`)).toBeTruthy();
    expect(screen.queryByText('Try again')).toBeFalsy();
    expect(screen.queryByTestId('WorkspacePage')).toBeFalsy();
  });

  it('should show generic error and be able to try again', async () => {
    const testError: WorkspaceError = {
      message: 'test error',
      type: WorkspaceErrorType.GENERIC,
    };

    const store = createStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceDetail workspaceId="" componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.errorWorkspace({
          workspaceId: '',
          error: testError,
        }),
      ),
    );
    expect(await screen.findByText(`Failed to fetch workspace`)).toBeTruthy();
    expect(screen.getByText('TRY AGAIN')).toBeTruthy();
    expect(screen.queryByTestId('WorkspacePage')).toBeFalsy();

    fireEvent.click(screen.getByText('TRY AGAIN'));

    await waitFor(() =>
      expect(screen.queryByText(`Failed to fetch workspace`)).toBeFalsy(),
    );
    expect(screen.getByTestId('loading-bar')).toBeTruthy();
  });

  it('should show save error and be able to try again', async () => {
    const testError: WorkspaceError = {
      message: 'test error',
      type: WorkspaceErrorType.SAVE,
    };
    const store = createStore({
      extensions: [getSagaExtension()],
    });

    const mockSave = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        saveWorkspacePreset: mockSave,
      };
    };

    render(
      <WorkspaceWrapperProviderWithStore store={store} createApi={createApi}>
        <WorkspaceDetail workspaceId="" componentsLookUp={componentsLookUp} />
      </WorkspaceWrapperProviderWithStore>,
    );

    await act(() =>
      store.dispatch(
        workspaceActions.errorWorkspace({
          workspaceId: '',
          error: testError,
        }),
      ),
    );

    expect(
      await screen.findByText(
        `Failed to save workspace "${emptyMapWorkspace.title}"`,
      ),
    ).toBeTruthy();
    expect(screen.getByText('TRY AGAIN')).toBeTruthy();
    expect(screen.getByTestId('WorkspacePage')).toBeTruthy();

    fireEvent.click(screen.getByText('TRY AGAIN'));

    await waitFor(() =>
      expect(
        screen.queryByText(
          `Failed to save workspace "${emptyMapWorkspace.title}"`,
        ),
      ).toBeFalsy(),
    );
    expect(mockSave).toHaveBeenCalled();
  });

  it('should not fetch initial view presets when viewPresetsList is already available', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });
    const spy = jest.spyOn(viewPresetActions, 'fetchInitialViewPresets');

    const { rerender } = render(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceDetail componentsLookUp={componentsLookUp}>
          <p>some children</p>
        </WorkspaceDetail>
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.fetchedViewPresets({
          panelId,
          viewPresets: viewPresetsList,
          filterParams: {},
        }),
      ),
    );
    expect(store.getState().viewPresetsList.ids.length).not.toEqual(0);
    expect(screen.getByTestId('workspace')).toBeTruthy();

    spy.mockReset();
    expect(spy).not.toHaveBeenCalled();

    rerender(
      <WorkspaceWrapperProviderWithStore store={store}>
        <WorkspaceDetail componentsLookUp={componentsLookUp}>
          <p>some children</p>
        </WorkspaceDetail>
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.getByTestId('workspace')).toBeTruthy();
    await waitFor(() => expect(spy).not.toHaveBeenCalled());
    spy.mockRestore();
  });
});
