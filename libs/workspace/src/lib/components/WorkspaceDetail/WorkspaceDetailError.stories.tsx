/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';

import { createApi as createFakeApi } from '../../utils/fakeApi';
import { PresetsApi } from '../../utils/api';
import { WorkspacePresetFromBE } from '../../store/workspace/types';
import { StoryWrapper } from '../../storyUtils/StoryWrapper';
import { useFirstTimeError } from '../../storyUtils/useFirstTimeError';
import { ViewPreset } from '../../store/viewPresets/types';

export default {
  title: 'components/Workspace/Workspace Error',
};

export const WorkspaceNotFound = (): React.ReactElement => {
  return <StoryWrapper workspaceId="nonExistingId" />;
};

export const WorkspaceFetchError = (): React.ReactElement => {
  const { fetch } = useFirstTimeError(-1);
  const createFakeApiWithError = (): PresetsApi => {
    const currentFakeApi = createFakeApi();
    return {
      ...currentFakeApi,
      getWorkspacePreset: (
        workspaceId,
      ): Promise<{ data: WorkspacePresetFromBE }> => {
        return fetch(currentFakeApi.getWorkspacePreset(workspaceId));
      },
    };
  };
  return (
    <StoryWrapper
      createApi={createFakeApiWithError}
      workspaceId="screenConfigRadarTemp"
    />
  );
};

export const WorkspaceFetchDetailError = (): React.ReactElement => {
  const testCounter = React.useRef(0);
  const createFakeApiWithError = (): PresetsApi => {
    const currentFakeApi = createFakeApi();
    return {
      ...currentFakeApi,
      getViewPreset: (workspaceId): Promise<{ data: ViewPreset }> => {
        testCounter.current += 1;
        // first time return error, after retry it should return correct data
        if (testCounter.current % 2 === 0 && testCounter.current < 5) {
          return new Promise((_, reject) => {
            setTimeout(() => {
              reject(
                new Error(`Request fetching workspace ${workspaceId} failed`),
              );
            }, 1000);
          });
        }
        return currentFakeApi.getViewPreset(workspaceId);
      },
    };
  };
  return (
    <StoryWrapper
      workspaceId="screenConfigOBS"
      createApi={createFakeApiWithError}
    />
  );
};

export const WorkspaceSave = (): React.ReactElement => {
  const { fetch } = useFirstTimeError();

  const createFakeApiWithErrorOnSave = (): PresetsApi => {
    const currentFakeApi = createFakeApi();

    return {
      ...currentFakeApi,
      saveWorkspacePreset: (workspaceId, data): Promise<void> => {
        return fetch(currentFakeApi.saveWorkspacePreset(workspaceId, data));
      },
    };
  };
  return (
    <StoryWrapper
      createApi={createFakeApiWithErrorOnSave}
      workspaceId="radarView"
    />
  );
};

export const WorkspaceSaveAs = (): React.ReactElement => {
  const { fetch } = useFirstTimeError();

  const createFakeApiWithError = (): PresetsApi => {
    const currentFakeApi = createFakeApi();

    return {
      ...currentFakeApi,
      saveWorkspacePresetAs: (data): Promise<string> => {
        return fetch(currentFakeApi.saveWorkspacePresetAs(data));
      },
    };
  };
  return <StoryWrapper createApi={createFakeApiWithError} />;
};

export const WorkspaceDelete = (): React.ReactElement => {
  const { fetch } = useFirstTimeError();

  const createFakeApiWithError = (): PresetsApi => {
    const currentFakeApi = createFakeApi();

    return {
      ...currentFakeApi,
      deleteWorkspacePreset: (workspaceId): Promise<void> => {
        return fetch(currentFakeApi.deleteWorkspacePreset(workspaceId));
      },
    };
  };
  return <StoryWrapper createApi={createFakeApiWithError} />;
};
