/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import {
  defaultComponentsLookUp,
  ViewError,
  ViewLoading,
} from './DefaultComponentsLookUp';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { PresetsApi } from '../../utils/api';

describe('components/DefaultComponentsLookUp/DefaultComponentsLookUp', () => {
  describe('ViewLoading', () => {
    it('should render with default props', () => {
      const props = {
        viewPresetId: 'test-1',
      };
      render(<ViewLoading {...props} />);

      expect(screen.getByText(`Loading ${props.viewPresetId}`)).toBeTruthy();
    });
  });

  describe('ViewError', () => {
    it('should render with default props', () => {
      const props = {
        viewPresetId: 'test-1',
        mosaicNodeId: 'screen1',
      };

      render(
        <WorkspaceWrapperProviderWithStore>
          <ViewError {...props} />
        </WorkspaceWrapperProviderWithStore>,
      );

      expect(
        screen.getByText(`Failed to fetch view ${props.viewPresetId}`),
      ).toBeTruthy();
    });

    it('should be able to refetch a view', async () => {
      const props = {
        viewPresetId: 'test-1',
        mosaicNodeId: 'screen1',
      };

      const mockGetViewPreset = jest.fn();
      const createApi = (): PresetsApi => {
        return {
          ...createFakeApi(),
          getViewPreset: mockGetViewPreset,
        };
      };

      render(
        <WorkspaceWrapperProviderWithStore createApi={createApi}>
          <ViewError {...props} />
        </WorkspaceWrapperProviderWithStore>,
      );
      expect(mockGetViewPreset).toHaveBeenCalledTimes(0);

      fireEvent.click(screen.getByText('TRY AGAIN'));

      await waitFor(() =>
        expect(mockGetViewPreset).toHaveBeenCalledWith(props.viewPresetId),
      );
    });
  });

  describe('defaultComponentsLookUp', () => {
    it('should return ViewLoading', () => {
      const props = {
        componentType: 'ViewLoading',
        viewPresetId: 'test-1',
        mosaicNodeId: 'screen1',
      };

      render(
        <WorkspaceWrapperProviderWithStore>
          {defaultComponentsLookUp({
            viewPresetId: props.viewPresetId,
            componentType: props.componentType,
            mosaicNodeId: props.mosaicNodeId,
          })}
        </WorkspaceWrapperProviderWithStore>,
      );
      expect(screen.getByText(`Loading ${props.viewPresetId}`)).toBeTruthy();
    });

    it('should return ViewError', () => {
      const props = {
        componentType: 'ViewError',
        viewPresetId: 'test-1',
        mosaicNodeId: 'screen1',
      };

      render(
        <WorkspaceWrapperProviderWithStore>
          {defaultComponentsLookUp({
            viewPresetId: props.viewPresetId,
            componentType: props.componentType,
            mosaicNodeId: props.mosaicNodeId,
          })}
        </WorkspaceWrapperProviderWithStore>,
      );
      expect(
        screen.getByText(`Failed to fetch view ${props.viewPresetId}`),
      ).toBeTruthy();
    });

    it('should return null', () => {
      const props = {
        componentType: '',
        viewPresetId: 'test-1',
        mosaicNodeId: 'screen1',
      };
      expect(
        defaultComponentsLookUp({
          viewPresetId: props.viewPresetId,
          componentType: props.componentType,
          mosaicNodeId: props.mosaicNodeId,
        }),
      ).toEqual(null);

      const props2 = {
        componentType: 'Map',
        viewPresetId: 'test-1',
        mosaicNodeId: 'screen1',
      };
      expect(
        defaultComponentsLookUp({
          viewPresetId: props2.viewPresetId,
          componentType: props2.componentType,
          mosaicNodeId: props2.mosaicNodeId,
        }),
      ).toEqual(null);
    });
  });
});
