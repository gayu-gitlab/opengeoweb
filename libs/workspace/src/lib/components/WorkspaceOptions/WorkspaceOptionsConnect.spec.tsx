/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { createStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import {
  fireEvent,
  render,
  screen,
  act,
  waitFor,
} from '@testing-library/react';
import { lightTheme } from '@opengeoweb/theme';
import { WorkspaceWrapperProviderWithStore } from '../Providers/Providers';
import {
  TITLE_SUFFIX_EXISTING_WORKSPACE,
  TITLE_SUFFIX_NEW_WORKSPACE,
  WorkspaceOptionsConnect,
  getTitle,
} from './WorkspaceOptionsConnect';
import {
  WorkspaceState,
  WorkspacePresetAction,
} from '../../store/workspace/types';
import { getWorkspaceData } from '../../store/workspace/selectors';
import { workspaceActions } from '../../store/workspace/reducer';
import { viewPresetActions } from '../../store/viewPresets';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { PresetsApi } from '../../utils/api';

describe('workspace/components/WorkspaceOptions/WorkspaceOptionsConnect', () => {
  const screenConfig: WorkspaceState = {
    id: 'preset1',
    title: 'Preset 1',
    scope: 'system',
    views: {
      allIds: ['screen1', 'screen2'],
      byId: {
        screen1: {
          title: 'screen 1',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
        screen2: {
          title: 'screen 2',
          componentType: 'MyTestComponent',
          initialProps: { mapPreset: [{}], syncGroupsIds: [] },
        },
      },
    },
    mosaicNode: {
      direction: 'row',
      first: 'screen1',
      second: 'screen2',
    },
  };

  describe('getTitle', () => {
    it('should return correct title', () => {
      const testTitle = 'test title';
      const testWorkspaceId = '124';

      expect(getTitle(testTitle, false, testWorkspaceId)).toEqual(testTitle);
      expect(getTitle(testTitle, true, testWorkspaceId)).toEqual(
        `${testTitle} (${TITLE_SUFFIX_NEW_WORKSPACE})`,
      );
      expect(getTitle(testTitle, true, undefined!)).toEqual(
        `${testTitle} (${TITLE_SUFFIX_EXISTING_WORKSPACE})`,
      );
    });
  });

  it('should render WorkspaceOptionsConnect', async () => {
    const props = {
      title: 'test -1',
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    expect(screen.getByText(props.title)).toBeTruthy();
    const tab = screen.queryByTestId('workspace-options-toolbutton');
    const styles = getComputedStyle(tab!);
    expect(styles.fontStyle).not.toEqual('italic');
  });

  it('should show title in italic when the workspace has changes', async () => {
    const testWorkspacePreset: WorkspaceState = {
      id: 'preset1',
      title: 'Preset 1',
      views: {
        allIds: ['screen1', 'screen2'],
        byId: {
          screen1: {
            title: 'screen 1',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
          screen2: {
            title: 'screen 2',
            componentType: 'MyTestComponent',
            initialProps: { mapPreset: [{}], syncGroupsIds: [] },
          },
        },
      },
      hasChanges: true,
      mosaicNode: {
        direction: 'row',
        first: 'screen1',
        second: 'screen2',
      },
    };
    const props = {
      title: testWorkspacePreset.title,
      workspaceId: 'preset1',
    };
    const store = createStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.setActiveViewPresetId({
          viewPresetId: 'test-1',
          panelId: 'panel-2',
        }),
      ),
    );
    const tab = screen.queryByTestId('workspace-options-toolbutton');
    const styles = getComputedStyle(tab!);
    expect(styles.fontStyle).toEqual('italic');
  });

  it('should disable save and delete option for system preset', async () => {
    const store = createStore();
    const props = {
      title: screenConfig.title,
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({ workspacePreset: screenConfig }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const deleteMenuItem = screen.getByRole('menuitem', { name: 'Delete' });
    expect(deleteMenuItem.getAttribute('aria-disabled')).toBe('true');
    const saveMenuItem = screen.getByRole('menuitem', { name: 'Save' });
    expect(saveMenuItem.getAttribute('aria-disabled')).toBe('true');
  });

  it('should enable delete option for user preset and open dialog on click', async () => {
    const store = createStore();
    const props = {
      title: screenConfig.title,
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: { ...screenConfig, scope: 'user' },
        }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const deleteMenuItem = screen.getByRole('menuitem', { name: 'Delete' });
    expect(deleteMenuItem.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(deleteMenuItem);
    await waitFor(() =>
      expect(store.getState().workspaceList.workspaceActionDialog).toEqual({
        action: WorkspacePresetAction.DELETE,
        presetId: props.workspaceId,
        formValues: { title: props.title },
        isFetching: false,
      }),
    );
  });

  it('should enable save option for user preset and save preset with changes', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });
    const props = {
      title: screenConfig.title,
      workspaceId: 'preset1',
    };
    const mockSave = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        saveWorkspacePreset: mockSave,
      };
    };
    render(
      <WorkspaceWrapperProviderWithStore
        theme={lightTheme}
        store={store}
        createApi={createApi}
      >
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: {
            ...screenConfig,
            scope: 'user',
          },
        }),
      ),
    );
    await act(() =>
      store.dispatch(
        viewPresetActions.setActiveViewPresetId({
          viewPresetId: 'test-1',
          panelId: 'panel-2',
        }),
      ),
    );
    expect(
      screen.getByText(getTitle(props.title, true, props.workspaceId)),
    ).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const saveButton = screen.getByRole('menuitem', { name: 'Save' });
    expect(saveButton.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(saveButton);
    await screen.findByText(`Workspace "${props.title}" successfully saved`);
    await waitFor(() => expect(mockSave).toHaveBeenCalled());
  });

  it('should enable save option for user preset and save even if there no are changes', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });
    const props = {
      title: screenConfig.title,
      workspaceId: 'preset1',
    };
    const mockSave = jest.fn();
    const createApi = (): PresetsApi => {
      return {
        ...createFakeApi(),
        saveWorkspacePreset: mockSave,
      };
    };
    render(
      <WorkspaceWrapperProviderWithStore
        theme={lightTheme}
        store={store}
        createApi={createApi}
      >
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: {
            ...screenConfig,
            scope: 'user',
          },
        }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const saveButton = screen.getByRole('menuitem', { name: 'Save' });
    expect(saveButton.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(saveButton);
    await screen.findByText(`Workspace "${props.title}" successfully saved`);
    await waitFor(() => expect(mockSave).toHaveBeenCalled());
  });

  it('should enable save as for user preset and open dialog click', async () => {
    const store = createStore();
    const props = {
      title: screenConfig.title,
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: { ...screenConfig, scope: 'user' },
        }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const saveAsButton = screen.getByRole('menuitem', { name: 'Save as' });
    expect(saveAsButton.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(saveAsButton);

    await waitFor(() =>
      expect(store.getState().workspaceList.workspaceActionDialog).toEqual({
        action: WorkspacePresetAction.SAVE_AS,
        presetId: props.workspaceId,
        formValues: getWorkspaceData(store.getState()),
        isFetching: false,
      }),
    );
  });

  it('should disable the workspace option button when no viewpresets in store', async () => {
    const testWorkspacePreset: WorkspaceState = {
      id: 'preset1',
      title: 'Preset 1',
      views: {
        allIds: [],
        byId: {},
      },
      hasChanges: true,
      mosaicNode: {
        direction: 'row',
        first: 'screen1',
        second: 'screen2',
      },
    };
    const props = {
      title: testWorkspacePreset.title,
      workspaceId: 'preset1',
    };
    const store = createStore();
    render(
      <WorkspaceWrapperProviderWithStore store={store} theme={lightTheme}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: testWorkspacePreset,
        }),
      ),
    );
    const tab = screen.getByRole('button');
    expect(tab.classList).toContain('Mui-disabled');
  });

  it('should disable the workspace option button when the user is not logged in', async () => {
    const store = createStore();
    const props = {
      title: 'test -1',
      workspaceId: 'preset1',
    };
    const authLoggedOutProps = {
      isLoggedIn: false, // user is not logged in
      auth: {
        username: 'user.name',
        token: '1223344',
        refresh_token: '33455214',
      },
      onLogin: (): void => null!,
      onSetAuth: (): void => null!,
      sessionStorageProvider: null!,
    };
    render(
      <WorkspaceWrapperProviderWithStore
        auth={authLoggedOutProps}
        store={store}
        theme={lightTheme}
      >
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: screenConfig,
        }),
      ),
    );
    const tab = screen.getByRole('button');
    expect(tab.classList).toContain('Mui-disabled');
  });

  it('should open dialog when pressing Save (incl. viewpresets) for user workspace', async () => {
    const store = createStore();
    const props = {
      title: 'test -1',
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: { ...screenConfig, scope: 'user' },
        }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const saveAllButton = screen.getByRole('menuitem', {
      name: 'Save (incl. viewpresets)',
    });
    expect(saveAllButton.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(saveAllButton);

    await waitFor(() =>
      expect(store.getState().workspaceList.workspaceActionDialog).toEqual({
        action: WorkspacePresetAction.SAVE_ALL,
        presetId: props.workspaceId,
        formValues: getWorkspaceData(store.getState()),
        isFetching: false,
      }),
    );
  });

  it('should open dialog when pressing Save (incl. viewpresets) for system workspace', async () => {
    const store = createStore();
    const props = {
      title: 'test -1',
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: screenConfig,
        }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const saveAllButton = screen.getByRole('menuitem', {
      name: 'Save (incl. viewpresets)',
    });
    expect(saveAllButton.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(saveAllButton);

    await waitFor(() =>
      expect(store.getState().workspaceList.workspaceActionDialog).toEqual({
        action: WorkspacePresetAction.SAVE_ALL,
        presetId: props.workspaceId,
        formValues: getWorkspaceData(store.getState()),
        isFetching: false,
      }),
    );
  });

  it('should open dialog when pressing Edit for user workspace', async () => {
    const store = createStore();
    const props = {
      title: 'test -1',
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: { ...screenConfig, scope: 'user' },
        }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const editButton = screen.getByRole('menuitem', {
      name: 'Edit (name and abstract)',
    });
    expect(editButton.getAttribute('aria-disabled')).not.toBe('true');

    fireEvent.click(editButton);
    await waitFor(() =>
      expect(store.getState().workspaceList.workspaceActionDialog).toEqual({
        action: WorkspacePresetAction.EDIT,
        presetId: props.workspaceId,
        formValues: getWorkspaceData(store.getState()),
        isFetching: false,
      }),
    );
  });

  it('should disable Edit for system presets', async () => {
    const store = createStore();
    const props = {
      title: 'test -1',
      workspaceId: 'preset1',
    };
    render(
      <WorkspaceWrapperProviderWithStore theme={lightTheme} store={store}>
        <WorkspaceOptionsConnect {...props} />
      </WorkspaceWrapperProviderWithStore>,
    );
    await act(() =>
      store.dispatch(
        workspaceActions.setPreset({
          workspacePreset: screenConfig,
        }),
      ),
    );
    expect(screen.getByText(props.title)).toBeTruthy();

    expect(screen.getByRole('button')).toBeTruthy();
    fireEvent.click(screen.getByRole('button'));
    expect(screen.getByRole('menu')).toBeTruthy();
    const editButton = screen.getByRole('menuitem', {
      name: 'Edit (name and abstract)',
    });
    expect(editButton.getAttribute('aria-disabled')).toBe('true');
  });
});
