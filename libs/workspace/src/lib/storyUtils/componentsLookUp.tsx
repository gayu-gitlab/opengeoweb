/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  componentsLookUp as coreComponentsLookUp,
  ComponentsLookUpPayload as CoreComponentsLookUpPayload,
} from '@opengeoweb/core';
import {
  componentsLookUp as timeseriesComponentsLookUp,
  ComponentsLookUpPayload as TimeseriesComponentsLookUpPayload,
} from '@opengeoweb/timeseries';
import {
  TafModule,
  ComponentsLookUpPayload as TafComponentsLookUpPayload,
} from '@opengeoweb/taf';
import {
  MetInfoWrapper,
  SigmetComponentsLookUpPayload,
  AirmetComponentsLookUpPayload,
  sigmetConfig,
  airmetConfig,
} from '@opengeoweb/sigmet-airmet';
import {
  componentsLookUp as warningListComponentsLookUp,
  ComponentsLookUpPayload as WarningListComponentsLookUpPayLoad,
} from '@opengeoweb/warnings';
import { ErrorBoundary } from '@opengeoweb/shared';
import ActionExampleComponent from './ActionsExampleComponent';
import { ComponentsLookUpWrapper } from '../components/ComponentsLookUpWrapper';

// taf module without API
const tafComponentsLookUp = (
  payload: TafComponentsLookUpPayload,
): React.ReactElement => {
  const { componentType } = payload;

  switch (componentType) {
    case 'TafModule': {
      return (
        <ComponentsLookUpWrapper component={TafModule} payload={payload} />
      );
    }
    default:
      return null!;
  }
};

// sigmet/airmet module without API
const sigmetAirmetComponentsLookUp = (
  payload: SigmetComponentsLookUpPayload | AirmetComponentsLookUpPayload,
): React.ReactElement => {
  const { componentType } = payload;

  switch (componentType) {
    case 'SigmetModule': {
      return (
        <ComponentsLookUpWrapper
          component={MetInfoWrapper}
          payload={payload}
          productType="sigmet"
          productConfig={sigmetConfig}
        />
      );
    }
    case 'AirmetModule': {
      return (
        <ComponentsLookUpWrapper
          component={MetInfoWrapper}
          payload={payload}
          productType="airmet"
          productConfig={airmetConfig}
        />
      );
    }
    default:
      return null!;
  }
};

const ErrorExampleComponent: React.FC = () => {
  throw new Error('Dummy error to check if ErrorBoundary works');
};

/**
 * The lookup table is for registering your own components with the workspace.
 * @param payload
 * @returns
 */
export const componentsLookUp = (
  payload:
    | CoreComponentsLookUpPayload
    | TimeseriesComponentsLookUpPayload
    | TafComponentsLookUpPayload
    | SigmetComponentsLookUpPayload
    | AirmetComponentsLookUpPayload
    | WarningListComponentsLookUpPayLoad,
): React.ReactElement => {
  const coreComponent = coreComponentsLookUp(
    payload as CoreComponentsLookUpPayload,
  );
  const timeseriesComponent = timeseriesComponentsLookUp(
    payload as TimeseriesComponentsLookUpPayload,
  );

  const tafComponent = tafComponentsLookUp(
    payload as TafComponentsLookUpPayload,
  );

  const sigmetComponent = sigmetAirmetComponentsLookUp(
    payload as SigmetComponentsLookUpPayload,
  );

  const airmetComponent = sigmetAirmetComponentsLookUp(
    payload as SigmetComponentsLookUpPayload,
  );

  const warningListComponent = warningListComponentsLookUp(
    payload as WarningListComponentsLookUpPayLoad,
  );

  interface CustomPanelLookupProps {
    componentType: string;
    id: string;
  }

  const customPanelLookup = ({
    componentType,
    id,
  }: CustomPanelLookupProps): React.ReactElement => {
    switch (componentType) {
      case 'ErrorExample':
        return (
          <ErrorBoundary>
            <ErrorExampleComponent />
          </ErrorBoundary>
        );

      case 'ActionsExample':
        return <ActionExampleComponent viewId={id} />;

      default:
        return null!;
    }
  };

  const customPanelComponent = customPanelLookup(payload);

  return (
    coreComponent ||
    timeseriesComponent ||
    tafComponent ||
    sigmetComponent ||
    airmetComponent ||
    warningListComponent ||
    customPanelComponent
  );
};
