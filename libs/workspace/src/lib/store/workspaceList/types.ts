/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { EntityState } from '@reduxjs/toolkit';
import {
  WorkspacePresetListItem,
  WorkspacePresetAction,
  PresetScope,
  WorkspacePresetFromBE,
  WorkspacePreset,
} from '../workspace/types';

export enum WorkspaceListErrorType {
  GENERIC = 'GENERIC',
}

export type WorkspaceListError =
  | { type: WorkspaceListErrorType; message: string }
  | undefined;

export interface WorkspaceListFilter {
  label: string;
  id: string;
  type: 'scope' | 'keyword';
  isSelected?: boolean;
  isDisabled?: boolean;
}

export interface FetchWorkspaceParams {
  scope?: string;
  search?: string;
}

export type WorkspaceListEntityState = EntityState<WorkspacePresetListItem>;

export type WorkspacePresetFormValues =
  | {
      title?: string;
      abstract?: string;
      scope?: PresetScope;
      id?: string;
    }
  | WorkspacePresetFromBE;

export interface WorkspaceActionDialogType {
  action: WorkspacePresetAction;
  presetId: string;
  formValues: WorkspacePresetFormValues;
  error?: WorkspaceListError;
  isFetching?: boolean;
}

export interface WorkspaceListState extends WorkspaceListEntityState {
  isFetching: boolean;
  error: WorkspaceListError;
  isWorkspaceListDialogOpen: boolean;
  workspaceActionDialog?: WorkspaceActionDialogType;
  searchQuery?: string;
  workspaceListFilters?: WorkspaceListFilter[];
}

export type FetchWorkspaceList = FetchWorkspaceParams;

export interface FetchedWorkspaceList {
  workspaceList: WorkspacePresetListItem[];
}

export interface ErrorWorkspaceList {
  error: WorkspaceListError;
}

export interface ToggleListDialogPayload {
  isWorkspaceListDialogOpen: boolean;
}

export interface ToggleSelectFilterChip {
  id: string;
  isSelected: boolean;
}

export interface SearchWorkspaceFilter {
  searchQuery: string;
}

export type OpenWorkspaceActionDialogPayload = WorkspaceActionDialogType;

export interface SubmitFormWorkspaceActionDialogPayload {
  presetAction: WorkspacePresetAction;
  presetId: string;
  data: WorkspacePreset | WorkspacePresetFromBE;
}

export interface OnSuccesWorkspacePresetActionPayload {
  workspaceId: string;
  action: WorkspacePresetAction;
  title: string;
  abstract?: string;
  currentActiveWorkspaceId?: string;
}

export interface OnErrorWorkspacePresetActionPayload {
  error: WorkspaceListError;
}
