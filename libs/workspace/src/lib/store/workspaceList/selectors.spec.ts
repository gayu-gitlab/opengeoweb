/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { WorkspacePresetAction } from '../workspace/types';
import { AppStore } from '../store';
import * as viewPresetSelectors from './selectors';
import { WorkspaceListErrorType } from './types';

describe('store/workspaceList/selectors', () => {
  describe('getWorkspaceListStore', () => {
    it('should return the viewstore', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: false,
          isWorkspaceListDialogOpen: false,
          entities: {},
          ids: [],
        },
      };

      expect(viewPresetSelectors.getWorkspaceListStore(mockStore)).toEqual(
        mockStore.workspaceList,
      );

      expect(viewPresetSelectors.getWorkspaceListStore(null!)).toBeNull();
    });
  });

  describe('getWorkspaceList', () => {
    it('should return workspaceList', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: false,
          isWorkspaceListDialogOpen: false,
          entities: {
            preset: {
              id: 'mapPreset-1',
              scope: 'system',
              title: 'Layer manager preset',
              date: '2022-06-01T12:34:27.787192',
              viewType: 'singleWindow',
              abstract: '',
            },
            preset_2: {
              id: 'mapPreset-2',
              scope: 'system',
              title: 'Layer manager preset 2',
              date: '2022-06-01T12:34:27.787192',
              viewType: 'singleWindow',
              abstract: '',
            },
          },
          ids: ['preset', 'preset_2'],
        },
      };

      expect(viewPresetSelectors.getWorkspaceList(mockStore)).toEqual(
        mockStore.workspaceList!.ids.map(
          (id) => mockStore.workspaceList!.entities[id],
        ),
      );
    });

    it('should return empty list when no workspaceList in store', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: false,
          isWorkspaceListDialogOpen: false,
          entities: {},
          ids: [],
        },
      };

      expect(viewPresetSelectors.getWorkspaceList(mockStore)).toEqual([]);
    });
  });

  describe('getIsWorkspaceListFetching', () => {
    it('should return isFetching', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: true,
          isWorkspaceListDialogOpen: false,
          entities: {
            preset: {
              id: 'mapPreset-1',
              scope: 'system',
              title: 'Layer manager preset',
              date: '2022-06-01T12:34:27.787192',
              viewType: 'singleWindow',
              abstract: '',
            },
            preset_2: {
              id: 'mapPreset-2',
              scope: 'system',
              title: 'Layer manager preset 2',
              date: '2022-06-01T12:34:27.787192',
              viewType: 'singleWindow',
              abstract: '',
            },
          },
          ids: ['preset', 'preset_2'],
        },
      };

      expect(
        viewPresetSelectors.getIsWorkspaceListFetching(mockStore),
      ).toBeTruthy();
    });
    it('should return isFetching false if store is not set', () => {
      const mockStore: AppStore = {};

      expect(
        viewPresetSelectors.getIsWorkspaceListFetching(mockStore),
      ).toBeFalsy();
    });
  });

  describe('getWorkspaceActionDialogDetails', () => {
    it('should return false if store is not set', () => {
      const mockStore: AppStore = {};

      expect(
        viewPresetSelectors.getWorkspaceActionDialogDetails(mockStore),
      ).toBeFalsy();
    });
    it('should return false if no details set', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: true,
          isWorkspaceListDialogOpen: false,
          entities: {
            preset: {
              id: 'mapPreset-1',
              scope: 'system',
              title: 'Layer manager preset',
              date: '2022-06-01T12:34:27.787192',
              viewType: 'singleWindow',
              abstract: '',
            },
          },
          ids: ['preset'],
        },
      };

      expect(
        viewPresetSelectors.getWorkspaceActionDialogDetails(mockStore),
      ).toBeFalsy();
    });

    it('should return dialog details if set', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: true,
          isWorkspaceListDialogOpen: false,
          workspaceActionDialog: {
            action: WorkspacePresetAction.DELETE,
            presetId: 'someId',
            formValues: { title: 'Workspace 1' },
          },
          entities: {
            preset: {
              id: 'mapPreset-1',
              scope: 'system',
              title: 'Layer manager preset',
              date: '2022-06-01T12:34:27.787192',
              viewType: 'singleWindow',
              abstract: '',
            },
          },
          ids: ['preset'],
        },
      };

      expect(
        viewPresetSelectors.getWorkspaceActionDialogDetails(mockStore),
      ).toBe(mockStore.workspaceList!.workspaceActionDialog);
    });
  });

  describe('getWorkspaceListError', () => {
    it('should return error', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: {
            type: WorkspaceListErrorType.GENERIC,
            message: 'Failed to fetch workspace list',
          },
          isFetching: true,
          isWorkspaceListDialogOpen: false,
          entities: {},
          ids: [],
        },
      };

      expect(viewPresetSelectors.getWorkspaceListError(mockStore)).toEqual(
        mockStore.workspaceList!.error,
      );
    });
    it('should return undefined if store is not set', () => {
      const mockStore: AppStore = {};

      expect(
        viewPresetSelectors.getWorkspaceListError(mockStore),
      ).toBeUndefined();
    });
  });

  describe('getIsWorkspaceListDialogOpen', () => {
    it('should isListDialogOpen for closed listDialog', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: true,
          isWorkspaceListDialogOpen: false,
          entities: {},
          ids: [],
        },
      };

      expect(
        viewPresetSelectors.getIsWorkspaceListDialogOpen(mockStore),
      ).toBeFalsy();
    });
    it('should isListDialogOpen for open listDialog', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: true,
          isWorkspaceListDialogOpen: true,
          entities: {},
          ids: [],
        },
      };

      expect(
        viewPresetSelectors.getIsWorkspaceListDialogOpen(mockStore),
      ).toBeTruthy();
    });
    it('should return false if store is not set', () => {
      const mockStore: AppStore = {};

      expect(
        viewPresetSelectors.getIsWorkspaceListDialogOpen(mockStore),
      ).toBeFalsy();
    });
  });

  describe('getWorkspaceListFilters', () => {
    it('should return [] if workspaceListFilters not set', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: true,
          isWorkspaceListDialogOpen: false,
          entities: {},
          ids: [],
        },
      };

      expect(
        viewPresetSelectors.getWorkspaceListFilters(mockStore),
      ).toStrictEqual([]);
    });
    it('should return filters if set', () => {
      const mockStore: AppStore = {
        workspaceList: {
          error: undefined,
          isFetching: true,
          isWorkspaceListDialogOpen: true,
          entities: {},
          ids: [],
          workspaceListFilters: [
            { id: 'system', label: 'system', type: 'scope', isSelected: true },
            { id: 'user', label: 'user', type: 'scope', isSelected: true },
          ],
        },
      };

      expect(viewPresetSelectors.getWorkspaceListFilters(mockStore)).toBe(
        mockStore.workspaceList!.workspaceListFilters,
      );
    });

    it('should return empty array if store is not set', () => {
      const mockStore: AppStore = {};

      expect(
        viewPresetSelectors.getWorkspaceListFilters(mockStore),
      ).toStrictEqual([]);
    });
  });
});
