/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { Egg } from '@redux-eggs/core';
import { Store } from '@reduxjs/toolkit';

import { reducer as workspace } from './workspace/reducer';
import { reducer as workspaceList } from './workspaceList/reducer';
import { reducer as viewPresetsList } from './viewPresetsList/reducer';
import { reducer as viewPresets } from './viewPresets/reducer';
import workspaceSaga from './workspace/sagas';
import workspaceListSaga from './workspaceList/sagas';
import mapPresetsSagas from './viewPresets/sagas';

import { WorkspaceState } from './workspace/types';
import { WorkspaceListState } from './workspaceList/types';
import { ViewPresetListState } from './viewPresetsList/types';
import { ViewPresetState } from './viewPresets/types';

export interface WorkspaceModuleStore {
  workspace?: WorkspaceState;
  workspaceList?: WorkspaceListState;
  viewPresetsList?: ViewPresetListState;
  viewPresets?: ViewPresetState;
}

const workspaceModuleConfig: Egg<Store<WorkspaceModuleStore>> = {
  id: 'workspace-module',
  reducersMap: {
    workspace,
    workspaceList,
    viewPresetsList,
    viewPresets,
  },
  sagas: [workspaceSaga, workspaceListSaga, mapPresetsSagas],
};

export default workspaceModuleConfig;
