/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { darkTheme } from '@opengeoweb/theme';

import React from 'react';

import { StoryWrapper } from './storyUtils/StoryWrapper';

export default {
  title: 'components/Workspace/Workspace Demo',
};

export const WorkspaceDemoLight = (): React.ReactElement => {
  return <StoryWrapper />;
};

export const WorkspaceDemoDark = (): React.ReactElement => {
  return <StoryWrapper theme={darkTheme} />;
};

export const WorkspaceDemoLightSnapshot = (): React.ReactElement => {
  return <StoryWrapper isSnapshot shouldShowListOnOpen />;
};

export const WorkspaceDemoDarkSnapshot = (): React.ReactElement => {
  return <StoryWrapper theme={darkTheme} isSnapshot shouldShowListOnOpen />;
};

export const WorkspaceDemoLoggedOut = (): React.ReactElement => {
  return <StoryWrapper theme={darkTheme} isSnapshot isLoggedIn={false} />;
};

WorkspaceDemoLightSnapshot.storyName = 'WorkspaceDemoLight (takeSnapshot)';
WorkspaceDemoLightSnapshot.parameters = {
  zeplinLink: [
    {
      name: 'WorkspaceDemoLight',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6409aa1dc12bb51e9dd40028/version/64182a5790af4e21a023f98e',
    },
  ],
};

WorkspaceDemoDarkSnapshot.storyName = 'WorkspaceDemoDark (takeSnapshot)';
