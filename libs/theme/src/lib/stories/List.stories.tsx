/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  Box,
  Button,
  Card,
  List as MuiList,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Typography,
} from '@mui/material';

import { StoryWrapper } from './StoryWrapper';
import { ArrowUp, DrawRegion, Expand, Options } from '../components/Icons';

export default {
  title: 'demo/List',
};

const ListDemo: React.FC = () => {
  return (
    <Box>
      <Box
        sx={{
          padding: 2,
          width: 600,
        }}
        component="div"
      >
        <Typography>Simple list</Typography>
        <MuiList>
          <ListItem divider>item</ListItem>
          <ListItem divider>item</ListItem>
          <ListItem>item</ListItem>
        </MuiList>
      </Box>
      <Box
        sx={{
          padding: 2,
          width: 600,
        }}
        component="div"
      >
        <Typography>List with buttons</Typography>
        <MuiList disablePadding>
          <ListItem disablePadding>
            <ListItemButton>
              <ListItemIcon>
                <ArrowUp />
              </ListItemIcon>
              <ListItemText
                primary="Photos"
                secondary="Ut non placerat libero, a auctor metus. In ante arcu, tempor ac eleifend in, imperdiet ac lacus. "
              />
            </ListItemButton>
          </ListItem>
          <ListItem disablePadding>
            <ListItemButton selected>
              <ListItemIcon>
                <DrawRegion />
              </ListItemIcon>
              <ListItemText
                primary="Work"
                secondary="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non placerat libero, a auctor metus. In ante arcu, tempor ac eleifend in, imperdiet ac lacus."
              />
              <Button variant="flat">
                <Options />
              </Button>
            </ListItemButton>
          </ListItem>

          <ListItem disablePadding>
            <ListItemButton>
              <ListItemIcon>
                <Expand />
              </ListItemIcon>
              <ListItemText
                primary="Vacation"
                secondary="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut non placerat libero, a auctor metus. In ante arcu, tempor ac eleifend in, imperdiet ac lacus. "
              />
            </ListItemButton>
          </ListItem>
        </MuiList>
      </Box>
    </Box>
  );
};

export const ListLight = (): React.ReactElement => (
  <StoryWrapper>
    <Card elevation={0}>
      <ListDemo />
    </Card>
  </StoryWrapper>
);
ListLight.storyName = 'List light theme (takeSnapshot)';

ListLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5eea1ddffa198a4932b6977a',
    },
  ],
};

export const ListDark = (): React.ReactElement => (
  <StoryWrapper isDarkTheme>
    <Card elevation={0}>
      <ListDemo />
    </Card>
  </StoryWrapper>
);
ListDark.storyName = 'List dark theme (takeSnapshot)';

ListDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e69240e0eb31b66b4b17',
    },
  ],
};
