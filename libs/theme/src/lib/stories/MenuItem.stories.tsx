/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  ListItemIcon,
  Paper,
  MenuItem as MuiMenuItem,
  Typography,
} from '@mui/material';
import { Add } from '../components/Icons';
import { StoryWrapper } from './StoryWrapper';

export default {
  title: 'demo/MenuItem',
};

const MenuItemDemo: React.FC = () => (
  <Paper sx={{ width: '200px' }}>
    <MuiMenuItem divider>
      <Typography>Divider</Typography>
    </MuiMenuItem>
    <MuiMenuItem>
      <Typography>Text</Typography>
    </MuiMenuItem>
    <MuiMenuItem selected>
      <Typography>Selected</Typography>
    </MuiMenuItem>
    <MuiMenuItem>
      <ListItemIcon>
        <Add />
      </ListItemIcon>
      <Typography>Icon</Typography>
    </MuiMenuItem>
    <MuiMenuItem selected>
      <ListItemIcon>
        <Add />
      </ListItemIcon>
      <Typography>Icon selected</Typography>
    </MuiMenuItem>
  </Paper>
);

export const MenuItem = (): React.ReactElement => (
  <StoryWrapper>
    <MenuItemDemo />
  </StoryWrapper>
);
MenuItem.storyName = 'MenuItem light theme (takeSnapshot)';

MenuItem.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5eea2192ad7fd427b72f8c59',
    },
  ],
};

export const MenuItemDark = (): React.ReactElement => (
  <StoryWrapper isDarkTheme>
    <MenuItemDemo />
  </StoryWrapper>
);
MenuItemDark.storyName = 'MenuItem dark theme (takeSnapshot)';

MenuItemDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e7b9ac882b07e65e5d74',
    },
  ],
};
