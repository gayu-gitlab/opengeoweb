/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  Accordion as MuiAccordion,
  AccordionSummary as MuiAccordionSummary,
  AccordionDetails as MuiAccordionDetails,
  Box,
} from '@mui/material';

import { ChevronDown } from '../components/Icons';
import { ThemeWrapper } from '../components/Theme';
import { darkTheme } from '../utils/themes';

export default {
  title: 'demo/Accordion',
};

const AccordionDemo: React.FC = () => {
  return (
    <Box sx={{ width: '300px' }}>
      <MuiAccordion defaultExpanded={true} disableGutters={true}>
        <MuiAccordionSummary expandIcon={<ChevronDown />}>
          Expanded Accordion
        </MuiAccordionSummary>
        <MuiAccordionDetails>Accordion Details</MuiAccordionDetails>
      </MuiAccordion>
      <MuiAccordion defaultExpanded={false} disableGutters={true}>
        <MuiAccordionSummary expandIcon={<ChevronDown />}>
          Collapsed Accordion
        </MuiAccordionSummary>
        <MuiAccordionDetails>Accordion Details</MuiAccordionDetails>
      </MuiAccordion>
    </Box>
  );
};

export const AccordionLight = (): React.ReactElement => (
  <ThemeWrapper>
    <AccordionDemo />
  </ThemeWrapper>
);

AccordionLight.storyName = 'Accordion light theme (takeSnapshot)';

export const AccordionDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <AccordionDemo />
  </ThemeWrapper>
);

AccordionDark.storyName = 'Accordion dark theme (takeSnapshot)';
