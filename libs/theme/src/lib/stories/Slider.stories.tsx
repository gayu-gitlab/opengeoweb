/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Box, Card, Slider as MuiSlider } from '@mui/material';
import { StoryWrapper } from './StoryWrapper';

export default {
  title: 'demo/Slider',
};

// TODO: add more variants when implementing new slidersContainer
const SliderDemo: React.FC = () => {
  return (
    <Box
      sx={{
        width: 300,
        padding: 2,
      }}
    >
      <MuiSlider size="small" min={0} max={100} defaultValue={50} />
      <MuiSlider size="small" min={0} max={100} disabled defaultValue={50} />
    </Box>
  );
};

const SliderDemoWrapper: React.FC = () => (
  <Card elevation={0}>
    <SliderDemo />
  </Card>
);

export const SliderLight = (): React.ReactElement => (
  <StoryWrapper>
    <SliderDemoWrapper />
  </StoryWrapper>
);
SliderLight.storyName = 'Slider light theme (takeSnapshot)';

SliderLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6005c2ed83d29613673c3c3a',
    },
  ],
};

export const SliderDark = (): React.ReactElement => (
  <StoryWrapper isDarkTheme>
    <SliderDemoWrapper />
  </StoryWrapper>
);
SliderDark.storyName = 'Slider dark theme (takeSnapshot)';

SliderDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e7bea9b61535e5ca77ca',
    },
  ],
};
