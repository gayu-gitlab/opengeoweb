/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Card, Typography, Grid } from '@mui/material';

import { StoryWrapper } from './StoryWrapper';

export default {
  title: 'demo/Card',
};

const cardStyle = {
  height: '300px',
  width: '300px',
  padding: 2,
  display: 'inline-block',
  margin: 2,
};

const styles = {
  card: cardStyle,
  disabledCard: {
    ...cardStyle,
    backgroundColor: 'geowebColors.cards.cardContainerDisabled',
    borderColor: 'geowebColors.cards.cardContainerDisabledBorder',
  },
  mouseoverDisabledCard: {
    ...cardStyle,
    backgroundColor: 'geowebColors.cards.cardContainerDisabledMouseOver',
    borderColor: 'geowebColors.cards.cardContainerDisabledBorder',
  },
};

const CardDemo: React.FC = () => {
  return (
    <Grid container>
      <Card variant="outlined" elevation={0} sx={styles.card}>
        <Typography>with border (outlined)</Typography>
      </Card>
      <Card elevation={0} sx={styles.card}>
        <Typography>without border</Typography>
      </Card>
      <Card variant="outlined" elevation={0} sx={styles.disabledCard}>
        <Typography>disabled with border (outlined)</Typography>
      </Card>
      <Card elevation={0} sx={styles.disabledCard}>
        <Typography>disabled without border</Typography>
      </Card>
      <Card variant="outlined" elevation={0} sx={styles.mouseoverDisabledCard}>
        <Typography>mouseover disabled with border (outlined)</Typography>
      </Card>
    </Grid>
  );
};

export const CardsLight = (): React.ReactElement => (
  <StoryWrapper>
    <CardDemo />
  </StoryWrapper>
);

CardsLight.storyName = 'Card light theme (takeSnapshot)';
CardsLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5ecf9271ed18a9466d4e2e50',
    },
  ],
};

export const CardsDark = (): React.ReactElement => (
  <StoryWrapper isDarkTheme>
    <CardDemo />
  </StoryWrapper>
);

CardsDark.storyName = 'Card dark theme (takeSnapshot)';
CardsDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6093e6930d8bc230b183870c',
    },
  ],
};
