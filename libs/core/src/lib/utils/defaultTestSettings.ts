/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  LayerType,
  WMLayer,
  mockGetCapabilities,
  webmapTestSettings,
} from '@opengeoweb/webmap';
import { layerTypes } from '@opengeoweb/store';
import { layerSelectTypes } from '@opengeoweb/layer-select';

export const {
  multiDimensionLayer,
  WmdefaultReduxLayerRadarKNMI,
  defaultReduxLayerRadarKNMI,
  defaultReduxLayerRadarColor,
  multiDimensionLayer3,
} = webmapTestSettings;

export const flDimensionLayer: layerTypes.Layer = {
  service: 'https://testservice',
  id: 'multiDimensionLayerMock',
  name: 'MULTI_DIMENSION_LAYER',
  title: 'MULTI_DIMENSION_LAYER',
  layerType: LayerType.mapLayer,
  enabled: true,
  dimensions: [
    {
      name: 'flight level',
      units: 'hft',
      currentValue: '625',
      values: '25,325,625',
    },
    { name: 'time', units: 'ISO8601', currentValue: '2020-03-13T14:40:00Z' },
  ],
};

export const layerWithSingleDimensionValue = new WMLayer({
  service: 'https://testservice',
  id: 'singleDimensionValueMock',
  name: 'LAYER_WITH_ONE_VALUE',
  title: 'LAYER_WITH_ONE_VALUE',
  layerType: LayerType.mapLayer,
  enabled: true,
  dimensions: [
    {
      name: 'reference_time',
      units: 'ISO8601',
      currentValue: '2020-03-13T14:40:00Z',
      values: '2020-03-13T14:40:00Z',
    },
  ],
});

export const WmMultiDimensionLayer = new WMLayer({
  service: 'https://testservice',
  id: 'multiDimensionLayerMock',
  name: 'MULTI_DIMENSION_LAYER',
  title: 'MULTI_DIMENSION_LAYER',
  layerType: LayerType.mapLayer,
  enabled: true,
  dimensions: [
    {
      name: 'flight level',
      units: 'hft',
      currentValue: '625',
      values: '25,325,625',
    },
    {
      name: 'elevation',
      units: 'meters',
      currentValue: '9000',
      values: '1000,5000,9000',
    },
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2020-03-13T14:40:00Z',
      values: '2020-03-13T10:40:00Z/2020-03-13T14:40:00Z/PT5M',
    },
  ],
});

export const multiDimensionLayer2: layerTypes.Layer = {
  service: 'https://testservicedimensions',
  id: 'multiDimensionLayerMock2',
  name: 'netcdf_5dims',
  layerType: LayerType.mapLayer,
  dimensions: [
    {
      name: 'member',
      units: 'member number',
      currentValue: 'member1',
      values: 'member1,member2,member3,member4',
    },
    {
      name: 'elevation',
      units: 'meters',
      currentValue: '9000',
      values: '1000,5000,9000',
    },
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2017-01-01T00:25:00Z',
    },
  ],
};

export const WmMultiDimensionLayer3 = new WMLayer({
  service: 'https://testservicedimensions',
  id: 'multiDimensionLayerMock2',
  name: 'netcdf_5dims',
  layerType: LayerType.mapLayer,
  dimensions: [
    {
      name: 'member',
      units: 'member number',
      currentValue: 'member1',
      values: 'member1,member2,member3,member4',
    },
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2017-01-01T00:25:00Z',
    },
  ],
});

export const defaultTestServices: layerSelectTypes.LayerSelectService[] = [
  {
    name: 'Mock URL A for default service',
    url: mockGetCapabilities.MOCK_URL_DEFAULT,
    id: 'MOCK_URL_DEFAULT',
  },
  {
    name: 'Mock URL B for default service',
    url: mockGetCapabilities.MOCK_URL_DEFAULT2,
    id: 'MOCK_URL_DEFAULT2',
  },
];
