/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

const translations = {
  en: {
    translation: {
      greeting: 'Good morning',
      greetingWithName: 'Good morning {{name}}!',
      delete: 'Press to delete',
      deleteOK: 'DELETED',
      restore: 'Press to restore',
    },
  },
  nl: {
    translation: {
      greeting: 'Goedemorgen',
      greetingWithName: 'Goedemorgen {{name}}!',
      delete: 'Klik om te verwijderen',
      deleteOK: 'VERWIJDERD',
      restore: 'Druk op om te herstellen',
    },
  },
  fi: {
    translation: {
      greeting: 'Hyvää huomenta',
      greetingWithName: 'Hyvää huomenta {{name}}!',
      delete: 'Paina poistaaksesi',
      deleteOK: 'POISTETAAN',
      restore: 'Palauta painamalla',
    },
  },
  no: {
    translation: {
      greeting: 'God morgen',
      greetingWithName: 'God morgen {{name}}!',
      delete: 'Trykk for å slette',
      deleteOK: 'SLETTET',
      restore: 'Trykk for å gjenopprette',
    },
  },
};

export default translations;
