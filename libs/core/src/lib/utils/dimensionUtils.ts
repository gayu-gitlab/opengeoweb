/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { WMJSDimension } from '@opengeoweb/webmap';
import { dimensionConfig } from '../components/MultiMapDimensionSelect/MultiDimensionSelectConfig';

export interface Mark {
  value: number | string;
  label?: React.ReactNode;
}

export const marksByDimension = (dim: WMJSDimension): Mark[] => {
  if (!dim || !dim.name) {
    return [];
  }
  const dimCnf = dimensionConfig.find((cnf) => cnf.name === dim.name);
  const defaultUnit = dimCnf ? dimCnf.defaultUnit : '';
  const marks: Mark[] = [];
  if (dim && dim.size) {
    for (let i = 0; i < dim.size(); i += 1) {
      marks.push({
        value:
          Number(dim.getValueForIndex(i)) ||
          (dim.getValueForIndex(i) as string),
        label: `${Number(dim.getValueForIndex(i)) || dim.getValueForIndex(i)} ${
          dim.unitSymbol ? dim.unitSymbol : defaultUnit
        }`,
      });
    }
  }
  return marks;
};
