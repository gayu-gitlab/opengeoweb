/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import {
  WebMapStateModuleState,
  mapTypes,
  storeTestUtils,
} from '@opengeoweb/store';

export const { mockStateMapWithDimensions } = storeTestUtils;

const webmapStateWithDimensions = (
  mapId: string,
  dimensions?: mapTypes.Dimension[],
): mapTypes.WebMapState => {
  const webmap = storeTestUtils.createWebmapState(mapId);

  // Add the dimensions to the map state
  if (dimensions && dimensions.length) {
    dimensions.forEach((dimension) => {
      webmap.byId[mapId].dimensions!.push(dimension);
    });
  }
  return webmap;
};

export const mockStateMapWithDimensionsWithoutLayers = (
  mapId: string,
): WebMapStateModuleState => ({
  webmap: webmapStateWithDimensions(mapId, [
    {
      name: 'time',
      units: 'ISO8601',
      currentValue: '2017-01-01T00:25:00Z',
    },
  ]),
});
