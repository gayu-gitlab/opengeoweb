/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { getCapabilities, mockGetCapabilities } from '@opengeoweb/webmap';
import WMSLayerTree from './WMSLayerTree';

describe('src/components/WMSLoader/WMSLayerTree/WMSLayerTree', () => {
  jest
    .spyOn(getCapabilities, 'getLayersFromService')
    .mockImplementation(mockGetCapabilities.mockGetLayersFromService);

  it('should show a menu item for each layer in the tree', async () => {
    const mockProps = {
      service: {
        name: 'KNMI Radar',
        url: mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
        id: 'knmi-radar',
      },
      onClickLayer: jest.fn(),
      highlightedLayers: [],
    };
    render(<WMSLayerTree {...mockProps} />);
    const list = await screen.findAllByTestId('selectableLayer');

    expect(list).toBeTruthy();

    const { children } = mockGetCapabilities.mockLayersWithChildren;

    expect(list.length).toEqual(children.length);
    list.forEach((li) =>
      expect(
        children.find((child) => child.title === li.textContent),
      ).toBeTruthy(),
    );
  });

  it('should show a message if there are no layers', async () => {
    const mockProps = {
      service: {
        name: 'KNMI Radar',
        url: mockGetCapabilities.MOCK_URL_NO_CHILDREN,
        id: 'knmi-radar',
      },
      onClickLayer: jest.fn(),
      highlightedLayers: [],
    };
    render(<WMSLayerTree {...mockProps} />);
    const message = await screen.findByTestId('message');
    expect(message.textContent).toEqual('No data');

    // Button to reload layers from service should not be shown
    expect(screen.queryByTestId('reloadLayers')).toBeFalsy();
  });

  it('should show a message if loading layers from the service fails', async () => {
    const mockProps = {
      service: {
        name: 'KNMI Radar',
        url: mockGetCapabilities.MOCK_URL_INVALID,
        id: 'knmi-radar',
      },
      onClickLayer: jest.fn(),
      highlightedLayers: [],
    };
    render(<WMSLayerTree {...mockProps} />);
    const message = await screen.findByTestId('message');

    expect(message.textContent).toEqual('No data');

    // Button to reload layers from service should not be shown
    expect(screen.queryByTestId('reloadLayers')).toBeFalsy();
  });

  it('should show a spinner while loading the layers', async () => {
    const mockProps = {
      service: {
        name: 'KNMI Radar',
        url: mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
        id: 'knmi-radar',
      },
      onClickLayer: jest.fn(),
      highlightedLayers: [],
    };
    render(<WMSLayerTree {...mockProps} />);

    // while loading the loading state should be shown
    expect(screen.getByRole('progressbar')).toBeTruthy();

    // when loading is completed the loadingBar should be gone
    const list = await screen.findAllByTestId('selectableLayer');
    expect(list).toBeTruthy();
    expect(screen.queryByRole('progressbar')).toBeFalsy();
  });

  it('should trigger onClickLayer when clicking a layer in the list and highlight it', async () => {
    const mockProps = {
      service: {
        name: 'KNMI Radar',
        url: mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
        id: 'knmi-radar',
      },
      onClickLayer: jest.fn(),
      highlightedLayers: [],
    };

    render(<WMSLayerTree {...mockProps} />);
    const list = await screen.findAllByTestId('selectableLayer');

    expect(list).toBeTruthy();

    fireEvent.click(list[0]);

    expect(mockProps.onClickLayer).toHaveBeenCalledTimes(1);
    // check if layer is highlighted after clicking it
    expect(screen.getAllByTestId('selectableLayer')[0].classList).toContain(
      'Mui-selected',
    );
  });

  it('should show the subcategory as sticky header when there is a subcategory', async () => {
    const mockProps = {
      service: {
        name: 'KNMI Radar',
        url: mockGetCapabilities.MOCK_URL_WITH_SUBCATEGORY,
        id: 'knmi-radar',
      },
      onClickLayer: jest.fn(),
      highlightedLayers: [],
    };

    const expectedCategories = [{ title: 'Subcategory' }, { title: 'Klima' }];

    const expectedMenuItems = [
      {
        title: 'Cloud cover (flag)',
      },
      {
        title: 'Baselayer (-)',
      },
      {
        title: 'HAIC Airports',
      },
    ];

    const { baseElement } = render(<WMSLayerTree {...mockProps} />);
    const list = await screen.findAllByTestId('selectableLayer');

    // check if there is a menu-item for each layer, also the layers in a subcategory
    expect(list.length).toEqual(expectedMenuItems.length);
    list.forEach((li) =>
      expect(
        expectedMenuItems.find((item) => item.title === li.textContent),
      ).toBeTruthy(),
    );

    // check if the sticky header is shown for each category
    // eslint-disable-next-line testing-library/no-node-access
    const stickyHeaders = baseElement.querySelectorAll(
      '[class*=MuiListSubheader-sticky]',
    );
    stickyHeaders.forEach((header) =>
      expect(
        expectedCategories.find(
          (category) => category.title === header.textContent,
        ),
      ).toBeTruthy(),
    );
  });

  it('should reload the layers from the service when clicking the reload button', async () => {
    const mockProps = {
      service: {
        name: 'KNMI Radar',
        url: mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
        id: 'knmi-radar',
      },
      onClickLayer: jest.fn(),
      highlightedLayers: [],
    };

    render(<WMSLayerTree {...mockProps} />);

    // wait until the layers are loaded
    const list = await screen.findAllByTestId('selectableLayer');
    expect(list).toBeTruthy();

    // click the reload button
    fireEvent.click(screen.getByTestId('reloadLayers'));

    // while reloading the loading state should be shown
    expect(screen.getByRole('progressbar')).toBeTruthy();

    // when loading is completed the loadingBar should be gone
    const newList = await screen.findAllByTestId('selectableLayer');
    expect(newList).toBeTruthy();
    expect(screen.queryByRole('progressbar')).toBeFalsy();
  });
});
