/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

import { layerActions, layerTypes, storeTestUtils } from '@opengeoweb/store';
import {
  LayerType,
  getCapabilities,
  mockGetCapabilities,
} from '@opengeoweb/webmap';
import { layerSelectTypes } from '@opengeoweb/layer-select';
import WMSLayerTreeConnect, {
  WMSLayerTreeConnectProps,
} from './WMSLayerTreeConnect';
import { defaultReduxLayerRadarColor } from '../../../utils/defaultTestSettings';

describe('src/components/WMSLoader/WMSLayerTree/WMSLayerTreeConnect', () => {
  jest
    .spyOn(getCapabilities, 'getLayersFromService')
    .mockImplementation(mockGetCapabilities.mockGetLayersFromService);

  it('should add a layer', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);

    const mockProps: WMSLayerTreeConnectProps = {
      service: {
        url: mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
      } as layerSelectTypes.LayerSelectService,
      mapId,
    };
    render(
      <Provider store={store}>
        <WMSLayerTreeConnect {...mockProps} />
      </Provider>,
    );

    const list = await screen.findAllByTestId('selectableLayer');
    expect(list).toBeTruthy();

    fireEvent.click(list[0]);

    const expectedActions = [
      layerActions.addLayer({
        layer: {
          id: expect.any(String),
          layerType: layer.layerType,
          name: layer.name,
          service: mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
        },
        layerId: expect.any(String),
        mapId,
        origin: 'WMSLayerTreeConnect',
      }),
    ];
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should add a baselayer if layerType baselayer is passed', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);

    const mockStore = configureStore();
    const store = mockStore(mockState);

    const mockProps = {
      service: {
        url: mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
      },
      mapId,
      layerType: LayerType.baseLayer,
    } as WMSLayerTreeConnectProps;
    render(
      <Provider store={store}>
        <WMSLayerTreeConnect {...mockProps} />
      </Provider>,
    );

    const list = await screen.findAllByTestId('selectableLayer');
    expect(list).toBeTruthy();

    fireEvent.click(list[0]);

    const expectedActions = [
      layerActions.addAvailableBaseLayer({
        layer: {
          id: expect.any(String),
          layerType: LayerType.baseLayer,
          name: layer.name,
          service: mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
          mapId,
        },
      }),
      layerActions.setBaseLayers({
        layers: [
          {
            id: expect.any(String),
            layerType: LayerType.baseLayer,
            name: layer.name,
            service: mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
            mapId,
          },
        ],
        mapId,
        origin: layerTypes.LayerActionOrigin.wmsLoader,
      }),
    ];

    expect(store.getActions()).toEqual(expectedActions);
  });
});
