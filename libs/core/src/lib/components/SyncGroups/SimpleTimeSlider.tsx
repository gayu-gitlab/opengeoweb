/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { Slider } from '@mui/material';
import { handleMomentISOString } from '@opengeoweb/webmap';
import moment from 'moment';
import React from 'react';

export interface SimpleTimeSliderProps {
  setTime: (time: string) => void;
  timeValue: string;
  startValue: moment.Moment;
  endValue: moment.Moment;
}

export const SimpleTimeSlider: React.FC<SimpleTimeSliderProps> = ({
  setTime,
  timeValue,
  startValue,
  endValue,
}: SimpleTimeSliderProps) => {
  const unixStart = moment(startValue).utc().unix();
  const unixEnd = moment(endValue).utc().unix();
  const timeSliderValue = timeValue
    ? moment(timeValue).utc().unix()
    : unixStart;

  return (
    <div style={{ padding: '0px 10px', background: 'none' }}>
      <Slider
        min={unixStart}
        max={unixEnd}
        step={1}
        value={timeSliderValue}
        onChange={(_, val: number): void => {
          setTime(handleMomentISOString(moment.unix(val).toISOString()));
        }}
      />
    </div>
  );
};
