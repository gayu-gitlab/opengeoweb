/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Box, SxProps, Theme, Typography } from '@mui/material';
import { useDispatch } from 'react-redux';

import {
  mapActions,
  uiActions,
  uiTypes,
  mapTypes,
  layerTypes,
  defaultLayers,
  getSingularDrawtoolDrawLayerId,
  mapStoreActions,
} from '@opengeoweb/store';
import { LayerType, webmapUtils } from '@opengeoweb/webmap';
import { MapControls, emptyGeoJSON } from '@opengeoweb/webmap-react';
import {
  TimeSliderClockConnect,
  TimeSliderConnect,
} from '@opengeoweb/timeslider';
import { renderCounter } from '@opengeoweb/shared';
import { ZoomControlConnect } from '../MapControls';
import { MapViewConnect } from '../MapView';
import { LegendConnect, LegendMapButtonConnect } from '../Legend';
import {
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
} from '../LayerManager';
import { MultiDimensionSelectMapButtonsConnect } from '../MultiMapDimensionSelect';
import {
  GetFeatureInfoButtonConnect,
  GetFeatureInfoConnect,
} from '../FeatureInfo';
import { SearchControlButtonConnect } from '../Search/SearchControlButtonConnect';
import { SearchControlConnect } from '../Search';

const titleStyle = (theme: Theme): SxProps<Theme> => ({
  position: 'absolute',
  padding: '5px',
  zIndex: 50,
  color: theme.palette.common.black,
  whiteSpace: 'nowrap',
  userSelect: 'none',
});

export const defaultBbox = {
  srs: 'EPSG:3857',
  bbox: {
    left: 58703.6377,
    bottom: 6408480.4514,
    right: 3967387.5161,
    top: 11520588.9031,
  },
};

export interface ConfigurableMapConnectProps {
  id?: string;
  dockedLayerManagerSize?: mapTypes.DockedLayerManagerSize;
  shouldAutoUpdate?: boolean;
  shouldAnimate?: boolean;
  title?: string;
  layers: layerTypes.Layer[];
  autoUpdateLayerId?: string;
  autoTimeStepLayerId?: string;
  bbox?: mapTypes.Bbox;
  srs?: string;
  dimensions?: mapTypes.Dimension[];
  animationPayload?: mapTypes.AnimationPayloadType;
  shouldShowZoomControls?: boolean;
  displayMapPin?: boolean;
  showTimeSlider?: boolean; // used for map preset action
  disableTimeSlider?: boolean; // used by multimap to disable timeslider completely
  toggleTimestepAuto?: boolean;
  displayTimeInMap?: boolean;
  displayLayerManagerAndLegendButtonInMap?: boolean;
  displayDimensionSelectButtonInMap?: boolean;
  multiLegend?: boolean;
  shouldShowLayerManager?: boolean;
  shouldShowDockedLayerManager?: boolean;
  showClock?: boolean;
  displayGetFeatureInfoButtonInMap?: boolean;
  shouldDisplayDrawControls?: boolean;
  displaySearchButtonInMap?: boolean;
  children?: React.ReactNode;
  mapControls?: React.ReactNode;
}

export const ConfigurableMapConnect: React.FC<ConfigurableMapConnectProps> = ({
  id,
  dockedLayerManagerSize,
  title,
  layers = [],
  dimensions = [],
  shouldAutoUpdate = false,
  shouldAnimate = false,
  bbox = defaultBbox.bbox,
  srs = defaultBbox.srs,
  shouldShowZoomControls = true,
  displayMapPin = false,
  showTimeSlider = true,
  disableTimeSlider = false,
  displayTimeInMap = false,
  displayLayerManagerAndLegendButtonInMap = true,
  displayDimensionSelectButtonInMap = true,
  multiLegend = true,
  shouldShowLayerManager,
  shouldShowDockedLayerManager,
  showClock = true,
  displayGetFeatureInfoButtonInMap = false,
  shouldDisplayDrawControls = false,
  displaySearchButtonInMap = false,
  mapControls,
  children,
  ...props
}: ConfigurableMapConnectProps) => {
  const dispatch = useDispatch();
  const mapId = React.useRef(id || webmapUtils.generateMapId()).current;

  React.useEffect(() => {
    const layersWithDefaultBaseLayer = layers.find(
      (layer) => layer.layerType === LayerType.baseLayer,
    )
      ? layers
      : [...layers, defaultLayers.baseLayerGrey];

    const layersWithDefaultOverLayer = layersWithDefaultBaseLayer.find(
      (layer) => layer.layerType === LayerType.overLayer,
    )
      ? layersWithDefaultBaseLayer
      : [...layersWithDefaultBaseLayer, defaultLayers.overLayer];

    const mapPreset = {
      layers: layersWithDefaultOverLayer,
      proj: {
        bbox,
        srs,
      },
      dimensions,
      shouldAutoUpdate,
      shouldAnimate,
      shouldShowZoomControls,
      displayMapPin,
      showTimeSlider,
      dockedLayerManagerSize,
      ...props,
    };

    const initialProps: mapTypes.MapPresetInitialProps = { mapPreset };

    dispatch(mapActions.setMapPreset({ mapId, initialProps }));

    if (shouldShowLayerManager !== undefined) {
      dispatch(
        uiActions.setActiveMapIdForDialog({
          type: uiTypes.DialogTypes.LayerManager,
          mapId,
          setOpen: shouldShowLayerManager,
          source: 'app',
        }),
      );
    }
    if (shouldShowDockedLayerManager) {
      dispatch(
        uiActions.setToggleOpenDialog({
          type: uiTypes.DialogTypes.LayerManager,
          mapId,
          setOpen: false,
        }),
      );
      dispatch(
        uiActions.setToggleOpenDialog({
          type: `${uiTypes.DialogTypes.DockedLayerManager}-${mapId}`,
          mapId,
          setOpen: true,
        }),
      );
    }
    if (shouldDisplayDrawControls) {
      dispatch(
        mapStoreActions.addLayer({
          mapId,
          layer: {
            geojson: emptyGeoJSON,
            layerType: LayerType.featureLayer,
          },
          layerId: getSingularDrawtoolDrawLayerId(mapId),
          origin: 'ConfigurableMapConnect',
        }),
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const mapControlsPositionTop = title ? 24 : 8;

  const mapWidthRef = React.createRef<HTMLDivElement>();

  return React.useMemo(() => {
    renderCounter.count(`${ConfigurableMapConnect.name}_${mapId}`);
    return (
      <Box
        sx={{
          width: '100%',
          height: '100%',
          position: 'relative',
          overflow: 'hidden',
        }}
        data-testid="ConfigurableMap"
        ref={mapWidthRef}
      >
        {title && (
          <Typography data-testid="mapTitle" sx={titleStyle as SxProps<Theme>}>
            {title}
          </Typography>
        )}

        <MapControls
          data-testid="mapControls"
          style={{ top: mapControlsPositionTop }}
        >
          {displaySearchButtonInMap && (
            <SearchControlButtonConnect mapId={mapId} />
          )}
          <ZoomControlConnect mapId={id} />
          {displayLayerManagerAndLegendButtonInMap && (
            <LayerManagerMapButtonConnect mapId={mapId} />
          )}
          {displayLayerManagerAndLegendButtonInMap && (
            <LegendMapButtonConnect mapId={mapId} multiLegend={multiLegend} />
          )}
          {displayDimensionSelectButtonInMap && (
            <MultiDimensionSelectMapButtonsConnect mapId={mapId} />
          )}
          {displayGetFeatureInfoButtonInMap && (
            <GetFeatureInfoButtonConnect mapId={mapId} />
          )}
          {mapControls}
        </MapControls>

        {!disableTimeSlider && (
          <Box
            sx={{
              position: 'absolute',
              left: '0px',
              bottom: '0px',
              zIndex: 1000,
              width: '100%',
            }}
          >
            <TimeSliderConnect mapId={id!} sourceId={id!} />
          </Box>
        )}

        <MapViewConnect
          controls={{}}
          displayTimeInMap={displayTimeInMap}
          showScaleBar={false}
          mapId={mapId}
        >
          {children}
        </MapViewConnect>
        {multiLegend && (
          <LegendConnect showMapId mapId={mapId} multiLegend={multiLegend} />
        )}
        <SearchControlConnect mapId={mapId} />
        {showClock && <TimeSliderClockConnect mapId={mapId} />}
        {displayGetFeatureInfoButtonInMap && (
          <GetFeatureInfoConnect showMapId mapId={mapId} />
        )}
        <LayerManagerConnect
          mapId={mapId}
          bounds="parent"
          isDocked
          mapWidthRef={mapWidthRef}
        />
      </Box>
    );
  }, [
    children,
    disableTimeSlider,
    displayDimensionSelectButtonInMap,
    displayGetFeatureInfoButtonInMap,
    displayLayerManagerAndLegendButtonInMap,
    displaySearchButtonInMap,
    displayTimeInMap,
    id,
    mapControls,
    mapControlsPositionTop,
    mapId,
    multiLegend,
    showClock,
    title,
    mapWidthRef,
  ]);
};
