/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { WMJSMap, webmapUtils } from '@opengeoweb/webmap';

import { defaultReduxLayerRadarColor } from '../../utils/defaultTestSettings';

import { getLayersToUpdate } from './utils';

describe('components/GetFeatureInfo/utils', () => {
  describe('getFeatureInfo', () => {
    it('getLayersToUpdate should return no layers if no layers are defined', () => {
      const layers = getLayersToUpdate([], 'mapId');
      expect(layers).toHaveLength(0);
    });
    it('getLayersToUpdate should return no layers no wmjslayer is registered', async () => {
      const baseElement = document.createElement('div');
      const wmjsmap = new WMJSMap(baseElement);
      const mapId1 = 'mapId1';
      webmapUtils.registerWMJSMap(wmjsmap, mapId1);
      const layers = getLayersToUpdate([defaultReduxLayerRadarColor], 'mapId1');
      expect(layers).toHaveLength(0);
    });
  });
});
