/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { publicLayers, MapControls } from '@opengeoweb/webmap-react';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { store } from '../../storybookUtils/store';
import { SearchControlButtonConnect, SearchControlConnect } from '.';
import { MapViewConnect } from '../MapView';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import { CoreThemeStoreProvider } from '../Providers/Providers';

export default { title: 'components/Search' };

const SearchControlConnectComp: React.FC = () => {
  const mapId1 = 'mapid_1';
  const mapId2 = 'mapid_2';

  useDefaultMapSettings({
    mapId: mapId1,
    layers: [{ ...publicLayers.radarLayer, id: `radar-${mapId1}` }],
    baseLayers: [
      { ...publicLayers.defaultLayers.baseLayerGrey, id: `baseGrey-${mapId1}` },
    ],
  });
  useDefaultMapSettings({
    mapId: mapId2,
    layers: [{ ...publicLayers.harmoniePrecipitation, id: `radar-${mapId2}` }],
    baseLayers: [
      { ...publicLayers.defaultLayers.baseLayerGrey, id: `baseGrey-${mapId2}` },
    ],
  });

  return (
    <div style={{ display: 'flex' }}>
      <div style={{ width: '50%', height: '100vh', position: 'relative' }}>
        <MapControls>
          <SearchControlButtonConnect mapId={mapId1} />
        </MapControls>
        <MapViewConnect mapId={mapId1} />
        <SearchControlConnect mapId={mapId1} />
      </div>
      <div style={{ width: '50%', height: '100vh', position: 'relative' }}>
        <MapControls>
          <SearchControlButtonConnect mapId={mapId2} />
        </MapControls>
        <MapViewConnect mapId={mapId2} />
        <SearchControlConnect mapId={mapId2} />
      </div>
    </div>
  );
};

export const SearchControlConnectStoryLight = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store} theme={lightTheme}>
    <SearchControlConnectComp />
  </CoreThemeStoreProvider>
);

SearchControlConnectStoryLight.storyName = 'Search Control Light Theme';

export const SearchControlConnectStoryDark = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store} theme={darkTheme}>
    <SearchControlConnectComp />
  </CoreThemeStoreProvider>
);

SearchControlConnectStoryDark.storyName = 'Search Control Dark Theme';
