/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { MyLocation } from '@opengeoweb/theme';
import React, { useRef } from 'react';
import { LayerType, webmapUtils } from '@opengeoweb/webmap';
import { useDispatch, useSelector } from 'react-redux';
import { CoreAppStore, layerActions, layerSelectors } from '@opengeoweb/store';
import {
  MapDrawDrawFunctionArgs,
  registerDrawFunction,
} from '@opengeoweb/webmap-react';
import { CustomIconButton } from '@opengeoweb/shared';

export interface MyMapLocationProps {
  mapId: string | undefined;
}

export const MY_LOCATION = 'MyLocation';

const locationOptions = {
  enableHighAccuracy: true,
  timeout: 5000,
  maximumAge: 0,
};

const circleDrawFunction = (
  args: MapDrawDrawFunctionArgs,
  fillColor = '#88F',
  radius = 12,
): void => {
  const { context: ctx, coord } = args;
  ctx.strokeStyle = '#000';
  ctx.fillStyle = fillColor || '#88F';
  ctx.lineWidth = 1;
  ctx.beginPath();
  ctx.arc(coord.x, coord.y, radius || 10, 0, 2 * Math.PI);
  ctx.fill();
  ctx.stroke();
  ctx.fillStyle = '#000';
  ctx.beginPath();
  ctx.arc(coord.x, coord.y, 2, 0, 2 * Math.PI);
  ctx.fill();
};

export const MyMapLocation: React.FC<MyMapLocationProps> = ({
  mapId,
}: MyMapLocationProps) => {
  const dispatch = useDispatch();

  const layersBelongsToCurrentMapId = useSelector((store: CoreAppStore) =>
    layerSelectors.getLayersByMapId(store, mapId || ''),
  );

  const layerLocation = layersBelongsToCurrentMapId.find((layer) => {
    if (
      layer.geojson &&
      layer.geojson.features[0] &&
      layer.geojson.features[0].properties &&
      layer.geojson.features[0].properties?.name
    ) {
      if (layer.geojson.features[0].properties.name === MY_LOCATION) {
        return layer;
      }
    }
    return undefined;
  });

  const layerLocationId = layerLocation?.id || '';

  const circleDrawFunctionId = useRef(registerDrawFunction(circleDrawFunction));

  const handleLocationClick = (): void => {
    if (mapId && layerLocationId !== '') {
      dispatch(
        layerActions.layerDelete({
          mapId,
          layerIndex: 0,
          layerId: layerLocationId,
        }),
      );
    } else if (
      navigator.geolocation &&
      mapId &&
      layerLocationId === '' &&
      circleDrawFunctionId
    ) {
      navigator.geolocation.getCurrentPosition(
        (success) => {
          const {
            coords: { latitude, longitude },
          } = success;
          const geojson: GeoJSON.FeatureCollection = {
            type: 'FeatureCollection',
            features: [
              {
                type: 'Feature',
                properties: {
                  name: MY_LOCATION,
                  drawFunctionId: circleDrawFunctionId.current,
                },
                geometry: {
                  type: 'Point',
                  coordinates: [longitude, latitude],
                },
              },
            ],
          };
          const layerId = webmapUtils.generateLayerId();
          dispatch(
            layerActions.addLayer({
              mapId,
              layer: { geojson, layerType: LayerType.featureLayer },
              layerId,
              origin: MY_LOCATION,
            }),
          );
          const wmjsMap = webmapUtils.getWMJSMapById(mapId);
          if (!wmjsMap) {
            return;
          }
          wmjsMap.calculateBoundingBoxAndZoom(latitude, longitude);
        },
        (error) => {
          console.warn(`Geolocation error: ${error.message}`);
        },
        locationOptions,
      );
    }
  };

  return (
    <CustomIconButton
      title="Find my location"
      aria-label={MY_LOCATION}
      onClick={handleLocationClick}
      isSelected={layerLocationId !== ''}
      size="large"
    >
      <MyLocation />
    </CustomIconButton>
  );
};
