/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { act, render, screen } from '@testing-library/react';
import { createStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import { layerTypes, mapActions } from '@opengeoweb/store';
import userEvent from '@testing-library/user-event';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import { MY_LOCATION, MyMapLocation } from './MyMapLocation';

describe('src/components/Search/MapLocation', () => {
  it('should display my location in a single map', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });

    const mapId = 'mapId123';
    render(
      <CoreThemeStoreProvider store={store}>
        <MyMapLocation mapId={mapId} />
      </CoreThemeStoreProvider>,
    );

    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
    });

    Object.defineProperty(global.navigator, 'geolocation', {
      value: {
        getCurrentPosition: (success: PositionCallback) =>
          success({
            coords: {
              latitude: 1,
              longitude: 2,
            } as GeolocationCoordinates,
          } as GeolocationPosition),
      },
    });

    await userEvent.click(screen.getByRole('button', { name: MY_LOCATION }));

    const reduxLayer = Object.values(
      store.getState().layers.byId,
    )[0] as layerTypes.ReduxLayer;

    expect(reduxLayer.geojson!.features[0].geometry).toEqual({
      coordinates: [2, 1],
      type: 'Point',
    });

    expect(reduxLayer.geojson!.features[0].properties).toEqual({
      drawFunctionId: 'drawFunctionId_1',
      name: MY_LOCATION,
    });

    expect(screen.getByRole('button', { name: MY_LOCATION })).toBeEnabled();

    await userEvent.click(screen.getByRole('button', { name: MY_LOCATION }));

    const reduxLayerUpdated = Object.values(
      store.getState().layers.byId,
    )[0] as layerTypes.ReduxLayer;

    expect(reduxLayerUpdated).toBeUndefined();
  });
  it('should correctly display location in multiple maps', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });

    const mapId1 = 'mapId_1';
    const mapId2 = 'mapId_2';
    render(
      <CoreThemeStoreProvider store={store}>
        <MyMapLocation mapId={mapId1} />
      </CoreThemeStoreProvider>,
    );
    render(
      <CoreThemeStoreProvider store={store}>
        <MyMapLocation mapId={mapId2} />
      </CoreThemeStoreProvider>,
    );

    act(() => {
      store.dispatch(mapActions.registerMap({ mapId: mapId1 }));
      store.dispatch(mapActions.registerMap({ mapId: mapId2 }));
    });

    const locationButtons = screen.getAllByRole('button', {
      name: MY_LOCATION,
    });

    expect(locationButtons.length === 2);
    await userEvent.click(locationButtons[0]);
    expect(locationButtons[0]).toHaveAttribute('aria-pressed', 'true');

    // Location layer 1 is at position 0
    const reduxLayer1 = Object.values(
      store.getState().layers.byId,
    )[0] as layerTypes.ReduxLayer;

    expect(reduxLayer1.mapId).toEqual(mapId1);
    expect(reduxLayer1.geojson!.features[0].geometry).toEqual({
      coordinates: [2, 1],
      type: 'Point',
    });

    await userEvent.click(locationButtons[1]);
    expect(locationButtons[1]).toHaveAttribute('aria-pressed', 'true');

    // Location layer 2 is at position 1
    const reduxLayer2 = Object.values(
      store.getState().layers.byId,
    )[1] as layerTypes.ReduxLayer;

    expect(reduxLayer2.mapId).toEqual(mapId2);
    expect(reduxLayer2.geojson!.features[0].geometry).toEqual({
      coordinates: [2, 1],
      type: 'Point',
    });

    await userEvent.click(locationButtons[0]);
    expect(locationButtons[0]).toHaveAttribute('aria-pressed', 'false');

    // Check if redux has moved mapId2 to the former location of mapId1
    const reduxLayerUpdated = Object.values(
      store.getState().layers.byId,
    )[0] as layerTypes.ReduxLayer;

    expect(reduxLayerUpdated.mapId).toEqual(mapId2);

    await userEvent.click(locationButtons[1]);
    expect(locationButtons[1]).toHaveAttribute('aria-pressed', 'false');

    const reduxLayerEmptied = Object.values(
      store.getState().layers.byId,
    )[0] as layerTypes.ReduxLayer;

    expect(reduxLayerEmptied).toBeUndefined();
  });
});
