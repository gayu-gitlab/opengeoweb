/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { InputAdornment, TextField } from '@mui/material';
import { Box, Theme } from '@mui/system';
import { CustomIconButton } from '@opengeoweb/shared';
import { CSSProperties, Close } from '@opengeoweb/theme';
import React from 'react';
import { MyMapLocation } from './MyMapLocation';

export interface SearchDialogProps {
  isOpen: boolean;
  onClose: () => void;
  mapId?: string;
}

export const SearchDialog: React.FC<SearchDialogProps> = ({
  isOpen,
  onClose,
  mapId = '',
}: SearchDialogProps) => {
  return isOpen && mapId ? (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        position: 'absolute',
        pointerEvents: 'all',
        zIndex: 999,
        left: 50,
        top: 15,
        borderBottom: '1px solid grey',
        backgroundColor: (theme: Theme): CSSProperties =>
          theme.palette.background.paper,
      }}
    >
      <MyMapLocation mapId={mapId} />
      <TextField
        id="dummy-search-field"
        label="Search"
        variant="filled"
        InputProps={{
          disableUnderline: true,
          endAdornment: (
            <InputAdornment position="end">
              <CustomIconButton
                tooltipTitle="Close"
                edge="end"
                onClick={onClose}
                size="large"
              >
                <Close />
              </CustomIconButton>
            </InputAdornment>
          ),
        }}
      />
    </Box>
  ) : null;
};
