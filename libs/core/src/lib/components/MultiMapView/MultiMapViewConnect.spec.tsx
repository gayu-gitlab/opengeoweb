/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen, act } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { LayerType, WMJSService, webmapUtils } from '@opengeoweb/webmap';
import {
  createStore,
  CoreAppStore,
  syncGroupsActions,
} from '@opengeoweb/store';
import { Store } from '@reduxjs/toolkit';
import { MultiMapViewConnect, MultiMapViewProps } from './MultiMapViewConnect';
import { radarLayer } from '../../utils/testLayers';
import { CoreThemeStoreProvider } from '../Providers/Providers';

describe('src/components/MultiMapView/MultiMapViewConnect', () => {
  it('should render', () => {
    const props = {
      rows: 2,
      cols: 2,
      maps: [
        {
          id: webmapUtils.generateMapId(),
          syncGroupsIds: ['firstGroup'],
          layers: [],
          bbox: { left: -180, bottom: -90, right: 180, top: 90 },
          srs: 'EPSG:4326',
        },
      ],
    };
    const mockState = {
      syncronizationGroupStore: {
        sources: { byId: {}, allIds: [] },
        groups: { byId: {}, allIds: [] },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <CoreThemeStoreProvider store={store}>
        <MultiMapViewConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    const select = screen.getByTestId('MultiMapViewSliderConnect');
    expect(select).toBeDefined();
    expect(screen.getByTestId('mapTitle').innerHTML).toEqual(props.maps[0].id);
  });

  it('should render title', () => {
    const props = {
      rows: 2,
      cols: 2,
      maps: [
        {
          id: webmapUtils.generateMapId(),
          title: 'my testing tile',
          syncGroupsIds: ['firstGroup'],
          layers: [],
          bbox: { left: -180, bottom: -90, right: 180, top: 90 },
          srs: 'EPSG:4326',
        },
      ],
    };
    const mockState = {
      syncronizationGroupStore: {
        sources: { byId: {}, allIds: [] },
        groups: { byId: {}, allIds: [] },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <CoreThemeStoreProvider store={store}>
        <MultiMapViewConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    const select = screen.getByTestId('MultiMapViewSliderConnect');
    expect(select).toBeDefined();
    expect(screen.getByTestId('mapTitle').innerHTML).toEqual(
      props.maps[0].title,
    );
  });

  it('should show zoomcontrols by default', () => {
    const props = {
      rows: 2,
      cols: 2,
      maps: [
        {
          id: 'mapid',
          syncGroupsIds: ['firstGroup'],
          layers: [],
          bbox: { left: -180, bottom: -90, right: 180, top: 90 },
          srs: 'EPSG:4326',
        },
      ],
    } as MultiMapViewProps;
    const mockState = {
      syncronizationGroupStore: {
        sources: { byId: {}, allIds: [] },
        groups: { byId: {}, allIds: [] },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <CoreThemeStoreProvider store={store}>
        <MultiMapViewConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    const select = screen.getByTestId('MultiMapViewSliderConnect');
    expect(select).toBeDefined();
    expect(screen.getByTestId('zoom-reset')).toBeTruthy();
    expect(screen.getByTestId('zoom-in')).toBeTruthy();
    expect(screen.getByTestId('zoom-out')).toBeTruthy();
  });

  it('should not show zoomcontrols if passed as false', () => {
    const mapId = 'test';
    const props = {
      rows: 2,
      cols: 2,
      showZoomControls: false,
      maps: [
        {
          id: mapId,
          syncGroupsIds: ['firstGroups'],
          layers: [],
          bbox: { left: -180, bottom: -90, right: 180, top: 90 },
          srs: 'EPSG:4326',
        },
      ],
    } as MultiMapViewProps;
    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            shouldShowZoomControls: false,
            mapLayers: [],
            baseLayers: [],
            overLayers: [],
            dimensions: [],
          },
        },
        allIds: [mapId],
      },
      syncronizationGroupStore: {
        sources: { byId: {}, allIds: [] },
        groups: { byId: {}, allIds: [] },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <CoreThemeStoreProvider store={store}>
        <MultiMapViewConnect {...props} />
      </CoreThemeStoreProvider>,
    );
    const select = screen.getByTestId('MultiMapViewSliderConnect');
    expect(select).toBeDefined();
    expect(screen.queryByTestId('zoom-reset')).toBeFalsy();
    expect(screen.queryByTestId('zoom-in')).toBeFalsy();
    expect(screen.queryByTestId('zoom-out')).toBeFalsy();
  });

  const mapId0 = webmapUtils.generateMapId();
  const mapId1 = webmapUtils.generateMapId();
  const firstGroup = 'firstGroup';
  const secondGroup = 'secondGroup';

  const setup = (multiLegend: boolean): Store => {
    // avoid backend calls
    jest
      .spyOn(WMJSService.prototype, 'getCapabilities')
      .mockImplementation(() => {});

    const props = {
      rows: 1,
      cols: 2,
      multiLegend,
    };
    const maps = [
      {
        id: mapId0,
        syncGroupsIds: [firstGroup],
        title: 'Precipitation Radar NL',
        layers: [{ ...radarLayer, id: 'layerid_1' }],
      },
      {
        id: mapId1,
        syncGroupsIds: [secondGroup],
        title: 'Temperature Observations',
        layers: [
          {
            service: 'https://geoservices.knmi.nl/adagucserver?dataset=OBS',
            name: '10M/ta',
            layerType: LayerType.mapLayer,
            id: 'layerid_2',
          },
        ],
        activeLayerId: 'testid',
      },
    ];
    // first render without maps because sync groups has to be added manually to avoid console warnings
    const store = createStore();
    const { rerender } = render(<MultiMapViewConnect {...props} maps={[]} />, {
      wrapper: ({ children }) => (
        <CoreThemeStoreProvider store={store}>
          {children}
        </CoreThemeStoreProvider>
      ),
    });

    act(() => {
      store.dispatch(
        syncGroupsActions.syncGroupAddGroup({
          groupId: firstGroup,
          title: 'title',
          type: 'SYNCGROUPS_TYPE_SETBBOX',
        }),
      );
      store.dispatch(
        syncGroupsActions.syncGroupAddGroup({
          groupId: secondGroup,
          title: 'title',
          type: 'SYNCGROUPS_TYPE_SETBBOX',
        }),
      );
    });

    // add maps after sync groups are added
    rerender(<MultiMapViewConnect {...props} maps={maps} />);
    return store;
  };
  it('check if MultiMapView component stores expect state during mount with single legend', async () => {
    const multiLegend = false;
    const store = setup(multiLegend);
    const state: CoreAppStore = store.getState();
    expect(Object.keys(state.ui!.dialogs)).toEqual(
      expect.arrayContaining([
        `dockedLayerManager-${mapId0}`,
        `legend`,
        `search-${mapId0}`,
        `dockedLayerManager-${mapId1}`,
        `search-${mapId1}`,
      ]),
    );

    expect(Object.keys(state.webmap!.byId)).toEqual([mapId0, mapId1]);

    expect(
      state.syncronizationGroupStore!.groups.byId[firstGroup].targets.byId[
        mapId0
      ],
    ).toEqual({ linked: true });

    expect(
      state.syncronizationGroupStore!.groups.byId[secondGroup].targets.byId[
        mapId1
      ],
    ).toEqual({ linked: true });

    const getIsLegendOpen = (): boolean => {
      const state: CoreAppStore = store.getState();
      return state.ui!.dialogs[`legend`]!.isOpen;
    };

    const legendButtons = screen.queryAllByTestId('open-Legend');

    expect(getIsLegendOpen()).toEqual(true);
    fireEvent.click(legendButtons[0]);
    expect(getIsLegendOpen()).toEqual(false);

    fireEvent.click(legendButtons[1]);
    expect(getIsLegendOpen()).toEqual(true);
  });

  it('check if MultiMapView component stores correct state during mount with multiple legends', async () => {
    const multiLegend = true;
    const store = setup(multiLegend);
    const state: CoreAppStore = store.getState();
    expect(Object.keys(state.ui!.dialogs)).toEqual(
      expect.arrayContaining([
        `dockedLayerManager-${mapId0}`,
        `legend-${mapId0}`,
        `search-${mapId0}`,
        `dockedLayerManager-${mapId1}`,
        `legend-${mapId1}`,
        `search-${mapId1}`,
      ]),
    );

    expect(Object.keys(state.webmap!.byId)).toEqual([mapId0, mapId1]);

    expect(
      state.syncronizationGroupStore!.groups.byId[firstGroup].targets.byId[
        mapId0
      ],
    ).toEqual({ linked: true });

    expect(
      state.syncronizationGroupStore!.groups.byId[secondGroup].targets.byId[
        mapId1
      ],
    ).toEqual({ linked: true });

    const getIsLegendOpen = (mapId: string): boolean => {
      const state: CoreAppStore = store.getState();
      return state.ui!.dialogs[`legend-${mapId}`]!.isOpen;
    };

    const legendButtons = screen.queryAllByTestId('open-Legend');

    expect(getIsLegendOpen(mapId0)).toEqual(false);
    fireEvent.click(legendButtons[0]);
    expect(getIsLegendOpen(mapId0)).toEqual(true);

    expect(getIsLegendOpen(mapId1)).toEqual(false);
    fireEvent.click(legendButtons[1]);
    expect(getIsLegendOpen(mapId1)).toEqual(true);
  });
});
