/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { publicLayers } from '@opengeoweb/webmap-react';

export const mapPresetRadar = {
  layers: [publicLayers.radarLayer],
  proj: {
    bbox: {
      left: -450651.2255879827,
      bottom: 6490531.093143953,
      right: 1428345.8183648037,
      top: 7438773.776232235,
    },
    srs: 'EPSG:3857',
  },
};

export const mapPresetObsTA = {
  layers: [publicLayers.obsAirTemperature],
  proj: {
    bbox: {
      left: -450651.2255879827,
      bottom: 6490531.093143953,
      right: 1428345.8183648037,
      top: 7438773.776232235,
    },
    srs: 'EPSG:3857',
  },
};

export const mapPresetObsWind = {
  layers: [publicLayers.obsWind],
  proj: {
    bbox: {
      left: -450651.2255879827,
      bottom: 6490531.093143953,
      right: 1428345.8183648037,
      top: 7438773.776232235,
    },
    srs: 'EPSG:3857',
  },
};

export const mapPresetObsPP = {
  layers: [publicLayers.obsAirPressureAtSeaLevel],
  proj: {
    bbox: {
      left: -450651.2255879827,
      bottom: 6490531.093143953,
      right: 1428345.8183648037,
      top: 7438773.776232235,
    },
    srs: 'EPSG:3857',
  },
};

export const mapPresetObsRH = {
  layers: [publicLayers.obsRelativeHumidity],
  proj: {
    bbox: {
      left: -450651.2255879827,
      bottom: 6490531.093143953,
      right: 1428345.8183648037,
      top: 7438773.776232235,
    },
    srs: 'EPSG:3857',
  },
};

export const mapPresetObsQG = {
  layers: [publicLayers.obsGlobalSolarRadiation],
  proj: {
    bbox: {
      left: -450651.2255879827,
      bottom: 6490531.093143953,
      right: 1428345.8183648037,
      top: 7438773.776232235,
    },
    srs: 'EPSG:3857',
  },
};

export const mapPresetEumetsat = {
  layers: [publicLayers.msgNaturalenhncdEUMETSAT],
  proj: {
    bbox: {
      left: -7529663.50832266,
      bottom: 308359.5390525013,
      right: 7493930.85787452,
      top: 11742807.68245839,
    },
    srs: 'EPSG:3857',
  },
};

export const mapPresetHarmoniePrecipitationFlux = {
  layers: [publicLayers.harmoniePrecipitation],
  proj: {
    bbox: {
      left: -450651.2255879827,
      bottom: 6490531.093143953,
      right: 1428345.8183648037,
      top: 7438773.776232235,
    },
    srs: 'EPSG:3857',
  },
};

export const mapPresetHarmonieTemperature2m = {
  layers: [publicLayers.harmonieAirTemperature],
  proj: {
    bbox: {
      left: -450651.2255879827,
      bottom: 6490531.093143953,
      right: 1428345.8183648037,
      top: 7438773.776232235,
    },
    srs: 'EPSG:3857',
  },
};

export const mapPresetHarmoniePressureMSL = {
  layers: [publicLayers.harmoniePressure],
  proj: {
    bbox: {
      left: -450651.2255879827,
      bottom: 6490531.093143953,
      right: 1428345.8183648037,
      top: 7438773.776232235,
    },
    srs: 'EPSG:3857',
  },
};
