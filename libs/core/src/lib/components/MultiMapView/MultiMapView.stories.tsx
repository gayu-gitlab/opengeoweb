/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { MultiMapViewStory2x2 } from './MultiMapView2x2.stories';
import { MultiMapViewStory4x3 } from './MultiMapView4x3.stories';
import { MultiMapViewStory10x10 } from './MultiMapView10x10.stories';
import {
  ModelRunIntervalCustomStart,
  ModelRunIntervalDefault,
  ModelRunIntervalHourly,
} from './ModelRunInterval/ModelRunInterval.stories';

export {
  MultiMapViewStory2x2,
  MultiMapViewStory4x3,
  MultiMapViewStory10x10,
  ModelRunIntervalDefault,
  ModelRunIntervalHourly,
  ModelRunIntervalCustomStart,
};

export default {
  title: 'components/MultiMapView',
};
