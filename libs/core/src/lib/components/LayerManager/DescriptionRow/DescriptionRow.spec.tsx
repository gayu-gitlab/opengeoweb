/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { storeTestUtils } from '@opengeoweb/store';
import DescriptionRow from './DescriptionRow';
import { CoreThemeStoreProvider } from '../../Providers/Providers';
import { LayerManagerCustomSettings } from '../LayerManagerUtils';

const { mockStateMapWithAnimationDelayWithoutLayers } = storeTestUtils;
describe('src/components/LayerManager/DescriptionRow', () => {
  const props = {
    mapId: 'mapId_1',
  };
  const propsWithLayerSelect = {
    ...props,
  };

  it('should render correct components without layerSelect parameter', () => {
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = createMockStoreWithEggs(mockState);

    render(
      <CoreThemeStoreProvider store={store}>
        <DescriptionRow {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('descriptionRow')).toBeTruthy();
    expect(screen.getByTestId('layerSelectButton')).toBeTruthy();
    expect(screen.getByText('Layer')).toBeTruthy();
    expect(screen.getByText('Style')).toBeTruthy();
    expect(screen.getByText('Opacity')).toBeTruthy();
    expect(screen.getByText('Dimensions')).toBeTruthy();
    expect(screen.getByText('Acc Time')).toBeTruthy();
  });

  it('should render correct components with layerSelect parameter', () => {
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = createMockStoreWithEggs(mockState);

    render(
      <CoreThemeStoreProvider store={store}>
        <DescriptionRow {...propsWithLayerSelect} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('descriptionRow')).toBeTruthy();
    expect(screen.getByTestId('layerSelectButton')).toBeTruthy();
    expect(screen.getByText('Layer')).toBeTruthy();
    expect(screen.getByText('Style')).toBeTruthy();
    expect(screen.getByText('Opacity')).toBeTruthy();
    expect(screen.getByText('Dimensions')).toBeTruthy();
  });

  it('should render correctly with no settings prop', async () => {
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = createMockStoreWithEggs(mockState);

    render(
      <CoreThemeStoreProvider store={store}>
        <DescriptionRow {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(screen.getByText('Layer')).toBeTruthy();
    expect(screen.getByText('Style')).toBeTruthy();
    expect(screen.getByText('Opacity')).toBeTruthy();
    expect(screen.getByText('Dimensions')).toBeTruthy();
  });

  it('should render headers according to settings prop', async () => {
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = createMockStoreWithEggs(mockState);

    const customSettings: LayerManagerCustomSettings['header'] = {
      addLayer: {
        tooltipTitle: 'addLayerTooltip',
        icon: <div data-testid="testAddLayerIcon" />,
      },
      layerName: {
        title: 'layerNameTitle',
      },
      layerStyle: {
        title: 'layerStyleTitle',
      },
      opacity: {
        title: 'opacityTitle',
      },
      dimensions: {
        title: 'dimensionsTitle',
      },
      acceptanceTime: { title: 'acceptanceTimeTitle' },
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <DescriptionRow {...props} settings={customSettings} />
      </CoreThemeStoreProvider>,
    );

    expect(screen.queryByText('Layer')).toBeFalsy();
    expect(screen.queryByText('Style')).toBeFalsy();
    expect(screen.queryByText('Opacity')).toBeFalsy();
    expect(screen.queryByText('Dimensions')).toBeFalsy();
    expect(screen.queryByText('Acc Time')).toBeFalsy();

    expect(screen.getByTestId('testAddLayerIcon')).toBeTruthy();
    expect(screen.getByText(customSettings.layerName!.title)).toBeTruthy();
    expect(screen.getByText(customSettings.layerStyle!.title)).toBeTruthy();
    expect(screen.getByText(customSettings.opacity!.title)).toBeTruthy();
    expect(screen.getByText(customSettings.dimensions!.title)).toBeTruthy();
    expect(screen.getByText(customSettings.acceptanceTime!.title)).toBeTruthy();
  });

  it('should render correct headers with partial settings given', async () => {
    const mockState = mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = createMockStoreWithEggs(mockState);

    const customSettings: LayerManagerCustomSettings['header'] = {
      layerName: {
        title: 'layerNameTitle',
      },
      layerStyle: {
        title: 'layerStyleTitle',
      },
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <DescriptionRow {...props} settings={customSettings} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByText(customSettings.layerName!.title)).toBeTruthy();
    expect(screen.getByText(customSettings.layerStyle!.title)).toBeTruthy();
    expect(screen.getByText('Opacity')).toBeTruthy();
    expect(screen.getByText('Dimensions')).toBeTruthy();
    expect(screen.getByText('Acc Time')).toBeTruthy();
  });
});
