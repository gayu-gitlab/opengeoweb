/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { Box, Grid, Theme } from '@mui/material';
import * as React from 'react';
import { uiTypes } from '@opengeoweb/store';
import { LayerSelectButtonConnect } from '@opengeoweb/layer-select';
import {
  columnClasses,
  LayerManagerCustomSettings,
  leftButtonsStyle,
} from '../LayerManagerUtils';

interface DescriptionRowProps extends React.ComponentProps<typeof Grid> {
  mapId: string;
  mapPresetsModule?: React.ReactElement;
  source?: uiTypes.Source;
  settings?: LayerManagerCustomSettings['header'];
  isMultiMap?: boolean;
}

const styles = {
  text: (theme: Theme): React.CSSProperties => ({
    paddingLeft: '10px',
    fontSize: '12px',
    fontFamily: 'Roboto',
    opacity: 0.67,
    ...theme.palette.geowebColors.layerManager.headerRowText,
  }),
};

const DescriptionRow: React.FC<DescriptionRowProps> = ({
  mapId,
  mapPresetsModule,
  source = 'app',
  settings,
  isMultiMap,
  ...gridProps
}: DescriptionRowProps) => {
  const [mapPresetsModuleInline] = React.useState(
    mapPresetsModule
      ? {
          ...mapPresetsModule,
          props: {
            ...mapPresetsModule.props,
            id: 'mappresets-menubuttonInline',
          },
        }
      : undefined,
  );
  const hasMapPresetsModule = mapPresetsModule !== undefined;

  return (
    <Grid
      container
      item
      data-testid="descriptionRow"
      sx={{ height: 40 }}
      className="descriptionRow"
      alignItems="center"
      {...gridProps}
    >
      <Grid
        item
        className={columnClasses.column1}
        sx={{
          ...leftButtonsStyle,
          justifyContent: 'center',
          paddingRight: '6px',
        }}
      >
        <LayerSelectButtonConnect
          mapId={mapId}
          source={source}
          tooltipTitle={settings?.addLayer?.tooltipTitle}
          icon={settings?.addLayer?.icon}
          isMultiMap={isMultiMap}
        />
      </Grid>
      <Grid className={columnClasses.column2}>
        <Box
          className={`header-layer ${
            hasMapPresetsModule ? 'mappresetsModule-enabled' : ''
          }`}
          sx={styles.text}
        >
          {settings?.layerName?.title ?? 'Layer'}
        </Box>
        <Box className="header-mapPresets" sx={{ marginRight: 4 }}>
          {mapPresetsModuleInline}
        </Box>
      </Grid>
      <Grid className={columnClasses.column3}>
        <Box sx={styles.text}>{settings?.layerStyle?.title ?? 'Style'}</Box>
      </Grid>
      <Grid className={columnClasses.column4}>
        <Box sx={styles.text}>{settings?.opacity?.title ?? 'Opacity'}</Box>
      </Grid>
      <Grid className={columnClasses.column5}>
        <Box sx={styles.text}>
          {settings?.dimensions?.title ?? 'Dimensions'}
        </Box>
      </Grid>
      <Grid className={columnClasses.acceptanceTime}>
        <Box sx={[styles.text, { whiteSpace: 'nowrap' }]}>
          {settings?.acceptanceTime?.title ?? 'Acc Time'}
        </Box>
      </Grid>
      <Grid className={columnClasses.column6} />
    </Grid>
  );
};

export default DescriptionRow;
