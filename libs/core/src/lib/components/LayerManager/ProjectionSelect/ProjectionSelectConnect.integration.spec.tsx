/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import {
  layerReducer,
  mapActions,
  uiReducer,
  webmapReducer,
} from '@opengeoweb/store';
import { configureStore } from '@reduxjs/toolkit';
import { render, screen } from '@testing-library/react';
import createSagaMiddleware from 'redux-saga';
import userEvent from '@testing-library/user-event';
import { CoreThemeStoreProvider } from '../../Providers/Providers';
import LayerManagerMapButtonConnect from '../LayerManagerMapButtonConnect';
import LayerManagerConnect from '../LayerManagerConnect';

describe('src/components/LayerManager/ProjectionSelect/ProjectionSelectConnect', () => {
  const sagaMiddleware = createSagaMiddleware();

  const store = configureStore({
    reducer: {
      ui: uiReducer,
      layers: layerReducer,
      webmap: webmapReducer,
    },
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(sagaMiddleware),
  });

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  store.addEggs = jest.fn();

  it('should show the correct drawings', async () => {
    const mapId = 'map123';

    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <CoreThemeStoreProvider store={store}>
        <LayerManagerConnect showMapIdInTitle />
        <LayerManagerMapButtonConnect mapId={mapId} />
      </CoreThemeStoreProvider>,
    );

    const layerManagerButton = screen.getByRole('button', {
      name: /Layer Manager/i,
    });
    const user = userEvent.setup();
    await user.click(layerManagerButton);

    expect(await screen.findByText(`Layer Manager ${mapId}`)).toBeTruthy();
    expect(await screen.findByText(`Europe Mercator`)).toBeTruthy();
    await user.click(
      screen.getByRole('button', {
        name: /Europe Mercator/i,
      }),
    );

    expect(await screen.findByText(`Europe Robinson`)).toBeTruthy();

    await user.click(
      screen.getByRole('option', {
        name: /Europe Robinson/i,
      }),
    );

    expect(screen.queryByText(`Europe Mercator`)).toBeFalsy();
    expect(await screen.findByText(`Europe Robinson`)).toBeTruthy();
  });
});
