/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { storeTestUtils } from '@opengeoweb/store';
import LayerContainerRow from './LayerContainerRow';
import { CoreThemeProvider } from '../../Providers/Providers';
import {
  defaultReduxLayerRadarKNMI,
  multiDimensionLayer,
} from '../../../utils/defaultTestSettings';

describe('src/components/LayerManager/LayerContainerRow', () => {
  const props = {
    mapId: 'mapId_1',
  };

  it('should render correct component', () => {
    const mockStore = configureStore();
    const mockState =
      storeTestUtils.mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = mockStore(mockState);
    render(
      <Provider store={store}>
        <LayerContainerRow {...props} />
      </Provider>,
    );
    expect(screen.getByTestId('layerContainerRow')).toBeTruthy();
  });

  it('should have a layer row for each layer in the default order', () => {
    const layers = [defaultReduxLayerRadarKNMI, multiDimensionLayer];
    const mockState = storeTestUtils.mockStateMapWithMultipleLayers(
      layers,
      props.mapId,
    );
    const mockStore = configureStore();
    const store = mockStore(mockState);

    render(
      <CoreThemeProvider>
        <Provider store={store}>
          <LayerContainerRow {...props} />
        </Provider>
      </CoreThemeProvider>,
    );
    const layerRows = screen.getAllByRole('listitem');

    expect(layerRows.length).toEqual(layers.length);
    layerRows.forEach((row, index): void => {
      expect(row.getAttribute('data-testid')).toEqual(
        `layerRow-${layers[index].id}`,
      );
      const dragHandles = screen.queryAllByTestId(`dragHandle-${index}`);
      expect(dragHandles[index].getAttribute('disabled')).toBeNull();
    });
  });

  it('should disable drag if only one layer', () => {
    const layers = [defaultReduxLayerRadarKNMI];
    const mockState = storeTestUtils.mockStateMapWithMultipleLayers(
      layers,
      props.mapId,
    );
    const mockStore = configureStore();
    const store = mockStore(mockState);

    render(
      <CoreThemeProvider>
        <Provider store={store}>
          <LayerContainerRow {...props} />
        </Provider>
      </CoreThemeProvider>,
    );
    const layerRows = screen.getAllByRole('listitem');

    expect(layerRows.length).toEqual(layers.length);
    const dragHandle = screen.queryAllByTestId(`dragHandle-0`)[0];

    expect(dragHandle.getAttribute('title')).toBeNull();
    expect(dragHandle.getAttribute('disabled')).toEqual('');
  });

  it('should call prevent default to prevent scrolling when ctrl is pressed', async () => {
    const user = userEvent.setup();
    const layers = [defaultReduxLayerRadarKNMI];
    const mockState = storeTestUtils.mockStateMapWithMultipleLayers(
      layers,
      props.mapId,
    );
    const mockStore = configureStore();
    const store = mockStore(mockState);

    render(
      <CoreThemeProvider>
        <Provider store={store}>
          <LayerContainerRow {...props} />
        </Provider>
      </CoreThemeProvider>,
    );

    const layerContainerRow = screen.queryByTestId('layerContainerRow')!;
    // This simulates that keys are pressed (and kept pressed down)
    await user.keyboard('{meta}');
    await user.keyboard('{ctrl>}');
    const isPrevented = fireEvent.wheel(layerContainerRow);
    expect(isPrevented).toBe(true);
  });
});
