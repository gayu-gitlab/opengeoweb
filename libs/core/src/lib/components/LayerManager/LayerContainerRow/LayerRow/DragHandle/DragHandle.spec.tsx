/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import DragHandle, { TOOLTIP_TITLE } from './DragHandle';
import { CoreThemeProvider } from '../../../../Providers/Providers';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/DragHandle', () => {
  it('should render correct component with tooltip', () => {
    render(
      <CoreThemeProvider>
        <DragHandle hideTooltip={false} />
      </CoreThemeProvider>,
    );
    const dragHandleButton = screen.queryByTestId('dragHandle')!;
    expect(dragHandleButton).toBeTruthy();
    expect(dragHandleButton.getAttribute('aria-label')).toEqual(TOOLTIP_TITLE);
  });

  it('should hide tooltip', () => {
    render(
      <CoreThemeProvider>
        <DragHandle hideTooltip />
      </CoreThemeProvider>,
    );
    expect(
      screen.queryByTestId('dragHandle')!.getAttribute('aria-label'),
    ).toEqual('');
  });
  it('should show default tooltip on hover', async () => {
    render(
      <CoreThemeProvider>
        <DragHandle hideTooltip={false} />
      </CoreThemeProvider>,
    );
    fireEvent.mouseOver(screen.getByTestId('dragHandle'));
    expect(await screen.findByText(TOOLTIP_TITLE)).toBeTruthy();
  });
  it('should show custom tooltip on hover', async () => {
    const tooltipTitle = 'Custom tooltip';
    render(
      <CoreThemeProvider>
        <DragHandle hideTooltip={false} tooltipTitle={tooltipTitle} />
      </CoreThemeProvider>,
    );
    fireEvent.mouseOver(screen.getByTestId('dragHandle'));
    expect(await screen.findByText(tooltipTitle)).toBeTruthy();
  });
  it('should show default icon', async () => {
    render(
      <CoreThemeProvider>
        <DragHandle />
      </CoreThemeProvider>,
    );
    expect(screen.getByTestId('dragHandleIcon')).toBeTruthy();
  });
  it('should show custom icon', async () => {
    const testId = 'customIcon';
    render(
      <CoreThemeProvider>
        <DragHandle icon={<span data-testid={testId} />} />
      </CoreThemeProvider>,
    );
    expect(screen.getByTestId(testId)).toBeTruthy();
  });
});
