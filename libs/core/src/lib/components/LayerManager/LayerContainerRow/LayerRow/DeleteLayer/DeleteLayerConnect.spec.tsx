/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { layerActions, layerTypes, storeTestUtils } from '@opengeoweb/store';
import { defaultReduxLayerRadarColor } from '../../../../../utils/defaultTestSettings';

import DeleteLayerConnect from './DeleteLayerConnect';
import { CoreThemeStoreProvider } from '../../../../Providers/Providers';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/MenuButton/DeleteLayer/DeleteLayerConnect', () => {
  it('should delete a layer', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStoreWithEggs(mockState);

    render(
      <CoreThemeStoreProvider store={store}>
        <DeleteLayerConnect mapId={mapId} layerId={layer.id} layerIndex={0} />
      </CoreThemeStoreProvider>,
    );

    const button = screen.getByTestId('deleteButton');

    fireEvent.click(button);

    const expectedAction = [
      layerActions.layerDelete({
        mapId,
        layerId: layer.id,
        layerIndex: 0,
        origin: layerTypes.LayerActionOrigin.layerManager,
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
  it('should show custom icon and tooltip', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStoreWithEggs(mockState);

    render(
      <CoreThemeStoreProvider store={store}>
        <DeleteLayerConnect
          mapId={mapId}
          layerId={layer.id}
          layerIndex={0}
          tooltipTitle="Test tooltip"
          icon={<span data-testid="testIcon" />}
        />
      </CoreThemeStoreProvider>,
    );
    const icon = screen.getByTestId('testIcon');
    expect(icon).toBeTruthy();
    fireEvent.mouseOver(icon);
    expect(await screen.findByText('Test tooltip')).toBeTruthy();
  });
});
