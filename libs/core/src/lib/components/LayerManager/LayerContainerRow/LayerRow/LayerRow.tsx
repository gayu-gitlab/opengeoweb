/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { Box, Grid, Typography } from '@mui/material';
import React, { FC } from 'react';
import {
  AlertBanner,
  CustomAccordion,
  CustomIconButton,
  renderCounter,
} from '@opengeoweb/shared';
import { Delete, Visibility, VisibilityOff } from '@opengeoweb/theme';
import { mapTypes, layerTypes, layerUtils } from '@opengeoweb/store';
import type { serviceTypes } from '@opengeoweb/store';
import { LayerInfoButton } from '@opengeoweb/layer-select';
import DimensionSelect from './DimensionSelect/DimensionSelect';
import RenderLayers from './RenderLayers/RenderLayers';
import OpacitySelect from './OpacitySelect/OpacitySelect';
import LayerManagerMenuButton from './Menubutton/MenuButton';
import RenderStyles from './RenderStyles/RenderStyles';
import { columnClasses, leftButtonsStyle } from '../../LayerManagerUtils';
import ActivateLayer, { AutoOptions } from './ActivateLayer/ActivateLayer';
import { AcceptanceTime } from './AcceptanceTime/AcceptanceTime';
import { MissingData } from './MissingData/MissingData';

const LeftButtons: FC<{
  size?: string;
  layerEnableLayout?: React.ReactElement;
  layerMissingDataLayout?: React.ReactElement;
  layerActiveLayout?: React.ReactElement;
  disableActivateLayer?: boolean;
  isEnabled?: boolean;
  dragHandle?: React.ReactElement;
  layer?: layerTypes.Layer;
  onChangeEnable: (event: React.MouseEvent<HTMLElement>) => void;
  onChangeActive: (autoOption: AutoOptions) => void;
}> = ({
  size,
  dragHandle,
  layer,
  layerMissingDataLayout,
  layerEnableLayout,
  isEnabled,
  disableActivateLayer,
  layerActiveLayout,
  onChangeEnable,
  onChangeActive,
}) => {
  return (
    <Box sx={leftButtonsStyle}>
      {dragHandle}
      {layerMissingDataLayout || (
        <MissingData layerIsInsideAcceptanceTime="equal" />
      )}
      {layerEnableLayout || (
        <CustomIconButton
          shouldShowAsDisabled={!layer!.enabled}
          tooltipTitle={layer!.name!}
          onClick={onChangeEnable}
          data-testid={`enableButton${size}`}
        >
          {isEnabled ? <Visibility /> : <VisibilityOff />}
        </CustomIconButton>
      )}
      {!disableActivateLayer &&
        (layerActiveLayout || (
          <ActivateLayer
            onChange={onChangeActive}
            current={AutoOptions.BOTH}
            isEnabled={true}
          />
        ))}
    </Box>
  );
};

const Styles: FC<{
  style: string;
  onChangeStyle: (style: string) => void;
}> = ({ style, onChangeStyle }) => (
  <RenderStyles
    layerStyles={[]}
    currentLayerStyle={style}
    onChangeLayerStyle={onChangeStyle}
  />
);

const Opacity: FC<{
  opacity: number;
  onChangeOpacity: (opacity: number) => void;
}> = ({ opacity, onChangeOpacity }) => (
  <OpacitySelect
    currentOpacity={opacity}
    onLayerChangeOpacity={onChangeOpacity}
  />
);

const DeleteLayer: FC<{
  onClick: (event: React.MouseEvent<HTMLElement>) => void;
}> = ({ onClick }) => (
  <CustomIconButton
    tooltipTitle="Delete"
    onClick={onClick}
    data-testid="deleteButton"
  >
    <Delete />
  </CustomIconButton>
);

const ShowLayerInfo: FC<{
  onClick: () => void;
}> = ({ onClick }) => <LayerInfoButton onClick={onClick} />;

const Acceptance: FC<{
  acceptanceTime: number | undefined;
  onChangeAcceptanceTime: (acceptanceTime: number | undefined) => void;
}> = ({ acceptanceTime, onChangeAcceptanceTime }) => (
  <AcceptanceTime
    acceptanceTimeInMinutes={acceptanceTime}
    onChangeAcceptanceTime={onChangeAcceptanceTime}
  />
);

interface LayerRowProps {
  layerId?: string;
  layer?: layerTypes.Layer;
  layerName?: string;
  mapId: string;
  onLayerRowClick?: (layerId?: string) => void;
  onLayerEnable?: (payload: { layerId: string; enabled: boolean }) => void;
  onLayerChangeName?: (payload: { layerId: string; name: string }) => void;
  onLayerChangeStyle?: (payload: { layerId: string; style: string }) => void;
  onLayerChangeOpacity?: (payload: {
    layerId: string;
    opacity: number;
  }) => void;
  onLayerChangeDimension?: (payload: {
    origin: string;
    layerId: string;
    dimension: mapTypes.Dimension;
  }) => void;
  onLayerDelete?: (payload: { mapId: string; layerId: string }) => void;
  onLayerDuplicate?: (payload: { mapId: string; layerId: string }) => void;
  onChangeAcceptanceTime?: (payload: {
    layerId: string;
    acceptanceTime: number | undefined;
  }) => void;
  layerEnableLayout?: React.ReactElement;
  layerServicesLayout?: React.ReactElement;
  layerStylesLayout?: React.ReactElement;
  layerOpacityLayout?: React.ReactElement;
  layerDimensionLayout?: React.ReactElement;
  layerAcceptanceTimeLayout?: React.ReactElement;
  layerMissingDataLayout?: React.ReactElement;
  layerDeleteLayout?: React.ReactElement;
  layerShowLayerInfoLayout?: React.ReactElement;
  layerMenuLayout?: React.ReactElement;
  layerActiveLayout?: React.ReactElement;
  disableActivateLayer?: boolean;
  services?: serviceTypes.Services;
  isEnabled?: boolean;
  dragHandle?: React.ReactElement;
  isLayerMissing?: boolean;
}

const LayerRow: FC<LayerRowProps> = ({
  layerId,
  layer,
  layerName,
  services,
  mapId,
  onLayerRowClick = (): void => {},
  isEnabled = true,
  isLayerMissing = false,
  onLayerEnable,
  onLayerChangeName,
  onLayerChangeStyle,
  onLayerChangeOpacity,
  onLayerChangeDimension,
  onLayerDelete,
  onLayerDuplicate,
  onChangeAcceptanceTime,
  // layout
  layerEnableLayout,
  layerServicesLayout,
  layerStylesLayout,
  layerOpacityLayout,
  layerDimensionLayout,
  layerDeleteLayout,
  layerShowLayerInfoLayout,
  layerMenuLayout,
  layerActiveLayout,
  disableActivateLayer,
  dragHandle,
  layerAcceptanceTimeLayout,
  layerMissingDataLayout,
}: LayerRowProps) => {
  const onClickRow = (): void => onLayerRowClick(layerId);

  const changeEnable = (event: React.MouseEvent<HTMLElement>): void => {
    event.stopPropagation();
    onLayerEnable!({ layerId: layerId!, enabled: !isEnabled });
  };

  const changeStyle = (style: string): void => {
    onLayerChangeStyle!({ layerId: layerId!, style });
  };

  const changeOpacity = (opacity: number): void => {
    onLayerChangeOpacity!({
      layerId: layer!.id!,
      opacity,
    });
  };

  const changeAcceptanceTime = (acceptanceTime: number | undefined): void => {
    onChangeAcceptanceTime?.({
      layerId: layerId!,
      acceptanceTime,
    });
  };

  const deleteLayer = (): void => onLayerDelete!({ mapId, layerId: layerId! });

  renderCounter.count(LayerRow.name);

  return (
    <Grid
      item
      container
      data-testid={`layerRow-${layerId}`}
      className="layerRow"
      sx={{
        backgroundColor: isEnabled
          ? 'geowebColors.layerManager.tableRowDefaultCardContainer.fill'
          : 'geowebColors.layerManager.tableRowDisabledCardContainer.fill',
        border: 1,
        borderColor: isEnabled
          ? 'geowebColors.layerManager.tableRowDefaultCardContainer.borderColor'
          : 'geowebColors.layerManager.tableRowDisabledCardContainer.borderColor',
        borderRadius: 1,
        marginBottom: 0.5,
        minHeight: '34px',
        '&.sortable-chosen': {
          boxShadow: 1,
        },
        '&.sortable-ghost': {
          opacity: 0.5,
        },
      }}
      alignItems="center"
      role="listitem"
    >
      <Accordion
        LeftButtons={
          <LeftButtons
            size="-medium"
            dragHandle={dragHandle}
            layer={layer}
            layerMissingDataLayout={layerMissingDataLayout}
            layerEnableLayout={layerEnableLayout}
            isEnabled={isEnabled}
            disableActivateLayer={disableActivateLayer}
            layerActiveLayout={layerActiveLayout}
            onChangeEnable={changeEnable}
            onChangeActive={onClickRow}
          />
        }
        layerName={layerName}
        Styles={
          layerStylesLayout || (
            <Styles style={layer!.style!} onChangeStyle={changeStyle} />
          )
        }
        Opacity={
          layerOpacityLayout || (
            <Opacity
              opacity={layer!.opacity!}
              onChangeOpacity={changeOpacity}
            />
          )
        }
        Acceptance={
          layerAcceptanceTimeLayout || (
            <Acceptance
              acceptanceTime={layer!.acceptanceTimeInMinutes}
              onChangeAcceptanceTime={changeAcceptanceTime}
            />
          )
        }
        ShowLayerInfo={
          layerShowLayerInfoLayout || <ShowLayerInfo onClick={(): void => {}} />
        }
        DeleteLayer={layerDeleteLayout || <DeleteLayer onClick={deleteLayer} />}
      />
      <Grid item className={columnClasses.column1} alignItems="center">
        <LeftButtons
          dragHandle={dragHandle}
          layer={layer}
          layerMissingDataLayout={layerMissingDataLayout}
          layerEnableLayout={layerEnableLayout}
          isEnabled={isEnabled}
          disableActivateLayer={disableActivateLayer}
          layerActiveLayout={layerActiveLayout}
          onChangeEnable={changeEnable}
          onChangeActive={onClickRow}
        />
      </Grid>
      <Grid item className={columnClasses.column2}>
        {layerServicesLayout || (
          <RenderLayers
            layerName={layer!.name!}
            layers={
              (services &&
                layer!.service &&
                services[layer!.service] &&
                services[layer!.service].layers) ||
              []
            }
            onChangeLayerName={(name): void => {
              onLayerChangeName!({ layerId: layer!.id!, name });
            }}
          />
        )}
      </Grid>
      {isLayerMissing ? (
        <Grid item className={columnClasses.column7}>
          <AlertBanner
            title="This layer could not be loaded: does not exist on the server."
            isCompact
          />
        </Grid>
      ) : (
        <>
          <Grid item className={columnClasses.column3}>
            {layerStylesLayout || (
              <Styles style={layer!.style!} onChangeStyle={changeStyle} />
            )}
          </Grid>
          <Grid item className={columnClasses.column4}>
            {layerOpacityLayout || (
              <Opacity
                opacity={layer!.opacity!}
                onChangeOpacity={changeOpacity}
              />
            )}
          </Grid>
          <Grid item className={columnClasses.column5}>
            {layerDimensionLayout || (
              <DimensionSelect
                layerDimensions={layerUtils.filterNonTimeDimensions(
                  layer!.dimensions!,
                )}
                onLayerChangeDimension={(
                  dimensionName: string,
                  dimensionValue: string,
                ): void => {
                  const dimension: mapTypes.Dimension = {
                    name: dimensionName,
                    currentValue: dimensionValue,
                  };
                  onLayerChangeDimension!({
                    origin: 'layerrow',
                    layerId: layerId!,
                    dimension,
                  });
                }}
              />
            )}
          </Grid>
          <Grid item className={columnClasses.acceptanceTime}>
            {layerAcceptanceTimeLayout || (
              <Acceptance
                acceptanceTime={layer!.acceptanceTimeInMinutes}
                onChangeAcceptanceTime={changeAcceptanceTime}
              />
            )}
          </Grid>
        </>
      )}
      <Grid item className={columnClasses.column6} alignItems="center">
        {layerShowLayerInfoLayout || <ShowLayerInfo onClick={(): void => {}} />}
        {layerDeleteLayout || <DeleteLayer onClick={deleteLayer} />}
        {layerMenuLayout || (
          <LayerManagerMenuButton
            mapId={mapId}
            layerId={layerId}
            onLayerDuplicate={onLayerDuplicate}
          />
        )}
      </Grid>
    </Grid>
  );
};

export default LayerRow;

const Accordion: FC<{
  Styles: React.ReactNode;
  ShowLayerInfo: React.ReactNode;
  DeleteLayer: React.ReactNode;
  Acceptance: React.ReactNode;
  Opacity: React.ReactNode;
  layerName?: string;
  LeftButtons: React.ReactNode;
}> = ({
  Styles,
  Acceptance,
  ShowLayerInfo,
  DeleteLayer,
  Opacity,
  layerName,
  LeftButtons,
}) => {
  return (
    <CustomAccordion
      className="medium-layermanager"
      defaultExpanded={false}
      sx={{
        minHeight: 33,
        margin: 0,
        width: '100%',
        border: 'none',
        backgroundColor: 'inherit',
        '.MuiAccordionSummary-content': {
          margin: 0,
          padding: 0,
        },
      }}
      title={
        <>
          {LeftButtons}
          <Typography
            sx={{
              fontWeight: '500',
              fontSize: '12px',
              display: 'flex',
              alignItems: 'center',
            }}
          >
            {layerName}
          </Typography>
        </>
      }
    >
      <Grid container sx={{ padding: '16px 32px' }} spacing={3}>
        <Grid container item>
          <Grid item>Style</Grid>
          {Styles}
        </Grid>
        <Grid
          container
          item
          sx={{
            '.MuiButtonBase-root': {
              justifyContent: 'flex-start',
            },
            '.opacity-select': {
              height: 'auto',
            },
          }}
        >
          <Grid item>Opacity</Grid>
          {Opacity}
        </Grid>
        <Grid container item>
          <Grid item>Acc Time</Grid>
          {Acceptance}
        </Grid>
        <Grid container item justifyContent="center" spacing={2}>
          <Grid item sx={{}}>
            {ShowLayerInfo}
          </Grid>
          <Grid item sx={{}}>
            {DeleteLayer}
          </Grid>
        </Grid>
      </Grid>
    </CustomAccordion>
  );
};
