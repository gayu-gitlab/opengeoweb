/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, waitFor, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import {
  layerActions,
  serviceActions,
  layerTypes,
  storeTestUtils,
  storeTestSettings,
} from '@opengeoweb/store';
import { defaultReduxLayerRadarColor } from '../../../../../utils/defaultTestSettings';

import RenderLayersConnect from './RenderLayersConnect';
import { CoreThemeStoreProvider } from '../../../../Providers/Providers';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/RenderLayers/RenderLayersConnect', () => {
  it('should set layer name', async () => {
    const mapId = 'mapid_1';
    const layer = defaultReduxLayerRadarColor;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStoreWithEggs(mockState);
    const props = {
      mapId: 'test',
      layerId: layer.id,
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <RenderLayersConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const select = screen.getByTestId('selectLayer');

    fireEvent.mouseDown(select);
    const newName =
      storeTestSettings.defaultReduxServices['serviceid_1'].layers![1];

    const menuItem = await screen.findByText(newName.title);

    fireEvent.click(menuItem);

    const expectedAction = [
      layerActions.layerChangeName({
        layerId: layer.id,
        name: newName.name!,
        mapId: props.mapId,
        origin: layerTypes.LayerActionOrigin.layerManager,
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should set service layers in store when layer service is not yet available in store', async () => {
    const mapId = 'mapid_1';
    const layer = {
      ...defaultReduxLayerRadarColor,
      service: 'https://mock-test-service.nl',
    };
    const jsonData = {
      WMS_Capabilities: {
        attr: { version: '1.3.0' },
        Capability: {
          Layer: {},
        },
      },
    };
    // mock the getcapreq fetch with a succes response
    window.fetch = jest.fn().mockResolvedValue({
      text: () => Promise.resolve(jsonData),
    });
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStoreWithEggs(mockState);
    const props = {
      mapId: 'test',
      layerId: layer.id,
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <RenderLayersConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(screen.getByText('No service available')).toBeTruthy();

    const expectedAction = [
      serviceActions.serviceSetLayers(
        expect.objectContaining({ serviceUrl: layer.service, scope: 'user' }),
      ),
    ];
    await waitFor(() => expect(store.getActions()).toEqual(expectedAction));
  });
});
