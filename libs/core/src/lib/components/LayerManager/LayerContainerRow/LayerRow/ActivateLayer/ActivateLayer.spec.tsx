/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import ActivateLayer, { AutoOptions } from './ActivateLayer';
import { CoreThemeProvider } from '../../../../Providers/Providers';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/ActivateLayer/ActivateLayer', () => {
  const user = userEvent.setup();

  it('should show tooltip and call function when clicking button', async () => {
    const onClickActivate = jest.fn();
    render(
      <CoreThemeProvider>
        <ActivateLayer
          onChange={onClickActivate}
          current={AutoOptions.BOTH}
          isEnabled={true}
        />
      </CoreThemeProvider>,
    );
    const button = screen.getByRole('button', { name: /Auto both/i });

    fireEvent.mouseOver(button);
    expect(await screen.findByRole('tooltip')).toHaveTextContent(
      /Leading layer/i,
    );

    await user.click(button);
    await user.click(screen.getByRole('option', { name: /none/i }));
    expect(onClickActivate).toBeCalledWith('Auto none');
  });
});
