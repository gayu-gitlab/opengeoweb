/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { storeTestSettings } from '@opengeoweb/store';
import LayerRow from './LayerRow';
import { CoreThemeProvider } from '../../../Providers/Providers';

describe('src/components/LayerManager/LayerContainerRow/LayerRow/LayerRow', () => {
  const props = {
    mapId: 'mapId_1',
    layer: storeTestSettings.layerWithoutTimeDimension,
    layerId: storeTestSettings.layerWithoutTimeDimension.id,
    onLayerChangeDimension: jest.fn(),
  };

  const user = userEvent.setup();

  it('should render correct component and subcomponents', () => {
    render(
      <CoreThemeProvider>
        <LayerRow {...props} />
      </CoreThemeProvider>,
    );
    expect(
      screen.getByTestId(
        `layerRow-${storeTestSettings.layerWithoutTimeDimension.id}`,
      ),
    ).toBeTruthy();
    expect(screen.getAllByTestId('deleteButton')).toBeTruthy();
    expect(screen.getByRole('button', { name: 'layer info' })).toBeTruthy();
  });

  it('should be able to click on a row', async () => {
    const defaultProps = {
      services: storeTestSettings.defaultReduxServices,
      layer: storeTestSettings.layerWithoutTimeDimension,
      mapId: 'map_1',
      layerId: 'test-1',
      onLayerRowClick: jest.fn(),
      onLayerChangeDimension: jest.fn(),
    };

    render(
      <CoreThemeProvider>
        <LayerRow {...defaultProps} />
      </CoreThemeProvider>,
    );

    const button = screen.getAllByRole('button', { name: 'Auto both' })[1];
    await user.click(button);
    await user.click(screen.getByRole('option', { name: /auto-update/i }));
    expect(defaultProps.onLayerRowClick).toHaveBeenCalledWith(
      defaultProps.layerId,
    );
  });

  it('should be able to pass a draghandle', async () => {
    const TestComponent: React.ReactElement = <div>test draghandle</div>;
    const testProps = {
      ...props,
      dragHandle: TestComponent,
    };
    render(
      <CoreThemeProvider>
        <LayerRow {...testProps} />
      </CoreThemeProvider>,
    );
    expect(await screen.findAllByText('test draghandle')).toBeTruthy();
  });

  it('should contain an alert banner if selected layer is missing from service', () => {
    const propsWithMissingLayer = { ...props, isLayerMissing: true };
    render(
      <CoreThemeProvider>
        <LayerRow {...propsWithMissingLayer} />
      </CoreThemeProvider>,
    );
    expect(screen.getByRole('alert')).toBeTruthy();
  });

  it('should contain no alert banner if selected layer is not missing from service', () => {
    const propsWithNoMissingLayer = { ...props, isLayerMissing: false };
    render(
      <CoreThemeProvider>
        <LayerRow {...propsWithNoMissingLayer} />
      </CoreThemeProvider>,
    );
    expect(screen.queryByRole('alert')).toBeNull();
  });
  it('should render custom ActivateLayer', () => {
    const testId = 'testActivateLayer';
    const testProps = {
      ...props,
      layerActiveLayout: <span data-testid={testId} />,
    };
    render(
      <CoreThemeProvider>
        <LayerRow {...testProps} />
      </CoreThemeProvider>,
    );
    expect(screen.getAllByTestId(testId).length).toEqual(2);
  });
  it('should disable ActivateLayer', () => {
    const testId = 'testActivateLayer';
    const testProps = {
      ...props,
      layerActiveLayout: <span data-testid={testId} />,
    };
    render(
      <CoreThemeProvider>
        <LayerRow {...testProps} disableActivateLayer={true} />
      </CoreThemeProvider>,
    );
    expect(screen.queryAllByTestId(testId).length).toEqual(0);
  });
});
