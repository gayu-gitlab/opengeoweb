/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, screen, act, fireEvent } from '@testing-library/react';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import { createStore } from '@redux-eggs/redux-toolkit';
import { webmapUtils, WMJSMap, WMLayer } from '@opengeoweb/webmap';
import { layerActions, mapActions } from '@opengeoweb/store';
import { multiDimensionLayer2 as layer } from '../../../../../utils/defaultTestSettings';

import CustomDimensionSelectConnect from './CustomDimensionSelectConnect';
import { CoreThemeStoreProvider } from '../../../../Providers/Providers';

const initMap = (
  mapId: string,
  mapElement: HTMLElement,
  store: ReturnType<typeof createStore>,
): WMJSMap => {
  const origin = 'origin';
  const wmjsMap = new WMJSMap(mapElement);
  const wmLayer = new WMLayer({
    id: layer.id!,
    layerType: layer.layerType!,
    service: layer.service!,
    active: true,
    dimensions: layer.dimensions,
  });

  wmjsMap.setLayer(wmLayer);
  webmapUtils.registerWMLayer(wmLayer, layer.id!);
  webmapUtils.registerWMJSMap(wmjsMap, mapId);

  act(() => {
    store.dispatch(mapActions.registerMap({ mapId }));

    store.dispatch(
      layerActions.addLayer({
        mapId,
        layerId: layer.id!,
        layer,
        origin,
      }),
    );
  });

  return wmjsMap;
};

describe('src/components/LayerManager/LayerContainerRow/LayerRow/CustomDimensionSelect/CustomDimensionSelectConnect', () => {
  it('should allow filtering dimensions', async () => {
    const store = createStore({
      extensions: [getSagaExtension({})],
    });

    const mapId = 'mapid_1';

    const props = {
      layerId: layer.id!,
      mapId,
      dimensionsToShow: ['elevation'],
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <div data-testid="mapElement" />
        <CustomDimensionSelectConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const mapElement = screen.getByTestId('mapElement');
    initMap(mapId, mapElement, store);

    const select = screen.getByRole('button', { name: 'elevation' });
    fireEvent.mouseDown(select);
    const options = screen.getAllByRole('option');
    expect(options.length).toEqual(2); // one header options
  });

  it('should show custom single value component', async () => {
    const mapId = 'mapid_2';

    const store = createStore({
      extensions: [getSagaExtension({})],
    });

    const TestComponent: React.FC<{ currentValue: string }> = ({
      currentValue,
    }) => <span data-testid="testComponent">{currentValue}</span>;

    const props = {
      layerId: layer.id!,
      mapId: 'test-1',
      dimensionsToShow: ['elevation'],
      SingleValueComponent: TestComponent,
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <div data-testid="mapElement" />
        <CustomDimensionSelectConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    const mapElement = screen.getByTestId('mapElement');
    initMap(mapId, mapElement, store);

    const expectedTestComponent = screen.getByTestId('testComponent');
    expect(expectedTestComponent).toBeTruthy();
    expect(expectedTestComponent.innerHTML).toMatch('9000');
  });
});
