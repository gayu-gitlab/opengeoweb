/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  render,
  fireEvent,
  screen,
  waitFor,
  act,
} from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { uiActions, uiTypes } from '@opengeoweb/store';
import { createStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import LayerManager from './LayerManager';
import { CoreThemeStoreProvider } from '../Providers/Providers';

describe('src/components/LayerManager', () => {
  const props = {
    mapId: 'mapId_1',
    isOpen: true,
    onClose: (): void => {},
    showTitle: false,
    onMouseDown: jest.fn(),
    onToggleDock: jest.fn(),
    setFocused: jest.fn(),
  };

  it('should render required components and by default render undocked version', async () => {
    const mockState = {
      ui: {
        legend: {
          type: 'legend' as uiTypes.DialogType,
          activeMapId: 'map1',
          isOpen: false,
        },
        layerManager: {
          type: uiTypes.DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} />
      </CoreThemeStoreProvider>,
    );
    expect(screen.getByTestId('layerManagerRowContainer')).toBeTruthy();
    expect(screen.getByTestId('layerContainerRow')).toBeTruthy();
    expect(screen.getByTestId('baseLayerRow')).toBeTruthy();
    expect(screen.getByTestId('descriptionRow')).toBeTruthy();
    expect(screen.queryByTestId('mappresets-menubutton')).toBeFalsy();
    expect(screen.getByTestId('dockedLayerManager-uncollapse')).toBeTruthy();

    fireEvent.mouseDown(screen.queryByTestId('layerManagerWindow')!);
    expect(props.onMouseDown).toHaveBeenCalled();
  });

  it('should render component in leftHeaderComponent slot', async () => {
    const props = {
      mapId: 'mapId_1',
      isOpen: true,
      onClose: (): void => {},
      showTitle: false,
      onMouseDown: jest.fn(),
      setFocused: jest.fn(),
      leftHeaderComponent: <div>test component</div>,
    };
    const mockState = {
      ui: {
        layerManager: {
          type: uiTypes.DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(await screen.findAllByText('test component')).toBeTruthy();
  });

  it('should show the loading indicator when loading', async () => {
    const props = {
      mapId: 'mapId_3',
      isOpen: true,
      onClose: (): void => {},
      showTitle: false,
      onMouseDown: jest.fn(),
      setFocused: jest.fn(),
      isLoading: true,
    };
    const mockState = {
      ui: {
        layerManager: {
          type: uiTypes.DialogTypes.LayerManager,
          activeMapId: 'mapId_3',
          isOpen: true,
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(await screen.findByTestId('loading-bar')).toBeTruthy();
  });

  it('should show the alert banner when error given', async () => {
    const props = {
      mapId: 'mapId_3',
      isOpen: true,
      onClose: (): void => {},
      showTitle: false,
      onMouseDown: jest.fn(),
      setFocused: jest.fn(),
      error: 'Test error message.',
    };
    const mockState = {
      ui: {
        layerManager: {
          type: uiTypes.DialogTypes.LayerManager,
          activeMapId: 'mapId_3',
          isOpen: true,
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(await screen.findByText(props.error)).toBeTruthy();
    expect(await screen.findByTestId('alert-banner')).toBeTruthy();
  });

  it('should fire appropriate actions when clicking docking button', async () => {
    const mockState = {
      ui: {
        legend: {
          type: 'legend' as uiTypes.DialogType,
          activeMapId: 'map1',
          isOpen: false,
        },
        layerManager: {
          type: uiTypes.DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} />
      </CoreThemeStoreProvider>,
    );

    fireEvent.click(screen.queryByTestId('dockedBtn')!);
    expect(props.onToggleDock).toHaveBeenCalled();
  });

  it('should show the correct icon for docked version', async () => {
    const mockState = {
      ui: {
        legend: {
          type: 'legend' as uiTypes.DialogType,
          activeMapId: 'map1',
          isOpen: false,
        },
        layerManager: {
          type: uiTypes.DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} isDockedLayerManager />
      </CoreThemeStoreProvider>,
    );

    expect(screen.getByTestId('dockedLayerManager-collapse')).toBeTruthy();
  });

  it('should show default title', async () => {
    const mockState = {
      ui: {
        legend: {
          type: 'legend' as uiTypes.DialogType,
          activeMapId: 'map1',
          isOpen: false,
        },
        layerManager: {
          type: uiTypes.DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} isDockedLayerManager />
      </CoreThemeStoreProvider>,
    );

    expect(
      screen.getByRole('heading', { name: /Layer manager/i }).innerHTML,
    ).toMatch(/Layer Manager/);
  });
  it('should show custom title', async () => {
    const mockState = {
      ui: {
        legend: {
          type: 'legend' as uiTypes.DialogType,
          activeMapId: 'map1',
          isOpen: false,
        },
        layerManager: {
          type: uiTypes.DialogTypes.LayerManager,
          activeMapId: 'map2',
          isOpen: true,
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager {...props} isDockedLayerManager title="Test title" />
      </CoreThemeStoreProvider>,
    );

    expect(
      screen.getByRole('heading', { name: /Test title/i }).innerHTML,
    ).toMatch(/Test title/);
  });
  it('should auto adjust size of docked Layer Manager to sizeLarge depending on the size of the map', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });

    act(() => {
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: 'layerManager',
          mapId: 'map1',
          setOpen: true,
          source: 'app',
        }),
      );
    });

    const mockMapWidthRef = {
      current: {
        clientWidth: 1000, // Initial mock value
      },
    } as React.RefObject<HTMLDivElement>;

    // Use this mock object in your render function
    const { rerender } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager
          {...props}
          isDockedLayerManager
          mapWidthRef={mockMapWidthRef} // Pass the mock object instead of an actual ref
        />
      </CoreThemeStoreProvider>,
    );

    const mockMapWidthRef2 = {
      current: {
        clientWidth: 1001, // Initial mock value
      },
    } as React.RefObject<HTMLDivElement>;

    // Rerender with the updated mock object if necessary
    rerender(
      <CoreThemeStoreProvider store={store}>
        <LayerManager
          {...props}
          isDockedLayerManager
          mapWidthRef={mockMapWidthRef2} // The component uses the updated mock object
        />
      </CoreThemeStoreProvider>,
    );

    await waitFor(() => {
      const layerManagerWindow = screen.getByTestId('layerManagerWindow');
      expect(layerManagerWindow).toHaveAttribute('data-size', 'sizeLarge');
    });
  });

  it('should auto adjust size of docked Layer Manager to sizeMedium depending on the size of the map', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });

    act(() => {
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: 'layerManager',
          mapId: 'map1',
          setOpen: true,
          source: 'app',
        }),
      );
    });

    const mockMapWidthRef = {
      current: {
        clientWidth: 500, // Initial mock value
      },
    } as React.RefObject<HTMLDivElement>;

    // Use this mock object in your render function
    const { rerender } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager
          {...props}
          isDockedLayerManager
          mapWidthRef={mockMapWidthRef} // Pass the mock object instead of an actual ref
        />
      </CoreThemeStoreProvider>,
    );

    const mockMapWidthRef2 = {
      current: {
        clientWidth: 501, // Initial mock value
      },
    } as React.RefObject<HTMLDivElement>;

    // Rerender with the updated mock object if necessary
    rerender(
      <CoreThemeStoreProvider store={store}>
        <LayerManager
          {...props}
          isDockedLayerManager
          mapWidthRef={mockMapWidthRef2} // The component uses the updated mock object
        />
      </CoreThemeStoreProvider>,
    );

    await waitFor(() => {
      const layerManagerWindow = screen.getByTestId('layerManagerWindow');
      expect(layerManagerWindow).toHaveAttribute('data-size', 'sizeMedium');
    });
  });

  it('should auto adjust size of docked Layer Manager to sizeSmall depending on the size of the map', async () => {
    const store = createStore({
      extensions: [getSagaExtension()],
    });

    act(() => {
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: 'layerManager',
          mapId: 'map1',
          setOpen: true,
          source: 'app',
        }),
      );
    });

    const mockMapWidthRef = {
      current: {
        clientWidth: 200, // Initial mock value
      },
    } as React.RefObject<HTMLDivElement>;

    // Use this mock object in your render function
    const { rerender } = render(
      <CoreThemeStoreProvider store={store}>
        <LayerManager
          {...props}
          isDockedLayerManager
          mapWidthRef={mockMapWidthRef} // Pass the mock object instead of an actual ref
        />
      </CoreThemeStoreProvider>,
    );

    const mockMapWidthRef2 = {
      current: {
        clientWidth: 201, // Initial mock value
      },
    } as React.RefObject<HTMLDivElement>;

    // Rerender with the updated mock object if necessary
    rerender(
      <CoreThemeStoreProvider store={store}>
        <LayerManager
          {...props}
          isDockedLayerManager
          mapWidthRef={mockMapWidthRef2} // The component uses the updated mock object
        />
      </CoreThemeStoreProvider>,
    );
    await waitFor(() => {
      const layerManagerWindow = screen.getByTestId('layerManagerWindow');
      expect(layerManagerWindow).toHaveAttribute('data-size', 'sizeSmall');
    });
  });
});
