/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Grid, Divider, Typography } from '@mui/material';

import { useDispatch, useSelector } from 'react-redux';
import {
  CoreAppStore,
  defaultLayers,
  drawtoolActions,
  drawtoolSelectors,
  layerSelectors,
} from '@opengeoweb/store';

import { CustomIconButton } from '@opengeoweb/shared';
import {
  GeoJSONTextField,
  publicLayers,
  StoryLayoutGrid,
  IntersectionSelect,
  startToolExampleConfig,
  endToolExampleConfig,
  exampleIntersectionsMultiDrawTool,
  addFeatureProperties,
  firButtonDrawId,
  getFirTitle,
  getDoubleControlToolIcon,
  MapDrawToolOptions,
} from '@opengeoweb/webmap-react';

import { CoreThemeStoreProvider } from '../Providers/Providers';

import { MapViewConnect } from '../MapView';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import { store } from '../../storybookUtils/store';

export default {
  title: 'components/MapDrawToolRedux/multiple drawtools',
};

const mapId = 'test-map-id';
const startDrawToolId = `drawtool-1`;
const endDrawToolId = `drawtool-2`;

const getGeoJSONProperties = (
  properties: GeoJSON.GeoJsonProperties,
  config: MapDrawToolOptions,
): GeoJSON.GeoJsonProperties => {
  // get the properties from current shape or initial config
  return Object.keys(properties!).length
    ? properties
    : config.defaultGeoJSON?.features[0].properties!;
};

const BasicMapDrawToolStory: React.FC = () => {
  const dispatch = useDispatch();

  useDefaultMapSettings({
    mapId,
    layers: [{ ...publicLayers.radarLayer, id: `radar-${mapId}` }],
    baseLayers: [
      { ...defaultLayers.baseLayerGrey, id: `baseGrey-${mapId}` },
      defaultLayers.overLayer,
    ],
  });
  React.useEffect(() => {
    // register start draw tool
    dispatch(
      drawtoolActions.registerDrawTool({
        mapId,
        drawToolId: startDrawToolId,
        geoJSONLayerId: 'start-draw-layer',
        geoJSONIntersectionLayerId: 'start-intersection-layer',
        geoJSONIntersectionBoundsLayerId: 'static-intersection-layer',
        ...startToolExampleConfig,
      }),
    );

    // register end draw tool
    dispatch(
      drawtoolActions.registerDrawTool({
        mapId,
        drawToolId: endDrawToolId,
        geoJSONLayerId: 'end-draw-layer',
        geoJSONIntersectionLayerId: 'end-intersection-layer',
        geoJSONIntersectionBoundsLayerId: 'static-intersection-layer',
        ...endToolExampleConfig,
      }),
    );

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  // start tool selectors
  const startDrawTool = useSelector((store: CoreAppStore) =>
    drawtoolSelectors.selectDrawToolById(store, startDrawToolId),
  );

  const startGeoJSON = useSelector((store: CoreAppStore) =>
    layerSelectors.getFeatureLayerGeoJSON(store, startDrawTool?.geoJSONLayerId),
  );

  const startGeoJSONProperties = useSelector((store: CoreAppStore) =>
    layerSelectors.getFeatureLayerGeoJSONProperties(
      store,
      startDrawTool?.geoJSONLayerId,
    ),
  );

  const startGeoJSONIntersection = useSelector((store: CoreAppStore) =>
    layerSelectors.getFeatureLayerGeoJSON(
      store,
      startDrawTool?.geoJSONIntersectionLayerId,
    ),
  );
  // end tool selectors
  const endDrawTool = useSelector((store: CoreAppStore) =>
    drawtoolSelectors.selectDrawToolById(store, endDrawToolId),
  );

  const endGeoJSON = useSelector((store: CoreAppStore) =>
    layerSelectors.getFeatureLayerGeoJSON(store, endDrawTool?.geoJSONLayerId),
  );

  const endGeoJSONProperties = useSelector((store: CoreAppStore) =>
    layerSelectors.getFeatureLayerGeoJSONProperties(
      store,
      endDrawTool?.geoJSONIntersectionLayerId,
    ),
  );

  const endGeoJSONIntersection = useSelector((store: CoreAppStore) =>
    layerSelectors.getFeatureLayerGeoJSON(
      store,
      endDrawTool?.geoJSONIntersectionLayerId,
    ),
  );
  // actions
  const onChangeDrawMode = (drawToolId: string, drawModeId: string): void => {
    dispatch(
      drawtoolActions.changeDrawToolMode({
        drawToolId,
        drawModeId,
      }),
    );
  };

  const updateFirDrawToolWithNewShape = (
    drawToolId: string,
    shape: GeoJSON.FeatureCollection,
    title: string,
  ): void => {
    dispatch(
      drawtoolActions.updateDrawToolMode({
        drawToolId,
        drawModeId: firButtonDrawId,
        shape,
        title,
      }),
    );
  };

  const changeIntersectionBounds = (
    drawtoolId: string,
    geoJSON: GeoJSON.FeatureCollection,
  ): void => {
    dispatch(
      drawtoolActions.changeIntersectionBounds({
        drawToolId: drawtoolId,
        geoJSON,
      }),
    );
  };

  // methods
  const disableStartTool = (): void => {
    onChangeDrawMode(startDrawToolId, '');
  };

  const disableEndTool = (): void => {
    onChangeDrawMode(endDrawToolId, '');
  };

  const onChangeIntersection = (
    geoJSONIntersection: GeoJSON.FeatureCollection,
    newIntersectionTitle: string,
  ): void => {
    // deactivate tool modes
    disableStartTool();
    disableEndTool();

    // change intersection bounds
    changeIntersectionBounds(startDrawToolId, geoJSONIntersection);
    changeIntersectionBounds(endDrawToolId, geoJSONIntersection);

    // update controls with new shape for fir

    const isNL = newIntersectionTitle === 'NL';
    const newFirTooltipTitle = getFirTitle(isNL);
    const startProperties = getGeoJSONProperties(
      startGeoJSONProperties,
      startToolExampleConfig,
    );
    const endProperties = getGeoJSONProperties(
      endGeoJSONProperties,
      endToolExampleConfig,
    );
    updateFirDrawToolWithNewShape(
      startDrawToolId,
      addFeatureProperties(geoJSONIntersection, startProperties!),
      newFirTooltipTitle,
    );
    updateFirDrawToolWithNewShape(
      endDrawToolId,
      addFeatureProperties(geoJSONIntersection, endProperties!),
      newFirTooltipTitle,
    );
  };

  const onChangeStartTool = (drawModeId: string): void => {
    disableEndTool();
    onChangeDrawMode(startDrawToolId, drawModeId);
  };

  const onChangeEndTool = (drawModeId: string): void => {
    disableStartTool();
    onChangeDrawMode(endDrawToolId, drawModeId);
  };
  return (
    <StoryLayoutGrid mapComponent={<MapViewConnect mapId={mapId} />}>
      <IntersectionSelect
        intersections={exampleIntersectionsMultiDrawTool}
        onChangeIntersection={onChangeIntersection}
        isDisabled={
          startDrawTool?.activeDrawModeId !== '' ||
          endDrawTool?.activeDrawModeId !== ''
        }
      />
      <Grid item xs={12}>
        <Typography sx={{ margin: 1, marginLeft: 0 }}>Start geoJSON</Typography>
        {startDrawTool?.drawModes.map((mode) => (
          <CustomIconButton
            key={mode.drawModeId}
            variant="tool"
            tooltipTitle={mode.title}
            onClick={(): void => onChangeStartTool(mode.drawModeId)}
            isSelected={startDrawTool?.activeDrawModeId === mode.drawModeId}
            sx={{ marginRight: 1, marginBottom: 1 }}
          >
            {getDoubleControlToolIcon(mode)}
          </CustomIconButton>
        ))}

        <Divider sx={{ marginBottom: 1 }} />
        <GeoJSONTextField
          title="start geoJSON result"
          geoJSON={startGeoJSON as GeoJSON.FeatureCollection}
          sx={{ maxHeight: 200, height: 200, overflow: 'hidden' }}
          maxRows={7}
        />

        <GeoJSONTextField
          title="start intersection geoJSON result"
          geoJSON={startGeoJSONIntersection as GeoJSON.FeatureCollection}
          sx={{ maxHeight: 200, height: 200, overflow: 'hidden' }}
          maxRows={7}
        />
      </Grid>

      <Grid item xs={12}>
        <Typography sx={{ margin: 1, marginLeft: 0 }}>End geoJSON</Typography>
        {endDrawTool?.drawModes.map((mode) => (
          <CustomIconButton
            key={mode.drawModeId}
            variant="tool"
            tooltipTitle={mode.title}
            onClick={(): void => onChangeEndTool(mode.drawModeId)}
            isSelected={endDrawTool?.activeDrawModeId === mode.drawModeId}
            sx={{ marginRight: 1, marginBottom: 1 }}
          >
            {getDoubleControlToolIcon(mode)}
          </CustomIconButton>
        ))}

        <Divider sx={{ marginBottom: 1 }} />

        <GeoJSONTextField
          title="end geoJSON result"
          geoJSON={endGeoJSON as GeoJSON.FeatureCollection}
          sx={{ maxHeight: 200, height: 200, overflow: 'hidden' }}
          maxRows={7}
        />

        <GeoJSONTextField
          title="end intersection geoJSON result"
          geoJSON={endGeoJSONIntersection as GeoJSON.FeatureCollection}
          sx={{ maxHeight: 200, height: 200, overflow: 'hidden' }}
          maxRows={7}
        />
      </Grid>
    </StoryLayoutGrid>
  );
};

export const MultipleDrawToolsDemo = (): React.ReactElement => (
  <CoreThemeStoreProvider store={store}>
    <BasicMapDrawToolStory />
  </CoreThemeStoreProvider>
);
