/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Grid } from '@mui/material';
import { useSelector } from 'react-redux';
import { Link, LinkOff } from '@opengeoweb/theme';
import { CustomIconButton } from '@opengeoweb/shared';

import { CoreAppStore, layerSelectors } from '@opengeoweb/store';
import { webmapUtils } from '@opengeoweb/webmap';
import { DimensionConfig } from './MultiDimensionSelectConfig';
import DimensionSelectSlider from './DimensionSelectSlider';
import { marksByDimension } from '../../utils/dimensionUtils';

const Icon: React.FC<{ isLayerDimensionSynced: boolean }> = ({
  isLayerDimensionSynced,
}) =>
  isLayerDimensionSynced ? (
    <Link data-testid="syncIcon" />
  ) : (
    <LinkOff data-testid="syncDisIcon" />
  );

export interface DimensionSelectSliderConnectProps {
  layerId: string;
  dimConfig: DimensionConfig;
  dimensionName: string;
  handleDimensionValueChanged: (
    layerId: string,
    dimensionName: string,
    dimensionValue: string,
  ) => void;
  handleSyncChanged: (
    layerId: string,
    dimensionName: string,
    dimensionValue: string,
    synced: boolean,
  ) => void;
}

const DimensionSelectSliderConnect: React.FC<DimensionSelectSliderConnectProps> =
  ({
    layerId,
    dimConfig,
    dimensionName,
    handleDimensionValueChanged,
    handleSyncChanged,
  }: DimensionSelectSliderConnectProps) => {
    const layerDimension = useSelector((store: CoreAppStore) =>
      layerSelectors.getLayerDimension(store, layerId, dimensionName),
    );
    const wmLayer = webmapUtils.getWMLayerById(layerId);
    if (!layerDimension || !wmLayer) {
      return null;
    }
    const wmsDimension = wmLayer.getDimension(dimensionName);
    if (!wmsDimension) {
      return null;
    }
    const layerTitle = wmLayer.title!;
    const marks = marksByDimension(wmsDimension);
    const isLayerDimensionSynced =
      layerDimension.synced !== undefined && layerDimension.synced === true;

    const tooltipTitle = isLayerDimensionSynced
      ? 'Click to disconnect layer'
      : 'Click to connect layer';

    return (
      <Grid key={layerId} item xs="auto">
        <CustomIconButton
          tooltipTitle={tooltipTitle}
          data-testid="syncButton"
          onClick={(): void =>
            handleSyncChanged(
              layerId,
              dimensionName,
              layerDimension.currentValue,
              !layerDimension.synced,
            )
          }
          sx={{ margin: '5px' }}
        >
          <Icon isLayerDimensionSynced={isLayerDimensionSynced} />
        </CustomIconButton>

        <DimensionSelectSlider
          marks={marks}
          layerTitle={layerTitle}
          reverse={dimConfig ? dimConfig.reversed : false}
          managedValue={
            Number(layerDimension.currentValue) || layerDimension.currentValue
          }
          onChangeDimensionValue={(value): void => {
            handleDimensionValueChanged(layerId, dimensionName, value);
          }}
          isDisabled={false}
          validSelection={
            layerDimension.synced === false ||
            !(
              layerDimension.validSyncSelection !== undefined &&
              layerDimension.validSyncSelection === false
            )
          }
        />
      </Grid>
    );
  };

export default DimensionSelectSliderConnect;
