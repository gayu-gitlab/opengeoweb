/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { storeTestSettings, uiTypes } from '@opengeoweb/store';
import { webmapUtils } from '@opengeoweb/webmap';
import {
  flDimensionLayer,
  multiDimensionLayer,
  WmMultiDimensionLayer3,
} from '../../utils/defaultTestSettings';
import { mockStateMapWithDimensions } from '../../utils/testUtils';
import { dimensionConfig } from './MultiDimensionSelectConfig';
import { CoreThemeStoreProvider } from '../Providers/Providers';
import DimensionSelectSliderConnect from './DimensionSelectSliderConnect';

describe('src/components/MultiMapDimensionSelect/DimensionSelectSliderConnect', () => {
  it('should render a slider if the layer has the passed dimension', () => {
    const mapId = 'mapid_1';
    webmapUtils.registerWMLayer(
      storeTestSettings.WmMultiDimensionLayer,
      'multiDimensionLayerMock',
    );
    const mockState2 = mockStateMapWithDimensions(multiDimensionLayer, mapId);
    const mockState = {
      ...mockState2,
      ui: {
        order: [
          uiTypes.DialogTypes.DimensionSelectElevation,
          uiTypes.DialogTypes.DimensionSelectEnsembleMember,
        ],
        dialogs: {
          [uiTypes.DialogTypes.DimensionSelectElevation]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectElevation,
          },
          [uiTypes.DialogTypes.DimensionSelectEnsembleMember]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectEnsembleMember,
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    const dimConfig = dimensionConfig.find((cnf) => cnf.name === 'elevation')!;
    const props = {
      layerId: 'multiDimensionLayerMock',
      dimConfig,
      dimensionName: 'elevation',
      handleDimensionValueChanged: jest.fn(),
      handleSyncChanged: jest.fn(),
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <DimensionSelectSliderConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(screen.getByTestId('slider-dimensionSelect')).toBeTruthy();
    expect(screen.getByTestId('syncButton')).toBeTruthy();
  });

  it('should only render slider for elevation for layers with elevation dimension', () => {
    const mapId = 'mapid_1';
    webmapUtils.registerWMLayer(
      WmMultiDimensionLayer3,
      'multiDimensionLayerMock',
    );
    const mockState = mockStateMapWithDimensions(flDimensionLayer, mapId);
    const store = createMockStoreWithEggs(mockState);
    const dimConfig = dimensionConfig.find((cnf) => cnf.name === 'elevation')!;
    const props = {
      layerId: 'multiDimensionLayerMock',
      dimConfig,
      dimensionName: 'elevation',
      handleDimensionValueChanged: jest.fn(),
      handleSyncChanged: jest.fn(),
    };

    render(
      <CoreThemeStoreProvider store={store}>
        <DimensionSelectSliderConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(screen.queryByTestId('slider-dimensionSelect')).toBeFalsy();
  });

  it('should call handleSyncChanged when clicking on the sync button', () => {
    const mapId = 'mapid_1';
    webmapUtils.registerWMLayer(
      storeTestSettings.WmMultiDimensionLayer,
      'multiDimensionLayerMock',
    );
    const mockState2 = mockStateMapWithDimensions(multiDimensionLayer, mapId);
    const mockState = {
      ...mockState2,
      ui: {
        order: [
          uiTypes.DialogTypes.DimensionSelectElevation,
          uiTypes.DialogTypes.DimensionSelectEnsembleMember,
        ],
        dialogs: {
          [uiTypes.DialogTypes.DimensionSelectElevation]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectElevation,
          },
          [uiTypes.DialogTypes.DimensionSelectEnsembleMember]: {
            activeMapId: mapId,
            isOpen: true,
            type: uiTypes.DialogTypes.DimensionSelectEnsembleMember,
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    const dimConfig = dimensionConfig.find((cnf) => cnf.name === 'elevation')!;
    const props = {
      layerId: 'multiDimensionLayerMock',
      dimConfig,
      dimensionName: 'elevation',
      handleDimensionValueChanged: jest.fn(),
      handleSyncChanged: jest.fn(),
    };
    render(
      <CoreThemeStoreProvider store={store}>
        <DimensionSelectSliderConnect {...props} />
      </CoreThemeStoreProvider>,
    );

    expect(screen.getByTestId('slider-dimensionSelect')).toBeTruthy();
    expect(screen.getByTestId('syncButton')).toBeTruthy();

    fireEvent.click(screen.getByTestId('syncButton'));
    expect(props.handleSyncChanged).toHaveBeenCalled();
  });
});
