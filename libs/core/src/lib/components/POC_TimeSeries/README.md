# Description of timeseries test data for graph library comparison

## Format

The timeseries data is formatted as OMSF-JSON (see: https://github.com/opengeospatial/omsf-profile/tree/master/omsf-json). This uses GeoJSON point elements to encode timeseries for a point, specifying the data in the `properties` object of the Point features. The time steps of the data are in the `timeStep` array, the time series values are in the `value` array; both inside the `properties` element.
Each feature contains 1 timeseries. Features belonging together (for example needed for one graph) will be collected in a GeoJSON FeatureCollection.

## Data

There are a number of data files, each for a different graph. Each file is a FeatureColection of features for one of the graph types used in the graph library comparison.

## Data files and graphs

The graph library comparison should try to create the graphs mentioned in https://gitlab.com/opengeoweb/geoweb-assets/-/issues/1382

The graphs and the corresponding data files are:

- Meteogram: meteogram.json
- Stacked Probability bars: stacked.json
- Ensemble lines: ensemble.json
- Ensemble area and reference: ensemble_band.json
- Waveform - earthquakes (KNMI): waveform.json

### File meteogram.json

Has timeseries from HARMONIE for:

- cloud cover: cloud_area_fraction
- rain: precipitation_flux
- icon for weathertype: weathertype (random value between 0 and 10 inclusive)
- wind_speed: wind\_\_at_10m_speed
- wind direction: wind\_\_at_10m_direction
- temperature: air_temperature\_\_at_2m

### File ensemble.json

Has ensemble timeseries from ECMWF with 50 members, 1 control run and the operational run for Precipitation and MaximumTemperature.
The type of the timeseries is indicated by
feature["properties"]["observedpropertymodifier"] which can have values "member", "control", "operational" and "percentile". The member number or percentile value is coded in the field feature["properties"]["observedpropertymodifiervalue"].
For the ensemble graph the 50% percentile is the median of course.

### File ensemble_band.json

Has data for the banded ensemble timeseries from ECMWF with a set of percentile values for Precipitation and MaximumTemperature.
The type of the timeseries is indicated by
feature["properties"]["observedpropertymodifier"] which can have values "member", "control", "operational" and "percentile". The member number or percentile value is coded in the field feature["properties"]["observedpropertymodifiervalue"].
For the ensemble graph the 50% percentile is the median of course. The bands are made of the [25%,75%] and the [10%,90%] percentiles

### File stacked.json

Has data for the ensemble band probability bar graphs. There are time series for a range of classes of the parameter.
The type of the timeseries is indicated by
feature["properties"]["observedpropertymodifier"] which has the value "band". The class name (data range) is coded in the field feature["properties"]["observedpropertymodifiervalue"], for example "18-20"

### File waveform.json

Has data for seismometer timeseries (name: "seismometer_raw"). One file has been generated. The timestep values have many decimals for the seconds, which are very relevant for this data.
