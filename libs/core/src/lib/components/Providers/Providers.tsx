/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Provider } from 'react-redux';
import { Theme } from '@mui/material';
import {
  lightTheme,
  ThemeProviderProps,
  ThemeWrapper,
} from '@opengeoweb/theme';
import { Store } from '@reduxjs/toolkit';
import { withEggs } from '@opengeoweb/shared';
import { coreModuleConfig } from '@opengeoweb/store';
import { SnackbarWrapperConnect } from '@opengeoweb/snackbar';
import { layerSelectConfig } from '@opengeoweb/layer-select';

interface CoreThemeProviderProps {
  children?: React.ReactNode;
  theme?: Theme;
}

/**
 * A Provider component which provides the GeoWeb theme
 * @param children
 * @returns
 */
export const CoreThemeProvider: React.FC<CoreThemeProviderProps> = ({
  children,
  theme = lightTheme,
}: CoreThemeProviderProps) => (
  <ThemeWrapper theme={theme}>{children}</ThemeWrapper>
);

const ThemeWrapperWithModules: React.FC<ThemeProviderProps> = withEggs([
  ...coreModuleConfig,
  layerSelectConfig,
])(({ theme, children }: ThemeProviderProps) => (
  <CoreThemeProvider theme={theme}>{children} </CoreThemeProvider>
));

interface CoreThemeStoreProviderProps extends CoreThemeProviderProps {
  store: Store;
}

/**
 * A Provider component which provides the GeoWeb theme and store for the core.
 * Note: Should only be used with core components, as the provided store is only for core.
 * @param children
 * @returns
 */
export const CoreThemeStoreProvider: React.FC<CoreThemeStoreProviderProps> = ({
  children,
  theme = lightTheme,
  store,
}: CoreThemeStoreProviderProps) => (
  <Provider store={store}>
    <ThemeWrapperWithModules theme={theme}>
      <SnackbarWrapperConnect>
        {children as React.ReactElement}
      </SnackbarWrapperConnect>
    </ThemeWrapperWithModules>
  </Provider>
);
