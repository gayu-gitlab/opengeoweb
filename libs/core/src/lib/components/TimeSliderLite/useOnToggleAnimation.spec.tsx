/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { renderHook } from '@testing-library/react';
import { useOnToggleAnimation } from './useOnToggleAnimation';

describe('src/components/TimeSliderLite/onToggleAnimation', () => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  let result: any;
  const dispatch = jest.fn();
  const mapId = 'mockMapId';
  const timeStep = [5, 15] as [number, number];
  const defaultTimeStep = [5, 15] as [number, number];
  const startTime = 1701907200;
  const endTime = 1701990000;

  it('should start animation when onToggleAnimation is called', async () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const selectedTime = 1701935472;
    const isAnimating = false;
    const timeSliderProps = { currentTime: 1701935472 };

    renderHook(() => {
      result = useOnToggleAnimation(
        dispatch,
        mapId,
        selectedTime,
        timeStep,
        defaultTimeStep,
        startTime,
        endTime,
        isAnimating,
        timeSliderProps,
      );
    });

    result();
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch.mock.calls[0][0].payload.timeList[0].value).toBe(
      '2023-12-07T00:00:00.000Z',
    );
  });

  it('should stop animation when onToggleAnimation is called and isAnimating is true', async () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const selectedTime = 1701935472;
    const isAnimating = true;
    const timeSliderProps = { currentTime: 1701935472 };

    renderHook(() => {
      result = useOnToggleAnimation(
        dispatch,
        mapId,
        selectedTime,
        timeStep,
        defaultTimeStep,
        startTime,
        endTime,
        isAnimating,
        timeSliderProps,
      );
    });

    result();
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch.mock.calls[0][0].payload).toStrictEqual({
      mapId: 'mockMapId',
      origin: 'map',
    });
  });

  it('should start animation when onToggleAnimation is called and stopped when called again', async () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const selectedTime = 1701935472;
    let isAnimating = false;
    const timeSliderProps = { currentTime: 1701935472 };

    renderHook(() => {
      result = useOnToggleAnimation(
        dispatch,
        mapId,
        selectedTime,
        timeStep,
        defaultTimeStep,
        startTime,
        endTime,
        isAnimating,
        timeSliderProps,
      );
    });

    result();
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch.mock.calls[0][0].payload.timeList[0].value).toBe(
      '2023-12-07T00:00:00.000Z',
    );

    isAnimating = true;
    renderHook(() => {
      result = useOnToggleAnimation(
        dispatch,
        mapId,
        selectedTime,
        timeStep,
        defaultTimeStep,
        startTime,
        endTime,
        isAnimating,
        timeSliderProps,
      );
    });

    result();
    expect(dispatch).toHaveBeenCalledTimes(2);
    expect(dispatch.mock.calls[1][0].payload).toStrictEqual({
      mapId: 'mockMapId',
      origin: 'map',
    });
  });

  it('should only init startAnimation dispacth when onToggleAnimation is called and selectedTme is outside startTime and endTime range', async () => {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let result: any;
    const dispatch = jest.fn();
    const selectedTime = 1701471600;
    let isAnimating = false;
    const timeSliderProps = { currentTime: 1701471600 };

    renderHook(() => {
      result = useOnToggleAnimation(
        dispatch,
        mapId,
        selectedTime,
        timeStep,
        defaultTimeStep,
        startTime,
        endTime,
        isAnimating,
        timeSliderProps,
      );
    });

    result();
    expect(dispatch).toHaveBeenCalledTimes(1);
    expect(dispatch.mock.calls[0][0].payload.timeList[0].value).toBe(
      '2023-12-01T23:15:00.000Z',
    );

    isAnimating = true;
    renderHook(() => {
      result = useOnToggleAnimation(
        dispatch,
        mapId,
        selectedTime,
        timeStep,
        defaultTimeStep,
        startTime,
        endTime,
        isAnimating,
        timeSliderProps,
      );
    });

    // Thos won't stop the animation since the selectedTime is outside the time range
    // and startAnimation is called again
    result();
    expect(dispatch.mock.calls[0][0].payload.timeList[0].value).toBe(
      '2023-12-01T23:15:00.000Z',
    );
  });
});
