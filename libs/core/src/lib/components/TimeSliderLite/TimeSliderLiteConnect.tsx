/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';

import {
  mapSelectors,
  mapActions,
  genericActions,
  mapEnums,
  CoreAppStore,
  mapConstants,
} from '@opengeoweb/store';
import { handleMomentISOString } from '@opengeoweb/webmap';
import TimeSliderLite, { TimeSliderLiteProps } from './TimeSliderLite';
import {
  HOUR_TO_MINUTE,
  SECOND_TO_MILLISECOND,
  useDefaultTimeRange,
} from './timeSliderLiteUtils';
import TimeSliderLiteOptionsMenu, {
  TimeSliderLiteOptionsMenuProps,
} from './TimeSliderLiteOptionsMenu/TimeSliderLiteOptionsMenu';
import {
  TimeZone,
  TimeZoneSwitchProps,
} from './TimeSliderLiteOptionsMenu/TimeZoneSwitch/TimeZoneSwitch';
import { useOnToggleAnimation } from './useOnToggleAnimation';

const TWO_DAYS_IN_SECONDS = 2 * 24 * 60 * 60;
export const TIME_SLIDER_LITE_DEFAULT_TIME_RANGE = TWO_DAYS_IN_SECONDS;

export interface TimeSliderLiteConnectProps
  extends Partial<TimeSliderLiteProps> {
  overrideUseTimeRange?: [
    [number, number] | null,
    (timeRange: [number, number] | null) => void,
  ];
  overrideUseTimeStep?: [
    [number, number] | null,
    (timeStep: [number, number] | null) => void,
  ];
  overrideUseTimeZone?: [TimeZone, (timeZone: TimeZone) => void];
  sourceId: string;
  mapId: string;
  isAlwaysVisible?: boolean;
  TimeZoneSwitch?: React.FC<TimeZoneSwitchProps>;
  TimeSliderLiteOptionsMenuProps?: Partial<
    Omit<
      TimeSliderLiteOptionsMenuProps,
      | 'useTimeRange'
      | 'useTimeStep'
      | 'useAnimationSpeed'
      | 'useTimeZone'
      | 'currentTime'
    >
  >;
}

const TimeSliderLiteConnect: React.FC<TimeSliderLiteConnectProps> = ({
  overrideUseTimeRange,
  overrideUseTimeStep,
  overrideUseTimeZone,
  sourceId,
  mapId,
  isAlwaysVisible = false,
  TimeSliderLiteOptionsMenuProps,
  ...timeSliderProps
}: TimeSliderLiteConnectProps) => {
  const [menuOpen, setMenuOpen] = React.useState(false);
  const useTimeRange = React.useState<[number, number] | null>(null);
  const [timeRange, setTimeRange] = overrideUseTimeRange || useTimeRange;
  const useTimeStep = React.useState<[number, number] | null>(null);
  const [timeStep, setTimeStep] = overrideUseTimeStep || useTimeStep;
  const useTimeZone = React.useState<TimeZone>('LT');
  const [timeZone, setTimeZone] = overrideUseTimeZone || useTimeZone;

  const isTimeSliderVisible = useSelector((store: CoreAppStore) =>
    mapSelectors.isTimeSliderVisible(store, mapId),
  );
  const isVisible = isAlwaysVisible || isTimeSliderVisible;

  const dispatch = useDispatch();

  const onToggleTimeSliderVisibility = (): void => {
    dispatch(
      mapActions.toggleTimeSliderIsVisible({
        mapId,
        isTimeSliderVisible: !isVisible,
        origin: mapEnums.MapActionOrigin.map,
      }),
    );
  };

  const isAnimating = useSelector((store: CoreAppStore) =>
    mapSelectors.isAnimating(store, mapId),
  );

  const animationDelay = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapAnimationDelay(store, mapId),
  );
  const animationSpeed = animationDelay
    ? SECOND_TO_MILLISECOND / animationDelay
    : mapConstants.defaultAnimationDelayAtStart;

  const handleAnimationSpeedChange = (animationSpeed: number): void => {
    const animationDelay = SECOND_TO_MILLISECOND / animationSpeed;
    dispatch(
      mapActions.setAnimationDelay({
        mapId,
        animationDelay,
        origin: mapEnums.MapActionOrigin.map,
      }),
    );
  };

  const defaultTimeStep: [number, number] =
    TimeSliderLiteOptionsMenuProps?.defaultTimeStep ?? [5, 15];

  const layers = useSelector((store: CoreAppStore) =>
    mapSelectors.getMapLayers(store, mapId),
  );

  const [defaultStartTime, defaultEndTime] = useDefaultTimeRange(
    layers,
    HOUR_TO_MINUTE,
  );
  const [startTime, endTime] = timeRange || [defaultStartTime, defaultEndTime];

  const selectedTime = useSelector((store: CoreAppStore) =>
    mapSelectors.getSelectedTime(store, mapId),
  );

  const onToggleAnimation = useOnToggleAnimation(
    dispatch,
    mapId,
    selectedTime,
    timeStep,
    defaultTimeStep,
    startTime,
    endTime,
    isAnimating,
    timeSliderProps,
  );

  const handleTimeRangeChange = (
    newTimeRange: [number, number] | null,
  ): void => {
    if (isAnimating && newTimeRange) {
      dispatch(mapActions.mapStopAnimation({ mapId }));
    }
    setTimeRange(newTimeRange);
  };

  const handleTimeStepChange = (newTimeStep: [number, number] | null): void => {
    if (isAnimating) {
      dispatch(mapActions.mapStopAnimation({ mapId }));
    }
    setTimeStep(newTimeStep);
  };

  return (
    <>
      {isVisible && menuOpen && (
        <TimeSliderLiteOptionsMenu
          menuOpen={menuOpen}
          setMenuOpen={setMenuOpen}
          defaultTimeRange={[defaultStartTime, defaultEndTime]}
          defaultTimeStep={defaultTimeStep} // TODO: Calculate from layers
          useTimeRange={[timeRange, handleTimeRangeChange]}
          useTimeStep={[timeStep, handleTimeStepChange]}
          useAnimationSpeed={[animationSpeed, handleAnimationSpeedChange]}
          useTimeZone={[timeZone, setTimeZone]}
          closeButtonIcon={timeSliderProps.settings?.closeButton?.icon}
          dropdownButtonIcon={timeSliderProps.settings?.dropdownButton?.icon}
          TimeZoneSwitch={timeSliderProps?.settings?.TimeZoneSwitch}
          {...TimeSliderLiteOptionsMenuProps}
        />
      )}
      <TimeSliderLite
        selectedTime={selectedTime}
        timeStep={timeStep ?? defaultTimeStep}
        timeZone={timeZone}
        overrideAnimation
        isAnimating={!!isAnimating}
        isVisible={isVisible}
        mapId={mapId}
        setSelectedTime={(newSelectedTime: number): void => {
          const date = new Date(newSelectedTime * SECOND_TO_MILLISECOND);
          const newDate = date.toISOString();
          if (isAnimating) {
            dispatch(mapActions.mapStopAnimation({ mapId }));
          }
          dispatch(
            genericActions.setTime({
              sourceId,
              value: handleMomentISOString(newDate),
              origin: mapEnums.MapActionOrigin.map,
            }),
          );
        }}
        menuOpen={menuOpen}
        onToggleMenu={(): void => setMenuOpen(!menuOpen)}
        onToggleTimeSlider={onToggleTimeSliderVisibility}
        onToggleAnimation={onToggleAnimation}
        startTime={startTime}
        endTime={endTime}
        {...timeSliderProps}
      />
    </>
  );
};

export default TimeSliderLiteConnect;
