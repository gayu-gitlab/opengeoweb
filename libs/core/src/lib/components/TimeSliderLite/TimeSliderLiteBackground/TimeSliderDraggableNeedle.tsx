/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import { Tooltip, useTheme } from '@mui/material';
import React, { useRef } from 'react';
import Draggable from 'react-draggable';
import {
  reformatFinnishDateString,
  getSelectedTimeFromPx,
  getSelectedTimePx,
  MILLISECOND_TO_SECOND,
  floorLocalSeconds,
  shortUTCTime,
} from '../timeSliderLiteUtils';
import { TimeZone } from '../TimeSliderLiteOptionsMenu/TimeZoneSwitch/TimeZoneSwitch';

interface Props {
  draggableLabel?: boolean;
  width: number;
  height: number;
  locale: string;
  timeZone?: TimeZone;
  startTime: number;
  endTime: number;
  currentTime?: number;
  needleDragAreaWidth?: number;
  needleLabelZIndex?: number;
  needleLabelOffset?: [number, number];
  needleWidth?: number;
  timeStep: [number, number];
  selectedTime: number;
  style: React.CSSProperties;
  setSelectedTime: (time: number) => void;
}

interface NeedleProps {
  height: number;
  stroke: string;
  x: number;
  width: number;
}

export const DRAG_AREA_WIDTH = 10;
const NEEDLE_WIDTH = 5;
const SHADOW_SPREAD = 2;

const NeedleShadow: React.FC<NeedleProps> = ({ x, stroke, height, width }) => (
  <line
    x1={x}
    y1={height - (height - SHADOW_SPREAD / 2)}
    x2={x}
    y2={height - SHADOW_SPREAD / 2}
    strokeWidth={width + SHADOW_SPREAD}
    stroke={stroke}
    strokeLinecap="round"
    filter="blur(0.5px)"
    data-testid="TimeSliderLite-NeedleShadow"
  />
);

const NeedleLine: React.FC<NeedleProps> = ({ x, stroke, height, width }) => (
  <line
    x1={x}
    y1={width / 2}
    x2={x}
    y2={height - width / 2}
    strokeWidth={width}
    strokeLinecap="round"
    stroke={stroke}
    data-testid="TimeSliderLite-NeedleLine"
  />
);

const TimeSliderDraggableNeedle: React.FC<Props> = ({
  draggableLabel,
  width,
  height,
  locale,
  timeZone = 'LT',
  currentTime = Date.now() * MILLISECOND_TO_SECOND,
  startTime,
  endTime,
  needleDragAreaWidth = DRAG_AREA_WIDTH,
  needleLabelZIndex,
  needleLabelOffset,
  needleWidth = NEEDLE_WIDTH,
  timeStep,
  selectedTime,
  setSelectedTime,
  style,
}: Props) => {
  const draggableRef = useRef(null);
  const {
    palette: {
      geowebColors: { timeSliderLite },
    },
  } = useTheme();

  const observedTimeStepSeconds = timeStep[0];
  const forecastTimeStepSeconds = timeStep[1];
  const timeStepSeconds =
    selectedTime >= currentTime
      ? forecastTimeStepSeconds
      : observedTimeStepSeconds;
  const lastObservedTime = floorLocalSeconds(
    currentTime,
    observedTimeStepSeconds,
  );
  const firstForecastTime =
    floorLocalSeconds(currentTime, forecastTimeStepSeconds) +
    forecastTimeStepSeconds;

  const x =
    getSelectedTimePx(startTime, endTime, selectedTime, width) -
    needleDragAreaWidth / 2;
  const isBetweenObservedAndForecast =
    selectedTime >= lastObservedTime && selectedTime < firstForecastTime;
  const roundedSelectedTime = isBetweenObservedAndForecast
    ? lastObservedTime
    : floorLocalSeconds(selectedTime, timeStepSeconds);
  const dateString = new Date(roundedSelectedTime * 1000).toLocaleDateString(
    locale,
    {
      weekday: 'short',
      month: 'numeric',
      day: 'numeric',
      hour: 'numeric',
      minute: 'numeric',
      timeZone: timeZone === 'UTC' ? 'UTC' : undefined,
    },
  );

  const localLabel =
    locale === 'fi' || locale === 'sv-FI'
      ? reformatFinnishDateString(dateString)
      : dateString;
  const label = timeZone === 'UTC' ? shortUTCTime(localLabel) : localLabel;

  return (
    <Draggable
      handle=".TimeSliderLite-DraggableNeedle"
      axis="x"
      nodeRef={draggableRef}
      bounds={{
        left: -needleDragAreaWidth / 2,
        right: width - needleDragAreaWidth / 2,
      }}
      position={{
        x,
        y: 0,
      }}
      onDrag={(_, position): void => {
        const positionOffsetToCompensateFlooring = 0.5;
        const needleCenterPosition =
          position.x +
          needleDragAreaWidth / 2 +
          positionOffsetToCompensateFlooring;
        const newSelectedTime = getSelectedTimeFromPx(
          startTime,
          endTime,
          needleCenterPosition,
          width,
        );
        setSelectedTime(newSelectedTime);
      }}
      onStop={(_, position): void => {
        const positionOffsetToCompensateFlooring = 0.5;
        const needleCenterPosition =
          position.x +
          needleDragAreaWidth / 2 +
          positionOffsetToCompensateFlooring;
        const newSelectedTime = getSelectedTimeFromPx(
          startTime,
          endTime,
          needleCenterPosition,
          width,
        );
        const isLastObservedTime =
          newSelectedTime >= lastObservedTime &&
          newSelectedTime < firstForecastTime;
        const roundedSelectedTime = isLastObservedTime
          ? lastObservedTime
          : floorLocalSeconds(newSelectedTime, timeStepSeconds);
        setSelectedTime(roundedSelectedTime);
      }}
    >
      {/* Draggable ref is div for safari support */}
      <div
        className="TimeSliderLite-DraggableNeedle"
        ref={draggableRef}
        data-testid="TimeSliderLite-DraggableNeedle"
      >
        <Tooltip
          title={label}
          arrow
          placement="top"
          open={true}
          componentsProps={{
            tooltip: {
              sx: {
                padding: '0 3px 0 3px',
                fontSize: '12px',
                backgroundColor: timeSliderLite.needleLabel.backgroundColor,
                border: `1px solid ${timeSliderLite.needleLabel.borderColor}`,
              },
            },
            arrow: {
              sx: {
                color: timeSliderLite.needleLabel.backgroundColor,
              },
            },
            popper: {
              className: draggableLabel ? 'TimeSliderLite-DraggableNeedle' : '',
              sx: {
                pointerEvents: draggableLabel ? null : 'none',
                zIndex: needleLabelZIndex ?? null,
                cursor: draggableLabel ? 'ew-resize' : 'default',
              },
              modifiers: [
                {
                  name: 'offset',
                  options: {
                    offset: needleLabelOffset ?? [0, 30 - height],
                  },
                },
              ],
            },
          }}
        >
          <svg
            style={style}
            cursor="ew-resize"
            width={needleDragAreaWidth}
            height={height}
            data-testid="TimeSliderLite-NeedleSvg"
          >
            <NeedleShadow
              x={needleDragAreaWidth / 2}
              stroke={timeSliderLite.needleShadow.stroke as string}
              height={height}
              width={needleWidth}
            />
            <NeedleLine
              x={needleDragAreaWidth / 2}
              stroke={timeSliderLite.needle.stroke as string}
              height={height}
              width={needleWidth}
            />
          </svg>
        </Tooltip>
      </div>
    </Draggable>
  );
};

export default TimeSliderDraggableNeedle;
