/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  MenuItem,
  Select,
  SelectChangeEvent,
  SelectProps,
} from '@mui/material';
import React from 'react';

/**
 * TODO: Replace with actual translation keys
 */
type TimeStepOptions = Record<string, string>;

export const defaultTimeStepOptions: TimeStepOptions = {
  5: '5 minutes',
  15: '15 minutes',
  30: '30 minutes',
  60: '1 hour',
  180: '3 hours',
  360: '6 hours',
  720: '12 hours',
};
const allOptions = Object.keys(defaultTimeStepOptions);

interface CustomSelectProps extends SelectProps {
  setValue: (value: number) => void;
  dropdownButtonIcon?: React.ElementType;
  defaultTimeStep: number;
}

const TimeStepSelector: React.FC<CustomSelectProps> = ({
  dropdownButtonIcon,
  value,
  setValue,
  defaultTimeStep,
  ...props
}) => {
  const availableOptions: string[] = allOptions.filter(
    (option) => Number(option) >= defaultTimeStep,
  );

  return (
    <Select
      {...props}
      className="TimeSliderLite-timeStepSelect"
      data-testid="TimeSliderLite-timeStepSelect"
      inputProps={{
        'data-testid': 'TimeSliderLite-timeStepSelect-input',
      }}
      sx={{
        height: '40px',
        width: '100%',
      }}
      value={String(
        availableOptions.includes(String(value))
          ? value
          : availableOptions[availableOptions.length - 1],
      )}
      onChange={(event: SelectChangeEvent): void => {
        const newValue = Number(event?.target?.value);
        setValue(newValue);
      }}
      IconComponent={dropdownButtonIcon}
    >
      {availableOptions.map((option) => (
        <MenuItem key={String(option)} value={String(option)}>
          {defaultTimeStepOptions[option as keyof TimeStepOptions]}
        </MenuItem>
      ))}
    </Select>
  );
};

export default TimeStepSelector;
