/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render, screen, fireEvent } from '@testing-library/react';
import TimeZoneSwitch from './TimeZoneSwitch';

describe('TimeZoneSwitch', () => {
  it('renders with default local timezone', () => {
    render(<TimeZoneSwitch />);

    const timeZoneSwitch = screen.getByTestId('TimeSliderLite-timeZoneSwitch');
    const localButton = screen.getByText('LT');

    expect(timeZoneSwitch).toBeInTheDocument();
    expect(localButton).toBeInTheDocument();
    expect(localButton).toHaveClass('Mui-selected');
  });

  it('renders with UTC timezone when specified', () => {
    render(<TimeZoneSwitch useTimeZone={['UTC']} />);

    const timeZoneSwitch = screen.getByTestId('TimeSliderLite-timeZoneSwitch');
    const utcButton = screen.getByText('UTC');

    expect(timeZoneSwitch).toBeInTheDocument();
    expect(utcButton).toBeInTheDocument();
    expect(utcButton).toHaveClass('Mui-selected');
  });

  it('calls the setTimeZone callback when a new timezone is selected', () => {
    const setTimeZoneMock = jest.fn();
    render(<TimeZoneSwitch useTimeZone={['LT', setTimeZoneMock]} />);

    const utcButton = screen.getByText('UTC');
    fireEvent.click(utcButton);

    expect(setTimeZoneMock).toHaveBeenCalledWith('UTC');
  });

  it('disables the buttons when setTimeZone is not provided', () => {
    render(<TimeZoneSwitch />);
    const localButton = screen.getByText('LT');
    const utcButton = screen.getByText('UTC');

    expect(localButton).toHaveClass('Mui-disabled');
    expect(utcButton).toHaveClass('Mui-disabled');
  });
});
