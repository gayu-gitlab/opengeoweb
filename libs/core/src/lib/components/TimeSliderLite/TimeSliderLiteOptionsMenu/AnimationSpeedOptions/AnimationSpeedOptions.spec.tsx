/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render, screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mapTypes } from '@opengeoweb/store';
import { speedFactors } from '@opengeoweb/timeslider';
import AnimationSpeedOptions, {
  AnimationSpeedSelectorProps,
} from './AnimationSpeedOptions';

describe('TimeStepSelector', () => {
  const defaultProps: AnimationSpeedSelectorProps = {
    defaultSpeedFactor: 4,
    useAnimationSpeed: [2, jest.fn()],
  };

  const user = userEvent.setup();

  const getSpeedSelectListBox = async (): Promise<HTMLElement> => {
    const selectButton = within(
      screen.getByTestId(`TimeSliderLite-animationSpeedSelect`),
    ).getByRole('button', { hidden: true });
    await user.click(selectButton);
    return within(screen.getByRole('presentation')).getByRole('listbox');
  };

  it('should render with selected animationSpeed', () => {
    const selectedanimationSpeedDefaultLabel = 'x 2';
    render(<AnimationSpeedOptions {...defaultProps} />);

    const select = screen.getByTestId('TimeSliderLite-animationSpeedSelect');
    expect(select).toBeInTheDocument();

    expect(
      screen.getByText(selectedanimationSpeedDefaultLabel),
    ).toBeInTheDocument();
  });

  it('should render using dropdownButtonIcon', () => {
    const Icon: React.FC = () => <div>Test icon</div>;
    render(
      <AnimationSpeedOptions {...defaultProps} dropdownButtonIcon={Icon} />,
    );

    expect(screen.getByText('Test icon')).toBeInTheDocument();
  });

  test.each(speedFactors)(
    'should render x %s as default default selectable option',
    async (speedFactor) => {
      const { defaultSpeedFactor } = defaultProps;
      const speedFactorLabel =
        speedFactor !== defaultSpeedFactor
          ? `x ${speedFactor}`
          : `x ${speedFactor} (default)`;
      const initialSpeedFactor =
        speedFactor !== defaultSpeedFactor
          ? defaultSpeedFactor
          : speedFactors[0];

      render(
        <AnimationSpeedOptions
          {...defaultProps}
          useAnimationSpeed={[initialSpeedFactor as number, jest.fn()]}
        />,
      );

      const speedOptionListBox = await getSpeedSelectListBox();

      await user.click(
        // eslint-disable-next-line testing-library/no-node-access
        within(speedOptionListBox).getByText(speedFactorLabel).parentElement!,
      );
      expect(screen.getByText(speedFactorLabel)).toBeTruthy();
    },
  );

  it('should render custom options', async () => {
    const speedFactorsWithout3x: mapTypes.SpeedFactorType[] = [2, 4];

    render(
      <AnimationSpeedOptions
        {...defaultProps}
        speedOptions={speedFactorsWithout3x}
      />,
    );

    const speedOptionListBox = await getSpeedSelectListBox();

    expect(within(speedOptionListBox).queryByText('x 3')).toBeNull();

    await user.click(
      // eslint-disable-next-line testing-library/no-node-access
      within(speedOptionListBox).getByText('x 4 (default)').parentElement!,
    );
    expect(screen.getByText('x 4 (default)')).toBeTruthy();
  });

  it('should render custom prefix', async () => {
    const expectedOptionLabel = 'speed 4 (default)';
    render(
      <AnimationSpeedOptions
        {...defaultProps}
        speedOptionItemPrefix="speed "
      />,
    );

    const speedOptionListBox = await getSpeedSelectListBox();

    await user.click(
      // eslint-disable-next-line testing-library/no-node-access
      within(speedOptionListBox).getByText(expectedOptionLabel).parentElement!,
    );
    expect(screen.getByText(expectedOptionLabel)).toBeTruthy();
  });

  it('should render default custom postfix', async () => {
    const expectedOptionLabel = 'x 4 speed';
    render(
      <AnimationSpeedOptions
        {...defaultProps}
        defaultSpeedOptionPostfix=" speed"
      />,
    );

    const speedOptionListBox = await getSpeedSelectListBox();

    await user.click(
      // eslint-disable-next-line testing-library/no-node-access
      within(speedOptionListBox).getByText(expectedOptionLabel).parentElement!,
    );
    expect(screen.getByText(expectedOptionLabel)).toBeTruthy();
  });

  it('should render custom postfix', async () => {
    const expectedOptionLabel = 'x 2 speed';
    render(
      <AnimationSpeedOptions
        {...defaultProps}
        useAnimationSpeed={[4, jest.fn()]}
        speedOptionItemPostfix=" speed"
      />,
    );

    const speedOptionListBox = await getSpeedSelectListBox();

    await user.click(
      // eslint-disable-next-line testing-library/no-node-access
      within(speedOptionListBox).getByText(expectedOptionLabel).parentElement!,
    );
    expect(screen.getByText(expectedOptionLabel)).toBeTruthy();
  });

  it('should render disabled if readonly', () => {
    render(<AnimationSpeedOptions {...defaultProps} useAnimationSpeed={[2]} />);

    const selectInputWithoutSetter = screen.getByTestId(
      'TimeSliderLite-animationSpeedSelect-input',
    );
    expect(selectInputWithoutSetter).toBeDisabled();
  });

  it('should render disabled if only one option', () => {
    render(
      <AnimationSpeedOptions
        {...defaultProps}
        speedOptions={[4]}
        useAnimationSpeed={[4, jest.fn()]}
      />,
    );
    const selectInputWithoutOptions = screen.getByTestId(
      'TimeSliderLite-animationSpeedSelect-input',
    );
    expect(selectInputWithoutOptions).toBeDisabled();
  });
});
