/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';

import { CoreThemeProvider } from '../Providers/Providers';
import TimeSliderLite, { TimeSliderLiteProps } from './TimeSliderLite';
import { MILLISECOND_TO_SECOND } from './timeSliderLiteUtils';

describe('src/components/TimeSliderLite/TimeSliderLite', () => {
  const defaultProps: TimeSliderLiteProps = {
    mapId: 'map_1',
    selectedTime: Date.now() * MILLISECOND_TO_SECOND,
    startTime: Math.floor(
      (Date.now() - 2 * 24 * 60 * 60 * 1000) * MILLISECOND_TO_SECOND,
    ),
    endTime: (Date.now() + 2 * 24 * 60 * 60 * 1000) * MILLISECOND_TO_SECOND,
    timeStep: [60, 60],
    height: 44,
    isVisible: true,
    onToggleAnimation: (): void => {},
    onToggleMenu: (): void => {},
    onToggleTimeSlider: (): void => {},
    setSelectedTime: (): void => {},
  };

  it('should render control buttons', () => {
    render(
      <CoreThemeProvider>
        <TimeSliderLite {...defaultProps} />
      </CoreThemeProvider>,
    );
    expect(screen.getByTestId('TimeSliderLite-HideButton')).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-MenuButton')).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-PlayButton')).toBeTruthy();
    expect(screen.getByTestId('TimeSliderLite-StepButtonForward')).toBeTruthy();
    expect(
      screen.getByTestId('TimeSliderLite-StepButtonBackward'),
    ).toBeTruthy();
  });

  it('should render slider background', () => {
    render(
      <CoreThemeProvider>
        <TimeSliderLite {...defaultProps} />
      </CoreThemeProvider>,
    );
    expect(screen.getByTestId('TimeSliderLite-Background')).toBeTruthy();
  });

  it('should call onToggleMenu on menu button click', () => {
    const onToggleMenu = jest.fn();
    render(
      <CoreThemeProvider>
        <TimeSliderLite {...defaultProps} onToggleMenu={onToggleMenu} />
      </CoreThemeProvider>,
    );
    fireEvent.click(screen.getByTestId('TimeSliderLite-MenuButton'));
    expect(onToggleMenu).toHaveBeenCalled();
  });

  it('should call onToggleAnimation on play button click', () => {
    const onToggleAnimation = jest.fn();
    render(
      <CoreThemeProvider>
        <TimeSliderLite
          {...defaultProps}
          onToggleAnimation={onToggleAnimation}
        />
      </CoreThemeProvider>,
    );
    fireEvent.click(screen.getByTestId('TimeSliderLite-PlayButton'));
    expect(onToggleAnimation).toHaveBeenCalled();
  });

  it('should call onToggleTimeSlider on hide button click', () => {
    const onToggleTimeSlider = jest.fn();
    render(
      <CoreThemeProvider>
        <TimeSliderLite
          {...defaultProps}
          onToggleTimeSlider={onToggleTimeSlider}
        />
      </CoreThemeProvider>,
    );
    fireEvent.click(screen.getByTestId('TimeSliderLite-HideButton'));
    expect(onToggleTimeSlider).toHaveBeenCalled();
  });

  it('should call setSelectedTime after clicking step button', async () => {
    const setSelectedTime = jest.fn();
    render(
      <CoreThemeProvider>
        <TimeSliderLite {...defaultProps} setSelectedTime={setSelectedTime} />
      </CoreThemeProvider>,
    );
    // After initial render, setSelectedTime is called
    const setSelectedTimeInitialCalls = setSelectedTime.mock.calls.length;
    expect(setSelectedTimeInitialCalls).toBeGreaterThan(0);

    fireEvent.click(screen.getByTestId('TimeSliderLite-StepButtonForward'));
    expect(setSelectedTime.mock.calls.length).toBe(
      setSelectedTimeInitialCalls + 1,
    );

    fireEvent.click(screen.getByTestId('TimeSliderLite-StepButtonBackward'));
    expect(setSelectedTime.mock.calls.length).toBe(
      setSelectedTimeInitialCalls + 2,
    );
  });
});
