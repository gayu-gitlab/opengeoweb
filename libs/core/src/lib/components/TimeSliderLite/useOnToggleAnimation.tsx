/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { mapActions, mapEnums } from '@opengeoweb/store';
import React from 'react';
import { Dispatch } from '@reduxjs/toolkit';
import {
  floorLocalSeconds,
  HOUR_TO_SECOND,
  generateCustomTimeList,
  MILLISECOND_TO_SECOND,
  SECOND_TO_MILLISECOND,
} from './timeSliderLiteUtils';
import { TimeSliderLiteProps } from './TimeSliderLite';

export const useOnToggleAnimation = (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  dispatch: Dispatch<any>,
  mapId: string,
  selectedTime: number,
  timeStep: [number, number] | null,
  defaultTimeStep: [number, number],
  startTime: number,
  endTime: number,
  isAnimating: boolean,
  timeSliderProps: Partial<TimeSliderLiteProps>,
): (() => void) => {
  const startMapAnimation = (): void => {
    const observedTimeStep = timeStep?.[0] ?? defaultTimeStep[0];
    const forecastTimeStep = timeStep?.[1] ?? defaultTimeStep[1];
    const floorCorrectedTimeStart = floorLocalSeconds(
      startTime,
      HOUR_TO_SECOND,
    );
    const floorCorrectedEndTime = floorLocalSeconds(endTime, HOUR_TO_SECOND);
    const timeList = generateCustomTimeList(
      timeSliderProps.currentTime ?? Date.now() * MILLISECOND_TO_SECOND,
      floorCorrectedTimeStart,
      floorCorrectedEndTime,
      [observedTimeStep, forecastTimeStep],
    );
    dispatch(
      mapActions.mapStartAnimation({
        mapId,
        initialTime: new Date(
          selectedTime * SECOND_TO_MILLISECOND,
        ).toISOString(),
        timeList,
        origin: mapEnums.MapActionOrigin.map,
      }),
    );
  };

  const onToggleAnimation = (): void => {
    if (isAnimating) {
      dispatch(
        mapActions.mapStopAnimation({
          mapId,
          origin: mapEnums.MapActionOrigin.map,
        }),
      );
    } else {
      startMapAnimation();
    }
  };

  React.useEffect(() => {
    const hasTimeRange = startTime && endTime && selectedTime;
    const isInvalidAnimation =
      selectedTime < startTime || selectedTime > endTime;
    if (hasTimeRange && isInvalidAnimation && isAnimating) {
      startMapAnimation();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedTime, startTime, endTime]);

  return onToggleAnimation;
};
