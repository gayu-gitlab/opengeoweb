/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useTranslation } from 'react-i18next';
import { CoreThemeProvider } from '../Providers/Providers';
import LanguageSelect from './LanguageSelect';
import { outsideTranslations } from '../../utils/translationUpdater';

export default { title: 'components/LanguageSelect' };

export const Languages = (): JSX.Element => {
  const { t, i18n } = useTranslation();
  const [lang, setLang] = React.useState(i18n.language);

  const changeLanguage = (language: string): void => {
    i18n.changeLanguage(language);
    setLang(language);
  };

  const name = 'GeoWeb';

  return (
    <CoreThemeProvider>
      <>
        <h1>Examples of different ways to use react-i18next</h1>
        <LanguageSelect currentLang={lang} onChangeLanguage={changeLanguage} />
        <h4>useTranslation hook</h4>
        <p>{t('greeting')}</p>
        <h4>Interpolation with useTranslation</h4>
        <p>{t('greetingWithName', { name })}</p>
        <h4>Translate outside React component</h4>
        <p>{outsideTranslations.greeting}</p>
      </>
    </CoreThemeProvider>
  );
};

Languages.parameters = {
  layout: 'padded',
};
