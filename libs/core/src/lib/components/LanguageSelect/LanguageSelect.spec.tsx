/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import LanguageSelect from './LanguageSelect';
import { languages } from '../../utils/languagesService';
import { CoreThemeProvider } from '../Providers/Providers';

describe('src/components/LanguageSelect/LanguageSelect', () => {
  const defaultProps = {
    onChangeLanguage: jest.fn(),
    currentLang: 'en',
    defaultLang: 'en',
  };

  it('should show English by default', () => {
    const props = { ...defaultProps };
    render(
      <CoreThemeProvider>
        <LanguageSelect {...props} />
      </CoreThemeProvider>,
    );
    expect(
      screen.getByRole('textbox', { hidden: true }).getAttribute('value'),
    ).toBe(props.defaultLang);
  });

  it('should show English if the translation in another language is missed', () => {
    const props = { ...defaultProps, currentLang: 'rus' };
    render(
      <CoreThemeProvider>
        <LanguageSelect {...props} />
      </CoreThemeProvider>,
    );
    expect(
      screen.getByRole('textbox', { hidden: true }).getAttribute('value'),
    ).toBe(props.defaultLang);
  });

  it('should show the language that it was initialized with when it exists', () => {
    const props = {
      ...defaultProps,
      currentLang: languages[0].code,
    };
    render(
      <CoreThemeProvider>
        <LanguageSelect {...props} />
      </CoreThemeProvider>,
    );
    expect(
      screen.getByRole('textbox', { hidden: true }).getAttribute('value'),
    ).toBe(props.currentLang);
  });

  it('should call onChangeLanguage when a Menu item is clicked', async () => {
    const props = {
      ...defaultProps,
      currentLang: languages[0].code,
    };
    render(
      <CoreThemeProvider>
        <LanguageSelect {...props} />
      </CoreThemeProvider>,
    );
    const otherLanguage = languages[1].code.toUpperCase();

    fireEvent.mouseDown(screen.getByTestId('select'));

    const menuItem = await screen.findByText(otherLanguage);
    fireEvent.click(menuItem);

    expect(props.onChangeLanguage).toHaveBeenCalledWith(languages[1].code);
    expect(screen.getByTestId('select').textContent).toContain(otherLanguage);
  });

  it('should render a menu-item for each language in the list', () => {
    const props = {
      ...defaultProps,
      currentLang: languages[0].code,
    };
    render(
      <CoreThemeProvider>
        <LanguageSelect {...props} />
      </CoreThemeProvider>,
    );

    fireEvent.mouseDown(screen.getByTestId('select'));

    const menuItems = screen.getAllByRole('option');
    expect(menuItems.length).toEqual(languages.length);
  });
});
