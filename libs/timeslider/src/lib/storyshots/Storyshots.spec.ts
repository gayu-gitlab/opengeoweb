/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import initStoryshots from '@storybook/addon-storyshots';
// TODO: remove require https://gitlab.com/opengeoweb/geoweb-assets/-/issues/2678
// eslint-disable-next-line @typescript-eslint/no-var-requires
const { pipelineTest, localTest } = require('../../../../../setup-storyshots');

const chromePath = process.env.CHROME_BIN;

initStoryshots({
  configPath: './libs/timeslider/.storybook',
  test: chromePath ? pipelineTest('timeslider') : localTest('timeslider'),
  storyNameRegex: /.?takeSnapshot.?/,
});
