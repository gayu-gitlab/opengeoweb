/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import moment from 'moment';
import { mapUtils, storeTestUtils } from '@opengeoweb/store';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { webmapTestSettings } from '@opengeoweb/webmap';
import { ThemeProvider, ThemeStoreProvider } from '../../testUtils/Providers';
import { TimeSlider, TimeSliderProps } from './TimeSlider';

describe('src/components/TimeSlider/TimeSlider', () => {
  const defaultProps: TimeSliderProps = {
    mapId: 'map_1',
    currentTime: 0,
    dataEndTime: 0,
    dataStartTime: 0,
    selectedTime: 0,
    timeStep: 0,
    onSetCenterTime: (): void => {},
    onSetNewDate: (): void => {},
  };

  const mockStore = configureStore();
  const mockState = storeTestUtils.mockStateMapWithMultipleLayers(
    [webmapTestSettings.multiDimensionLayer3],
    defaultProps.mapId,
  );
  const store = mockStore(mockState);

  it('should render the wrapped components', () => {
    render(
      <ThemeProvider>
        <Provider store={store}>
          <TimeSlider {...defaultProps} />
        </Provider>
      </ThemeProvider>,
    );
    expect(screen.getByTestId('timeSliderButtons')).toBeTruthy();
    expect(screen.getByTestId('timeSliderTimeBox')).toBeTruthy();
    expect(screen.getByTestId('timeSliderLegend')).toBeTruthy();
  });

  it('should hide the timeslider', () => {
    const props = {
      ...defaultProps,
      mapId: 'map_1',
      isVisible: false,
      onToggleTimeSlider: jest.fn(),
    };

    render(
      <ThemeProvider>
        <Provider store={store}>
          <TimeSlider {...props} />
        </Provider>
      </ThemeProvider>,
    );
    expect(screen.queryByTestId('timeSliderButtons')).toBeFalsy();
    expect(screen.queryByTestId('timeSliderTimeBox')).toBeFalsy();
    expect(screen.queryByTestId('timeSliderLegend')).toBeFalsy();

    expect(props.onToggleTimeSlider).not.toHaveBeenCalled();
    fireEvent.keyDown(document, {
      key: 't',
      code: 'KeyT',
    });
    expect(props.onToggleTimeSlider).toHaveBeenCalledTimes(1);
  });

  it('should be able to hide the timeslider with t key when map is active', async () => {
    const props = {
      ...defaultProps,
      mapId: 'map_1',
      onToggleTimeSlider: jest.fn(),
    };
    const { baseElement } = render(
      <ThemeProvider>
        <Provider store={store}>
          <TimeSlider {...props} />
        </Provider>
      </ThemeProvider>,
    );
    expect(screen.getByTestId('timeSlider')).toBeTruthy();
    expect(props.onToggleTimeSlider).not.toHaveBeenCalled();
    fireEvent.keyDown(baseElement, {
      key: 't',
      code: 'KeyT',
    });
    expect(props.onToggleTimeSlider).toHaveBeenCalledTimes(1);
  });

  it('should be able to show the timeslider with t key when map is active', async () => {
    const props = {
      ...defaultProps,
      mapId: 'map_1',
      onToggleTimeSlider: jest.fn(),
      isVisible: false,
    };
    const { baseElement } = render(
      <ThemeProvider>
        <Provider store={store}>
          <TimeSlider {...props} />
        </Provider>
      </ThemeProvider>,
    );
    expect(screen.queryByTestId('timeSlider')).toBeFalsy();
    expect(props.onToggleTimeSlider).not.toHaveBeenCalled();
    fireEvent.keyDown(baseElement, {
      key: 't',
      code: 'KeyT',
    });
    expect(props.onToggleTimeSlider).toHaveBeenCalledTimes(1);
  });
  it('timeslider cannot be removed and fetched back with t key when map is inactive', async () => {
    const props = { ...defaultProps, mapId: 'map_1', mapIsActive: false };
    const { baseElement } = render(
      <ThemeProvider>
        <Provider store={store}>
          <TimeSlider {...props} />
        </Provider>
      </ThemeProvider>,
    );
    expect(screen.getByTestId('timeSlider')).toBeTruthy();
    fireEvent.keyDown(baseElement, {
      key: 't',
      code: 'KeyT',
    });
    expect(screen.getByTestId('timeSlider')).toBeTruthy();
  });
  it('timeslider cannot be removed and fetched back with t key in an input or textarea field, but can be done elsewhere', async () => {
    const props = {
      ...defaultProps,
      mapId: 'mapId_1',
      onToggleTimeSlider: jest.fn(),
    };
    const props2 = {
      ...defaultProps,
      mapId: 'map_1',
      onToggleTimeSlider: jest.fn(),
    };
    const mockState =
      storeTestUtils.mockStateMapWithAnimationDelayWithoutLayers(props.mapId);
    const store = createMockStoreWithEggs(mockState);
    render(
      <ThemeStoreProvider store={store}>
        <input data-testid="fakeTestField" />
        <textarea data-testid="fakeTestArea" />
        <div data-testid="testDiv" />
        <TimeSlider {...props2} />
      </ThemeStoreProvider>,
    );
    expect(screen.getByTestId('timeSlider')).toBeTruthy();
    const inputField = screen.getByTestId('fakeTestField');
    expect(inputField).toBeTruthy();
    const testDiv = screen.getByTestId('testDiv');
    expect(testDiv).toBeTruthy();
    const textArea = screen.getByTestId('fakeTestArea');
    expect(textArea).toBeTruthy();

    expect(screen.getByTestId('timeSlider')).toBeTruthy();
    expect(props2.onToggleTimeSlider).not.toHaveBeenCalled();

    fireEvent.keyDown(inputField, {
      key: 't',
      code: 'KeyT',
    });
    expect(props2.onToggleTimeSlider).not.toHaveBeenCalled();

    fireEvent.keyDown(textArea, {
      key: 't',
      code: 'KeyT',
    });
    expect(props2.onToggleTimeSlider).not.toHaveBeenCalled();

    fireEvent.keyDown(testDiv, {
      key: 't',
      code: 'KeyT',
    });
    await waitFor(() =>
      expect(props2.onToggleTimeSlider).toHaveBeenCalledWith(false),
    );
  });

  describe('change time with home button', () => {
    const date = moment.utc('20200101', 'YYYYMMDD');
    const props: TimeSliderProps = {
      mapId: 'map_1',
      timeStep: 5,
      currentTime: moment.utc(date).startOf('day').hours(12).unix(),
      selectedTime: 0,
      dataStartTime: 0,
      dataEndTime: 0,
      onSetNewDate: jest.fn(),
      onSetCenterTime: jest.fn(),
    };

    const renderTimeSliderLegendAndClickHome = (
      props: TimeSliderProps,
    ): void => {
      const { baseElement } = render(
        <ThemeProvider>
          <Provider store={store}>
            <TimeSlider {...props} />
          </Provider>
        </ThemeProvider>,
      );
      expect(baseElement).toBeTruthy();
      const canvas = screen.queryAllByRole('presentation', {
        name: 'canvas',
      })[0].firstChild as HTMLElement;
      canvas.focus();

      fireEvent.keyDown(canvas, {
        key: 'Home',
        code: 'Home',
        keyCode: 36,
      });
    };

    it('should call onSetNewDate and onSetCenterTime with correct parameters when no data is available in the future and home button is pressed', () => {
      const props2: TimeSliderProps = {
        ...props,
        dataStartTime: moment.utc(date).startOf('day').unix(),
        dataEndTime: moment.utc(date).startOf('day').hours(10).unix(),
      };

      renderTimeSliderLegendAndClickHome(props2);

      expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
      expect(props.onSetNewDate).toHaveBeenCalledWith(
        moment.utc(props2.dataEndTime! * 1000).toISOString(),
      );
      expect(props.onSetCenterTime).toHaveBeenCalledTimes(1);
      expect(props.onSetCenterTime).toHaveBeenCalledWith(props2.dataEndTime);
    });

    it('should call onSetNewDate and onSetCenterTime with correct parameters when observation and forecast data are available and home button is pressed', () => {
      const props2: TimeSliderProps = {
        ...props,
        dataStartTime: moment.utc(date).startOf('day').unix(),
        dataEndTime: moment.utc(date).endOf('day').unix(),
      };

      renderTimeSliderLegendAndClickHome(props2);

      expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
      expect(props2.onSetNewDate).toHaveBeenCalledWith(
        moment
          .utc(
            mapUtils.roundWithTimeStep(props.currentTime, props.timeStep) *
              1000,
          )
          .toISOString(),
      );
      expect(props.onSetCenterTime).toHaveBeenCalledTimes(1);
      expect(props.onSetCenterTime).toHaveBeenCalledWith(
        mapUtils.roundWithTimeStep(props.currentTime, props.timeStep),
      );
    });

    it('should call onSetNewDate and onSetCenterTime with correct parameters when no data is available in the past and home button is pressed', () => {
      const props2: TimeSliderProps = {
        ...props,
        dataStartTime: moment.utc(date).startOf('day').hours(14).unix(),
        dataEndTime: moment.utc(date).endOf('day').unix(),
      };

      renderTimeSliderLegendAndClickHome(props2);

      expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
      expect(props2.onSetNewDate).toHaveBeenCalledWith(
        moment.utc(props2.dataStartTime * 1000).toISOString(),
      );
      expect(props.onSetCenterTime).toHaveBeenCalledTimes(1);
      expect(props.onSetCenterTime).toHaveBeenCalledWith(props2.dataStartTime);
    });

    it('should not call onSetNewDate and onSetCenterTime when map is inactive and home button is pressed', () => {
      const props2: TimeSliderProps = {
        ...props,
        dataStartTime: moment.utc(date).startOf('day').unix(),
        dataEndTime: moment.utc(date).startOf('day').hours(10).unix(),
        mapIsActive: false,
      };

      renderTimeSliderLegendAndClickHome(props2);

      expect(props.onSetNewDate).not.toBeCalled();
      expect(props.onSetCenterTime).not.toBeCalled();
    });
  });
  describe('change time with arrow left or right', () => {
    beforeAll(() => {
      jest.useFakeTimers();
    });
    afterAll(() => {
      jest.clearAllTimers();
      jest.useRealTimers();
    });

    const date = moment.utc('20200101', 'YYYYMMDD');
    const props: TimeSliderProps = {
      ...defaultProps,
      mapId: 'map_1',
      timeStep: 5,
      currentTime: moment.utc(date).startOf('day').hours(12).unix(),
      selectedTime: moment.utc('20200101', 'YYYYMMDD').unix(),
      dataStartTime: moment.utc('20100101', 'YYYYMMDD').unix(),
      dataEndTime: moment.utc('20300101', 'YYYYMMDD').unix(),
      onSetNewDate: jest.fn(),
      onSetCenterTime: jest.fn(),
    };

    function changeLegendAnimatiomValueWithKeyboard(
      arrowKeyDirection: 'left' | 'right',
      mapIsActive: boolean,
    ): void {
      render(
        <ThemeProvider>
          <Provider store={store}>
            <TimeSlider {...props} mapIsActive={mapIsActive} />
          </Provider>
        </ThemeProvider>,
      );
      const canvas = screen.queryAllByRole('presentation', {
        name: 'canvas',
      })[0].firstChild as HTMLElement;
      canvas.focus();
      fireEvent.keyDown(
        canvas,
        arrowKeyDirection === 'left'
          ? {
              ctrlKey: true,
              key: 'ArrowLeft',
              code: 'ArrowLeft',
              keyCode: 37,
            }
          : {
              ctrlKey: true,
              key: 'ArrowRight',
              code: 'ArrowRight',
              keyCode: 39,
            },
      );
      jest.runOnlyPendingTimers();
    }

    it('Legend slider value can be changed with an right arrow key and a ctrl key if map is active', async () => {
      const newTime = moment
        .unix(props.selectedTime)
        .add(props.timeStep, 'm')
        .toISOString();

      changeLegendAnimatiomValueWithKeyboard('right', true);

      expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
      expect(props.onSetNewDate).toHaveBeenCalledWith(newTime);
    });
    it('rail slider value can not be changed with an right arrow key and a ctrl key if map is inactive', async () => {
      changeLegendAnimatiomValueWithKeyboard('right', false);

      expect(props.onSetNewDate).not.toBeCalled();
    });
    it('rail slider value can be changed with an left arrow key and a ctrl key if map is active', async () => {
      const newTime = moment
        .unix(props.selectedTime)
        .subtract(props.timeStep, 'm')
        .toISOString();

      changeLegendAnimatiomValueWithKeyboard('left', true);

      expect(props.onSetNewDate).toHaveBeenCalledTimes(1);
      expect(props.onSetNewDate).toHaveBeenCalledWith(newTime);
    });

    it('rail slider value can not be changed with an left arrow key and a ctrl key if map is inactive', async () => {
      changeLegendAnimatiomValueWithKeyboard('left', false);

      expect(props.onSetNewDate).not.toBeCalled();
    });
  });
});
