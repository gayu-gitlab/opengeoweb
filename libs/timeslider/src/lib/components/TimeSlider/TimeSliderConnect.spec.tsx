/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, renderHook, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { produce } from 'immer';
import {
  mapActions,
  mapConstants,
  mapEnums,
  mapTypes,
  storeTestSettings,
  storeTestUtils,
} from '@opengeoweb/store';
import { webmapTestSettings } from '@opengeoweb/webmap';
import {
  TimeSliderConnect,
  useUpdateTimeSpan,
  useUpdateTimestep,
} from './TimeSliderConnect';
import { ThemeStoreProvider } from '../../testUtils/Providers';

describe('src/components/TimeSlider/TimeSliderConnect', () => {
  describe('useUpdateTimestep', () => {
    it('should update timestep if auto is on and interval is a day', async () => {
      const mapId = 'mapid_1';
      const layer = produce(
        webmapTestSettings.defaultReduxLayerRadarKNMI,
        (draft) => {
          (draft.dimensions![0] as mapTypes.Dimension).timeInterval = {
            day: 1,
            hour: 0,
            isRegularInterval: true,
            minute: 0,
            month: 0,
            second: 0,
            year: 0,
          };
        },
      );
      const mockState = produce(
        storeTestUtils.mockStateMapWithLayer(layer, mapId),
        (draft) => {
          draft.webmap!.byId[mapId].isTimestepAuto = true;
        },
      );
      const store = createMockStoreWithEggs(mockState);

      renderHook(() => useUpdateTimestep(mapId), {
        wrapper: ({ children }): React.ReactElement => (
          <ThemeStoreProvider store={store}>{children}</ThemeStoreProvider>
        ),
      });

      expect(store.getActions()).toEqual([
        mapActions.setTimeStep({ mapId, timeStep: 24 * 60 }),
      ]);
    });

    it('should update timestep if auto is on and interval is an hour', async () => {
      const mapId = 'mapid_2';
      const layer = produce(
        webmapTestSettings.defaultReduxLayerRadarKNMI,
        (draft) => {
          (draft.dimensions![0] as mapTypes.Dimension).timeInterval = {
            day: 0,
            hour: 1,
            isRegularInterval: true,
            minute: 0,
            month: 0,
            second: 0,
            year: 0,
          };
        },
      );

      const mockState2 = produce(
        storeTestUtils.mockStateMapWithLayer(layer, mapId),
        (draft) => {
          draft.webmap!.byId[mapId].isTimestepAuto = true;
        },
      );

      const store2 = createMockStoreWithEggs(mockState2);
      renderHook(() => useUpdateTimestep(mapId), {
        wrapper: ({ children }): React.ReactElement => (
          <ThemeStoreProvider store={store2}>{children}</ThemeStoreProvider>
        ),
      });

      expect(store2.getActions()).toEqual([
        mapActions.setTimeStep({ mapId, timeStep: 60 }),
      ]);
    });

    it('should not update timestep if auto is off', async () => {
      const mapId = 'mapid_2';
      const layer = produce(
        webmapTestSettings.defaultReduxLayerRadarKNMI,
        (draft) => {
          (draft.dimensions![0] as mapTypes.Dimension).timeInterval = {
            day: 0,
            hour: 1,
            isRegularInterval: true,
            minute: 0,
            month: 0,
            second: 0,
            year: 0,
          };
        },
      );

      const mockState2 = produce(
        storeTestUtils.mockStateMapWithLayer(layer, mapId),
        (draft) => {
          draft.webmap!.byId[mapId].isTimestepAuto = false;
        },
      );
      const store2 = createMockStoreWithEggs(mockState2);

      renderHook(() => useUpdateTimestep(mapId), {
        wrapper: ({ children }): React.ReactElement => (
          <ThemeStoreProvider store={store2}>{children}</ThemeStoreProvider>
        ),
      });

      expect(store2.getActions()).not.toContainEqual(
        mapActions.setTimeStep({ mapId, timeStep: 60 }),
      );
      expect(store2.getActions()).toEqual([]);
    });
  });
  describe('useUpdateTimeSpan', () => {
    it('should set span from data when timespan auto is on', async () => {
      const now = '2022-01-09T14:00:00Z';
      jest.useFakeTimers().setSystemTime(new Date(now));

      const mapId = 'mapid_2';
      const layer = produce(
        webmapTestSettings.defaultReduxLayerRadarKNMI,
        (draft) => {
          (draft.dimensions![0] as mapTypes.Dimension).maxValue =
            '2020-03-14T12:00:00Z';
          (draft.dimensions![0] as mapTypes.Dimension).minValue =
            '2020-02-13T10:00:00Z';
        },
      );
      const mockState2 = produce(
        storeTestUtils.mockStateMapWithLayer(layer, mapId),
        (draft) => {
          draft.webmap!.byId[mapId].autoTimeStepLayerId = 'layerid_2';
          draft.webmap!.byId[mapId].isTimeSpanAuto = true;
        },
      );
      const store2 = createMockStoreWithEggs(mockState2);
      const onSetTimeSliderSpan = jest.fn();

      renderHook(() => useUpdateTimeSpan(mapId, onSetTimeSliderSpan), {
        wrapper: ({ children }): React.ReactElement => (
          <ThemeStoreProvider store={store2}>{children}</ThemeStoreProvider>
        ),
      });

      expect(onSetTimeSliderSpan).toHaveBeenCalledWith(
        2599200,
        1582887600,
        5907.272727272727,
      );
    });
    it('should set default span when update timespan auto is off', async () => {
      const now = '2022-01-09T14:00:00Z';
      jest.useFakeTimers().setSystemTime(new Date(now));

      const mapId = 'mapid_2';
      const layer = produce(
        webmapTestSettings.defaultReduxLayerRadarKNMI,
        (draft) => {
          (draft.dimensions![0] as mapTypes.Dimension).maxValue =
            '2020-03-14T12:00:00Z';
          (draft.dimensions![0] as mapTypes.Dimension).minValue =
            '2020-02-13T10:00:00Z';
        },
      );
      const mockState2 = produce(
        storeTestUtils.mockStateMapWithLayer(layer, mapId),
        (draft) => {
          draft.webmap!.byId[mapId].autoTimeStepLayerId = 'layerid_2';
          draft.webmap!.byId[mapId].isTimeSpanAuto = false;
        },
      );
      const store2 = createMockStoreWithEggs(mockState2);
      const onSetTimeSliderSpan = jest.fn();

      renderHook(() => useUpdateTimeSpan(mapId, onSetTimeSliderSpan), {
        wrapper: ({ children }): React.ReactElement => (
          <ThemeStoreProvider store={store2}>{children}</ThemeStoreProvider>
        ),
      });

      expect(onSetTimeSliderSpan).toHaveBeenCalledWith(
        mapConstants.defaultTimeSpan,
        1725563127.2727273,
        196.36363636363637,
      );
    });
  });
  it('should render the component when the map has a layer with time dimension', () => {
    const mapId = 'mapid_1';
    const layer = webmapTestSettings.defaultReduxLayerRadarKNMI;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStoreWithEggs(mockState);
    render(
      <ThemeStoreProvider store={store}>
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </ThemeStoreProvider>,
    );

    expect(screen.getByTestId('timeSliderButtons')).toBeTruthy();
    expect(screen.getByTestId('timeSliderTimeBox')).toBeTruthy();
    expect(screen.getByTestId('timeSliderLegend')).toBeTruthy();
    expect(screen.getByTestId('timeSlider')).toBeTruthy();
  });

  it('should render the component when the layer on the map does not have a time dimension', () => {
    const mapId = 'mapid_2';
    const layer = storeTestSettings.layerWithoutTimeDimension;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStoreWithEggs(mockState);
    render(
      <ThemeStoreProvider store={store}>
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </ThemeStoreProvider>,
    );

    expect(screen.getByTestId('timeSliderButtons')).toBeTruthy();
    expect(screen.getByTestId('timeSliderTimeBox')).toBeTruthy();
    expect(screen.getByTestId('timeSliderLegend')).toBeTruthy();
    expect(screen.getByTestId('timeSlider')).toBeTruthy();
  });

  it('should toggle time slider hover when ctrl + alt + h is pressed', async () => {
    const mapId = 'mapid_3';
    const layer = webmapTestSettings.defaultReduxLayerRadarKNMI;
    const mockState = storeTestUtils.mockStateMapWithLayer(layer, mapId);
    const store = createMockStoreWithEggs(mockState);
    render(
      <ThemeStoreProvider store={store}>
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </ThemeStoreProvider>,
    );
    const timeSlider = screen.getByTestId('timeSlider');
    expect(timeSlider).toBeTruthy();
    timeSlider.focus();

    fireEvent.keyDown(timeSlider, {
      ctrlKey: true,
      altKey: true,
      key: 'h',
      code: 'KeyH',
    });

    const expectedActions = mapActions.toggleTimeSliderHover({
      mapId,
      isTimeSliderHoverOn: true,
    });
    expect(store.getActions()).toContainEqual(expectedActions);
  });
  it('should be visible even when isTimeSliderVisible is true', () => {
    const mapId = 'mapid_3';
    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            baseLayers: [],
            mapLayers: [],
            overLayers: [],
            isTimeSliderVisible: true,
          },
        },
        allIds: [mapId],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
        viewState: {
          timeslider: {
            groups: [],
            sourcesById: [],
          },
          zoompane: {
            groups: [],
            sourcesById: [],
          },
          level: {
            groups: [],
            sourcesById: [],
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <ThemeStoreProvider store={store}>
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </ThemeStoreProvider>,
    );
    const timeSlider = screen.getByTestId('timeSlider');
    expect(timeSlider).toBeTruthy();
  });

  it('should be hidden even when isTimeSliderVisible is false', () => {
    const mapId = 'mapid_3';
    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            baseLayers: [],
            mapLayers: [],
            overLayers: [],
            isTimeSliderVisible: false,
          },
        },
        allIds: [mapId],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
        viewState: {
          timeslider: {
            groups: [],
            sourcesById: [],
          },
          zoompane: {
            groups: [],
            sourcesById: [],
          },
          level: {
            groups: [],
            sourcesById: [],
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <ThemeStoreProvider store={store}>
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </ThemeStoreProvider>,
    );
    const timeSlider = screen.queryByTestId('timeSlider');
    expect(timeSlider).toBeFalsy();
  });

  it('should be visible even when isTimeSliderVisible is false but has isAlwaysVisible as prop', () => {
    const mapId = 'mapid_3';
    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            baseLayers: [],
            mapLayers: [],
            overLayers: [],
            isTimeSliderVisible: false,
          },
        },
        allIds: [mapId],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
        viewState: {
          timeslider: {
            groups: [],
            sourcesById: [],
          },
          zoompane: {
            groups: [],
            sourcesById: [],
          },
          level: {
            groups: [],
            sourcesById: [],
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <ThemeStoreProvider store={store}>
        <TimeSliderConnect
          sourceId="timeslider-1"
          mapId={mapId}
          isAlwaysVisible
        />
      </ThemeStoreProvider>,
    );
    const timeSlider = screen.getByTestId('timeSlider');
    expect(timeSlider).toBeTruthy();
  });
  it('should toggle slider visibility when clicking the hide button', () => {
    const mapId = 'mapid_3';
    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            baseLayers: [],
            mapLayers: [],
            overLayers: [],
            isTimeSliderVisible: true,
          },
        },
        allIds: [mapId],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
        viewState: {
          timeslider: {
            groups: [],
            sourcesById: [],
          },
          zoompane: {
            groups: [],
            sourcesById: [],
          },
          level: {
            groups: [],
            sourcesById: [],
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <ThemeStoreProvider store={store}>
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </ThemeStoreProvider>,
    );
    fireEvent.click(screen.getByTestId('hideTimeSliderButton'));

    const expectedAction = mapActions.toggleTimeSliderIsVisible({
      mapId,
      isTimeSliderVisible: false,
      origin: mapEnums.MapActionOrigin.map,
    });
    expect(store.getActions()).toEqual(
      expect.arrayContaining([expect.objectContaining(expectedAction)]),
    );
  });
});
