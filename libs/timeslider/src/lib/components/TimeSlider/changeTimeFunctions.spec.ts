/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import { dateUtils } from '@opengeoweb/shared';
import { handleSetNowEvent, moveSelectedTimePx } from './changeTimeFunctions';

describe('src/components/TimeSlider/changeTimeFunctions', () => {
  const date = `2000-01-15T`;
  describe('handleSetNowEvent', () => {
    it('should not set time if data time range is invalid', () => {
      const onSetNewDate = jest.fn();
      const onSetCenterTime = jest.fn();

      const start = dateUtils.unix(dateUtils.utc(`${date}09:00:00Z`));

      const endIsBeforeStart = dateUtils.unix(
        dateUtils.utc(`${date}06:00:00Z`),
      );

      handleSetNowEvent(
        0,
        start,
        endIsBeforeStart,
        endIsBeforeStart,
        onSetNewDate,
        onSetCenterTime,
      );

      expect(onSetNewDate).not.toBeCalled();
      expect(onSetCenterTime).not.toBeCalled();
    });

    it('should set data end time if data range is in the past', () => {
      const onSetNewDate = jest.fn();
      const onSetCenterTime = jest.fn();

      const start = dateUtils.unix(dateUtils.utc(`${date}09:00:00Z`));

      const endIso = `${date}15:00:00.000Z`;
      const end = dateUtils.unix(dateUtils.utc(endIso));

      const currentIsPastEnd = dateUtils.unix(
        dateUtils.utc(`${date}18:00:00Z`),
      );

      handleSetNowEvent(
        0,
        start,
        end,
        currentIsPastEnd,
        onSetNewDate,
        onSetCenterTime,
      );

      expect(onSetNewDate).toBeCalledWith(endIso);
      expect(onSetCenterTime).toBeCalledWith(end);
    });

    it('should set closest to current if data range is current', () => {
      const onSetNewDate = jest.fn();
      const onSetCenterTime = jest.fn();

      const start = dateUtils.unix(dateUtils.utc(`${date}09:00:00Z`));

      const currentIso = `${date}12:00:00.000Z`;
      const current = dateUtils.unix(dateUtils.utc(currentIso));

      const end = dateUtils.unix(dateUtils.utc(`${date}15:00:00Z`));

      handleSetNowEvent(60, start, end, current, onSetNewDate, onSetCenterTime);

      expect(onSetNewDate).toBeCalledWith(currentIso);
      expect(onSetCenterTime).toBeCalledWith(current);
    });
  });

  describe('moveSelectedTimePx', () => {
    const secondsPerPx = 60; // every pixel is 1 minute
    const moveMouse6pixelsEquals6minutes = 6; // mouse moved 6 pixels forward / 6 minutes
    const moveMouse240pixelsEquals4Hours = 240;

    // canvas/timeslider shows 24 hours. 60 width pixels * 60 seconds per pixel = 3600 seconds = 24 hours
    const canvasWidth = 60;
    const timeStep10minutes = 10; // new image every 10 minutes

    const dataStartTime0900 = dateUtils.unix(new Date(`${date}09:00:00.000Z`));

    const selectedTime1200Date = new Date(`${date}12:00:00.000Z`);
    const selectedTime1200 = dateUtils.unix(selectedTime1200Date);

    // center time and selected time is same which means time is in the middle of the canvas/timeslider for this test
    const centerTime1200 = selectedTime1200;
    const unfilteredSelectedTime1200 = selectedTime1200;

    const dataEndTime1500 = dateUtils.unix(new Date(`${date}15:00:00.000Z`));

    const setUnfilteredSelectedTime = jest.fn();
    const onSetNewDate = jest.fn();

    jest.useFakeTimers();
    beforeEach(() => {
      jest.resetAllMocks();
    });

    it('move time within data limits', () => {
      moveSelectedTimePx(
        moveMouse6pixelsEquals6minutes,
        canvasWidth,
        centerTime1200,
        dataStartTime0900,
        dataEndTime1500,
        secondsPerPx,
        timeStep10minutes,
        unfilteredSelectedTime1200,
        setUnfilteredSelectedTime,
        onSetNewDate,
      );

      expect(setUnfilteredSelectedTime).toBeCalledWith(
        dateUtils.unix(
          dateUtils.add(selectedTime1200Date, {
            minutes: moveMouse6pixelsEquals6minutes,
          }),
        ),
      );
      expect(onSetNewDate).not.toBeCalled();

      jest.runOnlyPendingTimers();
      expect(onSetNewDate).toBeCalledTimes(1);
      expect(onSetNewDate).toBeCalledWith(
        dateUtils
          .add(selectedTime1200Date, { minutes: timeStep10minutes })
          .toISOString(),
      );
    });

    it('uses debouncing to only update selectedTime once when calling function twice', () => {
      moveSelectedTimePx(
        moveMouse6pixelsEquals6minutes,
        canvasWidth,
        centerTime1200,
        dataStartTime0900,
        dataEndTime1500,
        secondsPerPx,
        timeStep10minutes,
        unfilteredSelectedTime1200,
        setUnfilteredSelectedTime,
        onSetNewDate,
      );

      moveSelectedTimePx(
        moveMouse6pixelsEquals6minutes,
        canvasWidth,
        centerTime1200,
        dataStartTime0900,
        dataEndTime1500,
        secondsPerPx,
        timeStep10minutes,
        unfilteredSelectedTime1200,
        setUnfilteredSelectedTime,
        onSetNewDate,
      );

      expect(setUnfilteredSelectedTime).toBeCalledTimes(2);
      expect(onSetNewDate).not.toBeCalled();

      jest.runOnlyPendingTimers();
      expect(onSetNewDate).toBeCalledTimes(1);
      expect(onSetNewDate).toBeCalledWith(
        dateUtils
          .add(selectedTime1200Date, { minutes: timeStep10minutes })
          .toISOString(),
      );
    });

    it('move time after end time, sets selected time to end time', () => {
      moveSelectedTimePx(
        moveMouse240pixelsEquals4Hours,
        canvasWidth,
        centerTime1200,
        dataStartTime0900,
        dataEndTime1500,
        secondsPerPx,
        timeStep10minutes,
        unfilteredSelectedTime1200,
        setUnfilteredSelectedTime,
        onSetNewDate,
      );

      expect(setUnfilteredSelectedTime).toBeCalledWith(
        dateUtils.unix(
          dateUtils.add(selectedTime1200Date, {
            minutes: moveMouse240pixelsEquals4Hours,
          }),
        ),
      );
      expect(onSetNewDate).not.toBeCalled();

      jest.runOnlyPendingTimers();

      expect(onSetNewDate).toBeCalledWith(
        dateUtils.fromUnix(dataEndTime1500).toISOString(),
      );
    });

    it('move time before start time, sets selected time to start time', () => {
      moveSelectedTimePx(
        -moveMouse240pixelsEquals4Hours,
        canvasWidth,
        centerTime1200,
        dataStartTime0900,
        dataEndTime1500,
        secondsPerPx,
        timeStep10minutes,
        unfilteredSelectedTime1200,
        setUnfilteredSelectedTime,
        onSetNewDate,
      );

      expect(setUnfilteredSelectedTime).toBeCalledWith(
        dateUtils.unix(
          dateUtils.sub(selectedTime1200Date, {
            minutes: moveMouse240pixelsEquals4Hours,
          }),
        ),
      );
      expect(onSetNewDate).not.toBeCalled();

      jest.runOnlyPendingTimers();

      expect(onSetNewDate).toBeCalledWith(
        dateUtils.fromUnix(dataStartTime0900).toISOString(),
      );
    });
  });
});
