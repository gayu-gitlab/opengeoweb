/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { Theme } from '@mui/material';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import moment from 'moment';
import React, { CSSProperties, FC, useState } from 'react';

import { TimeSliderClock } from './TimeSliderClock';
import { ThemeProvider } from '../../testUtils/Providers';

export default {
  title: 'components/TimeSlider/TimeSliderClock',
};

const DemoTimeSliderClock: FC<{ theme: Theme; left: boolean }> = ({
  theme,
  left,
}) => {
  const [hideButton, setHideButton] = useState(false);

  React.useEffect(() => {
    const handleKeyDown = (event: KeyboardEvent): void => {
      if (event.key === 't') {
        setHideButton((hideButton) => !hideButton);
      }
    };

    document.addEventListener('keydown', handleKeyDown);
    return (): void => {
      document.removeEventListener('keydown', handleKeyDown);
    };
  }, []);

  const style: CSSProperties = {
    position: 'relative',
    height: left ? 100 : 400,
    width: left ? 700 : 400,
  };

  return (
    <ThemeProvider theme={theme}>
      <div style={style}>
        <TimeSliderClock
          time={moment(0)}
          hideButton={hideButton}
          isPopperOpenByDefault={true}
        />
      </div>
    </ThemeProvider>
  );
};

const zeplinLight = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/609aac2d83b125bd6e2040b7/version/62c841daa73da317154f7aea',
    },
  ],
};

const zeplinDark = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5eea2a91efab124620a1c354/version/62c841ee31ced8193a45c613',
    },
  ],
};

export const DemoTimeSliderClockMenuLeftLight = (): JSX.Element => (
  <DemoTimeSliderClock theme={lightTheme} left={true} />
);
DemoTimeSliderClockMenuLeftLight.storyName =
  'Time Slider Clock Menu Left Light (takeSnapshot)';
DemoTimeSliderClockMenuLeftLight.parameters = zeplinLight;

export const DemoTimeSliderClockMenuLeftDark = (): JSX.Element => (
  <DemoTimeSliderClock theme={darkTheme} left={true} />
);
DemoTimeSliderClockMenuLeftDark.storyName =
  'Time Slider Clock Menu Left Dark (takeSnapshot)';
DemoTimeSliderClockMenuLeftDark.parameters = zeplinDark;

export const DemoTimeSliderClockMenuBottomLight = (): JSX.Element => (
  <DemoTimeSliderClock theme={lightTheme} left={false} />
);
DemoTimeSliderClockMenuBottomLight.storyName =
  'Time Slider Clock Menu Bottom Light (takeSnapshot)';
DemoTimeSliderClockMenuBottomLight.parameters = zeplinLight;

export const DemoTimeSliderClockMenuBottomDark = (): JSX.Element => (
  <DemoTimeSliderClock theme={darkTheme} left={false} />
);
DemoTimeSliderClockMenuBottomDark.storyName =
  'Time Slider Clock Menu Bottom Dark (takeSnapshot)';
DemoTimeSliderClockMenuBottomDark.parameters = zeplinDark;
