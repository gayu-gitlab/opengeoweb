/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Box, useTheme } from '@mui/material';
import { useCallback, useState, useEffect, useRef } from 'react';
import { CustomTooltip, dateUtils } from '@opengeoweb/shared';
import { mapConstants, mapUtils } from '@opengeoweb/store';
import { CanvasComponent } from '@opengeoweb/webmap-react';
import { debounce, throttle } from 'lodash';
import { drawTimeSliderLegend } from './drawFunctions';
import {
  pixelToTimestamp,
  useCanvasTarget,
  timestampToPixel,
  secondsPerPxFromCanvasWidth,
} from '../TimeSlider/timeSliderUtils';
import {
  getFilteredTime,
  moveSelectedTimePx,
} from '../TimeSlider/changeTimeFunctions';
import {
  isInsideAnimationArea,
  isLeftAnimationIconArea,
  isRightAnimationIconArea,
  isSelectedTimeIconArea,
} from './timeSliderLegendUtils';

export const TIME_SLIDER_LEGEND_HEIGHT = 24;
export const DRAG_AREA_WIDTH = 24;

const timeValues = [
  3600, 10800, 21600, 43200, 86400, 172800, 604800, 1209600, 2592000, 7776000,
  31536000,
];

function convertStringTimeToUnix(time: string | undefined): number | undefined {
  return time ? dateUtils.unix(new Date(time)) : undefined;
}

type ShowTooltip = 'start' | 'end' | undefined;

export interface TimeSliderLegendProps {
  mapId: string;
  centerTime: number;
  timeSliderWidth: number;
  secondsPerPx: number;
  selectedTime?: number;
  currentTime?: number;
  dataStartTime?: number;
  dataEndTime?: number;
  reduxAnimationStartTime?: string;
  reduxAnimationEndTime?: string;
  isTimeSliderHoverOn?: boolean;
  timeStep?: number;
  unfilteredSelectedTime?: number;
  timeSliderSpan?: number;
  setUnfilteredSelectedTime?: (unfilteredSelectedTime: number) => void;
  onSetNewDate?: (newDate: string) => void;
  onSetCenterTime?: (newTime: number) => void;
  // eslint-disable-next-line react/no-unused-prop-types
  onZoom?: (newSecondsPerPx: number, newCenterTime: number) => void;
  onSetAnimationStartTime?: (time: string) => void;
  onSetAnimationEndTime?: (time: string) => void;
  updateCanvasWidth?: (storeWidth: number, newWidth: number) => void;
  isDraggingStartAnimation?: boolean;
  setIsDraggingStartAnimation?: (isDraggingStartAnimation: boolean) => void;
  isDraggingEndAnimation?: boolean;
  setIsDraggingEndAnimation?: (isDraggingEndAnimation: boolean) => void;
  onStartTimeChange?: (startTime: number) => void;
  onEndTimeChange?: (endTime: number) => void;
  onSetTimeSliderSpan?: (
    newSpan: number,
    newCenterTime: number,
    newSecondsPerPx: number,
  ) => void;
}

// Explanation of props can be found here:
// https://drive.google.com/file/d/1jqqNcciCH0UJiZ04HO-1vknmPPpT8fK5/view?usp=sharing

export const TimeSliderLegend: React.FC<TimeSliderLegendProps> = ({
  mapId,
  centerTime,
  timeSliderWidth: canvasWidth,
  secondsPerPx = mapConstants.defaultSecondsPerPx,
  selectedTime,
  currentTime,
  dataStartTime,
  dataEndTime,
  reduxAnimationStartTime,
  reduxAnimationEndTime,
  timeStep,
  unfilteredSelectedTime,
  isDraggingStartAnimation,
  isDraggingEndAnimation,
  isTimeSliderHoverOn,
  timeSliderSpan,
  setUnfilteredSelectedTime = (): void => {},
  onSetNewDate = (): void => {},
  onSetCenterTime = (): void => {},
  onSetAnimationStartTime = (): void => {},
  onSetAnimationEndTime = (): void => {},
  updateCanvasWidth = (): void => {},
  setIsDraggingStartAnimation,
  setIsDraggingEndAnimation,
  onStartTimeChange,
  onEndTimeChange,
  onSetTimeSliderSpan,
}) => {
  const [, node] = useCanvasTarget('mousedown');
  const [isDraggingSelectedTime, setIsDraggingSelectedTime] = useState(false);
  const [isDraggingLegend, setIsDraggingLegend] = useState(false);
  const xOffset = node.current?.canvas?.getBoundingClientRect().x || 0;
  const [isDraggingAnimationArea, setIsDraggingAnimationArea] = useState(false);
  const [isMouseOverCanvas, setIsMouseOverCanvas] = useState(false);

  const [cursorStyle, setCursorStyle] = useState('auto');

  /**
   * remove active drag. can happen outside canvas.
   */

  const [localAnimationStartTime, setLocalAnimationStartTime] = useState<
    number | undefined
  >();
  const [localAnimationEndTime, setLocalAnimationEndTime] = useState<
    number | undefined
  >();

  useEffect(() => {
    const startTimestamp = convertStringTimeToUnix(reduxAnimationStartTime);
    const endTimestamp = convertStringTimeToUnix(reduxAnimationEndTime);
    setLocalAnimationStartTime(startTimestamp);
    setLocalAnimationEndTime(endTimestamp);
  }, [reduxAnimationStartTime, reduxAnimationEndTime]);

  const timeBetweenMouseAndStartAndEnd = useRef<
    { timeFromMouseToStart: number; timeFromMouseToEnd: number } | undefined
  >();
  const pixelsMovedSinceStartDragging = useRef(0);

  useEffect(() => {
    const handleMouseUp = (): void => {
      pixelsMovedSinceStartDragging.current = 0;
      timeBetweenMouseAndStartAndEnd.current = undefined;
    };
    document.addEventListener('mouseup', handleMouseUp);
    return (): void => {
      document.removeEventListener('mouseup', handleMouseUp);
    };
  }, []);

  const roundedStartAnimationTime = mapUtils.roundWithTimeStep(
    localAnimationStartTime!,
    timeStep!,
  );
  const roundedEndAnimationTime = mapUtils.roundWithTimeStep(
    localAnimationEndTime!,
    timeStep!,
  );

  const isAnimationTimeModifiedSetRef = useRef(false);

  useEffect(() => {
    const stoppedDragging =
      !isDraggingStartAnimation &&
      !isDraggingEndAnimation &&
      !isDraggingAnimationArea;

    if (stoppedDragging && isAnimationTimeModifiedSetRef.current) {
      if (
        roundedStartAnimationTime &&
        roundedStartAnimationTime !== localAnimationStartTime
      ) {
        onSetAnimationStartTime(
          dateUtils.fromUnix(roundedStartAnimationTime).toISOString(),
        );
      }
      if (
        roundedEndAnimationTime &&
        roundedEndAnimationTime !== localAnimationEndTime
      ) {
        onSetAnimationEndTime(
          dateUtils.fromUnix(roundedEndAnimationTime).toISOString(),
        );
      }

      isAnimationTimeModifiedSetRef.current = false;
    }
  }, [
    isDraggingStartAnimation,
    isDraggingEndAnimation,
    isDraggingAnimationArea,
    localAnimationStartTime,
    localAnimationEndTime,
    onSetAnimationStartTime,
    onSetAnimationEndTime,
    roundedStartAnimationTime,
    roundedEndAnimationTime,
  ]);

  useEffect(() => {
    const handleMouseUp = (): void => {
      if (isDraggingAnimationArea) {
        onStartTimeChange && onStartTimeChange(localAnimationStartTime!);
        onEndTimeChange && onEndTimeChange(localAnimationEndTime!);
      }
      setCursorStyle('auto');
      setIsDraggingSelectedTime(false);
      setIsDraggingStartAnimation && setIsDraggingStartAnimation(false);
      setIsDraggingEndAnimation && setIsDraggingEndAnimation(false);
      setIsDraggingLegend(false);
      setShowTooltip(undefined);
      setIsDraggingAnimationArea(false);
      startDraggingPosition.current = 0;
      if (isAnimationTimeModifiedSetRef.current) {
        isAnimationTimeModifiedSetRef.current = false;
      }
    };
    document.addEventListener('mouseup', handleMouseUp);
    return (): void => {
      document.removeEventListener('mouseup', handleMouseUp);
    };
  }, [
    setIsDraggingStartAnimation,
    setIsDraggingEndAnimation,
    localAnimationStartTime,
    localAnimationEndTime,
    onStartTimeChange,
    onEndTimeChange,
    isDraggingAnimationArea,
  ]);

  const tooltipPosition = useRef<number>();
  const startDraggingPosition = useRef(0);

  const timeSliderLegendId = `timeSliderLegend_${mapId}`;
  const timeSliderLegendElement = document.querySelector(
    `.${timeSliderLegendId}`,
  );
  const pixelsBetweenViewportAndTimesliderOnLeft =
    timeSliderLegendElement?.getBoundingClientRect()['left'];

  const [showTooltip, setShowTooltip] = useState<ShowTooltip>(undefined);

  const [lastHandledRoundedTime, setLastHandledRoundedTime] =
    useState<number>();

  const handleAnimationDragging = useCallback(
    (x: number, clientX: number) => {
      if (!localAnimationStartTime || !localAnimationEndTime) {
        return;
      }

      if (
        isDraggingStartAnimation ||
        isDraggingEndAnimation ||
        isDraggingAnimationArea
      ) {
        if (!isAnimationTimeModifiedSetRef.current) {
          isAnimationTimeModifiedSetRef.current = true;
        }
      }

      const startAnimationPosition = timestampToPixel(
        localAnimationStartTime,
        centerTime,
        canvasWidth,
        secondsPerPx,
      );

      const endAnimationPosition = timestampToPixel(
        localAnimationEndTime,
        centerTime,
        canvasWidth,
        secondsPerPx,
      );

      if (isDraggingStartAnimation) {
        const mousePosition = clientX - xOffset;
        const startAnimationMaxPosition =
          endAnimationPosition - DRAG_AREA_WIDTH * 2;
        const isDraggingWithinBounds =
          mousePosition >= startAnimationMaxPosition || mousePosition <= 0;
        if (isDraggingWithinBounds) {
          return;
        }

        // eslint-disable-next-line no-param-reassign
        tooltipPosition.current =
          pixelsBetweenViewportAndTimesliderOnLeft &&
          mousePosition + pixelsBetweenViewportAndTimesliderOnLeft;

        const mouseTimeUnix = pixelToTimestamp(
          mousePosition,
          centerTime,
          canvasWidth,
          secondsPerPx,
        );

        const roundedStartTime = mapUtils.roundWithTimeStep(
          mouseTimeUnix,
          timeStep!,
        );
        setLocalAnimationStartTime(mouseTimeUnix);

        if (lastHandledRoundedTime !== roundedStartTime) {
          setLastHandledRoundedTime(roundedStartTime);
          setShowTooltip('start');
          onStartTimeChange && onStartTimeChange(roundedStartTime);
        }
      } else if (isDraggingEndAnimation) {
        const mousePosition = clientX - xOffset;
        const endAnimationMinimumPosition =
          startAnimationPosition + DRAG_AREA_WIDTH * 2;
        const isDraggingWithinBounds =
          endAnimationMinimumPosition >= mousePosition ||
          mousePosition >= canvasWidth;
        if (isDraggingWithinBounds) {
          return;
        }

        // eslint-disable-next-line no-param-reassign
        tooltipPosition.current =
          pixelsBetweenViewportAndTimesliderOnLeft &&
          mousePosition + pixelsBetweenViewportAndTimesliderOnLeft;

        const mouseTimeUnix = pixelToTimestamp(
          mousePosition,
          centerTime,
          canvasWidth,
          secondsPerPx,
        );

        const roundedEndTime = mapUtils.roundWithTimeStep(
          mouseTimeUnix,
          timeStep!,
        );

        setLocalAnimationEndTime(mouseTimeUnix);

        if (lastHandledRoundedTime !== roundedEndTime) {
          setLastHandledRoundedTime(roundedEndTime);
          setShowTooltip('end');
          onEndTimeChange && onEndTimeChange(roundedEndTime);
        }
      }
      if (isDraggingAnimationArea) {
        const mousePosition = clientX - xOffset;
        if (timeBetweenMouseAndStartAndEnd.current === undefined) {
          // to move the whole animation area we need to calculate the distance
          // between the mouse position and the start and end position
          const mousePositionTimestamp = pixelToTimestamp(
            mousePosition,
            centerTime,
            canvasWidth,
            secondsPerPx,
          );

          const timeFromMouseToStart =
            mousePositionTimestamp - roundedStartAnimationTime;
          const timeFromMouseToEnd =
            roundedEndAnimationTime - mousePositionTimestamp;
          timeBetweenMouseAndStartAndEnd.current = {
            timeFromMouseToEnd,
            timeFromMouseToStart,
          };
          return;
        }
        pixelsMovedSinceStartDragging.current += x;

        const mousePositionTimestamp = pixelToTimestamp(
          mousePosition,
          centerTime,
          canvasWidth,
          secondsPerPx,
        );
        const { timeFromMouseToEnd, timeFromMouseToStart } =
          timeBetweenMouseAndStartAndEnd.current;

        const newStartTime = mousePositionTimestamp - timeFromMouseToStart;
        const newEndTime = mousePositionTimestamp + timeFromMouseToEnd;

        const roundedNewStartTime = mapUtils.roundWithTimeStep(
          newStartTime,
          timeStep!,
        );
        const roundedNewEndTime = mapUtils.roundWithTimeStep(
          newEndTime,
          timeStep!,
        );

        setLocalAnimationStartTime(roundedNewStartTime);
        setLocalAnimationEndTime(roundedNewEndTime);
      }
    },
    [
      localAnimationStartTime,
      localAnimationEndTime,
      isDraggingStartAnimation,
      isDraggingEndAnimation,
      centerTime,
      canvasWidth,
      secondsPerPx,
      isDraggingAnimationArea,
      xOffset,
      pixelsBetweenViewportAndTimesliderOnLeft,
      timeStep,
      lastHandledRoundedTime,
      onStartTimeChange,
      onEndTimeChange,
      roundedStartAnimationTime,
      roundedEndAnimationTime,
    ],
  );

  useEffect(() => {
    const handleMouseActions = (event: MouseEvent): void => {
      if (event.movementX === 0) {
        return;
      }

      if (isDraggingLegend) {
        const dragDistanceTime = event.movementX * secondsPerPx;
        const newCenterTime = centerTime - dragDistanceTime;
        onSetCenterTime(newCenterTime);
      } else if (
        isDraggingStartAnimation ||
        isDraggingEndAnimation ||
        isDraggingAnimationArea
      ) {
        handleAnimationDragging(event.movementX, event.clientX);
      } else if (isDraggingSelectedTime) {
        moveSelectedTimePx(
          event.movementX,
          canvasWidth,
          centerTime,
          dataStartTime!,
          dataEndTime!,
          secondsPerPx,
          timeStep!,
          unfilteredSelectedTime!,
          setUnfilteredSelectedTime,
          onSetNewDate,
        );
      }
      if (isMouseOverCanvas && isTimeSliderHoverOn) {
        moveSelectedTimePx(
          event.movementX,
          canvasWidth,
          centerTime,
          dataStartTime!,
          dataEndTime!,
          secondsPerPx,
          timeStep!,
          unfilteredSelectedTime!,
          setUnfilteredSelectedTime,
          onSetNewDate,
        );
      }
    };

    const handleMouseMove = (event: MouseEvent): void => {
      handleMouseActions(event);
    };

    document.addEventListener('mousemove', handleMouseMove);
    return (): void => {
      document.removeEventListener('mousemove', handleMouseMove);
    };
  }, [
    isDraggingLegend,
    isDraggingStartAnimation,
    isDraggingEndAnimation,
    isDraggingAnimationArea,
    handleAnimationDragging,
    isDraggingSelectedTime,
    canvasWidth,
    centerTime,
    dataStartTime,
    dataEndTime,
    secondsPerPx,
    timeStep,
    unfilteredSelectedTime,
    setUnfilteredSelectedTime,
    onSetNewDate,
    onSetCenterTime,
    isTimeSliderHoverOn,
    isMouseOverCanvas,
  ]);

  const theme = useTheme();

  const isClickOrDrag = useRef<'click' | 'drag' | undefined>();

  const onMouseDown = (x: number, y: number, width: number): void => {
    isClickOrDrag.current = 'click';
    startDraggingPosition.current = x;

    const {
      startAnimationPosition,
      endAnimationPosition,
      selectedTimePosition,
    } = getPositions(width);

    // start dragging selected time.
    if (isSelectedTimeIconArea(x, selectedTimePosition, DRAG_AREA_WIDTH)) {
      setCursorStyle('grabbing');
      setIsDraggingSelectedTime(true);
      return;
    }
    if (reduxAnimationStartTime && reduxAnimationEndTime) {
      tooltipPosition.current = pixelsBetweenViewportAndTimesliderOnLeft
        ? x + pixelsBetweenViewportAndTimesliderOnLeft
        : x;

      // start dragging either marker
      if (isLeftAnimationIconArea(x, startAnimationPosition, DRAG_AREA_WIDTH)) {
        setCursorStyle('grabbing');
        setIsDraggingStartAnimation && setIsDraggingStartAnimation(true);
        setShowTooltip('start');

        return;
      }
      if (isRightAnimationIconArea(x, endAnimationPosition, DRAG_AREA_WIDTH)) {
        setCursorStyle('grabbing');
        setIsDraggingEndAnimation && setIsDraggingEndAnimation(true);
        setShowTooltip('end');
        return;
      }
      if (
        isInsideAnimationArea(x, startAnimationPosition, endAnimationPosition)
      ) {
        setCursorStyle('grabbing');
        setIsDraggingAnimationArea(true);

        return;
      }
    }
    // clicking the timeslider
    setIsDraggingLegend(true);
  };

  const onMouseUp = (x: number): void => {
    if (isClickOrDrag.current === 'click') {
      const unfilteredSelectedTimePx = timestampToPixel(
        unfilteredSelectedTime as number,
        centerTime,
        canvasWidth,
        secondsPerPx,
      );
      moveSelectedTimePx(
        x - unfilteredSelectedTimePx,
        canvasWidth,
        centerTime,
        dataStartTime!,
        dataEndTime!,
        secondsPerPx,
        timeStep!,
        unfilteredSelectedTime!,
        setUnfilteredSelectedTime,
        onSetNewDate!,
      );
    }
    isClickOrDrag.current = undefined;
  };

  const onMouseMove = (
    x: number,
    y: number,
    event: MouseEvent,
    width: number,
  ): void => {
    if (isClickOrDrag.current === 'click') {
      isClickOrDrag.current = 'drag';
    }
    if (
      isDraggingSelectedTime ||
      isDraggingStartAnimation ||
      isDraggingEndAnimation ||
      isDraggingAnimationArea
    ) {
      return;
    }
    const {
      startAnimationPosition,
      endAnimationPosition,
      selectedTimePosition,
    } = getPositions(width);

    // Adjust mouse cursor while hovering draggable areas
    const leftAnimationIconArea = isLeftAnimationIconArea(
      x,
      startAnimationPosition,
      DRAG_AREA_WIDTH,
    );
    const rightAnimationIconArea = isRightAnimationIconArea(
      x,
      endAnimationPosition,
      DRAG_AREA_WIDTH,
    );
    const selectedTimeArea = isSelectedTimeIconArea(
      x,
      selectedTimePosition,
      DRAG_AREA_WIDTH,
    );
    const insideAnimationArea = isInsideAnimationArea(
      x,
      startAnimationPosition,
      endAnimationPosition,
    );
    const hoveringLegendHandles =
      leftAnimationIconArea ||
      rightAnimationIconArea ||
      selectedTimeArea ||
      isDraggingLegend;
    if (hoveringLegendHandles) {
      setCursorStyle('ew-resize');
    } else if (insideAnimationArea && !isDraggingAnimationArea) {
      setCursorStyle('grab');
    } else if (insideAnimationArea && isDraggingAnimationArea) {
      setCursorStyle('grabbing');
    } else {
      setCursorStyle('auto');
    }
  };

  const roundWithTimeStep = (time: number): number =>
    mapUtils.roundWithTimeStep(time, timeStep!);

  function getPositions(width: number): {
    startAnimationPosition: number;
    endAnimationPosition: number;
    selectedTimePosition: number;
  } {
    const selectedTimePosition = timestampToPixel(
      selectedTime!,
      centerTime,
      width,
      secondsPerPx,
    );
    const endAnimationPosition = timestampToPixel(
      roundedEndAnimationTime,
      centerTime,
      width,
      secondsPerPx,
    );
    const startAnimationPosition = timestampToPixel(
      roundedStartAnimationTime,
      centerTime,
      width,
      secondsPerPx,
    );
    return {
      startAnimationPosition,
      endAnimationPosition,
      selectedTimePosition,
    };
  }

  const currentIndex = timeValues.indexOf(timeSliderSpan!);

  const updateTimeSpan = (deltaY: number): void => {
    if (onSetTimeSliderSpan) {
      // Don't do anything if we are already at the edges of the timespan options
      if (
        (deltaY < 0 && currentIndex === 0) ||
        (deltaY > 0 && currentIndex === timeValues.length - 1)
      ) {
        return;
      }
      const newTimeSpan =
        deltaY < 0
          ? timeValues[currentIndex - 1]
          : timeValues[currentIndex + 1];

      const newSecondsPerPx = secondsPerPxFromCanvasWidth(
        canvasWidth,
        newTimeSpan,
      );
      onSetTimeSliderSpan(newTimeSpan, centerTime, newSecondsPerPx!);
    }
  };

  const throttledUpdateTimeSpan = throttle(updateTimeSpan, 200);
  const debouncedAndThrottledUpdateTimeSpan = debounce(
    throttledUpdateTimeSpan,
    20,
  );

  const onWheel = ({
    event,
    deltaY,
  }: {
    event: WheelEvent | KeyboardEvent;
    deltaY: number;
  }): void => {
    event.preventDefault();
    event.stopPropagation();
    if (event.ctrlKey || event.metaKey) {
      debouncedAndThrottledUpdateTimeSpan(deltaY);
    } else {
      const pixelsPerScroll = (-deltaY * timeStep!) / (2 * secondsPerPx);
      moveSelectedTimePx(
        pixelsPerScroll,
        canvasWidth,
        centerTime,
        dataStartTime!,
        dataEndTime!,
        secondsPerPx,
        timeStep!,
        unfilteredSelectedTime!,
        setUnfilteredSelectedTime,
        onSetNewDate,
      );
    }
  };

  const popperRef = useRef(null);
  const pixelsBetweenViewportAndTimesliderOnTop =
    timeSliderLegendElement?.getBoundingClientRect()['top'];

  const setTooltipPosition = (): DOMRect => {
    const tooltipX = tooltipPosition.current;
    const tooltipY =
      pixelsBetweenViewportAndTimesliderOnTop &&
      pixelsBetweenViewportAndTimesliderOnTop - TIME_SLIDER_LEGEND_HEIGHT * 2.5;
    return new DOMRect(tooltipX, tooltipY, 0, 0);
  };

  // when selectedtime is updated from outside components
  useEffect(() => {
    if (
      !isDraggingLegend &&
      selectedTime &&
      unfilteredSelectedTime &&
      unfilteredSelectedTime !== selectedTime
    ) {
      setUnfilteredSelectedTime(selectedTime);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedTime]);

  const getTooltipText = (): string | undefined => {
    if (!showTooltip) {
      return undefined;
    }
    const time =
      showTooltip === 'start' ? localAnimationStartTime : localAnimationEndTime;
    const roundedTooltipTime = roundWithTimeStep(time!);
    const tooltipText = dateUtils.dateToString(
      dateUtils.fromUnix(roundedTooltipTime),
      'HH:mm',
    );
    return tooltipText;
  };

  return (
    <Box
      sx={{ position: 'relative', height: `${TIME_SLIDER_LEGEND_HEIGHT}px` }}
    >
      <CustomTooltip
        title={getTooltipText()}
        open={Boolean(showTooltip)}
        PopperProps={{
          popperRef,
          anchorEl: { getBoundingClientRect: setTooltipPosition },
        }}
      >
        <Box
          data-testid="timeSliderLegend"
          className={timeSliderLegendId}
          sx={{
            height: `${TIME_SLIDER_LEGEND_HEIGHT}px`,
            borderRadius: '4.5px',
            overflow: 'hidden',
            cursor: cursorStyle,
          }}
        >
          <CanvasComponent
            ref={node}
            onMouseMove={onMouseMove}
            onWheel={onWheel}
            onMouseDown={onMouseDown}
            onMouseUp={onMouseUp}
            onRenderCanvas={(
              ctx: CanvasRenderingContext2D,
              width: number,
              height: number,
            ): void => {
              updateCanvasWidth(canvasWidth, width);
              const filteredTime = getFilteredTime(
                unfilteredSelectedTime!,
                timeStep,
                dataStartTime,
                dataEndTime,
              )!;

              drawTimeSliderLegend(
                ctx,
                theme,
                width,
                height,
                centerTime,
                secondsPerPx,
                filteredTime,
                currentTime!,
                roundedStartAnimationTime,
                roundedEndAnimationTime,
                dataStartTime as number,
                dataEndTime as number,
              );
            }}
          />
        </Box>
      </CustomTooltip>
      {isTimeSliderHoverOn && (
        <Box
          data-testid="customCanvasMouseOverContainer"
          sx={{
            position: 'absolute',
            top: '-40px',
            left: 0,
            right: 0,
            bottom: 0,
            height: `${TIME_SLIDER_LEGEND_HEIGHT} + 40px`,
            zIndex: isMouseOverCanvas ? 100 : 0,
          }}
          onMouseOver={(): void => setIsMouseOverCanvas(true)}
          onMouseLeave={(): void => setIsMouseOverCanvas(false)}
        />
      )}
    </Box>
  );
};
