/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { Theme } from '@mui/material';
import { dragHandlePath } from '@opengeoweb/theme';
import moment from 'moment';
import { mapEnums } from '@opengeoweb/store';
import {
  getFundamentalScale,
  timestampToPixelEdges,
} from '../../TimeSlider/timeSliderUtils';
import {
  getCustomRoundedStartAndEnd,
  getCustomTimesteps,
} from './drawTimeScale';

export const drawBackground = (
  context: CanvasRenderingContext2D,
  theme: Theme,
  visibleTimeStart: number,
  visibleTimeEnd: number,
  animationStartPx: number,
  animationEndPx: number,
  canvasWidth: number,
  height: number,
  secondsPerPx: number,
  dataStartPx: number,
  dataEndPx: number,
  currentTimePx: number,
): void => {
  const ctx = context;
  const {
    timelineNightTime,
    ledgendObservedBackground,
    legendForecastBackground,
    legendNoDataBackground,
  } = theme.palette.geowebColors.timeSlider;

  // top spacing
  const y = 0;

  const fundamentalScale = getFundamentalScale(secondsPerPx);

  drawDefaultBackground();
  drawObservedData();
  drawForecastData();
  drawNightOrAlternateTime();
  drawAnimationArea();
  drawDraggableIcons();

  function drawDefaultBackground(): void {
    ctx.fillStyle = legendNoDataBackground.rgba!;
    ctx.fillRect(0, y, canvasWidth, height);
  }

  function drawObservedData(): void {
    if (dataStartPx! < currentTimePx) {
      ctx.fillStyle = ledgendObservedBackground.fill!;
      ctx.fillRect(
        Math.max(dataStartPx!, y),
        y,
        Math.min(currentTimePx, dataEndPx!) - Math.max(dataStartPx!, y),
        height,
      );
    }
  }

  function drawForecastData(): void {
    if (dataEndPx! > currentTimePx) {
      ctx.fillStyle = legendForecastBackground.fill!;
      ctx.fillRect(
        Math.max(currentTimePx, dataStartPx!),
        y,
        Math.min(canvasWidth, dataEndPx!) -
          Math.max(currentTimePx, dataStartPx!),
        height,
      );
    }
  }

  function drawAnimationArea(): void {
    ctx.fillStyle =
      theme.palette.geowebColors.timeSlider.timelineSelectionBackground.rgba!;
    ctx.strokeStyle =
      theme.palette.geowebColors.timeSlider.timelineSelectionOutline.rgba!;
    ctx.lineWidth = 3;
    ctx.rect(
      animationStartPx,
      y,
      Math.max(animationEndPx - animationStartPx, y),
      height,
    );
    ctx.fill();
    ctx.stroke();
  }

  function drawDraggableIcons(): void {
    ctx.save();
    const path = new Path2D(dragHandlePath);
    ctx.translate(animationStartPx, y);
    ctx.fillStyle =
      theme.palette.geowebColors.timeSlider.timelineTimeScale.fill!;
    ctx.fill(path);
    ctx.restore();
    ctx.save();
    ctx.translate(animationEndPx - 24, y);
    ctx.fillStyle =
      theme.palette.geowebColors.timeSlider.timelineTimeScale.fill!;
    ctx.fill(path);
    ctx.restore();
  }

  function drawNightOrAlternateTime(): void {
    const colorChangeTimesteps = getColorChangeTimesteps(
      fundamentalScale,
      visibleTimeStart,
      visibleTimeEnd,
    );
    colorChangeTimesteps.forEach((timestep, i, arr) => {
      if (i === 0) {
        return;
      }
      const [prevPx, currPx] = [arr[i - 1], timestep].map((timestep) =>
        timestampToPixelEdges(
          timestep,
          visibleTimeStart,
          visibleTimeEnd,
          canvasWidth,
        ),
      );
      if (!isColorIntervalEven(fundamentalScale, timestep)) {
        ctx.fillStyle = timelineNightTime.rgba!;
        const TOP_OFFSET = 16;
        ctx.fillRect(prevPx, TOP_OFFSET, currPx - prevPx, height);
      }
    });
  }
};

const getColorChangeTimestepUnit = (scale: mapEnums.Scale): string => {
  switch (scale) {
    case mapEnums.Scale.Year:
      return 'year';
    case mapEnums.Scale.Month:
      return 'month';
    case mapEnums.Scale.Week:
      return 'isoWeek';
    default:
      return 'day';
  }
};
const isColorIntervalEven = (
  scale: mapEnums.Scale,
  timestep: number,
): boolean => {
  const momentTime = moment.unix(timestep).utc();
  switch (scale) {
    case mapEnums.Scale.Year:
      return momentTime.year() % 2 === 0;
    case mapEnums.Scale.Month:
      return momentTime.month() % 2 === 0;
    case mapEnums.Scale.Week:
      return momentTime.isoWeek() % 2 === 0;
    default: {
      const NIGHT_TIME = 21;
      return moment.unix(timestep).utc().hour() === NIGHT_TIME;
    }
  }
};

export function getColorChangeTimesteps(
  scale: mapEnums.Scale,
  visibleTimeStart: number,
  visibleTimeEnd: number,
): number[] {
  const colorChangeUnit = getColorChangeTimestepUnit(scale);
  const [colorChangeStart, colorChangeEnd] = getCustomRoundedStartAndEnd(
    visibleTimeStart,
    visibleTimeEnd,
    colorChangeUnit,
    true,
  );
  const colorChangeTimesteps = getCustomTimesteps(
    colorChangeStart,
    colorChangeEnd,
    colorChangeUnit,
    true,
  );
  return colorChangeTimesteps;
}
