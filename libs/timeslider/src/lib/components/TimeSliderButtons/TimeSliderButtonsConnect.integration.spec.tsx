/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import {
  layerReducer,
  webmapReducer,
  mapActions,
  layerActions,
  genericSaga,
  syncGroupsReducer,
  mapSaga,
} from '@opengeoweb/store';
import { configureStore } from '@reduxjs/toolkit';
import { Provider } from 'react-redux';
import userEvent from '@testing-library/user-event';
import produce from 'immer';
import { Dimension, webmapTestSettings } from '@opengeoweb/webmap';
import createSagaMiddleware from 'redux-saga';
import { dateUtils } from '@opengeoweb/shared';
import { TimeSliderButtonsConnect } from './TimeSliderButtonsConnect';

describe('src/components/TimeSlider/TimeSliderButtons/TimeSliderButtonsConnect', () => {
  it('should work for backward, forward and now buttons', async () => {
    jest.useFakeTimers();

    const sagaMiddleware = createSagaMiddleware();
    const store = configureStore({
      reducer: {
        layers: layerReducer,
        webmap: webmapReducer,
        syncronizationGroupStore: syncGroupsReducer,
      },
      middleware: (getDefaultMiddleware) =>
        getDefaultMiddleware().concat(sagaMiddleware),
    });
    sagaMiddleware.run(genericSaga);
    sagaMiddleware.run(mapSaga);

    const mapId = 'mapId';
    store.dispatch(mapActions.registerMap({ mapId }));

    const date = '2023-01-31T';
    const oneHourTimestep = `PT1H`;
    const maxValue = `${date}13:00:00Z`;
    const selectedTime = `${date}12:00:00Z`;
    const timeDimension: Dimension[] = [
      {
        name: 'time',
        units: 'ISO8601',
        currentValue: selectedTime,
        // layer has images for 12, 13 clock
        values: `${selectedTime}/${maxValue}/${oneHourTimestep}`,
        maxValue,
        minValue: selectedTime,
      },
    ];
    const layerId = 'layerId';
    const layer = produce(
      webmapTestSettings.defaultReduxLayerRadarColor,
      (draft) => {
        draft.id = layerId;
        draft.dimensions = timeDimension;
      },
    );

    store.dispatch(
      layerActions.addLayer({ layer, layerId, mapId, origin: '' }),
    );
    store.dispatch(mapActions.setTimeStep({ mapId, timeStep: 60 }));
    store.dispatch(
      mapActions.mapUpdateAllMapDimensions({
        mapId,
        dimensions: timeDimension,
        origin: '',
      }),
    );
    store.dispatch(
      mapActions.setTimeSliderCenterTime({
        mapId,
        timeSliderCenterTime: dateUtils.unix(new Date(maxValue)),
      }),
    );

    jest.setSystemTime(new Date(`${date}12:15:00Z`));
    render(
      <Provider store={store}>
        <div>
          <TimeSliderButtonsConnect mapId={mapId} sourceId={mapId} />
        </div>
      </Provider>,
    );
    const user = userEvent.setup();

    const getMapTimeDimensionCurrentValue = (): string => {
      return store.getState().webmap.byId[mapId].dimensions![0].currentValue;
    };

    // click forward should move time to 13:00
    user.click(screen.getByRole('button', { name: 'forward' }));
    jest.runOnlyPendingTimers();
    await waitFor(() => {
      expect(getMapTimeDimensionCurrentValue()).toEqual(`${date}13:00:00Z`);
    });

    // click forward again should not move time because there is no image for 14:00
    user.click(screen.getByRole('button', { name: 'forward' }));
    jest.runOnlyPendingTimers();
    expect(getMapTimeDimensionCurrentValue()).toEqual(`${date}13:00:00Z`);

    // click backward should move time to 12:00
    user.click(screen.getByRole('button', { name: 'backward' }));
    jest.runOnlyPendingTimers();
    await waitFor(() => {
      expect(getMapTimeDimensionCurrentValue()).toEqual(`${date}12:00:00Z`);
    });

    // click backward again should not move time because there is no image for 11:00
    user.click(screen.getByRole('button', { name: 'backward' }));
    jest.runOnlyPendingTimers();
    expect(getMapTimeDimensionCurrentValue()).toEqual(`${date}12:00:00Z`);

    // click forward should move time to 13:00
    user.click(screen.getByRole('button', { name: 'forward' }));
    jest.runOnlyPendingTimers();
    await waitFor(() => {
      expect(getMapTimeDimensionCurrentValue()).toEqual(`${date}13:00:00Z`);
    });

    // open animation options menu to show now button
    expect(
      screen.queryByRole('button', { name: 'now' }),
    ).not.toBeInTheDocument();
    user.click(screen.getByRole('button', { name: 'animation options' }));

    // click now button should move time to closest timestep to now
    // and change center time to same value
    user.click(await screen.findByRole('button', { name: 'now' }));
    const timeStepClosestToNow = `${date}12:00:00Z`;
    await waitFor(() => {
      expect(getMapTimeDimensionCurrentValue()).toEqual(timeStepClosestToNow);
    });
    expect(store.getState().webmap.byId[mapId].timeSliderCenterTime).toEqual(
      dateUtils.unix(new Date(timeStepClosestToNow)),
    );

    jest.useRealTimers();
  });
});
