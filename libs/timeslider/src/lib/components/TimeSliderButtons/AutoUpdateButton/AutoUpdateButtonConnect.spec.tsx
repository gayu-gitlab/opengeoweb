/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { render, fireEvent, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { mapActions, mapEnums, storeTestUtils } from '@opengeoweb/store';
import { AutoUpdateButtonConnect } from './AutoUpdateButtonConnect';

import { ThemeStoreProvider } from '../../../testUtils/Providers';

describe('src/components/TimeSlider/TimeSliderButtons/AutoUpdateButton/AutoUpdateButtonConnect.tsx', () => {
  const mapId = 'mapid_1';
  const props = {
    mapId,
  };
  it('should render inactive auto update button by default', async () => {
    const mockState = storeTestUtils.mockStateMapWithTimeStepWithoutLayers(
      mapId,
      5,
    );
    const store = createMockStoreWithEggs(mockState);

    render(
      <ThemeStoreProvider store={store}>
        <AutoUpdateButtonConnect {...props} />
      </ThemeStoreProvider>,
    );
    const autoUpdateButton = screen.getByRole('button');
    expect(autoUpdateButton).toBeTruthy();
    expect(screen.getByTestId('autoUpdateButton').textContent).toContain(
      'AUTO',
    );
    const button = screen.getByTestId('autoUpdateButton');
    expect(button.classList.contains('Mui-disabled')).toBeFalsy();

    fireEvent.focus(screen.getByTestId('autoUpdateButton'));
    expect(await screen.findByText('Auto update off')).toBeTruthy();
  });

  it('autoupdate button is clicked, the autoupdate state should change in the store', async () => {
    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            shouldAutoUpdate: false,
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <ThemeStoreProvider store={store}>
        <AutoUpdateButtonConnect {...props} />
      </ThemeStoreProvider>,
    );
    const button = screen.getByRole('button');
    const expectedAction1 = [
      mapActions.toggleAutoUpdate({
        mapId: 'mapid_1',
        shouldAutoUpdate: true,
        origin: mapEnums.MapActionOrigin.map,
      }),
    ];
    await fireEvent.click(button);
    expect(store.getActions()).toEqual(expectedAction1);
  });
});
