/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Clock } from '@opengeoweb/theme';
import { mapConstants } from '@opengeoweb/store';
import { Mark, TimeSliderMenu } from '../TimeSliderMenu/TimeSliderMenu';
import {
  getNewCenterOfFixedPointZoom,
  secondsPerPxFromCanvasWidth,
} from '../../TimeSlider/timeSliderUtils';

const marks: Mark[] = [
  {
    label: '1 h',
    value: 3600,
    text: '1h',
  },
  {
    label: '3 h',
    value: 3 * 3600,
    text: '3h',
  },
  {
    label: '6 h',
    value: 6 * 3600,
    text: '6h',
  },
  {
    label: '12 h',
    value: 12 * 3600,
    text: '12h',
  },
  {
    label: '24 h',
    value: 24 * 3600,
    text: '24h',
  },
  {
    label: '48 h',
    value: 2 * 24 * 3600,
    text: '48h',
  },
  {
    label: 'week',
    value: 7 * 24 * 3600,
    text: 'w',
  },
  {
    label: '2 weeks',
    value: 2 * 7 * 24 * 3600,
    text: '2w',
  },
  {
    label: 'month',
    value: 30 * 24 * 3600,
    text: 'm',
  },
  {
    label: '3 months',
    value: 3 * 30 * 24 * 3600,
    text: '3m',
  },
  {
    label: 'year',
    value: 365 * 24 * 3600,
    text: 'y',
  },
].sort((a, b) => b.value - a.value);

interface TimeSpanButtonProps {
  timeSliderSpan: number;
  disabled?: boolean;
  centerTime: number;
  secondsPerPx: number;
  selectedTime: number;
  timeSliderWidth: number;
  onChangeTimeSliderSpan: (
    spanInSeconds: number,
    secondsPerPx: number,
    centerTime: number,
  ) => void;
  isTimeSpanAuto: boolean;
  onToggleTimeSpanAuto?: () => void;
  isOpenByDefault?: boolean;
  timeStepFromLayer?: number;
}

export const TimeSpanButton = ({
  timeSliderSpan = mapConstants.defaultTimeSpan,
  disabled = false,
  centerTime,
  secondsPerPx,
  selectedTime,
  timeSliderWidth,
  onChangeTimeSliderSpan,
  isTimeSpanAuto,
  onToggleTimeSpanAuto = (): void => {},
  isOpenByDefault = false,
  timeStepFromLayer,
}: TimeSpanButtonProps): JSX.Element => {
  const handleMenuItemClick = (mark: Mark): void => {
    if (isTimeSpanAuto) {
      onToggleTimeSpanAuto();
    }
    onChangeSliderValue(mark);
  };

  const handleToggleTimeSpan = (): void => {
    if (isTimeSpanAuto) {
      return;
    }
    onToggleTimeSpanAuto();
  };

  const onChangeSliderValue = (mark: Mark): void => {
    const newSliderValue = mark.value;
    const newValue = newSliderValue ? Math.round(newSliderValue) : 0;
    const spanInSeconds = marks.find((el) => el.value === newValue)!.value;

    const newSecondsPerPx = secondsPerPxFromCanvasWidth(
      timeSliderWidth,
      spanInSeconds,
    );

    const newCenterTime = getNewCenterOfFixedPointZoom(
      selectedTime,
      secondsPerPx,
      newSecondsPerPx!,
      centerTime,
    );
    if (
      spanInSeconds !== undefined &&
      newCenterTime !== undefined &&
      newSecondsPerPx !== undefined
    ) {
      onChangeTimeSliderSpan(spanInSeconds, newCenterTime, newSecondsPerPx);
    }
  };

  const layerHasTimeStep = Boolean(timeStepFromLayer);

  return (
    <TimeSliderMenu
      handleMenuItemClick={handleMenuItemClick}
      icon={<Clock />}
      marks={marks}
      title="Time span"
      value={timeSliderSpan}
      onChangeMouseWheel={onChangeSliderValue}
      allOptionsDisabled={disabled}
      isOpenByDefault={isOpenByDefault}
      isAutoSelected={isTimeSpanAuto}
      isAutoDisabled={!layerHasTimeStep}
      handleAutoClick={handleToggleTimeSpan}
    />
  );
};
