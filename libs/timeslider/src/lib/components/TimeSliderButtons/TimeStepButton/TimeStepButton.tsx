/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { FastForward } from '@opengeoweb/theme';
import { mapEnums } from '@opengeoweb/store';
import { renderCounter } from '@opengeoweb/shared';
import { Mark, TimeSliderMenu } from '../TimeSliderMenu/TimeSliderMenu';

const marks: Mark[] = [
  {
    label: '1 min',
    value: 1,
    text: '1m',
  },
  {
    label: '2 min',
    value: 2,
    text: '2m',
  },
  {
    label: '5 min',
    value: 5,
    text: '5m',
  },
  {
    label: '10 min',
    value: 10,
    text: '10m',
  },
  {
    label: '15 min',
    value: 15,
    text: '15m',
  },
  {
    label: '30 min',
    value: 30,
    text: '30m',
  },
  {
    label: '1 h',
    value: 60,
    text: '1h',
  },
  {
    label: '2 h',
    value: 120,
    text: '2h',
  },
  {
    label: '3 h',
    value: 180,
    text: '3h',
  },
  {
    label: '6 h',
    value: 360,
    text: '6h',
  },
  {
    label: '12 h',
    value: 720,
    text: '12h',
  },
  {
    label: '24 h',
    value: 1440,
    text: '24h',
  },
  {
    label: '48 h',
    value: 2880,
    text: '48h',
  },
  {
    label: '72 h',
    value: 4320,
    text: '72h',
  },
  {
    label: '1 week',
    value: 10080,
    text: '1w',
  },
  {
    label: '2 weeks',
    value: 20160,
    text: '2w',
  },
  {
    label: '1 month',
    value: 43800,
    text: '1mo',
  },
  {
    label: '2 months',
    value: 87600,
    text: '2mos',
  },
  {
    label: '6 months',
    value: 262800,
    text: '6mos',
  },
  {
    label: '1 year',
    value: 525600,
    text: '1y',
  },
  {
    label: '10 years',
    value: 5256000,
    text: '10y',
  },
].sort((a, b) => b.value - a.value);

export const timeStepTitle = 'Time step';
export interface TimeStepButtonProps {
  timeStep?: number;
  disabled?: boolean;
  isTimestepAuto: boolean;
  onChangeTimeStep: (step: number, origin?: mapEnums.MapActionOrigin) => void;
  onToggleTimestepAuto?: () => void;
  isOpenByDefault?: boolean;
  timeStepFromLayer?: number;
}

export const TimeStepButton: React.FC<TimeStepButtonProps> = ({
  timeStep = marks[0].value,
  disabled = false,
  isTimestepAuto,
  onChangeTimeStep,
  onToggleTimestepAuto = (): void => {},
  isOpenByDefault = false,
  timeStepFromLayer,
}: TimeStepButtonProps) => {
  const handleMenuItemClick = (mark: Mark): void => {
    if (isTimestepAuto) {
      onToggleTimestepAuto();
    }
    onChangeTimeStep(mark.value, mapEnums.MapActionOrigin.map);
  };

  const handleToggleTimeStep = (): void => {
    if (isTimestepAuto) {
      return;
    }
    onToggleTimestepAuto();
    timeStepFromLayer &&
      onChangeTimeStep(timeStepFromLayer, mapEnums.MapActionOrigin.map);
  };

  const layerHasTimeStep = Boolean(timeStepFromLayer);
  renderCounter.count(TimeStepButton.name);
  return (
    <TimeSliderMenu
      title={timeStepTitle}
      isOpenByDefault={isOpenByDefault}
      marks={marks}
      value={timeStep}
      handleMenuItemClick={handleMenuItemClick}
      allOptionsDisabled={disabled}
      isAutoSelected={isTimestepAuto}
      isAutoDisabled={!layerHasTimeStep}
      handleAutoClick={handleToggleTimeStep}
      icon={<FastForward />}
      onChangeMouseWheel={handleMenuItemClick}
      displayVariableDuration
    />
  );
};
