/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { darkTheme } from '@opengeoweb/theme';
import React from 'react';

import { TimeStepButton } from './TimeStepButton';
import { ThemeProvider } from '../../../testUtils/Providers';

export default {
  title: 'components/TimeSlider/TimeSliderButtons/TimeStepButtonDemo',
};

const TimeStepButtonDemo = (): React.ReactElement => {
  return (
    <div
      style={{
        width: '150px',
        height: '1200px',
        position: 'absolute',
      }}
    >
      <div
        style={{
          position: 'absolute',
          bottom: '16px',
          left: '16px',
        }}
      >
        <TimeStepButton
          onChangeTimeStep={(): void => {}}
          timeStep={5}
          isOpenByDefault
          isTimestepAuto={false}
        />
      </div>
    </div>
  );
};

export const TimeStepButtonDemoLight = (): JSX.Element => (
  <ThemeProvider>
    <TimeStepButtonDemo />
  </ThemeProvider>
);

TimeStepButtonDemoLight.storyName = 'TimeStepButtonDemoLight (takeSnapshot)';

TimeStepButtonDemoLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/609aac2d83b125bd6e2040b7/version/62c841daa73da317154f7aea',
    },
  ],
};

export const TimeStepButtonDemoDark = (): JSX.Element => (
  <ThemeProvider theme={darkTheme}>
    <TimeStepButtonDemo />
  </ThemeProvider>
);

TimeStepButtonDemoDark.storyName = 'TimeStepButtonDemoDark (takeSnapshot)';

TimeStepButtonDemoDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/5eea2a91efab124620a1c354/version/62c841ee31ced8193a45c613',
    },
  ],
};
