/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  fireEvent,
  render,
  screen,
  waitForElementToBeRemoved,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mapEnums } from '@opengeoweb/store';
import {
  TimeStepButton,
  TimeStepButtonProps,
  timeStepTitle,
} from './TimeStepButton';
import { ThemeProvider } from '../../../testUtils/Providers';

describe('src/components/TimeSlider/TimeStepButton', () => {
  const props: TimeStepButtonProps = {
    timeStep: 5,
    onChangeTimeStep: jest.fn(),
    isTimestepAuto: false,
  };
  const user = userEvent.setup();

  it('should render the component with text', () => {
    render(
      <ThemeProvider>
        <TimeStepButton {...props} />
      </ThemeProvider>,
    );
    expect(screen.getByText('5m')).toBeInTheDocument();
  });

  it('should show and hide tooltip', async () => {
    render(
      <ThemeProvider>
        <TimeStepButton {...props} />
      </ThemeProvider>,
    );

    const button = screen.getByRole('button', { name: /time step/i });
    expect(button).toBeInTheDocument();

    expect(screen.queryByRole('tooltip')).not.toBeInTheDocument();
    expect(screen.queryByText(timeStepTitle)).not.toBeInTheDocument();

    await user.hover(button);
    expect(await screen.findByRole('tooltip')).toBeInTheDocument();
    expect(screen.getByText(timeStepTitle)).toBeInTheDocument();

    await user.unhover(button);
    await waitForElementToBeRemoved(() => screen.queryByRole('tooltip'));
    expect(screen.queryByText(timeStepTitle)).not.toBeInTheDocument();
  });

  it('should toggle a menu on click', async () => {
    render(
      <ThemeProvider>
        <TimeStepButton {...props} />
      </ThemeProvider>,
    );

    expect(screen.queryByRole('menu')).not.toBeInTheDocument();

    const button = screen.getByRole('button', { name: /time step/i });
    await user.click(button);
    expect(screen.getByRole('menu')).toBeInTheDocument();

    await user.keyboard('{Escape}');
    expect(screen.queryByRole('menu')).not.toBeInTheDocument();
  });

  it('should render the slider correctly with 24 marks', async () => {
    render(
      <ThemeProvider>
        <TimeStepButton {...props} />
      </ThemeProvider>,
    );

    const button = screen.getByRole('button', { name: /time step/i });
    expect(button).toHaveTextContent('5m');

    await user.click(button);
    expect(screen.getAllByRole('menuitem')).toHaveLength(24);
  });

  it('should render a menu as enabled', async () => {
    render(
      <ThemeProvider>
        <TimeStepButton {...props} />
      </ThemeProvider>,
    );

    const button = screen.getByRole('button', { name: /time step/i });
    expect(button).toBeEnabled();

    await user.click(button);
    expect(screen.getByRole('menu')).toBeInTheDocument();
    expect(
      screen.getByRole('menuitem', { name: /30 min/i }),
    ).not.toHaveAttribute('aria-disabled');
  });

  it('should render a menu as disabled if passed in as props', async () => {
    const props2: TimeStepButtonProps = { disabled: true, ...props };
    render(
      <ThemeProvider>
        <TimeStepButton {...props2} />
      </ThemeProvider>,
    );

    const button = screen.getByRole('button', { name: /time step/i });
    expect(button).toBeEnabled();

    await user.click(button);
    expect(screen.getByRole('menu')).toBeInTheDocument();
    expect(screen.getByRole('menuitem', { name: /30 min/i })).toHaveAttribute(
      'aria-disabled',
    );
  });

  it('TimeStepButton menu can be opened and handleChange is called correctly', async () => {
    render(
      <ThemeProvider>
        <TimeStepButton {...props} />
      </ThemeProvider>,
    );

    const button = screen.getByRole('button', { name: /time step/i });

    await user.click(button);
    expect(screen.getByRole('menu')).toBeInTheDocument();

    await user.click(screen.getByRole('menuitem', { name: /10 years/i }));
    expect(props.onChangeTimeStep).toHaveBeenCalledTimes(1);
    expect(props.onChangeTimeStep).toHaveBeenCalledWith(
      5256000,
      mapEnums.MapActionOrigin.map,
    );

    await user.click(button);

    await user.click(screen.getByRole('menuitem', { name: /1 min/i }));
    expect(props.onChangeTimeStep).toHaveBeenCalledTimes(2);
    expect(props.onChangeTimeStep).toHaveBeenLastCalledWith(
      1,
      mapEnums.MapActionOrigin.map,
    );
  });

  it('TimeStepButton menu can be correctly called with other timeStep than 5', async () => {
    const props2: TimeStepButtonProps = {
      timeStep: 2880,
      onChangeTimeStep: jest.fn(),
      isTimestepAuto: false,
    };
    render(
      <ThemeProvider>
        <TimeStepButton {...props2} />
      </ThemeProvider>,
    );

    const button = screen.getByRole('button', { name: /time step/i });
    expect(button).toHaveTextContent('2d');

    await user.click(button);
    expect(screen.getByRole('menu')).toBeInTheDocument();

    expect(screen.getByRole('menuitem', { name: /48 h/i })).toHaveClass(
      'Mui-selected',
    );
  });

  it('TimeStepButton value can be scrolled with mouse', async () => {
    jest.useFakeTimers();

    render(
      <ThemeProvider>
        <TimeStepButton {...props} />
      </ThemeProvider>,
    );

    const button = screen.getByRole('button', { name: /time step/i });

    fireEvent.wheel(button, { deltaY: 1 });
    expect(props.onChangeTimeStep).toHaveBeenCalledTimes(1);
    expect(props.onChangeTimeStep).toHaveBeenCalledWith(
      2,
      mapEnums.MapActionOrigin.map,
    );

    // wait for throttle time
    jest.runOnlyPendingTimers();

    fireEvent.wheel(button, { deltaY: -1 });
    expect(props.onChangeTimeStep).toHaveBeenCalledTimes(2);
    expect(props.onChangeTimeStep).toHaveBeenCalledWith(
      10,
      mapEnums.MapActionOrigin.map,
    );
    jest.useRealTimers();
  });
});
