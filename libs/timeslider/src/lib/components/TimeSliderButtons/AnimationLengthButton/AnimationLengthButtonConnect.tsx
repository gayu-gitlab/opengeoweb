/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';

import {
  CoreAppStore,
  mapActions,
  mapEnums,
  mapSelectors,
} from '@opengeoweb/store';
import { AnimationLengthButton } from './AnimationLengthButton';

export interface AnimationLengthButtonConnectProps {
  mapId: string;
}

export const AnimationLengthButtonConnect: React.FC<
  AnimationLengthButtonConnectProps
> = ({ mapId }: AnimationLengthButtonConnectProps) => {
  const dispatch = useDispatch();

  const isAnimating = useSelector((store: CoreAppStore) =>
    mapSelectors.isAnimating(store, mapId),
  );

  const animationStartTime = useSelector((store: CoreAppStore) =>
    mapSelectors.getAnimationStartTime(store, mapId),
  );

  const animationEndTime = useSelector((store: CoreAppStore) =>
    mapSelectors.getAnimationEndTime(store, mapId),
  );

  const currentDiffInMinutes = moment(animationEndTime).diff(
    moment(animationStartTime),
    'minutes',
  );

  const handlechangeAnimationLength = (length: number): void => {
    const animationEndTime = moment(animationStartTime)
      .add(length, 'minutes')
      .toISOString();
    dispatch(
      mapActions.setAnimationEndTime({
        mapId,
        animationEndTime,
        origin: mapEnums.MapActionOrigin.map,
      }),
    );
  };

  return (
    <AnimationLengthButton
      disabled={isAnimating}
      animationLength={
        currentDiffInMinutes || Number(mapEnums.AnimationLength.Hours24)
      }
      onChangeAnimationLength={handlechangeAnimationLength}
    />
  );
};
