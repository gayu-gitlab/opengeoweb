/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import {
  WebMapStateModuleState,
  mapActions,
  mapEnums,
  storeTestUtils,
} from '@opengeoweb/store';
import { webmapTestSettings } from '@opengeoweb/webmap';
import { AnimationLengthButtonConnect } from './AnimationLengthButtonConnect';
import { ThemeProvider } from '../../../testUtils/Providers';

export const mockStateMapWithAnimationStartWithoutLayers = (
  mapId: string,
  animationStartTime: string,
): WebMapStateModuleState => {
  const webmap = storeTestUtils.createWebmapState(mapId);
  webmap.byId[mapId].animationStartTime = animationStartTime;

  return {
    webmap,
  };
};

describe('src/components/TimeSlider/TimeSliderButtons/AnimationLengthButton/AnimationLengthButtonConnect', () => {
  const mapId = 'mapid_1';
  const props = {
    mapId,
  };
  const user = userEvent.setup();

  it('should render an enabled animation length menu and show default 6h selection', () => {
    const mockStore = configureStore();
    const mockState = storeTestUtils.mockStateMapWithMultipleLayers(
      [webmapTestSettings.multiDimensionLayer3],
      mapId,
    );
    const store = mockStore(mockState);

    render(
      <ThemeProvider>
        <Provider store={store}>
          <AnimationLengthButtonConnect {...props} />
        </Provider>
      </ThemeProvider>,
    );

    const button = screen.getByRole('button', { name: /animation/i });

    expect(button).toBeInTheDocument();

    expect(button).not.toHaveClass('Mui-disabled');
  });

  it('Animation menu renders, when clicked open and should change animation length in store', async () => {
    const startAnimationTime = '2000-01-01T00:00:00.000Z';
    const mockStore = configureStore();
    const mockState = mockStateMapWithAnimationStartWithoutLayers(
      mapId,
      startAnimationTime,
    );
    const store = mockStore(mockState);
    render(
      <ThemeProvider>
        <Provider store={store}>
          <AnimationLengthButtonConnect {...props} />
        </Provider>
      </ThemeProvider>,
    );

    const animationButton = screen.getByRole('button', { name: /animation/i });
    await user.click(animationButton);
    expect(screen.getByRole('menu')).toBeInTheDocument();

    await user.click(screen.getByRole('menuitem', { name: '2 h' }));

    const expectedAction = [
      mapActions.setAnimationEndTime({
        mapId,
        animationEndTime: '2000-01-01T02:00:00.000Z',
        origin: mapEnums.MapActionOrigin.map,
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
