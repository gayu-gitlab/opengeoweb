/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { fireEvent, render, screen } from '@testing-library/react';
import moment from 'moment';
import React from 'react';

import {
  TimeSliderCurrentTimeBox,
  TimeSliderCurrentTimeBoxProps,
} from './TimeSliderCurrentTimeBox';
import { ThemeProvider } from '../../testUtils/Providers';

describe('src/components/TimeSlider/TimeSliderCurrentTime', () => {
  const date = moment.utc('20200101', 'YYYYMMDD');
  const props: TimeSliderCurrentTimeBoxProps = {
    secondsPerPx: 50,
    centerTime: moment.utc(date).startOf('day').hours(12).unix(),
    selectedTime: moment.utc(date).startOf('day').hours(6).unix(),
    unfilteredSelectedTime: moment.utc(date).startOf('day').hours(6).unix(),
    setUnfilteredSelectedTime: jest.fn(),
  };
  it('should render the canvas', () => {
    render(
      <ThemeProvider>
        <TimeSliderCurrentTimeBox {...props} />
      </ThemeProvider>,
    );
    expect(screen.getByRole('presentation')).toBeTruthy();
  });

  it('should move the slider when CurrentTimeBox is out of view', async () => {
    const date = moment.utc('20200101', 'YYYYMMDD');
    const onSetCenterTimeFn = jest.fn();
    const props: TimeSliderCurrentTimeBoxProps = {
      secondsPerPx: 50,
      centerTime: moment.utc(date).startOf('day').hours(12).unix(),
      selectedTime: moment.utc(date).startOf('day').hours(12).unix(),
      unfilteredSelectedTime: moment.utc(date).startOf('day').hours(12).unix(),
      setUnfilteredSelectedTime: jest.fn(),
      onSetCenterTime: onSetCenterTimeFn,
    };
    const { rerender } = render(
      <ThemeProvider>
        <TimeSliderCurrentTimeBox {...props} />
      </ThemeProvider>,
    );

    // make canvasComponent return a width of 1000
    const canvasContainerDiv = screen.getByRole('button')
      .firstChild as HTMLElement;
    jest.spyOn(canvasContainerDiv, 'clientWidth', 'get').mockReturnValue(1000);
    fireEvent(window, new Event('resize'));

    expect(onSetCenterTimeFn).not.toHaveBeenCalled();
    const changedProps: TimeSliderCurrentTimeBoxProps = {
      ...props,
      selectedTime: moment.utc(date).subtract(1, 'week').unix(),
      unfilteredSelectedTime: moment.utc(date).subtract(1, 'week').unix(),
    };
    rerender(
      <ThemeProvider>
        <TimeSliderCurrentTimeBox {...changedProps} />
      </ThemeProvider>,
    );
    expect(onSetCenterTimeFn).toHaveBeenCalled();
  });
});
