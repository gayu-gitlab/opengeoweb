/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { uiTypes, useSetupDialog } from '@opengeoweb/store';
import { useSelector } from 'react-redux';
import { TimeSeriesModuleState } from '../../../store/types';
import { timeSeriesSelectors } from '../../../store';
import { ParameterInfoDialog } from './ParameterInfoDialog';

export const ParameterInfoDialogConnect: React.FC = () => {
  const currentParameterIdDisplayed = useSelector(
    (store: TimeSeriesModuleState) =>
      timeSeriesSelectors.getCurrentParameterInfoDisplayed(store),
  );

  const currentParameterInfoDisplayed = useSelector(
    (store: TimeSeriesModuleState) =>
      timeSeriesSelectors.getParameterById(store, currentParameterIdDisplayed),
  );

  const relatedService = useSelector((store: TimeSeriesModuleState) =>
    timeSeriesSelectors.getService(
      store,
      currentParameterInfoDisplayed?.serviceId || '',
    ),
  );

  const { dialogOrder, onCloseDialog, setDialogOrder, uiSource, isDialogOpen } =
    useSetupDialog(uiTypes.DialogTypes.TimeseriesInfo);
  if (!currentParameterInfoDisplayed) {
    return null;
  }
  return (
    <ParameterInfoDialog
      isOpen={isDialogOpen}
      onClose={onCloseDialog}
      onMouseDown={setDialogOrder}
      order={dialogOrder}
      source={uiSource}
      currentParameterInfo={currentParameterInfoDisplayed}
      relatedService={relatedService}
    />
  );
};
