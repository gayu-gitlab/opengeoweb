/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { TimeSeriesService, ToolContainerDraggable } from '@opengeoweb/shared';
import Box from '@mui/material/Box';
import { uiTypes } from '@opengeoweb/store';
import { Typography } from '@mui/material';
import { Parameter } from '../../TimeSeries/types';

export interface ParameterInfoDialogProps {
  currentParameterInfo: Parameter;
  relatedService: TimeSeriesService | undefined;
  onClose: () => void;
  isOpen: boolean;
  onMouseDown?: () => void;
  order?: number;
  source?: uiTypes.Source;
  dialogHeight?: number;
}
export const ParameterInfoDialog: React.FC<ParameterInfoDialogProps> = ({
  onClose,
  currentParameterInfo,
  relatedService,
  isOpen,
  onMouseDown = (): void => {},
  order = 0,
  source = 'app',
  dialogHeight,
}: ParameterInfoDialogProps) => {
  return (
    <ToolContainerDraggable
      onClose={onClose}
      startSize={{ width: 288, height: dialogHeight || 650 }}
      startPosition={{ top: 150, left: 790 }}
      title="TS Info"
      isOpen={isOpen}
      onMouseDown={onMouseDown}
      order={order}
      source={source}
    >
      <Box sx={{ padding: 2 }}>
        <ParameterInfoText
          label="Name"
          value={currentParameterInfo.propertyName}
        />
        <ParameterInfoText
          label="Service"
          value={relatedService?.name || currentParameterInfo.serviceId}
        />
        {relatedService && (
          <>
            <ParameterInfoText label="Service URL" value={relatedService.url} />
            <ParameterInfoText
              label="Service description"
              value={relatedService.description}
            />
            <ParameterInfoText
              label="Service type"
              value={relatedService.type}
            />
          </>
        )}
        <ParameterInfoText
          label="Collection"
          value={currentParameterInfo.collectionId}
        />
      </Box>
    </ToolContainerDraggable>
  );
};

export interface ParameterInfoTextProps {
  label: string;
  value: string;
}

const textStyle = {
  lineHeight: '20px',
  letterSpacing: '0.25px',
};

const textStyleWithMargin = {
  ...textStyle,
  marginBottom: 1,
};

const ParameterInfoText: React.FC<ParameterInfoTextProps> = ({
  label,
  value,
}: ParameterInfoTextProps) => {
  return (
    <>
      <Typography variant="subtitle2" sx={textStyle}>
        {label}
      </Typography>
      <Typography variant="body2" sx={textStyleWithMargin}>
        {value}
      </Typography>
    </>
  );
};
