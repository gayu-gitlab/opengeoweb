/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { ThemeWrapper, darkTheme, lightTheme } from '@opengeoweb/theme';
import { TimeSeriesService } from '@opengeoweb/shared';
import { ParameterInfoDialog } from './ParameterInfoDialog';
import { Parameter } from '../../TimeSeries/types';

export default { title: 'components/ParameterInfoDialog' };

const parameter1: Parameter = {
  id: 'parameter1',
  plotId: 'Plot1',
  unit: '°C',
  propertyName: 'Temperature',
  plotType: 'line',
  serviceId: 'SomeServiceObs',
  collectionId: 'observations',
  opacity: 70,
};

const service: TimeSeriesService = {
  url: 'someService.com/EDR',
  id: 'SomeServiceObs',
  description: 'Observations service',
  type: 'EDR',
  name: 'Service Name',
  scope: 'system',
};

export const ParameterInfoDialogDemoLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <ParameterInfoDialog
      currentParameterInfo={parameter1}
      onClose={(): void => {}}
      isOpen
      onMouseDown={(): void => {}}
      relatedService={service}
    />
  </ThemeWrapper>
);

export const ParameterInfoDialogDemoDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <ParameterInfoDialog
      currentParameterInfo={parameter1}
      onClose={(): void => {}}
      isOpen
      onMouseDown={(): void => {}}
      relatedService={service}
    />
  </ThemeWrapper>
);

ParameterInfoDialogDemoLight.storyName =
  'TimeSeries ParameterInfoDialog light theme (takeSnapshot)';

ParameterInfoDialogDemoDark.storyName =
  'TimeSeries ParameterInfoDialog dark theme (takeSnapshot)';
