/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React, { FC, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useSetupDialog, uiTypes } from '@opengeoweb/store';
import { snackbarActions } from '@opengeoweb/snackbar';
import { TimeSeriesService, useConfirmationDialog } from '@opengeoweb/shared';
import { Parameter, PlotPreset } from '../TimeSeries/types';
import { CustomSortableEvent, TimeSeriesManager } from './TimeSeriesManager';
import { getPlotState } from '../../store/selectors';
import { TimeSeriesSelectConnect } from '../TimeSeriesSelect/TimeSeriesSelectConnect';
import { timeSeriesActions } from '../../store';
import { ParameterInfoDialogConnect } from './ParameterInfo/ParameterInfoDialogConnect';
import { MoveParameterPayload, UpdateTitlePayload } from '../../store/types';

export const DIALOG_TYPE_MANAGER = uiTypes.DialogTypes.TimeSeriesManager;

interface TimeSeriesManagerProps {
  preloadedTimeseriesServices?: TimeSeriesService[];
}
export const TimeSeriesManagerConnect: FC<TimeSeriesManagerProps> = ({
  preloadedTimeseriesServices,
}) => {
  const { setDialogOrder, dialogOrder, isDialogOpen, onCloseDialog } =
    useSetupDialog(DIALOG_TYPE_MANAGER);

  const [selectPlotId, setSelectPlotId] = useState('');
  const dispatch = useDispatch();
  const confirmDialog = useConfirmationDialog();

  const plotState: PlotPreset | undefined = useSelector(getPlotState);

  React.useEffect(() => {
    const addServices = (timeSeriesServices: TimeSeriesService[]): void => {
      dispatch(timeSeriesActions.addServices({ timeSeriesServices }));
    };
    if (preloadedTimeseriesServices) {
      addServices(preloadedTimeseriesServices);
    }
  }, [preloadedTimeseriesServices, dispatch]);

  const showSnackbar = React.useCallback(
    (message: string) => {
      dispatch(snackbarActions.openSnackbar({ message }));
    },
    [dispatch],
  );

  const deletePlotSnackbar = React.useCallback(
    (id: string) => {
      const message = `Plot "${id}" has been deleted`;
      showSnackbar(message);
      dispatch(timeSeriesActions.deletePlot(id));
    },
    [dispatch, showSnackbar],
  );

  const deletePlot = (id: string): void => {
    confirmDialog({
      title: 'Delete Plot',
      description: `Delete the plot "${id}"`,
      confirmLabel: 'Delete',
      cancelLabel: 'Cancel',
    }).then(() => {
      deletePlotSnackbar(id);
    });
  };

  const addPlot = (title: string): void => {
    dispatch(timeSeriesActions.addPlot({ title }));
  };
  const deleteParameter = (id: string): void => {
    dispatch(timeSeriesActions.deleteParameter({ id }));
  };
  const togglePlot = (id: string): void => {
    dispatch(timeSeriesActions.togglePlot(id));
  };
  const toggleParameter = (id: string): void => {
    dispatch(timeSeriesActions.toggleParameter({ id }));
  };
  const updateParameter = (parameter: Parameter): void => {
    dispatch(timeSeriesActions.updateParameter({ parameter }));
  };
  const movePlot = ({ oldIndex, newIndex }: CustomSortableEvent): void => {
    dispatch(timeSeriesActions.movePlot({ oldIndex, newIndex }));
  };
  const moveParameter = ({
    oldIndex,
    newIndex,
    plotId,
    toPlotId,
  }: MoveParameterPayload): void => {
    dispatch(
      timeSeriesActions.moveParameter({ oldIndex, newIndex, plotId, toPlotId }),
    );
  };
  const updateTitle = (payload: UpdateTitlePayload): void => {
    dispatch(timeSeriesActions.updateTitle(payload));
  };
  const addParameter = (parameter: Parameter): void => {
    dispatch(timeSeriesActions.addParameter(parameter));
  };

  return (
    <>
      {plotState && (
        <TimeSeriesManager
          isOpen={isDialogOpen}
          onClose={onCloseDialog}
          onMouseDown={setDialogOrder}
          order={dialogOrder}
          plotState={plotState}
          addPlot={addPlot}
          deletePlot={deletePlot}
          togglePlot={togglePlot}
          addParameter={addParameter}
          deleteParameter={deleteParameter}
          toggleParameter={toggleParameter}
          setSelectPlotId={setSelectPlotId}
          updateParameter={updateParameter}
          movePlot={movePlot}
          moveParameter={moveParameter}
          updateTitle={updateTitle}
        />
      )}
      <TimeSeriesSelectConnect selectPlotId={selectPlotId} />
      <ParameterInfoDialogConnect />
    </>
  );
};
