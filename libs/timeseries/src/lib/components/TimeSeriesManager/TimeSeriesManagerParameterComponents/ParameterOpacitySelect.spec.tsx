/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render, screen } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import ParameterOpacitySelect from './ParameterOpacitySelect';
import { mockTimeSeriesServices } from '../../TimeSeries/mockTimeSeriesServices';
import { PlotType } from '../../TimeSeries/types';

describe('src/components/TimeSeriesManager/TimeSeriesManagerParamterComponents/ParamterOpacitySelect', () => {
  it('should render the component', () => {
    const parameter = {
      plotId: 'Plot_2',
      unit: 'mm',
      propertyName: 'Precipitation1h',
      plotType: 'bar' as PlotType,
      serviceId: mockTimeSeriesServices[0].id,
      collectionId: 'ecmwf',
      id: 'precipPlot2',
    };
    const updateParameter = jest.fn();

    render(
      <ThemeWrapper>
        <ParameterOpacitySelect
          rowIsEnabled={true}
          parameter={parameter}
          updateParameter={updateParameter}
        />
        ,
      </ThemeWrapper>,
    );

    expect(screen.getByTestId('parameterOpacitySelect')).toBeTruthy();
  });
});
