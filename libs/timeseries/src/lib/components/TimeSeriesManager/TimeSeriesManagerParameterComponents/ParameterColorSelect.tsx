/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { MenuItem, SelectChangeEvent, Theme } from '@mui/material';
import { TooltipSelect } from '@opengeoweb/shared';
import { COLOR_MAP, COLOR_NAME_TO_HEX_MAP } from '../../../constants';
import { Parameter } from '../../TimeSeries/types';

function getTextColor(bgColor?: string): string {
  if (!bgColor) {
    return '#fff';
  }
  return parseInt(bgColor.replace('#', ''), 16) > 0xffffff / 2
    ? '#000'
    : '#fff';
}

const ParameterColorSelect: React.FC<{
  useEntireWidth?: boolean;
  rowIsEnabled: boolean;
  parameter: Parameter;
  updateParameter: (parameter: Parameter) => void;
}> = ({ useEntireWidth, rowIsEnabled, parameter, updateParameter }) => {
  const parameterColor: string =
    parameter.color ??
    COLOR_MAP[parameter.propertyName as keyof typeof COLOR_MAP] ??
    '#000';
  const textColor = getTextColor(
    COLOR_NAME_TO_HEX_MAP[parameterColor as keyof typeof COLOR_NAME_TO_HEX_MAP],
  );

  return (
    <TooltipSelect
      data-testid="parameterColorSelect"
      className="tooltipSelectWidth"
      hasBackgroundColor={false}
      value={parameterColor}
      tooltip="Choose a color"
      isEnabled={rowIsEnabled}
      sx={{
        '& .MuiSelect-select.MuiInputBase-input.MuiInput-input': {
          backgroundColor:
            COLOR_NAME_TO_HEX_MAP[
              parameterColor as keyof typeof COLOR_NAME_TO_HEX_MAP
            ],
          color: textColor,
          backgroundClip: useEntireWidth ? 'unset' : 'content-box',
          width: useEntireWidth ? '100%' : '24px',
          height: '26px',
          textAlign: 'center',
        },
      }}
      onChange={(event: SelectChangeEvent): void => {
        updateParameter({ ...parameter, color: event.target.value });
      }}
    >
      <MenuItem disabled>Colour</MenuItem>
      {Object.keys(COLOR_NAME_TO_HEX_MAP).map((name) => {
        const textColor = getTextColor(
          COLOR_NAME_TO_HEX_MAP[name as keyof typeof COLOR_NAME_TO_HEX_MAP],
        );
        return (
          <MenuItem
            key={name}
            value={name}
            sx={{
              width: useEntireWidth ? '100%' : '100px',
              '&.MuiMenuItem-root ': {
                color: (theme: Theme): string =>
                  `${theme.palette.geowebColors.background.surface}`,
                background: `${
                  COLOR_NAME_TO_HEX_MAP[
                    name as keyof typeof COLOR_NAME_TO_HEX_MAP
                  ]
                }`,
                backgroundClip: 'content-box',
                padding: '12px 8px',
                boxShadow: 'inset -8px -12px, inset 8px 12px',
                '&>div': {
                  paddingLeft: '4px',
                },
                '&:hover': {
                  boxShadow: (theme: Theme): string =>
                    `inset -8px -12px ${theme.palette.geowebColors.cards.cardContainerMouseOver}, inset 8px 12px ${theme.palette.geowebColors.cards.cardContainerMouseOver}`,
                  backgroundColor: `${
                    COLOR_NAME_TO_HEX_MAP[
                      name as keyof typeof COLOR_NAME_TO_HEX_MAP
                    ]
                  } !important`,
                  backgroundClip: 'content-box',
                },
                '&.Mui-selected': {
                  backgroundColor: `${
                    COLOR_NAME_TO_HEX_MAP[
                      name as keyof typeof COLOR_NAME_TO_HEX_MAP
                    ]
                  } !important`,
                },
              },
            }}
          >
            <div style={{ paddingTop: '2px', color: `${textColor}` }}>
              {name}
            </div>
          </MenuItem>
        );
      })}
    </TooltipSelect>
  );
};

export default ParameterColorSelect;
