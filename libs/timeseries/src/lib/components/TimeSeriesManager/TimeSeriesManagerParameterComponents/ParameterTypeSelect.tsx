/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { MenuItem, SelectChangeEvent } from '@mui/material';
import { TooltipSelect } from '@opengeoweb/shared';
import { Parameter, PlotType } from '../../TimeSeries/types';

const ParameterTypeSelect: React.FC<{
  rowIsEnabled: boolean;
  parameter: Parameter;
  updateParameter: (parameter: Parameter) => void;
}> = ({ rowIsEnabled, parameter, updateParameter }) => {
  return (
    <TooltipSelect
      data-testid="parameterTypeSelect"
      value={parameter.plotType}
      tooltip="Choose a type"
      isEnabled={rowIsEnabled}
      onChange={(event: SelectChangeEvent): void => {
        updateParameter({
          ...parameter,
          plotType: event.target.value as PlotType,
        });
      }}
    >
      <MenuItem disabled>Type</MenuItem>
      <MenuItem value="line">Line</MenuItem>
      <MenuItem value="bar">Bar</MenuItem>
      <MenuItem value="area">Area</MenuItem>
      <MenuItem value="scatter">Scatter</MenuItem>
    </TooltipSelect>
  );
};

export default ParameterTypeSelect;
