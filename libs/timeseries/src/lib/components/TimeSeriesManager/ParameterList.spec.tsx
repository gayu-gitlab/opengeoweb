/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';

import { createToolkitMockStoreWithEggs } from '@opengeoweb/shared';
import { uiReducer, webmapReducer } from '@opengeoweb/store';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';
import { Plot } from '../TimeSeries/types';
import { ParameterList } from './ParameterList';
import { reducer } from '../../store/reducer';

const collectionId = 'ecmwf';
const mockPlot: Plot = {
  title: 'Plot 1',
  plotId: 'plot_1',
  parameters: [
    {
      id: '1',
      plotId: 'plot_1',
      unit: '°C',
      propertyName: 'Temperature',
      plotType: 'line',
      serviceId: 'fmi',
      collectionId,
      opacity: 70,
    },
    {
      id: '2',
      plotId: 'plot_2',
      unit: 'mm',
      propertyName: 'Precipitation1h',
      plotType: 'bar',
      serviceId: 'fmi',
      collectionId,
      opacity: 70,
    },
  ],
};

const mockDeleteParameter = jest.fn();
const mockToggleParameter = jest.fn();
const mockUpdateParameter = jest.fn();
const addUpdateParameter = jest.fn();
const mockMoveParameter = jest.fn();
const togglePlot = jest.fn();

describe('src/components/TimeSeriesManager/ParameterList', () => {
  it('should render without crashing', () => {
    const mockState = {
      reducer: { timeSeries: reducer, ui: uiReducer, webmap: webmapReducer },
      preloadedState: {},
    };
    const store = createToolkitMockStoreWithEggs(mockState);
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <ParameterList
          plot={mockPlot}
          plotIsEnabled={true}
          deleteParameter={mockDeleteParameter}
          toggleParameter={mockToggleParameter}
          updateParameter={mockUpdateParameter}
          addParameter={addUpdateParameter}
          moveParameter={mockMoveParameter}
          togglePlot={togglePlot}
        />
      </TimeSeriesThemeStoreProvider>,
    );
  });

  it('should render correct number of parameters', async () => {
    const mockState = {
      reducer: { timeSeries: reducer, ui: uiReducer, webmap: webmapReducer },
      preloadedState: {},
    };
    const store = createToolkitMockStoreWithEggs(mockState);
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <ParameterList
          plot={mockPlot}
          plotIsEnabled={true}
          deleteParameter={mockDeleteParameter}
          toggleParameter={mockToggleParameter}
          updateParameter={mockUpdateParameter}
          addParameter={addUpdateParameter}
          moveParameter={mockMoveParameter}
          togglePlot={togglePlot}
        />
      </TimeSeriesThemeStoreProvider>,
    );
    expect(await screen.findAllByTestId('enableButton')).toHaveLength(
      mockPlot.parameters!.length,
    );
  });

  it('should call deleteParameter when delete button is clicked', () => {
    const mockState = {
      reducer: { timeSeries: reducer, ui: uiReducer, webmap: webmapReducer },
      preloadedState: {},
    };
    const store = createToolkitMockStoreWithEggs(mockState);
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <ParameterList
          plot={mockPlot}
          plotIsEnabled={true}
          deleteParameter={mockDeleteParameter}
          toggleParameter={mockToggleParameter}
          updateParameter={mockUpdateParameter}
          addParameter={addUpdateParameter}
          moveParameter={mockMoveParameter}
          togglePlot={togglePlot}
        />
      </TimeSeriesThemeStoreProvider>,
    );
    fireEvent.click(screen.getAllByTestId('deleteButton')[0]);
    expect(mockDeleteParameter).toHaveBeenCalledWith('1');
  });

  it('should call toggleParameter when visibility button is clicked', () => {
    const mockState = {
      reducer: { timeSeries: reducer, ui: uiReducer, webmap: webmapReducer },
      preloadedState: {},
    };
    const store = createToolkitMockStoreWithEggs(mockState);
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <ParameterList
          plot={mockPlot}
          plotIsEnabled={true}
          deleteParameter={mockDeleteParameter}
          toggleParameter={mockToggleParameter}
          updateParameter={mockUpdateParameter}
          addParameter={addUpdateParameter}
          moveParameter={mockMoveParameter}
          togglePlot={togglePlot}
        />
      </TimeSeriesThemeStoreProvider>,
    );

    fireEvent.click(screen.getAllByTestId('enableButton')[0]);
    expect(mockToggleParameter).toHaveBeenCalledWith('1');
  });

  it('should show collectionId', () => {
    const mockState = {
      reducer: { timeSeries: reducer, ui: uiReducer, webmap: webmapReducer },
      preloadedState: {},
    };
    const store = createToolkitMockStoreWithEggs(mockState);
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <ParameterList
          plot={mockPlot}
          plotIsEnabled={true}
          addParameter={addUpdateParameter}
          deleteParameter={mockDeleteParameter}
          toggleParameter={mockToggleParameter}
          updateParameter={mockUpdateParameter}
          moveParameter={mockMoveParameter}
          togglePlot={togglePlot}
        />
      </TimeSeriesThemeStoreProvider>,
    );
    // There should be two plot lines with the same collectionId
    expect(screen.queryAllByText(collectionId).length).toBe(6);
  });
});
