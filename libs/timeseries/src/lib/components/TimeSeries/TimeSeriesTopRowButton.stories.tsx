/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import {
  Location,
  ThemeWrapper,
  darkTheme,
  lightTheme,
} from '@opengeoweb/theme';

import { TimeSeriesTopRowButton } from './TimeSeriesTopRowButton';

export default { title: 'components/TimeSeriesButtons' };

const testButton = (
  <TimeSeriesTopRowButton
    shortText="shortText"
    longText="longText"
    icon={<Location />}
  />
);

const TimeSeriesTopRowButtonDemo = (): React.ReactElement => (
  <div style={{ padding: '5vh' }}>
    <div
      style={{
        width: '500px',
        height: '30px',
        containerType: 'inline-size',
        containerName: 'time-series-container',
      }}
    >
      {testButton}
    </div>
    <div
      style={{
        width: '100px',
        height: '30px',
        containerType: 'inline-size',
        containerName: 'time-series-container',
      }}
    >
      {testButton}
    </div>
  </div>
);

export const TimeSeriesTopRowButtonDemoLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <TimeSeriesTopRowButtonDemo />
  </ThemeWrapper>
);

export const TimeSeriesTopRowButtonDemoDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <TimeSeriesTopRowButtonDemo />
  </ThemeWrapper>
);

TimeSeriesTopRowButtonDemoLight.storyName =
  'TimeSeries TopRowButtons light theme (takeSnapshot)';

TimeSeriesTopRowButtonDemoDark.storyName =
  'TimeSeries TopRowButtons dark theme (takeSnapshot)';
