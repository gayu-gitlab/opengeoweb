/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import React, { useEffect } from 'react';
import { TimeSeriesService, dateUtils } from '@opengeoweb/shared';
import { compact, groupBy, isEqual } from 'lodash';
import { Card } from '@mui/material';
import {
  ParameterWithData,
  PointerLocation,
  PlotPreset,
  PlotWithData,
  Plot,
} from './types';
import { TimeSeriesChart } from './TimeSeriesChart';
import { getOgcParameter } from './api';
import { getEdrParameter } from '../../utils/edrUtils';

interface TimeSeriesViewProps {
  plotPreset: PlotPreset;
  selectedLocation: PointerLocation;
  services: TimeSeriesService[];
}

export const TimeSeriesView: React.FC<TimeSeriesViewProps> = React.memo(
  ({ plotPreset, selectedLocation, services }: TimeSeriesViewProps) => {
    const plotsWithData = useGetPlotsWithData(
      plotPreset,
      selectedLocation,
      services,
    );
    return (
      <Card data-testid="TimeSeriesView">
        {plotsWithData && <TimeSeriesChart plotsWithData={plotsWithData} />}
      </Card>
    );
  },
  (previousProps, nextProps) => {
    const samePreset = previousProps.plotPreset === nextProps.plotPreset;
    const sameLocation = isEqual(
      previousProps.selectedLocation,
      nextProps.selectedLocation,
    );

    return samePreset && sameLocation;
  },
);

export const useGetPlotsWithData = (
  plotsWithoutData: PlotPreset,
  selectedLocation: PointerLocation,
  services: TimeSeriesService[],
): PlotWithData[] | undefined => {
  const [plotsWithData, setPlotsWithData] = React.useState<PlotWithData[]>();

  useEffect(() => {
    const setPlots = async (): Promise<void> => {
      const shownPlotsWithoutData =
        filterOutHiddenPlotsAndParameters(plotsWithoutData);

      if (!shownPlotsWithoutData) {
        setPlotsWithData(undefined);
      } else {
        const plotsWithData = await getPlotsWithData(
          shownPlotsWithoutData,
          selectedLocation,
          services,
        );
        setPlotsWithData(plotsWithData);
      }
    };
    const interval = window.setInterval(() => {
      setPlots();
    }, 60_000);

    setPlots();

    return () => {
      window.clearInterval(interval);
    };
  }, [plotsWithoutData, selectedLocation, services]);

  return plotsWithData;
};

const getPlotsWithData = async (
  plotsWithoutData: PlotPreset,
  selectedLocation: PointerLocation,
  services: TimeSeriesService[],
): Promise<PlotWithData[] | undefined> => {
  const parameters = await fetchParameters(
    plotsWithoutData,
    selectedLocation,
    services,
  );
  const plotsWithData = getPlotsWithParameters(
    plotsWithoutData.plots,
    parameters,
  );
  return plotsWithData;
};

const filterOutHiddenPlotsAndParameters = (
  plotPreset: PlotPreset,
): PlotPreset | undefined => {
  const shownPlots = plotPreset.plots.filter((plot) => plot.enabled !== false);
  if (shownPlots.length === 0) {
    return undefined;
  }
  const hiddenPlotsIds = plotPreset.plots
    .filter((plot) => {
      return plot.enabled === false;
    })
    .map((plot) => plot.plotId);

  const shownParameters = plotPreset.parameters.filter((parameter) => {
    const parameterIsInEnabledPlot = !hiddenPlotsIds.includes(parameter.plotId);
    const parameterIsEnabled = parameter.enabled !== false;
    return parameterIsEnabled && parameterIsInEnabledPlot;
  });

  return {
    ...plotPreset,
    plots: shownPlots,
    parameters: shownParameters,
  };
};

function getPlotsWithParameters(
  plots: Plot[],
  parameters: ParameterWithData[],
): PlotWithData[] {
  const parametersByPlotId = groupBy(
    parameters,
    (parameter) => parameter.plotId,
  );

  const plotsWithParameters = plots
    .map((plot) => {
      return { ...plot, parametersWithData: parametersByPlotId[plot.plotId] };
    })
    .filter((plot) => plot.parametersWithData?.length);
  return plotsWithParameters;
}

export const fetchParameters = async (
  plotsWithoutData: PlotPreset,
  selectedLocation: PointerLocation,
  services: TimeSeriesService[],
): Promise<ParameterWithData[]> => {
  const serviceIdToUrlMap: Record<string, TimeSeriesService> = services.reduce(
    (serviceMap, service) => ({
      ...serviceMap,
      [service.id]: service,
    }),
    {},
  );

  const parametersWithDataPromise = plotsWithoutData.parameters.map(
    (parameter) => {
      if (!serviceIdToUrlMap[parameter.serviceId]) {
        console.warn(
          'Parameter service id does not exist in',
          serviceIdToUrlMap,
          parameter,
        );
        return null;
      }

      const { url, type } = serviceIdToUrlMap[parameter.serviceId];

      if (type === 'OGC') {
        return getOgcParameter(
          parameter,
          url,
          selectedLocation.lon,
          selectedLocation.lat,
        );
      }
      return getEdrParameter(parameter, url, selectedLocation);
    },
  );

  const parametersWithData = await Promise.all(parametersWithDataPromise);
  const parametersWithNonNullData = compact(parametersWithData);
  return parametersWithNonNullData;
};

export function getUtcTime(time: string): Date {
  return dateUtils.utc(time);
}
