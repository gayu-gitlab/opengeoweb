/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import {
  DefaultLabelFormatterCallbackParams,
  EChartsOption,
  TooltipComponentOption,
  XAXisComponentOption,
  YAXisComponentOption,
  GridComponentOption,
  LineSeriesOption,
  BarSeriesOption,
  ScatterSeriesOption,
} from 'echarts';
import { groupBy, sortBy, uniq, uniqBy } from 'lodash';
import { dateUtils } from '@opengeoweb/shared';
import { COLOR_MAP, COLOR_NAME_TO_HEX_MAP } from '../../constants';
import { ParameterWithData, Plot, PlotWithData } from './types';

export const getYAxis = (plotCharts: PlotChart[]): YAXisComponentOption[] => {
  const yAxis = plotCharts.flatMap(
    (plotChart, plotIndex): YAXisComponentOption[] => {
      return Object.entries(plotChart.parametersByUnit).flatMap(
        ([unit, parameters], unitIndex): YAXisComponentOption => {
          const allValues = parameters
            .flatMap((parameter) => parameter.value)
            .filter((value) => !isNaN(value));
          const max = Math.max(...allValues);
          const min = Math.min(...allValues);
          const position = unitIndex % 2 === 0 ? 'left' : 'right';
          const offset = Math.floor(unitIndex / 2) * Y_AXIS_OFFSET;
          const name = unitIndex === 0 ? plotChart.title : undefined;

          return {
            type: 'value',
            position,
            name,
            axisLabel: {
              formatter: `{value} ${unit}`,
            },
            max,
            min,
            gridIndex: plotIndex,
            axisLine: {
              show: true,
            },
            offset,
          };
        },
      );
    },
  );

  return yAxis;
};

export const dateFormatter = (date: string): string => {
  return dateUtils.dateToString(dateUtils.utc(date), 'eee HH:mm')!;
};

export const getTooltipTimeLabel = (date: string): string => {
  return dateUtils.dateToString(dateUtils.utc(date), 'eee dd MMM HH:mm')!;
};

export const getXAxis = (plotCharts: PlotChart[]): XAXisComponentOption[] => {
  return plotCharts.map((_plotChart, index): XAXisComponentOption => {
    if (index === plotCharts.length - 1) {
      return {
        type: 'category',
        gridIndex: index,
        axisLabel: {
          formatter: dateFormatter,
        },
      };
    }
    return {
      gridIndex: index,
      type: 'category',
      show: false,
    };
  });
};

export const getOption = (plotsWithData: PlotWithData[]): EChartsOption => {
  const plotCharts = getPlotCharts(plotsWithData);
  const xAxis = getXAxis(plotCharts);
  const yAxis = getYAxis(plotCharts);
  const series = getSeries(plotCharts);
  const grid = getGrid(plotCharts);

  const tooltip: TooltipComponentOption = {
    trigger: 'axis',
    formatter: (params: DefaultLabelFormatterCallbackParams[]) => {
      const paramLabels = params
        .map(
          (param) =>
            `<br />${param.seriesName}: ${
              Array.isArray(param.data) ? param.data[1] : ''
            }`,
        )
        .reduce((paramLabel, paramLabels) => paramLabel + paramLabels);

      const date: string = Array.isArray(params[0].data)
        ? (params[0].data[0] as string)
        : '';
      const timeLabel = getTooltipTimeLabel(date);

      return `<b>${timeLabel}</b>${paramLabels}`;
    },
  };
  const parameters = plotsWithData.flatMap((plot) => {
    return plot.parametersWithData;
  });
  const parameterNames = uniq(
    parameters.map((parameter) => parameter.propertyName),
  );

  const dataZoomLabelWidth = 70;
  const option: EChartsOption = {
    legend: {
      data: parameterNames,
    },
    tooltip,
    axisPointer: {
      link: [
        {
          xAxisIndex: 'all',
        },
      ],
    },
    dataZoom: [
      {
        show: true,
        realtime: true,
        left: dataZoomLabelWidth,
        right: dataZoomLabelWidth,
        xAxisIndex: parameters.map((_, index) => index),
        labelFormatter: (_, date): string => {
          return dateFormatter(date);
        },
      },
    ],
    grid,
    xAxis,
    yAxis,
    series,
  };
  return option;
};

export const getEChartsSeriesDataByTimestep = (
  timesteps: Date[],
  parameter: ParameterWithData,
): (number | string)[][] => {
  const data = timesteps.map((timestep) => {
    const index = parameter.timestep.findIndex((parameterTimestep) => {
      return parameterTimestep.getTime() === timestep.getTime();
    });
    const value = index === -1 ? NaN : parameter.value[index];
    return [timestep.toISOString(), value];
  });
  return data;
};

export const getSeries = (
  plotCharts: PlotChart[],
): (LineSeriesOption | BarSeriesOption | ScatterSeriesOption)[] => {
  const allTimesteps = plotCharts.flatMap((plotChart) =>
    Object.values(plotChart.parametersByUnit).flatMap((parameters) =>
      parameters.flatMap((parameter) => parameter.timestep),
    ),
  );

  const uniqueTimesteps = sortBy(
    uniqBy(allTimesteps, (timestep) => timestep.getTime()),
    (timestep: Date) => timestep,
    'desc',
  );
  const series = plotCharts.flatMap((plotChart, plotIndex) => {
    return Object.entries(plotChart.parametersByUnit).flatMap(
      ([, parameters], unitIndex) => {
        return parameters.flatMap(
          (
            parameter,
          ): LineSeriesOption | BarSeriesOption | ScatterSeriesOption => {
            const color =
              COLOR_NAME_TO_HEX_MAP[
                parameter.color as keyof typeof COLOR_NAME_TO_HEX_MAP
              ] ??
              COLOR_NAME_TO_HEX_MAP[
                COLOR_MAP[parameter.propertyName as keyof typeof COLOR_MAP]
              ];
            const type =
              parameter.plotType === 'area' ? 'line' : parameter.plotType;
            return {
              data: getEChartsSeriesDataByTimestep(uniqueTimesteps, parameter),
              name: parameter.propertyName,
              type,
              xAxisIndex: plotIndex,
              yAxisIndex: plotChart.countPreviousUnits + unitIndex,
              lineStyle: { color, opacity: parameter.opacity! / 100 },
              itemStyle: { color, opacity: parameter.opacity! / 100 },
              ...(parameter.plotType === 'area' && { areaStyle: {} }),
              ...(parameter.plotType === 'scatter' && { symbolSize: 6 }),
            };
          },
        );
      },
    );
  });

  return series;
};

export const getGrid = (plotCharts: PlotChart[]): GridComponentOption[] => {
  const maxUniqueUnitPerPlot = plotCharts.reduce(
    (maxUniqueUnitPerPlot, plotChart) => {
      const countUniqueUnitsPerPlot = Object.keys(
        plotChart.parametersByUnit,
      ).length;
      if (countUniqueUnitsPerPlot > maxUniqueUnitPerPlot) {
        return countUniqueUnitsPerPlot;
      }
      return maxUniqueUnitPerPlot;
    },
    0,
  );

  const grid = plotCharts.map((_, index): GridComponentOption => {
    const top = PLOT_TOP_MARGIN + PLOT_HEIGHT * index;

    const marginValue = (): number => {
      const marginValue = 50;
      if (maxUniqueUnitPerPlot > 0) {
        return marginValue + maxUniqueUnitPerPlot * 2;
      }
      return marginValue;
    };

    return {
      left: `${marginValue()}px`,
      right: `${marginValue()}px`,
      top: `${top}px`,
      height: '100px',
    };
  });
  return grid;
};

export const Y_AXIS_OFFSET = 80;
export const PLOT_HEIGHT = 150;
const PLOT_TOP_MARGIN = 50;

export interface PlotChart extends Plot {
  parametersByUnit: Record<string, ParameterWithData[]>;
  countPreviousUnits: number;
}

function getPlotCharts(plotsWithData: PlotWithData[]): PlotChart[] {
  let countUnits = 0;
  const plotCharts = plotsWithData.map((plot): PlotChart => {
    const countPreviousUnits = countUnits;
    const parametersByUnit = groupBy(
      plot.parametersWithData,
      (parameter) => parameter.unit,
    );
    const units = Object.keys(parametersByUnit);
    countUnits += units.length;
    return { ...plot, parametersByUnit, countPreviousUnits };
  });

  return plotCharts;
}
