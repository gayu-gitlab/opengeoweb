/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React, { useEffect, useRef, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import {
  CoreAppStore,
  layerActions,
  mapStoreActions,
  mapSelectors,
  layerSelectors,
} from '@opengeoweb/store';
import { Box, Stack } from '@mui/material';
import { LockLocked, LockUnlocked } from '@opengeoweb/theme';
import { FeatureCollection, Point } from 'geojson';
import { registerDrawFunction } from '@opengeoweb/webmap-react';
import { LayerType, webmapUtils } from '@opengeoweb/webmap';
import { CustomTooltip } from '@opengeoweb/shared';
import { PlotPreset, PointerLocation, TimeSeriesPresetLocation } from './types';
import { TimeSeriesView } from './TimeSeriesView';
import { LocationSelect } from './LocationSelect';
import { getPlotState, getServices } from '../../store/selectors';
import {
  circleDrawFunction,
  createGeojson,
  useHandleClickOnMap,
} from './utils';
import { getLocations } from '../../utils/edrUtils';
import { TimeSeriesTopRowButton } from './TimeSeriesTopRowButton';
import { TimeSeriesModuleState } from '../../store/types';
import TimeSeriesManagerMapButtonConnect from '../TimeSeriesManager/TimeSeriesManagerMapButtonConnect';

interface Props {
  timeSeriesPresetLocations?: TimeSeriesPresetLocation[];
}

export type PointerLocationAndName = PointerLocation & {
  id?: string;
};

let idCounter = 0;
export const LOCK_LOCATION_TEXT = 'Lock location';
export const UNLOCK_LOCATION_TEXT = 'Unlock location';
export const LOCK_LOCATION_TEXT_SHORT = 'Lock';
export const UNLOCK_LOCATION_TEXT_SHORT = 'Unlock';

export const TimeSeriesConnect: React.FC<Props> = ({
  timeSeriesPresetLocations,
}) => {
  const dispatch = useDispatch();

  const services = useSelector((state: TimeSeriesModuleState) =>
    getServices(state),
  );

  const [viewId] = React.useState<string>(() => {
    idCounter += 1;
    return `timeseriesView_${idCounter}`;
  });

  const plotPresetState: PlotPreset | undefined = useSelector(getPlotState);

  const mapId = plotPresetState?.connectedMap ?? plotPresetState?.mapId ?? '';

  const mapPinLocation: PointerLocation | undefined = useSelector(
    (store: CoreAppStore) => mapSelectors.getPinLocation(store, mapId),
  );

  const [timeSeriesLocation, setTimeSeriesLocation] = useState<
    PointerLocationAndName | undefined
  >(mapPinLocation);

  const [selectedLocation, setSelectedLocation] = React.useState('');

  const disableMapPin = useSelector((store: CoreAppStore) =>
    mapSelectors.getDisableMapPin(store, mapId),
  );

  const isMapPresent = useSelector((store: CoreAppStore) =>
    mapSelectors.getIsMapPresent(store, mapId),
  );

  const isMapPinVisible: boolean = useSelector(
    (store: CoreAppStore) => mapSelectors.getDisplayMapPin(store, mapId)!,
  );

  const circleDrawFunctionId = useRef(registerDrawFunction(circleDrawFunction));
  const selectedCircleDrawFunctionId = useRef(
    registerDrawFunction((args): void => {
      circleDrawFunction(args, '#F00', 12);
    }),
  );

  const [geojsonLayerId, setGeojsonLayerId] = useState<string | undefined>();
  const [geojson, setGeojson] = useState<FeatureCollection<Point>>();

  useEffect(() => {
    if (services && services[0].type === 'OGC' && timeSeriesPresetLocations) {
      const geojson = createGeojson(
        timeSeriesPresetLocations,
        circleDrawFunctionId.current,
      );
      drawGeojson(geojson);
    } else if (services) {
      // Obtain the collectionId from the first parameter in the plotpreset
      const collectionId =
        plotPresetState && plotPresetState.parameters.length > 0
          ? plotPresetState.parameters[0].collectionId
          : 'ecmwf';
      const serviceId =
        plotPresetState && plotPresetState.parameters.length > 0
          ? plotPresetState.parameters[0].serviceId
          : services[0].id;
      const relatedService = services.find(
        (service) => service.id === serviceId,
      );
      getLocations(
        relatedService || services[0],
        collectionId,
        circleDrawFunctionId.current,
      ).then((geojson) => {
        if (geojson) {
          drawGeojson(geojson);
        }
      });
    }

    function drawGeojson(geojson: FeatureCollection<Point>): void {
      setGeojson(geojson);
      const layerId = webmapUtils.generateLayerId();
      setGeojsonLayerId(layerId);
      dispatch(
        mapStoreActions.addLayer({
          mapId,
          layer: { geojson, layerType: LayerType.featureLayer },
          layerId,
          origin: 'TimeSeriesConnect',
        }),
      );
    }
  }, [dispatch, mapId, plotPresetState, services, timeSeriesPresetLocations]);

  /* Make sure the mappin is visible when clicking the map */
  useEffect(() => {
    dispatch(
      mapStoreActions.toggleMapPinIsVisible({
        mapId,
        displayMapPin: true,
      }),
    );
  }, [dispatch, isMapPinVisible, mapId, mapPinLocation]);

  const toggleDisableMapPin = (): void => {
    dispatch(
      mapStoreActions.setDisableMapPin({
        mapId,
        disableMapPin: !disableMapPin,
      }),
    );
  };

  const setLayerGeojson = (geojson: FeatureCollection): void => {
    dispatch(
      layerActions.layerChangeGeojson({
        geojson,
        layerId: geojsonLayerId!,
      }),
    );
  };

  const setMapPinLocation = (location: PointerLocation): void => {
    dispatch(
      mapStoreActions.setMapPinLocation({
        mapId,
        mapPinLocation: location,
      }),
    );
  };

  const selectedFeatureIndex = useSelector((store: CoreAppStore) =>
    layerSelectors.getSelectedFeatureIndex(store, geojsonLayerId),
  );
  useHandleClickOnMap(
    selectedFeatureIndex,
    mapPinLocation!,
    setMapPinLocation,
    setTimeSeriesLocation,
    setSelectedLocation,
    geojson,
    disableMapPin!,
    setLayerGeojson,
    circleDrawFunctionId.current,
    selectedCircleDrawFunctionId.current,
  );

  const setSelectedFeatureIndex = (selectedFeatureIndex: number): void => {
    dispatch(
      mapStoreActions.setSelectedFeature({
        layerId: geojsonLayerId!,
        selectedFeatureIndex,
      }),
    );
  };

  return (
    <Stack
      sx={{
        padding: '20px',
        width: '100%',
        backgroundColor: 'geowebColors.background.surfaceApp',
        containerType: 'inline-size',
        containerName: 'time-series-container',
      }}
      data-testid="TimeSeriesConnect"
    >
      <Stack
        direction="row"
        spacing={1}
        maxWidth="400px"
        sx={{ marginBottom: '4px', marginLeft: '10px' }}
      >
        <TimeSeriesManagerMapButtonConnect viewId={viewId} />

        {geojson && (
          <LocationSelect
            selectedLocation={selectedLocation}
            isMapPresent={isMapPresent}
            mapPinLocation={timeSeriesLocation!}
            disableMapPin={disableMapPin!}
            geojson={geojson}
            setSelectedFeatureIndex={setSelectedFeatureIndex}
            setTimeSeriesLocation={setTimeSeriesLocation}
            setSelectedLocation={setSelectedLocation}
            setMapPinLocation={setMapPinLocation}
          />
        )}

        <CustomTooltip
          title={disableMapPin ? UNLOCK_LOCATION_TEXT : LOCK_LOCATION_TEXT}
        >
          <TimeSeriesTopRowButton
            onClick={(): void => toggleDisableMapPin()}
            selected={disableMapPin}
            data-testid="toggleLockLocationButton"
            icon={disableMapPin ? <LockLocked /> : <LockUnlocked />}
            longText={disableMapPin ? UNLOCK_LOCATION_TEXT : LOCK_LOCATION_TEXT}
            shortText={
              disableMapPin
                ? UNLOCK_LOCATION_TEXT_SHORT
                : LOCK_LOCATION_TEXT_SHORT
            }
          />
        </CustomTooltip>
      </Stack>
      {timeSeriesLocation && plotPresetState && (
        <Box>
          {services && (
            <TimeSeriesView
              plotPreset={plotPresetState}
              selectedLocation={timeSeriesLocation}
              services={services}
            />
          )}
        </Box>
      )}
    </Stack>
  );
};
