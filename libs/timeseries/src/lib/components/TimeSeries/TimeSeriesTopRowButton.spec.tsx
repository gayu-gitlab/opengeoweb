/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Location } from '@opengeoweb/theme';
import { render, screen } from '@testing-library/react';
import { TimeSeriesTopRowButton } from './TimeSeriesTopRowButton';

describe('src/components/TimeSeries/TimeSeriesTopRowButton', () => {
  it('should include short and long text', () => {
    const longText = 'long text is looong';
    const shortText = 'short text is short';
    render(
      <TimeSeriesTopRowButton
        icon={<Location />}
        longText={longText}
        shortText={shortText}
      />,
    );
    expect(
      // eslint-disable-next-line testing-library/no-node-access
      screen.getByRole('button').getElementsByClassName('shortText')[0]
        .textContent,
    ).toBe(shortText);
    expect(
      // eslint-disable-next-line testing-library/no-node-access
      screen.getByRole('button').getElementsByClassName('longText')[0]
        .textContent,
    ).toBe(longText);
  });
  it('should fallback to long text if short text is unavailable', () => {
    const longText = 'long text is looong';
    render(<TimeSeriesTopRowButton icon={<Location />} longText={longText} />);
    expect(
      // eslint-disable-next-line testing-library/no-node-access
      screen.getByRole('button').getElementsByClassName('shortText')[0]
        .textContent,
    ).toBe(longText);
  });
});
