/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { act, render, screen, waitFor } from '@testing-library/react';
import {
  TimeSeriesConnectWrapper,
  ERROR_TITLE,
} from './TimeSeriesConnectWrapper';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { TimeSeriesApi } from '../../utils/api';
import { store } from '../../utils/store';
import {
  TimeSeriesThemeStoreProvider,
  TimeSeriesApiProvider,
} from '../Providers';
import { TimeSeriesStoreType } from '../../store/types';
import { mockTimeSeriesServices } from './mockTimeSeriesServices';

describe('components/TimeSeries/TimeSeriesConnectWrapper', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  const timeSeriesPreset: TimeSeriesStoreType = {
    services: mockTimeSeriesServices,
    plotPreset: {
      mapId: 'map-1',
      plots: [],
      parameters: [],
    },
  };

  it('should render correctly with config', async () => {
    const timeseriesConfigKey = 'exampleTimeSeriesPresetLocations.json';

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesApiProvider createApi={createFakeApi}>
          <TimeSeriesConnectWrapper
            productConfigKey={timeseriesConfigKey}
            timeSeriesPreset={timeSeriesPreset}
          />
        </TimeSeriesApiProvider>
      </TimeSeriesThemeStoreProvider>,
    );

    expect(screen.getByTestId('loadingSpinner')).toBeTruthy();
    await act(async () => jest.advanceTimersByTime(2000));
    await waitFor(() => {
      expect(screen.queryByTestId('loadingSpinner')).toBeFalsy();
    });
    expect(screen.getByTestId('TimeSeriesConnect')).toBeTruthy();
  });

  it('should fetch timeseries config from api', async () => {
    const timeseriesRequest = jest.fn();
    const fakeApi = (): TimeSeriesApi =>
      ({
        ...createFakeApi(),
        getTimeSeriesConfiguration: timeseriesRequest,
      } as unknown as TimeSeriesApi);

    const timeseriesConfigKey = 'exampleTimeSeriesPresetLocations.json';

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesApiProvider createApi={fakeApi}>
          <TimeSeriesConnectWrapper
            productConfigKey={timeseriesConfigKey}
            timeSeriesPreset={timeSeriesPreset}
          />
        </TimeSeriesApiProvider>
      </TimeSeriesThemeStoreProvider>,
    );

    expect(screen.getByTestId('loadingSpinner')).toBeTruthy();
    await waitFor(() => {
      expect(screen.queryByTestId('ConfigurableMap')).toBeFalsy();
    });

    expect(timeseriesRequest).toHaveBeenCalledWith(
      'exampleTimeSeriesPresetLocations.json',
      expect.any(String),
    );
  });

  it('should show error if configuration is missing', async () => {
    const timeseriesRequest = jest.fn();
    const fakeApi = (): TimeSeriesApi =>
      ({
        ...createFakeApi(),
        getTimeSeriesConfiguration: timeseriesRequest,
      } as unknown as TimeSeriesApi);

    const emptyConfig = '';

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesApiProvider createApi={fakeApi}>
          <TimeSeriesConnectWrapper
            productConfigKey={emptyConfig}
            timeSeriesPreset={timeSeriesPreset}
          />
        </TimeSeriesApiProvider>
      </TimeSeriesThemeStoreProvider>,
    );

    expect(await screen.findByText(ERROR_TITLE)).toBeTruthy();
  });

  it('should not render without config', async () => {
    const timeseriesRequest = jest.fn();
    const fakeApi = (): TimeSeriesApi =>
      ({
        ...createFakeApi(),
        getTimeSeriesConfiguration: timeseriesRequest,
      } as unknown as TimeSeriesApi);

    const emptyConfig = '';

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesApiProvider createApi={fakeApi}>
          <TimeSeriesConnectWrapper
            productConfigKey={emptyConfig}
            timeSeriesPreset={timeSeriesPreset}
          />
        </TimeSeriesApiProvider>
      </TimeSeriesThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('ConfigurableMap')).toBeFalsy();
    });
  });
});
