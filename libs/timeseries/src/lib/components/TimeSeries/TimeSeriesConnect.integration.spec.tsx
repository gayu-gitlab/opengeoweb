/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { act, fireEvent, render, screen, within } from '@testing-library/react';
import React from 'react';
import userEvent from '@testing-library/user-event';
import { createStore, layerActions, mapActions } from '@opengeoweb/store';
import produce from 'immer';
import { webmapUtils } from '@opengeoweb/webmap';
import { ToolkitStore } from '@reduxjs/toolkit/dist/configureStore';
import { Store } from '@reduxjs/toolkit';
import { ServiceInterface, TimeseriesServiceScope } from '@opengeoweb/shared';
import { TimeSeriesConnect } from './TimeSeriesConnect';
import { TimeSeriesPresetLocation } from './types';
import { serverMock } from '../../../mocks/server';
import {
  parameterNameMock,
  EDR_FINLAND_URL,
  OGC_URL,
  helsinkiLat,
  helsinkiLon,
  helsinki,
  EDR_NORWAY_URL,
  EDR_NORWAY_COLLECTION_ID,
} from '../../../mocks';
import { TimeSeriesThemeStoreProvider } from '../Providers';
import { TimeSeriesManagerConnect } from '../TimeSeriesManager';
import { TimeSeriesStoreType } from '../../store/types';
import { actions } from '../../store/reducer';

const mapId = 'mapId';
const geojsonLayerId = 'geojsonLayerId';
const plotTitle = 'plotTitle';
describe('src/components/TimeSeries/TimeSeriesConnect/integration', () => {
  const plotId = 'plotId';

  const service = {
    id: 'serviceId',
    type: 'EDR' as ServiceInterface,
    url: EDR_FINLAND_URL,
    description: 'a description',
    name: 'FMI OGC',
    scope: 'system' as TimeseriesServiceScope,
  };

  const plotServicesEdr = [service];

  const collectionId = 'ecmwf';
  const timeSeriesPresetEdrFinland: TimeSeriesStoreType = {
    plotPreset: {
      mapId,
      parameters: [
        {
          plotId,
          plotType: 'bar',
          propertyName: parameterNameMock,
          serviceId: 'serviceId',
          unit: 'unit',
          collectionId,
        },
      ],
      plots: [{ plotId, title: plotTitle }],
    },
    services: plotServicesEdr,
    timeseriesSelect: {
      filters: {
        searchFilter: '',
        serviceFilterChips: {
          entities: {
            [service.id]: {
              serviceId: service.id,
              type: service.type,
              serviceUrl: service.url,
              serviceName: service.name,
              enabled: true,
              scope: 'system',
            },
          },
          ids: [service.id],
        },
        allServiceFilterChipsEnabled: true,
      },
    },
  };

  const defaultParamOgc = `?f=json&limit=200`;

  const plotPresetOgc = produce(timeSeriesPresetEdrFinland, (draft) => {
    if (draft.services) {
      draft.services[0].type = 'OGC';
      draft.services[0].url = `${OGC_URL}${defaultParamOgc}`;
    }
  });

  const presetLocations = [
    {
      lat: helsinkiLat,
      lon: helsinkiLon,
      name: helsinki,
    },
  ];

  jest.spyOn(webmapUtils, 'generateLayerId').mockReturnValue(geojsonLayerId);

  serverMock.listen();
  afterAll(() => {
    serverMock.close();
  });

  const setup = (
    timeSeriesPreset: TimeSeriesStoreType,
    presetLocations?: TimeSeriesPresetLocation[],
    twoTimeSeries?: boolean,
  ): Store => {
    const store = createStore();
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesConnect timeSeriesPresetLocations={presetLocations} />
        {twoTimeSeries && (
          <TimeSeriesConnect timeSeriesPresetLocations={presetLocations} />
        )}
        <TimeSeriesManagerConnect />
      </TimeSeriesThemeStoreProvider>,
    );

    act(() => {
      store.dispatch(mapActions.registerMap({ mapId }));
      store.dispatch(actions.registerTimeSeriesPreset(timeSeriesPreset));
    });

    return store;
  };

  it('should work for ogc timeseries', async () => {
    const store = setup(plotPresetOgc, presetLocations);

    await clickAround(store, false, 'collectionOGC');
  });

  it('should work for edr finland timeseries', async () => {
    const store = setup(timeSeriesPresetEdrFinland);
    await clickAround(store, true, collectionId);
  });

  it('should work for edr norway timeseries', async () => {
    const plotPresetEdrNorway = produce(timeSeriesPresetEdrFinland, (draft) => {
      if (draft.services) {
        draft.services[0].url = EDR_NORWAY_URL;
        if (
          draft.plotPreset &&
          draft.plotPreset.parameters &&
          draft.plotPreset.parameters.length > 0
        ) {
          draft.plotPreset.parameters.forEach((draftParameter) => {
            draftParameter.collectionId = EDR_NORWAY_COLLECTION_ID;
          });
        }
      }
    });
    const store = setup(plotPresetEdrNorway);
    await clickAround(store, true, EDR_NORWAY_COLLECTION_ID);
  });

  it('should work for two ogc timeseries', async () => {
    /**
     * TODO:
     * We need to mock a warning for this test to work.
     * Remove when we have a sufficient implementation of timeseries multiview.
     */
    jest.spyOn(console, 'warn').mockImplementation();
    const hasTwoTimeseries = true;
    setup(plotPresetOgc, presetLocations, hasTwoTimeseries);

    const user = userEvent.setup();

    const openManagerButtons = screen.getAllByRole('button', {
      name: 'TimeSeries Manager',
    });
    expect(openManagerButtons).toHaveLength(2);
    const [first, second] = openManagerButtons;
    expect(first).toHaveAttribute('aria-pressed', 'false');
    expect(second).toHaveAttribute('aria-pressed', 'false');
    expect(
      screen.queryByRole('heading', { name: /time series manager/i }),
    ).not.toBeInTheDocument();

    await user.click(first);
    expect(first).toHaveAttribute('aria-pressed', 'true');
    expect(second).toHaveAttribute('aria-pressed', 'false');
    screen.getByRole('heading', { name: /time series manager/i });

    await user.click(second);
    expect(first).toHaveAttribute('aria-pressed', 'false');
    expect(second).toHaveAttribute('aria-pressed', 'true');
    screen.getByRole('heading', { name: /time series manager/i });

    await user.click(second);
    expect(first).toHaveAttribute('aria-pressed', 'false');
    expect(second).toHaveAttribute('aria-pressed', 'false');
    expect(
      screen.queryByRole('heading', { name: /time series manager/i }),
    ).not.toBeInTheDocument();
    expect(console.warn).toHaveBeenCalled();
  });
});

const clickAround = async (
  store: ToolkitStore,
  isEdr = false,
  collectionId = '',
): Promise<void> => {
  const user = userEvent.setup();

  // map pin has not been set so no chart is shown
  expect(screen.queryByTestId('TimeSeriesChart')).not.toBeInTheDocument();

  // simulate setting map pin on the map outside of a preset location
  await act(() => {
    store.dispatch(
      mapActions.setMapPinLocation({
        mapId,
        mapPinLocation: { lat: 1, lon: 1 },
      }),
    );
  });

  expect(await screen.findByTestId('TimeSeriesChart')).toBeInTheDocument();

  // set location to helsinki with the location select
  // which will change map pin location
  const locationSelect = await screen.findByRole('button', {
    name: 'Location',
  });
  expect(locationSelect).not.toHaveTextContent(helsinki);
  await user.click(locationSelect);
  await user.click(screen.getByRole('menuitem', { name: helsinki }));
  expect(
    screen.getByRole('button', {
      name: 'Location',
    }),
  ).toHaveTextContent(helsinki);

  expect(store.getState().webmap.byId[mapId].mapPinLocation).toEqual({
    lat: helsinkiLat,
    lon: helsinkiLon,
    id: isEdr ? 'helsinki' : undefined,
  });

  // simulate setting map pin on the map outside of a preset location
  // will reset the location select
  act(() => {
    store.dispatch(
      mapActions.setMapPinLocation({
        mapId,
        mapPinLocation: { lat: 1, lon: 1 },
      }),
    );
    store.dispatch(
      layerActions.setSelectedFeature({
        layerId: geojsonLayerId,
        selectedFeatureIndex: undefined,
      }),
    );
  });
  expect(
    screen.getByRole('button', {
      name: 'Location',
    }),
  ).not.toHaveTextContent(helsinki);

  // Open timeseries manager
  await user.click(screen.getByRole('button', { name: /timeseries manager/i }));
  screen.getByRole('heading', { name: /time series manager/i });
  screen.getByText(plotTitle);
  expect(screen.getAllByText(parameterNameMock)[1]).toHaveTextContent(
    'Temperature',
  );

  // Open timeseries select
  await user.click(
    screen.getByRole('button', { name: 'Add parameter to plot' }),
  );
  screen.getByRole('heading', { name: 'Timeseries Select for plotTitle' });
  const timeSeriesSelectList = screen.getByTestId('timeseriesSelectList');
  const row = within(timeSeriesSelectList).getByText(collectionId);
  fireEvent.click(row);
  within(screen.getByTestId('TimeSeriesSelect')).getByText(parameterNameMock);
};
