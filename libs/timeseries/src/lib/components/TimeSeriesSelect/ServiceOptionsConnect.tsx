/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { ToggleMenu } from '@opengeoweb/shared';
import { useSelector } from 'react-redux';
import { TimeSeriesModuleState } from '../../store/types';
import { timeSeriesSelectors } from '../../store';
import { ServiceOptionsDialog } from './ServiceOptionsDialog';

export const ServiceOptionsButtonConnect: React.FC = () => {
  const services = useSelector((store: TimeSeriesModuleState) =>
    timeSeriesSelectors.getServices(store),
  );

  const parametersInUse = useSelector((store: TimeSeriesModuleState) =>
    timeSeriesSelectors.getParametersInUse(store),
  );

  return (
    <ToggleMenu menuPosition="bottom">
      <ServiceOptionsDialog
        services={services}
        parametersInUse={parametersInUse}
      />
    </ToggleMenu>
  );
};
