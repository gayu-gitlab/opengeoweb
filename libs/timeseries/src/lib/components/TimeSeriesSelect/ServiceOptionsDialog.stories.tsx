/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { darkTheme, lightTheme } from '@opengeoweb/theme';

import { createStore } from '@opengeoweb/store';
import { TimeSeriesService } from '@opengeoweb/shared';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';
import { Parameter } from '../TimeSeries/types';
import { ServiceOptionsDialog } from './ServiceOptionsDialog';

const ServiceOptionsDialogDemo: React.FC<{
  services?: TimeSeriesService[];
  parametersInUse?: Parameter[];
}> = ({
  services = [
    {
      id: 'norway_EDR',
      type: 'EDR',
      url: 'https://interpol-b.met.no',
      description: 'Norwegian EDR service',
      name: 'MetNorway',
      scope: 'system',
    },
    {
      id: 'fmi_EDR',
      type: 'EDR',
      url: 'https://opendata.fmi.fi/edr',
      description: 'Finnish EDR service',
      name: 'FMI',
      scope: 'system',
    },
    {
      id: 'ogc_fake_api',
      type: 'OGC',
      url: 'someURL',
      description: 'Fake OGC service',
      name: 'Fake OGC',
      scope: 'system',
    },
    {
      id: 'personalService',
      type: 'EDR',
      url: 'someURL/personal',
      description: 'Personal added service',
      name: 'Personal service',
      scope: 'user',
    },
  ],
  parametersInUse = [
    {
      plotId: 'plot1',
      unit: '°C',
      propertyName: 'Temperature',
      plotType: 'line',
      serviceId: services[0].id,
      id: 'param1',
      collectionId: 'ecmwf',
    },
    {
      plotId: 'plot1',
      unit: '°C',
      propertyName: 'DewPoint',
      plotType: 'line',
      serviceId: services[0].id,
      collectionId: 'ecmwf',
    },
    {
      plotId: 'plotId2',
      unit: '%',
      propertyName: 'Humidity',
      plotType: 'line',
      serviceId: services[0].id,
      collectionId: 'ecmwf',
    },
  ],
}) => {
  return (
    <div style={{ backgroundColor: 'grey', padding: '16px', width: '400px' }}>
      <ServiceOptionsDialog
        services={services}
        parametersInUse={parametersInUse}
      />
    </div>
  );
};

export const ServiceOptionsDialogDemoLightTheme = (): React.ReactElement => (
  <TimeSeriesThemeStoreProvider store={createStore()} theme={lightTheme}>
    <ServiceOptionsDialogDemo />
  </TimeSeriesThemeStoreProvider>
);
ServiceOptionsDialogDemoLightTheme.storyName =
  'ServiceOptionsDialog light theme (takeSnapshot)';

export const ServiceOptionsDialogDemoDarkTheme = (): React.ReactElement => (
  <TimeSeriesThemeStoreProvider store={createStore()} theme={darkTheme}>
    <ServiceOptionsDialogDemo />
  </TimeSeriesThemeStoreProvider>
);

ServiceOptionsDialogDemoDarkTheme.storyName =
  'ServiceOptionsDialog dark theme (takeSnapshot)';

export default { title: 'components/TimeSeriesSelect' };
