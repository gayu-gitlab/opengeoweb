/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';

import ServiceChip from './ServiceChip';
import { ServiceFilterChipsObject } from '../../../store/types';
import { TimeSeriesThemeProvider } from '../../../storybookUtils/Providers';

describe('src/components/LayerSelect/ServiceChip', () => {
  const defaultService: ServiceFilterChipsObject = {
    // serviceName: 'testservice1',
    serviceId: 'service1',
    type: 'EDR',
    serviceUrl: 'https://testservice1',
    enabled: true,
  };

  const props = {
    service: defaultService,
    isSelected: false,
    toggleChip: jest.fn(),
  };
  const disabledProps = {
    service: defaultService,
    isSelected: false,
    isDisabled: true,
    toggleChip: jest.fn(),
  };
  it('should have disabled state if isDisabled is true', () => {
    render(
      <TimeSeriesThemeProvider>
        <ServiceChip {...disabledProps} />
      </TimeSeriesThemeProvider>,
    );

    const chip = screen.getByRole('button', {
      name: defaultService.serviceName,
    });
    fireEvent.click(chip);
    expect(props.toggleChip).not.toHaveBeenCalled();
    expect(chip.classList.contains('Mui-disabled')).toBeTruthy();
  });

  it('should show tooltip when chip is hovered', async () => {
    render(
      <TimeSeriesThemeProvider>
        <ServiceChip {...props} />
      </TimeSeriesThemeProvider>,
    );

    const chip = screen.getByRole('button', {
      name: defaultService.serviceName,
    });
    expect(chip).toBeTruthy();

    fireEvent.mouseOver(chip);

    const tooltip = await screen.findByRole('tooltip');
    expect(tooltip.textContent).toContain(`URL: ${defaultService.serviceUrl}`);
  });
});
