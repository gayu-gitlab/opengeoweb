/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  act,
  fireEvent,
  render,
  screen,
  within,
  waitFor,
} from '@testing-library/react';
import {
  uiTypes,
  uiReducer,
  webmapReducer,
  CoreAppStore,
  mapActions,
  uiActions,
} from '@opengeoweb/store';
import {
  ServiceInterface,
  TimeSeriesService,
  createToolkitMockStoreWithEggs,
} from '@opengeoweb/shared';
import axios from 'axios';
import userEvent from '@testing-library/user-event';
import { TimeSeriesThemeStoreProvider } from '../../storybookUtils/Providers';
import { TimeSeriesSelectConnect } from './TimeSeriesSelectConnect';
import { TimeSeriesModuleState } from '../../store/types';
import { reducer } from '../../store/reducer';
import { mockTimeSeriesServices as services } from '../TimeSeries/mockTimeSeriesServices';
import { timeSeriesActions } from '../../store';
import { NO_RESULTS_FOUND_MESSAGE } from './TimeSeriesSelectList';
import { ogcParameters } from './ogcParameters';

describe('components/TimeSeriesSelect/TimeSeriesSelectConnect', () => {
  const mockMapId = 'mockMapId';

  it('should not show select dialog if dialog is not in ui store', () => {
    const timeSeriesStore: TimeSeriesModuleState = {
      timeSeries: {
        plotPreset: {
          mapId: mockMapId,
          plots: [{ plotId: 'plotId1', title: 'title' }],
          parameters: [],
        },
        services,
        timeseriesSelect: {
          filters: {
            searchFilter: '',
            serviceFilterChips: {
              entities: {
                [services[0].id]: {
                  serviceId: services[0].id,
                  type: services[0].type,
                  serviceUrl: services[0].url,
                  serviceName: services[0].name,
                  enabled: true,
                  scope: 'system',
                },
              },
              ids: [services[0].id],
            },
            allServiceFilterChipsEnabled: true,
          },
        },
      },
    };

    const mockState = {
      reducer: { timeSeries: reducer },
      preloadedState: timeSeriesStore,
    };
    const store = createToolkitMockStoreWithEggs(mockState);

    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelectConnect selectPlotId="plotIdNotInStore" />
      </TimeSeriesThemeStoreProvider>,
    );

    expect(
      screen.queryByRole('heading', {
        name: /timeseries select/i,
      }),
    ).not.toBeInTheDocument();
  });

  const coreStore = {
    webmap: {
      byId: {
        mockMapId: {
          bbox: {
            left: -450651.2255879827,
            bottom: 6490531.093143953,
            right: 1428345.8183648037,
            top: 7438773.776232235,
          },
          srs: 'EPSG:3857',
          id: 'test',
          isAnimating: false,
          isAutoUpdating: false,
          isEndTimeOverriding: false,
          baseLayers: [],
          overLayers: [],
          mapLayers: [],
          featureLayers: [],
        },
      },
      allIds: [mockMapId],
    },
    ui: {
      order: [uiTypes.DialogTypes.TimeSeriesSelect],
      dialogs: {
        timeSeriesSelect: {
          activeMapId: mockMapId,
          isOpen: true,
          type: uiTypes.DialogTypes.TimeSeriesSelect,
          source: 'app',
        },
      },
    },
  } as CoreAppStore;
  const plotId = 'plotId1';

  it('should handle adding and removing parameters', async () => {
    const parameterId = 'parameter1';
    const temperatureParameter = ogcParameters[0];
    const service: TimeSeriesService = {
      id: temperatureParameter.serviceId,
      url: 'serviceUrl',
      type: 'OGC' as ServiceInterface,
      description: 'A description',
      name: 'OGC',
      scope: 'system',
    };

    const timeSeriesStore: TimeSeriesModuleState = {
      timeSeries: {
        plotPreset: {
          mapId: mockMapId,
          connectedMap: mockMapId,
          plots: [
            {
              title: 'Plot 1',
              plotId,
            },
          ],
          parameters: [
            {
              id: parameterId,
              plotId,
              plotType: 'line',
              propertyName: temperatureParameter.propertyName,
              unit: temperatureParameter.unit,
              serviceId: service.id,
              collectionId: temperatureParameter.collectionId,
            },
          ],
        },
        services: [service],
        timeseriesSelect: {
          filters: {
            searchFilter: '',
            serviceFilterChips: {
              entities: {
                [services[0].id]: {
                  serviceId: services[0].id,
                  type: services[0].type,
                  serviceUrl: services[0].url,
                  serviceName: services[0].name,
                  enabled: true,
                  scope: 'system',
                },
              },
              ids: [services[0].id],
            },
            allServiceFilterChipsEnabled: true,
          },
        },
      },
    };
    const mockState = {
      reducer: {
        timeSeries: reducer,
        ui: uiReducer,
        webmap: webmapReducer,
      },
      preloadedState: { ...timeSeriesStore, ...coreStore },
    };

    const store = createToolkitMockStoreWithEggs(mockState);
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelectConnect selectPlotId={plotId} />
      </TimeSeriesThemeStoreProvider>,
    );

    const OGCRow = await screen.findByText(service.name);
    fireEvent.click(OGCRow);

    expect(screen.getByText(temperatureParameter.propertyName)).toBeTruthy();

    const temperatureRow = screen.getByRole('listitem', {
      name: temperatureParameter.propertyName,
    });

    const removeTemperatureButton = within(temperatureRow).getByRole('button', {
      name: /Remove/i,
    });

    expect(store.getState().timeSeries.plotPreset.parameters).toHaveLength(1);

    fireEvent.click(removeTemperatureButton);

    expect(store.getState().timeSeries.plotPreset.parameters).toHaveLength(0);

    const temperatureRowNew = screen.getByRole('listitem', {
      name: temperatureParameter.propertyName,
    });
    await within(temperatureRowNew).findByText('Add');
    const addTemperatureButton = within(temperatureRowNew).getByRole('button', {
      name: /Add/i,
    });

    fireEvent.click(addTemperatureButton);

    const temperatureRowLast = screen.getByRole('listitem', {
      name: temperatureParameter.propertyName,
    });
    expect(
      within(temperatureRowLast).getByRole('button', {
        name: /Remove/i,
      }),
    ).toBeInTheDocument();
  });

  it('should fetch edr parameters and display them', async () => {
    const timeSeriesStore: TimeSeriesModuleState = {
      timeSeries: {
        plotPreset: {
          mapId: mockMapId,
          plots: [
            {
              title: 'Plot 1',
              plotId,
            },
          ],
          parameters: [],
        },
        services: [],
        timeseriesSelect: {
          filters: {
            searchFilter: '',
            serviceFilterChips: {
              entities: {
                [services[0].id]: {
                  serviceId: services[0].id,
                  type: services[0].type,
                  serviceUrl: services[0].url,
                  serviceName: services[0].name,
                  enabled: true,
                  scope: 'system',
                },
              },
              ids: [services[0].id],
            },
            allServiceFilterChipsEnabled: true,
          },
        },
      },
    };

    const mockState = {
      reducer: { timeSeries: reducer, ui: uiReducer, webmap: webmapReducer },
      preloadedState: { ...timeSeriesStore, ...coreStore },
    };
    const store = createToolkitMockStoreWithEggs(mockState);

    const geopHeight = 'GeopHeight';
    jest.spyOn(axios, 'get').mockResolvedValue({
      data: {
        collections: [
          {
            id: 'someCollection',
            title: 'title',
            description: 'description',
          },
        ],
        parameter_names: {
          [geopHeight]: {
            id: geopHeight,
            description: geopHeight,
            type: 'Parameter',
            observedProperty: {
              label: geopHeight,
            },
          },
        },
      },
    });
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelectConnect selectPlotId={plotId} />
      </TimeSeriesThemeStoreProvider>,
    );

    act(() => {
      store.dispatch(mapActions.registerMap({ mapId: mockMapId }));
      store.dispatch(
        timeSeriesActions.registerTimeSeriesPreset({
          plotPreset: {
            mapId: mockMapId,
            plots: [
              {
                title: 'Plot 1',
                plotId,
              },
            ],
            parameters: [],
          },
          services: [
            {
              id: 'id',
              type: 'EDR',
              url: 'serviceUrlEdrMock',
              description: 'desc',
              name: 'EDR Mock',
              scope: 'system',
            },
          ],
          timeseriesSelect: {
            filters: {
              searchFilter: '',
              serviceFilterChips: {
                entities: {
                  [services[0].id]: {
                    serviceId: services[0].id,
                    type: services[0].type,
                    serviceUrl: services[0].url,
                    serviceName: services[0].name,
                    enabled: true,
                    scope: 'system',
                  },
                },
                ids: [services[0].id],
              },
              allServiceFilterChipsEnabled: true,
            },
          },
        }),
      );
      store.dispatch(
        uiActions.setToggleOpenDialog({
          type: uiTypes.DialogTypes.TimeSeriesSelect,
          setOpen: true,
        }),
      );
    });

    await screen.findByText('someCollection');
    fireEvent.click(screen.getByText('someCollection'));

    const parameterRow = screen.getByRole('listitem', {
      name: geopHeight,
    });
    expect(
      within(parameterRow).getByRole('button', {
        name: /add/i,
      }),
    ).toBeInTheDocument();
  });

  it('should highlight and search', async () => {
    const parameterId = 'parameter1';
    const temperatureParameter = ogcParameters[0];
    const parameter2Id = 'parameter2';
    const otherParameter = ogcParameters[1];
    const service: TimeSeriesService = {
      id: temperatureParameter.serviceId,
      url: 'serviceUrl',
      type: 'OGC',
      description: 'desc',
      name: 'OGC',
      scope: 'system',
    };

    const timeSeriesStore: TimeSeriesModuleState = {
      timeSeries: {
        plotPreset: {
          mapId: mockMapId,
          plots: [
            {
              title: 'Plot 1',
              plotId,
            },
          ],
          parameters: [
            {
              id: parameterId,
              plotId,
              plotType: 'line',
              propertyName: temperatureParameter.propertyName,
              unit: temperatureParameter.unit,
              serviceId: service.id,
              collectionId: temperatureParameter.collectionId,
            },
            {
              id: parameter2Id,
              plotId,
              plotType: 'line',
              propertyName: otherParameter.propertyName,
              unit: otherParameter.unit,
              serviceId: service.id,
              collectionId: otherParameter.collectionId,
            },
          ],
        },
        services: [service],
        timeseriesSelect: {
          filters: {
            searchFilter: '',
            serviceFilterChips: {
              entities: {
                [service.id]: {
                  serviceId: service.id,
                  type: service.type,
                  serviceUrl: service.url,
                  serviceName: service.name,
                  enabled: true,
                  scope: 'system',
                },
              },
              ids: [service.id],
            },
            allServiceFilterChipsEnabled: true,
          },
        },
      },
    };
    const mockState = {
      reducer: {
        timeSeries: reducer,
        ui: uiReducer,
        webmap: webmapReducer,
      },
      preloadedState: { ...timeSeriesStore, ...coreStore },
    };

    const store = createToolkitMockStoreWithEggs(mockState);
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelectConnect selectPlotId={plotId} />
      </TimeSeriesThemeStoreProvider>,
    );

    // Expand the collection
    const row = await screen.findByText(temperatureParameter.collectionId);
    fireEvent.click(row);

    // Both parameters should be visible and not marked as searched
    expect(
      screen
        .getByText(temperatureParameter.propertyName)
        .nodeName.toLowerCase(),
    ).toBe('span');
    expect(
      screen.getByText(otherParameter.propertyName).nodeName.toLowerCase(),
    ).toBe('span');

    const searchInput = screen.getByRole('textbox', {
      name: /Search TS or enter a service URL/i,
    });

    expect(screen.queryByRole('button', { name: /Clear/i })).toBeFalsy();

    await fireEvent.change(searchInput, {
      target: { value: temperatureParameter.propertyName },
    });

    expect(screen.getByRole('button', { name: /Clear/i })).toBeTruthy();

    await waitFor(() => {
      expect(
        store.getState().timeSeries.timeseriesSelect.filters.searchFilter,
      ).toEqual(temperatureParameter.propertyName);
    });

    // Temperature parameter should be marked as searched and other parameter no longer visible
    expect(
      screen
        .getByText(temperatureParameter.propertyName)
        .nodeName.toLowerCase(),
    ).toBe('mark');
    expect(screen.queryByText(otherParameter.propertyName)).toBeFalsy();

    await fireEvent.change(searchInput, {
      target: { value: 'someRandom Search string' },
    });

    expect(screen.getByRole('button', { name: /Clear/i })).toBeTruthy();

    await waitFor(() => {
      expect(
        store.getState().timeSeries.timeseriesSelect.filters.searchFilter,
      ).toEqual('someRandom Search string');
    });

    // Both parameters should no longer be visible
    expect(await screen.findByText('0 results')).toBeTruthy();
    expect(screen.queryByText(temperatureParameter.propertyName)).toBeFalsy();
    expect(screen.queryByText(otherParameter.propertyName)).toBeFalsy();

    // Search for collection name
    await fireEvent.change(searchInput, {
      target: { value: temperatureParameter.collectionId },
    });

    expect(screen.getByRole('button', { name: /Clear/i })).toBeTruthy();

    await waitFor(() => {
      expect(
        store.getState().timeSeries.timeseriesSelect.filters.searchFilter,
      ).toEqual(temperatureParameter.collectionId);
    });

    expect(
      screen
        .getByText(temperatureParameter.collectionId)
        .nodeName.toLowerCase(),
    ).toBe('mark');
    // Since none of the parameters match the search, no parameters were found
    expect(await screen.findByText(NO_RESULTS_FOUND_MESSAGE)).toBeTruthy();
  });

  it('should filter out services that are not selected via the filter chips', async () => {
    const parameterId = 'parameter1';
    const temperatureParameter = ogcParameters[0];
    const parameter2Id = 'parameter2';
    const otherParameter = {
      ...ogcParameters[1],
      serviceId: 'serviceId2',
    };
    const service: TimeSeriesService = {
      id: temperatureParameter.serviceId,
      url: 'serviceUrl',
      type: 'OGC',
      description: 'desc',
      name: 'OGC',
    };

    const service2: TimeSeriesService = {
      id: 'serviceId2',
      url: 'serviceUrl2',
      type: 'OGC',
      description: 'desc2',
      name: 'OGC2',
    };

    const service3: TimeSeriesService = {
      id: 'serviceId3',
      url: 'serviceUrl3',
      type: 'OGC',
      description: 'desc3',
      name: 'OGC3',
    };

    const timeSeriesStore: TimeSeriesModuleState = {
      timeSeries: {
        plotPreset: {
          mapId: mockMapId,
          plots: [
            {
              title: 'Plot 1',
              plotId,
            },
          ],
          parameters: [
            {
              id: parameterId,
              plotId,
              plotType: 'line',
              propertyName: temperatureParameter.propertyName,
              unit: temperatureParameter.unit,
              serviceId: service.id,
              collectionId: temperatureParameter.collectionId,
            },
            {
              id: parameter2Id,
              plotId,
              plotType: 'line',
              propertyName: otherParameter.propertyName,
              unit: otherParameter.unit,
              serviceId: service.id,
              collectionId: otherParameter.collectionId,
            },
          ],
        },
        services: [service, service2, service3],
        timeseriesSelect: {
          filters: {
            searchFilter: '',
            serviceFilterChips: {
              entities: {
                [service.id]: {
                  serviceId: service.id,
                  type: service.type,
                  serviceUrl: service.url,
                  serviceName: service.name,
                  enabled: true,
                  scope: 'system',
                },
                [service2.id]: {
                  serviceId: service2.id,
                  type: service2.type,
                  serviceUrl: service2.url,
                  serviceName: service2.name,
                  enabled: true,
                  scope: 'system',
                },
                [service3.id]: {
                  serviceId: service3.id,
                  type: service3.type,
                  serviceUrl: service3.url,
                  serviceName: service3.name,
                  enabled: true,
                  scope: 'system',
                },
              },
              ids: [service.id, service2.id, service3.id],
            },
            allServiceFilterChipsEnabled: true,
          },
        },
      },
    };
    const mockState = {
      reducer: {
        timeSeries: reducer,
        ui: uiReducer,
        webmap: webmapReducer,
      },
      preloadedState: { ...timeSeriesStore, ...coreStore },
    };

    const store = createToolkitMockStoreWithEggs(mockState);
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelectConnect selectPlotId={plotId} />
      </TimeSeriesThemeStoreProvider>,
    );

    const user = userEvent.setup();

    // Expect to see service chips for both services
    const serviceChips = screen.getByTestId('serviceList');
    expect(within(serviceChips).getByText(service.name)).toBeTruthy();
    expect(within(serviceChips).getByText(service2.name)).toBeTruthy();
    expect(within(serviceChips).getByText(service3.name)).toBeTruthy();

    // We should see three collections (one for each service)
    const rows = await screen.findAllByText(temperatureParameter.collectionId);
    expect(rows.length).toBe(3);
    const timeseriesSelectListStart = screen.getByTestId(
      'timeseriesSelectList',
    );
    expect(
      within(timeseriesSelectListStart).getByText(service.name),
    ).toBeTruthy();
    expect(
      within(timeseriesSelectListStart).getByText(service2.name),
    ).toBeTruthy();
    expect(
      within(timeseriesSelectListStart).getByText(service3.name),
    ).toBeTruthy();

    // All 3 services are selected so "All" should be the chip that's selected
    expect(
      screen.getByRole('button', { name: 'All', pressed: true }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service.name, pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service2.name, pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service3.name, pressed: false }),
    ).toBeTruthy();

    // Now select the first service,

    const firstChip = screen.getByRole('button', {
      name: service.name,
      pressed: false,
    });
    await user.click(firstChip);

    // All should become unselected, only the first service row should still be there
    expect(
      await screen.findByRole('button', { name: 'All', pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service.name, pressed: true }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service2.name, pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service3.name, pressed: false }),
    ).toBeTruthy();

    const rows1 = screen.getAllByText(temperatureParameter.collectionId);
    expect(rows1.length).toBe(1);
    const timeseriesSelectList = screen.getByTestId('timeseriesSelectList');
    expect(within(timeseriesSelectList).getByText(service.name)).toBeTruthy();
    expect(within(timeseriesSelectList).queryByText(service2.name)).toBeFalsy();
    expect(within(timeseriesSelectList).queryByText(service3.name)).toBeFalsy();

    // Also select the second service
    const secondChip = screen.getByRole('button', {
      name: service2.name,
      pressed: false,
    });
    await user.click(secondChip);

    expect(
      await screen.findByRole('button', { name: service2.name, pressed: true }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: 'All', pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service.name, pressed: true }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service3.name, pressed: false }),
    ).toBeTruthy();

    const rows2 = screen.getAllByText(temperatureParameter.collectionId);
    expect(rows2.length).toBe(2);
    const timeseriesSelectList2 = screen.getByTestId('timeseriesSelectList');
    expect(within(timeseriesSelectList2).getByText(service.name)).toBeTruthy();
    expect(within(timeseriesSelectList2).getByText(service2.name)).toBeTruthy();
    expect(
      within(timeseriesSelectList2).queryByText(service3.name),
    ).toBeFalsy();

    // Then select the third service this should reselect All and both rows should become visible
    const thirdChip = screen.getByRole('button', {
      name: service3.name,
      pressed: false,
    });
    await user.click(thirdChip);

    expect(
      await screen.findByRole('button', { name: 'All', pressed: true }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service2.name, pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service.name, pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service3.name, pressed: false }),
    ).toBeTruthy();

    const rows3 = screen.getAllByText(temperatureParameter.collectionId);
    expect(rows3.length).toBe(3);
    const timeseriesSelectList3 = screen.getByTestId('timeseriesSelectList');
    expect(within(timeseriesSelectList3).getByText(service.name)).toBeTruthy();
    expect(within(timeseriesSelectList3).getByText(service2.name)).toBeTruthy();
    expect(within(timeseriesSelectList3).getByText(service3.name)).toBeTruthy();

    // Then select the third service again
    const thirdChipAgain = screen.getByRole('button', {
      name: service3.name,
      pressed: false,
    });
    await user.click(thirdChipAgain);

    expect(
      await screen.findByRole('button', { name: 'All', pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service2.name, pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service.name, pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service3.name, pressed: true }),
    ).toBeTruthy();

    const rows4 = screen.getAllByText(temperatureParameter.collectionId);
    expect(rows4.length).toBe(1);
    const timeseriesSelectList4 = screen.getByTestId('timeseriesSelectList');
    expect(within(timeseriesSelectList4).queryByText(service.name)).toBeFalsy();
    expect(
      within(timeseriesSelectList4).queryByText(service2.name),
    ).toBeFalsy();
    expect(within(timeseriesSelectList4).getByText(service3.name)).toBeTruthy();

    // Then select the second service
    const secondChipAgain = screen.getByRole('button', {
      name: service2.name,
      pressed: false,
    });
    await user.click(secondChipAgain);

    expect(
      await screen.findByRole('button', { name: 'All', pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service2.name, pressed: true }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service.name, pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service3.name, pressed: true }),
    ).toBeTruthy();

    const rows5 = screen.getAllByText(temperatureParameter.collectionId);
    expect(rows5.length).toBe(2);
    const timeseriesSelectList5 = screen.getByTestId('timeseriesSelectList');
    expect(within(timeseriesSelectList5).queryByText(service.name)).toBeFalsy();
    expect(within(timeseriesSelectList5).getByText(service2.name)).toBeTruthy();
    expect(within(timeseriesSelectList5).getByText(service3.name)).toBeTruthy();

    // Then unselect the second service
    const secondChipAgainUnselect = screen.getByRole('button', {
      name: service2.name,
      pressed: true,
    });
    await user.click(secondChipAgainUnselect);

    expect(
      await screen.findByRole('button', { name: 'All', pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service2.name, pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service.name, pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service3.name, pressed: true }),
    ).toBeTruthy();

    const rows6 = screen.getAllByText(temperatureParameter.collectionId);
    expect(rows6.length).toBe(1);
    const timeseriesSelectList6 = screen.getByTestId('timeseriesSelectList');
    expect(within(timeseriesSelectList6).queryByText(service.name)).toBeFalsy();
    expect(
      within(timeseriesSelectList6).queryByText(service2.name),
    ).toBeFalsy();
    expect(within(timeseriesSelectList6).getByText(service3.name)).toBeTruthy();

    // Then unselect the third service
    const thirdChipAgainUnselect = screen.getByRole('button', {
      name: service3.name,
      pressed: true,
    });
    await user.click(thirdChipAgainUnselect);

    expect(
      await screen.findByRole('button', { name: 'All', pressed: true }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service2.name, pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service.name, pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service3.name, pressed: false }),
    ).toBeTruthy();

    const rows7 = screen.getAllByText(temperatureParameter.collectionId);
    expect(rows7.length).toBe(3);
    const timeseriesSelectList7 = screen.getByTestId('timeseriesSelectList');
    expect(within(timeseriesSelectList7).getByText(service.name)).toBeTruthy();
    expect(within(timeseriesSelectList7).getByText(service2.name)).toBeTruthy();
    expect(within(timeseriesSelectList7).getByText(service3.name)).toBeTruthy();

    // Then select the first service again
    const firstChipAgain = screen.getByRole('button', {
      name: service.name,
      pressed: false,
    });
    await user.click(firstChipAgain);

    expect(
      await screen.findByRole('button', { name: 'All', pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service2.name, pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service.name, pressed: true }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service3.name, pressed: false }),
    ).toBeTruthy();

    const rows8 = screen.getAllByText(temperatureParameter.collectionId);
    expect(rows8.length).toBe(1);
    const timeseriesSelectList8 = screen.getByTestId('timeseriesSelectList');
    expect(
      within(timeseriesSelectList8).queryByText(service2.name),
    ).toBeFalsy();
    expect(
      within(timeseriesSelectList8).queryByText(service3.name),
    ).toBeFalsy();
    expect(within(timeseriesSelectList8).getByText(service.name)).toBeTruthy();

    // Then press All to select all again
    const allChip = screen.getByRole('button', {
      name: 'All',
      pressed: false,
    });
    await user.click(allChip);

    expect(
      await screen.findByRole('button', { name: 'All', pressed: true }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service2.name, pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service.name, pressed: false }),
    ).toBeTruthy();
    expect(
      screen.getByRole('button', { name: service3.name, pressed: false }),
    ).toBeTruthy();

    const rows9 = screen.getAllByText(temperatureParameter.collectionId);
    expect(rows9.length).toBe(3);
    const timeseriesSelectList9 = screen.getByTestId('timeseriesSelectList');
    expect(within(timeseriesSelectList9).getByText(service.name)).toBeTruthy();
    expect(within(timeseriesSelectList9).getByText(service2.name)).toBeTruthy();
    expect(within(timeseriesSelectList9).getByText(service3.name)).toBeTruthy();
  }, 40000);

  it('should show a list of services in the services dialog', async () => {
    const parameterId = 'parameter1';
    const temperatureParameter = ogcParameters[0];
    const parameter2Id = 'parameter2';
    const otherParameter = {
      ...ogcParameters[1],
      serviceId: 'serviceId3',
    };
    const service: TimeSeriesService = {
      id: temperatureParameter.serviceId,
      url: 'serviceUrl',
      type: 'OGC',
      description: 'desc',
      name: 'OGC',
      scope: 'system',
    };

    const service2: TimeSeriesService = {
      id: 'serviceId2',
      url: 'serviceUrl2',
      type: 'OGC',
      description: 'desc2',
      name: 'OGC2',
      scope: 'user',
    };

    const service3: TimeSeriesService = {
      id: 'serviceId3',
      url: 'serviceUrl3',
      type: 'OGC',
      description: 'desc3',
      name: 'OGC3',
      scope: 'user',
    };

    const timeSeriesStore: TimeSeriesModuleState = {
      timeSeries: {
        plotPreset: {
          mapId: mockMapId,
          plots: [
            {
              title: 'Plot 1',
              plotId,
            },
          ],
          parameters: [
            {
              id: parameterId,
              plotId,
              plotType: 'line',
              propertyName: temperatureParameter.propertyName,
              unit: temperatureParameter.unit,
              serviceId: service.id,
              collectionId: temperatureParameter.collectionId,
            },
            {
              id: parameter2Id,
              plotId,
              plotType: 'line',
              propertyName: otherParameter.propertyName,
              unit: otherParameter.unit,
              serviceId: otherParameter.serviceId,
              collectionId: otherParameter.collectionId,
            },
          ],
        },
        services: [service, service2, service3],
        timeseriesSelect: {
          filters: {
            searchFilter: '',
            serviceFilterChips: {
              entities: {
                [service.id]: {
                  serviceId: service.id,
                  type: service.type,
                  serviceUrl: service.url,
                  serviceName: service.name,
                  enabled: true,
                  scope: 'system',
                },
                [service2.id]: {
                  serviceId: service2.id,
                  type: service2.type,
                  serviceUrl: service2.url,
                  serviceName: service2.name,
                  enabled: true,
                  scope: 'system',
                },
                [service3.id]: {
                  serviceId: service3.id,
                  type: service3.type,
                  serviceUrl: service3.url,
                  serviceName: service3.name,
                  enabled: true,
                  scope: 'system',
                },
              },
              ids: [service.id, service2.id, service3.id],
            },
            allServiceFilterChipsEnabled: true,
          },
        },
      },
    };
    const mockState = {
      reducer: {
        timeSeries: reducer,
        ui: uiReducer,
        webmap: webmapReducer,
      },
      preloadedState: { ...timeSeriesStore, ...coreStore },
    };

    const store = createToolkitMockStoreWithEggs(mockState);
    render(
      <TimeSeriesThemeStoreProvider store={store}>
        <TimeSeriesSelectConnect selectPlotId={plotId} />
      </TimeSeriesThemeStoreProvider>,
    );

    const user = userEvent.setup();

    // We should see three collections (one for each service)
    const rows = await screen.findAllByText(temperatureParameter.collectionId);
    expect(rows.length).toBe(3);
    const timeseriesSelectListStart = screen.getByTestId(
      'timeseriesSelectList',
    );
    expect(
      within(timeseriesSelectListStart).getByText(service.name),
    ).toBeTruthy();
    expect(
      within(timeseriesSelectListStart).getByText(service2.name),
    ).toBeTruthy();
    expect(
      within(timeseriesSelectListStart).getByText(service3.name),
    ).toBeTruthy();

    // Open the service options dialog and check all services are listed
    await user.click(screen.getByTestId('toggleMenuButton'));

    const serviceOptionsDialog = screen.getByRole('menu');
    expect(within(serviceOptionsDialog).getByText(service.name)).toBeTruthy();
    expect(within(serviceOptionsDialog).getByText(service2.name)).toBeTruthy();
    expect(within(serviceOptionsDialog).getByText(service3.name)).toBeTruthy();

    // Press Esc to close the dialog
    await user.keyboard('{Escape}');
    expect(screen.queryByRole('menu')).toBeFalsy();
  });
});
