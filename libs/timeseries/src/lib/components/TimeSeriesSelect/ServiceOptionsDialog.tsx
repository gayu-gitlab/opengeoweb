/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  Box,
  List,
  ListItem,
  ListItemText,
  ListItemSecondaryAction,
  Button,
  Typography,
  Grid,
} from '@mui/material';

import {
  CustomIconButton,
  CustomTooltip,
  TimeSeriesService,
} from '@opengeoweb/shared';
import { Add, Delete, Edit, Visibility, Cached } from '@opengeoweb/theme';
import { Parameter } from '../TimeSeries/types';

export const sortByService = (
  serviceObj: TimeSeriesService[],
): TimeSeriesService[] => {
  // Need to make a copy as we are not allowed to mutate the original serviceObj and .sort mutates
  const newServiceObj = serviceObj.slice();
  return newServiceObj.sort((a, b) => a!.name!.localeCompare(b?.name!));
};

interface ServiceOptionsDialogProps {
  services: TimeSeriesService[] | undefined;
  parametersInUse: Parameter[];
}

const styles = {
  servicesContainer: {
    width: '360px',
    maxHeight: '412px',
    top: '140px',
    backgroundColor: 'geowebColors.background.surface',
    boxShadow: 6,
    overflow: 'auto',
  },
  header: {
    position: 'sticky',
    top: 0,
    fontSize: 'default',
    padding: '12px',
    backgroundColor: 'geowebColors.background.surface',
    zIndex: 100,
  },
  loading: {
    position: 'absolute',
    zIndex: 101,
  },
  footer: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'sticky',
    bottom: 0,
    backgroundColor: 'geowebColors.background.surface',
    zIndex: 100,
  },
  button: {
    margin: '16px',
    width: '80%',
    fontSize: '12px',
    textTransform: 'none',
  },
};

const stopTabPropagation = (event: React.KeyboardEvent): void => {
  if (event.key === 'Tab') {
    event.stopPropagation();
  }
};

export const ServiceOptionsDialog: React.FC<ServiceOptionsDialogProps> = ({
  services,
  parametersInUse,
}: ServiceOptionsDialogProps) => {
  const dialogRef = React.useRef<HTMLElement | null>(null);

  React.useEffect(() => {
    dialogRef.current?.focus();
  }, []);

  if (!services || services.length === 0) {
    return (
      <Box
        sx={{
          backgroundColor: 'geowebColors.background.surface',
          boxShadow: 6,
          padding: 1,
        }}
        onKeyDown={(event): void => {
          if (event.key !== 'Escape') {
            event.stopPropagation();
          }
        }}
        tabIndex={-1}
        ref={dialogRef}
      >
        No services found
      </Box>
    );
  }

  return (
    <Box
      sx={styles.servicesContainer}
      onKeyDown={(event): void => {
        if (event.key !== 'Escape') {
          event.stopPropagation();
        }
      }}
      tabIndex={-1}
      ref={dialogRef}
      aria-label="Service options dialog for timeseries"
    >
      <Box sx={styles.header}>Services</Box>
      <Rows services={services} parametersInUse={parametersInUse} />
      <Box sx={styles.footer}>
        <Button
          onClick={(): void => {}}
          onKeyDown={stopTabPropagation}
          sx={styles.button}
          variant="tertiary"
          startIcon={<Add />}
          disabled
        >
          Add a new TS service
        </Button>
      </Box>
    </Box>
  );
};

const updateTSServiceTitle =
  'Update collections and parameters from TS service';
const editTSServiceTitle = 'Edit Service';
const showTSServiceTitle = 'Show Service';

const deleteServiceTitle = (hasActiveLayers: boolean): string => {
  return hasActiveLayers
    ? 'Service has active layers. Cannot remove this service'
    : 'Delete Service';
};

const tooltipPopperProps = {
  disablePortal: true,
  sx: {
    '& .MuiTooltip-tooltip': {
      width: '200px',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
    },
  },
};

const hasServiceParmetersInUse = (
  serviceId: string,
  parametersInUse: Parameter[],
): boolean => {
  return parametersInUse.some((parameter) => parameter.serviceId === serviceId);
};

const Rows: React.FC<{
  services: TimeSeriesService[];
  parametersInUse: Parameter[];
}> = ({ services, parametersInUse }) => {
  const sortedServices = sortByService(services);

  return (
    <List disablePadding>
      {sortedServices.map((service) => {
        const { url, name, id, scope } = service;
        const serviceHasParametersInUse = hasServiceParmetersInUse(
          id,
          parametersInUse,
        );
        const userAddedService = scope === 'user';

        return (
          <ListItem key={url}>
            <ListItemText
              style={{
                fontSize: '16px',
              }}
            >
              <Typography
                noWrap
                style={{
                  textOverflow: 'ellipsis',
                  paddingRight: '70px',
                }}
              >
                {name || id}
              </Typography>
            </ListItemText>
            <ListItemSecondaryAction
              sx={{
                marginRight: '0px',
                width: 96,
                button: { marginLeft: 1 },
              }}
            >
              <Grid container justifyContent="left">
                <CustomTooltip
                  title={updateTSServiceTitle}
                  placement="left"
                  PopperProps={tooltipPopperProps}
                >
                  <span>
                    <CustomIconButton
                      aria-label={updateTSServiceTitle}
                      onKeyDown={stopTabPropagation}
                      onClick={(): void => {}}
                      disabled
                    >
                      <Cached />
                    </CustomIconButton>
                  </span>
                </CustomTooltip>
                {userAddedService ? (
                  <CustomTooltip
                    title={editTSServiceTitle}
                    placement="left"
                    PopperProps={tooltipPopperProps}
                  >
                    <span>
                      <CustomIconButton
                        aria-label="Edit TS service"
                        onKeyDown={stopTabPropagation}
                        onClick={(): void => {}}
                        disabled
                      >
                        <Edit />
                      </CustomIconButton>
                    </span>
                  </CustomTooltip>
                ) : (
                  <CustomTooltip
                    title={showTSServiceTitle}
                    placement="left"
                    PopperProps={tooltipPopperProps}
                  >
                    <span>
                      <CustomIconButton
                        aria-label="Show TS service"
                        onKeyDown={stopTabPropagation}
                        onClick={(): void => {}}
                        disabled
                      >
                        <Visibility />
                      </CustomIconButton>
                    </span>
                  </CustomTooltip>
                )}
                {userAddedService && (
                  <Box>
                    <CustomTooltip
                      title={deleteServiceTitle(serviceHasParametersInUse)}
                      PopperProps={tooltipPopperProps}
                      placement="left"
                    >
                      <span>
                        <CustomIconButton
                          aria-label="Delete TS service"
                          disabled
                          // disabled={serviceHasParametersInUse}
                          onKeyDown={stopTabPropagation}
                          onClick={(): void => {}}
                        >
                          <Delete />
                        </CustomIconButton>
                      </span>
                    </CustomTooltip>
                  </Box>
                )}
              </Grid>
            </ListItemSecondaryAction>
          </ListItem>
        );
      })}
    </List>
  );
};
