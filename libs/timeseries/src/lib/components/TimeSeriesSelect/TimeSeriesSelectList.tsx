/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  Box,
  Collapse,
  ListItem,
  ListItemButton,
  List as MuiList,
  ListItemText,
  Grid,
  SxProps,
} from '@mui/material';
import {
  CustomToggleButton,
  SearchHighlight,
  getHeight,
} from '@opengeoweb/shared';
import { VariableSizeList as List, VariableSizeList } from 'react-window';
import React, { FC } from 'react';
import { ArrowDropDownDown, ArrowDropDownUp } from '@opengeoweb/theme';
import { Parameter, Plot } from '../TimeSeries/types';
import {
  TimeseriesCollectionDetail,
  filterCollectionsParametersFromService,
} from './utils';
import { ServiceFilterChipsObjectEntities } from '../../store/types';

export const NO_RESULTS_FOUND_MESSAGE = 'No parameters found';
export const DEFAULT_DIALOG_HEIGHT = 500;
const MIN_HEIGHT_SCROLLABLE_LIST = 90; // Close to two list rows
const DEFAULT_PADDING_TOP_BOTTOM = 12;
const DEFAULT_RESULTS_HEIGHT = 22;

const DEFAULT_COLLECTION_ROW_HEIGHT = 48; // listItem height + 4px marginTop
const DEFAULT_PARAMETER_ROW_HEIGHT = 34; // listItem height
const DEFAULT_PARAMTER_LIST_PADDING_BOTTOM = 4;
const DEFAULT_PARAMTER_LIST_BORDER_BOTTOM = 1;

const useGetRequiredHeights = (): {
  listBoxRef: React.RefObject<HTMLDivElement>;
  listHeight: number;
} => {
  const listBoxRef = React.useRef<HTMLDivElement>(null);

  // Used to store the height of the list dialog content
  const [dialogHeight, setDialogHeight] = React.useState<number>(
    DEFAULT_DIALOG_HEIGHT,
  );
  // Used to store the height of the filter and search elements
  const [filterSearchHeight, setFilterSearchHeight] = React.useState<number>(
    DEFAULT_DIALOG_HEIGHT,
  );

  // Obtain the height of of the list dialog content
  React.useLayoutEffect(() => {
    const dialogElement = listBoxRef.current?.parentElement?.parentElement
      ?.parentElement as HTMLDivElement;
    dialogElement?.offsetHeight && setDialogHeight(dialogElement?.offsetHeight);

    const observer = new ResizeObserver(() => {
      const height = getHeight(dialogElement);
      height && setDialogHeight(height);
    });
    observer.observe(dialogElement);

    return (): void => {
      observer.disconnect();
    };
  }, [listBoxRef]);

  // Obtain the height of the filters and serach
  React.useLayoutEffect(() => {
    const filterSearchElement = listBoxRef.current?.parentElement?.parentElement
      ?.firstChild as HTMLDivElement;
    filterSearchElement?.offsetHeight &&
      setFilterSearchHeight(filterSearchElement?.offsetHeight);

    const observer = new ResizeObserver(() => {
      const height = getHeight(filterSearchElement);
      height && setFilterSearchHeight(height);
    });
    observer.observe(filterSearchElement);

    return (): void => {
      observer.disconnect();
    };
  }, [listBoxRef]);

  const availableSpaceForList =
    dialogHeight -
    filterSearchHeight -
    DEFAULT_PADDING_TOP_BOTTOM -
    DEFAULT_RESULTS_HEIGHT;
  const listHeight =
    availableSpaceForList && availableSpaceForList < MIN_HEIGHT_SCROLLABLE_LIST
      ? MIN_HEIGHT_SCROLLABLE_LIST
      : availableSpaceForList;

  return {
    listBoxRef,
    listHeight,
  };
};

const fontStyling = (primary = true): SxProps => {
  return {
    fontSize: '12px',
    height: '14px',
    lineHeight: '14px',
    fontWeight: primary ? 500 : 'normal',
    textOverflow: 'ellipsis',
    overflow: 'hidden',
  };
};

interface TimeSeriesSelectListProps {
  selectPlot: Plot;
  collectionListItemDetails: Record<string, boolean>;
  collectionList: TimeseriesCollectionDetail[];
  setCollectionListItemDetails: (newList: Record<string, boolean>) => void;
  handleAddOrRemoveClick: (
    parameter: Parameter,
    plotHasParameter: boolean,
  ) => void;
  searchFilter: string;
  filterChipsServices: ServiceFilterChipsObjectEntities;
  allFilterChipsEnabled: boolean;
}

export const TimeSeriesSelectList: FC<TimeSeriesSelectListProps> = ({
  selectPlot,
  collectionListItemDetails,
  collectionList,
  handleAddOrRemoveClick,
  setCollectionListItemDetails,
  searchFilter,
  filterChipsServices,
  allFilterChipsEnabled,
}) => {
  const listRef = React.useRef<VariableSizeList>(null);
  const [filteredCollectionList, setFilteredCollectionList] = React.useState<
    TimeseriesCollectionDetail[]
  >([]);
  // Used to store the height of the list dialog content
  const { listBoxRef, listHeight } = useGetRequiredHeights();

  React.useEffect(() => {
    const filterResults = filterCollectionsParametersFromService(
      collectionList,
      searchFilter,
      filterChipsServices,
      allFilterChipsEnabled,
    );
    setFilteredCollectionList(filterResults);
    // Needed to recalculate the height of the list elements now that some collections/parameters have been filtered out
    listRef?.current?.resetAfterIndex(0);
  }, [
    searchFilter,
    collectionList,
    filterChipsServices,
    allFilterChipsEnabled,
  ]);

  return (
    <Box data-testid="timeseriesSelectList">
      <Box sx={{ marginBottom: '4px', fontSize: '12px' }}>
        {filteredCollectionList.length} results
      </Box>
      <Box ref={listBoxRef}>
        <List
          ref={listRef}
          height={listHeight}
          itemCount={filteredCollectionList.length}
          itemSize={(index: number): number => {
            const { collectionId } = filteredCollectionList[index];

            if (!collectionListItemDetails[collectionId]) {
              return DEFAULT_COLLECTION_ROW_HEIGHT;
            }
            if (filteredCollectionList[index].parameters.length < 1) {
              return (
                DEFAULT_PARAMETER_ROW_HEIGHT +
                DEFAULT_COLLECTION_ROW_HEIGHT +
                DEFAULT_PARAMTER_LIST_PADDING_BOTTOM +
                DEFAULT_PARAMTER_LIST_BORDER_BOTTOM
              );
            }
            return (
              filteredCollectionList[index].parameters.length *
                DEFAULT_PARAMETER_ROW_HEIGHT +
              DEFAULT_COLLECTION_ROW_HEIGHT +
              DEFAULT_PARAMTER_LIST_PADDING_BOTTOM +
              DEFAULT_PARAMTER_LIST_BORDER_BOTTOM
            );
          }}
          width="100%"
        >
          {({ index, style }): JSX.Element => {
            const collectionListItem = filteredCollectionList[index];
            const { collectionId } = collectionListItem;

            return (
              <Box style={style}>
                <ListItem
                  sx={{
                    backgroundColor: 'geowebColors.cards.cardContainer',
                    height: '44px',
                    marginTop: 0.5,
                    borderWidth: '1px',
                    borderStyle: collectionListItemDetails[collectionId]
                      ? 'solid solid none solid'
                      : 'solid',
                    borderRadius: '3px',
                    padding: 0,
                    borderColor: 'geowebColors.cards.cardContainerBorder',
                    '.MuiListItemText-primary': {
                      fontSize: '12px',
                      fontWeight: 500,
                      textOverflow: 'ellipsis',
                      overflow: 'hidden',
                    },
                  }}
                >
                  <ListItemButton
                    sx={{
                      height: '100%',
                      padding: 0,
                    }}
                    onClick={(): void => {
                      const currentState =
                        collectionListItemDetails[collectionId];
                      setCollectionListItemDetails({
                        ...collectionListItemDetails,
                        [collectionId]: !currentState,
                      });
                      listRef?.current?.resetAfterIndex(index);
                    }}
                  >
                    {collectionListItemDetails[collectionId] ? (
                      <ArrowDropDownUp />
                    ) : (
                      <ArrowDropDownDown />
                    )}
                    <Grid
                      container
                      justifyContent="space-between"
                      alignItems="center"
                    >
                      <Grid item>
                        <Box sx={fontStyling()}>
                          <SearchHighlight
                            text={collectionListItem.collectionId}
                            search={searchFilter}
                          />
                        </Box>

                        <Box sx={fontStyling(false)}>
                          <SearchHighlight
                            text={collectionListItem.serviceName}
                            search={searchFilter}
                          />
                        </Box>
                      </Grid>
                      <Grid item>
                        <ListItemText
                          sx={{
                            paddingLeft: 1,
                            paddingRight: 1,
                            textAlign: 'right',
                          }}
                          primary={`${collectionListItem.parameters.length} results`}
                        />
                      </Grid>
                    </Grid>
                  </ListItemButton>
                </ListItem>
                <Collapse
                  in={collectionListItemDetails[collectionId]}
                  unmountOnExit
                >
                  <MuiList
                    sx={{
                      paddingTop: 0,
                      paddingBottom: `${DEFAULT_PARAMTER_LIST_PADDING_BOTTOM}px`,
                      backgroundColor: 'geowebColors.cards.cardContainer',
                      borderWidth: `${DEFAULT_PARAMTER_LIST_BORDER_BOTTOM}px`,
                      borderStyle: 'none solid solid solid',
                      borderColor: 'geowebColors.cards.cardContainerBorder',
                      borderBottomLeftRadius: '3px',
                      borderBottomRightRadius: '3px',
                      boxShadow: 1,
                    }}
                  >
                    {collectionListItem?.parameters.length < 1 && (
                      <ListItem
                        sx={{
                          height: `${DEFAULT_PARAMETER_ROW_HEIGHT}px`,
                          minHeight: '32px',
                          paddingRight: '4px',
                          ' .MuiListItemText-primary': {
                            fontSize: '12px',
                            fontWeight: 500,
                            textOverflow: 'ellipsis',
                            overflow: 'hidden',
                          },
                        }}
                        role="listitem"
                      >
                        <ListItemText
                          sx={{
                            paddingLeft: 1,
                            paddingRight: 1,
                          }}
                          secondary={NO_RESULTS_FOUND_MESSAGE}
                        />
                      </ListItem>
                    )}
                    {collectionListItem.parameters.map((parameter) => {
                      const plotParameter = selectPlot.parameters!.find(
                        (plotParameter) =>
                          collectionListItem.serviceId ===
                            plotParameter.serviceId &&
                          collectionListItem.collectionId ===
                            plotParameter.collectionId &&
                          parameter.propertyName === plotParameter.propertyName,
                      );
                      const plotHasParameter = Boolean(plotParameter);
                      return (
                        <ListItem
                          sx={{
                            height: `${DEFAULT_PARAMETER_ROW_HEIGHT}px`,
                            minHeight: '32px',
                            paddingRight: '4px',
                          }}
                          key={parameter.propertyName}
                          role="listitem"
                          aria-label={parameter.propertyName}
                        >
                          <Grid
                            container
                            justifyContent="space-between"
                            alignItems="center"
                          >
                            <Grid item>
                              <Box sx={fontStyling()}>
                                <SearchHighlight
                                  text={parameter.propertyName}
                                  search={searchFilter}
                                />
                              </Box>
                            </Grid>
                            <Grid item>
                              <Box
                                sx={{
                                  marginRight: '8px',
                                  marginTop: '4px',
                                }}
                              >
                                <CustomToggleButton
                                  sx={{
                                    width: '60px',
                                    height: '24px !important',
                                  }}
                                  variant="tool"
                                  fullWidth={true}
                                  selected={plotHasParameter}
                                  onClick={(): void => {
                                    const parameterWithPlotId: Parameter = {
                                      ...parameter,
                                      plotId: selectPlot.plotId,
                                      id: plotParameter?.id,
                                    };
                                    handleAddOrRemoveClick(
                                      parameterWithPlotId,
                                      plotHasParameter,
                                    );
                                  }}
                                >
                                  {plotHasParameter ? 'Remove' : 'Add'}
                                </CustomToggleButton>
                              </Box>
                            </Grid>
                          </Grid>
                        </ListItem>
                      );
                    })}
                  </MuiList>
                </Collapse>
              </Box>
            );
          }}
        </List>
      </Box>
    </Box>
  );
};
