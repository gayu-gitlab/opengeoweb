/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { produce } from 'immer';
import { TimeSeriesService } from '@opengeoweb/shared';
import { generateServiceId, initialState, reducer } from './reducer';
import { Parameter, PlotPreset } from '../components/TimeSeries/types';
import { ServiceFilterChipsType, TimeSeriesStoreType } from './types';
import { mockTimeSeriesServices } from '../components/TimeSeries/mockTimeSeriesServices';
import { timeSeriesActions } from '.';

const {
  deletePlot,
  registerTimeSeriesPreset,
  togglePlot,
  deleteParameter,
  addParameter,
  toggleParameter,
  addPlot,
  updateParameter,
  setSearchFilter,
  setCurrentParameterInfoDisplayed,
  addServices,
  updateTitle,
  moveParameter,
} = timeSeriesActions;

describe('store/reducer', () => {
  it('should return initial state if no state and action passed in', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore can't trigger empty actions (yet)
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  const plot1 = { plotId: 'plotId1', title: 'Plot 1' };
  const plot2 = { plotId: 'plotId2', title: 'Plot 2' };
  const parameter1: Parameter = {
    id: 'parameter1',
    plotId: plot1.plotId,
    unit: '°C',
    propertyName: 'Temperature',
    plotType: 'line',
    serviceId: mockTimeSeriesServices[0].id,
    collectionId: 'ecmwf',
    opacity: 70,
  };

  const plotPreset: PlotPreset = {
    mapId: 'TimeseriesMap',
    plots: [plot1, plot2],
    parameters: [
      parameter1,
      {
        id: 'parameter2',
        plotId: plot2.plotId,
        unit: 'mm',
        propertyName: 'Precipitation1h',
        plotType: 'bar',
        serviceId: mockTimeSeriesServices[0].id,
        collectionId: 'ecmwf',
        opacity: 70,
      },
    ],
  };

  const serviceFilterChipStore: ServiceFilterChipsType = {
    entities: {
      [mockTimeSeriesServices[0].id]: {
        serviceId: mockTimeSeriesServices[0].id,
        type: mockTimeSeriesServices[0].type,
        serviceUrl: mockTimeSeriesServices[0].url,
        serviceName: mockTimeSeriesServices[0].name,
        enabled: true,
        scope: 'system',
        isLoading: false,
      },
    },
    ids: [mockTimeSeriesServices[0].id],
  };

  const mockTimeSeriesState: TimeSeriesStoreType = {
    plotPreset,
    services: mockTimeSeriesServices,
    timeseriesSelect: {
      filters: {
        searchFilter: '',
        serviceFilterChips: serviceFilterChipStore,
        allServiceFilterChipsEnabled: true,
      },
    },
  };

  it('should register plot', () => {
    const expectedState = produce(mockTimeSeriesState, (draft) => {
      draft.plotPreset!.parameters.forEach((draftParameter) => {
        draftParameter.id = expect.any(String);
      });
    });
    expect(
      reducer(
        undefined,
        registerTimeSeriesPreset({
          plotPreset,
          services: mockTimeSeriesServices,
          timeseriesSelect: {
            filters: {
              searchFilter: '',
              allServiceFilterChipsEnabled: true,
              serviceFilterChips: {
                entities: {},
                ids: [],
              },
            },
          },
        }),
      ),
    ).toEqual(expectedState);
  });

  it('should register an extra service', () => {
    const testService = {
      id: 'test',
      url: 'test',
      description: 'test',
      type: 'OGC',
      scope: 'system',
      name: 'name',
    } as TimeSeriesService;

    const testService2 = {
      id: 'test1',
      url: 'testWithDifferentUrl',
      description: 'test',
      type: 'OGC',
      scope: 'system',
      name: 'name1',
    } as TimeSeriesService;

    const state = {
      plotPreset,
      services: [...mockTimeSeriesServices, testService, testService2],
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: {
            entities: {
              [mockTimeSeriesServices[0].id]: {
                serviceId: mockTimeSeriesServices[0].id,
                type: mockTimeSeriesServices[0].type,
                serviceUrl: mockTimeSeriesServices[0].url,
                serviceName: mockTimeSeriesServices[0].name,
                enabled: true,
                scope: 'system',
                isLoading: false,
              },
              [testService.id]: {
                serviceId: testService.id,
                type: testService.type,
                serviceUrl: testService.url,
                serviceName: testService.name,
                enabled: true,
                scope: 'system',
                isLoading: false,
              },
              [testService2.id]: {
                serviceId: testService2.id,
                type: testService2.type,
                serviceUrl: testService2.url,
                serviceName: testService2.name,
                enabled: true,
                scope: 'system',
                isLoading: false,
              },
            },
            ids: [
              mockTimeSeriesServices[0].id,
              testService.id,
              testService2.id,
            ],
          },
          allServiceFilterChipsEnabled: true,
        },
      },
    };
    const expectedState = produce(state, (draft) => {
      draft.plotPreset!.parameters.forEach((draftParameter) => {
        draftParameter.id = expect.any(String);
      });
    });

    const store: TimeSeriesStoreType = {
      plotPreset: {
        mapId: 'TimeseriesMap',
        plots: [plot1],
        parameters: [parameter1],
      },
      services: [...mockTimeSeriesServices, testService],
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: {
            entities: {
              [mockTimeSeriesServices[0].id]: {
                serviceId: mockTimeSeriesServices[0].id,
                type: mockTimeSeriesServices[0].type,
                serviceUrl: mockTimeSeriesServices[0].url,
                serviceName: mockTimeSeriesServices[0].name,
                enabled: true,
                isLoading: false,
                scope: 'system',
              },
              [testService.id]: {
                serviceId: testService.id,
                type: testService.type,
                serviceUrl: testService.url,
                serviceName: testService.name,
                enabled: true,
                isLoading: false,
                scope: 'system',
              },
            },
            ids: [mockTimeSeriesServices[0].id, testService.id],
          },
          allServiceFilterChipsEnabled: true,
        },
      },
    };
    const newState = reducer(
      store,
      registerTimeSeriesPreset({
        plotPreset,
        services: [...mockTimeSeriesServices, testService2],
      }),
    );
    expect(newState).toEqual(expectedState);
  });

  it('should adjust parameter service id if the service was already in the store', () => {
    const testServiceWhichShouldNotBeAdded = {
      id: 'test',
      url: mockTimeSeriesServices[0].url,
      description: 'test',
      type: 'OGC',
    } as TimeSeriesService;

    const store: TimeSeriesStoreType = {
      plotPreset: {
        mapId: 'TimeseriesMap',
        plots: [plot1],
        parameters: [
          {
            id: 'parameter1',
            plotId: plot1.plotId,
            unit: '°C',
            propertyName: 'Temperature',
            plotType: 'line',
            serviceId: mockTimeSeriesServices[0].id,
            collectionId: 'ecmwf',
          },
        ],
      },
      services: [...mockTimeSeriesServices],
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
      },
    };
    const newState = reducer(
      store,
      registerTimeSeriesPreset({
        plotPreset: {
          mapId: 'TimeseriesMap',
          plots: [plot1],
          parameters: [
            {
              id: 'parameter1',
              plotId: plot1.plotId,
              unit: '°C',
              propertyName: 'Temperature',
              plotType: 'line',
              serviceId: testServiceWhichShouldNotBeAdded.id,
              collectionId: 'ecmwf',
            },
          ],
        },
        services: [testServiceWhichShouldNotBeAdded],
        timeseriesSelect: {
          filters: {
            searchFilter: '',
            serviceFilterChips: {
              entities: {
                [testServiceWhichShouldNotBeAdded.id]: {
                  serviceId: testServiceWhichShouldNotBeAdded.id,
                  type: testServiceWhichShouldNotBeAdded.type,
                  serviceUrl: testServiceWhichShouldNotBeAdded.url,
                  serviceName: testServiceWhichShouldNotBeAdded.name,
                  enabled: true,
                  scope: 'system',
                },
              },
              ids: [testServiceWhichShouldNotBeAdded.id],
            },
            allServiceFilterChipsEnabled: true,
          },
        },
      }),
    );
    // The existing mockTimeSeriesServices should have been re-used in the parameter id
    expect(newState.plotPreset?.parameters[0].serviceId).toEqual(
      mockTimeSeriesServices[0].id,
    );
  });

  it('should register an extra service with a new id if the service id was existing and the url is different', () => {
    const testService = {
      id: 'test',
      url: 'test',
      description: 'test',
      type: 'OGC',
      scope: 'system',
      name: 'name',
    } as TimeSeriesService;

    const testService2 = {
      id: 'test',
      url: 'testWithDifferentUrl',
      description: 'test',
      type: 'OGC',
      scope: 'system',
      name: 'name2',
    } as TimeSeriesService;

    const newServiceId = 'timeseriesserviceid_1'; // This is the id for testService2

    const state = {
      plotPreset,
      services: [...mockTimeSeriesServices, testService, testService2],
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: {
            entities: {
              [mockTimeSeriesServices[0].id]: {
                serviceId: mockTimeSeriesServices[0].id,
                type: mockTimeSeriesServices[0].type,
                serviceUrl: mockTimeSeriesServices[0].url,
                serviceName: mockTimeSeriesServices[0].name,
                enabled: true,
                isLoading: false,
                scope: 'system',
              },
              [testService.id]: {
                serviceId: testService.id,
                type: testService.type,
                serviceUrl: testService.url,
                serviceName: testService.name,
                enabled: true,
                isLoading: false,
                scope: 'system',
              },
              [newServiceId]: {
                serviceId: newServiceId,
                type: testService2.type,
                serviceUrl: testService2.url,
                serviceName: testService2.name,
                enabled: true,
                isLoading: false,
                scope: 'system',
              },
            },
            ids: [mockTimeSeriesServices[0].id, testService.id, newServiceId],
          },
          allServiceFilterChipsEnabled: true,
        },
      },
    };
    const expectedState = produce(state, (draft) => {
      draft.plotPreset!.parameters.forEach((draftParameter) => {
        draftParameter.id = expect.any(String);
      });
      draft.services[2].id = newServiceId;
    });

    const store: TimeSeriesStoreType = {
      plotPreset: {
        mapId: 'TimeseriesMap',
        plots: [plot1],
        parameters: [parameter1],
      },
      services: [...mockTimeSeriesServices, testService],
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: {
            entities: {
              [mockTimeSeriesServices[0].id]: {
                serviceId: mockTimeSeriesServices[0].id,
                type: mockTimeSeriesServices[0].type,
                serviceUrl: mockTimeSeriesServices[0].url,
                serviceName: mockTimeSeriesServices[0].name,
                enabled: true,
                isLoading: false,
                scope: 'system',
              },
              [testService.id]: {
                serviceId: testService.id,
                type: testService.type,
                serviceUrl: testService.url,
                serviceName: testService.name,
                enabled: true,
                isLoading: false,
                scope: 'system',
              },
            },
            ids: [mockTimeSeriesServices[0].id, testService.id],
          },
          allServiceFilterChipsEnabled: true,
        },
      },
    };
    const newState = reducer(
      store,
      registerTimeSeriesPreset({
        plotPreset,
        services: [...mockTimeSeriesServices, testService2],
      }),
    );
    expect(newState).toEqual(expectedState);
  });

  it('should delete plot', () => {
    const expectedState: TimeSeriesStoreType = {
      plotPreset: {
        mapId: 'TimeseriesMap',
        plots: [plot1],
        parameters: [parameter1],
      },
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
      },
    };
    expect(
      reducer(
        {
          plotPreset,
          services: mockTimeSeriesServices,
          timeseriesSelect: {
            filters: {
              searchFilter: '',
              serviceFilterChips: serviceFilterChipStore,
              allServiceFilterChipsEnabled: true,
            },
          },
        },
        deletePlot(plot2.plotId),
      ),
    ).toEqual(expectedState);
  });
  it('should toggle plot', () => {
    const initialState: TimeSeriesStoreType = {
      plotPreset,
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
      },
    };

    const state = reducer(initialState, togglePlot('plotId2'));
    const expectedState1 = produce(initialState, (draft) => {
      draft.plotPreset!.plots[1].enabled = false;
    });
    expect(state).toEqual(expectedState1);

    const expectedState2 = produce(expectedState1, (draft) => {
      draft.plotPreset!.plots[1].enabled = true;
    });
    expect(reducer(state, togglePlot('plotId2'))).toEqual(expectedState2);
  });

  it('should delete plot parameter', () => {
    const idToDelete = 'id1';
    const initialPlotPreset = produce(plotPreset, (draft) => {
      draft.parameters.push({
        id: idToDelete,
        plotId: plot1.plotId,
        unit: 'unit1',
        propertyName: 'prop1',
        plotType: 'line',
        serviceId: mockTimeSeriesServices[0].id,
        collectionId: 'ecmwf',
      });
    });
    expect(
      reducer(
        {
          plotPreset: initialPlotPreset,
          services: mockTimeSeriesServices,
          timeseriesSelect: {
            filters: {
              searchFilter: '',
              serviceFilterChips: serviceFilterChipStore,
              allServiceFilterChipsEnabled: true,
            },
          },
        },
        deleteParameter({ id: idToDelete }),
      ),
    ).toEqual({
      plotPreset,
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
      },
    });
  });

  it('should add parameter', () => {
    const newParameter: Parameter = {
      id: expect.any(String),
      plotId: 'plotId1',
      unit: '°C',
      propertyName: 'DewPoint',
      plotType: 'line',
      serviceId: mockTimeSeriesServices[0].id,
      collectionId: 'ecmwf',
    };
    const expectedState = produce(mockTimeSeriesState, (draft) => {
      draft.plotPreset!.parameters.push(newParameter);
    });

    expect(reducer(mockTimeSeriesState, addParameter(newParameter))).toEqual(
      expectedState,
    );
  });
  it('should toggle parameter', () => {
    const initialState: TimeSeriesStoreType = {
      plotPreset,
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
      },
    };

    const idToToggle = plotPreset.parameters[1].id;

    const state1 = reducer(initialState, toggleParameter({ id: idToToggle! }));
    const expectedState1 = produce(initialState, (draft) => {
      draft.plotPreset!.parameters[1].enabled = false;
    });
    expect(state1).toEqual(expectedState1);

    const state2 = reducer(
      state1,
      toggleParameter({
        id: idToToggle!,
      }),
    );
    const expectedState2 = produce(expectedState1, (draft) => {
      draft.plotPreset!.parameters[1].enabled = true;
    });
    expect(state2).toEqual(expectedState2);
  });

  it('should add a plot', () => {
    const titleToAdd = 'Plot_2';
    const expectedState = produce(mockTimeSeriesState, (draft) => {
      draft.plotPreset!.plots.push({
        title: titleToAdd,
        plotId: expect.any(String),
      });
    });
    expect(
      reducer(mockTimeSeriesState, addPlot({ title: titleToAdd })),
    ).toEqual(expectedState);
  });

  it('should update color and type of parameter', () => {
    const expectedState = produce(mockTimeSeriesState, (draft) => {
      draft.plotPreset!.parameters[0].color = 'blue';
      draft.plotPreset!.parameters[0].plotType = 'bar';
    });
    const parameterToUpdate = plotPreset.parameters[0];
    expect(
      reducer(
        mockTimeSeriesState,
        updateParameter({
          parameter: {
            ...parameterToUpdate,
            color: 'blue',
            plotType: 'bar',
          },
        }),
      ),
    ).toEqual(expectedState);
  });

  it('should set a search string', () => {
    const expectedState: TimeSeriesStoreType = {
      plotPreset,
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        filters: {
          searchFilter: 'some search filter',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
      },
    };
    expect(
      reducer(
        {
          plotPreset,
          services: mockTimeSeriesServices,
          timeseriesSelect: {
            filters: {
              searchFilter: '',
              serviceFilterChips: serviceFilterChipStore,
              allServiceFilterChipsEnabled: true,
            },
          },
        },
        setSearchFilter({ filterText: 'some search filter' }),
      ),
    ).toEqual(expectedState);
  });

  it('should set parameterInfo', () => {
    const expectedState: TimeSeriesStoreType = {
      plotPreset,
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        currentParameterInfo: parameter1.id,
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
      },
    };
    expect(
      reducer(
        {
          plotPreset,
          services: mockTimeSeriesServices,
          timeseriesSelect: {
            filters: {
              searchFilter: '',
              serviceFilterChips: serviceFilterChipStore,
              allServiceFilterChipsEnabled: true,
            },
          },
        },
        setCurrentParameterInfoDisplayed({
          parameterId: parameter1.id as string,
        }),
      ),
    ).toEqual(expectedState);
  });

  it('should not duplicate services', () => {
    const expectedState: TimeSeriesStoreType = {
      plotPreset,
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
      },
    };
    expect(
      reducer(
        {
          plotPreset,
          services: mockTimeSeriesServices,
          timeseriesSelect: {
            filters: {
              searchFilter: '',
              serviceFilterChips: serviceFilterChipStore,
              allServiceFilterChipsEnabled: true,
            },
          },
        },
        addServices({
          timeSeriesServices: mockTimeSeriesServices,
        }),
      ),
    ).toEqual(expectedState);
  });

  it('should add a generated id for diffrent services with matching ids', () => {
    // ids are addative and used in other tests.
    const currIdValue = Number(generateServiceId().slice(-1)) + 1;
    const expectedState: TimeSeriesStoreType = {
      plotPreset,
      services: [
        ...mockTimeSeriesServices,
        {
          ...mockTimeSeriesServices[0],
          url: 'someurl',
          id: `timeseriesserviceid_${currIdValue}`,
        },
      ],
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: {
            entities: {
              [mockTimeSeriesServices[0].id]: {
                serviceId: mockTimeSeriesServices[0].id,
                type: mockTimeSeriesServices[0].type,
                serviceUrl: mockTimeSeriesServices[0].url,
                serviceName: mockTimeSeriesServices[0].name,
                enabled: true,
                scope: 'system',
                isLoading: false,
              },
              [`timeseriesserviceid_${currIdValue}`]: {
                serviceId: `timeseriesserviceid_${currIdValue}`,
                type: mockTimeSeriesServices[0].type,
                serviceUrl: 'someurl',
                serviceName: mockTimeSeriesServices[0].name,
                enabled: true,
                scope: 'system',
                isLoading: false,
              },
            },
            ids: [
              mockTimeSeriesServices[0].id,
              `timeseriesserviceid_${currIdValue}`,
            ],
          },
          allServiceFilterChipsEnabled: true,
        },
      },
    };
    expect(
      reducer(
        {
          plotPreset,
          services: mockTimeSeriesServices,
          timeseriesSelect: {
            filters: {
              searchFilter: '',
              serviceFilterChips: {
                entities: {
                  [mockTimeSeriesServices[0].id]: {
                    serviceId: mockTimeSeriesServices[0].id,
                    type: mockTimeSeriesServices[0].type,
                    serviceUrl: mockTimeSeriesServices[0].url,
                    serviceName: mockTimeSeriesServices[0].name,
                    enabled: true,
                    scope: 'system',
                    isLoading: false,
                  },
                },
                ids: [mockTimeSeriesServices[0].id],
              },
              allServiceFilterChipsEnabled: true,
            },
          },
        },
        addServices({
          timeSeriesServices: [
            { ...mockTimeSeriesServices[0], url: 'someurl' },
          ],
        }),
      ),
    ).toEqual(expectedState);
  });

  it('should update plot title', () => {
    const plotPresetNewTitle = produce(plotPreset, (draft) => {
      draft.plots[0].title = 'newTitle';
    });
    const expectedState: TimeSeriesStoreType = {
      plotPreset: plotPresetNewTitle,
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
      },
    };
    expect(
      reducer(
        mockTimeSeriesState,
        updateTitle({
          id: plot1.plotId,
          title: 'newTitle',
        }),
      ),
    ).toEqual(expectedState);
  });
  it('should change parameter plot id', () => {
    const paramNewPlotId = produce(plotPreset, (draft) => {
      draft.parameters[1].plotId = plot1.plotId;
    });
    const expectedState: TimeSeriesStoreType = {
      plotPreset: paramNewPlotId,
      services: mockTimeSeriesServices,
      timeseriesSelect: {
        filters: {
          searchFilter: '',
          serviceFilterChips: serviceFilterChipStore,
          allServiceFilterChipsEnabled: true,
        },
      },
    };
    expect(
      reducer(
        mockTimeSeriesState,
        moveParameter({
          oldIndex: 0,
          newIndex: 1,
          toPlotId: plot1.plotId,
          plotId: plot2.plotId,
        }),
      ),
    ).toEqual(expectedState);
  });
});
