/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { EntityState } from '@reduxjs/toolkit';
import { ServiceInterface, TimeSeriesService } from '@opengeoweb/shared';
import type { Parameter, PlotPreset } from '../components/TimeSeries/types';

export interface Plot {
  title: string;
  plotId: string;
}

export type ServiceScope = 'user' | 'system';

export interface ServiceFilterChipsObject {
  serviceId: string;
  serviceName?: string;
  serviceUrl: string;
  type: ServiceInterface;
  enabled?: boolean;
  scope?: ServiceScope;
  isLoading?: boolean;
}

export type ServiceFilterChipsType = EntityState<ServiceFilterChipsObject>;
export type ServiceFilterChipsObjectEntities = Record<
  string,
  ServiceFilterChipsObject
>;

export interface TimeseriesSelectFilters {
  searchFilter: string;
  serviceFilterChips: ServiceFilterChipsType;
  allServiceFilterChipsEnabled: boolean;
}

export interface TimeseriesSelectStore {
  filters: TimeseriesSelectFilters;
  currentParameterInfo?: string;
}

export interface TimeSeriesStoreType {
  plotPreset?: PlotPreset;
  timeseriesSelect?: TimeseriesSelectStore;
  services?: TimeSeriesService[];
}

export interface UpdateParameterPayload {
  parameter: Parameter;
}

export interface UpdateTitlePayload {
  id: string;
  title: string;
}

export interface TimeSeriesModuleState {
  timeSeries?: TimeSeriesStoreType;
}

export interface SetSearchFilterPayload {
  filterText: string;
}

export interface MovePlotPayload {
  oldIndex: number;
  newIndex: number;
}

export interface MoveParameterPayload {
  oldIndex: number;
  newIndex: number;
  plotId: string;
  toPlotId: string;
}

export interface SetCurrentParameterInfo {
  parameterId: string;
}
export interface SetTimeSeriesServicesPayload {
  timeSeriesServices: TimeSeriesService[];
}
