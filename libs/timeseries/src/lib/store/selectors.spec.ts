/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { TimeSeriesModuleState } from './types';
import {
  getCurrentParameterInfoDisplayed,
  getMapId,
  getParameterById,
  getPlotState,
  getPlotWithParameters,
  getSearchFilter,
} from './selectors';
import { Parameter } from '../components/TimeSeries/types';
import { mockTimeSeriesServices as services } from '../components/TimeSeries/mockTimeSeriesServices';

describe('store/ui/selectors', () => {
  const mapId = 'mapId';
  const plotId1 = 'plotId1';

  const parameter1: Parameter = {
    plotId: plotId1,
    unit: '°C',
    propertyName: 'Temperature',
    plotType: 'line',
    serviceId: services[0].id,
    id: 'param1',
    collectionId: 'ecmwf',
  };
  const parameter2: Parameter = {
    plotId: plotId1,
    unit: '°C',
    propertyName: 'DewPoint',
    plotType: 'line',
    serviceId: services[0].id,
    collectionId: 'ecmwf',
  };
  const plot1 = {
    title: 'Plot 1',
    plotId: plotId1,
  };

  const mockStore: TimeSeriesModuleState = {
    timeSeries: {
      plotPreset: {
        mapId,
        plots: [
          plot1,
          {
            title: 'Plot 2',
            plotId: 'plotId2',
          },
        ],
        parameters: [
          parameter1,
          parameter2,
          {
            plotId: 'plotId2',
            unit: '%',
            propertyName: 'Humidity',
            plotType: 'line',
            serviceId: services[0].id,
            collectionId: 'ecmwf',
          },
        ],
      },
      services,
      timeseriesSelect: {
        filters: {
          searchFilter: 'some search filter',
          serviceFilterChips: {
            entities: {
              [services[0].id]: {
                serviceId: services[0].id,
                type: services[0].type,
                serviceUrl: services[0].url,
                serviceName: services[0].name,
                enabled: true,
                scope: 'system',
              },
            },
            ids: [services[0].id],
          },
          allServiceFilterChipsEnabled: true,
        },
        currentParameterInfo: parameter1.id,
      },
    },
  };

  const emptyMockStore: TimeSeriesModuleState = {
    timeSeries: undefined,
  };
  describe('getPlotState', () => {
    it('should return plots', () => {
      expect(getPlotState(mockStore)).toEqual(mockStore.timeSeries!.plotPreset);
    });
    it('should return undefined if no plots', () => {
      expect(getPlotState(emptyMockStore)).toEqual(undefined);
    });
    describe('getMapId', () => {
      it('should return mapId', () => {
        expect(getMapId(mockStore)).toEqual(mapId);
      });
      it('should return undefined if no state', () => {
        expect(getMapId(emptyMockStore)).toEqual(undefined);
      });
    });

    describe('getPlotWithParameters', () => {
      it('should return plot with parameters', () => {
        const plot = getPlotWithParameters(mockStore, plotId1);
        expect(plot).toEqual({
          ...plot1,
          parameters: [parameter1, parameter2],
        });
      });
      it('should return undefined if no state', () => {
        expect(getPlotWithParameters(emptyMockStore, plotId1)).toEqual(
          undefined,
        );
      });
      it('should return undefined if no plot id', () => {
        expect(getPlotWithParameters(mockStore, '')).toEqual(undefined);
      });
      it('should return undefined if plot id is not in store', () => {
        expect(getPlotWithParameters(mockStore, 'plotIdNotInStore')).toEqual(
          undefined,
        );
      });
    });

    describe('getSearchFilter', () => {
      it('should return the search filter', () => {
        expect(getSearchFilter(mockStore)).toEqual('some search filter');
      });
      it('should return empty string if no state', () => {
        expect(getSearchFilter(emptyMockStore)).toEqual('');
      });
    });

    describe('getCurrentParameterInfoDisplayed', () => {
      it('should return the paramter info', () => {
        expect(getCurrentParameterInfoDisplayed(mockStore)).toEqual(
          parameter1.id,
        );
      });
      it('should return undefined if no state', () => {
        expect(getCurrentParameterInfoDisplayed(emptyMockStore)).toEqual('');
      });
    });

    describe('getParameterById', () => {
      it('should return the related parameter', () => {
        expect(getParameterById(mockStore, parameter1.id as string)).toEqual(
          parameter1,
        );
      });
      it('should return undefined if no state', () => {
        expect(
          getParameterById(emptyMockStore, parameter1.id as string),
        ).toEqual(undefined);
      });
    });
  });
});
