/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import {
  createSlice,
  PayloadAction,
  Draft,
  createEntityAdapter,
} from '@reduxjs/toolkit';
import { produce } from 'immer';
import { mapUtils } from '@opengeoweb/store';
import { compact } from 'lodash';
import { TimeSeriesService } from '@opengeoweb/shared';
import { Parameter } from '../components/TimeSeries/types';
import {
  ServiceFilterChipsObject,
  SetCurrentParameterInfo,
  SetSearchFilterPayload,
  MovePlotPayload,
  SetTimeSeriesServicesPayload,
  TimeSeriesStoreType,
  UpdateParameterPayload,
  UpdateTitlePayload,
  MoveParameterPayload,
} from './types';
import { getServiceById } from '../utils/edrUtils';
// import { mockTimeSeriesServices as services } from './utils';

export const timeseriesSelectServiceFilterChipsAdapter =
  createEntityAdapter<ServiceFilterChipsObject>({
    selectId: (service) => service.serviceId!,
  });

export const initialState: TimeSeriesStoreType = {
  plotPreset: undefined,
  services: undefined,
  timeseriesSelect: {
    currentParameterInfo: undefined,
    filters: {
      searchFilter: '',
      serviceFilterChips:
        timeseriesSelectServiceFilterChipsAdapter.getInitialState(),
      allServiceFilterChipsEnabled: true,
    },
  },
};

const slice = createSlice({
  initialState,
  name: 'timeseries',
  reducers: {
    registerTimeSeriesPreset: (
      draft: Draft<TimeSeriesStoreType>,
      action: PayloadAction<TimeSeriesStoreType>,
    ) => {
      // Make sure to initialize services to empty array (if not set)
      if (!draft.services || !draft.services.length) {
        draft.services = [];
      }

      const newServicesAdded: ServiceFilterChipsObject[] = [];

      const timeSeriesPresetWithParameterIds = produce(
        action.payload,
        (draftPreset) => {
          if (!draftPreset.plotPreset) {
            console.warn('missing draft plotpresets', draftPreset);
            return;
          }
          // Make sure to initialize services to empty array (if not set)
          if (!draftPreset.services || !draftPreset.services.length) {
            draftPreset.services = [];
          }
          draftPreset.plotPreset.parameters.forEach((draftParameter) => {
            // Add parameter id to plotPreset.parameters
            draftParameter.id = generateParameterId();

            // Add opacity value to plotPreset.parameters
            draftParameter.opacity = draftParameter.opacity || 70;

            // Check if service is already in the services list
            const serviceFromStore = getServiceById(
              draft.services,
              draftParameter.serviceId,
            );
            if (!serviceFromStore) {
              // The service is not in the store. Add it based on the preset
              const serviceFromAction = getServiceById(
                draftPreset.services,
                draftParameter.serviceId,
              );
              if (!serviceFromAction) {
                console.error(
                  `A parameter from the preset has a service with id ${draftParameter.serviceId} which is not in the store or in the preset`,
                );
                return;
              }
              // Now check if the url is already in the store, it might be there with a different id
              const reUseAbleService = draft.services?.find((storeService) => {
                return storeService.url === serviceFromAction.url;
              });
              if (!reUseAbleService) {
                // The url and or serviceId were not yet in the services state
                draft.services?.push({ ...serviceFromAction, scope: 'system' });
                newServicesAdded.push({
                  serviceId: serviceFromAction.id,
                  serviceUrl: serviceFromAction.url,
                  serviceName: serviceFromAction.name,
                  type: serviceFromAction.type,
                  scope: 'system',
                });
              } else {
                // The same url is already in the store, but has the wrong id. Adjust the parameters serviceId.
                draftParameter.serviceId = reUseAbleService.id;
              }
            }
          });
        },
      );

      // Check if all the services from the preset are now in the store. Some services might not be used in the parameter.
      timeSeriesPresetWithParameterIds?.services?.forEach(
        (serviceFromPreset) => {
          const serviceFromAction = draft.services?.find((serviceFromStore) => {
            return serviceFromPreset.url === serviceFromStore.url;
          });
          // The service from the preset was not added yet because it was not used in any parameter
          if (!serviceFromAction) {
            // Check if the id was already taken, if it was generate a new one.
            const uniqueServiceId = draft.services?.find((service) => {
              return serviceFromPreset.id === service.id;
            })
              ? generateServiceId()
              : serviceFromPreset.id;
            // Finally push the new (and unused) service to the store
            draft.services?.push({
              ...serviceFromPreset,
              id: uniqueServiceId,
              scope: 'system',
            });
            newServicesAdded.push({
              serviceId: uniqueServiceId,
              serviceUrl: serviceFromPreset.url,
              serviceName: serviceFromPreset.name,
              type: serviceFromPreset.type,
              scope: 'system',
            });
          }
        },
      );

      draft.plotPreset = timeSeriesPresetWithParameterIds.plotPreset;
      // Add filterchips for any new services that were added
      reducer(draft, {
        payload: newServicesAdded,
        type: actions.setServiceFilterChipsInStore,
      });
    },
    deletePlot: (
      draft: Draft<TimeSeriesStoreType>,
      action: PayloadAction<string>,
    ) => {
      draft.plotPreset!.plots = draft.plotPreset!.plots.filter((plot) => {
        return plot.plotId !== action.payload;
      });
      draft.plotPreset!.parameters = draft.plotPreset!.parameters.filter(
        (parameter) => {
          return parameter.plotId !== action.payload;
        },
      );
    },
    togglePlot: (
      draft: Draft<TimeSeriesStoreType>,
      action: PayloadAction<string>,
    ) => {
      draft.plotPreset!.plots = draft.plotPreset!.plots.map((plot) => {
        if (plot.plotId === action.payload) {
          const plotIsEnabled = plot.enabled !== false;
          return { ...plot, enabled: !plotIsEnabled };
        }
        return plot;
      });
    },
    addPlot: (
      draft: Draft<TimeSeriesStoreType>,
      action: PayloadAction<{ title: string }>,
    ) => {
      draft.plotPreset!.plots.push({
        title: action.payload.title,
        plotId: generateTimeSeriesId(),
      });
    },
    deleteParameter: (
      draft: Draft<TimeSeriesStoreType>,
      action: PayloadAction<{ id: string }>,
    ) => {
      draft.plotPreset!.parameters = draft.plotPreset!.parameters.filter(
        (parameter) => {
          return parameter.id !== action.payload.id;
        },
      );
    },
    addParameter: (
      draft: Draft<TimeSeriesStoreType>,
      action: PayloadAction<Parameter>,
    ) => {
      draft.plotPreset!.parameters.push({
        ...action.payload,
        id: generateParameterId(),
      });
    },
    toggleParameter: (
      draft: Draft<TimeSeriesStoreType>,
      action: PayloadAction<{ id: string }>,
    ) => {
      draft.plotPreset!.parameters = draft.plotPreset!.parameters.map(
        (parameter) => {
          if (parameter.id === action.payload.id) {
            const parameterIsEnabled = parameter.enabled !== false;
            return { ...parameter, enabled: !parameterIsEnabled };
          }
          return parameter;
        },
      );
    },
    updateParameter: (
      draft: Draft<TimeSeriesStoreType>,
      action: PayloadAction<UpdateParameterPayload>,
    ) => {
      const { id, color, plotType, opacity, instanceId, propertyName } =
        action.payload.parameter;
      draft.plotPreset!.parameters = draft.plotPreset!.parameters.map(
        (parameter): Parameter => {
          if (parameter.id === id) {
            return {
              ...parameter,
              color,
              plotType,
              opacity,
              instanceId,
              propertyName,
            };
          }
          return parameter;
        },
      );
    },

    updateTitle: (
      draft: Draft<TimeSeriesStoreType>,
      action: PayloadAction<UpdateTitlePayload>,
    ) => {
      const plot = draft.plotPreset?.plots.find(
        (elem) => elem.plotId === action.payload.id,
      );
      if (plot) {
        plot.title = action.payload.title;
      }
    },
    setSearchFilter: (draft, action: PayloadAction<SetSearchFilterPayload>) => {
      const { filterText } = action.payload;
      if (draft.timeseriesSelect) {
        draft.timeseriesSelect.filters.searchFilter = filterText;
      }
    },
    movePlot: (draft, action: PayloadAction<MovePlotPayload>) => {
      const { oldIndex, newIndex } = action.payload;
      if (!draft.plotPreset?.plots) {
        return;
      }
      draft.plotPreset.plots = mapUtils.moveArrayElements(
        draft.plotPreset.plots,
        oldIndex,
        newIndex,
      );
    },

    moveParameter: (draft, action: PayloadAction<MoveParameterPayload>) => {
      const { oldIndex, newIndex, toPlotId, plotId } = action.payload;
      if (!draft.plotPreset) {
        return;
      }
      const oldPos =
        draft.plotPreset.parameters.findIndex(
          (param) => param.plotId === plotId,
        ) + oldIndex;
      const newPos =
        draft.plotPreset.parameters.findIndex(
          (param) => param.plotId === toPlotId,
        ) + newIndex;
      if (toPlotId !== plotId) {
        draft.plotPreset.parameters[oldPos].plotId = toPlotId;
      }
      draft.plotPreset.parameters = mapUtils.moveArrayElements(
        draft.plotPreset.parameters,
        oldPos,
        newPos,
      );
    },

    // Select a filter chip - if this means all will be selected, ensure all gets set to selected
    setServiceFilterChipSelected: (
      draft,
      action: PayloadAction<{ serviceId: string }>,
    ) => {
      const serviceFilterChipsById =
        draft.timeseriesSelect?.filters.serviceFilterChips.entities;
      if (serviceFilterChipsById) {
        const activeServices = compact(Object.values(serviceFilterChipsById));
        const countSelectedServices = activeServices.filter(
          (service) => service.enabled,
        ).length;
        const isAllServicesGoingToBeEnabled =
          countSelectedServices === activeServices.length - 1;
        if (isAllServicesGoingToBeEnabled) {
          slice.caseReducers.setAllServiceFilterChipSelected(draft);
          return;
        }

        const service = serviceFilterChipsById[action.payload.serviceId]!;
        service.enabled = true;
      }
    },
    // Select all filter chips
    setAllServiceFilterChipSelected: (draft) => {
      if (draft.timeseriesSelect) {
        const serviceFilterChipsById =
          draft.timeseriesSelect.filters.serviceFilterChips.entities;
        const servicesToTurnOn = compact(
          Object.values(serviceFilterChipsById),
        ).filter((service) => !service.enabled);

        const updates = servicesToTurnOn.map((service) => ({
          id: service.serviceId!,
          changes: { enabled: true },
        }));
        timeseriesSelectServiceFilterChipsAdapter.updateMany(
          draft.timeseriesSelect.filters.serviceFilterChips,
          updates,
        );
        draft.timeseriesSelect.filters.allServiceFilterChipsEnabled = true;
      }
    },
    // Unselect all selected filter chips so this is the only one remaining selected
    setOnlyThisServiceFilterChipSelected: (
      draft,
      action: PayloadAction<{ serviceId: string }>,
    ) => {
      if (draft.timeseriesSelect) {
        const serviceFilterChipsById =
          draft.timeseriesSelect.filters.serviceFilterChips.entities;

        const servicesToTurnOff = compact(
          Object.values(serviceFilterChipsById),
        ).filter((service) => service.serviceId !== action.payload.serviceId);

        const updates = servicesToTurnOff.map((service) => ({
          id: service.serviceId!,
          changes: { enabled: false },
        }));
        timeseriesSelectServiceFilterChipsAdapter.updateMany(
          draft.timeseriesSelect.filters.serviceFilterChips,
          updates,
        );

        draft.timeseriesSelect.filters.allServiceFilterChipsEnabled = false;
      }
    },
    // Unselect one filter chip - if this means all would be unselected, automatically reselect ALL
    setServiceFilterChipUnselected: (
      draft,
      action: PayloadAction<{ serviceId: string }>,
    ) => {
      if (draft.timeseriesSelect) {
        const serviceFilterChipsById =
          draft.timeseriesSelect.filters.serviceFilterChips.entities;
        const countPressedServices = Object.values(
          serviceFilterChipsById,
        ).filter((service) => service?.enabled).length;
        const isAllServicesGoingToBeDisabled = countPressedServices === 1;
        if (isAllServicesGoingToBeDisabled) {
          slice.caseReducers.setAllServiceFilterChipSelected(draft);
          return;
        }

        const service = serviceFilterChipsById[action.payload.serviceId]!;
        service.enabled = false;
      }
    },
    setServiceFilterChipsInStore: (
      // eslint-disable-next-line no-unused-vars
      draft,
      // eslint-disable-next-line no-unused-vars
      action: PayloadAction<ServiceFilterChipsObject[]>,
    ) => {
      action.payload.forEach((service) => {
        if (draft.timeseriesSelect) {
          draft.timeseriesSelect.filters.serviceFilterChips.entities[
            service.serviceId
          ] = {
            serviceId: service.serviceId,
            serviceName: service.serviceName || service.serviceId,
            enabled:
              draft.timeseriesSelect?.filters.allServiceFilterChipsEnabled,
            scope: service.scope || 'system',
            type: service.type,
            serviceUrl: service.serviceUrl,
            isLoading: false,
          };

          if (
            !draft.timeseriesSelect.filters.serviceFilterChips.ids.includes(
              service.serviceId,
            )
          ) {
            draft.timeseriesSelect.filters.serviceFilterChips.ids.push(
              service.serviceId,
            );
          }
        }
      });
    },

    setCurrentParameterInfoDisplayed: (
      draft,
      action: PayloadAction<SetCurrentParameterInfo>,
    ) => {
      const { parameterId } = action.payload;
      if (draft.timeseriesSelect) {
        draft.timeseriesSelect.currentParameterInfo = parameterId;
      }
    },
    addServices: (
      draft,
      action: PayloadAction<SetTimeSeriesServicesPayload>,
    ) => {
      if (!draft.services?.length) {
        draft.services = action.payload.timeSeriesServices;
      } else {
        const filteredServices = action.payload.timeSeriesServices.reduce(
          (acc, service) => {
            if (!draft.services!.some((s) => s.url === service.url)) {
              const hasDuplicateId = draft.services!.some(
                (s) => s.id === service.id,
              );
              if (hasDuplicateId) {
                acc.push({
                  ...service,
                  id: generateServiceId(),
                } as TimeSeriesService);
              } else {
                acc.push(service);
              }
            }
            return acc;
          },
          [] as TimeSeriesService[],
        );
        draft.services = [...draft.services, ...filteredServices];
      }
      const newServicesAdded = draft.services.map((service) => {
        return {
          serviceId: service.id,
          serviceUrl: service.url,
          serviceName: service.name,
          type: service.type,
          scope: 'system',
        };
      });
      // Add filterchips for any new services that were added
      reducer(draft, {
        payload: newServicesAdded,
        type: actions.setServiceFilterChipsInStore,
      });
    },
  },
});

export const { reducer, actions } = slice;

let generatedTimeseriesIds = 0;
const generateTimeSeriesId = (): string => {
  generatedTimeseriesIds += 1;
  return `timeseriesid_${generatedTimeseriesIds}`;
};

let generatedParameterIds = 0;
const generateParameterId = (): string => {
  generatedParameterIds += 1;
  return `timeseriesparamid_${generatedParameterIds}`;
};

let generatedServiceIds = 0;
export const generateServiceId = (): string => {
  generatedServiceIds += 1;
  return `timeseriesserviceid_${generatedServiceIds}`;
};
