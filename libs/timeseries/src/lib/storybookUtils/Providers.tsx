/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Provider } from 'react-redux';
import { ConfirmationServiceProvider, withEggs } from '@opengeoweb/shared';
import { Theme } from '@mui/material';
import { lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import { Store } from '@reduxjs/toolkit';
import { mapStoreModuleConfig, uiModuleConfig } from '@opengeoweb/store';
import { SnackbarWrapperConnect } from '@opengeoweb/snackbar';
import { timeSeriesModuleConfig } from '../store/config';

interface TimeSeriesThemeProviderProps {
  children?: React.ReactElement;
  theme?: Theme;
}

/**
 * A Provider component which provides the GeoWeb theme
 * @param children
 * @returns
 */
export const TimeSeriesThemeProvider: React.FC<
  TimeSeriesThemeProviderProps
> = ({ children, theme = lightTheme }: TimeSeriesThemeProviderProps) => (
  <ThemeWrapper theme={theme}>{children}</ThemeWrapper>
);

interface TimeSeriesThemeStoreProviderProps
  extends TimeSeriesThemeProviderProps {
  store: Store;
}

const ThemeWrapperWithModules: React.FC<TimeSeriesThemeProviderProps> =
  withEggs([mapStoreModuleConfig, uiModuleConfig, timeSeriesModuleConfig])(
    ({ theme, children }: TimeSeriesThemeProviderProps) => (
      <ThemeWrapper theme={theme}>{children}</ThemeWrapper>
    ),
  );

/**
 * A Provider component which provides the GeoWeb theme and store for the timeseries.
 * Note: Should only be used with timeseries components, as the provided store is only for timeseries.
 * @param children
 * @returns
 */
export const TimeSeriesThemeStoreProvider: React.FC<
  TimeSeriesThemeStoreProviderProps
> = ({ store, theme, children }: TimeSeriesThemeStoreProviderProps) => (
  <Provider store={store}>
    <ThemeWrapperWithModules theme={theme}>
      <SnackbarWrapperConnect>
        <ConfirmationServiceProvider>{children}</ConfirmationServiceProvider>
      </SnackbarWrapperConnect>
    </ThemeWrapperWithModules>
  </Provider>
);
