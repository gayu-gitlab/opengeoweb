/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { TimeSeriesService, dateUtils } from '@opengeoweb/shared';
import { Feature, FeatureCollection, Point } from 'geojson';
import { isString, sortBy } from 'lodash';
import Axios from 'axios';
import { setupCache } from 'axios-cache-interceptor';
import type {
  EDRCollection,
  EDRInstance,
  EDRPositionResponse,
  EdrCollection,
  Parameter,
  ParameterWithData,
  SelectParameter,
} from '../components/TimeSeries/types';
import { COLOR_MAP } from '../constants';
import type { PointerLocationAndName } from '../components/TimeSeries/TimeSeriesConnect';
import { TimeseriesCollectionDetail } from '../components/TimeSeriesSelect/utils';
import { ogcParameters } from '../components/TimeSeriesSelect/ogcParameters';

const AXIOS_TIME_TO_LIVE_MS_FOR_EDR = 1000 * 60; // 1 minute

// Cached axios
const cachedAxios = setupCache(Axios, {
  cacheTakeover: false,
  ttl: AXIOS_TIME_TO_LIVE_MS_FOR_EDR,
});

const EDR_NORWAY_URL_NO_MOCK = 'https://interpol-b.met.no';

export const supportedOutputFormat = 'CoverageJSON';

export const getBaseQueryString = (
  parameterName: string,
  instance: EDRInstance,
  clipInterval: [number, number] = [12, 48],
): string => {
  const nameParam = `&parameter-name=${parameterName}`;
  const interval = instance.extent?.temporal.interval[0];

  const [start, end] = clipEdrInterval(
    interval,
    clipInterval[0],
    clipInterval[1],
  );

  const timeParam = interval?.length ? `&datetime=${start}/${end}` : '';
  const crsParam = instance.extent?.spatial.crs
    ? `&crs=${instance.extent.spatial.crs}`
    : '';
  const formatParam = `&f=${supportedOutputFormat}`;
  return `?${nameParam}${timeParam}${crsParam}${formatParam}`;
};

export const latestInstance = (instances: EDRInstance[]): EDRInstance => {
  return instances.reduce(
    (latest, current) => (latest.id < current.id ? current : latest),
    { id: '' },
  );
};

export const isSupportedInstance = (instances: EDRInstance): boolean => {
  const outputFormatsPosition =
    instances.data_queries?.position?.link.variables.output_formats ?? [];
  const outputFormatsLocation =
    instances.data_queries?.locations?.link.variables.output_formats ?? [];
  return [...outputFormatsLocation, ...outputFormatsPosition].includes(
    supportedOutputFormat,
  );
};

export const latestSupportedInstance = (
  instances: EDRInstance[],
): EDRInstance => {
  return latestInstance(
    instances.filter((instance) => isSupportedInstance(instance)),
  );
};

export const listSupportedInstance = (
  instances: EDRInstance[],
): EDRInstance[] => {
  return instances.filter((instance) => isSupportedInstance(instance));
};

export const fetchEdrParameterApiData = async (
  urlDomain: string,
  parameterName: string,
  collectionId: string,
  location: PointerLocationAndName,
  instance: EDRInstance,
): Promise<EDRPositionResponse | null> => {
  const base = `${urlDomain}/collections/${collectionId}/instances/${instance.id}`;
  const queryString = getBaseQueryString(parameterName, instance);
  const { lat, lon, id } = location;

  try {
    if (urlDomain === EDR_NORWAY_URL_NO_MOCK) {
      if (id === undefined) {
        return null;
      }
      const locationUrl = `${base}/locations/${id}${queryString}`;
      const result = await cachedAxios.get<EDRPositionResponse>(locationUrl);
      return result.data;
    }
    const positionUrl = `${base}/position${queryString}&coords=POINT(${lon} ${lat})`;
    const result = await cachedAxios.get<EDRPositionResponse>(positionUrl);
    return result.data;
  } catch (error) {
    return null;
  }
};

export const fetchEdrLatestInstances = async (
  collectionUrl: string,
  collectionId: string,
): Promise<EDRInstance> => {
  const url = `${collectionUrl}/collections/${collectionId}/instances`;
  try {
    const result = await cachedAxios.get<{ instances: EDRInstance[] }>(url);
    return latestSupportedInstance(result.data.instances);
  } catch (error) {
    return { id: '' };
  }
};

export const fetchEdrAllInstances = async (
  baseUrl: string,
  collectionId: string,
): Promise<EDRInstance[]> => {
  const url = `${baseUrl}/collections/${collectionId}/instances`;
  try {
    const result = await Axios.get<{ instances: EDRInstance[] }>(url);
    return listSupportedInstance(result.data.instances);
  } catch (error) {
    return [];
  }
};

export const getEdrParameter = async (
  presetParameter: Parameter,
  url: string,
  location: PointerLocationAndName,
): Promise<ParameterWithData | null> => {
  const allEdrInstances = await fetchEdrAllInstances(
    url,
    presetParameter.collectionId,
  );

  const instance =
    presetParameter.instanceId &&
    allEdrInstances.find((inst) => inst.id === presetParameter.instanceId);

  const apiData: EDRPositionResponse | null = await fetchEdrParameterApiData(
    url,
    presetParameter.propertyName,
    presetParameter.collectionId,
    location,
    instance || latestSupportedInstance(allEdrInstances),
  );

  if (!apiData?.ranges || apiData.domain.axes.t.values.length === 0) {
    return null;
  }
  // assume only one parameter
  const paramKey = Object.keys(apiData.ranges)[0];

  const { symbol } = apiData.parameters[paramKey].unit;
  const unit = isString(symbol) ? symbol : symbol.value;
  return {
    ...presetParameter,
    unit,
    timestep: apiData.domain.axes.t.values.map((str) => dateUtils.utc(str)),
    value: apiData.ranges[paramKey].values,
  };
};

export const fetchEdrCollections = async (
  serivceUrl: string,
): Promise<EDRCollection[]> => {
  const url = `${serivceUrl}/collections`;
  try {
    const result = await cachedAxios.get<{ collections: EDRCollection[] }>(url);
    return result.data.collections;
  } catch (error) {
    return [];
  }
};

export const constructParameterObject = (
  serviceId: string,
  collectionId: string,
  parametersById: EdrParameters,
): SelectParameter[] => {
  return Object.keys(parametersById).map((parameterId) => {
    const parameter = parametersById[parameterId];
    return {
      plotType: 'line',
      propertyName: parameter.id || parameterId,
      unit: parameter.unit?.label ?? ' ',
      serviceId,
      collectionId,
      color: COLOR_MAP[parameter.id as keyof typeof COLOR_MAP] ?? 'A',
      opacity: 70,
    };
  });
};

export const getEdrSelectCollectionsParameters = async (
  service: TimeSeriesService,
): Promise<EdrCollection[]> => {
  try {
    const collectionsParameters: EdrCollection[] = [];
    // Fetch all collections for this service
    const serviceCollections = (await fetchEdrCollections(service.url)).filter(
      (collection) => {
        // TODO: Maarten Plieger, discuss what to do with collections that do not have a position query?
        // See https://gitlab.com/opengeoweb/geoweb-assets/-/issues/3005
        const hasCubeLocationQuery = !!(
          collection &&
          collection.data_queries &&
          collection.data_queries.cube
        );
        const hasPositionLocationQuery = !!(
          collection &&
          collection.data_queries &&
          collection.data_queries.position
        );
        const usableCollection = !(
          hasCubeLocationQuery && !hasPositionLocationQuery
        );

        if (!usableCollection) {
          console.warn('Skipping collection', collection);
        }
        return usableCollection;
      },
    );

    for await (const collection of serviceCollections) {
      const collectionUrl = `${service.url}/collections/${collection.id}`;

      if (service.url === EDR_NORWAY_URL_NO_MOCK) {
        // Retrieve the latest instance
        const instance = await fetchEdrLatestInstances(
          service.url,
          collection.id,
        );

        // Retrieve all params of the latest instance
        const result = await cachedAxios.get<{ parameters: EdrParameters }>(
          `${collectionUrl}/instances/${instance.id}`,
        );

        collectionsParameters.push({
          id: collection.id,
          description: collection.description,
          parameters: constructParameterObject(
            service.id,
            collection.id,
            result.data.parameters,
          ),
        });
      } else {
        const result = await cachedAxios.get<{
          parameter_names: EdrParameters;
        }>(collectionUrl);
        collectionsParameters.push({
          id: collection.id,
          description: collection.description,
          parameters: constructParameterObject(
            service.id,
            collection.id,
            result.data.parameter_names,
          ),
        });
      }
    }
    return collectionsParameters;
  } catch (error) {
    console.error(error);
    return [];
  }
};

type EdrParameters = Record<
  string,
  {
    id: string;
    description: string;
    type: string;
    collectionId: string;
    observedProperty: {
      label: string;
    };
    unit?: {
      label: string;
      symbol: {
        type: string;
        value: string;
      };
    };
  }
>;

export const getLocations = async (
  service: TimeSeriesService,
  collectionId: string,
  circleDrawFunctionId: string,
): Promise<FeatureCollection<Point> | undefined> => {
  const url = `${service.url}/collections/${collectionId}/locations`;
  try {
    const result: {
      data: {
        features: {
          id: string;
          type: 'Feature';
          geometry: { coordinates: [number, number]; type: 'Point' };
          properties: {
            datetime: string;
            detail: string;
            name: string;
          };
        }[];
      };
    } = await cachedAxios.get(url);

    const features = result.data.features.map((feature): Feature<Point> => {
      return {
        id: feature.id,
        type: 'Feature',
        properties: {
          name: feature.properties.name,
          drawFunctionId: circleDrawFunctionId,
        },
        geometry: {
          type: 'Point',
          coordinates: feature.geometry.coordinates,
        },
      };
    });
    return {
      type: 'FeatureCollection',
      features: sortBy(features, (feature) => feature.properties?.name),
    };
  } catch (error) {
    console.warn(error.message);
  }
  return undefined;
};

export const getServiceById = (
  services: TimeSeriesService[] | undefined,
  serviceId: string | undefined,
): TimeSeriesService | undefined => {
  return serviceId && services && services.length > 0
    ? services.find((service) => service.id === serviceId)
    : undefined;
};

export const getCollectionListForTimeSeriesService = async (
  service: TimeSeriesService,
): Promise<TimeseriesCollectionDetail[]> => {
  if (service?.type === 'EDR') {
    return getEdrSelectCollectionsParameters(service).then(
      (edrCollectionParams: EdrCollection[]) => {
        return edrCollectionParams.map((edrCollectionParam) => {
          return {
            collectionId: edrCollectionParam.id,
            serviceId: service.id,
            serviceName: service.name || service.id,
            parameters: edrCollectionParam.parameters,
          };
        });
      },
    );
  }

  return [
    {
      collectionId: 'collectionOGC',
      serviceId: service.id,
      parameters: ogcParameters,
      serviceName: service.name || service.id,
    },
  ];
};

export const getParameterListForCollectionId = async (
  service: TimeSeriesService,
  collectionId: string,
): Promise<TimeseriesCollectionDetail> => {
  const collections = await getCollectionListForTimeSeriesService(service);
  const collection = collections.find(
    (collection: TimeseriesCollectionDetail): boolean => {
      return collection.collectionId === collectionId;
    },
  );
  if (collection) {
    return collection;
  }
  throw new Error(`Collection with id${collectionId}not available`);
};

const HOUR_IN_MILLISECONDS = 3600 * 1000;

/**
 * This function clips the dates in the EDR interval string to a less wide  time range.
 *
 * @param interval EDR interval, e.g. ["startdate_as_isostring","enddate_as_isostring"]
 * @param maxHoursBefore Clips to maximum hours before the given date
 * @param maxHoursAfter Clip to maximum hours after the given date
 * @param date The date to use to clip arround, defaults to Date.now()
 * @returns Clipped EDR interval, e.g. ["startdate_as_isostring","enddate_as_isostring"]
 */
export const clipEdrInterval = (
  interval: string[] | undefined,
  maxHoursBefore = 12,
  maxHoursAfter = 48,
  date = Date.now(),
): [string, string] => {
  const startTimeFromEdr = interval?.length && interval[0];
  const endTimeFromEdr = interval?.length && interval[1];

  const startDate = (
    dateUtils.isoStringToDate(String(startTimeFromEdr), false) ||
    dateUtils.getNowUtc()
  ).getTime();

  const start = dateUtils.dateToString(
    new Date(
      Math.max(startDate || date, date - maxHoursBefore * HOUR_IN_MILLISECONDS),
    ),
  )!;

  const endDate = (
    dateUtils.isoStringToDate(String(endTimeFromEdr), false) ||
    dateUtils.getNowUtc()
  ).getTime();

  const end = dateUtils.dateToString(
    new Date(
      Math.min(endDate || date, date + maxHoursAfter * HOUR_IN_MILLISECONDS),
    ),
  )!;
  return [start, end];
};
