/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { TimeSeriesService, dateUtils } from '@opengeoweb/shared';
import { Parameter } from '../components/TimeSeries/types';
import {
  getBaseQueryString,
  fetchEdrLatestInstances,
  fetchEdrParameterApiData,
  getEdrParameter,
  isSupportedInstance,
  latestInstance,
  latestSupportedInstance,
  supportedOutputFormat,
  listSupportedInstance,
  getCollectionListForTimeSeriesService,
  getParameterListForCollectionId,
  clipEdrInterval,
} from './edrUtils';
import { serverMock } from '../../mocks/server';
import {
  edrPositionResponseMock,
  latestInstanceMock,
  parameterNameMock,
  parameterUnitMock,
  EDR_FINLAND_URL,
  timestepMock,
  valueMock,
  EDR_FINLAND_ECMMF_COLLECTION_ID,
} from '../../mocks';

describe('utils/edrUtils', () => {
  beforeAll(() => serverMock.listen());
  afterAll(() => serverMock.close());
  const supportedInstance = {
    id: '20230101',
    extent: {
      temporal: {
        interval: [['2023-01-01T00:00:00Z', '2023-01-11T00:00:00Z']],
      },
      spatial: {
        crs: 'EPSG:4326',
      },
    },
    data_queries: {
      position: {
        link: {
          variables: {
            output_formats: ['unknown', supportedOutputFormat],
          },
        },
      },
    },
  };

  describe('latestInstance', () => {
    it('should return default instance if array is empty', () => {
      const instance = latestInstance([]);
      expect(instance).toEqual({ id: '' });
    });

    it('should return latest instance by id', () => {
      const instance = latestInstance([
        { id: '20230101' },
        { id: '20230109' },
        { id: '20230108' },
      ]);
      expect(instance.id).toEqual('20230109');
    });
  });

  describe('isSupportedInstance', () => {
    it('should require position data query', () => {
      expect(isSupportedInstance({ id: '20230101' })).toBeFalsy();
      expect(
        isSupportedInstance({ id: '20230101', data_queries: {} }),
      ).toBeFalsy();
    });

    it('should require supported format', () => {
      expect(
        isSupportedInstance({
          id: '20230101',
          data_queries: {
            position: { link: { variables: { output_formats: ['unknown'] } } },
          },
        }),
      ).toBeFalsy();

      expect(isSupportedInstance(supportedInstance)).toBeTruthy();
    });
  });

  describe('latestSupportedInstance', () => {
    it('should return latest supported', () => {
      const instances = [
        supportedInstance,
        {
          id: '20230102',
          data_queries: {
            position: {
              link: {
                variables: {
                  output_formats: ['unknown'],
                },
              },
            },
          },
        },
      ];
      expect(latestSupportedInstance(instances)).toBe(instances[0]);
    });
  });

  describe('listOfSupportedInstance', () => {
    it('should return list of supported instance', () => {
      const instances = [
        supportedInstance,
        {
          id: '20230102',
          data_queries: {
            position: {
              link: {
                variables: {
                  output_formats: ['unknown'],
                },
              },
            },
          },
        },
      ];
      expect(listSupportedInstance(instances)).toStrictEqual([
        supportedInstance,
      ]);
    });
  });

  describe('edrPositionRequestUrl', () => {
    beforeAll(() => {
      jest.useFakeTimers();
      jest.setSystemTime(new Date('2023-01-05T00:00:00Z'));
    });

    afterAll(() => {
      jest.useRealTimers();
    });

    it('should return url with correct request parameters', () => {
      const urlParams = new URLSearchParams(
        getBaseQueryString(parameterNameMock, supportedInstance, [1000, 1000]),
      );
      expect(urlParams.get('parameter-name')).toEqual(parameterNameMock);
      expect(urlParams.get('crs')).toEqual('EPSG:4326');
      expect(urlParams.get('datetime')).toEqual(
        '2023-01-01T00:00:00Z/2023-01-11T00:00:00Z',
      );
    });

    it('should handle missing extend by ignoring time and crs', () => {
      const urlParams = new URLSearchParams(
        getBaseQueryString(parameterNameMock, {
          ...supportedInstance,
          extent: undefined,
        }),
      );
      expect(urlParams.get('parameter-name')).toEqual(parameterNameMock);
      expect(urlParams.get('crs')).toBeFalsy();
      expect(urlParams.get('datetime')).toBeFalsy();
    });

    it('should include supported output format', () => {
      const urlParams = new URLSearchParams(
        getBaseQueryString(parameterNameMock, supportedInstance),
      );
      expect(urlParams.get('f')).toEqual(supportedOutputFormat);
    });
  });

  describe('fetchEdrParameterApiData', () => {
    it('should fetch data', async () => {
      const res = await fetchEdrParameterApiData(
        EDR_FINLAND_URL,
        parameterNameMock,
        EDR_FINLAND_ECMMF_COLLECTION_ID,
        {
          lat: 1,
          lon: 2,
        },
        latestInstanceMock,
      );
      expect(res).toEqual(edrPositionResponseMock);
    });

    it('should return null on error', async () => {
      const res = await fetchEdrParameterApiData(
        EDR_FINLAND_URL,
        'nonSupportedProperty',
        EDR_FINLAND_ECMMF_COLLECTION_ID,
        {
          lat: 1,
          lon: 2,
        },
        supportedInstance,
      );
      expect(res).toEqual(null);
    });
  });

  describe('fetchEdrLatestInstances', () => {
    it('should fetch data', async () => {
      const instance = await fetchEdrLatestInstances(
        EDR_FINLAND_URL,
        EDR_FINLAND_ECMMF_COLLECTION_ID,
      );
      expect(instance).toEqual(latestInstanceMock);
    });

    it('should return object with empty id on error', async () => {
      const res = await fetchEdrLatestInstances(
        'http://url-that-doesnt-have-instances-api.com',
        '',
      );
      expect(res.id).toEqual('');
    });
  });

  describe('getEdrParameter', () => {
    const parameter: Parameter = {
      plotId: 'plotId1',
      plotType: 'line',
      propertyName: parameterNameMock,
      serviceId: 'fmi',
      collectionId: EDR_FINLAND_ECMMF_COLLECTION_ID,
      instanceId: '20230927T120000',
    };
    it('should fetch data', async () => {
      const paramWithData = await getEdrParameter(parameter, EDR_FINLAND_URL, {
        lat: 1.2,
        lon: 3.4,
      });
      expect(paramWithData).toEqual({
        ...parameter,
        unit: parameterUnitMock,
        timestep: timestepMock.map((time) => new Date(time)),
        value: valueMock,
      });
    });

    it('should return null on error', async () => {
      const paramWithData = await getEdrParameter(
        {
          ...parameter,
          propertyName: 'NotSupportedProperty',
        },
        EDR_FINLAND_URL,
        { lat: 1.2, lon: 3.4 },
      );
      expect(paramWithData).toEqual(null);
    });
  });

  describe('getCollectionListForTimeSeriesService', () => {
    it('should return collection list for timeseries service', async () => {
      const expectedResult = [
        {
          collectionId: 'ecmwf',
          serviceId: 'fmi_ecmwf',
          serviceName: 'test',
          parameters: [
            {
              plotType: 'line',
              propertyName: 'Temperature',
              unit: ' ',
              serviceId: 'fmi_ecmwf',
              collectionId: 'ecmwf',
              color: 'X',
              opacity: 70,
            },
          ],
        },
      ];
      const service: TimeSeriesService = {
        name: 'test',
        id: 'fmi_ecmwf',
        description: 'fmi timeseries edr endpoint',
        url: 'https://opendata.fmi.fi/edr',
        type: 'EDR',
      };
      const result = await getCollectionListForTimeSeriesService(service);
      expect(result).toEqual(expectedResult);
    });
    it('should return collection list for timeseries service when name is missing', async () => {
      const expectedResult = [
        {
          collectionId: 'ecmwf',
          serviceId: 'fmi_ecmwf',
          serviceName: 'fmi_ecmwf',
          parameters: [
            {
              plotType: 'line',
              propertyName: 'Temperature',
              unit: ' ',
              serviceId: 'fmi_ecmwf',
              collectionId: 'ecmwf',
              color: 'X',
              opacity: 70,
            },
          ],
        },
      ];
      const service = {
        id: 'fmi_ecmwf',
        description: 'fmi timeseries edr endpoint',
        url: 'https://opendata.fmi.fi/edr',
        type: 'EDR',
      } as unknown as TimeSeriesService;
      const result = await getCollectionListForTimeSeriesService(service);
      expect(result).toEqual(expectedResult);
    });
  });

  describe('getParameterListForCollectionId', () => {
    it('should return collection for given id for timeseries service', async () => {
      const expectedResult = {
        collectionId: 'ecmwf',
        parameters: [
          {
            collectionId: 'ecmwf',
            color: 'X',
            opacity: 70,
            plotType: 'line',
            propertyName: 'Temperature',
            serviceId: 'fmi_ecmwf',
            unit: ' ',
          },
        ],
        serviceId: 'fmi_ecmwf',
        serviceName: 'test',
      };
      const service: TimeSeriesService = {
        name: 'test',
        id: 'fmi_ecmwf',
        description: 'fmi timeseries edr endpoint',
        url: 'https://opendata.fmi.fi/edr',
        type: 'EDR',
      };
      const result = await getParameterListForCollectionId(service, 'ecmwf');
      expect(result).toEqual(expectedResult);
    });
  });

  describe('clipEdrInterval', () => {
    it('should not clip dates if timerange is within cliprange', async () => {
      const [start, end] = clipEdrInterval(
        ['2020-06-10T12:00:00Z', '2020-06-10T18:00:00Z'],
        3,
        3,
        dateUtils.isoStringToDate('2020-06-10T15:00:00Z', false)!.getTime(),
      );
      expect(start).toEqual('2020-06-10T12:00:00Z');
      expect(end).toEqual('2020-06-10T18:00:00Z');
    });
    it('should clip dates if timerange is outside cliprange', async () => {
      const [start, end] = clipEdrInterval(
        ['2020-06-10T12:00:00Z', '2020-06-10T18:00:00Z'],
        2,
        2,
        dateUtils.isoStringToDate('2020-06-10T15:00:00Z', false)!.getTime(),
      );
      expect(start).toEqual('2020-06-10T13:00:00Z');
      expect(end).toEqual('2020-06-10T17:00:00Z');
    });

    it('should clip start if start timerange is outside cliprange', async () => {
      const [start, end] = clipEdrInterval(
        ['2010-06-10T12:00:00Z', '2020-06-10T18:00:00Z'],
        12,
        2,
        dateUtils.isoStringToDate('2020-06-10T18:00:00Z', false)!.getTime(),
      );
      expect(start).toEqual('2020-06-10T06:00:00Z');
      expect(end).toEqual('2020-06-10T18:00:00Z');
    });
    it('should clip end if end timerange is outside cliprange', async () => {
      const [start, end] = clipEdrInterval(
        ['2020-06-10T12:00:00Z', '2020-06-10T23:00:00Z'],
        100,
        2,
        dateUtils.isoStringToDate('2020-06-10T18:00:00Z', false)!.getTime(),
      );
      expect(start).toEqual('2020-06-10T12:00:00Z');
      expect(end).toEqual('2020-06-10T20:00:00Z');
    });
    it('should make a reasonable time range of KNMI EDR interval', async () => {
      const [start, end] = clipEdrInterval(
        ['2003-06-10T12:00:00Z', '2020-06-10T17:00:00Z'],
        12,
        48,
        dateUtils.isoStringToDate('2020-06-10T18:00:00Z', false)!.getTime(),
      );
      expect(start).toEqual('2020-06-10T06:00:00Z');
      expect(end).toEqual('2020-06-10T17:00:00Z');
    });
  });
});
