/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { rest } from 'msw';
import produce from 'immer';
import {
  EDRCollection,
  EDRDomain,
  EDRInstance,
  EDRParameter,
  EDRParameters,
  EDRPositionResponse,
  EDRRange,
  EDRRangeParameter,
} from '../lib/components/TimeSeries/types';

export const EDR_FINLAND_URL = 'https://opendata.fmi.fi/edr';
export const EDR_FINLAND_ECMMF_COLLECTION_ID = 'ecmwf';

export const EDR_NORWAY_URL = 'https://interpol-b.met.no';
export const EDR_NORWAY_COLLECTION_ID = 'meps-det-vdiv';

export const OGC_URL =
  'https://geoservices.knmi.nl/ogcapi/collections/HARM_N25/items';
const date = `20230927`;
export const latestInstanceIdMock = `${date}T120000`;
const oldestInstanceIdMock = `${date}T000000`;
export const latestInstanceMock: EDRInstance = {
  id: latestInstanceIdMock,
  extent: {
    spatial: {
      crs: 'CRS:84',
    },
    temporal: {
      interval: [['2023-09-26T03:00:00Z', '2023-10-06T00:00:00Z']],
    },
  },
  data_queries: {
    position: {
      link: {
        variables: {
          output_formats: ['CoverageJSON'],
        },
      },
    },
  },
};

export const oldInstanceMock: EDRInstance = {
  id: oldestInstanceIdMock,
  extent: {
    spatial: {
      crs: 'CRS:84',
    },
    temporal: {
      interval: [['2023-09-26T06:00:00Z', '2023-10-06T03:00:00Z']],
    },
  },
  data_queries: {
    position: {
      link: {
        variables: {
          output_formats: ['CoverageJSON'],
        },
      },
    },
  },
};

export const helsinkiLat = 60.192059;
export const helsinkiLon = 24.945831;
export const helsinki = 'Helsinki';

export const parameterNameMock = 'Temperature';
export const parameterUnitMock = '˚C';

export const timestepMock = [
  '2023-09-27T03:00:00.000Z',
  '2023-09-27T04:00:00.000Z',
  '2023-09-27T05:00:00.000Z',
];
export const valueMock = [1020, 1020, 1020];

const edrParameters = {
  [parameterNameMock]: {
    id: parameterNameMock,
    unit: {
      symbol: {
        value: parameterUnitMock,
      },
    },
  } as EDRParameter,
} as EDRParameters;
export const edrPositionResponseMock = {
  domain: {
    axes: {
      t: {
        values: timestepMock,
      },
    },
  } as EDRDomain,
  parameters: edrParameters,
  ranges: {
    [parameterNameMock]: {
      values: valueMock,
    } as EDRRangeParameter,
  } as EDRRange,
} as EDRPositionResponse;

const oldestInstanceMock = produce(oldInstanceMock, (draft) => {
  draft.id = oldestInstanceIdMock;
});
const locations = {
  features: [
    {
      id: 'helsinki',
      geometry: {
        coordinates: [helsinkiLon, helsinkiLat],
      },
      properties: {
        name: helsinki,
      },
    },
  ],
};

export const collectionsMockNorway: EDRCollection = {
  id: EDR_NORWAY_COLLECTION_ID,
  title: EDR_NORWAY_COLLECTION_ID,
  description: EDR_NORWAY_COLLECTION_ID,
};

export const collectionsMockFinland: EDRCollection = {
  id: EDR_FINLAND_ECMMF_COLLECTION_ID,
  title: EDR_FINLAND_ECMMF_COLLECTION_ID,
  description: EDR_FINLAND_ECMMF_COLLECTION_ID,
};

export const edrEndpoints = [
  rest.get(`${EDR_FINLAND_URL}/collections`, (_, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        collections: [collectionsMockFinland],
      }),
    );
  }),
  rest.get(`${EDR_NORWAY_URL}/collections`, (_, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        collections: [collectionsMockNorway],
      }),
    );
  }),
  rest.get(
    `${EDR_FINLAND_URL}/collections/${EDR_FINLAND_ECMMF_COLLECTION_ID}/instances`,
    (_, res, ctx) => {
      return res(
        ctx.status(200),
        ctx.json({
          instances: [latestInstanceMock, oldestInstanceMock],
        }),
      );
    },
  ),
  rest.get(
    `${EDR_NORWAY_URL}/collections/${EDR_NORWAY_COLLECTION_ID}/instances`,
    (_, res, ctx) => {
      return res(
        ctx.status(200),
        ctx.json({
          instances: [latestInstanceMock, oldestInstanceMock],
        }),
      );
    },
  ),
  rest.get(
    `${EDR_FINLAND_URL}/collections/${EDR_FINLAND_ECMMF_COLLECTION_ID}/locations`,
    (_, res, ctx) => {
      return res(ctx.status(200), ctx.json(locations));
    },
  ),
  rest.get(
    `${EDR_NORWAY_URL}/collections/${EDR_NORWAY_COLLECTION_ID}/locations`,
    (_, res, ctx) => {
      return res(ctx.status(200), ctx.json(locations));
    },
  ),
  // these endpoints are only used by norway
  rest.get(
    `${EDR_NORWAY_URL}/collections/${EDR_NORWAY_COLLECTION_ID}/instances/${latestInstanceIdMock}`,
    (_, res, ctx) => {
      return res(ctx.status(200), ctx.json({ parameters: edrParameters }));
    },
  ),
  rest.get(
    `${EDR_NORWAY_URL}/collections/${EDR_NORWAY_COLLECTION_ID}/instances/${latestInstanceIdMock}/locations/:id`,
    (_, res, ctx) => {
      return res(ctx.status(200), ctx.json(edrPositionResponseMock));
    },
  ),
  // these endpoints are only used by finland
  rest.get(
    `${EDR_FINLAND_URL}/collections/${EDR_FINLAND_ECMMF_COLLECTION_ID}`,
    (_, res, ctx) => {
      return res(ctx.status(200), ctx.json({ parameter_names: edrParameters }));
    },
  ),
  rest.get(
    `${EDR_FINLAND_URL}/collections/${EDR_FINLAND_ECMMF_COLLECTION_ID}/instances/${latestInstanceIdMock}/position`,
    (req, res, ctx) => {
      // if not supported parameter return error status
      if (req.url.searchParams.get('parameter-name') !== parameterNameMock) {
        return res(ctx.status(400));
      }

      return res(ctx.status(200), ctx.json(edrPositionResponseMock));
    },
  ),
];
export const ogcEndpoints = [
  rest.get(OGC_URL, (_, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        features: [
          {
            properties: {
              timestep: timestepMock,
              result: valueMock,
            },
          },
        ],
      }),
    );
  }),
];
export const unsupportedEndpoints = [
  rest.get('*', (_, res, ctx) => {
    return res(ctx.status(400));
  }),
];
