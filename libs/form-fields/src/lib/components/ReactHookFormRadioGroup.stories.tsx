/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { Button, FormControlLabel, Radio, Typography } from '@mui/material';
import * as React from 'react';
import { useFormContext } from 'react-hook-form';
import { ReactHookFormRadioGroup, ReactHookFormProvider } from '.';
import { FormFieldsWrapper, zeplinLinks } from './Providers';

export default {
  title: 'ReactHookForm/Radio Group',
};

const ReactHookFormRadioDemoLayout = (): React.ReactElement => {
  const { handleSubmit } = useFormContext();

  return (
    <>
      <Typography>Row:</Typography>
      <ReactHookFormRadioGroup
        name="radio1"
        row
        rules={{
          required: true,
        }}
      >
        <FormControlLabel value="one" control={<Radio />} label="One" />
        <FormControlLabel value="two" control={<Radio />} label="Two" />
        <FormControlLabel value="three" control={<Radio />} label="Three" />
      </ReactHookFormRadioGroup>

      <Typography>List:</Typography>
      <ReactHookFormRadioGroup
        name="radio2"
        rules={{
          required: true,
          validate: {
            eatApples: (value: string): boolean | string =>
              value === 'apples' || 'You should eat more apples',
          },
        }}
      >
        <FormControlLabel value="bananas" control={<Radio />} label="Bananas" />
        <FormControlLabel value="apples" control={<Radio />} label="Apples" />
      </ReactHookFormRadioGroup>

      <Typography>Disabled:</Typography>
      <ReactHookFormRadioGroup
        name="radio3"
        rules={{
          required: true,
        }}
        disabled
      >
        <FormControlLabel
          key="apples"
          value="apples"
          control={<Radio />}
          label="Apples"
        />
        <FormControlLabel
          key="bananas"
          value="bananas"
          control={<Radio />}
          label="Bananas"
        />
      </ReactHookFormRadioGroup>

      <Button
        variant="contained"
        color="secondary"
        onClick={(): void => {
          handleSubmit(() => {})();
        }}
      >
        Validate
      </Button>
    </>
  );
};

export const RadioGroup = (): React.ReactElement => (
  <FormFieldsWrapper
    options={{
      mode: 'onChange',
      reValidateMode: 'onChange',
      defaultValues: {
        radio3: 'bananas',
        radio2: 'bananas',
      },
    }}
  >
    <ReactHookFormRadioDemoLayout />
  </FormFieldsWrapper>
);

RadioGroup.parameters = {
  zeplinLink: zeplinLinks,
};

// snapshots
const RadioGroupsSnapshotLayout = (): React.ReactElement => (
  <ReactHookFormProvider
    options={{
      mode: 'onChange',
      reValidateMode: 'onChange',
      defaultValues: {
        radio3: 'bananas',
        radio2: 'bananas',
        readonly: 'apples',
      },
    }}
  >
    <ReactHookFormRadioGroup
      name="radio1"
      row
      rules={{
        required: true,
      }}
    >
      <FormControlLabel value="one" control={<Radio />} label="One" />
      <FormControlLabel value="two" control={<Radio />} label="Two" />
      <FormControlLabel value="three" control={<Radio />} label="Three" />
    </ReactHookFormRadioGroup>

    <ReactHookFormRadioGroup
      name="radio2"
      rules={{
        required: true,
        validate: {
          eatApples: (value: string): boolean | string =>
            value === 'apples' || 'You should eat more apples',
        },
      }}
    >
      <FormControlLabel value="bananas" control={<Radio />} label="Bananas" />
      <FormControlLabel value="apples" control={<Radio />} label="Apples" />
    </ReactHookFormRadioGroup>

    <ReactHookFormRadioGroup
      name="radio3"
      rules={{
        required: true,
      }}
      disabled
    >
      <FormControlLabel
        key="bananas"
        value="bananas"
        control={<Radio />}
        label="Bananas"
      />
      <FormControlLabel
        key="apples"
        value="apples"
        control={<Radio />}
        label="Apples"
      />
    </ReactHookFormRadioGroup>

    <ReactHookFormRadioGroup
      name="readonly"
      rules={{
        required: true,
      }}
      disabled
      isReadOnly
    >
      <FormControlLabel
        key="bananas"
        value="bananas"
        control={<Radio />}
        label="Bananas"
      />
      <FormControlLabel
        key="apples"
        value="apples"
        control={<Radio />}
        label="Apples"
      />
    </ReactHookFormRadioGroup>
  </ReactHookFormProvider>
);

// light theme
export const RadioGroupLightTheme = (): React.ReactElement => (
  <FormFieldsWrapper>
    <RadioGroupsSnapshotLayout />
  </FormFieldsWrapper>
);

RadioGroupLightTheme.storyName = 'Radio group light theme (takeSnapshot)';
RadioGroupLightTheme.parameters = {
  zeplinLink: [zeplinLinks[0]],
};

// dark theme
export const RadioGroupDarkTheme = (): React.ReactElement => (
  <FormFieldsWrapper isDarkTheme>
    <RadioGroupsSnapshotLayout />
  </FormFieldsWrapper>
);

RadioGroupDarkTheme.storyName = 'Radio group dark theme (takeSnapshot)';
RadioGroupDarkTheme.parameters = {
  zeplinLink: [zeplinLinks[1]],
};
