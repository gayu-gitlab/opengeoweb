/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import { renderHook } from '@testing-library/react';
import { usePoller } from './usePoller';

describe('src/utils/hooks/usePoller', () => {
  beforeEach(() => {
    jest.useFakeTimers();
    jest.spyOn(global, 'setInterval');
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });
  it('should trigger action function after timer', () => {
    const data = ['test', 'test', 'test'];
    const actionFunc = jest.fn();

    renderHook(() => usePoller([data], actionFunc));

    expect(setInterval).toHaveBeenCalledTimes(1);

    // Expect to have been called with default pollingInterval - 300000
    expect(setInterval).toHaveBeenLastCalledWith(expect.any(Function), 300000);
    jest.runOnlyPendingTimers();

    expect(actionFunc).toHaveBeenCalled();
  });
  it('should call interval with passed pollingInterval', () => {
    const data = ['test', 'test', 'test'];
    const actionFunc = jest.fn();

    renderHook(() => usePoller([data], actionFunc, 10000));

    expect(setInterval).toHaveBeenCalledTimes(1);
    expect(setInterval).toHaveBeenLastCalledWith(expect.any(Function), 10000);
    jest.runOnlyPendingTimers();

    expect(actionFunc).toHaveBeenCalled();
  });

  it('should not trigger action if no dependencies are passed in and default conditions are used', () => {
    const actionFunc = jest.fn();

    renderHook(() => usePoller([null], actionFunc));

    expect(setInterval).not.toHaveBeenCalled();
  });

  it('should clear timer if already interval running', () => {
    const intitalDeps = ['test', 'test', 'test'];
    const actionFunc = jest.fn();

    const { rerender } = renderHook(
      ({ intitalDeps }) => usePoller(intitalDeps, actionFunc, 10000),
      {
        initialProps: { intitalDeps },
      },
    );

    expect(setInterval).toHaveBeenCalledTimes(1);
    expect(setInterval).toHaveBeenLastCalledWith(expect.any(Function), 10000);

    // Change dependencies so the useEffect is triggered
    const data2 = ['test22', 'test', 'test'];

    rerender({ intitalDeps: data2 });
    // expect(clearInterval).toHaveBeenCalled();
    expect(setInterval).toHaveBeenCalledTimes(2);
    expect(setInterval).toHaveBeenLastCalledWith(expect.any(Function), 10000);
  });
});
