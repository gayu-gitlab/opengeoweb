/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { darkTheme, lightTheme, ThemeWrapper } from '@opengeoweb/theme';

import Avatar from './Avatar';

export default {
  title: 'components/Avatar',
};

const Avatars = (): React.ReactElement => {
  return (
    <div style={{ width: '50px', padding: 10 }}>
      <Avatar>G</Avatar>
      <br />
      <Avatar>GW</Avatar>
      <br />
      <Avatar size="small">G</Avatar>
      <br />
      <Avatar size="small">GW</Avatar>
    </div>
  );
};

export const AvatarLight = (): React.ReactElement => (
  <ThemeWrapper theme={lightTheme}>
    <Avatars />
  </ThemeWrapper>
);

AvatarLight.storyName = 'Avatar Light (takeSnapshot)';

AvatarLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60ec68c87ed4860e1d14f5dd',
    },
  ],
};

export const AvatarDark = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <Avatars />
  </ThemeWrapper>
);

AvatarDark.storyName = 'Avatar Dark (takeSnapshot)';

AvatarDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/60ec68cb9e0ee610eaf967ec',
    },
  ],
};
