/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import ToolContainer from './ToolContainer';

describe('components/ToolContainer', () => {
  it('should render successfully', () => {
    const props = {
      onClose: jest.fn(),
    };
    const { baseElement } = render(
      <ThemeWrapper>
        <ToolContainer {...props} />
      </ThemeWrapper>,
    );
    expect(baseElement).toBeTruthy();
    expect(screen.queryByRole('contentinfo')).toBeFalsy();
  });

  it('should render children and footer', () => {
    const props = {
      onClose: jest.fn(),
      isResizable: true,
    };

    const { baseElement } = render(
      <ThemeWrapper>
        <ToolContainer {...props}>
          <div role="article">test children</div>
        </ToolContainer>
      </ThemeWrapper>,
    );
    expect(baseElement).toBeTruthy();
    expect(screen.getByRole('contentinfo')).toBeTruthy();
    expect(screen.getByRole('article')).toBeTruthy();
    fireEvent.click(screen.queryByTestId('closeBtn')!);
    expect(props.onClose).toHaveBeenCalled();
  });

  it('should pass headerClass', () => {
    const props = {
      onClose: jest.fn(),
      headerClassName: 'test-classname',
      title: 'testing',
    };
    const { baseElement } = render(
      <ThemeWrapper>
        <ToolContainer {...props} />
      </ThemeWrapper>,
    );
    expect(baseElement).toBeTruthy();
    expect(
      screen
        .getByRole('heading', { name: 'toolheader' })
        .classList.contains(props.headerClassName),
    ).toBeTruthy();
  });

  it('should pass custom leftHeaderComponent', () => {
    const TestComponent = (
      <div data-testid="test-leftHeaderComponent">testing left component</div>
    );
    render(
      <ThemeWrapper>
        <ToolContainer leftHeaderComponent={TestComponent} />
      </ThemeWrapper>,
    );
    expect(screen.queryByTestId('dragBtn')).toBeFalsy();
    expect(screen.getByTestId('test-leftHeaderComponent')).toBeTruthy();
  });

  it('should pass custom rightHeaderComponent', () => {
    const TestComponent = (
      <div data-testid="test-rightHeaderComponent">testing right component</div>
    );
    render(
      <ThemeWrapper>
        <ToolContainer rightHeaderComponent={TestComponent} />
      </ThemeWrapper>,
    );
    expect(screen.queryByTestId('dragBtn')).toBeFalsy();
    expect(screen.getByTestId('test-rightHeaderComponent')).toBeTruthy();
  });

  it('should use default xs header', () => {
    const TestComponent = (
      <div data-testid="test-rightHeaderComponent">testing right component</div>
    );
    render(
      <ThemeWrapper>
        <ToolContainer
          title="test title"
          rightHeaderComponent={TestComponent}
        />
      </ThemeWrapper>,
    );
    expect(
      getComputedStyle(screen.getByRole('heading', { name: /test title/i }))
        .fontSize,
    ).toEqual('12px');
  });
});
