/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Select, SelectProps, SxProps, useTheme } from '@mui/material';
import { Theme } from '@mui/material/styles';
import { tooltipContainerStyles } from './tooltipContainerStyles';
import { CustomTooltip, useControlledTooltip } from '../CustomTooltip';

interface ListELement {
  value: string;
}
interface TooltipSelectProps extends SelectProps {
  tooltip: string;
  list?: ListELement[];
  currentIndex?: number;
  onChangeMouseWheel?: (element: ListELement) => void;
  requiresCtrlToChange?: boolean;
  isEnabled?: boolean;
  noStyling?: boolean;
  hasBackgroundColor?: boolean;
}

export const TooltipSelect: React.FC<TooltipSelectProps> = ({
  tooltip,
  list = [],
  currentIndex = -1,
  onChangeMouseWheel = (): void => {},
  requiresCtrlToChange = false,
  isEnabled,
  noStyling,
  hasBackgroundColor,
  ...props
}: TooltipSelectProps) => {
  const [selectOpen, setSelectOpen] = React.useState(false);
  const [tooltipOpen, setTooltipOpen] = useControlledTooltip();

  const theme = useTheme();

  const onWheel = React.useCallback(
    (event: React.WheelEvent): void => {
      if (
        (requiresCtrlToChange && (event.ctrlKey || event.metaKey)) ||
        !requiresCtrlToChange
      ) {
        if (selectOpen) {
          return;
        }
        const direction = event.deltaY < 0 ? 1 : -1;
        const newIndex = currentIndex - direction;
        if (newIndex < 0 || newIndex >= list.length) {
          return;
        }
        onChangeMouseWheel(list[newIndex]);
      }
    },
    [selectOpen, currentIndex, list, onChangeMouseWheel, requiresCtrlToChange],
  );
  const onOpen = React.useCallback(() => {
    setSelectOpen(true);
    setTooltipOpen(false);
  }, [setTooltipOpen]);
  const onClose = React.useCallback(() => {
    setSelectOpen(false);
  }, []);
  const onMouseEnter = React.useCallback(() => {
    if (!selectOpen) {
      setTooltipOpen(true);
    }
  }, [selectOpen, setTooltipOpen]);
  const onMouseLeave = React.useCallback(() => {
    setTooltipOpen(false);
  }, [setTooltipOpen]);
  const onFocus = React.useCallback(() => {
    setTooltipOpen(true);
  }, [setTooltipOpen]);
  const onBlur = React.useCallback(() => {
    setTooltipOpen(false);
  }, [setTooltipOpen]);

  const selectStyles = noStyling
    ? {}
    : tooltipContainerStyles(!!isEnabled, hasBackgroundColor);
  return (
    <CustomTooltip title={tooltip} placement="top" open={tooltipOpen}>
      <Select
        variant="standard"
        disableUnderline
        onOpen={onOpen}
        onClose={onClose}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
        onFocus={onFocus}
        onBlur={onBlur}
        onWheel={onWheel}
        MenuProps={{
          BackdropProps: {
            sx: {
              '&.MuiBackdrop-root': {
                backgroundColor: 'transparent',
              },
            },
          },
          sx: {
            '& ul': {
              backgroundColor: 'geowebColors.background.surface',
            },
            '& li': {
              fontSize: '16px',
              padding: '12px',
              '&:hover': {
                backgroundColor: 'geowebColors.buttons.tool.mouseOver.fill',
              },
              '&.Mui-selected': {
                color: 'geowebColors.buttons.tool.active.color',
                backgroundColor: 'geowebColors.buttons.tool.active.fill',
                boxShadow: `none`,
                '&:hover': {
                  backgroundColor:
                    'geowebColors.buttons.tool.activeMouseOver.fill',
                },
                '&:focus': {
                  backgroundColor:
                    'geowebColors.buttons.tool.activeMouseOver.fill',
                },
                '.MuiListItemIcon-root': {
                  color: 'geowebColors.buttons.tool.active.color',
                },
              },
              '&:after': {
                background: 'none!important',
              },
              '&.Mui-disabled': {
                fontSize: '12px',
                opacity: 0.67,
                padding: '0px 12px',
                '&.MuiMenuItem-root': {
                  minHeight: '30px!important',
                },
              },
            },
            ...theme.palette.geowebColors.tooltips.tooltipSelect.menu,
          },
        }}
        sx={
          {
            ...selectStyles,
            ...theme.palette.geowebColors.tooltips.tooltipSelect.select,
          } as SxProps<Theme>
        }
        {...props}
      />
    </CustomTooltip>
  );
};
