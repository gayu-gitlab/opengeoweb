/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Card, Grid } from '@mui/material';
import { ThemeWrapper, darkTheme } from '@opengeoweb/theme';
import SearchHighlight from './SearchHighlight';

export default {
  title: 'components/SearchHighlight',
};

const testString =
  'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras non blandit tellus. Donec dignissim odio vitae ex vestibulum commodo.';

const SearchHighlightDemo = (): React.ReactElement => (
  <Card sx={{ padding: 1, maxWidth: 800 }}>
    <Grid container spacing={5}>
      <Grid item>
        <SearchHighlight text={testString} search="adipiscing Donec" />
        <br />
        <SearchHighlight
          text={testString}
          search="consectetur adipiscing elit"
        />
        <br />
        <SearchHighlight text={testString} search="Lorem Cras COMMODO" />
      </Grid>
    </Grid>
  </Card>
);

export const SearchHighlightLightTheme = (): React.ReactElement => (
  <ThemeWrapper>
    <SearchHighlightDemo />
  </ThemeWrapper>
);

SearchHighlightLightTheme.storyName = 'SearchHighlight light (takeSnapshot)';

export const SearchHighlightDarkTheme = (): React.ReactElement => (
  <ThemeWrapper theme={darkTheme}>
    <SearchHighlightDemo />
  </ThemeWrapper>
);

SearchHighlightDarkTheme.storyName = 'SearchHighlight dark (takeSnapshot)';
