/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import { ThemeProvider } from '@opengeoweb/theme';
import LastUpdateTime, {
  BUTTON_TITLE,
  getLastUpdateTitle,
} from './LastUpdateTime';

describe('components/TafLayout/LastUpdateTime', () => {
  it('should display last update time and update taf', async () => {
    const props = {
      onPressRefresh: jest.fn(),
      lastUpdateTime: '10:11',
    };

    render(
      <ThemeProvider>
        <LastUpdateTime {...props} />
      </ThemeProvider>,
    );

    expect(
      screen.getByText(getLastUpdateTitle(props.lastUpdateTime)),
    ).toBeTruthy();

    expect(props.onPressRefresh).not.toHaveBeenCalled();

    const refreshButton = screen.getByRole('button');
    fireEvent.mouseOver(refreshButton);
    expect(await screen.findByText(BUTTON_TITLE)).toBeTruthy();

    fireEvent.click(refreshButton);
    expect(props.onPressRefresh).toHaveBeenCalledTimes(1);
  });

  it('should use prop dataTestId', async () => {
    const props = {
      onPressRefresh: jest.fn(),
      lastUpdateTime: '10:11',
      dataTestId: 'testing-button',
    };

    render(
      <ThemeProvider>
        <LastUpdateTime {...props} />
      </ThemeProvider>,
    );

    expect(screen.getByTestId(props.dataTestId)).toBeTruthy();
  });
});
