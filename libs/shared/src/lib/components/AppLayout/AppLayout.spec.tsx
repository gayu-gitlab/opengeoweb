/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render, screen } from '@testing-library/react';

import { ThemeWrapper } from '@opengeoweb/theme';
import AppLayout from './AppLayout';

interface TestComponentProps {
  title: string;
}

const TestComponent: React.FC<TestComponentProps> = ({
  title,
}: TestComponentProps) => <div>{title}</div>;

describe('components/AppLayout', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <ThemeWrapper>
        <AppLayout />
      </ThemeWrapper>,
    );
    expect(baseElement).toBeTruthy();
    expect(screen.getByTestId('appLayout')).toBeTruthy();
  });

  it('should render different layout parts', async () => {
    const props = {
      header: <TestComponent title="header" />,
    };
    render(
      <ThemeWrapper>
        <AppLayout {...props}>
          <TestComponent title="main" />
        </AppLayout>
      </ThemeWrapper>,
    );

    const header = await screen.findByText('header');
    const main = await screen.findByText('main');

    expect(header).toBeTruthy();
    expect(main).toBeTruthy();
  });
});
