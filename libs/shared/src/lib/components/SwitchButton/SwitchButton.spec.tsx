/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';

import { ThemeProvider } from '@opengeoweb/theme';
import SwitchButton from './SwitchButton';

describe('components/SwitchButton/SwitchButton', () => {
  it('should render SwitchButton with labels', () => {
    render(
      <ThemeProvider>
        <SwitchButton checked={false} onChange={jest.fn()} />
      </ThemeProvider>,
    );
    expect(screen.getByTestId('switchMode')).toBeTruthy();
    expect(screen.getByTestId('switchMode').classList).not.toContain(
      'Mui-checked',
    );
    expect(screen.getByText('Viewer')).toBeTruthy();
    expect(screen.getByText('Editor')).toBeTruthy();
  });

  it('should handle onChange', () => {
    const mockOnChange = jest.fn();
    const checked = true;
    render(
      <ThemeProvider>
        <SwitchButton checked={checked} onChange={mockOnChange} />
      </ThemeProvider>,
    );
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');
    fireEvent.click(screen.getByRole('checkbox'));
    expect(mockOnChange).toHaveBeenCalled();
  });
});
