/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render, screen } from '@testing-library/react';
import { ThemeWrapper } from '@opengeoweb/theme';
import ShortcutList, { getShortcutList, shortcuts } from './ShortcutList';
import ShortcutSegment from './ShortcutSegment';

describe('components/UserMenu/CheatSheet/Shortcutlist', () => {
  it('should work with default props', async () => {
    render(
      <ThemeWrapper>
        <ShortcutList />
      </ThemeWrapper>,
    );

    expect(screen.getByTestId('list-windows')).toBeTruthy();
    expect(screen.queryByTestId('list-mac')).toBeFalsy();
    expect(screen.getAllByRole('listitem')).toHaveLength(shortcuts.length);
  });

  it('should list platform mac', async () => {
    render(
      <ThemeWrapper>
        <ShortcutList platform="mac" />
      </ThemeWrapper>,
    );

    expect(screen.getByTestId('list-mac')).toBeTruthy();
    expect(screen.queryByTestId('list-windows')).toBeFalsy();
    expect(screen.getAllByRole('listitem')).toHaveLength(shortcuts.length);
  });

  describe('getShortcutList', () => {
    const list = [
      {
        title: 'Choose a selected time',
        description: (
          <ShortcutSegment>Left click on the time slider rail</ShortcutSegment>
        ),
      },

      {
        title: 'Move backwards/forwards in selected time',
        description: (
          <ShortcutSegment>
            Mouse scroll over the time slider rail
          </ShortcutSegment>
        ),
      },
      {
        title: 'Move 1 step backwards/forwards in selected time',
        description: (
          <>
            <ShortcutSegment type="icon">Ctrl</ShortcutSegment>
            <ShortcutSegment>+</ShortcutSegment>
            <ShortcutSegment type="icon">&larr;</ShortcutSegment>
            <ShortcutSegment type="divider">or</ShortcutSegment>
            <ShortcutSegment type="icon">&rarr;</ShortcutSegment>
          </>
        ),
        descriptionMac: (
          <>
            <ShortcutSegment type="icon">OPT</ShortcutSegment>
            <ShortcutSegment>+</ShortcutSegment>
            <ShortcutSegment type="icon">CMD</ShortcutSegment>
            <ShortcutSegment>+</ShortcutSegment>
            <ShortcutSegment type="icon">&larr;</ShortcutSegment>
            <ShortcutSegment type="divider">or</ShortcutSegment>
            <ShortcutSegment type="icon">&rarr;</ShortcutSegment>
          </>
        ),
      },
    ];

    it('should return list for platform windows', () => {
      const result = getShortcutList(list);
      expect(result[0]).toEqual(list[0]);
      expect(result[1]).toEqual(list[1]);
      expect(result[2]).toEqual({
        title: list[2].title,
        description: list[2].description,
      });
    });

    it('should return list for platform mac', () => {
      const result = getShortcutList(list, 'mac');
      expect(result[0]).toEqual(list[0]);
      expect(result[1]).toEqual(list[1]);
      expect(result[2]).toEqual({
        title: list[2].title,
        description: list[2].descriptionMac,
      });
    });
  });
});
