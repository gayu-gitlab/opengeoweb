/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import createSagaMiddleware from 'redux-saga';
import {
  createMockStoreWithEggs,
  createMockStoreWithEggsAndMiddleware,
  createToolkitMockStoreWithEggs,
} from './createMockStoreWithEggs';

describe('createMockStoreWithEggs', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  const mockState = {
    dummyValue: 1,
  };

  it('should return a mock store', () => {
    const store = createMockStoreWithEggs(mockState);
    expect(store.getState()).toEqual(mockState);
  });

  it('the mock store should contain addEggs', () => {
    const store = createMockStoreWithEggs({});
    expect(typeof store.addEggs).toBe('function');
    expect(jest.isMockFunction(store.addEggs)).toBeTruthy();
    store.addEggs!();
    expect(store.addEggs).toHaveBeenCalledTimes(1);
  });
});

describe('createMockStoreWithEggsAndMiddleware', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  const mockState = {
    dummyValue: 1,
  };
  const sagaMiddleware = createSagaMiddleware();

  it('should return a mock store', () => {
    const store = createMockStoreWithEggsAndMiddleware(
      mockState,
      sagaMiddleware,
    );
    expect(store.getState()).toEqual(mockState);
  });

  it('the mock store should contain addEggs', () => {
    const store = createMockStoreWithEggsAndMiddleware({}, sagaMiddleware);
    expect(typeof store.addEggs).toBe('function');
    expect(jest.isMockFunction(store.addEggs)).toBeTruthy();
    store.addEggs!();
    expect(store.addEggs).toHaveBeenCalledTimes(1);
  });
});

describe('createToolkitMockStoreWithEggs', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  const mockReducer = (state = {}): Record<string, unknown> => state;

  const mockState = {
    reducer: { mock: mockReducer },
    preloadedState: {
      mock: {
        testValue: 'example',
      },
    },
  };

  it('should return a mock store', () => {
    const store = createToolkitMockStoreWithEggs(mockState);
    expect(store.getState()).toEqual(mockState.preloadedState);
  });

  it('the mock store should contain addEggs', () => {
    const store = createToolkitMockStoreWithEggs(mockState);
    expect(typeof store.addEggs).toBe('function');
    expect(jest.isMockFunction(store.addEggs)).toBeTruthy();
    store.addEggs!();
    expect(store.addEggs).toHaveBeenCalledTimes(1);
  });
});
