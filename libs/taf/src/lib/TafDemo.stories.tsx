/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Paper } from '@mui/material';
import { darkTheme } from '@opengeoweb/theme';
import { TafModule } from './components/TafModule';
import { TafThemeApiProvider } from './components/Providers';
import { createApi as createFakeApi } from './utils/fakeApi';

export default { title: 'demo' };

const Demo = (): React.ReactElement => (
  <Paper sx={{ maxHeight: '100%', height: '100vh', overflow: 'auto' }}>
    <TafModule />
  </Paper>
);

export const TafModuleLightTheme = (): React.ReactElement => {
  return (
    <TafThemeApiProvider createApiFunc={createFakeApi}>
      <Demo />
    </TafThemeApiProvider>
  );
};

TafModuleLightTheme.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61f2bfb9406dbcaf80a85144/version/631b188b60601f4cddd8eed6',
    },
  ],
};

export const TafModuleDarkTheme = (): React.ReactElement => {
  return (
    <TafThemeApiProvider theme={darkTheme} createApiFunc={createFakeApi}>
      <Demo />
    </TafThemeApiProvider>
  );
};
TafModuleDarkTheme.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61f2bfc3d95b46b1bff80bbc/version/631b18add13d5b493aa28996',
    },
  ],
};
