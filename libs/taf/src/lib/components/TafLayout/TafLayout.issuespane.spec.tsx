/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  act,
  fireEvent,
  render,
  waitFor,
  screen,
  within,
} from '@testing-library/react';

import TafLayout from './TafLayout';
import {
  fakeAmendmentFixedTaf,
  fakeDraftAmendmentFixedTaf,
  MOCK_USERNAME,
  previousTafForfakeDraftAmendmentFixedTaf,
} from '../../utils/mockdata/fakeTafList';
import { TafThemeApiProvider } from '../Providers';

jest.mock('../../utils/api');

describe('components/TafLayout/TafLayout - test issuesPane', () => {
  beforeEach(() => {
    jest.useFakeTimers();
    Element.prototype.scrollTo = jest.fn();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });
  it('should toggle the issues pane via issuesButton and dialog close button', async () => {
    const props = {
      tafList: [{ ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME }],
      onUpdateTaf: jest.fn(),
    };

    render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    const tafLocations = within(
      screen.getByTestId('location-tabs'),
    ).getAllByRole('menuitem');

    await waitFor(() => expect(tafLocations.length).toBe(1));

    expect(screen.queryByTestId('moveable-issues-pane')).toBeFalsy();

    // select taf that is in edit mode
    fireEvent.click(tafLocations[0]);
    await waitFor(() => {
      // check editor mode
      expect(screen.getByTestId('switchMode').classList).toContain(
        'Mui-checked',
      );
    });

    expect(screen.getByTestId('issuesButton')).toBeTruthy();
    fireEvent.click(screen.getByTestId('issuesButton'));

    expect(screen.getByTestId('moveable-issues-pane')).toBeTruthy();

    // close it via dialog close button
    fireEvent.click(screen.getByTestId('closeBtn'));
    expect(screen.queryByTestId('moveable-issues-pane')).toBeFalsy();

    // open again
    fireEvent.click(screen.getByTestId('issuesButton'));
    expect(screen.getByTestId('moveable-issues-pane')).toBeTruthy();

    // close it via issuesButton
    fireEvent.click(screen.getByTestId('issuesButton'));
    expect(screen.queryByTestId('moveable-issues-pane')).toBeFalsy();
  });

  it('should toggle the issues pane and should keep being opened while changing TAFs', async () => {
    const props = {
      tafList: [
        { ...fakeAmendmentFixedTaf, editor: MOCK_USERNAME },
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
      ],
      onUpdateTaf: jest.fn(),
    };

    render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    const tafLocations = within(
      screen.getByTestId('location-tabs'),
    ).getAllByRole('menuitem');

    await waitFor(() => expect(tafLocations.length).toBe(2));

    expect(screen.queryByTestId('moveable-issues-pane')).toBeFalsy();

    // select taf that is in edit mode
    fireEvent.click(tafLocations[1]);
    await waitFor(() => {
      // check editor mode
      expect(screen.getByTestId('switchMode').classList).toContain(
        'Mui-checked',
      );
    });

    expect(screen.getByTestId('issuesButton')).toBeTruthy();
    fireEvent.click(screen.getByTestId('issuesButton'));
    await waitFor(() => {
      expect(screen.getByTestId('moveable-issues-pane')).toBeTruthy();
    });
    // switch back to taf which is in view mode
    fireEvent.click(tafLocations[0]);
    await waitFor(() => {
      expect(screen.getByTestId('moveable-issues-pane')).toBeTruthy();
    });
    // close it
    fireEvent.click(screen.getByTestId('closeBtn'));
    await waitFor(() => {
      expect(screen.queryByTestId('moveable-issues-pane')).toBeFalsy();
    });
  });

  it('should open issues pane after submit and form has errors', async () => {
    const props = {
      tafList: [
        { ...fakeAmendmentFixedTaf, editor: MOCK_USERNAME },
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
        previousTafForfakeDraftAmendmentFixedTaf,
      ],
      onUpdateTaf: jest.fn(),
    };

    render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    const tafLocations = within(
      screen.getByTestId('location-tabs'),
    ).getAllByRole('menuitem');

    await waitFor(() => expect(tafLocations.length).toBe(props.tafList.length));

    expect(screen.queryByTestId('moveable-issues-pane')).toBeFalsy();

    // select taf that is in edit mode
    fireEvent.click(tafLocations[2]);

    await waitFor(() => {
      const tafLocation = tafLocations[2];
      expect(tafLocation.classList).toContain('Mui-selected');
    });
    expect(screen.getByText('draft amendment')).toBeTruthy();
    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    // eslint-disable-next-line testing-library/no-unnecessary-act
    await act(async () => {
      const wind = screen.getByRole('textbox', { name: 'baseForecast.wind' });
      fireEvent.change(wind, {
        target: { value: 'error' },
      });
      fireEvent.blur(wind);
    });

    fireEvent.click(screen.getByTestId('publishtaf'));

    await screen.findByTestId('moveable-issues-pane');
  });

  it('should not open issues pane after submit and form has no errors', async () => {
    const props = {
      tafList: [
        { ...fakeAmendmentFixedTaf, editor: MOCK_USERNAME },
        { ...fakeDraftAmendmentFixedTaf, editor: MOCK_USERNAME },
        previousTafForfakeDraftAmendmentFixedTaf,
      ],
      onUpdateTaf: jest.fn(),
    };

    render(
      <TafThemeApiProvider>
        <TafLayout {...props} />
      </TafThemeApiProvider>,
    );

    const tafLocations = within(
      screen.getByTestId('location-tabs'),
    ).getAllByRole('menuitem');

    await waitFor(() => expect(tafLocations.length).toBe(props.tafList.length));

    expect(screen.queryByTestId('moveable-issues-pane')).toBeFalsy();

    // select taf that is in edit mode
    fireEvent.click(tafLocations[2]);

    await waitFor(() => {
      const tafLocation = tafLocations[2];
      expect(tafLocation.classList).toContain('Mui-selected');
    });
    expect(screen.getByText('draft amendment')).toBeTruthy();
    // check editor mode
    expect(screen.getByTestId('switchMode').classList).toContain('Mui-checked');

    fireEvent.click(screen.getByTestId('publishtaf'));

    await screen.findByTestId('confirmationDialog-confirm');
    expect(screen.queryByTestId('moveable-issues-pane')).toBeFalsy();
  });
});
