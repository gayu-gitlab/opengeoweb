/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render, waitFor, act, screen } from '@testing-library/react';

import userEvent from '@testing-library/user-event';
import TacOverviewMobile from './TacOverviewMobile';
import { TafThemeApiProvider } from '../../Providers';
import {
  fakeAmendmentFixedTaf,
  fakeDraftAmendmentFixedTaf,
  fakeDraftFixedTaf,
  fakeNewFixedTaf,
  fakePublishedFixedTaf,
} from '../../../utils/mockdata/fakeTafList';
import { fakeTestTac } from '../../../utils/__mocks__/api';

jest.mock('../../../utils/api');

describe('components/TafLayout/TacOverviewMobile', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should open and close the mobile TAC Overview', async () => {
    Element.prototype.scrollTo = jest.fn();
    const upcomingTafs = [
      fakeNewFixedTaf,
      fakeDraftFixedTaf,
      fakePublishedFixedTaf,
    ];
    const currentTafs = [fakeDraftAmendmentFixedTaf, fakeAmendmentFixedTaf];
    const expiredTafs = upcomingTafs;
    const props = {
      upcomingTafs,
      currentTafs,
      expiredTafs,
    };

    render(
      <TafThemeApiProvider>
        <TacOverviewMobile {...props} />
      </TafThemeApiProvider>,
    );

    expect(screen.queryByTestId('tac-overview-mobile')).toBeFalsy();
    const user = userEvent.setup({ advanceTimers: jest.advanceTimersByTime });
    await user.click(screen.getByTestId('tac-overview-mobile-button'));

    expect(screen.getByTestId('tac-overview-mobile')).toBeTruthy();
    expect(screen.getAllByText('TAC Overview').length).toBe(1);

    await waitFor(() =>
      expect(
        screen.queryAllByTestId('adverse-weather-tac')[0].textContent,
      ).toContain(fakeTestTac),
    );
    const { children: panel1Children } = screen.getByTestId('tab-panel-0');
    expect(panel1Children).toHaveLength(upcomingTafs.length);
    expect(screen.queryByTestId('tab-panel-1')!.firstChild).toBeNull();
    act(() => {
      jest.runOnlyPendingTimers();
    });

    await waitFor(() =>
      expect(Element.prototype.scrollTo).toHaveBeenCalledTimes(1),
    );

    await user.click(screen.getByTestId('CloseIcon'));
    await waitFor(() =>
      expect(screen.queryByTestId('tac-overview-mobile')).toBeFalsy(),
    );
  });
});
