/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { darkTheme } from '@opengeoweb/theme';
import { TafThemeApiProvider } from '../../Providers';
import {
  fakeAmendmentFixedTaf,
  fakeDraftAmendmentFixedTaf,
  fakeDraftFixedTaf,
  fakeNewFixedTaf,
  fakePublishedFixedTaf,
} from '../../../utils/mockdata/fakeTafList';
import TacOverviewMobile from './TacOverviewMobile';
import { createApi as createFakeApi } from '../../../utils/fakeApi';

export default { title: 'components/Taf Layout/TacOverview' };

const TacOverviewMobileDemo = (): React.ReactElement => {
  return (
    <div style={{ height: '100vh' }}>
      <TacOverviewMobile
        upcomingTafs={[
          fakeDraftFixedTaf,
          fakePublishedFixedTaf,
          fakeNewFixedTaf,
        ]}
        currentTafs={[fakeDraftAmendmentFixedTaf, fakeAmendmentFixedTaf]}
        expiredTafs={[]}
        activeTaf={fakePublishedFixedTaf}
        defaultOpen
      />
    </div>
  );
};

export const TacOverviewMobileLightTheme = (): React.ReactElement => {
  return (
    <TafThemeApiProvider createApiFunc={createFakeApi}>
      <TacOverviewMobileDemo />
    </TafThemeApiProvider>
  );
};
TacOverviewMobileLightTheme.storyName =
  'Tac overview mobile light (takeSnapshot)';
TacOverviewMobileLightTheme.parameters = {
  viewport: {
    defaultViewport: 'mobile',
  },
};

export const TacOverviewMobileDarkTheme = (): React.ReactElement => {
  return (
    <TafThemeApiProvider theme={darkTheme} createApiFunc={createFakeApi}>
      <TacOverviewMobileDemo />
    </TafThemeApiProvider>
  );
};
TacOverviewMobileDarkTheme.storyName =
  'Tac overview mobile dark (takeSnapshot)';
TacOverviewMobileDarkTheme.parameters = {
  viewport: {
    defaultViewport: 'mobile',
  },
};
