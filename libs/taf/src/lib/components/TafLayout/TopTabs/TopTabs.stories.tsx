/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { darkTheme } from '@opengeoweb/theme';
import { TafThemeApiProvider } from '../../Providers';

import TopTabs from './TopTabs';
import { fakeAmendmentFixedTaf } from '../../../utils/mockdata/fakeTafList';

export default { title: 'components/Taf Layout/TopTabs' };

const TopTabsDemo = (): React.ReactElement => (
  <TopTabs timeSlot="UPCOMING" tafFromBackend={fakeAmendmentFixedTaf} />
);

export const TopTabsLightTheme = (): React.ReactElement => {
  return (
    <div style={{ width: 600, padding: 10, height: 100 }}>
      <TafThemeApiProvider>
        <TopTabsDemo />
      </TafThemeApiProvider>
    </div>
  );
};
TopTabsLightTheme.storyName = 'TopTabs light (takeSnapshot)';

export const TopTabsDarkTheme = (): React.ReactElement => {
  return (
    <div style={{ width: 680, padding: 10, height: 100 }}>
      <TafThemeApiProvider theme={darkTheme}>
        <TopTabsDemo />
      </TafThemeApiProvider>
    </div>
  );
};
TopTabsDarkTheme.storyName = 'TopTabs dark (takeSnapshot)';
