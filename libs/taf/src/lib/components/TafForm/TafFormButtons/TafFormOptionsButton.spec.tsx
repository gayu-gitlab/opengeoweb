/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { ThemeProvider } from '@opengeoweb/theme';
import TafFormOptionsButton from './TafFormOptionsButton';

describe('components/TafForm/TafFormButtons/TafFormOptionsButton', () => {
  it('should insert 1 row below', () => {
    const props = {
      isChangeGroup: true,
      addChangeGroupAbove: jest.fn(),
      addChangeGroupBelow: jest.fn(),
      removeChangeGroup: jest.fn(),
      clearRow: jest.fn(),
      index: 0,
    };

    render(
      <ThemeProvider>
        <TafFormOptionsButton {...props} />
      </ThemeProvider>,
    );
    fireEvent.click(screen.getByTestId('tafFormOptions[0]'));
    fireEvent.click(screen.queryByText('Insert 1 row below')!);
    expect(props.addChangeGroupBelow).toHaveBeenCalled();
    expect(props.removeChangeGroup).not.toHaveBeenCalled();
  });

  it('should insert 1 row above', () => {
    const props = {
      isChangeGroup: true,
      addChangeGroupAbove: jest.fn(),
      addChangeGroupBelow: jest.fn(),
      removeChangeGroup: jest.fn(),
      clearRow: jest.fn(),
      index: 0,
    };

    render(
      <ThemeProvider>
        <TafFormOptionsButton {...props} />
      </ThemeProvider>,
    );
    fireEvent.click(screen.getByTestId('tafFormOptions[0]'));
    fireEvent.click(screen.queryByText('Insert 1 row above')!);
    expect(props.addChangeGroupAbove).toHaveBeenCalled();
  });

  it('should delete a changegroup row', () => {
    const props = {
      isChangeGroup: true,
      addChangeGroupAbove: jest.fn(),
      addChangeGroupBelow: jest.fn(),
      removeChangeGroup: jest.fn(),
      clearRow: jest.fn(),
      index: 0,
    };

    render(
      <ThemeProvider>
        <TafFormOptionsButton {...props} />
      </ThemeProvider>,
    );
    fireEvent.click(screen.getByTestId('tafFormOptions[0]'));
    fireEvent.click(screen.queryByText('Delete row')!);
    expect(props.removeChangeGroup).toHaveBeenCalled();
  });

  it('should clear a row', () => {
    const props = {
      isChangeGroup: true,
      addChangeGroupAbove: jest.fn(),
      addChangeGroupBelow: jest.fn(),
      removeChangeGroup: jest.fn(),
      clearRow: jest.fn(),
      index: 0,
    };

    render(
      <ThemeProvider>
        <TafFormOptionsButton {...props} />
      </ThemeProvider>,
    );
    fireEvent.click(screen.getByTestId('tafFormOptions[0]'));
    fireEvent.click(screen.queryByText('Clear row')!);
    expect(props.clearRow).toHaveBeenCalled();
  });

  it('should show the correct menu items for the baseforecast row', async () => {
    const props = {
      isChangeGroup: false,
      addChangeGroupAbove: jest.fn(),
      addChangeGroupBelow: jest.fn(),
      removeChangeGroup: jest.fn(),
      clearRow: jest.fn(),
      index: -1,
    };

    render(
      <ThemeProvider>
        <TafFormOptionsButton {...props} />
      </ThemeProvider>,
    );
    fireEvent.click(screen.getByTestId('tafFormOptions[-1]'));
    expect(screen.getByText('Insert 1 row below')).toBeTruthy();
    expect(screen.getByText('Clear row')).toBeTruthy();
    expect(screen.queryByText('Delete row')).toBeFalsy();
    expect(screen.queryByText('Insert 1 row above')).toBeFalsy();
  });
});
