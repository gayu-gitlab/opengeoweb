/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import {
  ToolContainerDraggable,
  calculateDialogSizeAndPosition,
} from '@opengeoweb/shared';
import { darkTheme } from '@opengeoweb/theme';
import { Paper } from '@mui/material';
import { TafThemeApiProvider } from '../Providers';

import TafModule from './TafModule';
import { fakeTafTac, createApi as createFakeApi } from '../../utils/fakeApi';
import { TafFromBackend } from '../../types';
import {
  fakeCancelledFixedTaf,
  fakeDraftAmendmentTaf,
  fakePublishedTaf,
  fakeTafList,
  MOCK_USERNAME,
} from '../../utils/mockdata/fakeTafList';
import { TafApi } from '../../utils/api';

export default { title: 'components/Taf Module' };

const { width, height, top, left } = calculateDialogSizeAndPosition();
const DemoWrapped = (): React.ReactElement => {
  return (
    <ToolContainerDraggable
      startSize={{ width, height }}
      startPosition={{ top, left }}
      onClose={(): void => {}}
      title="TAF"
    >
      <TafModule />
    </ToolContainerDraggable>
  );
};

const Demo = (): React.ReactElement => (
  <Paper sx={{ maxHeight: '100%', height: '100vh', overflow: 'auto' }}>
    <TafModule />
  </Paper>
);

export const TafModuleLightThemeWithErrorOnAction = (): React.ReactElement => {
  const getTafList = (): Promise<{ data: TafFromBackend[] }> => {
    return new Promise((resolve) => {
      resolve({ data: fakeTafList });
    });
  };
  const postTaf = (): Promise<void> => {
    return new Promise<void>((resolve, reject) => {
      setTimeout(() => {
        reject(new Error('Status change not allowed'));
      }, 1000);
    });
  };
  const patchTaf = (): Promise<void> => {
    return new Promise<void>((resolve, reject) => {
      setTimeout(() => {
        // eslint-disable-next-line no-console
        console.log('Editor change failed');
        reject(new Error('Editor change failed'));
      }, 300);
    });
  };
  const getTAC = (): Promise<{ data: string }> => {
    return new Promise((resolve) => {
      resolve({
        data: fakeTafTac,
      });
    });
  };
  return (
    <TafThemeApiProvider
      createApiFunc={(): TafApi => {
        return {
          postTaf,
          getTafList,
          getTAC,
          patchTaf,
        };
      }}
    >
      <Demo />
    </TafThemeApiProvider>
  );
};

export const TafModuleLightThemeWithErrorOnLoadingList =
  (): React.ReactElement => {
    const getTafList = (): Promise<{ data: TafFromBackend[] }> => {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          reject(new Error('Request failed with status code 502'));
        }, 1000);
      });
    };

    return (
      <TafThemeApiProvider
        createApiFunc={(): TafApi => {
          return {
            ...createFakeApi(),
            getTafList,
          };
        }}
      >
        <Demo />
      </TafThemeApiProvider>
    );
  };

export const TafModuleWithNewData = (): React.ReactElement => {
  // returns tafs with empty data every second time it fetches to simulate new data
  let counter = 0;
  const getTafList = (): Promise<{ data: TafFromBackend[] }> => {
    return new Promise((resolve) => {
      counter += 1;
      const emptyList: TafFromBackend[] = fakeTafList.map((tafFromBackend) => ({
        ...tafFromBackend,
        taf: {
          ...tafFromBackend.taf,
          baseForecast: {
            valid: tafFromBackend.taf.baseForecast.valid,
          },
          changeGroups: [],
        },
      }));

      const data = counter % 2 === 0 ? emptyList : fakeTafList;

      resolve({ data });
    });
  };

  return (
    <TafThemeApiProvider
      createApiFunc={(): TafApi => {
        return {
          ...createFakeApi(),
          getTafList,
        };
      }}
    >
      <Demo />
    </TafThemeApiProvider>
  );
};

export const TafModuleLightTheme = (): React.ReactElement => {
  return (
    <TafThemeApiProvider createApiFunc={createFakeApi}>
      <Demo />
    </TafThemeApiProvider>
  );
};

TafModuleLightTheme.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61f2bf652767ebb4431c32f0/version/6322121028718da9d0e3ffd3',
    },
  ],
};

export const TafModuleDarkTheme = (): React.ReactElement => {
  return (
    <TafThemeApiProvider theme={darkTheme} createApiFunc={createFakeApi}>
      <Demo />
    </TafThemeApiProvider>
  );
};

TafModuleDarkTheme.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61f2bfc4b88a00b44ea967ac/version/6322124601a8d4a646d78c98',
    },
  ],
};

export const TafModuleToolContainerExample = (): React.ReactElement => {
  return (
    <TafThemeApiProvider createApiFunc={createFakeApi}>
      <DemoWrapped />
    </TafThemeApiProvider>
  );
};

export const TafModulePublishDraftAmendmentExample = (): React.ReactElement => {
  const tafPublished: TafFromBackend = {
    creationDate: `2022-01-06T12:00:00Z`,
    canbe: ['CANCELLED', 'AMENDED', 'CORRECTED'],
    editor: MOCK_USERNAME,
    taf: {
      ...fakePublishedTaf.taf,
      uuid: 'sowi3u4o-lsdjflsk',
      location: 'EHAM',
      baseTime: `2022-01-06T12:00:00Z`,
      issueDate: `2022-01-06T12:00:00Z`,
      validDateStart: `2022-01-06T12:00:00Z`,
      validDateEnd: `2022-01-07T18:00:00Z`,
      baseForecast: {
        ...fakePublishedTaf.taf.baseForecast,
        valid: {
          start: `2022-01-06T12:00:00Z`,
          end: `2022-01-07T18:00:00Z`,
        },
      },
    },
  };
  const tafDraftAmended: TafFromBackend = {
    creationDate: `2022-01-06T14:00:00Z`,
    canbe: ['DRAFTED', 'DISCARDED', 'PUBLISHED'],
    editor: MOCK_USERNAME,
    taf: {
      ...fakeDraftAmendmentTaf.taf,
      uuid: '23920102',
      previousId: tafPublished.taf.uuid,
      location: 'EHAM',
      baseTime: `2022-01-06T12:00:00Z`,
      validDateStart: `2022-01-06T14:00:00Z`,
      validDateEnd: `2022-01-07T18:00:00Z`,
      previousValidDateStart: `2022-01-06T12:00:00Z`,
      previousValidDateEnd: `2022-01-07T18:00:00Z`,
      baseForecast: {
        ...fakeDraftAmendmentTaf.taf.baseForecast,
        valid: {
          start: `2022-01-06T14:00:00Z`,
          end: `2022-01-07T18:00:00Z`,
        },
      },
      changeGroups: [],
    },
  };
  const tafAmended: TafFromBackend = {
    creationDate: `2022-01-06T14:00:00Z`,
    canbe: ['CANCELLED', 'AMENDED', 'CORRECTED'],
    editor: null!,
    taf: {
      ...tafDraftAmended.taf,
      status: 'AMENDED',
      issueDate: `2022-01-06T14:00:00Z`,
    },
  };

  let counter = 0;
  const getTafList = (): Promise<{ data: TafFromBackend[] }> => {
    return new Promise((resolve) => {
      counter += 1;

      const data =
        counter % 2 === 0
          ? [tafAmended, fakeCancelledFixedTaf]
          : [tafDraftAmended, tafPublished, fakeCancelledFixedTaf];

      resolve({ data });
    });
  };
  return (
    <TafThemeApiProvider
      createApiFunc={(): TafApi => {
        return {
          ...createFakeApi(),
          getTafList,
        };
      }}
    >
      <Demo />
    </TafThemeApiProvider>
  );
};
