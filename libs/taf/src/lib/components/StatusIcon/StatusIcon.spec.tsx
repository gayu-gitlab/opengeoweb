/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';

import { render, screen } from '@testing-library/react';
import StatusIcon from './StatusIcon';
import { TafThemeApiProvider } from '../Providers';

describe('components/StatusIcon/StatusIcon', () => {
  it('should return correct status icons', async () => {
    const { rerender } = render(<StatusIcon status="NEW" timeSlot="ACTIVE" />);

    // status new
    expect(
      render(<StatusIcon status="NEW" timeSlot="ACTIVE" />).container.classList,
    ).not.toContain('MuiSvgIcon-root');
    // status expired
    rerender(
      <TafThemeApiProvider>
        <StatusIcon status="EXPIRED" timeSlot="ACTIVE" />
      </TafThemeApiProvider>,
    );
    expect(screen.getByTestId('status-expired')).toBeTruthy();
    const tooltipExpired = await screen.findByTestId('statusIcon-tooltip');
    expect(tooltipExpired.getAttribute('aria-label')).toEqual('expired');

    // status cancelled
    rerender(
      <TafThemeApiProvider>
        <StatusIcon status="CANCELLED" timeSlot="ACTIVE" />
      </TafThemeApiProvider>,
    );
    expect(screen.getByTestId('status-cancelled')).toBeTruthy();
    const tooltipCancelled = await screen.findByTestId('statusIcon-tooltip');
    expect(tooltipCancelled.getAttribute('aria-label')).toEqual('cancelled');

    // status draft
    rerender(
      <TafThemeApiProvider>
        <StatusIcon status="DRAFT" timeSlot="ACTIVE" />
      </TafThemeApiProvider>,
    );
    expect(screen.getByTestId('status-draft')).toBeTruthy();
    const tooltipDraft = await screen.findByTestId('statusIcon-tooltip');
    expect(tooltipDraft.getAttribute('aria-label')).toEqual('draft');

    rerender(
      <TafThemeApiProvider>
        <StatusIcon status="DRAFT_CORRECTED" timeSlot="ACTIVE" />
      </TafThemeApiProvider>,
    );
    expect(screen.getByTestId('status-draft')).toBeTruthy();
    const tooltipDraftCorrected = await screen.findByTestId(
      'statusIcon-tooltip',
    );
    expect(tooltipDraftCorrected.getAttribute('aria-label')).toEqual(
      'draft correction',
    );

    rerender(
      <TafThemeApiProvider>
        <StatusIcon status="DRAFT_AMENDED" timeSlot="ACTIVE" />
      </TafThemeApiProvider>,
    );
    expect(screen.getByTestId('status-draft')).toBeTruthy();
    const tooltipDraftAmended = await screen.findByTestId('statusIcon-tooltip');
    expect(tooltipDraftAmended.getAttribute('aria-label')).toEqual(
      'draft amendment',
    );

    // upcoming
    rerender(
      <TafThemeApiProvider>
        <StatusIcon status="PUBLISHED" timeSlot="UPCOMING" />
      </TafThemeApiProvider>,
    );
    expect(screen.getByTestId('status-upcoming')).toBeTruthy();
    const tooltipUpcoming = await screen.findByTestId('statusIcon-tooltip');
    expect(tooltipUpcoming.getAttribute('aria-label')).toEqual('published');

    rerender(
      <TafThemeApiProvider>
        <StatusIcon status="CORRECTED" timeSlot="UPCOMING" />
      </TafThemeApiProvider>,
    );
    expect(screen.getByTestId('status-upcoming')).toBeTruthy();
    const tooltipUpcomingCorrected = await screen.findByTestId(
      'statusIcon-tooltip',
    );
    expect(tooltipUpcomingCorrected.getAttribute('aria-label')).toEqual(
      'published correction',
    );

    rerender(
      <TafThemeApiProvider>
        <StatusIcon status="AMENDED" timeSlot="UPCOMING" />
      </TafThemeApiProvider>,
    );
    expect(screen.getByTestId('status-upcoming')).toBeTruthy();
    const tooltipUpcomingAmended = await screen.findByTestId(
      'statusIcon-tooltip',
    );
    expect(tooltipUpcomingAmended.getAttribute('aria-label')).toEqual(
      'published amendment',
    );

    // active
    rerender(
      <TafThemeApiProvider>
        <StatusIcon status="PUBLISHED" timeSlot="ACTIVE" />
      </TafThemeApiProvider>,
    );
    expect(screen.getByTestId('status-active')).toBeTruthy();
    const tooltipActive = await screen.findByTestId('statusIcon-tooltip');
    expect(tooltipActive.getAttribute('aria-label')).toEqual('published');

    rerender(
      <TafThemeApiProvider>
        <StatusIcon status="CORRECTED" timeSlot="ACTIVE" />
      </TafThemeApiProvider>,
    );
    expect(screen.getByTestId('status-active')).toBeTruthy();
    const tooltipActiveCorrected = await screen.findByTestId(
      'statusIcon-tooltip',
    );
    expect(tooltipActiveCorrected.getAttribute('aria-label')).toEqual(
      'published correction',
    );

    rerender(
      <TafThemeApiProvider>
        <StatusIcon status="AMENDED" timeSlot="ACTIVE" />
      </TafThemeApiProvider>,
    );
    expect(screen.getByTestId('status-active')).toBeTruthy();
    const tooltipActiveAmended = await screen.findByTestId(
      'statusIcon-tooltip',
    );
    expect(tooltipActiveAmended.getAttribute('aria-label')).toEqual(
      'published amendment',
    );
  });
});
