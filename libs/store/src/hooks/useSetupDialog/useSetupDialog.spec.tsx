/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { renderHook } from '@testing-library/react';
import React from 'react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { SetupDialogReturnValue, useSetupDialog } from './useSetupDialog';
import { uiTypes, uiActions, ThemeStoreProvider } from '../../store';

describe('hooks/useSetupDialog/useSetupDialog', () => {
  const dialogType = uiTypes.DialogTypes.TimeSeriesManager;
  const dialogTypeLayerManager = uiTypes.DialogTypes.LayerManager;
  it('should register the dialog and return dialog order', async () => {
    const store = createMockStoreWithEggs({});

    const { result } = renderHook(() => useSetupDialog(dialogType), {
      wrapper: ({ children }) => (
        <ThemeStoreProvider store={store}>{children}</ThemeStoreProvider>
      ),
    });

    expect(store.getActions()).toEqual([
      uiActions.registerDialog({
        type: dialogType,
        source: 'app',
        setOpen: false,
      }),
    ]);
    store.clearActions();

    const expected: SetupDialogReturnValue = {
      dialogOrder: 0,
      isDialogOpen: false,
      onCloseDialog: expect.any(Function),
      setDialogOrder: expect.any(Function),
      uiSource: 'app',
      uiIsLoading: false,
      uiError: '',
      setFocused: expect.any(Function),
    };

    expect(result.current).toEqual(expected);

    result.current.setDialogOrder();
    expect(store.getActions()).toEqual([
      uiActions.orderDialog({
        type: dialogType,
      }),
    ]);
    store.clearActions();

    result.current.onCloseDialog();
    expect(store.getActions()).toEqual([
      uiActions.setToggleOpenDialog({
        type: dialogType,
        setOpen: false,
      }),
    ]);
  });
  it('should close the dialog when map is not present anymore', async () => {
    const mockState = {
      webmap: { byId: { otherMapId: {} }, allIds: ['otherMapId'] },
      ui: {
        order: [dialogTypeLayerManager],
        dialogs: {
          [dialogTypeLayerManager]: {
            activeMapId: 'mapId123',
            isOpen: true,
            type: dialogTypeLayerManager,
            source: 'app',
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    renderHook(() => useSetupDialog(dialogTypeLayerManager), {
      wrapper: ({ children }) => (
        <ThemeStoreProvider store={store}>{children}</ThemeStoreProvider>
      ),
    });

    expect(store.getActions()[0]).toEqual(
      uiActions.setToggleOpenDialog({
        setOpen: false,
        type: dialogTypeLayerManager,
      }),
    );
  });

  it('should not close the timeseries dialog when map is not present anymore', async () => {
    const mockState = {
      webmap: { byId: { otherMapId: {} }, allIds: ['otherMapId'] },
      ui: {
        order: [dialogType],
        dialogs: {
          timeSeriesManager: {
            activeMapId: 'mapId123',
            isOpen: true,
            type: dialogType,
            source: 'app',
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    renderHook(() => useSetupDialog(dialogType), {
      wrapper: ({ children }) => (
        <ThemeStoreProvider store={store}>{children}</ThemeStoreProvider>
      ),
    });

    expect(store.getActions()[0]).toEqual(
      uiActions.registerDialog({
        setOpen: false,
        type: dialogType,
        source: 'app',
      }),
    );
  });
});
