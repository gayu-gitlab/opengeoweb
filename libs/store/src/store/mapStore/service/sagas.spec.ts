/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { takeEvery, call, all, put } from 'redux-saga/effects';

import { getCapabilities } from '@opengeoweb/webmap';
import { rootSaga, fetchInitialServicesSaga, fetchServiceSaga } from './sagas';
import { serviceActions } from './reducer';
import { SetLayersForServicePayload } from './types';

describe('store/mapStore/services/sagas', () => {
  describe('rootSaga', () => {
    it('should catch rootSaga actions and fire corresponding sagas', () => {
      const generator = rootSaga();
      expect(generator.next().value).toEqual(
        takeEvery(
          serviceActions.fetchInitialServices,
          fetchInitialServicesSaga,
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('fetchInitialServicesSaga', () => {
    it('should fetch all services', () => {
      const testServices = [
        {
          id: 'serviceid_12',
          name: 'msgrt',
          serviceUrl:
            'https://adaguc-server-msg-cpp-portal.pmc.knmi.cloud/wms?DATASET=msgrt&',
          scope: 'user' as const,
          abstract:
            'This service demonstrates how the ADAGUC server can be used to create OGC services.',
        },
        {
          id: 'serviceid_11',
          name: 'DWD GeoServer WMSs',
          serviceUrl: 'https://maps.dwd.de/geoserver/dwd/WX-Produkt/ows?2',
          scope: 'user' as const,
          abstract: 'This is the Web Map Server of DWD.',
        },
      ];
      const generator = fetchInitialServicesSaga(
        serviceActions.fetchInitialServices({ services: testServices }),
      );
      expect(generator.next().value).toEqual(
        all(
          testServices.map((service) =>
            call(fetchServiceSaga, service as SetLayersForServicePayload),
          ),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('fetchServiceSaga', () => {
    it('should get layers from service and set serviceLayers', () => {
      const testService = {
        id: 'serviceid_12',
        name: 'msgrt',
        serviceUrl:
          'https://adaguc-server-msg-cpp-portal.pmc.knmi.cloud/wms?DATASET=msgrt&',
        scope: 'user' as const,
        abstract:
          'This service demonstrates how the ADAGUC server can be used to create OGC services.',
        layers: [
          {
            name: 'test-layer-1',
            title: 'test layer 1',
            leaf: true,
            path: ['path1', 'path2'],
            keywords: ['keyword1', 'keyword2'],
          },
        ],
      };
      const generator = fetchServiceSaga(testService);
      expect(generator.next().value).toEqual(
        call(
          getCapabilities.getLayersFlattenedFromService,
          testService.serviceUrl,
        ),
      );
      const fakeResponse = [
        {
          name: 'RAD_NL25_PCP_CM',
          title: 'Precipitation Radar NL',
          leaf: true,
          path: ['WMS of  RADAR'],
          keywords: [],
          styles: [
            {
              title: 'radar/nearest',
              name: 'radar/nearest',
              legendURL:
                'https://geoservices.knmi.nl/adaguc-server?DATASET=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=radar/nearest',
              abstract: 'No abstract available',
            },
            {
              title: 'precip-rainbow/nearest',
              name: 'precip-rainbow/nearest',
              legendURL:
                'https://geoservices.knmi.nl/adaguc-server?DATASET=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=RAD_NL25_PCP_CM&format=image/png&STYLE=precip-rainbow/nearest',
              abstract: 'No abstract available',
            },
          ],
          dimensions: [
            {
              name: 'time',
              units: 'ISO8601',
              currentValue: '2023-04-05T12:00:00Z',
              values: '2021-03-31T09:25:00Z/2023-04-05T12:00:00Z/PT5M',
            },
          ],
          geographicBoundingBox: {
            east: '10.856452',
            west: '0.000000',
            north: '55.973600',
            south: '48.895303',
          },
          queryable: true,
          crs: [
            {
              name: 'EPSG:3411',
              bbox: {
                left: 2682754.74362,
                right: 3759536.917562,
                bottom: -3245034.014141,
                top: -2168251.989038,
              },
            },
            {
              name: 'EPSG:3412',
              bbox: {
                left: 0,
                right: 7413041.166015,
                bottom: 32318824.826266,
                top: 40075258.815074,
              },
            },
            {
              name: 'EPSG:3575',
              bbox: {
                left: -770622.801471,
                right: 56845.766135,
                bottom: -4485814.811314,
                top: -3684039.44362,
              },
            },
          ],
        },
        {
          name: 'Reflectivity',
          title: 'Reflectivity',
          leaf: true,
          path: ['WMS of  RADAR'],
          keywords: [],
          styles: [
            {
              title: 'radarReflectivity/nearest',
              name: 'radarReflectivity/nearest',
              legendURL:
                'https://geoservices.knmi.nl/adaguc-server?DATASET=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=Reflectivity&format=image/png&STYLE=radarReflectivity/nearest',
              abstract: 'No abstract available',
            },
            {
              title: 'radarReflectivity_transparant/nearest',
              name: 'radarReflectivity_transparant/nearest',
              legendURL:
                'https://geoservices.knmi.nl/adaguc-server?DATASET=RADAR&SERVICE=WMS&&version=1.1.1&service=WMS&request=GetLegendGraphic&layer=Reflectivity&format=image/png&STYLE=radarReflectivity_transparant/nearest',
              abstract: 'No abstract available',
            },
          ],
          dimensions: [
            {
              name: 'time',
              units: 'ISO8601',
              currentValue: '2023-04-05T12:00:00Z',
              values: '2021-10-25T11:45:00Z/2023-04-05T12:00:00Z/PT5M',
            },
          ],
          geographicBoundingBox: {
            east: '10.856452',
            west: '0.000000',
            north: '55.973600',
            south: '48.895303',
          },
          queryable: true,
          crs: [
            {
              name: 'EPSG:3411',
              bbox: {
                left: 2682754.74362,
                right: 3759536.917562,
                bottom: -3245034.014141,
                top: -2168251.989038,
              },
            },
            {
              name: 'EPSG:3412',
              bbox: {
                left: 0,
                right: 7413041.166015,
                bottom: 32318824.826266,
                top: 40075258.815074,
              },
            },
          ],
        },
      ];

      expect(generator.next(fakeResponse).value).toEqual(
        put(
          serviceActions.serviceSetLayers({
            ...testService,
            layers: fakeResponse,
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });
});
