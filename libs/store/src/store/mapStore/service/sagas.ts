/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { put, call, takeEvery, all } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import { getCapabilities } from '@opengeoweb/webmap';
import { serviceActions } from './reducer';
import { SetLayersForServicePayload } from './types';

export function* fetchServiceSaga(
  service: SetLayersForServicePayload,
): SagaIterator {
  try {
    const layers = yield call(
      getCapabilities.getLayersFlattenedFromService,
      service.serviceUrl,
    );
    yield put(
      serviceActions.serviceSetLayers({
        ...service,
        layers,
      }),
    );
  } catch (error) {
    // not implemented yet
  }
}

export function* fetchInitialServicesSaga({
  payload,
}: ReturnType<typeof serviceActions.fetchInitialServices>): SagaIterator {
  const { services } = payload;

  yield all(
    services.map((service) =>
      call(fetchServiceSaga, service as SetLayersForServicePayload),
    ),
  );
}

export function* rootSaga(): SagaIterator {
  yield takeEvery(
    serviceActions.fetchInitialServices,
    fetchInitialServicesSaga,
  );
}

export default rootSaga;
