/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { LayerType } from '@opengeoweb/webmap';
import { filterLayers, parseBoolean, parseLayer } from './filterLayers';
import { layerTypes } from '../layers';

describe('filterLayers', () => {
  describe('parseBoolean', () => {
    it('should return correct value as boolean', () => {
      expect(parseBoolean(true)).toBeTruthy();
      expect(parseBoolean('true')).toBeTruthy();
      expect(parseBoolean(false)).toBeFalsy();
      expect(parseBoolean('false')).toBeFalsy();
    });
  });

  describe('parseLayer', () => {
    it('should parse layer', () => {
      const testLayer1 = { layerType: LayerType.baseLayer };
      expect(parseLayer(testLayer1)).toEqual({
        ...testLayer1,
        id: expect.stringContaining('layer'),
      });

      const testLayer2 = {
        id: 'test',
        layerType: LayerType.baseLayer,
        name: 'testing',
      };
      expect(parseLayer(testLayer2)).toEqual(testLayer2);

      const testLayer3 = {
        id: 'test',
        layerType: LayerType.baseLayer,
        enabled: 'true',
      } as unknown as layerTypes.Layer;

      expect(parseLayer(testLayer3)).toEqual({
        ...testLayer3,
        enabled: true,
      });
    });
  });

  describe('filterLayers', () => {
    it('should filter baselayers, mapLayers and overLayers', () => {
      expect(filterLayers()).toEqual({
        mapLayers: [],
        baseLayers: [],
        overLayers: [],
      });
      expect(filterLayers([])).toEqual({
        mapLayers: [],
        baseLayers: [],
        overLayers: [],
      });
      expect(
        filterLayers([
          { id: 'test-baselayer-1', layerType: LayerType.baseLayer },
        ]),
      ).toEqual({
        mapLayers: [],
        baseLayers: [
          { id: 'test-baselayer-1', layerType: LayerType.baseLayer },
        ],
        overLayers: [],
      });
      expect(
        filterLayers([
          { id: 'test-maplayer-1', layerType: LayerType.mapLayer },
        ]),
      ).toEqual({
        mapLayers: [{ id: 'test-maplayer-1', layerType: LayerType.mapLayer }],
        baseLayers: [],
        overLayers: [],
      });
      expect(
        filterLayers([
          { id: 'test-overLayer-1', layerType: LayerType.overLayer },
        ]),
      ).toEqual({
        mapLayers: [],
        baseLayers: [],
        overLayers: [
          { id: 'test-overLayer-1', layerType: LayerType.overLayer },
        ],
      });

      expect(
        filterLayers([
          { id: 'test-maplayer-1', layerType: LayerType.mapLayer },
          { id: 'test-baseLayer-1', layerType: LayerType.baseLayer },
          { id: 'test-overlayer-1', layerType: LayerType.overLayer },
          { id: 'test-maplayer-2', layerType: LayerType.mapLayer },
          { id: 'test-baseLayer-2', layerType: LayerType.baseLayer },
          { id: 'test-overlayer-2', layerType: LayerType.overLayer },
          { id: 'test-maplayer-3', layerType: LayerType.mapLayer },
          { id: 'test-baseLayer-3', layerType: LayerType.baseLayer },
        ]),
      ).toEqual({
        mapLayers: [
          { id: 'test-maplayer-1', layerType: LayerType.mapLayer },
          { id: 'test-maplayer-2', layerType: LayerType.mapLayer },
          { id: 'test-maplayer-3', layerType: LayerType.mapLayer },
        ],
        baseLayers: [
          { id: 'test-baseLayer-1', layerType: LayerType.baseLayer },
          { id: 'test-baseLayer-2', layerType: LayerType.baseLayer },
          { id: 'test-baseLayer-3', layerType: LayerType.baseLayer },
        ],
        overLayers: [
          { id: 'test-overlayer-1', layerType: LayerType.overLayer },
          { id: 'test-overlayer-2', layerType: LayerType.overLayer },
        ],
      });
    });

    it('should be able to add a parser', () => {
      const layersWithoutId = [
        {
          name: 'test-baselayer',
          layerType: LayerType.baseLayer,
        },
        {
          name: 'test-overlayer',
          layerType: LayerType.overLayer,
        },
        {
          name: 'test-mapLayer',
          layerType: LayerType.mapLayer,
        },
      ];

      expect(filterLayers(layersWithoutId, parseLayer)).toEqual({
        baseLayers: [
          {
            ...layersWithoutId[0],
            id: expect.stringContaining('layer'),
          },
        ],
        overLayers: [
          {
            ...layersWithoutId[1],
            id: expect.stringContaining('layer'),
          },
        ],
        mapLayers: [
          {
            ...layersWithoutId[2],
            id: expect.stringContaining('layer'),
          },
        ],
      });
    });
  });
});
