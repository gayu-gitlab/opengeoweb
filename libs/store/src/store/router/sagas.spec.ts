/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { call, takeLatest } from 'redux-saga/effects';
import { rootSaga, navigateToUrlSaga } from './sagas';
import { routerActions } from './reducer';

import * as routeUtils from './utils';

jest.mock('./utils', () => ({
  historyDict: {
    navigate: jest.fn(),
  },
}));

describe('store/router/sagas', () => {
  describe('rootSaga', () => {
    it('should catch rootSaga actions and fire corresponding sagas', () => {
      const generator = rootSaga();
      expect(generator.next().value).toEqual(
        takeLatest(routerActions.navigateToUrl.type, navigateToUrlSaga),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });
});

describe('navigateToUrlSaga', () => {
  it('should fire action to navigate to given url', () => {
    const testUrl = '/test/124';

    const generator = navigateToUrlSaga({
      payload: {
        url: testUrl,
      },
      type: routerActions.navigateToUrl.type,
    });

    // routeUtils.historyDict.
    expect(generator.next().value).toEqual(
      call(routeUtils.historyDict.navigate, testUrl),
    );
    expect(generator.next().done).toBeTruthy();
  });
});
