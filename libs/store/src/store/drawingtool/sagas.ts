/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * */

import { put, select, takeLatest } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import { LayerType } from '@opengeoweb/webmap';
import {
  defaultIntersectionStyleProperties,
  emptyGeoJSON,
  getFeatureCollection,
  getGeoJson,
  getLastEmptyFeatureIndex,
  moveFeature,
} from '@opengeoweb/webmap-react';
import { drawtoolActions } from './reducer';
import * as drawtoolSelectors from './selectors';
import { layerActions, layerSelectors, mapStoreActions } from '../mapStore';

const registerOrigin = 'drawings saga:registerDrawToolSaga';

export function* registerDrawToolSaga({
  payload,
}: ReturnType<typeof drawtoolActions.registerDrawTool>): SagaIterator {
  const {
    mapId,
    geoJSONLayerId,
    geoJSONIntersectionLayerId,
    geoJSONIntersectionBoundsLayerId,
    defaultGeoJSON = emptyGeoJSON,
    defaultGeoJSONIntersection = emptyGeoJSON,
    defaultGeoJSONIntersectionBounds,
    defaultGeoJSONIntersectionProperties = defaultIntersectionStyleProperties,
  } = payload;

  // create for every drawTool a draw layer
  if (geoJSONLayerId && mapId) {
    yield put(
      mapStoreActions.addLayer({
        mapId,
        layer: {
          // empty geoJSON
          geojson: defaultGeoJSON,
          layerType: LayerType.featureLayer,
        },
        layerId: geoJSONLayerId,
        origin: registerOrigin,
      }),
    );
  }
  // create intersection layer
  if (geoJSONIntersectionLayerId && mapId) {
    yield put(
      mapStoreActions.addLayer({
        mapId,
        layer: {
          geojson: defaultGeoJSONIntersection,
          layerType: LayerType.featureLayer,
          defaultGeoJSONProperties: defaultGeoJSONIntersectionProperties,
        },
        layerId: geoJSONIntersectionLayerId,
        origin: registerOrigin,
      }),
    );
  }
  // create intersection bounds layer
  if (geoJSONIntersectionBoundsLayerId && mapId) {
    const existingBoundsLayer = yield select(
      layerSelectors.getLayerById,
      geoJSONIntersectionBoundsLayerId,
    );

    // don't add a new boundslayer when another drawtool using that same boundslayer
    if (!existingBoundsLayer) {
      yield put(
        mapStoreActions.addLayer({
          mapId,
          layer: {
            geojson: defaultGeoJSONIntersectionBounds,
            layerType: LayerType.featureLayer,
          },
          layerId: geoJSONIntersectionBoundsLayerId,
          origin: registerOrigin,
        }),
      );
    }

    yield put(
      layerActions.orderLayerToFront({
        layerId: geoJSONIntersectionBoundsLayerId,
      }),
    );
  }
}

export function* changeDrawToolSaga({
  payload,
}: ReturnType<typeof drawtoolActions.changeDrawToolMode>): SagaIterator {
  try {
    const { drawModeId, drawToolId, shouldUpdateShape = true } = payload;
    const drawingTool = yield select(
      drawtoolSelectors.selectDrawToolById,
      drawToolId,
    );

    const {
      shouldAllowMultipleShapes = false,
      geoJSONIntersectionLayerId,
      geoJSONIntersectionBoundsLayerId,
    } = drawingTool;

    const newDrawMode = yield select(
      drawtoolSelectors.getDrawModeById,
      drawToolId,
      drawModeId,
    );

    // disable layer when no new drawmode or when selecting same tool and is selectable
    if (
      !newDrawMode ||
      (!drawingTool.activeDrawModeId && newDrawMode.isSelectable)
    ) {
      yield put(
        layerActions.toggleFeatureMode({
          layerId: drawingTool.geoJSONLayerId,
          isInEditMode: false,
          drawMode: '',
        }),
      );

      const geoJSONLayer = yield select(
        layerSelectors.getLayerById,
        drawingTool.geoJSONLayerId,
      );
      const geoJSON = geoJSONLayer?.geojson;
      const lastFeatureIndex =
        geoJSON !== undefined && getLastEmptyFeatureIndex(geoJSON);

      if (lastFeatureIndex !== undefined) {
        const newGeoJSON = {
          ...geoJSON,
          features: (geoJSON as GeoJSON.FeatureCollection).features.filter(
            (_feature, index) => index !== lastFeatureIndex,
          ),
        };
        yield put(
          layerActions.updateFeature({
            layerId: drawingTool.geoJSONLayerId,
            geojson: newGeoJSON,
          }),
        );

        yield put(
          layerActions.setSelectedFeature({
            layerId: drawingTool.geoJSONLayerId,
            selectedFeatureIndex:
              newGeoJSON.features.length > 0
                ? newGeoJSON.features.length - 1
                : 0,
          }),
        );
      }

      return;
    }

    // delete shape
    if (newDrawMode.value === 'DELETE') {
      yield put(
        layerActions.layerChangeGeojson({
          layerId: drawingTool.geoJSONLayerId,
          geojson: emptyGeoJSON,
        }),
      );

      // clear intersection shape
      if (geoJSONIntersectionLayerId) {
        yield put(
          layerActions.layerChangeGeojson({
            layerId: geoJSONIntersectionLayerId,
            geojson: emptyGeoJSON,
          }),
        );
      }

      yield put(
        layerActions.toggleFeatureMode({
          layerId: drawingTool.geoJSONLayerId,
          isInEditMode: false,
          drawMode: '',
        }),
      );
      return;
    }

    // check tool is selected of existing drawn shape
    const currentGeoJSONLayer = yield select(
      layerSelectors.getLayerById,
      drawingTool.geoJSONLayerId,
    );
    const { geojson: currentGeoJSON, selectedFeatureIndex = 0 } =
      currentGeoJSONLayer || {};

    const currentSelectionType =
      currentGeoJSON?.features[selectedFeatureIndex]?.properties?.selectionType;
    const newSelectionType = newDrawMode.shape.properties?.selectionType;
    const isNewToolSelected = currentSelectionType !== newSelectionType;

    const shouldUpdateNewShape =
      !currentGeoJSON?.features?.length || isNewToolSelected;

    // don't change anything if same tool is selected again
    if (shouldUpdateNewShape && shouldUpdateShape) {
      const geoJSONFeatureCollection = getFeatureCollection(
        newDrawMode.shape,
        shouldAllowMultipleShapes,
        currentGeoJSON,
      );
      const newGeoJSON = getGeoJson(
        geoJSONFeatureCollection,
        shouldAllowMultipleShapes,
      );

      if (shouldAllowMultipleShapes) {
        moveFeature(currentGeoJSON, newGeoJSON, selectedFeatureIndex, '');
      }

      yield put(
        layerActions.setSelectedFeature({
          layerId: drawingTool.geoJSONLayerId,
          selectedFeatureIndex: newGeoJSON.features.length - 1,
        }),
      );

      yield put(
        layerActions.updateFeature({
          layerId: drawingTool.geoJSONLayerId,
          geojson: newGeoJSON,
          ...(geoJSONIntersectionLayerId && {
            geoJSONIntersectionLayerId,
          }),
          ...(geoJSONIntersectionBoundsLayerId && {
            geoJSONIntersectionBoundsLayerId,
          }),
        }),
      );
    }
    yield put(
      layerActions.toggleFeatureMode({
        layerId: drawingTool.geoJSONLayerId,
        isInEditMode: newDrawMode.isSelectable,
        drawMode: newDrawMode.value,
      }),
    );
  } catch (error) {
    // eslint-disable-next-line no-console
    console.log('error changeDrawToolSaga', error);
  }
}

export function* changeIntersectionSaga({
  payload,
}: ReturnType<typeof drawtoolActions.changeIntersectionBounds>): SagaIterator {
  const { drawToolId, geoJSON } = payload;

  const drawtool = yield select(
    drawtoolSelectors.selectDrawToolById,
    drawToolId,
  );
  yield put(
    layerActions.layerChangeGeojson({
      layerId: drawtool.geoJSONIntersectionBoundsLayerId,
      geojson: geoJSON,
    }),
  );
  yield put(
    layerActions.layerChangeGeojson({
      layerId: drawtool.geoJSONLayerId,
      geojson: emptyGeoJSON,
    }),
  );
  yield put(
    layerActions.layerChangeGeojson({
      layerId: drawtool.geoJSONIntersectionLayerId,
      geojson: emptyGeoJSON,
    }),
  );
}

function* drawingSaga(): SagaIterator {
  yield takeLatest(drawtoolActions.registerDrawTool, registerDrawToolSaga);
  yield takeLatest(drawtoolActions.changeDrawToolMode, changeDrawToolSaga);
  yield takeLatest(
    drawtoolActions.changeIntersectionBounds,
    changeIntersectionSaga,
  );
}

export default drawingSaga;
