/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2024 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2024 - Finnish Meteorological Institute (FMI)
 * */

import { put, select, takeLatest } from 'redux-saga/effects';
import { LayerType } from '@opengeoweb/webmap';
import {
  DRAWMODE,
  DrawMode,
  defaultIntersectionStyleProperties,
  emptyGeoJSON,
  getFeatureCollection,
  getGeoJson,
} from '@opengeoweb/webmap-react';
import drawingSaga, {
  changeDrawToolSaga,
  changeIntersectionSaga,
  registerDrawToolSaga,
} from './sagas';
import { drawtoolActions } from './reducer';
import { layerActions, layerSelectors, mapStoreActions } from '../mapStore';
import * as drawtoolSelectors from './selectors';
import { testGeoJSON } from './testUtils';

describe('store/drawingtool/sagas', () => {
  describe('registerDrawToolSaga', () => {
    // Create a mock for payload
    const geoJSONLayerId = 'draw-laye';
    const payload = {
      drawToolId: 'mockDrawToolId',
      mapId: 'mockMapId',
      defaultDrawModes: [],
      geoJSONLayerId,
    };
    it('should add a drawlayer when geoJSONLayerID and mapId are given', () => {
      const generator = registerDrawToolSaga(
        drawtoolActions.registerDrawTool(payload),
      );

      const drawLayerId = 'mockDrawLayerId';

      // Expect the put action to be dispatched with the correct payload
      expect(generator.next(drawLayerId).value).toEqual(
        put(
          mapStoreActions.addLayer({
            mapId: payload.mapId,
            layer: {
              geojson: {
                type: 'FeatureCollection',
                features: [],
              },
              layerType: LayerType.featureLayer,
            },
            layerId: geoJSONLayerId,
            origin: 'drawings saga:registerDrawToolSaga',
          }),
        ),
      );

      // Expect the generator to be done
      expect(generator.next().done).toBe(true);
    });

    it('should not add a drawlayer when geoJSONLayerID and mapId are missing', () => {
      const generator = registerDrawToolSaga(
        drawtoolActions.registerDrawTool({
          drawToolId: 'mockDrawToolId',
          defaultDrawModes: [],
        }),
      );

      // Expect the generator to be done
      expect(generator.next().done).toBe(true);
    });

    it('should add a drawlayer when geoJSONLayerID and mapId are given', () => {
      const payloadWithGeoJSON = {
        drawToolId: 'mockDrawToolId',
        mapId: 'mockMapId',
        defaultDrawModes: [],
        geoJSONLayerId,
        defaultGeoJSON: testGeoJSON,
      };
      const generator = registerDrawToolSaga(
        drawtoolActions.registerDrawTool(payloadWithGeoJSON),
      );

      const drawLayerId = 'mockDrawLayerId';

      // Expect the put action to be dispatched with the correct payload
      expect(generator.next(drawLayerId).value).toEqual(
        put(
          mapStoreActions.addLayer({
            mapId: payload.mapId,
            layer: {
              geojson: testGeoJSON,
              layerType: LayerType.featureLayer,
            },
            layerId: geoJSONLayerId,
            origin: 'drawings saga:registerDrawToolSaga',
          }),
        ),
      );

      // Expect the generator to be done
      expect(generator.next().done).toBe(true);
    });

    it('should be able to add intersection layers with defaultGeoJSONIntersectionProperties', () => {
      const payload = {
        mapId: 'map-1',
        drawToolId: 'mockDrawToolId',
        defaultDrawModes: [],
        geoJSONIntersectionLayerId: 'test-intersection-layer',
        geoJSONIntersectionBoundsLayerId: 'test-intersectionbounds-layer',
        defaultGeoJSONIntersectionBounds: testGeoJSON,
        defaultGeoJSONIntersection: testGeoJSON,
        defaultGeoJSONIntersectionProperties: {
          fill: '#FF00FF',
          selectionType: 'box',
        },
      };
      const generator = registerDrawToolSaga(
        drawtoolActions.registerDrawTool(payload),
      );

      expect(generator.next(payload).value).toEqual(
        put(
          mapStoreActions.addLayer({
            mapId: payload.mapId,
            layer: {
              geojson: payload.defaultGeoJSONIntersection,
              layerType: LayerType.featureLayer,
              defaultGeoJSONProperties:
                payload.defaultGeoJSONIntersectionProperties,
            },
            layerId: payload.geoJSONIntersectionLayerId,
            origin: 'drawings saga:registerDrawToolSaga',
          }),
        ),
      );

      expect(generator.next(payload).value).toEqual(
        select(
          layerSelectors.getLayerById,
          payload.geoJSONIntersectionBoundsLayerId,
        ),
      );

      expect(generator.next().value).toEqual(
        put(
          mapStoreActions.addLayer({
            mapId: payload.mapId,
            layer: {
              geojson: payload.defaultGeoJSONIntersectionBounds,
              layerType: LayerType.featureLayer,
            },
            layerId: payload.geoJSONIntersectionBoundsLayerId,
            origin: 'drawings saga:registerDrawToolSaga',
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        put(
          layerActions.orderLayerToFront({
            layerId: payload.geoJSONIntersectionBoundsLayerId,
          }),
        ),
      );
      // Expect the generator to be done
      expect(generator.next().done).toBe(true);
    });

    it('should not add an intersection layer when it is already existing in another drawtool', () => {
      const payload = {
        mapId: 'map-1',
        drawToolId: 'mockDrawToolId',
        defaultDrawModes: [],
        geoJSONIntersectionLayerId: 'test-intersection-layer',
        geoJSONIntersectionBoundsLayerId: 'test-intersectionbounds-layer',
        defaultGeoJSONIntersectionBounds: testGeoJSON,
        defaultGeoJSONIntersection: testGeoJSON,
      };
      const generator = registerDrawToolSaga(
        drawtoolActions.registerDrawTool(payload),
      );

      expect(generator.next(payload).value).toEqual(
        put(
          mapStoreActions.addLayer({
            mapId: payload.mapId,
            layer: {
              geojson: payload.defaultGeoJSONIntersection,
              layerType: LayerType.featureLayer,
              defaultGeoJSONProperties: defaultIntersectionStyleProperties,
            },
            layerId: payload.geoJSONIntersectionLayerId,
            origin: 'drawings saga:registerDrawToolSaga',
          }),
        ),
      );

      expect(generator.next(payload).value).toEqual(
        select(
          layerSelectors.getLayerById,
          payload.geoJSONIntersectionBoundsLayerId,
        ),
      );
      const boundsLayer = {
        id: payload.geoJSONIntersectionBoundsLayerId,
      };

      expect(generator.next(boundsLayer).value).toEqual(
        put(layerActions.orderLayerToFront({ layerId: boundsLayer.id })),
      );

      // Expect the generator to be done
      expect(generator.next().done).toBe(true);
    });
  });

  describe('changeDrawToolSaga', () => {
    it('should handle DELETE draw mode', () => {
      const payload = {
        drawModeId: 'someId',
        drawToolId: 'toolId',
      };

      const generator = changeDrawToolSaga(
        drawtoolActions.changeDrawToolMode(payload),
      );

      // Mocking selectors and actions
      expect(generator.next().value).toEqual(
        select(drawtoolSelectors.selectDrawToolById, 'toolId'),
      );
      const drawingTool = {
        geoJSONLayerId: 'layerId',
      };
      expect(generator.next(drawingTool).value).toEqual(
        select(drawtoolSelectors.getDrawModeById, 'toolId', 'someId'),
      );
      const newDrawMode = {
        value: 'DELETE',
        shape: {},
      };
      expect(generator.next(newDrawMode).value).toEqual(
        put(
          layerActions.layerChangeGeojson({
            layerId: 'layerId',
            geojson: {
              type: 'FeatureCollection',
              features: [],
            },
          }),
        ),
      );

      expect(generator.next(newDrawMode).value).toEqual(
        put(
          layerActions.toggleFeatureMode({
            layerId: 'layerId',
            isInEditMode: false,
            drawMode: '',
          }),
        ),
      );

      // Make sure the saga is finished
      expect(generator.next().done).toBe(true);
    });

    it('should clear clear intersection layer when handling DELETE', () => {
      const payload = {
        drawModeId: 'someId',
        drawToolId: 'toolId',
      };

      const generator = changeDrawToolSaga(
        drawtoolActions.changeDrawToolMode(payload),
      );

      // Mocking selectors and actions
      expect(generator.next().value).toEqual(
        select(drawtoolSelectors.selectDrawToolById, 'toolId'),
      );
      const drawingTool = {
        geoJSONLayerId: 'layerId',
        geoJSONIntersectionLayerId: 'intersection-layer',
      };
      expect(generator.next(drawingTool).value).toEqual(
        select(drawtoolSelectors.getDrawModeById, 'toolId', 'someId'),
      );
      const newDrawMode = {
        value: 'DELETE',
        shape: {},
      };
      expect(generator.next(newDrawMode).value).toEqual(
        put(
          layerActions.layerChangeGeojson({
            layerId: 'layerId',
            geojson: emptyGeoJSON,
          }),
        ),
      );

      expect(generator.next(newDrawMode).value).toEqual(
        put(
          layerActions.layerChangeGeojson({
            layerId: drawingTool.geoJSONIntersectionLayerId,
            geojson: emptyGeoJSON,
          }),
        ),
      );

      expect(generator.next(newDrawMode).value).toEqual(
        put(
          layerActions.toggleFeatureMode({
            layerId: 'layerId',
            isInEditMode: false,
            drawMode: '',
          }),
        ),
      );

      // Make sure the saga is finished
      expect(generator.next().done).toBe(true);
    });

    it('should handle disabling the tool', () => {
      const payload = {
        drawModeId: 'someId',
        drawToolId: 'toolId',
      };

      const generator = changeDrawToolSaga(
        drawtoolActions.changeDrawToolMode(payload),
      );

      // Mocking selectors and actions
      expect(generator.next().value).toEqual(
        select(drawtoolSelectors.selectDrawToolById, 'toolId'),
      );
      const drawingTool = {
        geoJSONLayerId: 'layerId',
        activeDrawModeId: '',
        isSelectable: true,
      };

      expect(generator.next(drawingTool).value).toEqual(
        select(
          drawtoolSelectors.getDrawModeById,
          payload.drawToolId,
          payload.drawModeId,
        ),
      );
      expect(generator.next(drawingTool).value).toEqual(
        put(
          layerActions.toggleFeatureMode({
            layerId: 'layerId',
            isInEditMode: false,
            drawMode: '',
          }),
        ),
      );

      expect(generator.next(drawingTool).value).toEqual(
        select(layerSelectors.getLayerById, drawingTool.geoJSONLayerId),
      );

      expect(generator.next().done).toBe(true);
    });

    it('should disabling the tool and remove last empty shape', () => {
      const payload = {
        drawModeId: 'someId',
        drawToolId: 'toolId',
      };

      const generator = changeDrawToolSaga(
        drawtoolActions.changeDrawToolMode(payload),
      );

      // Mocking selectors and actions
      expect(generator.next().value).toEqual(
        select(drawtoolSelectors.selectDrawToolById, 'toolId'),
      );
      const drawingTool = {
        geoJSONLayerId: 'layerId',
        activeDrawModeId: '',
        isSelectable: true,
      };

      expect(generator.next(drawingTool).value).toEqual(
        select(
          drawtoolSelectors.getDrawModeById,
          payload.drawToolId,
          payload.drawModeId,
        ),
      );
      expect(generator.next(drawingTool).value).toEqual(
        put(
          layerActions.toggleFeatureMode({
            layerId: 'layerId',
            isInEditMode: false,
            drawMode: '',
          }),
        ),
      );

      expect(generator.next(drawingTool).value).toEqual(
        select(layerSelectors.getLayerById, drawingTool.geoJSONLayerId),
      );

      const testGeoJSON: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            properties: {
              fill: '#33ccFF',
              'fill-opacity': 0.5,
              stroke: '#8F8',
              'stroke-width': 4,
              'stroke-opacity': 1,
              selectionType: 'box',
              _type: 'box',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [4.462610728972621, 55.964457972835795],
                  [4.462610728972621, 52.70695422198958],
                  [9.590504693726611, 52.70695422198958],
                  [9.590504693726611, 55.964457972835795],
                  [4.462610728972621, 55.964457972835795],
                ],
              ],
            },
          },
          {
            type: 'Feature',
            properties: {
              fill: '#33ccFF',
              'fill-opacity': 0.5,
              stroke: '#8F8',
              'stroke-width': 4,
              'stroke-opacity': 1,
              selectionType: 'poly',
            },
            geometry: {
              type: 'Polygon',
              coordinates: [[]],
            },
          },
        ],
      };

      expect(generator.next({ geojson: testGeoJSON }).value).toEqual(
        put(
          layerActions.updateFeature({
            layerId: 'layerId',
            geojson: {
              ...testGeoJSON,
              features: [testGeoJSON.features[0]],
            },
          }),
        ),
      );

      expect(generator.next({ geojson: testGeoJSON }).value).toEqual(
        put(
          layerActions.setSelectedFeature({
            layerId: 'layerId',
            selectedFeatureIndex: 0,
          }),
        ),
      );
    });

    it('should disable tool when selecting empty drawmode', () => {
      const payload = {
        drawModeId: '',
        drawToolId: 'toolId',
      };

      const generator = changeDrawToolSaga(
        drawtoolActions.changeDrawToolMode(payload),
      );

      // Mocking selectors and actions
      expect(generator.next().value).toEqual(
        select(drawtoolSelectors.selectDrawToolById, 'toolId'),
      );
      const drawingTool = {
        geoJSONLayerId: 'layerId',
        activeDrawModeId: '',
        isSelectable: true,
      };

      expect(generator.next(drawingTool).value).toEqual(
        select(
          drawtoolSelectors.getDrawModeById,
          payload.drawToolId,
          payload.drawModeId,
        ),
      );
      expect(generator.next(drawingTool).value).toEqual(
        put(
          layerActions.toggleFeatureMode({
            layerId: 'layerId',
            isInEditMode: false,
            drawMode: '',
          }),
        ),
      );

      expect(generator.next(drawingTool).value).toEqual(
        select(layerSelectors.getLayerById, drawingTool.geoJSONLayerId),
      );

      expect(generator.next().done).toBe(true);
    });

    it('should handle changing the draw tool', () => {
      const payload = {
        drawModeId: 'newModeId',
        drawToolId: 'toolId',
      };

      const generator = changeDrawToolSaga(
        drawtoolActions.changeDrawToolMode(payload),
      );

      expect(generator.next().value).toEqual(
        select(drawtoolSelectors.selectDrawToolById, 'toolId'),
      );
      const newDrawMode: DrawMode = {
        drawModeId: 'drawtools-box',
        value: DRAWMODE.POLYGON,
        title: 'Box',
        shape: testGeoJSON.features[0],
        isSelectable: true,
      };
      const drawingTool = {
        geoJSONLayerId: 'layerId',
        activeDrawModeId: 'oldModeId',
      };

      expect(generator.next(drawingTool).value).toEqual(
        select(
          drawtoolSelectors.getDrawModeById,
          payload.drawToolId,
          payload.drawModeId,
        ),
      );

      expect(generator.next(newDrawMode).value).toEqual(
        select(layerSelectors.getLayerById, 'layerId'),
      );

      expect(generator.next().value).toEqual(
        put(
          layerActions.setSelectedFeature({
            layerId: 'layerId',
            selectedFeatureIndex: testGeoJSON.features.length - 1,
          }),
        ),
      );

      expect(generator.next(newDrawMode).value).toEqual(
        put(
          mapStoreActions.updateFeature({
            layerId: 'layerId',
            geojson: testGeoJSON,
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        put(
          layerActions.toggleFeatureMode({
            layerId: 'layerId',
            isInEditMode: true,
            drawMode: newDrawMode.value,
          }),
        ),
      );

      expect(generator.next().done).toBe(true);
    });

    it('should handle changing the draw tool for non selectable tool', () => {
      const payload = {
        drawModeId: 'newModeId',
        drawToolId: 'toolId',
      };

      const generator = changeDrawToolSaga(
        drawtoolActions.changeDrawToolMode(payload),
      );

      expect(generator.next().value).toEqual(
        select(drawtoolSelectors.selectDrawToolById, 'toolId'),
      );
      const newDrawMode: DrawMode = {
        drawModeId: 'drawtools-box',
        value: DRAWMODE.POLYGON,
        title: 'Box',
        shape: testGeoJSON.features[0],
        isSelectable: false,
      };
      const drawingTool = {
        geoJSONLayerId: 'layerId',
        activeDrawModeId: 'oldModeId',
      };

      expect(generator.next(drawingTool).value).toEqual(
        select(
          drawtoolSelectors.getDrawModeById,
          payload.drawToolId,
          payload.drawModeId,
        ),
      );

      expect(generator.next(newDrawMode).value).toEqual(
        select(layerSelectors.getLayerById, 'layerId'),
      );

      expect(generator.next().value).toEqual(
        put(
          layerActions.setSelectedFeature({
            layerId: 'layerId',
            selectedFeatureIndex: testGeoJSON.features.length - 1,
          }),
        ),
      );

      expect(generator.next(newDrawMode).value).toEqual(
        put(
          mapStoreActions.updateFeature({
            layerId: 'layerId',
            geojson: testGeoJSON,
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        put(
          layerActions.toggleFeatureMode({
            layerId: 'layerId',
            isInEditMode: false,
            drawMode: newDrawMode.value,
          }),
        ),
      );

      expect(generator.next().done).toBe(true);
    });

    it('should handle changing the draw tool for shouldAllowMultipleShapes when changing drawtool', () => {
      const payload = {
        drawModeId: 'newModeId',
        drawToolId: 'toolId',
      };

      const generator = changeDrawToolSaga(
        drawtoolActions.changeDrawToolMode(payload),
      );

      expect(generator.next().value).toEqual(
        select(drawtoolSelectors.selectDrawToolById, 'toolId'),
      );
      const newDrawMode: DrawMode = {
        drawModeId: 'drawtools-box',
        value: DRAWMODE.POLYGON,
        title: 'Box',
        shape: testGeoJSON.features[0],
        isSelectable: true,
      };
      const drawingTool = {
        geoJSONLayerId: 'layerId',
        activeDrawModeId: 'oldModeId',
        shouldAllowMultipleShapes: true,
      };

      expect(generator.next(drawingTool).value).toEqual(
        select(
          drawtoolSelectors.getDrawModeById,
          payload.drawToolId,
          payload.drawModeId,
        ),
      );

      expect(generator.next(newDrawMode).value).toEqual(
        select(layerSelectors.getLayerById, 'layerId'),
      );

      const currentGeoJSON: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          testGeoJSON.features[0],
          {
            ...testGeoJSON.features[0],
            properties: { selectionType: 'fir-new' },
          },
        ],
      };

      const currentGeoJSONLayer = {
        geojson: currentGeoJSON,
        selectedFeatureIndex: 1,
      };

      const featureCollection = getFeatureCollection(
        newDrawMode.shape,
        drawingTool.shouldAllowMultipleShapes,
        currentGeoJSON,
      );
      const newGeoJSON = getGeoJson(
        featureCollection,
        drawingTool.shouldAllowMultipleShapes,
      );

      expect(generator.next(currentGeoJSONLayer).value).toEqual(
        put(
          layerActions.setSelectedFeature({
            layerId: 'layerId',
            selectedFeatureIndex: newGeoJSON.features.length - 1,
          }),
        ),
      );

      expect(generator.next(newDrawMode).value).toEqual(
        put(
          mapStoreActions.updateFeature({
            layerId: 'layerId',
            geojson: newGeoJSON,
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        put(
          layerActions.toggleFeatureMode({
            layerId: 'layerId',
            isInEditMode: true,
            drawMode: newDrawMode.value,
          }),
        ),
      );

      expect(generator.next().done).toBe(true);
    });

    it('should select last feature when changing same tool for shouldAllowMultipleShapes', () => {
      const payload = {
        drawModeId: 'newModeId',
        drawToolId: 'toolId',
      };

      const generator = changeDrawToolSaga(
        drawtoolActions.changeDrawToolMode(payload),
      );

      expect(generator.next().value).toEqual(
        select(drawtoolSelectors.selectDrawToolById, 'toolId'),
      );
      const newDrawMode: DrawMode = {
        drawModeId: 'drawtools-box',
        value: DRAWMODE.POLYGON,
        title: 'Box',
        shape: testGeoJSON.features[0],
        isSelectable: true,
      };
      const drawingTool = {
        geoJSONLayerId: 'layerId',
        activeDrawModeId: 'oldModeId',
        shouldAllowMultipleShapes: true,
      };

      expect(generator.next(drawingTool).value).toEqual(
        select(
          drawtoolSelectors.getDrawModeById,
          payload.drawToolId,
          payload.drawModeId,
        ),
      );

      expect(generator.next(newDrawMode).value).toEqual(
        select(layerSelectors.getLayerById, 'layerId'),
      );

      const currentGeoJSON: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [testGeoJSON.features[0], testGeoJSON.features[0]],
      };

      const currentGeoJSONLayer = {
        geojson: currentGeoJSON,
        selectedFeatureIndex: 1,
      };

      expect(generator.next(currentGeoJSONLayer).value).toEqual(
        put(
          layerActions.toggleFeatureMode({
            layerId: 'layerId',
            isInEditMode: newDrawMode.isSelectable,
            drawMode: newDrawMode.value,
          }),
        ),
      );

      expect(generator.next().done).toBe(true);
    });

    it('should handle changing the draw tool and not update any shape', () => {
      const payload = {
        drawModeId: 'newModeId',
        drawToolId: 'toolId',
        shouldUpdateShape: false,
      };

      const generator = changeDrawToolSaga(
        drawtoolActions.changeDrawToolMode(payload),
      );

      expect(generator.next().value).toEqual(
        select(drawtoolSelectors.selectDrawToolById, 'toolId'),
      );

      const newDrawMode: DrawMode = {
        drawModeId: 'drawtools-box',
        value: DRAWMODE.POLYGON,
        title: 'Box',
        shape: testGeoJSON.features[0],
        isSelectable: false,
      };
      const drawingTool = {
        geoJSONLayerId: 'layerId',
        activeDrawModeId: 'oldModeId',
        shouldAllowMultipleShapes: true,
      };

      expect(generator.next(drawingTool).value).toEqual(
        select(
          drawtoolSelectors.getDrawModeById,
          payload.drawToolId,
          payload.drawModeId,
        ),
      );

      expect(generator.next(newDrawMode).value).toEqual(
        select(layerSelectors.getLayerById, 'layerId'),
      );

      expect(generator.next().value).toEqual(
        put(
          layerActions.toggleFeatureMode({
            layerId: 'layerId',
            isInEditMode: newDrawMode.isSelectable,
            drawMode: newDrawMode.value,
          }),
        ),
      );

      expect(generator.next().done).toBe(true);
    });

    it('should handle changing the draw tool and update intersection shape', () => {
      const payload = {
        drawModeId: 'newModeId',
        drawToolId: 'toolId',
      };

      const generator = changeDrawToolSaga(
        drawtoolActions.changeDrawToolMode(payload),
      );

      expect(generator.next().value).toEqual(
        select(drawtoolSelectors.selectDrawToolById, 'toolId'),
      );
      const newDrawMode: DrawMode = {
        drawModeId: 'drawtools-box',
        value: DRAWMODE.POLYGON,
        title: 'Box',
        shape: testGeoJSON.features[0],
        isSelectable: true,
      };
      const drawingTool = {
        geoJSONLayerId: 'layerId',
        activeDrawModeId: 'oldModeId',
        shouldAllowMultipleShapes: true,
        geoJSONIntersectionLayerId: 'intersection-layer',
        geoJSONIntersectionBoundsLayerId: 'bounds-layer',
      };

      expect(generator.next(drawingTool).value).toEqual(
        select(
          drawtoolSelectors.getDrawModeById,
          payload.drawToolId,
          payload.drawModeId,
        ),
      );

      expect(generator.next(newDrawMode).value).toEqual(
        select(layerSelectors.getLayerById, 'layerId'),
      );
      const currentGeoJSON: GeoJSON.FeatureCollection = {
        type: 'FeatureCollection',
        features: [
          testGeoJSON.features[0],
          {
            ...testGeoJSON.features[0],
            properties: { selectionType: 'fir-new' },
          },
        ],
      };

      const currentGeoJSONLayer = {
        geojson: currentGeoJSON,
        selectedFeatureIndex: 1,
      };

      const featureCollection = getFeatureCollection(
        newDrawMode.shape,
        drawingTool.shouldAllowMultipleShapes,
        currentGeoJSON,
      );
      const newGeoJSON = getGeoJson(
        featureCollection,
        drawingTool.shouldAllowMultipleShapes,
      );

      expect(generator.next(currentGeoJSONLayer).value).toEqual(
        put(
          layerActions.setSelectedFeature({
            layerId: 'layerId',
            selectedFeatureIndex: newGeoJSON.features.length - 1,
          }),
        ),
      );

      expect(generator.next(newDrawMode).value).toEqual(
        put(
          mapStoreActions.updateFeature({
            layerId: 'layerId',
            geojson: newGeoJSON,
            geoJSONIntersectionLayerId: drawingTool.geoJSONIntersectionLayerId,
            geoJSONIntersectionBoundsLayerId:
              drawingTool.geoJSONIntersectionBoundsLayerId,
          }),
        ),
      );
      expect(generator.next().value).toEqual(
        put(
          layerActions.toggleFeatureMode({
            layerId: 'layerId',
            isInEditMode: true,
            drawMode: newDrawMode.value,
          }),
        ),
      );

      expect(generator.next().done).toBe(true);
    });

    it('should handle errors', () => {
      const payload = {
        drawModeId: 'someId',
        drawToolId: 'toolId',
      };
      jest.spyOn(console, 'log').mockImplementationOnce(() => {});

      const spy = jest.spyOn(global.console, 'log');

      const generator = changeDrawToolSaga(
        drawtoolActions.changeDrawToolMode(payload),
      );

      // Mocking selectors and actions
      expect(generator.next().value).toEqual(
        select(drawtoolSelectors.selectDrawToolById, 'toolId'),
      );
      const drawingTool = {
        geoJSONLayerId: 'layerId',
      };
      expect(generator.next(drawingTool).value).toEqual(
        select(drawtoolSelectors.getDrawModeById, 'toolId', 'someId'),
      );

      // Simulate an error
      const error = new Error('Test error');
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      expect(generator.throw(error).value).toBeUndefined(); // Handle the error

      // Make sure the saga is finished
      expect(generator.next().done).toBe(true);
      expect(spy).toHaveBeenCalledWith('error changeDrawToolSaga', error);
    });
  });

  describe('changeIntersectionSaga', () => {
    it('should change geojson intersection bounds and clear other layers', () => {
      const payload = {
        drawToolId: 'drawtool-1',
        geoJSON: testGeoJSON,
      };
      const generator = changeIntersectionSaga(
        drawtoolActions.changeIntersectionBounds(payload),
      );

      expect(generator.next().value).toEqual(
        select(drawtoolSelectors.selectDrawToolById, payload.drawToolId),
      );

      const drawTool = {
        geoJSONIntersectionBoundsLayerId: 'test-bounds-layer',
        geoJSONLayerId: 'test-geojson-layer',
        geoJSONIntersectionLayerId: 'test-intersection-layer',
      };

      expect(generator.next(drawTool).value).toEqual(
        put(
          layerActions.layerChangeGeojson({
            layerId: drawTool.geoJSONIntersectionBoundsLayerId,
            geojson: payload.geoJSON,
          }),
        ),
      );

      expect(generator.next(drawTool).value).toEqual(
        put(
          layerActions.layerChangeGeojson({
            layerId: drawTool.geoJSONLayerId,
            geojson: emptyGeoJSON,
          }),
        ),
      );

      expect(generator.next(drawTool).value).toEqual(
        put(
          layerActions.layerChangeGeojson({
            layerId: drawTool.geoJSONIntersectionLayerId,
            geojson: emptyGeoJSON,
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('drawingSaga', () => {
    it('should intercept actions and fire sagas', () => {
      const generator = drawingSaga();

      expect(generator.next().value).toEqual(
        takeLatest(drawtoolActions.registerDrawTool, registerDrawToolSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(drawtoolActions.changeDrawToolMode, changeDrawToolSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(
          drawtoolActions.changeIntersectionBounds,
          changeIntersectionSaga,
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });
});
