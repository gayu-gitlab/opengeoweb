/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import produce from 'immer';
import { defaultReduxLayerRadarKNMI } from '../utils/testUtils';
import WMLayer from './WMLayer';
import WMJSMap from './WMJSMap';
import { getMapImageStore } from './WMImageStore';
import {
  prefetchImagesForAnimation,
  prefetchImagesForNonAnimation,
} from './WMPrefetch';

describe('prefetchImages', () => {
  const date = '2023-01-31T';
  const t10 = `${date}10:00:00Z`;
  const t11 = `${date}11:00:00Z`;
  const t1130 = `${date}11:30:00Z`;
  const t12currentTime = `${date}12:00:00Z`;
  const t1230 = `${date}12:30:00Z`;
  const t13 = `${date}13:00:00Z`;

  const service0 = 'http://www.service0.com';
  const layer0oneHourTimeStep = produce(defaultReduxLayerRadarKNMI, (draft) => {
    const oneHourTimestep = `PT1H`;
    const dimensions = [
      {
        name: 'time',
        units: 'ISO8601',
        currentValue: t12currentTime,
        // layer has images for 10, 11, 12, 13 clock
        values: `${t10}/${t13}/${oneHourTimestep}`,
      },
    ];
    draft.dimensions = dimensions;
    draft.id = '0';
    draft.service = service0;
  });

  const service1 = 'http://www.service1.com';
  const layer1halfHourTimeStep = produce(
    defaultReduxLayerRadarKNMI,
    (draft) => {
      const halfHourTimestep = `PT30M`;
      const dimensions = [
        {
          name: 'time',
          units: 'ISO8601',
          currentValue: t12currentTime,
          // layer has images for 1130, 12, 1230 clock
          values: `${t1130}/${t1230}/${halfHourTimestep}`,
        },
      ];
      draft.dimensions = dimensions;
      draft.id = '1';
      draft.service = service1;
    },
  );

  // avoid doing fetch for getCapabilities for layer
  jest
    .spyOn(WMLayer.prototype, 'parseLayer')
    .mockImplementation((): void => {});

  const wmLayer0 = new WMLayer(layer0oneHourTimeStep);
  const wmLayer1 = new WMLayer(layer1halfHourTimeStep);

  const wmMap = new WMJSMap(document.createElement('test'));
  wmMap.setDimension('time', t12currentTime);

  wmMap.addLayer(wmLayer0);
  wmMap.addLayer(wmLayer1);

  // track calls to load images and avoid doing backend calls
  const imageLoadCalls = jest
    .spyOn(global.Image.prototype, 'src', 'set')
    .mockImplementation();

  beforeEach(() => {
    imageLoadCalls.mockClear();
    getMapImageStore.clear();
    wmMap.isAnimating = false;
  });
  describe('prefetchImagesForNonAnimation', () => {
    beforeEach(() => {
      wmMap.timestepInMinutes = 60;
    });
    it('prefetches available images in correct order, forward and backward', async () => {
      prefetchImagesForNonAnimation(wmMap);

      expect(imageLoadCalls).toHaveBeenCalledTimes(7);

      const callsUrls = imageLoadCalls.mock.calls.map(
        (call) => new URL(call[0]),
      );

      expect(callsUrls[0].origin).toEqual(service0);
      expect(callsUrls[0].searchParams.get('TIME')).toEqual(t12currentTime);
      expect(callsUrls[1].origin).toEqual(service1);
      expect(callsUrls[1].searchParams.get('TIME')).toEqual(t12currentTime);

      expect(callsUrls[2].origin).toEqual(service0);
      expect(callsUrls[2].searchParams.get('TIME')).toEqual(t13);
      expect(callsUrls[3].origin).toEqual(service1);
      expect(callsUrls[3].searchParams.get('TIME')).toEqual(t1230);

      expect(callsUrls[4].origin).toEqual(service0);
      expect(callsUrls[4].searchParams.get('TIME')).toEqual(t11);
      expect(callsUrls[5].origin).toEqual(service1);
      expect(callsUrls[5].searchParams.get('TIME')).toEqual(t1130);

      expect(callsUrls[6].origin).toEqual(service0);
      expect(callsUrls[6].searchParams.get('TIME')).toEqual(t10);

      // map still has correct current time
      const mapCurrentValue = new Date(wmMap.getDimension('time').currentValue);
      expect(mapCurrentValue).toEqual(new Date(t12currentTime));
    });

    it('prefetches available images within max count of images to prefetch', async () => {
      prefetchImagesForNonAnimation(wmMap, 4);

      expect(imageLoadCalls).toHaveBeenCalledTimes(4);

      const callsUrls = imageLoadCalls.mock.calls.map(
        (call) => new URL(call[0]),
      );

      expect(callsUrls[0].origin).toEqual(service0);
      expect(callsUrls[0].searchParams.get('TIME')).toEqual(t12currentTime);
      expect(callsUrls[1].origin).toEqual(service1);
      expect(callsUrls[1].searchParams.get('TIME')).toEqual(t12currentTime);

      expect(callsUrls[2].origin).toEqual(service0);
      expect(callsUrls[2].searchParams.get('TIME')).toEqual(t13);
      expect(callsUrls[3].origin).toEqual(service1);
      expect(callsUrls[3].searchParams.get('TIME')).toEqual(t1230);
    });
    it('prefetches available images when timestep is 2 hours', async () => {
      wmMap.timestepInMinutes = 120;

      prefetchImagesForNonAnimation(wmMap);

      expect(imageLoadCalls).toHaveBeenCalledTimes(6);

      const callsUrls = imageLoadCalls.mock.calls.map(
        (call) => new URL(call[0]),
      );

      expect(callsUrls[0].origin).toEqual(service0);
      expect(callsUrls[0].searchParams.get('TIME')).toEqual(t12currentTime);
      expect(callsUrls[1].origin).toEqual(service1);
      expect(callsUrls[1].searchParams.get('TIME')).toEqual(t12currentTime);

      expect(callsUrls[2].origin).toEqual(service0);
      expect(callsUrls[2].searchParams.get('TIME')).toEqual(t13);
      expect(callsUrls[3].origin).toEqual(service1);
      expect(callsUrls[3].searchParams.get('TIME')).toEqual(t1230);

      expect(callsUrls[4].origin).toEqual(service0);
      expect(callsUrls[4].searchParams.get('TIME')).toEqual(t10);
      expect(callsUrls[5].origin).toEqual(service1);
      expect(callsUrls[5].searchParams.get('TIME')).toEqual(t1130);
    });
  });
  describe('prefetchImagesForAnimation', () => {
    beforeEach(() => {
      wmMap.isAnimating = true;
      wmMap.animationList = [
        {
          name: 'time',
          value: t11,
          requests: [
            { url: `${service0}/image1`, headers: [] },
            { url: `${service1}/image1`, headers: [] },
          ],
        },
        {
          name: 'time',
          value: t12currentTime,
          requests: [
            { url: `${service0}/image2`, headers: [] },
            { url: `${service1}/image2`, headers: [] },
          ],
        },
        {
          name: 'time',
          value: t13,
          requests: [
            { url: `${service0}/image3`, headers: [] },
            { url: `${service1}/image3`, headers: [] },
          ],
        },
      ];
      wmMap.currentAnimationStep = 1;
    });
    it('prefetches images inside animation period, only forward until end then at the beginning of animation period', () => {
      prefetchImagesForAnimation(wmMap);

      expect(imageLoadCalls).toBeCalledTimes(6);

      const callsUrls = imageLoadCalls.mock.calls.map(
        (call) => new URL(call[0]),
      );

      expect(callsUrls[0].origin).toEqual(service0);
      expect(callsUrls[0].pathname).toEqual('/image2');
      expect(callsUrls[1].origin).toEqual(service1);
      expect(callsUrls[1].pathname).toEqual('/image2');

      expect(callsUrls[2].origin).toEqual(service0);
      expect(callsUrls[2].pathname).toEqual('/image3');
      expect(callsUrls[3].origin).toEqual(service1);
      expect(callsUrls[3].pathname).toEqual('/image3');

      expect(callsUrls[4].origin).toEqual(service0);
      expect(callsUrls[4].pathname).toEqual('/image1');
      expect(callsUrls[5].origin).toEqual(service1);
      expect(callsUrls[5].pathname).toEqual('/image1');
    });

    it('prefetches images inside animation period, within max count of images to prefetch', () => {
      const maxCountOfImagesToPrefetch = 4;
      prefetchImagesForAnimation(wmMap, maxCountOfImagesToPrefetch);

      expect(imageLoadCalls).toBeCalledTimes(4);

      const callsUrls = imageLoadCalls.mock.calls.map(
        (call) => new URL(call[0]),
      );

      expect(callsUrls[0].origin).toEqual(service0);
      expect(callsUrls[0].pathname).toEqual('/image2');
      expect(callsUrls[1].origin).toEqual(service1);
      expect(callsUrls[1].pathname).toEqual('/image2');

      expect(callsUrls[2].origin).toEqual(service0);
      expect(callsUrls[2].pathname).toEqual('/image3');
      expect(callsUrls[3].origin).toEqual(service1);
      expect(callsUrls[3].pathname).toEqual('/image3');
    });

    it('prefetches images inside animation period. Animation period has time without requests.', () => {
      const t14WhichLayerDoesntHaveImageFor = `${date}14:00:00Z`;
      const t15WhichLayerDoesntHaveImageFor = `${date}15:00:00Z`;
      wmMap.animationList = [
        {
          name: 'time',
          value: t11,
          requests: [
            { url: `${service0}/image1`, headers: [] },
            { url: `${service1}/image1`, headers: [] },
          ],
        },
        {
          name: 'time',
          value: t12currentTime,
          requests: [
            { url: `${service0}/image2`, headers: [] },
            { url: `${service1}/image2`, headers: [] },
          ],
        },
        {
          name: 'time',
          value: t13,
          requests: [
            { url: `${service0}/image3`, headers: [] },
            { url: `${service1}/image3`, headers: [] },
          ],
        },
        {
          name: 'time',
          value: t14WhichLayerDoesntHaveImageFor,
          requests: [],
        },
        {
          name: 'time',
          value: t15WhichLayerDoesntHaveImageFor,
        },
      ];

      prefetchImagesForAnimation(wmMap);

      expect(imageLoadCalls).toBeCalledTimes(6);

      const { calls } = imageLoadCalls.mock;
      const callsUrls = calls.map((call) => new URL(call[0]));

      expect(callsUrls[0].origin).toEqual(service0);
      expect(callsUrls[0].pathname).toEqual('/image2');
      expect(callsUrls[1].origin).toEqual(service1);
      expect(callsUrls[1].pathname).toEqual('/image2');

      expect(callsUrls[2].origin).toEqual(service0);
      expect(callsUrls[2].pathname).toEqual('/image3');
      expect(callsUrls[3].origin).toEqual(service1);
      expect(callsUrls[3].pathname).toEqual('/image3');

      expect(callsUrls[4].origin).toEqual(service0);
      expect(callsUrls[4].pathname).toEqual('/image1');
      expect(callsUrls[5].origin).toEqual(service1);
      expect(callsUrls[5].pathname).toEqual('/image1');
    });
  });
});
