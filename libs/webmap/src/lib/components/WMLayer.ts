/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { FeatureCollection } from 'geojson';
import type WMJSMap from './WMJSMap';
import I18n from '../utils/I18n/lang.en';
import {
  isDefined,
  WMJScheckURL,
  getUriWithParam,
  debugLogger,
  DebugType,
  buildLayerTreeFromNestedWMSLayer,
} from './WMJSTools';
import { WMEmptyLayerTitle, WMSVersion } from './WMConstants';
import WMJSDimension from './WMJSDimension';
import WMProjection from './WMProjection';
import WMBBOX from './WMBBOX';
import WMGetServiceFromStore from './WMGetServiceFromStore';
import { Capability, GetCapabilitiesJson } from './WMJSService';
import {
  Style,
  Dimension,
  LayerProps,
  GeographicBoundingBox,
  LayerTree,
} from './types';

export enum LayerType {
  mapLayer = 'mapLayer',
  baseLayer = 'baseLayer',
  overLayer = 'overLayer',
  featureLayer = 'featureLayer',
}

export interface LayerFoundation {
  acceptanceTimeInMinutes?: number;
  dimensions?: Dimension[];
  enabled?: boolean;
  format?: string;
  id?: string;
  layerType?: LayerType;
  name?: string;
  opacity?: number; // between 0.0 and 1.0
  service?: string;
  style?: string;
  title?: string;
  type?: string;
  geojson?: FeatureCollection;
}

export interface LayerOptions extends LayerFoundation {
  id: string;
  layerType: LayerType;
  service: string;
  active?: boolean;
  getgraphinfoURL?: string;
  currentStyle?: string;
  sldURL?: string;
  keepOnTop?: boolean;
  transparent?: boolean;
  onReady?: (param: LayerOptions) => void;
  parentMap?: WMJSMap;
  headers?: Headers[];
  failure?: (layer: WMLayer, message: string) => void;
  ReactWMJSLayerId?: string;
  onLayerError?: (
    layer: WMLayer,
    error: Error,
    webmapInstance: WMJSMap,
  ) => void;
  onLayerReady?: (layer: WMLayer, webmap?: WMJSMap) => void;
  geojson?: GeoJSON.FeatureCollection;
}

interface WMSLayerDimInfo {
  name: string;
  units: string;
  unitSymbol: string;
  value: string;
  default: string;
  linkDimensionToMap: boolean;
}

/**
 * Helper function to figure out the dimensions from the WMS layer object (From WMS GetCapabilities). Parses both WMS 1.1.1 and WMS 1.3.0.
 * @param layerObjectFromWMS The corresponding layer object from the WMS GetCapabilities.
 * @param layer THe WMLayer to configure
 * @param layerDimNamesToRemove A set of dimensions names which are flagged for removal, this function removes the dimension from the set if found in the layer.
 */
export const addDimsForLayer = (
  layerProps: LayerProps,
  layer: WMLayer,
  layerDimNamesToRemove: Set<string>,
): void => {
  /* Loop through all the dims from the WMS GetCapabilities document for this layer */
  layerProps.dimensions?.forEach((layerDimension) => {
    /* Obtain layerDim info from the GetCapabilities document and put into a WMSLayerDimInfo object  */
    const dimName = (layerDimension.name || '').toLowerCase();
    const layerDim: WMSLayerDimInfo = {
      name: dimName,
      units: layerDimension.units || '',
      unitSymbol: layerDimension.unitSymbol || '',
      value: layerDimension.values || '',
      default: layerDimension.currentValue,
      /* If presets with other dimensions, like elevation, cause
      problems, try to add the dimension here */
      linkDimensionToMap:
        dimName !== 'reference_time' &&
        dimName !== 'interval_start' &&
        dimName !== 'interval_end',
    };

    /* Flag that this dim should not be removed from the layer */
    layerDimNamesToRemove.delete(layerDim.name);

    /* Now configure the dimensions in the Layer based on the obtained info */
    const wmLayerDimensionIndex = layer.dimensions.findIndex(
      (dim): boolean => dim.name === layerDim.name,
    );

    /* Index === -1: Dimension was not found */
    if (wmLayerDimensionIndex === -1) {
      /* Add a new dimension for this layer */
      const dimension = new WMJSDimension({
        name: layerDim.name,
        units: layerDim.units,
        values: layerDim.value,
        currentValue: layerDim.default,
        defaultValue: layerDim.default,
        linked: layerDim.linkDimensionToMap,
        unitSymbol: layerDim.unitSymbol,
      });
      /* Check if the dimension should take the value from the map */
      if (layer.parentMap) {
        const mapDim = layer.parentMap.getDimension(layerDim.name);
        if (mapDim && mapDim.linked && isDefined(mapDim.currentValue)) {
          const dimensionCurrentValue = dimension.getClosestValue(
            mapDim.currentValue,
          );
          dimension.setValue(dimensionCurrentValue);
        }
      }
      layer.dimensions.push(dimension);
    } else {
      /* Dimension already exists, keep its current value but update the rest of its properties */
      const dimensionToUpdate = layer.dimensions[wmLayerDimensionIndex];
      const currentValue = dimensionToUpdate.getValue();
      dimensionToUpdate.values = layerDim.value;
      dimensionToUpdate.name = layerDim.name;
      dimensionToUpdate.units = layerDim.units;
      dimensionToUpdate.unitSymbol = layerDim.unitSymbol;
      dimensionToUpdate.reInitializeValues(dimensionToUpdate.values);
      dimensionToUpdate.setClosestValue(currentValue);
    }
  });
};

/**
 * Helper function to configure the WMS Styles from the WMS GetCapabilities document.
 * @param nestedLayerPath Array of WMS Layers from `[Root WMS Layer] => [Parent(s) WMS Layer] => [This WMS layer]`.
 * @param wmLayer The WMLayer object to configure
 */
export const configureStyles = (
  layerProps: LayerProps,
  wmLayer: WMLayer,
): void => {
  // eslint-disable-next-line no-param-reassign
  wmLayer.styles = layerProps.styles || [];

  /* Set the default style */
  if (wmLayer.styles.length > 0) {
    /* Check if the current style is in the list */
    const styleIndex = wmLayer.styles.findIndex(
      (style) => style.name === wmLayer.currentStyle,
    );

    /* If not found or not set, set the first one */
    if (styleIndex === -1) {
      wmLayer.setStyle(wmLayer.styles[0].name);
    } else {
      /* Otherwise set it */
      wmLayer.setStyle(wmLayer.styles[styleIndex].name);
    }
  }
};

/**
 * Helper function to configure the dimensions for this layer from the WMS GetCapabilities document.
 * @param nestedLayerPath Array of WMS Layers from `[Root WMS Layer] => [Parent(s) WMS Layer] => [This WMS layer]`.
 * @param wmLayer The WMLayer object to configure
 */
export const configureDimensions = (
  layerProps: LayerProps,
  wmLayer: WMLayer,
): void => {
  /*
  WMS 1.1.1:
    <Dimension name="time" units="ISO8601"/>
    <Extent name="time" default="2021-05-14T10:35:00Z" multipleValues="1" nearestValue="0">2021-03-31T09:25:00Z/2021-05-14T10:35:00Z/PT5M</Extent>

  WMS 1.3.0:
    <Dimension name="time" units="ISO8601" default="2021-05-14T07:35:00Z" multipleValues="1" nearestValue="0" current="1">2021-03-31T09:25:00Z/2021-05-14T07:35:00Z/PT5M</Dimension>
  */

  /* Flag dimensions which are currently in use for this layer, it is possible that the update will remove dimensions from this layer; keep track of the current ones */
  const layerDimNamesToRemove = new Set<string>();
  for (const layerDim of wmLayer.dimensions) {
    layerDimNamesToRemove.add(layerDim.name);
  }

  /* Now start adding all dimensions starting with the parent layer. Layers more deeply nested will inherit Dimension info */

  addDimsForLayer(layerProps, wmLayer, layerDimNamesToRemove);

  /* The list with dimensions to remove is now complete, remove the unneeded dimensions from the layer */
  layerDimNamesToRemove.forEach((dimName: string): void => {
    const dimIndex = wmLayer.dimensions.findIndex(
      (dim) => dim.name === dimName,
    );
    if (dimIndex !== -1) {
      wmLayer.dimensions.splice(dimIndex, 1);
    }
  });

  /* Handle reference time dim */
  const refTimeDimension = wmLayer.getDimension('reference_time');
  if (refTimeDimension) {
    wmLayer.handleReferenceTime('reference_time', refTimeDimension.getValue());
  }

  if (wmLayer.parentMap) {
    wmLayer.parentMap.configureMapDimensions(wmLayer);
  }
};

export const configureGeographicBoundingBox = (
  layerProps: LayerProps,
  wmLayer: WMLayer,
): void => {
  const bbox = layerProps.geographicBoundingBox;
  if (bbox) {
    // eslint-disable-next-line no-param-reassign
    wmLayer.geographicBoundingBox = bbox;
  }
};

export default class WMLayer {
  id!: string;

  name?: string;

  ReactWMJSLayerId?: string;

  hasError?: boolean;

  public enabled?: boolean;

  lastError?: string;

  legendGraphic?: string;

  title?: string;

  public opacity?: number;

  service!: string;

  layerType!: LayerType;

  currentStyle?: string;

  linkedInfo!: { layer: string; message: string };

  headers!: Headers[];

  autoupdate!: boolean;

  timer!: NodeJS.Timeout | number;

  getmapURL!: string;

  getfeatureinfoURL!: string;

  getlegendgraphicURL!: string;

  keepOnTop!: boolean;

  transparent!: boolean;

  legendIsDimensionDependent!: boolean;

  wms130bboxcompatibilitymode!: boolean;

  version!: string;

  path!: string;

  type!: string;

  abstract!: string;

  dimensions!: WMJSDimension[];

  projectionProperties!: WMProjection[];

  queryable!: boolean;

  styles!: Style[];

  serviceTitle!: string;

  parentMap!: WMJSMap;

  sldURL!: string;

  active!: boolean;

  getgraphinfoURL!: string;

  format!: string;

  _options!: LayerOptions;

  onReady!: (param: LayerOptions) => void;

  optimalFormat!: string;

  wmsextensions!: { url: string; colorscalerange: Record<string, unknown>[] };

  isConfigured!: boolean;

  geographicBoundingBox?: GeographicBoundingBox;

  geojson?: GeoJSON.FeatureCollection;

  init(): void {
    this.autoupdate = false;
    this.timer = undefined!;
    this.service = undefined!; // URL of the WMS Service
    this.getmapURL = undefined!;
    this.getfeatureinfoURL = undefined!;
    this.getlegendgraphicURL = undefined!;
    this.keepOnTop = false;
    this.transparent = true;
    this.hasError = false;
    this.legendIsDimensionDependent = true;
    this.wms130bboxcompatibilitymode = false;
    this.version = WMSVersion.version111;
    this.path = '';
    this.type = 'wms';
    this.wmsextensions = { url: '', colorscalerange: [] };
    this.name = undefined;
    this.title = WMEmptyLayerTitle;
    this.abstract = undefined!;
    this.dimensions = []; // Array of Dimension
    this.legendGraphic = '';
    this.projectionProperties = []; // Array of WMProjections
    this.queryable = false;
    this.enabled = true;
    this.styles = [];
    this.currentStyle = '';
    this.id = '-1';
    this.opacity = 1.0; // Ranges from 0.0-1.0
    this.serviceTitle = 'not defined';
    this.parentMap = null!;
    this.sldURL = null!;
    this.isConfigured = false;
  }

  constructor(options?: LayerOptions) {
    this.init = this.init.bind(this);
    this.getLayerName = this.getLayerName.bind(this);
    this.toggleAutoUpdate = this.toggleAutoUpdate.bind(this);
    this.setAutoUpdate = this.setAutoUpdate.bind(this);
    this.setOpacity = this.setOpacity.bind(this);
    this.getOpacity = this.getOpacity.bind(this);
    this.remove = this.remove.bind(this);
    this.moveUp = this.moveUp.bind(this);
    this.moveDown = this.moveDown.bind(this);
    this.zoomToLayer = this.zoomToLayer.bind(this);
    this.draw = this.draw.bind(this);
    this.handleReferenceTime = this.handleReferenceTime.bind(this);
    this.setDimension = this.setDimension.bind(this);
    this.parseLayer = this.parseLayer.bind(this);
    this.__parseGetCapForLayer = this.__parseGetCapForLayer.bind(this);
    this.parseLayerPromise = this.parseLayerPromise.bind(this);
    this.cloneLayer = this.cloneLayer.bind(this);
    this.setName = this.setName.bind(this);
    this.setStyle = this.setStyle.bind(this);
    this.getStyles = this.getStyles.bind(this);
    this.getStyleObject = this.getStyleObject.bind(this);
    this.getStyle = this.getStyle.bind(this);
    this.getDimension = this.getDimension.bind(this);
    this.getProjection = this.getProjection.bind(this);
    this.getCRS = this.getCRS.bind(this);
    this.getCRSByName = this.getCRSByName.bind(this);
    this.setSLDURL = this.setSLDURL.bind(this);
    this.display = this.display.bind(this);
    this.getDimensions = this.getDimensions.bind(this);
    this.init();
    this._options = options!;
    this.sldURL = null!;
    this.headers = [];
    if (options) {
      this.service = options.service;
      this.getmapURL = options.service;
      this.getfeatureinfoURL = options.service;
      this.getlegendgraphicURL = options.service;
      if (options.active === true) {
        this.active = true;
      } else {
        this.active = false;
      }
      this.name = options.name;
      if (options.getgraphinfoURL) {
        this.getgraphinfoURL = options.getgraphinfoURL;
      }
      if (options.style) {
        this.currentStyle = options.style;
      }
      if (options.currentStyle) {
        this.currentStyle = options.currentStyle;
      }
      if (options.sldURL) {
        this.sldURL = options.sldURL;
      }
      if (options.id) {
        this.id = options.id;
      }
      if (options.format) {
        this.format = options.format;
      } else {
        this.format = 'image/png';
      }
      if (isDefined(options.opacity)) {
        this.opacity = options.opacity;
      }
      if (options.title) {
        this.title = options.title;
      }
      this.abstract = I18n.not_available_message.text;

      if (options.enabled === false) {
        this.enabled = false;
      }

      if (options.keepOnTop === true) {
        this.keepOnTop = true;
      }

      if (options.transparent === false) {
        this.transparent = false;
      }
      if (options.dimensions?.length) {
        options.dimensions.forEach((dimension) => {
          this.dimensions.push(new WMJSDimension(dimension));
        });
      }
      if (isDefined(options.onReady)) {
        this.onReady = options.onReady;
      }
      if (isDefined(options.type)) {
        this.type = options.type;
      }
      if (options.parentMap) {
        this.parentMap = options.parentMap;
      }
      if (options.headers) {
        this.headers = options.headers;
      }
      if (options.geojson) {
        this.geojson = options.geojson;
      }
    }
  }

  getLayerName(): string {
    return this.name!;
  }

  toggleAutoUpdate(): void {
    this.autoupdate = !this.autoupdate;
    if (this.autoupdate) {
      const numDeltaMS = 60000;
      this.timer = setInterval(() => {
        this.parseLayer(undefined!, true, 'WMLayer toggleAutoUpdate');
      }, numDeltaMS);
    } else {
      clearInterval(this.timer as NodeJS.Timeout);
    }
  }

  setAutoUpdate(val: boolean, interval?: number, callback?: () => void): void {
    if (val !== this.autoupdate) {
      this.autoupdate = val;
      if (!val) {
        clearInterval(this.timer as NodeJS.Timeout);
      } else {
        this.timer = setInterval(() => {
          this.parseLayer(callback!, true, 'WMLayer setAutoUpdate');
        }, interval);
      }
    }
  }

  setOpacity(opacityValue: number): void {
    this.opacity = opacityValue;

    this.parentMap && this.parentMap.redrawBuffer();
  }

  getOpacity(): number {
    return this.opacity!;
  }

  remove(): void {
    if (this.parentMap) {
      this.parentMap.deleteLayer(this);
      this.parentMap.draw('WMLayer::remove');
    }
    clearInterval(this.timer as NodeJS.Timeout);
  }

  moveUp(): void {
    if (this.parentMap) {
      this.parentMap.moveLayerUp(this);
      this.parentMap.draw('WMLayer::moveUp');
    }
  }

  moveDown(): void {
    if (this.parentMap) {
      this.parentMap.moveLayerDown(this);
      this.parentMap.draw('WMLayer::moveDown');
    }
  }

  zoomToLayer(): void {
    if (this.parentMap) {
      this.parentMap.zoomToLayer(this);
    }
  }

  draw(e: string): void {
    if (this.parentMap) {
      this.parentMap.draw(`WMLayer::draw::${e}`);
    }
  }

  handleReferenceTime(
    name: string,
    value: string,
    updateMapDimensions = true,
  ): void {
    if (name !== 'reference_time') {
      return;
    }
    const timeDim = this.getDimension('time');
    const referenceTimeDim = this.getDimension(name);
    if (timeDim) {
      timeDim.setTimeValuesForReferenceTime(value, referenceTimeDim!);
      if (updateMapDimensions && this.parentMap && this.enabled !== false) {
        this.parentMap.getListener().triggerEvent('ondimchange', 'time');
      }
    }
  }

  getDimensions(): WMJSDimension[] {
    return this.dimensions;
  }

  setDimension(name: string, value: string, updateMapDimensions = true): void {
    if (!isDefined(value)) {
      return;
    }

    const dim = this.getDimension(name);
    if (!dim) {
      return;
    }

    dim.setValue(value);

    this.handleReferenceTime(name, value, updateMapDimensions);
    if (updateMapDimensions && dim.linked === true && this.parentMap) {
      this.parentMap.setDimension(name, dim.getValue());
    }
  }

  __parseGetCapForLayer(
    getcapabilitiesjson: GetCapabilitiesJson,
    layerDoneCallback: (layer: WMLayer) => void,
    fail: (layer: WMLayer, message: string) => void,
  ): void {
    if (!getcapabilitiesjson) {
      this.title = I18n.service_has_error.text;
      this.abstract = I18n.not_available_message.text;
      fail(this, I18n.unable_to_connect_server.text);
      return;
    }

    const wmjsService = WMGetServiceFromStore(this.service);

    // Get the capability object
    let capabilityObject: Capability;
    try {
      capabilityObject = wmjsService.getCapabilityElement(getcapabilitiesjson);
    } catch {
      fail(this, 'No capability element in service');
      return;
    }

    this.version = wmjsService.version;

    // Get the rootLayer
    const rootLayer = capabilityObject.Layer;
    if (!isDefined(rootLayer)) {
      fail(this, 'No Layer element in service');
      return;
    }

    try {
      this.serviceTitle = rootLayer.Title.value;
    } catch (e) {
      this.serviceTitle = 'Unnamed service';
    }

    this.optimalFormat = 'image/png';
    // Get the optimal image format for this layer
    try {
      const serverFormats = capabilityObject.Request.GetMap.Format;
      for (const serverFormat of serverFormats) {
        if (serverFormat.value.indexOf('24') > 0) {
          this.optimalFormat = serverFormat.value;
        }
        if (serverFormat.value.indexOf('32') > 0) {
          this.optimalFormat = serverFormat.value;
        }
      }
    } catch (e) {
      debugLogger(
        DebugType.Error,
        'This WMS service has no getmap formats listed: using image/png',
      );
    }

    if (this.name === undefined || this.name.length < 1) {
      this.title = WMEmptyLayerTitle;
      this.abstract = I18n.not_available_message.text;
      layerDoneCallback(this);
      return;
    }

    this.getmapURL = undefined!;
    try {
      this.getmapURL =
        capabilityObject.Request.GetMap.DCPType.HTTP.Get.OnlineResource.attr[
          'xlink:href'
        ];
    } catch (e) {
      /* Do nothing */
    }
    if (!isDefined(this.getmapURL)) {
      this.getmapURL = this.service;
      debugLogger(
        DebugType.Error,
        'GetMap OnlineResource is not specified. Using default.',
      );
    }

    this.getfeatureinfoURL = undefined!;
    try {
      this.getfeatureinfoURL =
        capabilityObject.Request.GetFeatureInfo.DCPType.HTTP.Get.OnlineResource.attr[
          'xlink:href'
        ];
    } catch (e) {
      /* Do nothing */
    }
    if (!isDefined(this.getfeatureinfoURL)) {
      this.getfeatureinfoURL = this.service;
      debugLogger(
        DebugType.Error,
        'GetFeatureInfo OnlineResource is not specified. Using default.',
      );
    }

    this.getlegendgraphicURL = undefined!;
    try {
      this.getlegendgraphicURL =
        capabilityObject.Request.GetLegendGraphic.DCPType.HTTP.Get.OnlineResource.attr[
          'xlink:href'
        ];
    } catch (e) {
      /* Do nothing */
    }

    if (!isDefined(this.getlegendgraphicURL)) {
      this.getlegendgraphicURL = this.service;
    }

    // TODO Should be arranged also for the other services:
    this.getmapURL = WMJScheckURL(this.getmapURL);
    this.getfeatureinfoURL = WMJScheckURL(this.getfeatureinfoURL);
    this.getlegendgraphicURL = WMJScheckURL(this.getlegendgraphicURL);

    this.styles = [];
    this.projectionProperties = [];
    /* Set default to layer name, try to find details in next steps */
    this.title = this.name;
    this.abstract = '';
    this.path = '';

    // Function will be called when the layer with the right name is found in the getcap doc
    const populateLayerFromTreeElement = (jsonlayer: LayerTree): void => {
      this.title = jsonlayer.title || jsonlayer.name || '';
      this.abstract = jsonlayer.abstract || I18n.not_available_message.text;

      this.path = jsonlayer.path.join('/');

      /** ***************** Go through styles **************** */
      configureStyles(jsonlayer, this);

      /** ***************** Go through Dimensions **************** */
      configureDimensions(jsonlayer, this);

      /** ***************** Go through geographicBoundingBox **************** */
      configureGeographicBoundingBox(jsonlayer, this);

      this.queryable = jsonlayer.queryable || false;

      if (jsonlayer.crs) {
        jsonlayer.crs.forEach((p) => {
          const wmProjection = new WMProjection();
          wmProjection.srs = p.name || '';
          if (p.bbox) {
            const swapBBOX =
              this.version === WMSVersion.version130 &&
              wmProjection.srs === 'EPSG:4326' &&
              this.wms130bboxcompatibilitymode === false;

            if (swapBBOX) {
              wmProjection.bbox.setBBOX(
                p.bbox.bottom,
                p.bbox.left,
                p.bbox.top,
                p.bbox.right,
              );
            } else {
              wmProjection.bbox.setBBOX(
                p.bbox.left,
                p.bbox.bottom,
                p.bbox.right,
                p.bbox.top,
              );
            }
          }

          this.projectionProperties.push(wmProjection);
        });
      }
    };

    const layerTreeStructure = buildLayerTreeFromNestedWMSLayer(rootLayer);

    /* Recursively walk the layertree until a child is found with the corresponding name */
    const findTreeNode = (tree: LayerTree): LayerTree | undefined => {
      if (tree.name === this.name) {
        return tree;
      }
      if (tree.children) {
        let foundChild: LayerTree | undefined;
        tree.children.find((node) => {
          foundChild = findTreeNode(node);
          return foundChild;
        });
        return foundChild;
      }
      return undefined;
    };

    const foundLayer = findTreeNode(layerTreeStructure);

    if (!foundLayer) {
      /* Layer was not found... */
      let message = '';
      if (this.name) {
        message = `Unable to find layer '${this.name}' in service '${this.service}'`;
      } else {
        message = `Unable to find layer '${this.title}' in service '${this.service}'`;
      }
      this.title = '--- layer not found in service ---';
      this.abstract = I18n.not_available_message.text;
      fail(this, message);
      return;
    }

    /* Layer was found */
    populateLayerFromTreeElement(foundLayer);

    if (this.onReady) {
      this.onReady(this);
    }
    this.isConfigured = true;
    layerDoneCallback(this);
  }

  /**
   * Calls success with a configured layer object
   * Calls options.failure with error message.
   * Throws string exceptions when someting goes wrong
   */
  parseLayer(
    _layerDoneCallback: (layer: WMLayer) => void,
    forceReload: boolean,
    // eslint-disable-next-line no-unused-vars
    origin: string,
  ): void {
    this.hasError = false;
    const layerDoneCallback = (__layer: WMLayer): void => {
      if (isDefined(_layerDoneCallback)) {
        try {
          _layerDoneCallback(__layer);
        } catch (e) {
          debugLogger(DebugType.Error, (e as Error).message);
        }
      }
    };
    const fail = (_layer: WMLayer, message: string): void => {
      this.hasError = true;
      this.lastError = message;
      this.title = I18n.service_has_error.text;
      layerDoneCallback(_layer);
      if (isDefined(this._options.failure)) {
        this._options.failure(_layer, message);
      }
    };

    const callback = (data: GetCapabilitiesJson): void => {
      this.__parseGetCapForLayer(data, layerDoneCallback, fail);
    };

    const requestfail = (): void => {
      fail(this, I18n.no_capability_element_found.text);
    };

    const options = { headers: {} };
    if (this.headers && this.headers.length > 0) {
      options.headers = this.headers;
    }

    const wmjsService = WMGetServiceFromStore(this.service);
    if (wmjsService.service !== undefined) {
      wmjsService.getCapabilities(
        (data) => {
          callback(data);
        },
        requestfail,
        forceReload,
        options,
      );
    }
  }

  /**
   * A Promise to parse the layer, it will fetch the WMS GetCapabilities document, configure the layer and resolve to a WMLayer object
   * @param forceReload Do not use the cache of the WMS GetCapabilties document, but instead fetch new data from the server
   * @returns WMLayer object
   */
  parseLayerPromise(forceReload = false): Promise<WMLayer> {
    return new Promise((resolve, reject) => {
      this.parseLayer(
        (layer: WMLayer): void => {
          if (layer.hasError === true) {
            debugLogger(
              DebugType.Error,
              `[ERROR] Parse layer failed, Reason [${layer.lastError}]`,
            );
            reject(layer);
          } else {
            resolve(layer);
          }
        },
        forceReload,
        'WMLayer parseLayerPromise',
      );
    });
  }

  cloneLayer(): WMLayer {
    return new WMLayer(this._options);
  }

  /**
   * Set or change the name of this layer. This involves re-parsing the WMS GetCapabilities document and updating the affected layer properties. When done, it resolves the WMLayer
   * @param name The name of the layer
   * @returns Promise resolving to a WMLayer object
   */
  setName(name: string): Promise<WMLayer> {
    this.name = name;
    return this.parseLayerPromise(false);
  }

  /**
   * Sets the style by its name
   * @param style: The name of the style (not the object)
   */
  setStyle(styleName: string): void {
    debugLogger(DebugType.Log, `WMLayer::setStyle: ${styleName}`);

    if (!this.styles || this.styles.length === 0) {
      this.currentStyle = '';
      this.legendGraphic = '';
      debugLogger(DebugType.Log, 'Layer has no styles.');
      return;
    }

    let styleFound = false;

    for (const style of this.styles) {
      if (style.name === styleName) {
        this.legendGraphic = style.legendURL;
        this.currentStyle = style.name;
        styleFound = true;
      }
    }
    if (!styleFound) {
      debugLogger(
        DebugType.Log,
        `WMLayer::setStyle: Style ${styleName} not found, setting style ${this.styles[0].name}`,
      );
      this.currentStyle = this.styles[0].name;
      this.legendGraphic = this.styles[0].legendURL;
    }

    /* Check if this legenURL has already a Layer Property set. If so set the Layer to the name of this layer */
    this.legendGraphic = getUriWithParam(this.legendGraphic!);
  }

  getStyles(): Style[] {
    if (this.styles) {
      return this.styles;
    }
    return [];
  }

  /**
   * Get the styleobject by name
   * @param styleName The name of the style
   * @param nextPrev, can be -1 or +1 to get the next or previous style object in circular manner.
   */
  getStyleObject(styleName: string, nextPrev: number): Style {
    if (isDefined(this.styles) === false) {
      return undefined!;
    }
    for (let j = 0; j < this.styles.length; j += 1) {
      if (this.styles[j].name === styleName) {
        if (nextPrev === -1) {
          j -= 1;
        }
        if (nextPrev === 1) {
          j += 1;
        }
        if (j < 0) {
          j = this.styles.length - 1;
        }
        if (j > this.styles.length - 1) {
          j = 0;
        }
        return this.styles[j];
      }
    }
    return undefined!;
  }

  /*
   *Get the current stylename as used in the getmap request
   */
  getStyle(): string {
    return this.currentStyle!;
  }

  getDimension(name: string): WMJSDimension | undefined {
    return this.dimensions.find((dim) => dim.name === name);
  }

  /**
   * Returns the Coordinate Reference Systems for this layer
   * @returns List of WMProjection objects
   */
  getCRS(): WMProjection[] {
    const crs: WMProjection[] = [];
    for (const projectionProperty of this.projectionProperties) {
      if (projectionProperty) {
        const { bbox } = projectionProperty;
        if (bbox) {
          crs.push({
            srs: `${projectionProperty.srs}`,
            bbox: new WMBBOX(bbox.left, bbox.bottom, bbox.right, bbox.top),
          });
        }
      }
    }
    return crs;
  }

  /**
   * Get Coordinate reference system by name
   *
   * @param srsName Name of the coordinate reference system to get
   * @returns A WMProjection object if found, otherwise undefined.
   */
  getCRSByName(srsName: string): WMProjection {
    for (const projProps of this.projectionProperties) {
      if (projProps.srs === srsName) {
        const returnSRS = {
          srs: '',
          bbox: new WMBBOX(undefined, undefined, undefined, undefined),
        };
        returnSRS.srs = `${projProps.srs}`;
        returnSRS.bbox = new WMBBOX(
          projProps.bbox.left,
          projProps.bbox.bottom,
          projProps.bbox.right,
          projProps.bbox.top,
        );
        return returnSRS;
      }
    }
    return undefined!;
  }

  /**
   * Get Coordinate reference system by name. Note 2021-05-17: It is more inline with other function names to use the getCRSByName function instead.
   *
   * @param srsName Name of the coordinate reference system to get
   * @returns A WMProjection object if found, otherwise undefined.
   */
  getProjection(srsName: string): WMProjection {
    return this.getCRSByName(srsName);
  }

  setSLDURL(url: string): void {
    this.sldURL = url;
  }

  display(displayornot: boolean): void {
    this.enabled = displayornot;
    if (this.parentMap) {
      this.parentMap.displayLayer(this, this.enabled);
    }
  }
}
