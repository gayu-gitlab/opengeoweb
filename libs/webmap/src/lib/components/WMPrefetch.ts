/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { range } from 'lodash';
import { dateUtils } from '@opengeoweb/shared';
import { getMapImageStore } from './WMImageStore';
import type WMJSMap from './WMJSMap';
import type { AnimationStep } from './types';

const MAX_IMAGES_TO_PREFETCH_DEFAULT = 8;
export function prefetchImagesForAnimation(
  wmMap: WMJSMap,
  maxImagesToPrefetch: number = MAX_IMAGES_TO_PREFETCH_DEFAULT,
): void {
  const animationSteps = wmMap.animationList as AnimationStep[];

  const countAnimationSteps = animationSteps.length;

  const animationStepsInOrder = range(0, countAnimationSteps).map((offset) => {
    // start prefecthing images for the next animation step first
    // when reaching end of animation, start from the beginning
    const indexOfNextAnimationStep =
      (wmMap.currentAnimationStep + offset) % countAnimationSteps;
    return animationSteps[indexOfNextAnimationStep];
  });

  prefetchLayerImagesInOrderWithUrls(
    animationStepsInOrder,
    maxImagesToPrefetch,
  );
}

function prefetchLayerImagesInOrderWithUrls(
  animationStepsInOrder: AnimationStep[],
  maxImagesToPrefetch: number,
): void {
  for (const animationStep of animationStepsInOrder) {
    // dont prefetch if already prefetching too many images
    const countImagesLoading = getMapImageStore.getNumImagesLoading();
    if (countImagesLoading > maxImagesToPrefetch - 1) {
      break;
    }

    prefetchLayerImagesForTime(animationStep.requests || []);
  }
}

function prefetchLayerImagesInOrder(
  wmMap: WMJSMap,
  timesToPrefetchInOrder: string[],
  maxImagesToPrefetch: number,
): void {
  for (const timeToPrefetch of timesToPrefetchInOrder) {
    // dont prefetch if already prefetching too many images
    const countImagesLoading = getMapImageStore.getNumImagesLoading();
    if (countImagesLoading > maxImagesToPrefetch - 1) {
      break;
    }

    const layersImageUrls = getLayersImageUrlsForTime(wmMap, timeToPrefetch);

    prefetchLayerImagesForTime(layersImageUrls);
  }
}

interface PrefetchRequest {
  url: string;
  headers: Headers[];
}

export function getLayersImageUrlsForTime(
  wmMap: WMJSMap,
  timeToPrefetch: string,
): PrefetchRequest[] {
  const layersImageUrls = wmMap.getWMSRequests([
    {
      name: 'time',
      currentValue: timeToPrefetch,
    },
  ]);

  return layersImageUrls;
}

function prefetchLayerImagesForTime(requests: PrefetchRequest[]): void {
  requests.forEach((request) => {
    const image = getMapImageStore.getImage(request.url, {
      headers: request.headers,
    });
    if (image.hasNotStartedLoading()) {
      image.load();
    }
  });
}

export function prefetchImagesForNonAnimation(
  wmMap: WMJSMap,
  maxImagesToPrefetch: number = MAX_IMAGES_TO_PREFETCH_DEFAULT,
): void {
  const { timestepInMinutes } = wmMap;
  const timeDimension = wmMap.getDimension('time');
  if (!timeDimension || !timestepInMinutes) {
    return;
  }

  const { currentValue } = timeDimension;
  const currentTimeIsoString = timeDimension.getClosestValue(currentValue);

  const currentTime = dateUtils.parseISO(currentTimeIsoString);
  if (!dateUtils.isValid(currentTime)) {
    return;
  }

  const maxTimesToPrefetchIfOneLayer = maxImagesToPrefetch / 2;

  const timesToPrefetchInOrder: string[] = range(
    0,
    maxTimesToPrefetchIfOneLayer,
  ).flatMap((offset): string[] => {
    const duration = { minutes: timestepInMinutes * offset };
    const nextTime = dateUtils.add(currentTime, duration).toISOString();
    const previousTime = dateUtils.sub(currentTime, duration).toISOString();
    return [nextTime, previousTime];
  });

  prefetchLayerImagesInOrder(
    wmMap,
    timesToPrefetchInOrder,
    maxImagesToPrefetch,
  );
}
