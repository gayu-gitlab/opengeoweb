/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2020 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2020 - Finnish Meteorological Institute (FMI)
 * */

import { WMBBOX } from '.';
import WMImageStore from './WMImageStore';
import WMTileRenderer from './WMTileRenderer';

describe('components/WMTileRenderer', () => {
  it('should render succesfully', () => {
    const result = new WMTileRenderer();
    const srs = 'EPSG3857';
    const layerName = 'testlayer';
    const tileOptions = {
      [layerName]: {
        [srs]: {
          copyRight: 'dummytext',
          home: 'dummyurl',
          maxLevel: 9,
          minLevel: 0,
          tileServerType: 'osm',
        },
      },
    };
    expect(
      result.render(
        new WMBBOX(),
        new WMBBOX(),
        srs,
        100,
        100,
        new CanvasRenderingContext2D(),
        new WMImageStore(300, {
          id: 'test-WMTileRenderer',
        }),
        tileOptions,
        layerName,
      ),
    ).toEqual({ attributionText: tileOptions[layerName][srs].copyRight });
  });
});
