/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { uiActions } from '@opengeoweb/store';
import { call, put, takeLatest } from 'redux-saga/effects';
import { snackbarActions } from '@opengeoweb/snackbar';
import drawingSaga, { getSnackbarMessage, submitDrawValuesSaga } from './sagas';
import { getWarningsApi } from '../../utils/api';
import { drawActions } from './reducer';
import { DrawingScope, SubmitDrawValuesPayload } from './types';
import { drawingDialogType } from './utils';

describe('store/drawings/sagas', () => {
  describe('drawingSaga', () => {
    it('should have rootSaga', () => {
      const generator = drawingSaga();

      expect(generator.next().value).toEqual(
        takeLatest(drawActions.submitDrawValues, submitDrawValuesSaga),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('submitDrawValuesSaga', () => {
    const payload: SubmitDrawValuesPayload = {
      objectName: 'Test Object',
      scope: 'user' as DrawingScope,
      geoJSON: {
        type: 'FeatureCollection',
        features: [
          {
            type: 'Feature',
            geometry: {
              type: 'Polygon',
              coordinates: [
                [
                  [5.0, 55.0],
                  [4.331914, 55.332644],
                  [3.368817, 55.764314],
                  [2.761908, 54.379261],
                  [3.15576, 52.913554],
                  [2.000002, 51.500002],
                  [3.370001, 51.369722],
                  [3.370527, 51.36867],
                  [3.362223, 51.320002],
                  [3.36389, 51.313608],
                  [3.373613, 51.309999],
                  [3.952501, 51.214441],
                  [4.397501, 51.452776],
                  [5.078611, 51.391665],
                  [5.848333, 51.139444],
                  [5.651667, 50.824717],
                  [6.011797, 50.757273],
                  [5.934168, 51.036386],
                  [6.222223, 51.361666],
                  [5.94639, 51.811663],
                  [6.405001, 51.830828],
                  [7.053095, 52.237764],
                  [7.031389, 52.268885],
                  [7.063612, 52.346109],
                  [7.065557, 52.385828],
                  [7.133055, 52.888887],
                  [7.14218, 52.898244],
                  [7.191667, 53.3],
                  [6.5, 53.666667],
                  [6.500002, 55.000002],
                  [5.0, 55.0],
                ],
              ],
            },
            properties: {
              selectionType: 'fir',
            },
          },
        ],
      } as GeoJSON.FeatureCollection,
    };

    it('should handle save a new drawing', () => {
      const iterator = submitDrawValuesSaga({ payload });
      const mockedWarningsApi = {
        saveDrawingAs: jest.fn(),
      };

      expect(iterator.next().value).toEqual(
        put(
          uiActions.toggleIsLoadingDialog({
            isLoading: true,
            type: drawingDialogType,
          }),
        ),
      );

      expect(iterator.next().value).toEqual(call(getWarningsApi));

      expect(iterator.next(mockedWarningsApi).value).toEqual(
        call(mockedWarningsApi.saveDrawingAs, {
          objectName: payload.objectName,
          geoJSON: payload.geoJSON,
          scope: 'user',
        }),
      );
      const fakeNewID = 'new-drawing-id';
      expect(iterator.next(fakeNewID).value).toEqual(
        put(
          drawActions.setDrawValues({
            objectName: payload.objectName,
            id: fakeNewID,
            drawingInstanceId: drawingDialogType,
          }),
        ),
      );

      expect(iterator.next().value).toEqual(
        put(
          uiActions.setErrorDialog({
            type: drawingDialogType,
            error: '',
          }),
        ),
      );

      expect(iterator.next().value).toEqual(
        put(
          snackbarActions.openSnackbar({
            message: getSnackbarMessage(payload.objectName, ''),
          }),
        ),
      );

      expect(iterator.next().value).toEqual(
        put(
          uiActions.toggleIsLoadingDialog({
            isLoading: false,
            type: drawingDialogType,
          }),
        ),
      );

      expect(iterator.next().done).toBeTruthy();
    });

    it('should handle save an existing drawing', () => {
      const actionPayload = { ...payload, id: 'test-drawing-id' };
      const iterator = submitDrawValuesSaga({ payload: actionPayload });
      const mockedWarningsApi = {
        updateDrawing: jest.fn(),
      };

      expect(iterator.next().value).toEqual(
        put(
          uiActions.toggleIsLoadingDialog({
            isLoading: true,
            type: drawingDialogType,
          }),
        ),
      );

      expect(iterator.next().value).toEqual(call(getWarningsApi));

      expect(iterator.next(mockedWarningsApi).value).toEqual(
        call(mockedWarningsApi.updateDrawing, actionPayload.id, {
          objectName: payload.objectName,
          geoJSON: payload.geoJSON,
          scope: 'user',
        }),
      );

      expect(iterator.next().value).toEqual(
        put(
          uiActions.setErrorDialog({
            type: drawingDialogType,
            error: '',
          }),
        ),
      );

      expect(iterator.next().value).toEqual(
        put(
          snackbarActions.openSnackbar({
            message: getSnackbarMessage(payload.objectName, actionPayload.id),
          }),
        ),
      );

      expect(iterator.next().value).toEqual(
        put(
          uiActions.toggleIsLoadingDialog({
            isLoading: false,
            type: drawingDialogType,
          }),
        ),
      );

      expect(iterator.next().done).toBeTruthy();
    });

    it('should handle error', () => {
      const iterator = submitDrawValuesSaga({ payload });

      expect(iterator.next().value).toEqual(
        put(
          uiActions.toggleIsLoadingDialog({
            isLoading: true,
            type: drawingDialogType,
          }),
        ),
      );

      expect(iterator.next().value).toEqual(call(getWarningsApi));

      const errorMessage = 'Network error';
      const error = new Error(errorMessage);
      expect(iterator.throw!(error).value).toEqual(
        put(
          uiActions.toggleIsLoadingDialog({
            isLoading: false,
            type: drawingDialogType,
          }),
        ),
      );

      expect(iterator.next().value).toEqual(
        put(
          uiActions.setErrorDialog({
            type: drawingDialogType,
            error: errorMessage,
          }),
        ),
      );

      expect(iterator.next().done).toBeTruthy();
    });
  });
});
