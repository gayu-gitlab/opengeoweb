/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import { call, put, select, takeLatest } from 'redux-saga/effects';

import { snackbarActions } from '@opengeoweb/snackbar';
import { uiActions, uiSelectors } from '@opengeoweb/store';
import publicWarningFormSaga, {
  PUBLISH_SUCCES,
  SAVE_SUCCES,
  errorSaga,
  getToggleEditorSuccessMessage,
  openDialogSaga,
  publishWarningSaga,
  saveOrUpdateWarning,
  saveWarningSaga,
  successSaga,
  toggleDialogLoadingSaga,
  toggleEditorWarningSaga,
} from './sagas';
import { publicWarningFormActions } from './reducer';
import { testGeoJSON } from '../../storybookUtils/testUtils';
import { PublicWarning, PublicWarningDetail } from './types';
import { getWarningsApi } from '../../utils/api';
import { publicWarningDialogType } from './utils';
import { MOCK_USERNAME } from '../../utils/fakeApi';
import { publicWarningActions } from '../publicWarnings/reducer';

const publicWarningDetail: PublicWarningDetail = {
  id: '923723984872338768743',
  phenomenon: 'coastalEvent',
  validFrom: '2022-06-01T15:00:00Z',
  validUntil: '2022-06-01T18:00:00Z',
  level: 'extreme',
  probability: 80,
  descriptionOriginal: 'Some pretty intense coastal weather is coming our way',
  descriptionTranslation: 'And this would be the translation',
  geoJSON: testGeoJSON,
};

describe('store/publicWarningForm/sagas', () => {
  describe('saveOrUpdateWarning', () => {
    it('should save an existing warning', () => {
      const testPublicWarning: PublicWarning = {
        warningDetail: publicWarningDetail,
        status: 'PUBLISHED',
      };

      const generator = saveOrUpdateWarning(testPublicWarning);

      const mockedWarningsApi = {
        updateWarning: jest.fn(),
      };

      expect(generator.next().value).toEqual(call(getWarningsApi));

      expect(generator.next(mockedWarningsApi).value).toEqual(
        call(
          mockedWarningsApi.updateWarning,
          testPublicWarning.warningDetail.id,
          testPublicWarning,
        ),
      );
      expect(generator.next().value).toEqual(testPublicWarning);
      expect(generator.next().done).toBeTruthy();
    });

    it('should save a new warning', () => {
      const { id, ...warningDetail } = publicWarningDetail;
      const testPublicWarning: PublicWarning = {
        warningDetail: warningDetail as PublicWarningDetail,
        status: 'PUBLISHED',
      };

      const generator = saveOrUpdateWarning(testPublicWarning);

      const mockedWarningsApi = {
        saveWarningAs: jest.fn(),
      };

      expect(generator.next().value).toEqual(call(getWarningsApi));

      expect(generator.next(mockedWarningsApi).value).toEqual(
        call(mockedWarningsApi.saveWarningAs, testPublicWarning),
      );
      expect(generator.next().value).toEqual(testPublicWarning);
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('publicWarningFormSaga', () => {
    it('should have rootsaga', () => {
      const generator = publicWarningFormSaga();

      expect(generator.next().value).toEqual(
        takeLatest(
          publicWarningFormActions.toggleEditorWarning,
          toggleEditorWarningSaga,
        ),
      );
      expect(generator.next().value).toEqual(
        takeLatest(publicWarningFormActions.publishWarning, publishWarningSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(publicWarningFormActions.saveWarning, saveWarningSaga),
      );

      expect(generator.next().value).toEqual(
        takeLatest(
          [
            publicWarningFormActions.publishWarningSuccess,
            publicWarningFormActions.saveWarningSuccess,
            publicWarningFormActions.toggleEditorWarningSuccess,
          ],
          successSaga,
        ),
      );

      expect(generator.next().value).toEqual(
        takeLatest(
          [
            publicWarningFormActions.publishWarning,
            publicWarningFormActions.saveWarning,
          ],
          toggleDialogLoadingSaga,
          true,
        ),
      );

      expect(generator.next().value).toEqual(
        takeLatest(
          publicWarningFormActions.openPublicWarningFormDialog,
          openDialogSaga,
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });
  describe('publishWarningSaga', () => {
    it('should publish a warning', () => {
      const testPublicWarning: PublicWarning = {
        warningDetail: publicWarningDetail,
        status: 'PUBLISHED',
      };

      const generator = publishWarningSaga(
        publicWarningFormActions.publishWarning({
          publicWarning: testPublicWarning,
        }),
      );
      expect(generator.next().value).toEqual(
        call(saveOrUpdateWarning, testPublicWarning),
      );

      expect(generator.next(testPublicWarning).value).toEqual(
        put(
          publicWarningFormActions.publishWarningSuccess({
            publicWarning: testPublicWarning,
            message: PUBLISH_SUCCES,
          }),
        ),
      );

      expect(generator.next().value).toEqual(
        put(
          uiActions.setToggleOpenDialog({
            type: publicWarningDialogType,
            setOpen: false,
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should trigger error', () => {
      const testPublicWarning: PublicWarning = {
        warningDetail: publicWarningDetail,
        status: 'DRAFT',
      };

      const generator = publishWarningSaga(
        publicWarningFormActions.publishWarning({
          publicWarning: testPublicWarning,
        }),
      );
      expect(generator.next().value).toEqual(
        call(saveOrUpdateWarning, testPublicWarning),
      );
      const errorMessage = 'Network error';
      const error = new Error(errorMessage);

      expect(generator.throw!(error).value).toEqual(
        call(errorSaga, errorMessage),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });
  describe('saveWarningSaga', () => {
    it('should save a new warning', () => {
      const newPublicWarning = { ...publicWarningDetail, id: '' };

      const testPublicWarning: PublicWarning = {
        warningDetail: newPublicWarning,
        status: 'DRAFT',
      };

      const generator = saveWarningSaga(
        publicWarningFormActions.saveWarning({
          publicWarning: testPublicWarning,
        }),
      );
      expect(generator.next().value).toEqual(
        call(saveOrUpdateWarning, testPublicWarning),
      );

      const warningWithNewId = {
        ...testPublicWarning,
        warningDetail: {
          ...testPublicWarning.warningDetail,
          id: 'new id',
        },
      };

      expect(generator.next(warningWithNewId).value).toEqual(
        put(
          publicWarningFormActions.saveWarningSuccess({
            publicWarning: warningWithNewId,
            message: SAVE_SUCCES,
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });
    it('should save an existing warning', () => {
      const testPublicWarning: PublicWarning = {
        warningDetail: publicWarningDetail,
        status: 'DRAFT',
      };

      const generator = saveWarningSaga(
        publicWarningFormActions.saveWarning({
          publicWarning: testPublicWarning,
        }),
      );
      expect(generator.next().value).toEqual(
        call(saveOrUpdateWarning, testPublicWarning),
      );

      expect(generator.next(testPublicWarning).value).toEqual(
        put(
          publicWarningFormActions.saveWarningSuccess({
            publicWarning: testPublicWarning,
            message: SAVE_SUCCES,
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should trigger error', () => {
      const testPublicWarning: PublicWarning = {
        warningDetail: publicWarningDetail,
        status: 'DRAFT',
      };

      const generator = saveWarningSaga(
        publicWarningFormActions.saveWarning({
          publicWarning: testPublicWarning,
        }),
      );
      expect(generator.next().value).toEqual(
        call(saveOrUpdateWarning, testPublicWarning),
      );

      const errorMessage = 'Network error';
      const error = new Error(errorMessage);

      expect(generator.throw!(error).value).toEqual(
        call(errorSaga, errorMessage),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('toggleEditorWarningSaga', () => {
    it('should call BE to set editor if isEditor is true', () => {
      const generator = toggleEditorWarningSaga(
        publicWarningFormActions.toggleEditorWarning({
          warningId: 'warning1',
          isEditor: true,
          username: MOCK_USERNAME,
        }),
      );
      const mockedWarningsApi = {
        toggleEditorWarning: jest.fn(),
      };

      expect(generator.next().value).toEqual(call(getWarningsApi));

      expect(generator.next(mockedWarningsApi).value).toEqual(
        call(mockedWarningsApi.toggleEditorWarning, 'warning1', true),
      );

      expect(generator.next().value).toEqual(
        put(publicWarningActions.fetchWarnings()),
      );

      expect(generator.next().value).toEqual(
        put(
          publicWarningFormActions.toggleEditorWarningSuccess({
            isEditor: true,
            username: MOCK_USERNAME,
            message: getToggleEditorSuccessMessage(true),
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should call BE to unset editor if isEditor is false', () => {
      const generator = toggleEditorWarningSaga(
        publicWarningFormActions.toggleEditorWarning({
          warningId: 'warning1',
          isEditor: false,
          username: MOCK_USERNAME,
        }),
      );
      const mockedWarningsApi = {
        toggleEditorWarning: jest.fn(),
      };

      expect(generator.next().value).toEqual(call(getWarningsApi));

      expect(generator.next(mockedWarningsApi).value).toEqual(
        call(mockedWarningsApi.toggleEditorWarning, 'warning1', false),
      );

      expect(generator.next().value).toEqual(
        put(publicWarningActions.fetchWarnings()),
      );

      expect(generator.next().value).toEqual(
        put(
          publicWarningFormActions.toggleEditorWarningSuccess({
            isEditor: false,
            username: MOCK_USERNAME,
            message: getToggleEditorSuccessMessage(false),
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should trigger error', () => {
      const generator = toggleEditorWarningSaga(
        publicWarningFormActions.toggleEditorWarning({
          warningId: 'warning1',
          isEditor: false,
          username: MOCK_USERNAME,
        }),
      );
      const mockedWarningsApi = {
        toggleEditorWarning: jest.fn(),
      };

      expect(generator.next().value).toEqual(call(getWarningsApi));

      expect(generator.next(mockedWarningsApi).value).toEqual(
        call(mockedWarningsApi.toggleEditorWarning, 'warning1', false),
      );
      const errorMessage = 'Network error';
      const error = new Error(errorMessage);

      expect(generator.throw!(error).value).toEqual(
        call(errorSaga, errorMessage),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('successSaga', () => {
    it('should show a snackar on success of publish warning', () => {
      const payload = {
        publicWarning: {
          warningDetail: publicWarningDetail,
          status: 'DRAFT',
        } as PublicWarning,
        message: 'great success',
      };
      const generator = successSaga(
        publicWarningFormActions.publishWarningSuccess(payload),
      );

      expect(generator.next().value).toEqual(
        put(snackbarActions.openSnackbar({ message: payload.message })),
      );

      expect(generator.next().value).toEqual(
        call(toggleDialogLoadingSaga, false),
      );

      expect(generator.next().done).toBeTruthy();
    });

    it('should show a snackar on success of save warning', () => {
      const payload = {
        publicWarning: {
          warningDetail: publicWarningDetail,
          status: 'DRAFT',
        } as PublicWarning,
        message: 'great success',
      };
      const generator = successSaga(
        publicWarningFormActions.saveWarningSuccess(payload),
      );

      expect(generator.next().value).toEqual(
        put(snackbarActions.openSnackbar({ message: payload.message })),
      );

      expect(generator.next().value).toEqual(
        call(toggleDialogLoadingSaga, false),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('errorSaga', () => {
    it('should set error and toggle dialog saga', () => {
      const message = 'something went wrong';
      const generator = errorSaga(message);

      expect(generator.next().value).toEqual(
        put(publicWarningFormActions.setFormError({ message })),
      );

      expect(generator.next().value).toEqual(
        call(toggleDialogLoadingSaga, false),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('toggleDialogSaga', () => {
    it('should toggle loading for publicWarningDialogType if dialog exists', () => {
      const isLoading = true;
      const isDialogOpen = true;
      const generator = toggleDialogLoadingSaga(isLoading);

      expect(generator.next().value).toEqual(
        select(uiSelectors.getDialogDetailsByType, publicWarningDialogType),
      );
      expect(generator.next(isDialogOpen).value).toEqual(
        put(
          uiActions.toggleIsLoadingDialog({
            isLoading,
            type: publicWarningDialogType,
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
    it('should toggle loading false for publicWarningDialogType if dialog exists', () => {
      const isLoading = false;
      const isDialogOpen = true;
      const generator = toggleDialogLoadingSaga(isLoading);

      expect(generator.next().value).toEqual(
        select(uiSelectors.getDialogDetailsByType, publicWarningDialogType),
      );
      expect(generator.next(isDialogOpen).value).toEqual(
        put(
          uiActions.toggleIsLoadingDialog({
            isLoading,
            type: publicWarningDialogType,
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should do nothing if dialog does not exist', () => {
      const isLoading = true;
      const isDialogOpen = false;
      const generator = toggleDialogLoadingSaga(isLoading);

      expect(generator.next().value).toEqual(
        select(uiSelectors.getDialogDetailsByType, publicWarningDialogType),
      );

      expect(generator.next(isDialogOpen).done).toBeTruthy();
    });
  });

  describe('openDialogSaga', () => {
    it('should open ui dialog when opening publicwarningformdialog', () => {
      const generator = openDialogSaga();

      expect(generator.next().value).toEqual(
        put(
          uiActions.setToggleOpenDialog({
            setOpen: true,
            type: publicWarningDialogType,
          }),
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });
});
