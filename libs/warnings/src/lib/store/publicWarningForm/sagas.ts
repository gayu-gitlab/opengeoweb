/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import { call, put, select, takeLatest } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import { snackbarActions } from '@opengeoweb/snackbar';
import { uiActions, uiSelectors } from '@opengeoweb/store';
import { AxiosError, isAxiosError } from 'axios';
import { getAxiosErrorMessage } from '@opengeoweb/shared';
import { publicWarningFormActions } from './reducer';
import { WarningsApi, getWarningsApi } from '../../utils/api';
import { publicWarningDialogType } from './utils';
import { PublicWarning } from './types';
import { publicWarningActions } from '../publicWarnings/reducer';

export const PUBLISH_SUCCES = 'Public warning has succesfully been published!';
export const SAVE_SUCCES = 'Public warning has succesfully been saved!';

const getErrorMessage = (error: AxiosError | Error): string =>
  isAxiosError(error) ? getAxiosErrorMessage(error) : error.message;

export function* saveOrUpdateWarning(
  publicWarning: PublicWarning,
): SagaIterator<PublicWarning> {
  const warningsApi: WarningsApi = yield call(getWarningsApi);
  if (publicWarning.warningDetail.id) {
    yield call(
      warningsApi.updateWarning,
      publicWarning.warningDetail.id,
      publicWarning,
    );
    return publicWarning;
  }
  const newWarningId = yield call(warningsApi.saveWarningAs, publicWarning);
  return {
    ...publicWarning,
    warningDetail: { ...publicWarning.warningDetail, id: newWarningId },
  };
}

export const getToggleEditorSuccessMessage = (isEditor: boolean): string =>
  `You are now ${isEditor ? '' : 'no longer '}the editor for this warning`;

export function* toggleEditorWarningSaga({
  payload,
}: ReturnType<
  typeof publicWarningFormActions.toggleEditorWarning
>): SagaIterator {
  try {
    const { warningId, isEditor, username } = payload;

    const warningsApi: WarningsApi = yield call(getWarningsApi);
    yield call(warningsApi.toggleEditorWarning, warningId, isEditor);

    // update list to show correct avatar
    yield put(publicWarningActions.fetchWarnings());

    yield put(
      publicWarningFormActions.toggleEditorWarningSuccess({
        isEditor,
        username,
        message: getToggleEditorSuccessMessage(isEditor),
      }),
    );
  } catch (error) {
    yield call(errorSaga, getErrorMessage(error));
  }
}

export function* publishWarningSaga({
  payload,
}: ReturnType<typeof publicWarningFormActions.publishWarning>): SagaIterator {
  try {
    const { publicWarning } = payload;
    const publishedWarning = yield call(saveOrUpdateWarning, publicWarning);

    yield put(
      publicWarningFormActions.publishWarningSuccess({
        publicWarning: publishedWarning,
        message: PUBLISH_SUCCES,
      }),
    );

    yield put(
      uiActions.setToggleOpenDialog({
        type: publicWarningDialogType,
        setOpen: false,
      }),
    );
  } catch (error) {
    yield call(errorSaga, getErrorMessage(error));
  }
}

export function* saveWarningSaga({
  payload,
}: ReturnType<typeof publicWarningFormActions.saveWarning>): SagaIterator {
  try {
    const { publicWarning } = payload;
    const savedWarning = yield call(saveOrUpdateWarning, publicWarning);

    yield put(
      publicWarningFormActions.saveWarningSuccess({
        publicWarning: savedWarning,
        message: SAVE_SUCCES,
      }),
    );
  } catch (error) {
    yield call(errorSaga, getErrorMessage(error));
  }
}

export function* successSaga({
  payload,
}: ReturnType<
  | typeof publicWarningFormActions.publishWarningSuccess
  | typeof publicWarningFormActions.saveWarningSuccess
>): SagaIterator {
  const { message } = payload;
  yield put(
    snackbarActions.openSnackbar({
      message,
    }),
  );

  yield call(toggleDialogLoadingSaga, false);
}

export function* errorSaga(message: string): SagaIterator {
  yield put(publicWarningFormActions.setFormError({ message }));
  yield call(toggleDialogLoadingSaga, false);
}

export function* toggleDialogLoadingSaga(isLoading: boolean): SagaIterator {
  const isDialogOpen = yield select(
    uiSelectors.getDialogDetailsByType,
    publicWarningDialogType,
  );

  if (isDialogOpen) {
    yield put(
      uiActions.toggleIsLoadingDialog({
        isLoading,
        type: publicWarningDialogType,
      }),
    );
  }
}

export function* openDialogSaga(): SagaIterator {
  yield put(
    uiActions.setToggleOpenDialog({
      setOpen: true,
      type: publicWarningDialogType,
    }),
  );
}

function* publicWarningRootSaga(): SagaIterator {
  // requests
  yield takeLatest(
    publicWarningFormActions.toggleEditorWarning,
    toggleEditorWarningSaga,
  );
  yield takeLatest(publicWarningFormActions.publishWarning, publishWarningSaga);
  yield takeLatest(publicWarningFormActions.saveWarning, saveWarningSaga);

  // side effects ui/snackbar
  yield takeLatest(
    [
      publicWarningFormActions.publishWarningSuccess,
      publicWarningFormActions.saveWarningSuccess,
      publicWarningFormActions.toggleEditorWarningSuccess,
    ],
    successSaga,
  );
  yield takeLatest(
    [
      publicWarningFormActions.publishWarning,
      publicWarningFormActions.saveWarning,
    ],
    toggleDialogLoadingSaga,
    true,
  );
  yield takeLatest(
    publicWarningFormActions.openPublicWarningFormDialog,
    openDialogSaga,
  );
}

export default publicWarningRootSaga;
