/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { uiActions, uiTypes } from '@opengeoweb/store';
import {
  PublicWarningFormState,
  TAKEOVER_MESSAGE,
  initialState,
  publicWarningFormActions,
  reducer,
} from './reducer';
import { publicWarningDialogType } from './utils';
import { DrawingListItem } from '../drawings/types';
import { PublicWarning, PublicWarningDetail } from './types';
import { testGeoJSON } from '../../storybookUtils/testUtils';
import { MOCK_USERNAME } from '../../utils/fakeApi';
import { publicWarningActions } from '../publicWarnings/reducer';
import { warningList } from '../../utils/fakeApi/index';

const object: DrawingListItem = {
  id: '923723984872338768743',
  objectName: 'Drawing object 101',
  lastUpdatedTime: '2022-06-01T12:34:27Z',
  scope: 'user',
  geoJSON: testGeoJSON,
};

const publicWarningDetail: PublicWarningDetail = {
  id: '923723984872338768743',
  phenomenon: 'coastalEvent',
  validFrom: '2022-06-01T15:00:00Z',
  validUntil: '2022-06-01T18:00:00Z',
  level: 'extreme',
  probability: 80,
  descriptionOriginal: 'Some pretty intense coastal weather is coming our way',
  descriptionTranslation: 'And this would be the translation',
  geoJSON: testGeoJSON,
};

const publicWarning: PublicWarning = {
  warningDetail: publicWarningDetail,
  status: 'DRAFT',
};

describe('public warning reducer', () => {
  describe('setFormValues', () => {
    const actionPayload = {
      object,
      publicWarning,
    };
    it('should set setFormValues', () => {
      const result = reducer(
        initialState,
        publicWarningFormActions.setFormValues(actionPayload),
      );

      expect(result.object?.id).toEqual(actionPayload.object.id);
      expect(result.object?.objectName).toEqual(
        actionPayload.object.objectName,
      );
      expect(result.object?.lastUpdatedTime).toEqual(
        actionPayload.object.lastUpdatedTime,
      );
      expect(result.publicWarning).toEqual(actionPayload.publicWarning);
      expect(result.formState).toBeUndefined();

      const actionPayLoadWithFormAction = {
        formState: 'readonly' as const,
      };
      const resultWithFormAction = reducer(
        initialState,
        publicWarningFormActions.setFormValues(actionPayLoadWithFormAction),
      );
      expect(resultWithFormAction.formState).toEqual(
        actionPayLoadWithFormAction.formState,
      );

      const actionPayLoadWithFormAction2 = {
        formState: '' as const,
      };
      const resultWithFormAction2 = reducer(
        initialState,
        publicWarningFormActions.setFormValues(actionPayLoadWithFormAction2),
      );
      expect(resultWithFormAction2.formState).toEqual(
        actionPayLoadWithFormAction2.formState,
      );
    });

    it('should reset values when public warning dialog closes', () => {
      const state = {
        ...actionPayload,
        error: { message: 'something went wrong' },
        formState: 'readonly' as const,
        publicWarning: {
          ...actionPayload.publicWarning,
          status: 'DRAFT' as const,
        },
        isFormDirty: true,
      };

      const result = reducer(
        state,
        uiActions.setToggleOpenDialog({
          type: uiTypes.DialogTypes.DrawingTool,
          setOpen: false,
        }),
      );

      expect(result.object?.id).toEqual(actionPayload.object.id);

      expect(result.object?.objectName).toEqual(
        actionPayload.object.objectName,
      );
      expect(result.publicWarning).toEqual(actionPayload.publicWarning);

      expect(result.object?.lastUpdatedTime).toEqual(
        actionPayload.object.lastUpdatedTime,
      );
      expect(result.error).toEqual(state.error);
      expect(result.publicWarning?.status).toEqual(state.publicWarning.status);
      expect(result.formState).toEqual(state.formState);
      expect(result.isFormDirty).toEqual(state.isFormDirty);

      const result2 = reducer(
        state,
        uiActions.setToggleOpenDialog({
          type: publicWarningDialogType,
          setOpen: false,
        }),
      );

      expect(result2.object).toBeUndefined();
      expect(result2.publicWarning).toBeUndefined();
      expect(result2.formState).toBeUndefined();
      expect(result2.error).toBeUndefined();
      expect(result2.formState).toBeUndefined();
      expect(result2.isFormDirty).toBeUndefined();
    });
  });

  describe('openPublicWarningFormDialog', () => {
    const actionPayload = {
      object,
      publicWarning: {
        ...publicWarning,
        status: 'DRAFT' as const,
      },
      formState: 'readonly' as const,
    };
    it('should open dialog and set selectedPublicWarningId', () => {
      const result = reducer(
        initialState,
        publicWarningFormActions.openPublicWarningFormDialog(actionPayload),
      );

      expect(result.object?.id).toEqual(actionPayload.object.id);
      expect(result.object?.objectName).toEqual(
        actionPayload.object.objectName,
      );
      expect(result.object?.lastUpdatedTime).toEqual(
        actionPayload.object.lastUpdatedTime,
      );
      expect(result.publicWarning).toEqual(actionPayload.publicWarning);
      expect(result.formState).toEqual(actionPayload.formState);
      expect(result.selectedPublicWarningId).toEqual(
        actionPayload.publicWarning.id,
      );

      expect(result.publicWarning?.status).toEqual(
        actionPayload.publicWarning.status,
      );

      const closeDialogResult = reducer(
        result,
        uiActions.setToggleOpenDialog({
          type: publicWarningDialogType,
          setOpen: false,
        }),
      );

      expect(closeDialogResult.selectedPublicWarningId).toBeUndefined();
    });

    it('should open dialog and not set selectedPublicWarningId', () => {
      const result = reducer(
        initialState,
        publicWarningFormActions.openPublicWarningFormDialog({}),
      );

      expect(result.object?.id).toBeUndefined();
      expect(result.object?.objectName).toBeUndefined();
      expect(result.publicWarning).toBeUndefined();
      expect(result.formState).toEqual('');
      expect(result.selectedPublicWarningId).toBeUndefined();
    });
  });

  describe('publishWarning', () => {
    it('change formAction to publishing', () => {
      const result = reducer(
        initialState,
        publicWarningFormActions.publishWarning({
          publicWarning: {
            status: 'PUBLISHED',
            warningDetail: publicWarningDetail,
          },
        }),
      );
      expect(result.formState).toEqual('publishing');
    });

    it('reset error on formAction to publishing', () => {
      const testInitialState = {
        ...initialState,
        error: { message: 'test error' },
      };
      const result = reducer(
        testInitialState,
        publicWarningFormActions.publishWarning({
          publicWarning: {
            status: 'PUBLISHED',
            warningDetail: publicWarningDetail,
          },
        }),
      );
      expect(result.formState).toEqual('publishing');
      expect(result.error).toBeUndefined();
    });
  });

  describe('publishWarningSuccess', () => {
    it('should reset formAction to readonly and store warning on publishWarningSuccess', () => {
      const result = reducer(
        initialState,
        publicWarningFormActions.publishWarningSuccess({
          publicWarning: {
            status: 'PUBLISHED',
            warningDetail: publicWarningDetail,
          },
          message: 'great success',
        }),
      );
      expect(result.formState).toEqual('readonly');
      expect(result.publicWarning?.status).toEqual('PUBLISHED');
      expect(result.publicWarning?.warningDetail).toEqual(publicWarningDetail);
    });
  });

  describe('saveWarning', () => {
    it('change formAction to saving', () => {
      const result = reducer(
        initialState,
        publicWarningFormActions.saveWarning({
          publicWarning: {
            status: 'DRAFT',
            warningDetail: publicWarningDetail,
          },
        }),
      );
      expect(result.formState).toEqual('saving');
    });

    it('reset error on formAction to saving', () => {
      const testInitialState = {
        ...initialState,
        error: { message: 'test error' },
      };
      const result = reducer(
        testInitialState,
        publicWarningFormActions.saveWarning({
          publicWarning: {
            status: 'DRAFT',
            warningDetail: publicWarningDetail,
          },
        }),
      );
      expect(result.formState).toEqual('saving');
      expect(result.error).toBeUndefined();
    });
  });

  describe('saveWarningSuccess', () => {
    it('should reset formAction and store warning on publishWarningSuccess', () => {
      const result = reducer(
        initialState,
        publicWarningFormActions.saveWarningSuccess({
          publicWarning: {
            status: 'DRAFT',
            warningDetail: publicWarningDetail,
          },
          message: 'great success',
        }),
      );
      expect(result.formState).toEqual('');
      expect(result.publicWarning?.warningDetail).toEqual(publicWarningDetail);
      expect(result.selectedPublicWarningId).toEqual(publicWarningDetail.id);
    });
  });

  describe('toggleEditorWarning', () => {
    it('reset error on formAction to publishing', () => {
      const testInitialState = {
        ...initialState,
        error: { message: 'test error' },
      };
      const result = reducer(
        testInitialState,
        publicWarningFormActions.toggleEditorWarning({
          warningId: 'warning1',
          isEditor: true,
          username: MOCK_USERNAME,
        }),
      );
      expect(result.error).toBeUndefined();
    });
  });

  describe('toggleEditorWarningSuccess', () => {
    it('should set editor if isEditor is true and formstate to empty', () => {
      const result = reducer(
        { publicWarning: {} } as PublicWarningFormState,
        publicWarningFormActions.toggleEditorWarningSuccess({
          isEditor: true,
          username: MOCK_USERNAME,
          message: 'great success',
        }),
      );
      expect(result.publicWarning?.editor).toBe(MOCK_USERNAME);
      expect(result.formState).toBe('');
    });
    it('should remove editor if isEditor is false and formstate to readonly', () => {
      const result = reducer(
        { publicWarning: {} } as PublicWarningFormState,
        publicWarningFormActions.toggleEditorWarningSuccess({
          isEditor: false,
          username: MOCK_USERNAME,
          message: 'great success',
        }),
      );
      expect(result.publicWarning?.editor).toBe('');
      expect(result.formState).toBe('readonly');
    });
  });

  describe('setFormError', () => {
    it('should set error', () => {
      const error = {
        message: 'something went wrong',
      };
      const result = reducer(
        initialState,
        publicWarningFormActions.setFormError(error),
      );
      expect(result.formState).toEqual('');
      expect(result.error).toEqual({
        message: error.message,
        severity: 'error',
      });
    });

    it('should set error as warning', () => {
      const error = {
        message: 'something went wrong',
        severity: 'warning' as const,
      };
      const result = reducer(
        initialState,
        publicWarningFormActions.setFormError(error),
      );
      expect(result.formState).toEqual('');
      expect(result.error).toEqual(error);
    });

    it('should set error and reset formAction', () => {
      const testInitialState = {
        ...initialState,
        error: { message: 'test error' },
        formState: 'publishing' as const,
      };
      const error = {
        message: 'something went wrong',
      };
      const result = reducer(
        testInitialState,
        publicWarningFormActions.setFormError(error),
      );
      expect(result.formState).toEqual('');
      expect(result.error).toEqual({
        message: error.message,
        severity: 'error',
      });
    });
  });

  describe('setFormDirty', () => {
    it('should set isFormDirty', () => {
      const result = reducer(
        initialState,
        publicWarningFormActions.setFormDirty({
          isFormDirty: true,
        }),
      );
      expect(result.isFormDirty).toBeTruthy();

      const result2 = reducer(
        result,
        publicWarningFormActions.setFormDirty({
          isFormDirty: false,
        }),
      );
      expect(result2.isFormDirty).toBeFalsy();
    });
  });

  describe('publicWarningActions.fetchWarningsSuccess', () => {
    it('should update current editor after a successfull fetch of warnings and show warning', () => {
      const result = reducer(
        {
          ...initialState,
          publicWarning: {
            ...publicWarning,
            editor: 'current.user',
          },
          formState: '',
          selectedPublicWarningId: warningList[5].id,
        },
        publicWarningActions.fetchWarningsSuccess({
          warnings: warningList,
          lastUpdatedTime: '10:13',
        }),
      );
      expect(result.publicWarning?.editor).toEqual(warningList[5].editor);
      expect(result.error).toEqual({
        severity: 'warning',
        message: TAKEOVER_MESSAGE,
      });
    });

    it('should update current editor after a successfull fetch of warnings and not show warning on readonly', () => {
      const result = reducer(
        {
          ...initialState,
          publicWarning: {
            ...publicWarning,
            editor: 'current.user',
          },
          formState: 'readonly',
          selectedPublicWarningId: warningList[5].id,
        },
        publicWarningActions.fetchWarningsSuccess({
          warnings: warningList,
          lastUpdatedTime: '10:13',
        }),
      );
      expect(result.publicWarning?.editor).toEqual(warningList[5].editor);
      expect(result.error).toBeUndefined();
    });

    it('should not update current editor if no selected warning', () => {
      const result = reducer(
        {
          ...initialState,
          publicWarning: {
            ...publicWarning,
            editor: 'current.user',
          },
          formState: 'readonly',
        },
        publicWarningActions.fetchWarningsSuccess({
          warnings: warningList,
          lastUpdatedTime: '10:13',
        }),
      );
      expect(result.publicWarning?.editor).toEqual('current.user');
      expect(result.error).toBeUndefined();
    });
  });
});
