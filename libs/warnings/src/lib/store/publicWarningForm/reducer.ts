/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { createSlice, PayloadAction, Draft } from '@reduxjs/toolkit';
import { uiActions } from '@opengeoweb/store';
import { AlertColor } from '@mui/material';
import { DrawingListItem } from '../drawings/types';
import { publicWarningDialogType } from './utils';
import { PublicWarning } from './types';
import { publicWarningActions } from '../publicWarnings/reducer';

export type FormAction = '' | 'saving' | 'publishing' | 'readonly';
export interface FormError {
  severity?: AlertColor;
  message: string;
}

export interface PublicWarningFormState {
  // form data
  object?: DrawingListItem;
  publicWarning?: PublicWarning;
  // form state
  formState?: FormAction;
  error?: FormError;
  selectedPublicWarningId?: string;
  isFormDirty?: boolean;
}
export const TAKEOVER_MESSAGE = 'You are no longer the editor of this warning';

export const initialState: PublicWarningFormState = {};

const slice = createSlice({
  initialState,
  name: 'publicWarningForm',
  reducers: {
    setFormValues: (
      draft: Draft<PublicWarningFormState>,
      action: PayloadAction<{
        object?: DrawingListItem;
        publicWarning?: PublicWarning;
        formState?: FormAction;
      }>,
    ) => {
      const { object, publicWarning, formState } = action.payload;

      if (object) {
        draft.object = object;
      }

      if (publicWarning) {
        draft.publicWarning = publicWarning;
      }

      if (formState !== undefined) {
        draft.formState = formState;
      }
    },
    openPublicWarningFormDialog: (
      draft: Draft<PublicWarningFormState>,
      action: PayloadAction<{
        object?: DrawingListItem;
        formState?: FormAction;
        publicWarning?: PublicWarning;
      }>,
    ) => {
      const { object, publicWarning, formState = '' } = action.payload;

      draft.object = object;
      draft.publicWarning = publicWarning;
      draft.selectedPublicWarningId = publicWarning?.id;
      draft.formState = formState;
    },

    toggleEditorWarning: (
      draft: Draft<PublicWarningFormState>,
      // eslint-disable-next-line no-unused-vars
      action: PayloadAction<{
        warningId: string;
        isEditor: boolean;
        username: string;
      }>,
    ) => {
      delete draft.error;
    },
    toggleEditorWarningSuccess: (
      draft: Draft<PublicWarningFormState>,
      action: PayloadAction<{
        isEditor: boolean;
        username: string;
        // eslint-disable-next-line no-unused-vars
        message: string;
      }>,
    ) => {
      const { isEditor, username } = action.payload;
      if (draft.publicWarning) {
        draft.publicWarning.editor = isEditor ? username : '';
      }
      draft.formState = isEditor ? '' : 'readonly';
    },

    // publish warning
    publishWarning: (
      draft: Draft<PublicWarningFormState>,
      // eslint-disable-next-line no-unused-vars
      action: PayloadAction<{
        publicWarning: PublicWarning;
      }>,
    ) => {
      draft.formState = 'publishing';
      delete draft.error;
    },
    publishWarningSuccess: (
      draft: Draft<PublicWarningFormState>,
      action: PayloadAction<{
        publicWarning: PublicWarning;
        message: string;
      }>,
    ) => {
      const { publicWarning } = action.payload;
      draft.publicWarning = publicWarning;
      // If form has been published, set form to readonly
      draft.formState = 'readonly';
    },
    // save warning
    saveWarning: (
      draft: Draft<PublicWarningFormState>,
      // eslint-disable-next-line no-unused-vars
      action: PayloadAction<{
        publicWarning: PublicWarning;
      }>,
    ) => {
      draft.formState = 'saving';
      delete draft.error;
      draft.publicWarning = action.payload.publicWarning;
    },
    saveWarningSuccess: (
      draft: Draft<PublicWarningFormState>,
      action: PayloadAction<{
        publicWarning: PublicWarning;
        message: string;
      }>,
    ) => {
      const { publicWarning } = action.payload;
      draft.publicWarning = publicWarning;
      if (publicWarning?.warningDetail?.id) {
        draft.selectedPublicWarningId = publicWarning.warningDetail.id;
      }
      draft.formState = '';
    },
    setFormError: (
      draft: Draft<PublicWarningFormState>,
      action: PayloadAction<FormError>,
    ) => {
      const { message, severity = 'error' } = action.payload;
      draft.formState = '';
      draft.error = {
        severity,
        message,
      };
    },
    setFormDirty: (
      draft: Draft<PublicWarningFormState>,
      action: PayloadAction<{
        isFormDirty: boolean;
      }>,
    ) => {
      const { isFormDirty } = action.payload;
      draft.isFormDirty = isFormDirty;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(
        uiActions.setToggleOpenDialog,
        (draft: Draft<PublicWarningFormState>, action) => {
          const { type, setOpen } = action.payload;
          if (type === publicWarningDialogType && setOpen === false) {
            delete draft.object;
            delete draft.publicWarning;
            delete draft.formState;
            delete draft.error;
            delete draft.selectedPublicWarningId;
            delete draft.isFormDirty;
          }
        },
      )
      .addCase(publicWarningActions.fetchWarningsSuccess, (draft, action) => {
        const { warnings } = action.payload;
        if (draft.selectedPublicWarningId) {
          const currentWarning = warnings.find(
            (warning) => warning.id === draft.selectedPublicWarningId,
          );
          if (draft.publicWarning?.editor !== currentWarning?.editor) {
            draft.publicWarning!.editor = currentWarning?.editor;
            // if form is in edit mode show warning
            if (draft.formState === '') {
              draft.error = {
                severity: 'warning',
                message: TAKEOVER_MESSAGE,
              };
            }
          }
        }
      });
  },
});

export const { reducer } = slice;
export const publicWarningFormActions = slice.actions;
