/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { SagaIterator } from 'redux-saga';
import { call, delay, put, takeLatest } from 'redux-saga/effects';
import { dateUtils, pollingIntervalTimeout } from '@opengeoweb/shared';
import { WarningsApi, getWarningsApi } from '../../utils/api';
import { publicWarningActions } from './reducer';
import { publicWarningFormActions } from '../publicWarningForm/reducer';
import { DATE_FORMAT_LASTUPDATEDTIME } from '../../utils/constants';

export const getLastUpdatedTimeFormatted = (): string =>
  dateUtils.dateToString(dateUtils.utc(), DATE_FORMAT_LASTUPDATEDTIME)!;

export function* fetchWarningsSaga(): SagaIterator {
  try {
    const warningsApi: WarningsApi = yield call(getWarningsApi);
    const { data: warnings } = yield call(warningsApi.getWarnings);
    const lastUpdatedTime = getLastUpdatedTimeFormatted();
    yield put(
      publicWarningActions.fetchWarningsSuccess({ warnings, lastUpdatedTime }),
    );

    yield delay(pollingIntervalTimeout);
    yield put(publicWarningActions.fetchWarnings());
  } catch (error) {
    yield put(
      publicWarningActions.fetchWarningsError({
        error: { name: error.name, message: error.message },
      }),
    );
  }
}

export function* refetchWarnings(): SagaIterator {
  yield put(publicWarningActions.fetchWarnings());
}

function* publicWarnings(): SagaIterator {
  yield takeLatest(publicWarningActions.fetchWarnings, fetchWarningsSaga);

  yield takeLatest(
    [
      publicWarningFormActions.publishWarningSuccess,
      publicWarningFormActions.saveWarningSuccess,
    ],
    refetchWarnings,
  );
}

export default publicWarnings;
