/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  createEntityAdapter,
  createSlice,
  PayloadAction,
  Draft,
} from '@reduxjs/toolkit';
import { PublicWarningsState } from './types';
import { PublicWarning } from '../publicWarningForm/types';

export const publicWarningsAdapter = createEntityAdapter<PublicWarning>({
  selectId: (warning) => warning.id!,
});

export const initialState: PublicWarningsState = {
  warnings: publicWarningsAdapter.getInitialState(),
  isLoading: true,
  lastUpdatedTime: '',
};

const slice = createSlice({
  initialState,
  name: 'publicWarnings',
  reducers: {
    fetchWarnings: (draft: Draft<PublicWarningsState>) => {
      draft.isLoading = true;
      delete draft.error;
    },
    fetchWarningsSuccess: (
      draft: Draft<PublicWarningsState>,
      action: PayloadAction<{
        warnings: PublicWarning[];
        lastUpdatedTime: string;
      }>,
    ) => {
      const { warnings, lastUpdatedTime } = action.payload;
      draft.isLoading = false;
      draft.lastUpdatedTime = lastUpdatedTime;
      publicWarningsAdapter.setAll(draft.warnings, warnings);
    },
    fetchWarningsError: (
      draft: Draft<PublicWarningsState>,
      action: PayloadAction<{ error: { name: string; message: string } }>,
    ) => {
      const { error } = action.payload;
      draft.isLoading = false;
      draft.error = error;
    },
  },
});

export const { reducer } = slice;
export const publicWarningActions = slice.actions;
