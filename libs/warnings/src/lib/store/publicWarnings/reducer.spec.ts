/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { initialState, publicWarningActions, reducer } from './reducer';
import { warningList } from '../../utils/fakeApi/index';

describe('public warnings reducer', () => {
  it('should handle fetchWarnings', () => {
    const result = reducer(initialState, publicWarningActions.fetchWarnings());
    expect(result.isLoading).toBeTruthy();
    expect(result.error).toBeUndefined();

    const result2 = reducer(
      {
        warnings: initialState.warnings,
        isLoading: false,
        error: { name: 'error', message: 'error descript' },
        lastUpdatedTime: '',
      },
      publicWarningActions.fetchWarnings(),
    );
    expect(result2.isLoading).toBeTruthy();
    expect(result2.error).toBeUndefined();
  });

  it('should handle fetchWarningsSuccess', () => {
    const result = reducer(
      initialState,
      publicWarningActions.fetchWarningsSuccess({
        warnings: warningList,
        lastUpdatedTime: '10:13',
      }),
    );
    result.warnings.ids.forEach((id) =>
      expect(result.warnings.entities[id]).toBeTruthy(),
    );
    expect(result.isLoading).toBeFalsy();
    expect(result.lastUpdatedTime).toEqual('10:13');

    const result2 = reducer(
      {
        isLoading: true,
        warnings: initialState.warnings,
        lastUpdatedTime: '12:13',
      },
      publicWarningActions.fetchWarningsSuccess({
        warnings: warningList,
        lastUpdatedTime: '13:13',
      }),
    );
    result2.warnings.ids.forEach((id) =>
      expect(result2.warnings.entities[id]).toBeTruthy(),
    );
    expect(result2.isLoading).toBeFalsy();
    expect(result2.lastUpdatedTime).toEqual('13:13');
  });

  it('should handle fetchWarningsError', () => {
    const error = { name: 'TestError', message: 'An error occurred' };

    const result = reducer(
      initialState,
      publicWarningActions.fetchWarningsError({ error }),
    );
    expect(result.error).toEqual(error);
    expect(result.isLoading).toBeFalsy();

    const result2 = reducer(
      {
        isLoading: true,
        warnings: initialState.warnings,
        error: { name: 'some diff', message: 'other message' },
        lastUpdatedTime: '',
      },
      publicWarningActions.fetchWarningsError({ error }),
    );
    expect(result2.error).toEqual(error);
    expect(result2.isLoading).toBeFalsy();
  });
});
