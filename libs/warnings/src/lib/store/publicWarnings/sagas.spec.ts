/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { call, delay, put, takeLatest } from 'redux-saga/effects';
import { pollingIntervalTimeout } from '@opengeoweb/shared';
import publicWarningsSaga, {
  fetchWarningsSaga,
  getLastUpdatedTimeFormatted,
  refetchWarnings,
} from './sagas';
import { publicWarningActions } from './reducer';
import { publicWarningFormActions } from '../publicWarningForm/reducer';
import { getWarningsApi } from '../../utils/api';
import { warningList } from '../../utils/fakeApi/index';

describe('store/publicWarnings/sagas', () => {
  describe('getLastUpdatedTimeFormatted', () => {
    it('should return formatted lastUpdatedTime', () => {
      const now = '2024-01-23T10:37:33Z';
      jest.useFakeTimers().setSystemTime(new Date(now));
      expect(getLastUpdatedTimeFormatted()).toEqual('10:37');

      const now2 = '2023-01-22T12:14:33Z';
      jest.useFakeTimers().setSystemTime(new Date(now2));
      expect(getLastUpdatedTimeFormatted()).toEqual('12:14');
    });
  });

  describe('fetchWarningsSaga', () => {
    it('should fetch warnings', () => {
      const now = '2024-01-23T10:37:33Z';
      jest.useFakeTimers().setSystemTime(new Date(now));

      const mockedWarningsApi = {
        getWarnings: jest.fn(),
      };
      const generator = fetchWarningsSaga();
      expect(generator.next().value).toEqual(call(getWarningsApi));
      expect(generator.next(mockedWarningsApi).value).toEqual(
        call(mockedWarningsApi.getWarnings),
      );
      const lastUpdatedTime = getLastUpdatedTimeFormatted();
      expect(generator.next({ data: warningList }).value).toEqual(
        put(
          publicWarningActions.fetchWarningsSuccess({
            warnings: warningList,
            lastUpdatedTime,
          }),
        ),
      );

      expect(generator.next().value).toEqual(delay(pollingIntervalTimeout));
      expect(generator.next().value).toEqual(
        put(publicWarningActions.fetchWarnings()),
      );
      expect(generator.next().done).toBeTruthy();
    });

    it('should trigger errror', () => {
      const mockedWarningsApi = {
        getWarnings: jest.fn(),
      };
      const generator = fetchWarningsSaga();
      expect(generator.next().value).toEqual(call(getWarningsApi));
      expect(generator.next(mockedWarningsApi).value).toEqual(
        call(mockedWarningsApi.getWarnings),
      );
      const errorMessage = 'Network error';
      const error = new Error(errorMessage);
      expect(generator.throw!(error).value).toEqual(
        put(
          publicWarningActions.fetchWarningsError({
            error: {
              name: error.name,
              message: error.message,
            },
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });
  describe('refetchWarnings', () => {
    it('should refetch warnings', () => {
      const generator = refetchWarnings();
      expect(generator.next().value).toEqual(
        put(publicWarningActions.fetchWarnings()),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });
  describe('publicWarnings rootSaga', () => {
    it('should have rootSaga', () => {
      const generator = publicWarningsSaga();
      expect(generator.next().value).toEqual(
        takeLatest(publicWarningActions.fetchWarnings, fetchWarningsSaga),
      );
      expect(generator.next().value).toEqual(
        takeLatest(
          [
            publicWarningFormActions.publishWarningSuccess,
            publicWarningFormActions.saveWarningSuccess,
          ],
          refetchWarnings,
        ),
      );

      expect(generator.next().done).toBeTruthy();
    });
  });
});
