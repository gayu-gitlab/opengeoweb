/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { WarningModuleStore } from '../config';
import {
  getPublicWarningsError,
  getPublicWarningsLastUpdatedTime,
  getPublicWarningsStore,
  isPublicWarningsLoading,
  selectAllPublicWarnings,
} from './selectors';
import { PublicWarningDetail } from '../publicWarningForm/types';

describe('public warnings selectors', () => {
  const fakeError = { name: 'test name', message: 'test-message' };
  const initialState: WarningModuleStore = {
    publicWarnings: {
      warnings: {
        ids: ['test-1', 'test-2'],
        entities: {
          'test-1': {
            id: 'test-1',
            lastUpdatedTime: '2023-12-06T06:56:39.376366',
            status: 'TODO',
            warningDetail: {
              id: 'test-1',
              phenomenon: 'wind',
              geoJSON: {
                type: 'FeatureCollection',
                features: [
                  {
                    type: 'Feature',
                    geometry: {
                      type: 'Polygon',
                      coordinates: [
                        [
                          [5.85, 53.5],
                          [5.8806901106, 53.5306901106],
                          [5.8307113686, 53.5275952683],
                          [5.6597877094, 53.5325946796],
                          [5.6584259036, 53.5326159539],
                          [5.5831325046, 53.5327665984],
                          [5.57114459, 53.5313329254],
                          [5.4888185611, 53.5111814389],
                          [5.5, 53.5],
                          [5.6, 53.4],
                          [5.7, 53.3833333333],
                          [5.75, 53.4],
                          [5.8, 53.45],
                          [5.85, 53.5],
                        ],
                      ],
                    },
                    properties: {
                      fill: '#fae21e',
                      stroke: '#fae21e',
                      'fill-opacity': 0.2,
                      'stroke-width': 2,
                      selectionType: 'poly',
                      warning_level: '1',
                      'stroke-opacity': 1,
                    },
                  },
                ],
              },
              validFrom: '2023-12-19T00:00:00+00:00',
              validUntil: '2023-12-20T00:00:00+00:00',
              level: 'moderate',
              domain: 'public',
              warningProposalSource: 'PASCAL-ECMWF-EPS',
              authority: 'KNMI',
              type: 1,
            } as unknown as PublicWarningDetail,
          },
          'test-2': {
            id: 'test-2',
            lastUpdatedTime: '2023-12-06T06:56:39.065168',
            status: 'TODO',
            warningDetail: {
              id: 'test-2',
              phenomenon: 'highTemp',
              geoJSON: {
                type: 'FeatureCollection',
                features: [
                  {
                    type: 'Feature',
                    geometry: {
                      type: 'Polygon',
                      coordinates: [
                        [
                          [6.6, 53.6],
                          [6.6366225551, 53.6183112776],
                          [6.6223825394, 53.6203037828],
                          [6.5181105804, 53.6151103358],
                          [6.5141415498, 53.6147536502],
                          [6.3688734368, 53.5958374422],
                          [6.3648329277, 53.5951417883],
                          [6.2483980021, 53.5701407766],
                          [6.1552617629, 53.5686530705],
                          [6.144801414, 53.5673753257],
                          [6.1069679905, 53.5586314937],
                          [6.1066699795, 53.5585616577],
                          [6.0276590975, 53.5397908964],
                          [5.9170578817, 53.5329421183],
                          [5.95, 53.5],
                          [6, 53.4857142857],
                          [6.1, 53.4777777778],
                          [6.2, 53.4888888889],
                          [6.3, 53.4888888889],
                          [6.4, 53.5],
                          [6.5, 53.55],
                          [6.6, 53.6],
                        ],
                      ],
                    },
                    properties: {
                      fill: '#fae21e',
                      stroke: '#fae21e',
                      'fill-opacity': 0.2,
                      'stroke-width': 2,
                      selectionType: 'poly',
                      warning_level: '1',
                      'stroke-opacity': 1,
                    },
                  },
                ],
              },
              validFrom: '2023-12-19T00:00:00+00:00',
              validUntil: '2023-12-20T00:00:00+00:00',
              level: 'moderate',
              domain: 'public',
              warningProposalSource: 'PASCAL-ECMWF-EPS',
              authority: 'KNMI',
              type: 1,
            } as unknown as PublicWarningDetail,
          },
        },
      },
      isLoading: true,
      error: fakeError,
      lastUpdatedTime: '2023-12-06T06:56:39.376366',
    },
  };

  describe('getPublicWarningsStore', () => {
    it('should return public warning store', () => {
      const publicWarningStore = getPublicWarningsStore(initialState);
      expect(publicWarningStore).toEqual(initialState.publicWarnings);
    });
    it('should return null if store can not be found', () => {
      const publicWarningStore = getPublicWarningsStore({});
      expect(publicWarningStore).toBeNull();
    });
  });

  describe('selectAllPublicWarnings', () => {
    it('should return stored warnings', () => {
      const publicWarningStore = selectAllPublicWarnings(initialState);
      expect(publicWarningStore).toEqual([
        initialState.publicWarnings?.warnings.entities['test-1'],
        initialState.publicWarnings?.warnings.entities['test-2'],
      ]);
    });
    it('should empty array if store can not be found', () => {
      const publicWarningStore = selectAllPublicWarnings({});
      expect(publicWarningStore).toEqual([]);
    });
  });

  describe('isPublicWarningsLoading', () => {
    it('should return isPublicWarningsLoading', () => {
      const publicWarningStore = isPublicWarningsLoading(initialState);
      expect(publicWarningStore).toBeTruthy();
    });
    it('should false if store can not be found', () => {
      const publicWarningStore = isPublicWarningsLoading({});
      expect(publicWarningStore).toBeFalsy();
    });
  });

  describe('getPublicWarningsError', () => {
    it('should return getPublicWarningsError', () => {
      const publicWarningStore = getPublicWarningsError(initialState);
      expect(publicWarningStore).toEqual(fakeError);
    });
    it('should false if store can not be found', () => {
      const publicWarningStore = getPublicWarningsError({});
      expect(publicWarningStore).toBeUndefined();
    });
  });

  describe('getPublicWarningsLastUpdatedTime', () => {
    it('should return getPublicWarningsLastUpdatedTime', () => {
      const publicWarningStore = getPublicWarningsLastUpdatedTime(initialState);
      expect(publicWarningStore).toEqual(
        initialState.publicWarnings?.lastUpdatedTime,
      );
    });
    it('should false if store can not be found', () => {
      const publicWarningStore = getPublicWarningsLastUpdatedTime({});
      expect(publicWarningStore).toBeUndefined();
    });
  });
});
