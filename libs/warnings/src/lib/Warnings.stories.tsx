/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { lightTheme } from '@opengeoweb/theme';
import { createStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import { WarningsThemeStoreProvider } from './components/Providers/Providers';
import { MapWithWarnings } from './storybookUtils/storyComponents';

export default {
  title: 'demo',
};

export const WarningsDemo = (): React.ReactElement => {
  const store = createStore({
    extensions: [getSagaExtension()],
  });
  return (
    <WarningsThemeStoreProvider theme={lightTheme} store={store}>
      <MapWithWarnings mapId="mapid_1" />
    </WarningsThemeStoreProvider>
  );
};
