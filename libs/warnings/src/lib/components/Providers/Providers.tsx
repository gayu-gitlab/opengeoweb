/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Theme } from '@mui/material';
import { Provider } from 'react-redux';
import { lightTheme, ThemeWrapper } from '@opengeoweb/theme';
import { ConfirmationServiceProvider, withEggs } from '@opengeoweb/shared';
import { Store } from '@reduxjs/toolkit';
import { createStore } from '@redux-eggs/redux-toolkit';
import { coreModuleConfig } from '@opengeoweb/store';
import { ApiProvider } from '@opengeoweb/api';
import { SnackbarWrapperConnect } from '@opengeoweb/snackbar';
import { AuthenticationProvider } from '@opengeoweb/authentication';
import { createApi as createFakeApi, MOCK_USERNAME } from '../../utils/fakeApi';
import { API_NAME, WarningsApi } from '../../utils/api';
import { drawingModuleConfig } from '../../store/config';

const appStore = createStore();

export interface WarningsThemeProviderProps {
  children: React.ReactNode;
  theme?: Theme;
}

export const WarningsThemeProvider: React.FC<WarningsThemeProviderProps> = ({
  children,
  theme = lightTheme,
}: WarningsThemeProviderProps) => {
  return <ThemeWrapper theme={theme}>{children}</ThemeWrapper>;
};

const WarningssWrapperProvider: React.FC<WarningsThemeProviderProps> = withEggs(
  [...coreModuleConfig, drawingModuleConfig],
)(({ children, ...props }: WarningsThemeProviderProps) => (
  <WarningsThemeProvider {...props}>{children}</WarningsThemeProvider>
));

export interface WarningsThemeStoreProviderProps
  extends WarningsThemeProviderProps {
  store?: Store;
  createApi?: () => WarningsApi;
}

export const WarningsThemeStoreProvider: React.FC<
  WarningsThemeStoreProviderProps
> = ({
  children,
  theme = lightTheme,
  store = appStore,
  createApi = createFakeApi,
}: WarningsThemeStoreProviderProps) => {
  return (
    <Provider store={store}>
      <AuthenticationProvider
        value={{
          isLoggedIn: true,
          auth: {
            username: MOCK_USERNAME,
            token: '1223344',
            refresh_token: '33455214',
          },
          onLogin: (): void => null!,
          onSetAuth: (): void => null!,
          sessionStorageProvider: null!,
        }}
      >
        <ApiProvider createApi={createApi} name={API_NAME}>
          <WarningssWrapperProvider theme={theme}>
            <SnackbarWrapperConnect>
              <ConfirmationServiceProvider>
                {children as React.ReactElement}
              </ConfirmationServiceProvider>
            </SnackbarWrapperConnect>
          </WarningssWrapperProvider>
        </ApiProvider>
      </AuthenticationProvider>
    </Provider>
  );
};
