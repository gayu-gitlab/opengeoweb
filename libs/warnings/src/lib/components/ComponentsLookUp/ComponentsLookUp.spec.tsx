/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { Credentials } from '@opengeoweb/api';
import { createStore } from '@redux-eggs/redux-toolkit';
import { render, waitFor, screen } from '@testing-library/react';
import * as React from 'react';

import {
  componentsLookUp,
  ComponentsLookUpPayload,
  PublicWarningApiWrapper,
} from './ComponentsLookUp';
import { WarningsThemeStoreProvider } from '../Providers/Providers';

describe('components/ComponentsLookUp/componentsLookUp', () => {
  const store = createStore();
  it('should return WarningList module', () => {
    const testPayload = {
      componentType: 'WarningList',
      id: 'test',
      initialProps: {},
    } as ComponentsLookUpPayload;

    const testConfig = {
      baseURL: 'test',
      appURL: 'test',
      authTokenURL: 'test',
      authClientId: 'test',
    };

    const testAuth: Credentials = {
      username: 'test user',
      token: '',
      refresh_token: '',
    };
    const testOnSetAuth = jest.fn();

    const result = componentsLookUp(testPayload, {
      config: testConfig,
      auth: testAuth,
      onSetAuth: testOnSetAuth,
    });

    expect(result.props['data-testid']).toEqual('publicWarningModule');
    expect(result.props.config).toEqual(testConfig);
    expect(result.props.auth).toEqual(testAuth);
  });

  it('should render PublicWarningApiWrapper', async () => {
    const config = {
      baseURL: 'test',
      appURL: 'test',
      authTokenURL: 'test',
      authClientId: 'test',
    };

    const auth: Credentials = {
      username: 'test user',
      token: '',
      refresh_token: '',
    };
    const onSetAuth = jest.fn();
    const props = {
      config,
      auth,
      onSetAuth,
    };
    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningApiWrapper {...props} />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(screen.getByTestId('publicWarningList')).toBeTruthy();
    });
  });

  it('should not render PublicWarningApiWrapper when no config provided', async () => {
    const config = undefined!;

    const auth: Credentials = {
      username: 'test user',
      token: '',
      refresh_token: '',
    };
    const onSetAuth = jest.fn();
    const props = {
      config,
      auth,
      onSetAuth,
    };
    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningApiWrapper {...props} />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByTestId('publicWarningList')).toBeFalsy();
    });
  });
});
