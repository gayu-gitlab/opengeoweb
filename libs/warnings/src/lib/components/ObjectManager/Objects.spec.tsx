/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { dateUtils } from '@opengeoweb/shared';
import { DrawingListItem } from '../../store/drawings/types';
import { WarningsThemeProvider } from '../Providers/Providers';
import {
  Objects,
  NO_OBJECTS_FOUND,
  BUTTON_SHARE,
  BUTTON_PUBLIC_WARNING,
  getListInclHeaders,
} from './Objects';

describe('objects', () => {
  it('should render objects and have correct callbacks', async () => {
    const props = {
      objects: [
        {
          id: '923723984872338768743',
          objectName: 'Drawing object 101',
          lastUpdatedTime: '2022-06-01T12:34:27Z',
          scope: 'user',
        },
        {
          id: '934834893283',
          objectName: 'object 89',
          lastUpdatedTime: '2022-05-26T12:34:27Z',
          scope: 'user',
        },
        {
          id: '345346456345',
          objectName: 'object 88',
          lastUpdatedTime: '2022-05-26T12:34:27Z',
          scope: 'user',
        },
      ] as DrawingListItem[],
      onClickObject: jest.fn(),
      onClickEdit: jest.fn(),
      onClickDelete: jest.fn(),
      onClickShare: jest.fn(),
      activeObject: '923723984872338768743',
    };

    render(
      <WarningsThemeProvider>
        <Objects {...props} />
      </WarningsThemeProvider>,
    );

    const drawingElement = await screen.findByText(props.objects[0].objectName);
    expect(drawingElement).toBeTruthy();

    expect(screen.getAllByRole('button')[0].classList).toContain(
      'Mui-selected',
    );

    fireEvent.click(drawingElement);

    expect(props.onClickObject).toHaveBeenCalledWith(props.objects[0]);

    fireEvent.click(
      screen.queryAllByRole('button', { name: /Object options/i })[0],
    );
    const editElement = await screen.findByText('Edit');
    fireEvent.click(editElement);

    expect(props.onClickEdit).toHaveBeenCalledWith(props.objects[0].id);

    fireEvent.click(
      screen.queryAllByRole('button', { name: /Object options/i })[0],
    );
    const deleteElement = await screen.findByText('Delete');
    fireEvent.click(deleteElement);

    expect(props.onClickDelete).toHaveBeenCalledWith(props.objects[0].id);

    expect(screen.queryAllByLabelText(BUTTON_SHARE)).toHaveLength(
      props.objects.length,
    );
    const shareElement = screen.queryAllByLabelText(BUTTON_SHARE)[0];
    expect(shareElement).toBeTruthy();
    fireEvent.click(shareElement);

    const publicWarningElement = await screen.findByText(BUTTON_PUBLIC_WARNING);
    expect(publicWarningElement).toBeTruthy();
    fireEvent.click(publicWarningElement);

    expect(props.onClickShare).toHaveBeenCalledWith(props.objects[0]);
  });

  it('should not render edit and delete buttons for global objects', async () => {
    const props = {
      objects: [
        {
          id: '923723984872338768743',
          objectName: 'Global drawing object 101',
          lastUpdatedTime: '2022-06-01T12:34:27Z',
          scope: 'global',
        },
      ] as DrawingListItem[],
      onClickObject: jest.fn(),
      onClickEdit: jest.fn(),
      onClickDelete: jest.fn(),
      onClickShare: jest.fn(),
      activeObject: '923723984872338768743',
    };

    render(
      <WarningsThemeProvider>
        <Objects {...props} />
      </WarningsThemeProvider>,
    );

    const drawingElement = await screen.findByText(props.objects[0].objectName);
    expect(drawingElement).toBeTruthy();

    expect(screen.getAllByRole('button')[0].classList).toContain(
      'Mui-selected',
    );

    fireEvent.click(drawingElement);

    expect(props.onClickObject).toHaveBeenCalledWith(props.objects[0]);

    expect(
      screen.queryByRole('button', { name: /Object options/i }),
    ).toBeFalsy();

    expect(screen.queryAllByLabelText(BUTTON_SHARE)).toHaveLength(
      props.objects.length,
    );
  });

  it('should show no results message', async () => {
    const props = {
      objects: [] as DrawingListItem[],
      onClickObject: jest.fn(),
      onClickEdit: jest.fn(),
      onClickDelete: jest.fn(),
      onClickShare: jest.fn(),
      activeObject: '923723984872338768743',
    };

    render(
      <WarningsThemeProvider>
        <Objects {...props} />
      </WarningsThemeProvider>,
    );

    expect(await screen.findByText(NO_OBJECTS_FOUND));
  });

  it('should not show no results message while loading', async () => {
    const props = {
      objects: [] as DrawingListItem[],
      onClickObject: jest.fn(),
      onClickEdit: jest.fn(),
      onClickDelete: jest.fn(),
      onClickShare: jest.fn(),
      activeObject: '923723984872338768743',
      isLoading: true,
    };

    render(
      <WarningsThemeProvider>
        <Objects {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.queryByText(NO_OBJECTS_FOUND)).toBeFalsy();
  });

  it('should show the header for each date once also when there is only one date in the list and the active drawing is updated', async () => {
    const props = {
      objects: [
        {
          id: '923723984872338768743',
          objectName: 'Drawing object 101',
          lastUpdatedTime: '2022-06-01T12:34:27Z',
          scope: 'user',
        },
        {
          id: '390390993999',
          objectName: 'Drawing object 102',
          lastUpdatedTime: '2022-06-01T14:34:27Z',
          scope: 'user',
        },
      ] as DrawingListItem[],
      onClickObject: jest.fn(),
      onClickEdit: jest.fn(),
      onClickDelete: jest.fn(),
      onClickShare: jest.fn(),
      activeObject: '',
    };

    const { rerender } = render(
      <WarningsThemeProvider>
        <Objects {...props} />
      </WarningsThemeProvider>,
    );

    const expectedDate = dateUtils.dateToString(
      dateUtils.isoStringToDate(props.objects[0].lastUpdatedTime),
      'EEEE, MMMM dd',
      true,
    )!;

    const drawingElement = await screen.findByText(props.objects[0].objectName);
    expect(drawingElement).toBeTruthy();
    expect(screen.getAllByRole('button')[0].classList).not.toContain(
      'Mui-selected',
    );
    expect((await screen.findAllByText(expectedDate)).length).toEqual(1);

    const props2 = {
      ...props,
      activeObject: props.objects[0].id,
    };

    rerender(
      <WarningsThemeProvider>
        <Objects {...props2} />
      </WarningsThemeProvider>,
    );

    const drawingElement2 = await screen.findByText(
      props.objects[0].objectName,
    );
    expect(drawingElement2).toBeTruthy();

    expect(screen.getAllByRole('button')[0].classList).toContain(
      'Mui-selected',
    );

    expect((await screen.findAllByText(expectedDate)).length).toEqual(1);
  });

  describe('getListInclHeaders', () => {
    const objects = [
      {
        id: '923723984872338768743',
        objectName: 'Drawing object 101',
        lastUpdatedTime: '2022-06-01T12:34:27Z',
        scope: 'user',
      },
      {
        id: '934834893283',
        objectName: 'object 89',
        lastUpdatedTime: '2022-05-26T12:34:27Z',
        scope: 'user',
      },
      {
        id: '345346456345',
        objectName: 'object 88',
        lastUpdatedTime: '2022-05-26T12:34:27Z',
        scope: 'user',
      },
    ] as DrawingListItem[];
    it('should create a list including headers with their index and a list of all the headers', async () => {
      expect(getListInclHeaders(objects)).toEqual({
        listInclHeaders: [
          {
            isObject: false,
            index: 0,
          },
          {
            isObject: true,
            index: 0,
          },
          {
            isObject: false,
            index: 1,
          },
          {
            isObject: true,
            index: 1,
          },
          {
            isObject: true,
            index: 2,
          },
        ],
        headerList: ['Wednesday, June 01', 'Thursday, May 26'],
      });
    });
  });
});
