/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { ApiProvider } from '@opengeoweb/api';
import { API_NAME, WarningsApi } from '../../utils/api';
import { WarningsThemeProvider } from '../Providers/Providers';
import {
  CANCEL,
  DELETE_CONFIRM,
  ConfirmDeleteDialog,
} from './ConfirmDeleteDialog';
import { createApi as createFakeApi } from '../../utils/fakeApi';

describe('components/ConfirmDeleteDialog', () => {
  it('renders correctly', () => {
    const props = {
      objectId: '1',
      objectName: 'test name',
      onDeleteSucces: jest.fn(),
      onClose: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <ConfirmDeleteDialog {...props} />,
      </WarningsThemeProvider>,
    );

    expect(screen.getByText(DELETE_CONFIRM)).toBeInTheDocument();
    expect(screen.getByText(CANCEL)).toBeInTheDocument();
  });

  it('calls onDeleteSuccess when delete is confirmed', async () => {
    const props = {
      objectId: '1',
      objectName: 'test name',
      onDeleteSucces: jest.fn(),
      onClose: jest.fn(),
    };

    render(
      <ApiProvider createApi={createFakeApi} name={API_NAME}>
        <WarningsThemeProvider>
          <ConfirmDeleteDialog {...props} />,
        </WarningsThemeProvider>
      </ApiProvider>,
    );

    const confirmButton = screen.getByText(DELETE_CONFIRM);
    fireEvent.click(confirmButton);

    expect(screen.getByTestId('confirm-dialog-spinner')).toBeTruthy();

    await waitFor(() => {
      expect(props.onDeleteSucces).toHaveBeenCalledWith(
        props.objectId,
        props.objectName,
      );
    });
    expect(screen.queryByTestId('confirm-dialog-spinner')).toBeFalsy();
  });

  it('show error from backend when delete returns error', async () => {
    const props = {
      objectId: '1',
      objectName: 'test name',
      onDeleteSucces: jest.fn(),
      onClose: jest.fn(),
    };

    const message = 'something went wrong test message';
    const errorFromBackend = { response: { data: { message } } };

    const createFakeErrorApi = (): WarningsApi => ({
      ...createFakeApi(),
      deleteDrawing: (): Promise<void> => {
        return new Promise((_, reject) => {
          reject(errorFromBackend);
        });
      },
    });

    render(
      <ApiProvider createApi={createFakeErrorApi} name={API_NAME}>
        <WarningsThemeProvider>
          <ConfirmDeleteDialog {...props} />,
        </WarningsThemeProvider>
      </ApiProvider>,
    );

    const confirmButton = screen.getByText(DELETE_CONFIRM);
    fireEvent.click(confirmButton);

    expect(screen.getByTestId('confirm-dialog-spinner')).toBeTruthy();

    await waitFor(() => {
      expect(screen.getByText(message)).toBeTruthy();
    });
    expect(props.onDeleteSucces).not.toHaveBeenCalled();
    expect(screen.queryByTestId('confirm-dialog-spinner')).toBeFalsy();
  });

  it('show error when delete returns error', async () => {
    const props = {
      objectId: '1',
      objectName: 'test name',
      onDeleteSucces: jest.fn(),
      onClose: jest.fn(),
    };

    const createFakeErrorApi = (): WarningsApi => ({
      ...createFakeApi(),
      deleteDrawing: (): Promise<void> => {
        return new Promise((_, reject) => {
          reject(new Error('Something went wrong'));
        });
      },
    });

    render(
      <ApiProvider createApi={createFakeErrorApi} name={API_NAME}>
        <WarningsThemeProvider>
          <ConfirmDeleteDialog {...props} />,
        </WarningsThemeProvider>
      </ApiProvider>,
    );

    const confirmButton = screen.getByText(DELETE_CONFIRM);
    fireEvent.click(confirmButton);

    expect(screen.getByTestId('confirm-dialog-spinner')).toBeTruthy();

    await waitFor(() => {
      expect(screen.getByText('Something went wrong')).toBeTruthy();
    });
    expect(props.onDeleteSucces).not.toHaveBeenCalled();
    expect(screen.queryByTestId('confirm-dialog-spinner')).toBeFalsy();
  });

  it('calls on close', async () => {
    const props = {
      objectId: '1',
      objectName: 'test name',
      onDeleteSucces: jest.fn(),
      onClose: jest.fn(),
    };

    render(
      <WarningsThemeProvider>
        <ConfirmDeleteDialog {...props} />,
      </WarningsThemeProvider>,
    );

    const cancelButton = screen.getByText(CANCEL);
    fireEvent.click(cancelButton);

    await waitFor(() => {
      expect(props.onClose).toHaveBeenCalled();
    });
  });
});
