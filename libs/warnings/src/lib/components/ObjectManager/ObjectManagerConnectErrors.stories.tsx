/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { lightTheme } from '@opengeoweb/theme';
import { createStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { WarningsApi } from '../../utils/api';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { DrawingFromBE, DrawingListItem } from '../../store/drawings/types';
import { MapWithWarnings } from '../../storybookUtils/storyComponents';
import { fakeErrorRequest } from '../../storybookUtils/testUtils';

const store = createStore({
  extensions: [getSagaExtension()],
});

export default {
  title: 'components/ObjectManagerConnect/errors',
};

export const ErrorSaveDrawing = (): React.ReactElement => {
  const createFakeApiWithErrorOnSave = (): WarningsApi => ({
    ...createFakeApi(),
    saveDrawingAs: () =>
      fakeErrorRequest<string>(
        'Something went wrong while saving, please try again.',
      ),
    updateDrawing: () =>
      fakeErrorRequest<void>(
        'Something went wrong while saving, please try again.',
      ),
  });
  return (
    <WarningsThemeStoreProvider
      createApi={createFakeApiWithErrorOnSave}
      theme={lightTheme}
      store={store}
    >
      <MapWithWarnings mapId="mapid_1" />
    </WarningsThemeStoreProvider>
  );
};

export const ErrorGetList = (): React.ReactElement => {
  const createFakeApiWithErrorOnGetList = (): WarningsApi => ({
    ...createFakeApi(),
    getDrawings: () =>
      fakeErrorRequest<{ data: DrawingListItem[] }>('Something went wrong'),
  });
  return (
    <WarningsThemeStoreProvider
      createApi={createFakeApiWithErrorOnGetList}
      theme={lightTheme}
      store={store}
    >
      <MapWithWarnings mapId="mapid_1" />
    </WarningsThemeStoreProvider>
  );
};

export const ErrorEditDrawing = (): React.ReactElement => {
  const createFakeApiWithErrorOnEdit = (): WarningsApi => ({
    ...createFakeApi(),
    getDrawingDetails: () =>
      fakeErrorRequest<{ data: DrawingFromBE }>(
        'Error fetching drawing, please try again.',
      ),
  });
  return (
    <WarningsThemeStoreProvider
      createApi={createFakeApiWithErrorOnEdit}
      theme={lightTheme}
      store={store}
    >
      <MapWithWarnings mapId="mapid_1" />
    </WarningsThemeStoreProvider>
  );
};

export const ErrorDeleteDrawing = (): React.ReactElement => {
  const createFakeApiWithError = (): WarningsApi => ({
    ...createFakeApi(),
    deleteDrawing: () => fakeErrorRequest<void>('Something went wrong'),
  });

  return (
    <WarningsThemeStoreProvider
      createApi={createFakeApiWithError}
      theme={lightTheme}
      store={store}
    >
      <MapWithWarnings mapId="mapid_1" />
    </WarningsThemeStoreProvider>
  );
};
