/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Box } from '@mui/material';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { ObjectManager } from './ObjectManager';
import { WarningsThemeProvider } from '../Providers/Providers';
import { drawingList } from '../../utils/fakeApi/index';
import { DrawingListItem } from '../../store/drawings/types';

export default {
  title: 'components/ObjectManager',
};

const extraLongItem: DrawingListItem = {
  id: 'longest292002',
  objectName: 'Reallylongnonclippablenameovermultiplelines',
  lastUpdatedTime: '2023-09-20T12:34:27Z',
  scope: 'global',
  geoJSON: {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        properties: {
          fill: '#ff7800',
          'fill-opacity': 0.2,
          stroke: '#ff7800',
          'stroke-width': 2,
          'stroke-opacity': 1,
          selectionType: 'poly',
        },
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [3.357673379394467, 54.021903403974555],
              [6.600428193305415, 54.583753881732186],
              [6.642909697417786, 50.7929736738295],
              [3.357673379394467, 54.021903403974555],
            ],
          ],
        },
      },
    ],
  },
};

export const ObjectManagerSmall = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '300px', height: '500px' }}>
        <ObjectManager
          onClose={(): void => {}}
          isOpen
          drawingListItems={[extraLongItem, ...drawingList]}
          activeDrawingId={extraLongItem.id}
          size={{ width: 160, height: 300 }}
        />
      </Box>
    </WarningsThemeProvider>
  );
};

ObjectManagerSmall.storyName = 'ObjectManagerSmall (takeSnapshot)';

export const ObjectManagerLight = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <ObjectManager
          onClose={(): void => {}}
          isOpen
          drawingListItems={drawingList}
          activeDrawingId={drawingList[0].id}
        />
      </Box>
    </WarningsThemeProvider>
  );
};

ObjectManagerLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64956cfe39ec23476bc3b1c8',
    },
  ],
};

ObjectManagerLight.storyName = 'ObjectManagerLight (takeSnapshot)';

export const ObjectManagerDark = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <ObjectManager
          onClose={(): void => {}}
          isOpen
          drawingListItems={drawingList}
          activeDrawingId={drawingList[0].id}
        />
      </Box>
    </WarningsThemeProvider>
  );
};

ObjectManagerDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64997ae799d4a822f85e26cc',
    },
  ],
};

ObjectManagerDark.storyName = 'ObjectManagerDark (takeSnapshot)';

export const ObjectManagerLightNoResults = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <ObjectManager onClose={(): void => {}} isOpen drawingListItems={[]} />
      </Box>
    </WarningsThemeProvider>
  );
};

ObjectManagerLightNoResults.storyName =
  'ObjectManagerLightNoResults (takeSnapshot)';

export const ObjectManagerDarkNoResults = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <ObjectManager onClose={(): void => {}} isOpen drawingListItems={[]} />
      </Box>
    </WarningsThemeProvider>
  );
};

ObjectManagerDarkNoResults.storyName =
  'ObjectManagerDarkNoResults (takeSnapshot)';

export const ObjectManagerLoading = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <ObjectManager
          onClose={(): void => {}}
          isOpen
          drawingListItems={drawingList}
          isLoading
        />
      </Box>
    </WarningsThemeProvider>
  );
};

export const ObjectManagerErrorLight = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <ObjectManager
          onClose={(): void => {}}
          isOpen
          drawingListItems={drawingList}
          error="Something went wrong"
        />
      </Box>
    </WarningsThemeProvider>
  );
};

export const ObjectManagerErrorDark = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Box sx={{ width: '500px', height: '800px' }}>
        <ObjectManager
          onClose={(): void => {}}
          isOpen
          drawingListItems={drawingList}
          error="Something went wrong"
        />
      </Box>
    </WarningsThemeProvider>
  );
};

ObjectManagerErrorLight.storyName = 'ObjectManagerErrorLight (takeSnapshot)';
ObjectManagerErrorDark.storyName = 'ObjectManagerErrorDark (takeSnapshot)';
