/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { createStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import { MapControls, emptyGeoJSON } from '@opengeoweb/webmap-react';
import { useDispatch } from 'react-redux';
import {
  getSingularDrawtoolDrawLayerId,
  mapStoreActions,
} from '@opengeoweb/store';
import {
  LayerManagerConnect,
  LayerManagerMapButtonConnect,
  LegendConnect,
  LegendMapButtonConnect,
  MapViewConnect,
  MultiDimensionSelectMapButtonsConnect,
  MultiMapDimensionSelectConnect,
} from '@opengeoweb/core';
import { LayerType } from '@opengeoweb/webmap';
import { TimeSliderConnect } from '@opengeoweb/timeslider';
import DrawingToolConnect from './DrawingToolConnect';
import DrawingToolMapButtonConnect from './DrawingToolMapButtonConnect';
import { useDefaultMapSettings } from '../../storybookUtils/defaultStorySettings';
import { WarningsThemeStoreProvider } from '../Providers/Providers';

export default {
  title: 'components/DrawingToolConnect',
};

const store = createStore({
  extensions: [getSagaExtension()],
});

interface MapWithDrawingToolProps {
  mapId: string;
}

const MapWithDrawingTool: React.FC<MapWithDrawingToolProps> = ({
  mapId,
}: MapWithDrawingToolProps) => {
  const dispatch = useDispatch();
  useDefaultMapSettings({
    mapId,
  });

  React.useEffect(() => {
    // add draw layer for drawingtool
    dispatch(
      mapStoreActions.addLayer({
        mapId,
        layer: {
          // empty geoJSON
          geojson: emptyGeoJSON,
          layerType: LayerType.featureLayer,
        },
        layerId: getSingularDrawtoolDrawLayerId(mapId),
        origin: 'ConfigurableMapConnect',
      }),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div style={{ height: '100vh' }}>
      <DrawingToolConnect mapId={mapId} />
      <LayerManagerConnect mapId={mapId} />
      <LayerManagerConnect mapId={mapId} isDocked />
      <MapControls>
        <LayerManagerMapButtonConnect mapId={mapId} />
        <LegendMapButtonConnect mapId={mapId} />
        <MultiDimensionSelectMapButtonsConnect mapId={mapId} />
        <DrawingToolMapButtonConnect mapId={mapId} />
      </MapControls>
      <LegendConnect mapId={mapId} />
      <MultiMapDimensionSelectConnect />
      <div
        style={{
          position: 'absolute',
          left: '0px',
          bottom: '0px',
          zIndex: 50,
          width: '100%',
        }}
      >
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </div>
      <MapViewConnect mapId={mapId} />
    </div>
  );
};

export const DrawingToolConnectLight = (): React.ReactElement => {
  return (
    <WarningsThemeStoreProvider theme={lightTheme} store={store}>
      <MapWithDrawingTool mapId="mapid_1" />
    </WarningsThemeStoreProvider>
  );
};

export const DrawingToolConnectDark = (): React.ReactElement => {
  return (
    <WarningsThemeStoreProvider theme={darkTheme} store={store}>
      <MapWithDrawingTool mapId="mapid_1" />
    </WarningsThemeStoreProvider>
  );
};

DrawingToolConnectLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64956cfe39ec23476bc3b1c8',
    },
  ],
};

DrawingToolConnectLight.storyName = 'DrawingToolConnect LightTheme ';

DrawingToolConnectDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/64997ae799d4a822f85e26cc',
    },
  ],
};

DrawingToolConnectDark.storyName = 'DrawingToolConnect DarkTheme';
