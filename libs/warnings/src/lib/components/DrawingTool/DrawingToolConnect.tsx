/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  uiSelectors,
  uiTypes,
  CoreAppStore,
  drawtoolActions,
  useSetupDialog,
} from '@opengeoweb/store';
import { currentlySupportedDrawModes } from '@opengeoweb/webmap-react';
import DrawingToolDialog from './DrawingToolDialog';
import { useCloseDrawDialog } from './useObjectDrawDialogAction';
import { DrawingToolFormConnect } from '../DrawingToolForm';
import { drawingDialogType } from '../../store/drawings/utils';

export const getDialogType = (mapId: string, isMultiMap: boolean): string => {
  if (isMultiMap) {
    return `${uiTypes.DialogTypes.DrawingTool}-${mapId}`;
  }
  return uiTypes.DialogTypes.DrawingTool;
};

interface DrawingToolConnectProps {
  mapId?: string;
  bounds?: string;
  title?: string;
  showMapIdInTitle?: boolean;
  isMultiMap?: boolean;
  source?: uiTypes.Source;
}

const DrawingToolConnect: React.FC<DrawingToolConnectProps> = ({
  bounds,
  title = 'Drawing Toolbox',
  showMapIdInTitle = false,
  mapId: initialMapId = null!,
  isMultiMap = false,
  source = 'app',
}: DrawingToolConnectProps) => {
  const dialogType = getDialogType(initialMapId, isMultiMap);
  const activeMapId = useSelector((store: CoreAppStore) =>
    uiSelectors.getDialogMapId(store, dialogType),
  );
  // In case of a multimap, use the map id that is passed
  // For floating drawingtoolbox, use the currently active, selected mapid
  const mapId = initialMapId || activeMapId;

  const { closeDrawDialog } = useCloseDrawDialog({
    mapId,
    source,
  });
  const {
    dialogOrder,
    setDialogOrder,
    isDialogOpen,
    uiSource,
    uiIsLoading,
    setFocused,
  } = useSetupDialog(dialogType, source);

  const shownTitle = showMapIdInTitle ? `${title} ${mapId}` : title;

  const onClose = (): void => {
    closeDrawDialog();
  };

  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(
      drawtoolActions.registerDrawTool({
        drawToolId: drawingDialogType,
        defaultDrawModes: currentlySupportedDrawModes,
      }),
    );

    return () => {
      dispatch(
        drawtoolActions.unregisterDrawTool({
          drawToolId: drawingDialogType,
        }),
      );
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (!isDialogOpen) {
    return null;
  }

  return (
    <DrawingToolDialog
      bounds={bounds}
      isOpen={isDialogOpen}
      onClose={onClose}
      title={shownTitle}
      onMouseDown={setDialogOrder}
      order={dialogOrder}
      source={uiSource}
      isLoading={uiIsLoading}
      setFocused={setFocused}
      headerSize="xs"
      startPosition={{ top: 134, left: 50 }}
    >
      <DrawingToolFormConnect />
    </DrawingToolDialog>
  );
};

export default DrawingToolConnect;
