/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { act, renderHook, waitFor } from '@testing-library/react';

import {
  drawtoolActions,
  getSingularDrawtoolDrawLayerId,
  layerActions,
  uiActions,
} from '@opengeoweb/store';
import {
  currentlySupportedDrawModes,
  defaultPolygon,
  emptyGeoJSON,
} from '@opengeoweb/webmap-react';
import { createStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';
import { useObjectDrawDialogAction } from './useObjectDrawDialogAction';
import { testGeoJSON } from '../../storybookUtils/testUtils';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import {
  drawingDialogType,
  objectDialogType,
} from '../../store/drawings/utils';

const mapId = 'map-id-1';
const mapId2 = 'map-id-2';
const source = 'app';
const layerId = getSingularDrawtoolDrawLayerId(mapId);
const layerId2 = getSingularDrawtoolDrawLayerId(mapId2);

const store = createStore({
  extensions: [getSagaExtension()],
});

const wrapper = ({
  children,
}: {
  children: React.ReactNode;
}): React.ReactElement => (
  <WarningsThemeStoreProvider store={store}>
    <ReactHookFormProvider>{children}</ReactHookFormProvider>
  </WarningsThemeStoreProvider>
);

describe('openObjectDialog', () => {
  it('should open object dialog for another map and move existing drawn shape with it', async () => {
    const { result } = renderHook(
      () => useObjectDrawDialogAction({ mapId, source }),
      {
        wrapper,
      },
    );

    // prepare store
    act(() => {
      store.dispatch(
        layerActions.addLayer({
          layerId,
          mapId,
          origin: source,
          layer: { geojson: emptyGeoJSON },
        }),
      );
      // the drawlayer for map2 has a drawn shape
      store.dispatch(
        layerActions.addLayer({
          layerId: layerId2,
          mapId: mapId2,
          origin: source,
          layer: { geojson: testGeoJSON },
        }),
      );
      store.dispatch(
        uiActions.registerDialog({
          type: objectDialogType,
        }),
      );
      // open object dialog for map 2
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          mapId: mapId2,
          setOpen: true,
          type: objectDialogType,
        }),
      );
    });

    expect(store.getState().layers.byId[layerId].geojson).toEqual(emptyGeoJSON);
    expect(store.getState().layers.byId[layerId2].geojson).toEqual(testGeoJSON);

    // open object dialog for map 1
    await waitFor(() => result.current.openObjectDialog());

    await waitFor(() =>
      expect(store.getState().ui.dialogs.objectManager.activeMapId).toEqual(
        mapId,
      ),
    );
    expect(store.getState().layers.byId[layerId].geojson).toEqual(testGeoJSON);
    expect(store.getState().layers.byId[layerId2].geojson).toEqual(
      emptyGeoJSON,
    );
  });
});

describe('openDrawDialog', () => {
  it('should open draw dialog for another map and move existing drawn shape with it', () => {
    const { result } = renderHook(
      () => useObjectDrawDialogAction({ mapId, source }),
      {
        wrapper,
      },
    );

    // prepare store
    act(() => {
      store.dispatch(
        layerActions.addLayer({
          layerId,
          mapId,
          origin: source,
          layer: { geojson: emptyGeoJSON },
        }),
      );
      // the drawlayer for map2 has a drawn shape
      store.dispatch(
        layerActions.addLayer({
          layerId: layerId2,
          mapId: mapId2,
          origin: source,
          layer: { geojson: testGeoJSON },
        }),
      );
      store.dispatch(
        uiActions.registerDialog({
          type: drawingDialogType,
        }),
      );
      // open drawing toolbox for map 2
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          mapId: mapId2,
          setOpen: true,
          type: drawingDialogType,
        }),
      );
    });

    expect(store.getState().layers.byId[layerId].geojson).toEqual(emptyGeoJSON);
    expect(store.getState().layers.byId[layerId2].geojson).toEqual(testGeoJSON);

    act(() => result.current.openDrawDialog());

    expect(store.getState().layers.byId[layerId].geojson).toEqual(testGeoJSON);
    expect(store.getState().layers.byId[layerId2].geojson).toEqual(
      emptyGeoJSON,
    );
  });

  it('should open draw dialog and activate draw mode', () => {
    const testDrawMode = defaultPolygon;

    const { result } = renderHook(
      () => useObjectDrawDialogAction({ mapId, source }),
      {
        wrapper,
      },
    );

    // prepare store
    act(() => {
      store.dispatch(
        layerActions.addLayer({
          layerId,
          mapId,
          origin: source,
          layer: { drawMode: '', isInEditMode: false },
        }),
      );
      store.dispatch(
        uiActions.registerDialog({
          type: drawingDialogType,
        }),
      );
      // activate drawing toolbox for map 2
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          mapId: mapId2,
          setOpen: false,
          type: drawingDialogType,
        }),
      );
      store.dispatch(
        drawtoolActions.registerDrawTool({
          defaultDrawModes: currentlySupportedDrawModes,
          drawToolId: drawingDialogType,
          geoJSONLayerId: '',
        }),
      );
      store.dispatch(
        drawtoolActions.changeDrawToolMode({
          drawToolId: drawingDialogType,
          drawModeId: testDrawMode.drawModeId,
        }),
      );
    });

    expect(store.getState().layers.byId[layerId].drawMode).toEqual('');
    expect(store.getState().layers.byId[layerId].isInEditMode).toEqual(false);

    act(() => result.current.openDrawDialog());

    expect(
      store.getState().drawingtools.entities[drawingDialogType].geoJSONLayerId,
    ).toEqual(layerId);
    expect(store.getState().layers.byId[layerId].isInEditMode).toEqual(true);
    expect(store.getState().layers.byId[layerId].drawMode).toEqual(
      testDrawMode.drawModeId,
    );
  });
});
