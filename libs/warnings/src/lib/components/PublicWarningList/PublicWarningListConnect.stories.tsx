/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { darkTheme } from '@opengeoweb/theme';
import React from 'react';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import { createStore } from '@redux-eggs/redux-toolkit';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { PublicWarningListConnect } from './PublicWarningListConnect';
import { fakeErrorRequest } from '../../storybookUtils/testUtils';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { warningList } from '../../utils/fakeApi/index';
import { WarningsApi } from '../../utils/api';
import { PublicWarning } from '../../store/publicWarningForm/types';
import { PublicWarningsFormDialogConnect } from '../PublicWarningsFormDialog';

export default {
  title: 'components/PublicWarningListConnect',
};

const store = createStore({
  extensions: [getSagaExtension()],
});

export const PublicWarningListLight = (): React.ReactElement => {
  return (
    <WarningsThemeStoreProvider store={store}>
      <PublicWarningListConnect />
      <PublicWarningsFormDialogConnect />
    </WarningsThemeStoreProvider>
  );
};

export const PublicWarningListDark = (): React.ReactElement => {
  return (
    <WarningsThemeStoreProvider store={store} theme={darkTheme}>
      <PublicWarningListConnect />
      <PublicWarningsFormDialogConnect />
    </WarningsThemeStoreProvider>
  );
};

export const PublicWarningListError = (): React.ReactElement => {
  const createFakeApiWithErrorOnSave = (): WarningsApi => ({
    ...createFakeApi(),
    getWarnings: () =>
      fakeErrorRequest<{ data: PublicWarning[] }>(
        'Something went wrong while fetching, please try again.',
      ),
  });
  return (
    <WarningsThemeStoreProvider
      store={store}
      theme={darkTheme}
      createApi={createFakeApiWithErrorOnSave}
    >
      <PublicWarningListConnect />
      <PublicWarningsFormDialogConnect />
    </WarningsThemeStoreProvider>
  );
};

export const PublicWarningListErrorDeleteWarning = (): React.ReactElement => {
  const createFakeApiWithError = (): WarningsApi => ({
    ...createFakeApi(),
    deleteWarning: () => fakeErrorRequest<void>('Oops, something went wrong'),
  });

  return (
    <WarningsThemeStoreProvider
      createApi={createFakeApiWithError}
      store={store}
    >
      <PublicWarningListConnect />
      <PublicWarningsFormDialogConnect />
    </WarningsThemeStoreProvider>
  );
};

export const PublicWarningListDifferentEditors = (): React.ReactElement => {
  // open a warning which you can edit, take over and press refresh button in background. Sometimes you have to press twice to test the result, as takeover could take a getWarning request.
  // After testing, refresh browser to test other cases
  let warningsLoadedCounter = 0;
  const createFakeApiWithError = (): WarningsApi => ({
    ...createFakeApi(),
    getWarnings: (): Promise<{ data: PublicWarning[] }> => {
      warningsLoadedCounter += 1;
      const data =
        warningsLoadedCounter > 3
          ? warningList.map((warning) => ({
              ...warning,
              editor: 'taken.over',
            }))
          : warningList;

      return new Promise((resolve) => {
        resolve({ data });
      });
    },
  });

  return (
    <WarningsThemeStoreProvider
      createApi={createFakeApiWithError}
      store={store}
    >
      <PublicWarningListConnect />
      <PublicWarningsFormDialogConnect />
    </WarningsThemeStoreProvider>
  );
};
