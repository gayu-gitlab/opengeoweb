/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CoreAppStore, uiActions } from '@opengeoweb/store';
import { useConfirmationDialog } from '@opengeoweb/shared';
import { snackbarActions } from '@opengeoweb/snackbar';
import { PublicWarningList } from './PublicWarningList';
import { PublicWarning } from '../../store/publicWarningForm/types';
import {
  FormAction,
  publicWarningFormActions,
} from '../../store/publicWarningForm/reducer';
import {
  getSelectedPublicIsFormDirty,
  getSelectedPublicWarningId,
} from '../../store/publicWarningForm/selectors';
import { publicWarningActions } from '../../store/publicWarnings/reducer';
import {
  getPublicWarningsError,
  getPublicWarningsLastUpdatedTime,
  isPublicWarningsLoading,
  selectAllPublicWarnings,
} from '../../store/publicWarnings/selectors';
import { warningFormConfirmationOptions } from '../PublicWarningsFormDialog/PublicWarningsFormDialogConnect';
import { ConfirmDeleteWarningDialog } from './ConfirmDeleteWarningDialog';
import { publicWarningDialogType } from '../../store/publicWarningForm/utils';

export const DELETE_WARNING_SUCCESS_MESSAGE = `Warning has been deleted`;

export const PublicWarningListConnect: React.FC = () => {
  const dispatch = useDispatch();
  const confirmDialog = useConfirmationDialog();

  const [confirmDeleteWarning, setConfirmDeleteWarning] = React.useState<
    string | undefined
  >(undefined);

  const warnings = useSelector((store: CoreAppStore) =>
    selectAllPublicWarnings(store),
  );
  const selectedPublicWarningId = useSelector((store: CoreAppStore) =>
    getSelectedPublicWarningId(store),
  );
  const isLoading = useSelector((store: CoreAppStore) =>
    isPublicWarningsLoading(store),
  );
  const error = useSelector((store: CoreAppStore) =>
    getPublicWarningsError(store),
  );

  const isFormDirty = useSelector((store: CoreAppStore) =>
    getSelectedPublicIsFormDirty(store),
  );

  const lastUpdatedTime = useSelector((store: CoreAppStore) =>
    getPublicWarningsLastUpdatedTime(store),
  );

  const openPublicWarningDialog = async (
    formAction?: FormAction,
    publicWarning?: PublicWarning,
  ): Promise<void> => {
    if (isFormDirty) {
      const mayProceed = await confirmDialog(warningFormConfirmationOptions)
        .then(() => true)
        .catch(() => false);

      if (!mayProceed) {
        return;
      }
    }

    dispatch(
      publicWarningFormActions.openPublicWarningFormDialog({
        ...(publicWarning && {
          publicWarning,
        }),
        formState: formAction,
      }),
    );
  };

  const selectWarning = (publicWarning?: PublicWarning): void => {
    openPublicWarningDialog('readonly', publicWarning);
  };
  const createWarning = (): void => {
    openPublicWarningDialog('');
  };
  const editWarning = (publicWarning: PublicWarning): void => {
    openPublicWarningDialog('', publicWarning);
  };

  const openSnackbar = (message: string): void => {
    dispatch(
      snackbarActions.openSnackbar({
        message,
      }),
    );
  };

  const deleteWarning = (warningId: string): void => {
    setConfirmDeleteWarning(warningId);
  };
  const onDeleteSuccess = (warningId: string): void => {
    // close confirm dialog
    setConfirmDeleteWarning(undefined);
    // Close warning dialog if needed
    if (selectedPublicWarningId === warningId) {
      dispatch(
        uiActions.setToggleOpenDialog({
          setOpen: false,
          type: publicWarningDialogType,
        }),
      );
    }

    openSnackbar(DELETE_WARNING_SUCCESS_MESSAGE);
    fetchWarnings();
  };

  const fetchWarnings = React.useCallback(() => {
    dispatch(publicWarningActions.fetchWarnings());
  }, [dispatch]);

  React.useEffect(() => {
    // fetch warnings on mount
    fetchWarnings();
  }, [fetchWarnings]);

  return (
    <>
      <PublicWarningList
        warnings={warnings}
        isLoading={isLoading}
        error={error}
        onSelectWarning={selectWarning}
        onCreateWarning={createWarning}
        onEditWarning={editWarning}
        onDeleteWarning={deleteWarning}
        selectedPublicWarningId={selectedPublicWarningId}
        lastUpdatedTime={lastUpdatedTime}
        onRefetchWarnings={fetchWarnings}
      />
      {confirmDeleteWarning && (
        <ConfirmDeleteWarningDialog
          warningId={confirmDeleteWarning}
          onDeleteSucces={onDeleteSuccess}
          onClose={(): void => setConfirmDeleteWarning(undefined)}
        />
      )}
    </>
  );
};

export default PublicWarningListConnect;
