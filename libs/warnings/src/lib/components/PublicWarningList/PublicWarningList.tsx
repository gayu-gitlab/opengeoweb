/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  Button,
  Box,
  Grid,
  LinearProgress,
  List,
  ListItem,
  Typography,
  ListItemButton,
  GridProps,
} from '@mui/material';

import React from 'react';
import {
  AlertBanner,
  LastUpdateTime,
  StatusTag,
  ToggleMenu,
  dateUtils,
} from '@opengeoweb/shared';
import { Delete, Edit, Options } from '@opengeoweb/theme';
import {
  PublicWarning,
  PublicWarningStatus,
} from '../../store/publicWarningForm/types';
import { DATE_FORMAT } from '../../utils/constants';
import { warningPhenomena } from '../PublicWarningsForm/PhenomenonField';
import { levelOptions } from '../PublicWarningsForm/LevelField';
import WarningAvatar from '../PublicWarningsFormDialog/WarningsAvatar';

const getStatus = (status: string): string => {
  switch (status) {
    case 'DRAFT':
      return 'Drafts';
    case 'PUBLISHED':
      return 'Published';
    case 'TODO':
      return 'To do';
    default:
      return 'Expired (last 24 hours)';
  }
};

export const CREATE_WARNING = 'Create a warning';
export const BUTTON_EDIT = 'Edit';
export const BUTTON_DELETE = 'Delete';
export const BUTTON_OPTIONS = 'Options';

interface SortedWarnings {
  draft: PublicWarning[];
  published: PublicWarning[];
  todo: PublicWarning[];
  expired: PublicWarning[];
}

const sortWarningsOnStatus = (warnings: PublicWarning[]): SortedWarnings => {
  return warnings.reduce<SortedWarnings>(
    (allWarnings, warning) => {
      if (warning.status === 'DRAFT') {
        return {
          ...allWarnings,
          draft: allWarnings?.draft.concat(warning),
        };
      }
      if (warning.status === 'PUBLISHED') {
        return {
          ...allWarnings,
          published: allWarnings?.published.concat(warning),
        };
      }
      if (warning.status === 'TODO') {
        return {
          ...allWarnings,
          todo: allWarnings?.todo.concat(warning),
        };
      }
      if (warning.status === 'EXPIRED') {
        return {
          ...allWarnings,
          expired: allWarnings?.expired.concat(warning),
        };
      }

      return allWarnings;
    },
    {
      todo: [],
      draft: [],
      published: [],
      expired: [],
    },
  );
};

export const WarningListColumnContent: React.FC<
  GridProps & {
    title?: string;
    status: PublicWarningStatus;
    children?: React.ReactNode;
    width?: number;
  }
> = ({ title, status, children, width = 76, ...props }) => {
  return (
    <Grid item {...props} sx={{ ...props.sx, width }}>
      {title && (
        <Box
          sx={{
            overflow: 'hidden',
            fontWeight: 'normal',
            textOverflow: 'ellipsis',
            fontSize: 10,
            lineHeight: 1.6,
            height: 12,
            color:
              status === 'EXPIRED'
                ? 'geowebColors.typographyAndIcons.inactiveText'
                : 'geowebColors.captions.captionStatus.rgba',
          }}
        >
          {title}
        </Box>
      )}
      <Box
        sx={{
          overflow: 'visible',
          fontSize: 12,
          lineHeight: 1.2,
          color:
            status === 'EXPIRED'
              ? 'geowebColors.typographyAndIcons.inactiveText'
              : 'geowebColors.typographyAndIcons.text',
        }}
      >
        {children || '-'}
      </Box>
    </Grid>
  );
};

const WarningsOnStatus: React.FC<{
  warnings: PublicWarning[];
  status: PublicWarningStatus;
  activeWarningId: string;
  onSelectWarning: (warning: PublicWarning) => void;
  onEditWarning: (warning: PublicWarning) => void;
  onDeleteWarning: (warningId: string) => void;
}> = ({
  warnings,
  status,
  activeWarningId,
  onSelectWarning,
  onEditWarning,
  onDeleteWarning,
}) => {
  const headerText = `${warnings.length} ${getStatus(status)}`;

  const onClickWarning = (warning: PublicWarning) => (): void =>
    onSelectWarning(warning);

  const onClickEditWarning = (warning: PublicWarning) => (): void =>
    onEditWarning(warning);

  const onClickDeleteWarning = (warningId: string) => (): void =>
    onDeleteWarning(warningId);

  return (
    <Box sx={{ paddingBottom: 2 }}>
      <Typography variant="caption" sx={{ opacity: 0.67 }}>
        {headerText}
      </Typography>
      {warnings.length > 0 && (
        <List aria-label={`warninglist ${status}`}>
          {warnings.map((warning) => {
            const phenomenonDetails = warningPhenomena.find(
              (phenomenon) =>
                phenomenon.key === warning.warningDetail.phenomenon,
            );
            const levelDetails = levelOptions.find(
              (level) => level.key === warning.warningDetail.level,
            );
            return (
              <ListItem
                key={warning.id}
                sx={{
                  marginBottom: 0.5,
                  padding: '0 0',
                  minHeight: '48px',
                  backgroundColor:
                    status === 'EXPIRED'
                      ? 'geowebColors.cards.cardContainerDisabled'
                      : 'geowebColors.cards.cardContainer',
                  borderColor:
                    status === 'EXPIRED'
                      ? 'geowebColors.cards.cardContainerDisabledBorder'
                      : 'geowebColors.cards.cardContainerBorder',
                  borderRadius: 1,
                  borderWidth: activeWarningId === warning.id ? '1px' : '1px',
                  borderStyle: 'solid',
                }}
                secondaryAction={
                  status !== 'EXPIRED' && (
                    <ToggleMenu
                      menuTitle={BUTTON_OPTIONS}
                      tooltipTitle={BUTTON_OPTIONS}
                      menuPosition="bottom"
                      aria-label={BUTTON_OPTIONS}
                      menuItems={
                        status === 'DRAFT'
                          ? [
                              {
                                text: BUTTON_EDIT,
                                action: onClickEditWarning(warning),
                                icon: <Edit />,
                              },
                              {
                                text: BUTTON_DELETE,
                                action: onClickDeleteWarning(warning.id!),
                                icon: <Delete />,
                              },
                            ]
                          : []
                      }
                      buttonIcon={<Options />}
                    />
                  )
                }
              >
                <ListItemButton
                  sx={{
                    height: '100%',
                    minHeight: '48px',
                    padding: 1,
                  }}
                  onClick={onClickWarning(warning)}
                  aria-label={`warning item for ${warning.id}`}
                  selected={activeWarningId === warning.id}
                >
                  <Grid
                    container
                    item
                    xs
                    rowSpacing={0}
                    columnSpacing={2}
                    justifyContent="left"
                    alignItems="center"
                  >
                    <WarningListColumnContent
                      title="Incident"
                      status={status}
                      width={142}
                    />
                    <WarningListColumnContent
                      title="Last update"
                      status={status}
                      width={142}
                    >
                      {warning.lastUpdatedTime &&
                        `${dateUtils.dateToString(
                          dateUtils.utc(warning.lastUpdatedTime!),
                          DATE_FORMAT,
                        )} UTC`}
                    </WarningListColumnContent>
                    <WarningListColumnContent
                      title="Start time"
                      status={status}
                      width={142}
                    >
                      {warning.warningDetail.validFrom &&
                        `${dateUtils.dateToString(
                          dateUtils.utc(warning.warningDetail.validFrom),
                          DATE_FORMAT,
                        )} UTC`}
                    </WarningListColumnContent>
                    <WarningListColumnContent
                      title="End time"
                      status={status}
                      width={142}
                    >
                      {warning.warningDetail.validUntil &&
                        `${dateUtils.dateToString(
                          dateUtils.utc(warning.warningDetail.validUntil),
                          DATE_FORMAT,
                        )} UTC`}
                    </WarningListColumnContent>
                    <WarningListColumnContent title="Domain" status={status}>
                      {warning.warningDetail.domain}
                    </WarningListColumnContent>
                    <WarningListColumnContent
                      title="Phenomenon"
                      status={status}
                      width={140}
                      sx={{ position: 'relative' }}
                    >
                      {phenomenonDetails?.title || '-'}
                      <Box
                        sx={{
                          position: 'absolute',
                          right: 0,
                          bottom: -6,
                        }}
                      >
                        {phenomenonDetails?.icon}
                      </Box>
                    </WarningListColumnContent>

                    <WarningListColumnContent title="Level" status={status}>
                      <Box
                        component="span"
                        sx={{
                          color: levelDetails?.textColor,
                          backgroundColor: levelDetails?.color,
                          paddingLeft: 0.5,
                          paddingRight: 0.5,
                          paddingBottom: '1px',
                          marginLeft: -0.5,
                        }}
                      >
                        {(warning.warningDetail?.level &&
                          levelDetails?.title) ||
                          '-'}
                      </Box>
                    </WarningListColumnContent>

                    <WarningListColumnContent status={status}>
                      {status === 'DRAFT' ? (
                        <Box sx={{ position: 'relative' }}>
                          {warning.editor && (
                            <Box
                              sx={{
                                width: '16px',
                                position: 'absolute',
                                top: '1px',
                                right: '-25px',
                              }}
                            >
                              <WarningAvatar editor={warning.editor} />
                            </Box>
                          )}

                          <Button
                            sx={{
                              height: '20px',
                              padding: '0px',

                              '& .MuiSvgIcon-root': { marginRight: '-2px' },
                            }}
                            variant="flat"
                            startIcon={<Edit />}
                          >
                            DRAFT
                          </Button>
                        </Box>
                      ) : (
                        <StatusTag
                          content={status.toLowerCase()}
                          color={status === 'EXPIRED' ? 'grey' : 'green'}
                          sx={{
                            width: 66,
                          }}
                        />
                      )}
                    </WarningListColumnContent>
                  </Grid>
                </ListItemButton>
              </ListItem>
            );
          })}
        </List>
      )}
    </Box>
  );
};

export const PublicWarningList: React.FC<{
  warnings: PublicWarning[];
  isLoading?: boolean;
  error?: Error;
  selectedPublicWarningId?: string;
  lastUpdatedTime?: string;
  onSelectWarning?: (warning: PublicWarning) => void;
  onEditWarning?: (warning: PublicWarning) => void;
  onCreateWarning?: () => void;
  onDeleteWarning?: (warningId: string) => void;
  onRefetchWarnings?: () => void;
}> = ({
  warnings,
  isLoading = false,
  error,
  selectedPublicWarningId = '',
  lastUpdatedTime,
  onSelectWarning = (): void => {},
  onEditWarning = (): void => {},
  onCreateWarning = (): void => {},
  onDeleteWarning = (): void => {},
  onRefetchWarnings = (): void => {},
}) => {
  const sortedWarnings = warnings ? sortWarningsOnStatus(warnings) : null;

  return (
    <Box
      sx={{ padding: 3, height: '100%', overflowY: 'auto' }}
      data-testid="publicWarningList"
    >
      <Grid container justifyContent="flex-end">
        <Button
          variant="primary"
          onClick={onCreateWarning}
          sx={{
            textTransform: 'initial',
            marginBottom: '8px',
            fontSize: '12px',
            height: '32px',
          }}
        >
          {CREATE_WARNING}
        </Button>
      </Grid>
      {error && <AlertBanner title={error.message} shouldClose />}
      {isLoading && (
        <LinearProgress data-testid="loading-bar" color="secondary" />
      )}
      {lastUpdatedTime && (
        <Box sx={{ marginBottom: -3.25, button: { marginRight: 0 } }}>
          <LastUpdateTime
            onPressRefresh={onRefetchWarnings}
            lastUpdateTime={lastUpdatedTime}
          />
        </Box>
      )}

      {sortedWarnings && !error && (
        <>
          <WarningsOnStatus
            status="TODO"
            warnings={sortedWarnings.todo}
            onSelectWarning={onSelectWarning}
            onEditWarning={onEditWarning}
            onDeleteWarning={onDeleteWarning}
            activeWarningId={selectedPublicWarningId}
          />
          <WarningsOnStatus
            status="DRAFT"
            warnings={sortedWarnings.draft}
            onSelectWarning={onSelectWarning}
            onEditWarning={onEditWarning}
            onDeleteWarning={onDeleteWarning}
            activeWarningId={selectedPublicWarningId}
          />
          <WarningsOnStatus
            status="PUBLISHED"
            warnings={sortedWarnings.published}
            onSelectWarning={onSelectWarning}
            onEditWarning={onEditWarning}
            onDeleteWarning={onDeleteWarning}
            activeWarningId={selectedPublicWarningId}
          />
          <WarningsOnStatus
            status="EXPIRED"
            warnings={sortedWarnings.expired}
            onSelectWarning={onSelectWarning}
            onEditWarning={onEditWarning}
            onDeleteWarning={onDeleteWarning}
            activeWarningId={selectedPublicWarningId}
          />
        </>
      )}
    </Box>
  );
};

export default PublicWarningList;
