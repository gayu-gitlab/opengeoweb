/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { darkTheme } from '@opengeoweb/theme';
import React from 'react';
import { Box } from '@mui/system';
import { WarningsThemeProvider } from '../Providers/Providers';
import { PublicWarningList } from './PublicWarningList';
import { warningList } from '../../utils/fakeApi/index';

export default {
  title: 'components/PublicWarningList',
};

const lastUpdatedTime = '13:45';

export const PublicWarningListLight = (): React.ReactElement => {
  return (
    <Box sx={{ backgroundColor: '#f5f5f5', padding: 1 }}>
      <WarningsThemeProvider>
        <PublicWarningList
          lastUpdatedTime={lastUpdatedTime}
          warnings={warningList}
          selectedPublicWarningId="b6daea16-46db-497a-865e-9e7320ad8d12"
        />
      </WarningsThemeProvider>
    </Box>
  );
};
PublicWarningListLight.storyName = 'PublicWarningList light (takeSnapshot)';
PublicWarningListLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6560c47c0b6b4002a5584e59',
    },
  ],
};

export const PublicWarningListDark = (): React.ReactElement => {
  return (
    <Box sx={{ backgroundColor: '#2b2b2b', padding: 1 }}>
      <WarningsThemeProvider theme={darkTheme}>
        <PublicWarningList
          lastUpdatedTime={lastUpdatedTime}
          warnings={warningList}
          selectedPublicWarningId="b6daea16-46db-497a-865e-9e7320ad8d12"
        />
      </WarningsThemeProvider>
    </Box>
  );
};
PublicWarningListDark.storyName = 'PublicWarningList dark (takeSnapshot)';
PublicWarningListDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/6560c46423101b0eb22a203d',
    },
  ],
};

export const PublicWarningListError = (): React.ReactElement => {
  return (
    <WarningsThemeProvider>
      <PublicWarningList
        lastUpdatedTime={lastUpdatedTime}
        warnings={[]}
        error={new Error('Something went wrong')}
      />
    </WarningsThemeProvider>
  );
};

export const PublicWarningListLoading = (): React.ReactElement => {
  return (
    <WarningsThemeProvider>
      <PublicWarningList
        lastUpdatedTime={lastUpdatedTime}
        warnings={[]}
        isLoading
      />
    </WarningsThemeProvider>
  );
};
