/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import React from 'react';
import createSagaMiddleware from 'redux-saga';
import { configureStore } from '@reduxjs/toolkit';
import userEvent from '@testing-library/user-event';
import {
  layerReducer,
  uiReducer,
  webmapReducer,
  drawtoolReducer,
} from '@opengeoweb/store';
import { snackbarReducer, snackbarSaga } from '@opengeoweb/snackbar';
import {
  dateUtils,
  getLastUpdateTitle,
  BUTTON_TITLE,
} from '@opengeoweb/shared';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { WarningsApi } from '../../utils/api';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { drawingList, warningList } from '../../utils/fakeApi/index';
import { DrawingListItem } from '../../store/drawings/types';
import { reducer as drawingsReducer } from '../../store/drawings/reducer';
import { reducer as publicWarningFormReducer } from '../../store/publicWarningForm/reducer';
import { reducer as publicWarningReducer } from '../../store/publicWarnings/reducer';

import publicWarningSagas from '../../store/publicWarningForm/sagas';

import drawingSaga from '../../store/drawings/sagas';
import publicWarningsSaga from '../../store/publicWarnings/sagas';

import {
  DELETE_WARNING_SUCCESS_MESSAGE,
  PublicWarningListConnect,
} from './PublicWarningListConnect';
import {
  PublicWarningsFormDialogConnect,
  warningFormConfirmationOptions,
} from '../PublicWarningsFormDialog/PublicWarningsFormDialogConnect';
import {
  BUTTON_DELETE,
  BUTTON_EDIT,
  CREATE_WARNING,
} from './PublicWarningList';
import { publicWarningDialogType } from '../../store/publicWarningForm/utils';
import {
  NO_OBJECT,
  OBJECT_SELECTED,
} from '../PublicWarningsForm/AreaField/AreaField';
import {
  BUTTON_CLEAR,
  BUTTON_PUBLISH,
  BUTTON_SAVE,
} from '../PublicWarningsForm';
import { DIALOG_TITLE } from '../PublicWarningsFormDialog/PublicWarningsFormDialog';
import {
  DELETE_CONFIRM,
  DELETE_TITLE,
  getDeleteDescription,
} from './ConfirmDeleteWarningDialog';
import { DATE_FORMAT_LASTUPDATEDTIME } from '../../utils/constants';

describe('src/components/PublicWarningList/PublicWarningListConnectIntegration', () => {
  const createApi = (): WarningsApi => {
    return {
      ...createFakeApi(),
      getDrawings: (): Promise<{ data: DrawingListItem[] }> => {
        return new Promise((resolve) => {
          resolve({ data: drawingList });
        });
      },
    };
  };
  const sagaMiddleware = createSagaMiddleware();

  const store = configureStore({
    reducer: {
      ui: uiReducer,
      layers: layerReducer,
      webmap: webmapReducer,
      drawings: drawingsReducer,
      drawingtools: drawtoolReducer,
      snackbar: snackbarReducer,
      publicWarningForm: publicWarningFormReducer,
      publicWarnings: publicWarningReducer,
    },
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(sagaMiddleware),
  });

  sagaMiddleware.run(snackbarSaga);
  sagaMiddleware.run(drawingSaga);
  sagaMiddleware.run(publicWarningSagas);
  sagaMiddleware.run(publicWarningsSaga);

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  store.addEggs = jest.fn();

  it('should show warnings after loading', async () => {
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );
    expect(screen.getByTestId('publicWarningList')).toBeTruthy();
    expect(screen.getByTestId('loading-bar')).toBeTruthy();

    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });

    expect(screen.getByLabelText('warninglist DRAFT').childNodes).toHaveLength(
      3,
    );
    expect(
      screen.getByLabelText('warninglist PUBLISHED').childNodes,
    ).toHaveLength(3);
    expect(screen.getByLabelText('warninglist TODO').childNodes).toHaveLength(
      5,
    );
    expect(
      screen.getByLabelText('warninglist EXPIRED').childNodes,
    ).toHaveLength(2);
  });

  it('should create and show warnings', async () => {
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    const user = userEvent.setup();
    // open create new warning dialog
    await user.click(
      screen.getByRole('button', {
        name: CREATE_WARNING,
      }),
    );
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();

    // should show edit options for new warning
    expect(screen.getByRole('button', { name: BUTTON_CLEAR })).toBeTruthy();
    expect(screen.getByRole('button', { name: BUTTON_SAVE })).toBeTruthy();
    expect(screen.getByRole('button', { name: BUTTON_PUBLISH })).toBeTruthy();

    // New warning so no toggle visible until you save
    expect(screen.queryByLabelText('Switch mode')).toBeFalsy();

    expect(
      store.getState().publicWarningForm.selectedPublicWarningId,
    ).toBeUndefined();
    expect(screen.getByText(NO_OBJECT)).toBeTruthy();
    expect(screen.queryByText(OBJECT_SELECTED)).toBeFalsy();

    // close create new warning dialog
    await user.click(
      screen.getByRole('button', {
        name: 'Close',
      }),
    );
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    expect(screen.queryByLabelText('Switch mode')).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_CLEAR })).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_SAVE })).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_PUBLISH })).toBeFalsy();

    // open existing warning dialog
    const listItem = screen.getByRole('button', {
      name: `warning item for ${warningList[0].id}`,
    });
    await user.click(listItem);
    expect(store.getState().publicWarningForm.selectedPublicWarningId).toEqual(
      warningList[0].id,
    );
    expect(
      screen.getByRole('button', {
        name: `warning item for ${warningList[0].id}`,
      }).classList,
    ).toContain('Mui-selected');
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
    // should not show edit options for TODO
    expect(screen.queryByLabelText('Switch mode')).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_CLEAR })).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_SAVE })).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_PUBLISH })).toBeFalsy();

    expect(screen.queryByText(NO_OBJECT)).toBeFalsy();
    expect(screen.getByText(OBJECT_SELECTED)).toBeTruthy();
    // close dialog
    await user.click(screen.getByTestId('closeBtn'));
    expect(
      store.getState().publicWarningForm.selectedPublicWarningId,
    ).toBeUndefined();
    expect(
      screen.getByRole('button', {
        name: `warning item for ${warningList[0].id}`,
      }).classList,
    ).not.toContain('Mui-selected');
  });

  it('should hide edit warning options for all type of warnings except new and DRAFT', async () => {
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    const user = userEvent.setup();
    // open create new warning dialog
    await user.click(
      screen.getByRole('button', {
        name: CREATE_WARNING,
      }),
    );
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();

    // open TODO warning dialog
    const warningTODO = screen.getByRole('button', {
      name: `warning item for ${warningList[0].id}`,
    });
    await user.click(warningTODO);
    expect(screen.queryByLabelText('Switch mode')).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_CLEAR })).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_SAVE })).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_PUBLISH })).toBeFalsy();

    // open PUBLISHED warning dialog
    const warningPUBLISHED = screen.getByRole('button', {
      name: `warning item for ${warningList[8].id}`,
    });
    await user.click(warningPUBLISHED);
    expect(screen.queryByLabelText('Switch mode')).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_CLEAR })).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_SAVE })).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_PUBLISH })).toBeFalsy();
    // open EXPIRED warning dialog
    const warningEXPIRED = screen.getByRole('button', {
      name: `warning item for ${warningList[11].id}`,
    });
    await user.click(warningEXPIRED);
    expect(screen.queryByLabelText('Switch mode')).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_CLEAR })).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_SAVE })).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_PUBLISH })).toBeFalsy();
  });

  it('should open a new warning in edit mode', async () => {
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    const user = userEvent.setup();
    // open create new warning dialog
    await user.click(
      screen.getByRole('button', {
        name: CREATE_WARNING,
      }),
    );
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();

    // open new warning dialog
    expect(screen.getByRole('button', { name: BUTTON_CLEAR })).toBeTruthy();
    expect(screen.getByRole('button', { name: BUTTON_SAVE })).toBeTruthy();
    expect(screen.getByRole('button', { name: BUTTON_PUBLISH })).toBeTruthy();
    // New warning so no toggle visible
    expect(screen.queryByLabelText('Switch mode')).toBeFalsy();
  });

  it('should open a draft warning in readonly mode', async () => {
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    const user = userEvent.setup();
    // open TODO warning dialog
    const warningTODO = await screen.findByRole('button', {
      name: `warning item for ${warningList[5].id}`,
    });
    await user.click(warningTODO);

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();

    expect(screen.getByLabelText('Switch mode')).toBeTruthy();
    expect(screen.queryByRole('button', { name: BUTTON_CLEAR })).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_SAVE })).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_PUBLISH })).toBeFalsy();
  });

  it('should open a draft warning in edit mode if you are editor', async () => {
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    const options = await screen.findAllByLabelText('Options');
    fireEvent.click(options[5]);
    expect(screen.getByText(BUTTON_EDIT)).toBeTruthy();
    fireEvent.click(screen.getByText(BUTTON_EDIT));

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
    expect(screen.getByLabelText('Switch mode')).toBeTruthy();
    expect(screen.getByRole('button', { name: BUTTON_CLEAR })).toBeTruthy();
    expect(screen.getByRole('button', { name: BUTTON_SAVE })).toBeTruthy();
    expect(screen.getByRole('button', { name: BUTTON_PUBLISH })).toBeTruthy();
  });

  it('should open a draft warning in view mode if you not are editor', async () => {
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    const options = await screen.findAllByLabelText('Options');
    fireEvent.click(options[6]);
    expect(screen.getByText(BUTTON_EDIT)).toBeTruthy();
    fireEvent.click(screen.getByText(BUTTON_EDIT));

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
    expect(screen.getByLabelText('Switch mode')).toBeTruthy();
    expect(screen.queryByRole('button', { name: BUTTON_CLEAR })).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_SAVE })).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_PUBLISH })).toBeFalsy();
  });

  it('should show confirmation dialog if changes are made in the form and user tries to close form', async () => {
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );
    expect(screen.queryByText(DIALOG_TITLE)).toBeFalsy();

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    const user = userEvent.setup();
    // open create new warning dialog
    await user.click(
      screen.getByRole('button', {
        name: CREATE_WARNING,
      }),
    );
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
    expect(screen.getByText(DIALOG_TITLE)).toBeTruthy();

    // make a change
    fireEvent.mouseDown(
      screen.getByRole('button', { name: 'Probability 30 %' }),
    );
    const menuItem = await screen.findByText('50 %');
    fireEvent.click(menuItem);

    // try to close it
    fireEvent.click(screen.getByTestId('closeBtn'));

    expect(warningFormConfirmationOptions.title).toBeTruthy();
    expect(warningFormConfirmationOptions.description).toBeTruthy();

    await user.click(
      screen.getByRole('button', {
        name: warningFormConfirmationOptions.confirmLabel,
      }),
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();
    expect(screen.queryByText(DIALOG_TITLE)).toBeFalsy();
  });

  it('should show confirmation dialog if changes are made in the form and user tries to select other warning', async () => {
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );
    expect(screen.queryByText(DIALOG_TITLE)).toBeFalsy();

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    const user = userEvent.setup();
    // open create new warning dialog
    await user.click(
      screen.getByRole('button', {
        name: CREATE_WARNING,
      }),
    );
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
    expect(screen.getByText(DIALOG_TITLE)).toBeTruthy();

    // make a change
    fireEvent.mouseDown(
      screen.getByRole('button', { name: 'Probability 30 %' }),
    );
    const menuItem = await screen.findByText('50 %');
    fireEvent.click(menuItem);

    // try to open other warning
    const listItem = screen.getByRole('button', {
      name: `warning item for ${warningList[0].id}`,
    });
    await user.click(listItem);
    expect(
      store.getState().publicWarningForm.selectedPublicWarningId,
    ).toBeUndefined();

    expect(screen.getByText(warningFormConfirmationOptions.title)).toBeTruthy();
    expect(
      screen.getByText(warningFormConfirmationOptions.description),
    ).toBeTruthy();

    // cancel open other warning
    await user.click(
      screen.getByRole('button', {
        name: warningFormConfirmationOptions.cancelLabel,
      }),
    );

    expect(
      screen.queryByText(warningFormConfirmationOptions.title),
    ).toBeFalsy();
    expect(
      screen.queryByText(warningFormConfirmationOptions.description),
    ).toBeFalsy();
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();
  });

  it('should allow deleting a draft warning and show confirmation dialog', async () => {
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );
    expect(screen.queryByText(DIALOG_TITLE)).toBeFalsy();

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();

    const user = userEvent.setup();

    await screen.findByText('3 Drafts');

    // try to delete the warning via the list
    const options = await screen.findAllByLabelText('Options');
    await user.click(options[5]);
    expect(screen.getByText(BUTTON_DELETE)).toBeTruthy();
    await user.click(screen.getByText(BUTTON_DELETE));

    expect(screen.getByText(DELETE_TITLE)).toBeTruthy();
    expect(screen.getByText(getDeleteDescription())).toBeTruthy();

    // Confirm deletion
    await user.click(screen.getByText(DELETE_CONFIRM));

    // Snackbar should be shown
    await waitFor(() => {
      expect(screen.getByText(DELETE_WARNING_SUCCESS_MESSAGE)).toBeTruthy();
    });

    // Warnings dialog should be closed
    await waitFor(() => {
      expect(
        store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
      ).toBeFalsy();
    });
    await waitFor(() => {
      expect(screen.queryByText(DIALOG_TITLE)).toBeFalsy();
    });
    // List should be updated
    await waitFor(() => {
      expect(screen.getByText('2 Drafts')).toBeTruthy();
    });
  });

  it('should be able to refetch warning list', async () => {
    const user = userEvent.setup();
    const now = dateUtils.utc();
    const nowString = dateUtils.dateToString(now, DATE_FORMAT_LASTUPDATEDTIME)!;
    const then = dateUtils.add(now, { hours: 1 });
    const thenString = dateUtils.dateToString(
      then,
      DATE_FORMAT_LASTUPDATEDTIME,
    )!;
    jest.spyOn(dateUtils, 'utc').mockReturnValue(now);
    render(
      <WarningsThemeStoreProvider createApi={createApi} store={store}>
        <PublicWarningListConnect />
        <PublicWarningsFormDialogConnect />
      </WarningsThemeStoreProvider>,
    );
    expect(screen.getByTestId('loading-bar')).toBeTruthy();

    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });

    expect(screen.getByText(getLastUpdateTitle(nowString))).toBeTruthy();

    // press refresh
    jest.spyOn(dateUtils, 'utc').mockReturnValue(then);
    await user.click(screen.getByRole('button', { name: BUTTON_TITLE }));

    expect(screen.getByTestId('loading-bar')).toBeTruthy();
    await waitFor(() => {
      expect(screen.queryByTestId('loading-bar')).toBeFalsy();
    });

    expect(screen.getByText(getLastUpdateTitle(thenString))).toBeTruthy();
    jest.restoreAllMocks();
  });
});
