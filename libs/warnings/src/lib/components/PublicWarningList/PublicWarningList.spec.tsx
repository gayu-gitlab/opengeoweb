/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { BUTTON_TITLE, getLastUpdateTitle } from '@opengeoweb/shared';
import { WarningsThemeProvider } from '../Providers/Providers';
import {
  BUTTON_DELETE,
  BUTTON_EDIT,
  BUTTON_OPTIONS,
  CREATE_WARNING,
  PublicWarningList,
} from './PublicWarningList';
import { warningList } from '../../utils/fakeApi/index';

describe('components/PublicWarningList/PublicWarningList', () => {
  it('should show component', () => {
    render(
      <WarningsThemeProvider>
        <PublicWarningList warnings={[]} />
      </WarningsThemeProvider>,
    );
    expect(screen.getByText(CREATE_WARNING)).toBeTruthy();
    expect(screen.getByText('0 To do')).toBeTruthy();
    expect(screen.getByText('0 Drafts')).toBeTruthy();
    expect(screen.getByText('0 Published')).toBeTruthy();
    expect(screen.getByText('0 Expired (last 24 hours)')).toBeTruthy();
    expect(screen.queryByTestId('refresh-button')).toBeFalsy();
  });

  it('should show warnings sorted by type', () => {
    render(
      <WarningsThemeProvider>
        <PublicWarningList warnings={warningList} />
      </WarningsThemeProvider>,
    );
    expect(screen.getByLabelText('warninglist DRAFT').childNodes).toHaveLength(
      3,
    );
    expect(
      screen.getByLabelText('warninglist PUBLISHED').childNodes,
    ).toHaveLength(3);
    expect(screen.getByLabelText('warninglist TODO').childNodes).toHaveLength(
      5,
    );
    expect(
      screen.getByLabelText('warninglist EXPIRED').childNodes,
    ).toHaveLength(2);
  });

  it('should format dates correctly', () => {
    render(
      <WarningsThemeProvider>
        <PublicWarningList warnings={[warningList[0]]} />
      </WarningsThemeProvider>,
    );
    expect(screen.getByText('06/12/2023 06:56 UTC')).toBeTruthy();
    expect(screen.getByText('19/12/2023 00:00 UTC')).toBeTruthy();
    expect(screen.getByText('19/12/2023 00:00 UTC')).toBeTruthy();
  });

  it('should show error', () => {
    const errorMessage = 'this is a test';
    const testError = new Error(errorMessage);
    render(
      <WarningsThemeProvider>
        <PublicWarningList warnings={warningList} error={testError} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText(errorMessage)).toBeTruthy();
    expect(screen.queryByLabelText('warninglist DRAFT')).toBeFalsy();
    expect(screen.queryByLabelText('warninglist PUBLISHED')).toBeFalsy();
    expect(screen.queryByLabelText('warninglist TODO')).toBeFalsy();
    expect(screen.queryByLabelText('warninglist EXPIRED')).toBeFalsy();
  });

  it('should show as initial loading', () => {
    render(
      <WarningsThemeProvider>
        <PublicWarningList warnings={[]} isLoading />
      </WarningsThemeProvider>,
    );

    expect(screen.getByTestId('loading-bar')).toBeTruthy();
    expect(screen.queryByLabelText('warninglist DRAFT')).toBeFalsy();
    expect(screen.queryByLabelText('warninglist PUBLISHED')).toBeFalsy();
    expect(screen.queryByLabelText('warninglist TODO')).toBeFalsy();
    expect(screen.queryByLabelText('warninglist EXPIRED')).toBeFalsy();
  });

  it('should show as loading when warnings are present', () => {
    render(
      <WarningsThemeProvider>
        <PublicWarningList warnings={warningList} isLoading />
      </WarningsThemeProvider>,
    );

    expect(screen.getByTestId('loading-bar')).toBeTruthy();
    expect(screen.getByLabelText('warninglist DRAFT')).toBeTruthy();
    expect(screen.getByLabelText('warninglist PUBLISHED')).toBeTruthy();
    expect(screen.getByLabelText('warninglist TODO')).toBeTruthy();
    expect(screen.getByLabelText('warninglist EXPIRED')).toBeTruthy();
  });

  it('should be able to select a warning', () => {
    const props = {
      warnings: warningList,
      onSelectWarning: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...props} />
      </WarningsThemeProvider>,
    );

    const listItem = screen.getByRole('button', {
      name: `warning item for ${warningList[0].id}`,
    });
    fireEvent.click(listItem);
    expect(props.onSelectWarning).toHaveBeenCalledWith(warningList[0]);

    const listItem2 = screen.getByRole('button', {
      name: `warning item for ${warningList[1].id}`,
    });
    fireEvent.click(listItem2);
    expect(props.onSelectWarning).toHaveBeenCalledWith(warningList[1]);
  });

  it('should be create a warning', () => {
    const props = {
      warnings: warningList,
      onCreateWarning: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...props} />
      </WarningsThemeProvider>,
    );

    fireEvent.click(screen.getByText(CREATE_WARNING));

    expect(props.onCreateWarning).toHaveBeenCalled();
  });

  it('should show active warning', () => {
    const props = {
      warnings: warningList,
      onCreateWarning: jest.fn(),
      selectedPublicWarningId: warningList[0].id,
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...props} />
      </WarningsThemeProvider>,
    );

    expect(
      screen.getByRole('button', {
        name: `warning item for ${warningList[0].id}`,
      }).classList,
    ).toContain('Mui-selected');
  });

  it('should be able to edit a draft warning', () => {
    const props = {
      warnings: warningList,
      onEditWarning: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...props} />
      </WarningsThemeProvider>,
    );

    const optionsButton = screen.getAllByRole('button', {
      name: BUTTON_OPTIONS,
    })[5];
    fireEvent.click(optionsButton);
    fireEvent.click(screen.getByText(BUTTON_EDIT));
    expect(props.onEditWarning).toHaveBeenCalledWith(warningList[5]);
  });

  it('should not show edit option if warning is not a draft', () => {
    const props = {
      warnings: warningList,
      onSelectWarning: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...props} />
      </WarningsThemeProvider>,
    );

    const optionsButton = screen.getAllByRole('button', {
      name: BUTTON_OPTIONS,
    })[0];
    fireEvent.click(optionsButton);

    expect(screen.queryByText(BUTTON_EDIT)).toBeFalsy();
  });

  it('should be able to delete a draft warning', () => {
    const props = {
      warnings: warningList,
      onDeleteWarning: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...props} />
      </WarningsThemeProvider>,
    );

    const optionsButton = screen.getAllByRole('button', {
      name: BUTTON_OPTIONS,
    })[5];
    fireEvent.click(optionsButton);
    fireEvent.click(screen.getByText(BUTTON_DELETE));
    expect(props.onDeleteWarning).toHaveBeenCalledWith(warningList[5].id);
  });

  it('should not show delete option if warning is not a draft', () => {
    const props = {
      warnings: warningList,
      onDeleteWarning: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...props} />
      </WarningsThemeProvider>,
    );

    const optionsButton = screen.getAllByRole('button', {
      name: BUTTON_OPTIONS,
    })[0];
    fireEvent.click(optionsButton);

    expect(screen.queryByText(BUTTON_DELETE)).toBeFalsy();
  });

  it('should show avatar if editor for that warning', () => {
    const props = {
      warnings: [warningList[5]],
      onDeleteWarning: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText('UN')).toBeTruthy();
  });

  it('should show no avatar if no editor for that warning', () => {
    const props = {
      warnings: [warningList[7]],
      onDeleteWarning: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.queryByTestId('avatar')).toBeFalsy();
  });

  it('be able to see last updated time and trigger a refetch', () => {
    const props = {
      warnings: [warningList[7]],
      onRefetchWarnings: jest.fn(),
      lastUpdatedTime: '10:23',
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningList {...props} />
      </WarningsThemeProvider>,
    );
    expect(props.onRefetchWarnings).toHaveBeenCalledTimes(0);
    expect(
      screen.getByText(getLastUpdateTitle(props.lastUpdatedTime)),
    ).toBeTruthy();
    fireEvent.click(screen.getByRole('button', { name: BUTTON_TITLE }));
    expect(props.onRefetchWarnings).toHaveBeenCalledTimes(1);
  });
});
