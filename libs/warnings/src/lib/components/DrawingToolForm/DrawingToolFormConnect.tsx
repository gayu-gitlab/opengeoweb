/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  CoreAppStore,
  drawtoolActions,
  drawtoolSelectors,
  layerActions,
  layerSelectors,
  uiSelectors,
} from '@opengeoweb/store';
import { useDispatch, useSelector } from 'react-redux';
import { DrawMode } from '@opengeoweb/webmap-react';
import { DrawingToolForm, DrawingToolFormValues } from './DrawingToolForm';
import { selectDrawByInstanceId } from '../../store/drawings/selectors';
import { drawActions } from '../../store/drawings/reducer';
import { drawingDialogType } from '../../store/drawings/utils';

export const DrawingToolFormConnect: React.FC = () => {
  const dispatch = useDispatch();
  const drawing = useSelector((store: CoreAppStore) =>
    selectDrawByInstanceId(store, drawingDialogType),
  );
  const drawToolId = drawingDialogType;
  const drawTool = useSelector((store: CoreAppStore) =>
    drawtoolSelectors.selectDrawToolById(store, drawToolId),
  );

  const layerFeatureGeoJSON = useSelector((store: CoreAppStore) =>
    layerSelectors.getFeatureLayerGeoJSON(store, drawTool?.geoJSONLayerId),
  );

  const isLayerInEditMode = useSelector((store: CoreAppStore) =>
    layerSelectors.getIsLayerInEditMode(store, drawTool?.geoJSONLayerId),
  );

  const featureLayerGeoJSONProperties = useSelector((store: CoreAppStore) =>
    layerSelectors.getFeatureLayerGeoJSONProperties(
      store,
      drawTool?.geoJSONLayerId,
    ),
  );

  const isDialogLoading = useSelector((store: CoreAppStore) =>
    uiSelectors.getDialogIsLoading(store, drawToolId),
  );

  const dialogError = useSelector((store: CoreAppStore) =>
    uiSelectors.getDialogError(store, drawToolId),
  );

  const onSubmitForm = (formValues: DrawingToolFormValues): void => {
    dispatch(
      drawActions.submitDrawValues({
        geoJSON: formValues.geoJSON,
        objectName: formValues.objectName,
        scope: 'user',
        id: drawing?.id,
      }),
    );
  };
  const onChangeGeoJSONProperties = (
    properties: GeoJSON.GeoJsonProperties,
  ): void => {
    dispatch(
      layerActions.updateFeatureProperties({
        layerId: drawTool?.geoJSONLayerId!,
        properties,
        origin: 'DrawingToolFormConnect',
      }),
    );
  };

  const onChangeDrawMode = (tool: DrawMode): void => {
    dispatch(
      drawtoolActions.changeDrawToolMode({
        drawToolId,
        drawModeId: tool.drawModeId,
      }),
    );
  };

  const onDeactivateTool = (): void => {
    dispatch(
      layerActions.toggleFeatureMode({
        layerId: drawTool?.geoJSONLayerId!,
        isInEditMode: false,
        drawMode: '',
      }),
    );
    if (drawTool?.activeDrawModeId) {
      dispatch(
        drawtoolActions.changeDrawToolMode({
          drawToolId,
          drawModeId: drawTool?.activeDrawModeId,
        }),
      );
    }
  };

  const onChangeName = (newName: string): void => {
    dispatch(
      drawActions.setDrawValues({
        objectName: newName,
        id: drawing?.id || '',
        drawingInstanceId: drawingDialogType,
        isFormDirty: drawing?.isFormDirty,
      }),
    );
  };

  const onChangeFormDirty = (isFormDirty: boolean): void => {
    dispatch(
      drawActions.setFormDirty({
        drawingInstanceId: drawingDialogType,
        isFormDirty,
      }),
    );
  };

  return (
    <DrawingToolForm
      id={drawing?.id}
      onSubmitForm={onSubmitForm}
      objectName={drawing?.objectName}
      isLoading={isDialogLoading}
      error={dialogError}
      // new
      drawModes={drawTool?.drawModes!}
      onChangeDrawMode={onChangeDrawMode}
      activeDrawModeId={drawTool?.activeDrawModeId || ''}
      onDeactivateTool={onDeactivateTool}
      isInEditMode={isLayerInEditMode}
      geoJSONProperties={featureLayerGeoJSONProperties}
      onChangeProperties={onChangeGeoJSONProperties}
      onChangeName={onChangeName}
      geoJSON={layerFeatureGeoJSON}
      onFormDirty={onChangeFormDirty}
    />
  );
};
