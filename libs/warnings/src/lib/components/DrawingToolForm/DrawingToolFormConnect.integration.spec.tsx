/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { render, screen } from '@testing-library/react';
import React from 'react';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import { configureStore } from '@reduxjs/toolkit';
import userEvent from '@testing-library/user-event';
import {
  layerReducer,
  uiReducer,
  webmapReducer,
  mapActions,
  drawtoolReducer,
} from '@opengeoweb/store';
import { ApiProvider } from '@opengeoweb/api';
import { snackbarReducer, snackbarSaga } from '@opengeoweb/snackbar';

import { ConfirmationServiceProvider } from '@opengeoweb/shared';
import { ObjectManagerConnect } from '../ObjectManager/ObjectManagerConnect';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { API_NAME, WarningsApi } from '../../utils/api';
import {
  WarningsThemeProvider,
  WarningsThemeStoreProviderProps,
} from '../Providers/Providers';
import { drawingList } from '../../utils/fakeApi/index';
import { DrawingListItem } from '../../store/drawings/types';
import { ObjectManagerMapButtonConnect } from '../ObjectManager/ObjectManagerMapButtonConnect';
import { reducer as drawingsReducer } from '../../store/drawings/reducer';
import { reducer as publicWarningReducer } from '../../store/publicWarningForm/reducer';

import DrawingToolConnect from '../DrawingTool/DrawingToolConnect';
import DrawingToolMapButtonConnect from '../DrawingTool/DrawingToolMapButtonConnect';
import drawingSaga from '../../store/drawings/sagas';
import publicWarningSaga from '../../store/publicWarningForm/sagas';
import { confirmationDialogOptions } from '../DrawingTool/useObjectDrawDialogAction';

const TestWrapper: React.FC<WarningsThemeStoreProviderProps> = ({
  store,
  createApi,
  children,
}) => (
  <Provider store={store!}>
    <ApiProvider createApi={createApi!} name={API_NAME}>
      <WarningsThemeProvider>
        <ConfirmationServiceProvider>{children}</ConfirmationServiceProvider>
      </WarningsThemeProvider>
    </ApiProvider>
  </Provider>
);

describe('src/components/DrawingToolForm/DrawingToolFormConnect', () => {
  const createApi = (): WarningsApi => {
    return {
      ...createFakeApi(),
      getDrawings: (): Promise<{ data: DrawingListItem[] }> => {
        return new Promise((resolve) => {
          resolve({ data: drawingList });
        });
      },
    };
  };
  const sagaMiddleware = createSagaMiddleware();

  const store = configureStore({
    reducer: {
      ui: uiReducer,
      layers: layerReducer,
      webmap: webmapReducer,
      drawings: drawingsReducer,
      drawingtools: drawtoolReducer,
      snackbar: snackbarReducer,
      publicWarningForm: publicWarningReducer,
    },
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(sagaMiddleware),
  });

  sagaMiddleware.run(snackbarSaga);
  sagaMiddleware.run(drawingSaga);
  sagaMiddleware.run(publicWarningSaga);

  it('ask for confirmation if there are changes in the form and closing the draw dialog', async () => {
    const mapId = 'map129';
    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <TestWrapper store={store} createApi={createApi}>
        <ObjectManagerConnect />
        <ObjectManagerMapButtonConnect mapId={mapId} />
        <DrawingToolConnect showMapIdInTitle />
        <DrawingToolMapButtonConnect mapId={mapId} />
      </TestWrapper>,
    );

    const user = userEvent.setup();

    // open drawing tool
    await user.click(
      screen.getByRole('button', {
        name: `Drawing Toolbox`,
      }),
    );
    const drawtoolboxDialogTitle = `Drawing Toolbox ${mapId}`;
    expect(await screen.findByText(drawtoolboxDialogTitle)).toBeTruthy();
    expect(screen.queryByText(`Object Manager for ${mapId}`)).toBeFalsy();

    // check that drawing tool is active
    const drawingButton = screen.getByRole('button', {
      name: 'Drawing Toolbox',
    });
    expect(drawingButton.classList).toContain('Mui-selected');

    await user.type(
      screen.getByRole('textbox', {
        name: `Object name`,
      }),
      'some new value',
    );

    await user.click(
      screen.getByRole('button', {
        name: /Close/i,
      }),
    );

    // should show confirm dialog
    expect(screen.getByText(confirmationDialogOptions.title)).toBeTruthy();
    expect(
      screen.getByText(confirmationDialogOptions.description),
    ).toBeTruthy();

    // press cancel
    await user.click(
      screen.getByRole('button', {
        name: /Cancel/i,
      }),
    );

    expect(screen.queryByText(confirmationDialogOptions.title)).toBeFalsy();
    expect(
      screen.queryByText(confirmationDialogOptions.description),
    ).toBeFalsy();

    // open object manager
    await user.click(
      screen.getByRole('button', {
        name: `Object Manager Button for ${mapId}`,
      }),
    );

    // should show confirm dialog
    expect(screen.getByText(confirmationDialogOptions.title)).toBeTruthy();
    expect(
      screen.getByText(confirmationDialogOptions.description),
    ).toBeTruthy();

    // press close
    await user.click(screen.getByTestId('confirmationDialog-confirm'));
    expect(screen.queryByText(confirmationDialogOptions.title)).toBeFalsy();
    expect(
      screen.queryByText(confirmationDialogOptions.description),
    ).toBeFalsy();
    expect(screen.queryByText(drawtoolboxDialogTitle)).toBeFalsy();
  });
});
