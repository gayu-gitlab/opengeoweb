/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { ReactHookFormProvider } from '@opengeoweb/form-fields';
import { render, screen } from '@testing-library/react';
import React from 'react';
import { LABEL_LEVEL, LevelField } from './LevelField';

describe('components/PublicWarningsForm/PublicWarningsForm/LevelField', () => {
  it('should render with default props', () => {
    render(
      <ReactHookFormProvider>
        <LevelField />
      </ReactHookFormProvider>,
    );

    expect(screen.getByText(LABEL_LEVEL)).toBeTruthy();
    const button = screen.getByRole('button', { name: /Level/i, hidden: true });
    expect(button).toBeTruthy();
    expect(button.getAttribute('disabled')).toBeNull();
  });

  it('should render as disabled', () => {
    render(
      <ReactHookFormProvider>
        <LevelField isDisabled />
      </ReactHookFormProvider>,
    );

    const button = screen.getByRole('button', { name: /Level/i, hidden: true });
    expect(button).toBeTruthy();
    expect(button.getAttribute('disabled')).toBeDefined();
  });

  it('should render as readonly', () => {
    render(
      <ReactHookFormProvider>
        <LevelField isReadOnly />
      </ReactHookFormProvider>,
    );

    const button = screen.getByRole('button', { name: /Level/i, hidden: true });
    expect(button).toBeTruthy();
    expect(button.getAttribute('readonly')).toBeDefined();
  });
});
