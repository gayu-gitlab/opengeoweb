/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { MenuItem, Theme } from '@mui/material';
import {
  ReactHookFormSelect,
  useDraftFormHelpers,
} from '@opengeoweb/form-fields';
import { useFormContext } from 'react-hook-form';

export const LABEL_LEVEL = 'Level';

export const levelOptions = [
  {
    title: 'Moderate',
    key: 'moderate',
    color: '#FAE21E',
    textColor: '#051039',
  },
  { title: 'Severe', key: 'severe', color: '#E27000', textColor: '#051039' },
  { title: 'Extreme', key: 'extreme', color: '#D62A20', textColor: '#ffffff' },
];

export const LevelField: React.FC<{
  isDisabled?: boolean;
  isReadOnly?: boolean;
}> = ({ isDisabled = false, isReadOnly = false }) => {
  const { isRequired } = useDraftFormHelpers();
  const { watch } = useFormContext();

  const selectedLevel = watch('level');
  const levelDetails = levelOptions.find(
    (level) => level.key === selectedLevel,
  );

  return (
    <ReactHookFormSelect
      name="level"
      label={LABEL_LEVEL}
      rules={{ validate: { isRequired } }}
      sx={{
        '& .MuiSelect-select.MuiSelect-filled': {
          color: levelDetails?.textColor || 'none',
          backgroundColor: levelDetails?.color || 'none',
          backgroundClip: 'content-box',
        },
      }}
      disabled={isDisabled || isReadOnly}
      isReadOnly={isReadOnly}
    >
      {levelOptions.map((level) => (
        <MenuItem
          value={level.key}
          key={level.key}
          sx={{
            '&.MuiMenuItem-root ': {
              color: level.textColor,
              background: level.color,
              backgroundClip: 'content-box',
              padding: '12px 8px',
              '&:hover': {
                backgroundColor: `${level.color}`,
                backgroundClip: 'content-box',
                boxShadow: (theme: Theme): string =>
                  `inset -8px -12px ${theme.palette.geowebColors.cards.cardContainerMouseOver}, inset 8px 12px ${theme.palette.geowebColors.cards.cardContainerMouseOver}`,
              },
              '&.Mui-selected': {
                backgroundColor: `${level.color}`,
                '&:hover': {
                  boxShadow: (theme: Theme): string =>
                    `inset 4px 0px 0px 0px ${theme.palette.geowebColors.typographyAndIcons.iconLinkActive}, inset -8px -12px ${theme.palette.geowebColors.cards.cardContainerMouseOver}, inset 8px 12px ${theme.palette.geowebColors.cards.cardContainerMouseOver}`,
                },
              },
            },
          }}
        >
          &nbsp;{level.title}
        </MenuItem>
      ))}
    </ReactHookFormSelect>
  );
};
