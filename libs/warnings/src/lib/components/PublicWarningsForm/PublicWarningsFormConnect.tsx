/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import { useDispatch, useSelector } from 'react-redux';
import { useAuthenticationContext } from '@opengeoweb/authentication';
import { PublicWarningsForm } from './PublicWarningsForm';
import { WarningModuleStore } from '../../store/config';
import {
  getPublicWarning,
  getPublicWarningFormState,
  getPublicWarningFormError,
  getPublicWarningObject,
  getPublicWarningEditor,
} from '../../store/publicWarningForm/selectors';
import { publicWarningFormActions } from '../../store/publicWarningForm/reducer';
import { PublicWarningDetail } from '../../store/publicWarningForm/types';

export const PublicWarningsFormConnect: React.FC = () => {
  const publicWarning = useSelector((store: WarningModuleStore) =>
    getPublicWarning(store),
  );
  const publicWarningObject = useSelector((store: WarningModuleStore) =>
    getPublicWarningObject(store),
  );

  const publicWarningFormAction = useSelector((store: WarningModuleStore) =>
    getPublicWarningFormState(store),
  );
  const publicWarningFormError = useSelector((store: WarningModuleStore) =>
    getPublicWarningFormError(store),
  );

  const publicWarningEditor = useSelector((store: WarningModuleStore) =>
    getPublicWarningEditor(store),
  );
  const { auth } = useAuthenticationContext();
  const isEditor = publicWarningEditor === auth?.username || false;

  const dispatch = useDispatch();

  const publishForm = (newFormValues: PublicWarningDetail): void => {
    dispatch(
      publicWarningFormActions.publishWarning({
        publicWarning: { warningDetail: newFormValues, status: 'PUBLISHED' },
      }),
    );
  };

  const saveForm = (newFormValues: PublicWarningDetail): void => {
    dispatch(
      publicWarningFormActions.saveWarning({
        publicWarning: {
          warningDetail: newFormValues,
          status: 'DRAFT',
          editor: auth?.username,
        },
      }),
    );
  };

  const onFormDirty = (isFormDirty: boolean): void => {
    dispatch(
      publicWarningFormActions.setFormDirty({
        isFormDirty,
      }),
    );
  };

  // Readonly also in case you are not creating a new warning and you are not set as the editor
  const isReadOnly =
    publicWarningFormAction === 'readonly' ||
    (!isEditor && publicWarning?.status !== undefined);

  return (
    <PublicWarningsForm
      object={publicWarningObject}
      publicWarningDetails={publicWarning?.warningDetail}
      onPublishForm={publishForm}
      onSaveForm={saveForm}
      formAction={publicWarningFormAction}
      isReadOnly={isReadOnly}
      error={publicWarningFormError}
      onFormDirty={onFormDirty}
    />
  );
};
