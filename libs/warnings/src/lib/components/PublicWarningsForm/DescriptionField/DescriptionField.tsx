/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { Tabs, Tab, MenuItem, FormControl, SxProps } from '@mui/material';
import {
  ReactHookFormSelect,
  ReactHookFormTextField,
} from '@opengeoweb/form-fields';
import React from 'react';

const LABEL_DESCRIPTION_ORIGINAL = 'Description original';
const LABEL_DESCRIPTION_TRANSLATION = 'Description translation';

function a11yProps(index: number): { id: string; 'aria-controls': string } {
  return {
    id: `tab-${index}`,
    'aria-controls': `tabpanel-${index}`,
  };
}

const tabStyle: SxProps = {
  width: '50%',
  fontSize: '0.875',
  justifyContent: 'flex-end',
  color: 'geowebColors.typographyAndIcons.text',
  maxWidth: 'initial',
  '&.Mui-selected.Mui-disabled': {
    color: 'text.disabled',
  },
};

const CustomTabPanel: React.FC<{
  children: React.ReactNode;
  value: number;
  index: number;
  isDisabled?: boolean;
  isReadOnly?: boolean;
}> = (props) => {
  const {
    children,
    value,
    index,
    isDisabled = false,
    isReadOnly = false,
    ...other
  } = props;
  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      aria-labelledby={`tab-${index}`}
      style={{ position: 'relative' }}
      {...other}
    >
      {!isReadOnly && (
        <FormControl
          variant="standard"
          sx={{
            position: 'absolute',
            zIndex: 1,
            width: '50%',
            maxWidth: '360px',
            paddingLeft: 1.5,
            '.MuiInputBase-root': {
              marginTop: 0,
              fontSize: '0.75rem',
            },
            '.MuiInputBase-formControl:before': {
              borderBottom: 'none!important',
            },
          }}
        >
          <ReactHookFormSelect
            variant="standard"
            name="translation-select"
            value="NL"
            rules={{}}
            sx={{
              color: 'geowebColors.textInputField.label.rgba',
            }}
            disabled={isDisabled}
          >
            <MenuItem value="NL">Dutch (selected)</MenuItem>
          </ReactHookFormSelect>
        </FormControl>
      )}
      {value === index ? children : null}
    </div>
  );
};

export const DescriptionField: React.FC<{
  isDisabled?: boolean;
  isReadOnly?: boolean;
}> = ({ isDisabled = false, isReadOnly = false }) => {
  const [value, setValue] = React.useState(0);

  const handleChange = (
    event: React.SyntheticEvent,
    newValue: number,
  ): void => {
    setValue(newValue);
  };
  return (
    <>
      <Tabs
        value={value}
        onChange={handleChange}
        aria-label="description tabs"
        {...(isDisabled && {
          TabIndicatorProps: {
            style: { display: 'none' },
            'aria-disabled': true,
          },
        })}
      >
        <Tab
          sx={tabStyle}
          label={LABEL_DESCRIPTION_ORIGINAL}
          disabled={isDisabled}
          {...a11yProps(0)}
        />

        <Tab
          sx={tabStyle}
          label={LABEL_DESCRIPTION_TRANSLATION}
          disabled={isDisabled}
          {...a11yProps(1)}
        />
      </Tabs>

      <CustomTabPanel
        value={value}
        index={0}
        isDisabled={isDisabled}
        isReadOnly={isReadOnly}
      >
        <ReactHookFormTextField
          rules={{}}
          minRows={4}
          name="descriptionOriginal"
          multiline
          disabled={isDisabled}
          isReadOnly={isReadOnly}
        />
      </CustomTabPanel>
      <CustomTabPanel
        value={value}
        index={1}
        isDisabled={isDisabled}
        isReadOnly={isReadOnly}
      >
        <ReactHookFormTextField
          rules={{}}
          minRows={4}
          name="descriptionTranslation"
          multiline
          disabled={isDisabled}
          isReadOnly={isReadOnly}
        />
      </CustomTabPanel>
    </>
  );
};
