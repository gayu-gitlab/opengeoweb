/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { Paper, Theme } from '@mui/material';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import React from 'react';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';
import { WarningsThemeProvider } from '../../Providers/Providers';
import { DescriptionField } from './DescriptionField';

export default {
  title: 'components/PublicWarningsForm/DescriptionField',
};

const Demo: React.FC<{ theme?: Theme; isDisabled?: boolean }> = ({
  theme = lightTheme,
  isDisabled = false,
}): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={theme}>
      <ReactHookFormProvider>
        <Paper sx={{ width: '500px', padding: 2 }}>
          <DescriptionField isDisabled={isDisabled} />
        </Paper>
      </ReactHookFormProvider>
    </WarningsThemeProvider>
  );
};

export const DescriptionFieldLight = (): React.ReactElement => <Demo />;
DescriptionFieldLight.storyName = 'DescriptionField light';

export const DescriptionFieldDark = (): React.ReactElement => (
  <Demo theme={darkTheme} />
);
DescriptionFieldDark.storyName = 'DescriptionField dark';

export const DescriptionFieldDisabledLight = (): React.ReactElement => (
  <Demo isDisabled />
);
DescriptionFieldDisabledLight.storyName = 'DescriptionField disabled light';

export const DescriptionFieldDisabledDark = (): React.ReactElement => (
  <Demo theme={darkTheme} isDisabled />
);
DescriptionFieldDisabledDark.storyName = 'DescriptionField disabled dark';
