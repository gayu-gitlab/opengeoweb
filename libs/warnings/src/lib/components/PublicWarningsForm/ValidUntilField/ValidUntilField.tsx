/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { useFormContext } from 'react-hook-form';
import {
  ReactHookFormDateTime,
  isEmpty,
  isValidDate,
  isXHoursAfter,
  isXHoursBefore,
  useDraftFormHelpers,
} from '@opengeoweb/form-fields';
import { dateUtils } from '@opengeoweb/shared';
import { DATE_FORMAT_DATE_FIELDS } from '../../../utils/constants';

export const LABEL_VALID_UNTIL = 'Valid until';

const MAX_HOURS_AFTER_CURRENT = 180;
const MIN_HOURS_AFTER_VALID_FROM = 1;
export const VALID_UNTIL_ERROR_BEFORE_CURRENT =
  'Valid until time cannot be before current time';
export const VALID_UNTIL_ERROR_AFTER_CURRENT = `Valid until time can be no more than ${MAX_HOURS_AFTER_CURRENT} hours after current time`;
export const VALID_UNTIL_ERROR_AFTER_VALID_FROM = `Valid until time has to be at least ${MIN_HOURS_AFTER_VALID_FROM} hour after Valid from time`;

export const isValueBeforeCurrentTime = (value: string): boolean | string =>
  isXHoursBefore(value, dateUtils.getCurrentTimeAsString(), 0) ||
  VALID_UNTIL_ERROR_BEFORE_CURRENT;

export const isMaxHoursAfterCurrentTime = (value: string): boolean | string =>
  isXHoursAfter(
    value,
    dateUtils.getCurrentTimeAsString(),
    MAX_HOURS_AFTER_CURRENT,
  ) || VALID_UNTIL_ERROR_AFTER_CURRENT;

export const isMinHoursAfterValidFrom = (
  value: string,
  validFromValue: string,
): boolean | string => {
  if (
    isEmpty(value) ||
    isEmpty(validFromValue) ||
    !isValidDate(validFromValue)
  ) {
    return true;
  }
  const duration = dateUtils.differenceInHours(
    new Date(value),
    new Date(validFromValue),
  );
  return (
    duration >= MIN_HOURS_AFTER_VALID_FROM || VALID_UNTIL_ERROR_AFTER_VALID_FROM
  );
};

export const ValidUntil: React.FC<{
  isDisabled?: boolean;
  isReadOnly?: boolean;
}> = ({ isDisabled, isReadOnly }) => {
  const { isRequired } = useDraftFormHelpers();
  const { getValues } = useFormContext();

  return (
    <ReactHookFormDateTime
      name="validUntil"
      rules={{
        validate: {
          isRequired,
          isValidDate,
          // The valid until time cannot be before the current time
          isValueBeforeCurrentTime,
          // Valid until time can be no more than X hours after current time
          isMaxHoursAfterCurrentTime,
          // Valid until time has to be at least X hours after valid from time
          isMinHoursAfterValidFrom: (value): boolean | string =>
            isMinHoursAfterValidFrom(value, getValues('validFrom')),
        },
      }}
      label={LABEL_VALID_UNTIL}
      format={DATE_FORMAT_DATE_FIELDS}
      disablePast
      disabled={isDisabled || isReadOnly}
      isReadOnly={isReadOnly}
    />
  );
};
