/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  VALID_UNTIL_ERROR_AFTER_CURRENT,
  VALID_UNTIL_ERROR_AFTER_VALID_FROM,
  VALID_UNTIL_ERROR_BEFORE_CURRENT,
  isMaxHoursAfterCurrentTime,
  isMinHoursAfterValidFrom,
  isValueBeforeCurrentTime,
} from './ValidUntilField';

describe('components/PublicWarningsForm/PublicWarningsForm/ValidUntilField', () => {
  beforeEach(() => {
    jest.useFakeTimers().setSystemTime(new Date('2023-12-20T14:00:00Z'));
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });
  describe('isValueBeforeCurrentTime', () => {
    it('should return error when value is before current time', () => {
      expect(isValueBeforeCurrentTime('2023-12-20T13:59:00Z')).toEqual(
        VALID_UNTIL_ERROR_BEFORE_CURRENT,
      );
    });
    it('should return true when value is equal to current time', () => {
      expect(isValueBeforeCurrentTime('2023-12-20T14:00:00Z')).toEqual(true);
    });
    it('should return true when value is after current time', () => {
      expect(isValueBeforeCurrentTime('2023-12-20T14:01:00Z')).toEqual(true);
    });
  });

  describe('isMaxHoursAfterCurrentTime', () => {
    it('should return error when value is more than 180 hours after current time', () => {
      expect(isMaxHoursAfterCurrentTime('2023-12-28T02:01:00Z')).toEqual(
        VALID_UNTIL_ERROR_AFTER_CURRENT,
      );
    });
    it('should return true when value is exactly 180 hours after current time', () => {
      expect(isMaxHoursAfterCurrentTime('2023-12-28T02:00:00Z')).toEqual(true);
    });
    it('should return true when value less than 180 hours after current time', () => {
      expect(isMaxHoursAfterCurrentTime('2023-12-28T01:59:00Z')).toEqual(true);
    });
  });

  describe('isOneHourAfterValidFrom', () => {
    it('should return true when exactly 1 hour difference', () => {
      const validFrom = '2020-09-17T13:00:00Z';
      const validUntil = '2020-09-17T14:00:00Z';
      expect(isMinHoursAfterValidFrom(validUntil, validFrom)).toEqual(true);
    });

    it('should return true when more than 1 hour difference', () => {
      const validFrom = '2020-09-17T13:00:00Z';
      const validUntil = '2020-09-17T14:01:00Z';
      expect(isMinHoursAfterValidFrom(validUntil, validFrom)).toEqual(true);
    });

    it('should return error when less than 1 hour difference', () => {
      const validFrom = '2020-09-17T13:00:00Z';
      const validUntil = '2020-09-17T13:59:00Z';
      expect(isMinHoursAfterValidFrom(validUntil, validFrom)).toEqual(
        VALID_UNTIL_ERROR_AFTER_VALID_FROM,
      );
    });

    it('should return true when values are empty', () => {
      const validFrom = '2020-09-17T13:00:00Z';
      const validUntil = '2020-09-17T14:00:00Z';
      expect(isMinHoursAfterValidFrom('', '')).toEqual(true);
      expect(isMinHoursAfterValidFrom(validUntil, '')).toEqual(true);
      expect(isMinHoursAfterValidFrom('', validFrom)).toEqual(true);
    });
  });
});
