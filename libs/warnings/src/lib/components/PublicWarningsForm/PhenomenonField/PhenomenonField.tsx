/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Grid, ListItemIcon, MenuItem } from '@mui/material';
import {
  ReactHookFormSelect,
  useDraftFormHelpers,
} from '@opengeoweb/form-fields';
import {
  CoastalEvent,
  Fog,
  FunnelCloud,
  Lightning,
  Rain,
  Snow,
  TemperatureHigh,
  TemperatureLow,
  Wind,
} from '@opengeoweb/theme';

export const LABEL_PHENOMENON = 'Phenomenon';

export const warningPhenomena = [
  { title: 'Wind', key: 'wind', icon: <Wind /> },
  { title: 'Fog', key: 'fog', icon: <Fog /> },
  { title: 'Thunderstorm', key: 'thunderstorm', icon: <Lightning /> },
  { title: 'Snow/ice', key: 'snowIce', icon: <Snow /> },
  { title: 'Rain', key: 'rain', icon: <Rain /> },
  { title: 'High temperature', key: 'highTemp', icon: <TemperatureHigh /> },
  { title: 'Funnel cloud', key: 'funnelCloud', icon: <FunnelCloud /> },
  { title: 'Low temperature', key: 'lowTemp', icon: <TemperatureLow /> },
  { title: 'Coastal event', key: 'coastalEvent', icon: <CoastalEvent /> },
];

export const Phenomenon: React.FC<{
  isDisabled?: boolean;
  isReadOnly?: boolean;
}> = ({ isDisabled, isReadOnly }) => {
  const { isRequired } = useDraftFormHelpers();
  return (
    <ReactHookFormSelect
      name="phenomenon"
      label={LABEL_PHENOMENON}
      rules={{ validate: { isRequired } }}
      disabled={isDisabled || isReadOnly}
      isReadOnly={isReadOnly}
    >
      {warningPhenomena.map((phenomenon) => {
        return (
          <MenuItem key={phenomenon.key} value={phenomenon.key}>
            <Grid container justifyContent="space-between">
              <Grid item>{phenomenon.title}</Grid>
              <Grid item>
                <ListItemIcon>{phenomenon.icon}</ListItemIcon>
              </Grid>
            </Grid>
          </MenuItem>
        );
      })}
    </ReactHookFormSelect>
  );
};
