/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { Paper } from '@mui/material';
import * as React from 'react';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import { createStore } from '@redux-eggs/redux-toolkit';
import { useDispatch } from 'react-redux';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { PublicWarningsFormConnect } from './PublicWarningsFormConnect';
import { publicWarningFormActions } from '../../store/publicWarningForm/reducer';
import {
  testGeoJSON,
  fakeErrorRequest,
  fakePostError,
} from '../../storybookUtils/testUtils';
import { createApi as createFakeApi } from '../../utils/fakeApi';
import { WarningsApi } from '../../utils/api';

export default {
  title: 'components/PublicWarningsFormConnect/errors',
};
const store = createStore({
  extensions: [getSagaExtension()],
});

const Demo: React.FC = () => {
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(
      publicWarningFormActions.setFormValues({
        object: {
          id: '923723984872338768743',
          objectName: 'Drawing object 101',
          lastUpdatedTime: '2022-06-01T12:34:27Z',
          scope: 'user',
          geoJSON: testGeoJSON,
        },
        publicWarning: {
          warningDetail: {
            id: '923723984872338768743',
            phenomenon: 'coastalEvent',
            validFrom: '2022-06-01T15:00:00Z',
            validUntil: '2022-06-01T18:00:00Z',
            level: 'extreme',
            probability: 80,
            descriptionOriginal:
              'Some pretty intense coastal weather is coming our way',
            descriptionTranslation: 'And this would be the translation',
            geoJSON: testGeoJSON,
          },
          status: 'DRAFT',
        },
      }),
    );
  }, [dispatch]);
  return <PublicWarningsFormConnect />;
};

export const SaveError = (): React.ReactElement => {
  const fakeApiWithErrors = (): WarningsApi => ({
    ...createFakeApi(),
    updateWarning: () => fakeErrorRequest<void>(fakePostError),
  });

  return (
    <WarningsThemeStoreProvider store={store} createApi={fakeApiWithErrors}>
      <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
        <Demo />
      </Paper>
    </WarningsThemeStoreProvider>
  );
};

export const PublishError = (): React.ReactElement => {
  const fakeApiWithErrors = (): WarningsApi => ({
    ...createFakeApi(),
    saveWarningAs: () => fakeErrorRequest<string>(fakePostError),
  });

  return (
    <WarningsThemeStoreProvider store={store} createApi={fakeApiWithErrors}>
      <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
        <Demo />
      </Paper>
    </WarningsThemeStoreProvider>
  );
};
