/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  Box,
  Button,
  CircularProgress,
  Grid,
  MenuItem,
  SxProps,
  styled,
} from '@mui/material';
import {
  ReactHookFormHiddenInput,
  ReactHookFormProvider,
  ReactHookFormSelect,
  defaultFormOptions,
  useDraftFormHelpers,
} from '@opengeoweb/form-fields';
import { breakpoints } from '@opengeoweb/theme';
import * as React from 'react';
import { AlertBanner, dateUtils } from '@opengeoweb/shared';
import { useFormContext } from 'react-hook-form';
import { DescriptionField } from './DescriptionField';
import { AreaField } from './AreaField/AreaField';
import { DATE_FORMAT_UTC } from '../../utils/constants';
import { Phenomenon } from './PhenomenonField';
import { PublicWarningDetail } from '../../store/publicWarningForm/types';
import {
  BaseDrawingListItem,
  DrawingListItem,
} from '../../store/drawings/types';
import { LevelField } from './LevelField';
import { ValidFrom } from './ValidFromField';
import { ValidUntil } from './ValidUntilField';
import { FormAction, FormError } from '../../store/publicWarningForm/reducer';
import { useFormDirty } from '../../utils/useFormDirty';

export const getEmptyPublicWarning = (): PublicWarningDetail => {
  const currentTime = new Date();
  const validFromTime = dateUtils.add(currentTime, { minutes: 15 });
  const validUntilTime = dateUtils.add(currentTime, { hours: 6, minutes: 15 });

  return {
    id: '',
    phenomenon: '',
    validFrom: dateUtils.dateToString(validFromTime, DATE_FORMAT_UTC, true)!,
    validUntil: dateUtils.dateToString(validUntilTime, DATE_FORMAT_UTC, true)!,
    level: '',
    probability: 30,
    descriptionOriginal: '',
    descriptionTranslation: '',
    geoJSON: null!,
  };
};

export const getFormData = (
  publicWarningsDetails: PublicWarningDetail | undefined,
  object?: DrawingListItem,
): PublicWarningDetail => {
  const data = {
    ...(publicWarningsDetails
      ? {
          ...getEmptyPublicWarning(),
          ...publicWarningsDetails,
        }
      : getEmptyPublicWarning()),
    object,
  };

  return data;
};

// tablet breakpoint - 2 padding * 2 for both sides
const containerQueryBreakpoint = `@container (min-width: ${
  breakpoints.tablet - 32
}px)`;
const spinnerSx: SxProps = {
  width: '16px!important',
  height: '16px!important',
  position: 'absolute',
  top: 0,
  bottom: 0,
  margin: 'auto',
  marginLeft: 1,
};

export const LABEL_PROBABILITY = 'Probability';
export const BUTTON_CLEAR = 'Clear';
export const BUTTON_SAVE = 'Save';
export const BUTTON_SAVING = 'Saving';
export const BUTTON_PUBLISH = 'Publish';
export const BUTTON_PUBLISHING = 'Publishing';

const probablityOptions = [100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 0];

const ContainerWrapper = styled('div')(() => ({
  containerType: 'size',
  height: '100%',
}));

const ContainerFormWrapper = styled('div')<{ isReadOnly: boolean }>(
  ({ isReadOnly = false, theme }) => ({
    padding: theme.spacing(2),
    height: isReadOnly ? '100%' : `calc(100% - 206px)`,
    overflowY: 'auto',
    [containerQueryBreakpoint]: {
      height: 'calc(100% - 88px)',
    },
  }),
);

const ContainerGrid = styled('div')(({ theme }) => ({
  backgroundColor: theme.palette.background.paper,
  '.MuiGrid-grid-sm-6, .MuiGrid-grid-sm-4': {
    width: '100%',
    minWidth: '100%',
  },
  [containerQueryBreakpoint]: {
    '.MuiGrid-grid-sm-6': {
      width: '50%',
      minWidth: '50%',
    },
    '.MuiGrid-grid-sm-4': {
      width: '33.33%',
      minWidth: '33.33%',
    },
  },
}));

interface FormData extends PublicWarningDetail {
  IS_DRAFT?: boolean;
  object?: BaseDrawingListItem;
  'translation-select'?: string;
}

export const prepareFormValues = (data: FormData): PublicWarningDetail => {
  // remove unneeded fields
  const {
    IS_DRAFT,
    object,
    'translation-select': translationSelect,
    ...productToPost
  } = data;
  // remove empty fields
  return Object.keys(productToPost).reduce<PublicWarningDetail>(
    (
      warningDetail: PublicWarningDetail,
      fieldName: keyof PublicWarningDetail,
    ) => {
      const fieldValue = productToPost[fieldName];
      if (fieldValue) {
        return {
          ...warningDetail,
          [fieldName]: fieldValue,
        };
      }
      return warningDetail;
    },
    {} as PublicWarningDetail,
  );
};

interface FormProps {
  formAction?: FormAction;
  onSaveForm?: (formValues: PublicWarningDetail) => void;
  onPublishForm?: (formValues: PublicWarningDetail) => void;
  object?: DrawingListItem;
  publicWarningDetails?: PublicWarningDetail;
  error?: FormError;
  isReadOnly?: boolean;
  onFormDirty?: (isFormDirty: boolean) => void;
}

interface FormErrors {
  title: string;
  errors: React.ReactElement[];
}

const getErrors = (errors: string): null | FormErrors =>
  errors
    ? errors?.split(/\r?\n/).reduce<FormErrors>(
        (allErrors, error, index) => {
          if (index === 0) {
            return {
              ...allErrors,
              title: error,
            };
          }
          const isEven = index % 2 === 0;

          return {
            ...allErrors,
            errors: allErrors.errors.concat(
              // eslint-disable-next-line react/no-array-index-key
              <p key={`error-${index}`} style={{ fontSize: 14 }}>
                {isEven ? error : <i>{error}</i>}
              </p>,
            ),
          };
        },
        { title: '', errors: [] },
      )
    : null;

const Form: React.FC<FormProps> = ({
  onSaveForm = (): void => {},
  onPublishForm = (): void => {},
  object,
  formAction = '',
  publicWarningDetails,
  error = { severity: 'error', message: '' },
  isReadOnly = false,
  onFormDirty = (): void => {},
}) => {
  const {
    handleSubmit,
    reset,
    formState: { isDirty },
  } = useFormContext();
  const { toggleIsDraft, DraftFieldHelper } = useDraftFormHelpers();

  const isPublishing = formAction === 'publishing';
  const isSaving = formAction === 'saving';
  const areFieldsDisabled = isPublishing || isSaving;

  useFormDirty({
    isDirty,
    isLoading: isPublishing || isSaving,
    error: error?.message,
    onFormDirty,
    onSuccess: () => {
      // reset form dirty after successfull saving
      reset({}, { keepValues: true });
    },
  });

  const onPressPublish = (): void => {
    if (!isPublishing) {
      toggleIsDraft(false);
      handleSubmit(async (data: PublicWarningDetail) => {
        onPublishForm(prepareFormValues(data));
      })();
    }
  };

  const onPressSave = (): void => {
    if (!isSaving) {
      toggleIsDraft(true);
      handleSubmit(async (data: PublicWarningDetail) => {
        onSaveForm(prepareFormValues(data));
      })();
    }
  };

  const onPressClear = (): void => {
    reset(getEmptyPublicWarning());
  };

  React.useEffect(() => {
    // sync redux with react-hook-form
    reset(getFormData(publicWarningDetails, object));
  }, [object, reset, publicWarningDetails]);

  const errors = getErrors(error.message);

  return (
    <ContainerWrapper>
      <ContainerFormWrapper isReadOnly={isReadOnly}>
        <ContainerGrid>
          {errors && (
            <Box sx={{ marginBottom: 2 }}>
              <AlertBanner
                title={errors.title}
                info={errors.errors as unknown as React.ReactElement}
                shouldClose
                severity={error?.severity}
              />
            </Box>
          )}
          <Grid container rowSpacing={2}>
            <Grid item xs={12}>
              <Phenomenon
                isDisabled={areFieldsDisabled}
                isReadOnly={isReadOnly}
              />
            </Grid>

            <Grid item xs={12}>
              <Grid item xs={12}>
                <AreaField
                  isDisabled
                  onEditObject={(): void => {}}
                  onViewObject={(): void => {}}
                  onDeleteObject={(): void => {}}
                  onAddObject={(): void => {}}
                />
              </Grid>
            </Grid>

            <Grid item xs={12} container spacing={2}>
              <Grid item xs={12} sm={6}>
                <ValidFrom
                  isDisabled={areFieldsDisabled}
                  isReadOnly={isReadOnly}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <ValidUntil
                  isDisabled={areFieldsDisabled}
                  isReadOnly={isReadOnly}
                />
              </Grid>
            </Grid>

            <Grid item xs={12} container spacing={2}>
              <Grid item xs={12} sm={6}>
                <LevelField
                  isDisabled={areFieldsDisabled}
                  isReadOnly={isReadOnly}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <ReactHookFormSelect
                  name="probability"
                  label={LABEL_PROBABILITY}
                  rules={{}}
                  disabled={areFieldsDisabled || isReadOnly}
                  isReadOnly={isReadOnly}
                >
                  {probablityOptions.map((probability) => (
                    <MenuItem value={probability} key={probability}>
                      {probability} %
                    </MenuItem>
                  ))}
                </ReactHookFormSelect>
              </Grid>
            </Grid>

            <Grid item xs={12} container spacing={2}>
              <Grid item xs={12}>
                <DescriptionField
                  isDisabled={areFieldsDisabled}
                  isReadOnly={isReadOnly}
                />
              </Grid>
            </Grid>
          </Grid>
        </ContainerGrid>
        <DraftFieldHelper />
        <ReactHookFormHiddenInput name="id" />
      </ContainerFormWrapper>
      {!isReadOnly && (
        <Box
          sx={{
            padding: '24px 10px 14px 10px',
            position: 'absolute',
            bottom: 0,
            width: '100%',
            backgroundColor: 'geowebColors.background.surfaceApp',
            boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.5)',
            zIndex: 1,
          }}
        >
          <ContainerGrid>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={4}>
                <Button
                  variant="tertiary"
                  sx={{
                    width: '100%',
                    marginBottom: 1,
                  }}
                  disabled={areFieldsDisabled}
                  onClick={onPressClear}
                >
                  {BUTTON_CLEAR}
                </Button>
              </Grid>
              <Grid item xs={12} sm={4}>
                <Button
                  variant="tertiary"
                  sx={{
                    width: '100%',
                    marginBottom: 1,
                  }}
                  disabled={isPublishing}
                  onClick={onPressSave}
                >
                  <span style={{ position: 'relative' }}>
                    {isSaving ? BUTTON_SAVING : BUTTON_SAVE}
                    {isSaving && (
                      <CircularProgress
                        data-testid="spinner"
                        color="inherit"
                        sx={spinnerSx}
                      />
                    )}
                  </span>
                </Button>
              </Grid>
              <Grid item xs={12} sm={4}>
                <Button
                  variant="primary"
                  sx={{
                    width: '100%',
                  }}
                  disabled={isSaving}
                  onClick={onPressPublish}
                >
                  <span style={{ position: 'relative' }}>
                    {isPublishing ? BUTTON_PUBLISHING : BUTTON_PUBLISH}
                    {isPublishing && (
                      <CircularProgress
                        data-testid="spinner"
                        color="inherit"
                        sx={spinnerSx}
                      />
                    )}
                  </span>
                </Button>
              </Grid>
            </Grid>
          </ContainerGrid>
        </Box>
      )}
    </ContainerWrapper>
  );
};

export const PublicWarningsForm: React.FC<FormProps> = (props) => {
  const { object, publicWarningDetails } = props;
  const defaultValues = getFormData(publicWarningDetails, object);

  return (
    <ReactHookFormProvider
      options={{
        ...defaultFormOptions,
        defaultValues,
      }}
    >
      <Form {...props} />
    </ReactHookFormProvider>
  );
};
