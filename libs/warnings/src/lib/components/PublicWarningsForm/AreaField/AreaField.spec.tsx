/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { fireEvent, render, screen } from '@testing-library/react';
import React from 'react';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';
import {
  AreaField,
  BUTTON_ADD_OBJECT,
  BUTTON_DELETE,
  BUTTON_EDIT,
  BUTTON_VIEW,
  LABEL_AREA,
  OBJECTS_MENU,
  OBJECT_SELECTED,
  formatDate,
} from './AreaField';
import { testGeoJSON } from '../../../storybookUtils/testUtils';

describe('components/PublicWarningsForm/AreaField', () => {
  const props = {
    onEditObject: jest.fn(),
    onViewObject: jest.fn(),
    onDeleteObject: jest.fn(),
    onAddObject: jest.fn(),
  };

  const object = {
    id: 'test',
  };

  const defaultValues = {
    object,
    geoJSON: testGeoJSON,
  };

  it('renders the component with default props', () => {
    const defaultProps = {
      onEditObject: jest.fn(),
      onViewObject: jest.fn(),
      onDeleteObject: jest.fn(),
      onAddObject: jest.fn(),
    };
    render(
      <ReactHookFormProvider>
        <AreaField {...defaultProps} />
      </ReactHookFormProvider>,
    );

    // Check if the component renders with default labels
    expect(screen.getByText(LABEL_AREA)).toBeInTheDocument();
    expect(screen.getByText(BUTTON_ADD_OBJECT)).toBeInTheDocument();
    expect(screen.queryByRole('button', { name: OBJECTS_MENU })).toBeFalsy();
  });

  it('renders the area field as disabled', () => {
    render(
      <ReactHookFormProvider options={{ defaultValues }}>
        <AreaField {...props} isDisabled />
      </ReactHookFormProvider>,
    );

    // Check if the component renders with default labels
    expect(screen.getByText(LABEL_AREA)).toBeInTheDocument();
    expect(screen.getByText(BUTTON_ADD_OBJECT)).toBeInTheDocument();
    fireEvent.click(screen.getByRole('button', { name: /Object options/i }));
    expect(screen.queryByText(OBJECTS_MENU)).toBeFalsy();

    fireEvent.click(screen.getByText(BUTTON_ADD_OBJECT));
    expect(props.onAddObject).not.toHaveBeenCalled();
  });

  it('should edit an object', () => {
    render(
      <ReactHookFormProvider options={{ defaultValues }}>
        <AreaField {...props} />
      </ReactHookFormProvider>,
    );
    fireEvent.click(screen.getByRole('button', { name: /Object options/i }));
    expect(screen.getByText(OBJECTS_MENU)).toBeTruthy();
    fireEvent.click(screen.getByText(BUTTON_EDIT));
    expect(props.onEditObject).toHaveBeenCalled();
  });

  it('should view an object', () => {
    render(
      <ReactHookFormProvider options={{ defaultValues }}>
        <AreaField {...props} />
      </ReactHookFormProvider>,
    );
    fireEvent.click(screen.getByRole('button', { name: /Object options/i }));
    expect(screen.getByText(OBJECTS_MENU)).toBeTruthy();
    fireEvent.click(screen.getByText(BUTTON_VIEW));
    expect(props.onViewObject).toHaveBeenCalled();
  });

  it('should delete an object', () => {
    render(
      <ReactHookFormProvider options={{ defaultValues }}>
        <AreaField {...props} />
      </ReactHookFormProvider>,
    );
    fireEvent.click(screen.getByRole('button', { name: /Object options/i }));
    expect(screen.getByText(OBJECTS_MENU)).toBeTruthy();
    fireEvent.click(screen.getByText(BUTTON_DELETE));
    expect(props.onDeleteObject).toHaveBeenCalled();
  });

  it('should add an object', () => {
    render(
      <ReactHookFormProvider>
        <AreaField {...props} />
      </ReactHookFormProvider>,
    );
    fireEvent.click(screen.getByText(BUTTON_ADD_OBJECT));
    expect(props.onAddObject).toHaveBeenCalled();
  });

  it('should correct area values', () => {
    const testObject = {
      id: 'test',
      lastUpdatedTime: '2022-06-01T12:34:27Z',
      objectName: 'my test area',
    };

    render(
      <ReactHookFormProvider
        options={{
          defaultValues: { object: testObject, geoJSON: testGeoJSON },
        }}
      >
        <AreaField {...props} />
      </ReactHookFormProvider>,
    );

    expect(screen.getByText(testObject.objectName)).toBeTruthy();
    expect(
      screen.getByText(formatDate(testObject.lastUpdatedTime)),
    ).toBeTruthy();
    expect(screen.getByRole('button', { name: OBJECTS_MENU })).toBeTruthy();
  });

  it('should area with generic object name if no objectName is given', () => {
    const testObject = {
      id: 'test',
      lastUpdatedTime: '2022-06-01T12:34:27Z',
    };

    render(
      <ReactHookFormProvider
        options={{
          defaultValues: { object: testObject, geoJSON: testGeoJSON },
        }}
      >
        <AreaField {...props} />
      </ReactHookFormProvider>,
    );

    expect(screen.getByText(OBJECT_SELECTED)).toBeTruthy();
    expect(
      screen.getByText(formatDate(testObject.lastUpdatedTime)),
    ).toBeTruthy();
    expect(screen.getByRole('button', { name: OBJECTS_MENU })).toBeTruthy();
  });
});
