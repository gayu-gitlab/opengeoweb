/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { Button, Card, CardContent, Typography } from '@mui/material';
import { ToggleMenu, dateUtils } from '@opengeoweb/shared';
import { Edit, Visibility, Delete, Add } from '@opengeoweb/theme';
import React from 'react';
import {
  ReactHookFormHiddenInput,
  errorMessages,
  useDraftFormHelpers,
} from '@opengeoweb/form-fields';
import { useFormContext } from 'react-hook-form';
import { DATE_FORMAT } from '../../../utils/constants';

export const LABEL_AREA = 'Area';
export const BUTTON_ADD_OBJECT = 'Add object';
export const BUTTON_DELETE = 'Delete';
export const BUTTON_VIEW = 'View';
export const BUTTON_EDIT = 'Edit';
export const OBJECTS_MENU = 'Object options';
export const NO_OBJECT = '(no object selected)';
export const OBJECT_SELECTED = 'object selected';

export const formatDate = (lastUpdatedTime: string): string =>
  dateUtils.dateToString(
    dateUtils.isoStringToDate(lastUpdatedTime),
    `${DATE_FORMAT} 'UTC'`,
  )!;

const getObjectName = (
  geoJSON: GeoJSON.FeatureCollection,
  objectName: string,
): string => {
  if (geoJSON) {
    return objectName || OBJECT_SELECTED;
  }
  return NO_OBJECT;
};

export const AreaField: React.FC<{
  onEditObject: () => void;
  onViewObject: () => void;
  onDeleteObject: () => void;
  onAddObject: () => void;
  isDisabled?: boolean;
}> = ({
  onEditObject = (): void => {},
  onViewObject = (): void => {},
  onDeleteObject = (): void => {},
  onAddObject = (): void => {},
  isDisabled = false,
}) => {
  const {
    getValues,
    formState: { errors },
  } = useFormContext();
  const { isRequired } = useDraftFormHelpers();
  const lastUpdatedTime = getValues('object.lastUpdatedTime');
  const geoJSON = getValues('geoJSON');
  const objectName = getObjectName(geoJSON, getValues('object.objectName'));
  const hasErrors = errors?.geoJSON !== undefined;

  return (
    <>
      <Typography
        variant="h3"
        color="text.secondary"
        sx={{
          fontSize: '0.75rem',
          fontWeight: 'normal',
          lineHeight: 1.83,
          paddingLeft: 1.5,
          ...(hasErrors && {
            color: 'geowebColors.captions.captionError.color',
          }),
        }}
        {...(hasErrors && {
          'aria-errormessage': errorMessages.required,
        })}
      >
        {LABEL_AREA}
      </Typography>
      <Card elevation={0} sx={{ ...(!geoJSON && { background: 'none' }) }}>
        <CardContent
          sx={{
            position: 'relative',
            padding: 1,
            '&:last-child': { paddingBottom: 1 },
          }}
        >
          <Typography
            variant="h4"
            color="text.primary"
            sx={{
              fontSize: '0.75rem',
              fontWeight: 'normal',
              lineHeight: 1.83,
            }}
            role="heading"
          >
            {objectName}
          </Typography>
          <Typography variant="body2" color="text.secondary">
            {lastUpdatedTime ? formatDate(lastUpdatedTime) : ''}
          </Typography>

          {geoJSON ? (
            <ToggleMenu
              menuTitle={OBJECTS_MENU}
              tooltipTitle={OBJECTS_MENU}
              buttonSx={{
                position: 'absolute',
                right: 0,
                margin: 'auto',
                top: 0,
                bottom: 0,
                marginRight: '8px',
              }}
              menuPosition="bottom"
              isDisabled={isDisabled}
              menuItems={[
                {
                  text: BUTTON_EDIT,
                  action: onEditObject,
                  icon: <Edit />,
                },
                {
                  text: BUTTON_VIEW,
                  action: onViewObject,
                  icon: <Visibility />,
                },
                {
                  text: BUTTON_DELETE,
                  action: onDeleteObject,
                  icon: <Delete />,
                },
              ]}
            />
          ) : null}
        </CardContent>
      </Card>
      <ReactHookFormHiddenInput
        rules={{
          validate: { isRequired },
        }}
        name="geoJSON"
      />
      <Button
        sx={{ marginTop: 1 }}
        variant="tertiary"
        startIcon={<Add />}
        size="small"
        onClick={onAddObject}
        disabled={isDisabled}
      >
        {BUTTON_ADD_OBJECT}
      </Button>
    </>
  );
};
