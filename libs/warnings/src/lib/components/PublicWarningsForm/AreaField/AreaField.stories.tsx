/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { Paper } from '@mui/material';
import { darkTheme } from '@opengeoweb/theme';
import React from 'react';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';
import { WarningsThemeProvider } from '../../Providers/Providers';
import { AreaField } from './AreaField';

export default {
  title: 'components/PublicWarningsForm/AreaField',
};

const object = {
  id: '923723984872338768743',
  objectName: 'Drawing object 101',
  lastUpdatedTime: '2022-06-01T12:34:27Z',
};

const props = {
  // eslint-disable-next-line no-console
  onEditObject: (): void => console.log('edit'),
  // eslint-disable-next-line no-console
  onViewObject: (): void => console.log('view'),
  // eslint-disable-next-line no-console
  onDeleteObject: (): void => console.log('delete'),
  // eslint-disable-next-line no-console
  onAddObject: (): void => console.log('add'),
};

export const AreaFieldLight = (): React.ReactElement => {
  return (
    <WarningsThemeProvider>
      <ReactHookFormProvider options={{ defaultValues: { object } }}>
        <Paper sx={{ width: '500px', padding: 2 }}>
          <AreaField {...props} />
        </Paper>
      </ReactHookFormProvider>
    </WarningsThemeProvider>
  );
};
AreaFieldLight.storyName = 'AreaField light';

export const AreaFieldDark = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <ReactHookFormProvider options={{ defaultValues: { object } }}>
        <Paper sx={{ width: '500px', padding: 2 }}>
          <AreaField {...props} />
        </Paper>
      </ReactHookFormProvider>
    </WarningsThemeProvider>
  );
};
AreaFieldDark.storyName = 'AreaField dark';
