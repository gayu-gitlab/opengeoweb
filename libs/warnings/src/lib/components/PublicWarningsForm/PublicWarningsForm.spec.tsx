/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import {
  fireEvent,
  render,
  screen,
  waitFor,
  within,
} from '@testing-library/react';

import { dateUtils } from '@opengeoweb/shared';
import { errorMessages } from '@opengeoweb/form-fields';
import userEvent from '@testing-library/user-event';
import {
  BUTTON_CLEAR,
  BUTTON_PUBLISH,
  BUTTON_PUBLISHING,
  BUTTON_SAVE,
  BUTTON_SAVING,
  LABEL_PROBABILITY,
  PublicWarningsForm,
  getEmptyPublicWarning,
  prepareFormValues,
} from './PublicWarningsForm';
import { WarningsThemeProvider } from '../Providers/Providers';
import {
  LABEL_AREA,
  NO_OBJECT,
  OBJECTS_MENU,
  formatDate,
} from './AreaField/AreaField';
import {
  LABEL_PHENOMENON,
  warningPhenomena,
} from './PhenomenonField/PhenomenonField';
import { DATE_FORMAT } from '../../utils/constants';
import { DrawingListItem } from '../../store/drawings/types';
import { fakePostError, testGeoJSON } from '../../storybookUtils/testUtils';
import { LABEL_LEVEL, levelOptions } from './LevelField/LevelField';
import { PublicWarningDetail } from '../../store/publicWarningForm/types';
import {
  LABEL_VALID_FROM,
  VALID_FROM_ERROR_AFTER_CURRENT,
  VALID_FROM_ERROR_BEFORE_CURRENT,
} from './ValidFromField';
import {
  LABEL_VALID_UNTIL,
  VALID_UNTIL_ERROR_AFTER_CURRENT,
  VALID_UNTIL_ERROR_AFTER_VALID_FROM,
  VALID_UNTIL_ERROR_BEFORE_CURRENT,
} from './ValidUntilField';
import { BUTTON_EDIT } from '../PublicWarningList';

describe('components/PublicWarningsForm/PublicWarningsForm', () => {
  describe('prepareFormValues', () => {
    const object: DrawingListItem = {
      id: '923723984872338768743',
      objectName: 'Drawing object 101',
      lastUpdatedTime: '2022-06-01T12:34:27Z',
      scope: 'user',
      geoJSON: testGeoJSON,
    };
    const publicWarningDetails: PublicWarningDetail = {
      id: '923723984872338768743',
      phenomenon: 'coastalEvent',
      validFrom: '2022-06-01T15:00:00Z',
      validUntil: '2022-06-01T18:00:00Z',
      level: 'extreme',
      probability: 80,
      descriptionOriginal:
        'Some pretty intense coastal weather is coming our way',
      descriptionTranslation: 'And this would be the translation',
      geoJSON: testGeoJSON,
    };

    it('should remove unneeded fields', () => {
      const result = prepareFormValues({
        IS_DRAFT: true,
        object,
        'translation-select': 'NL',
        ...publicWarningDetails,
      });

      expect(result).toEqual(publicWarningDetails);
    });
    it('should remove empty fields', () => {
      const emptyForm: PublicWarningDetail = {
        id: '',
        phenomenon: '',
        validFrom: '2022-06-01T15:00:00Z',
        validUntil: '2022-06-01T18:00:00Z',
        level: '',
        probability: 80,
        descriptionOriginal: '',
        descriptionTranslation: '',
        geoJSON: null!,
      };

      const result = prepareFormValues(emptyForm);

      expect(result).toEqual({
        validFrom: '2022-06-01T15:00:00Z',
        validUntil: '2022-06-01T18:00:00Z',
        probability: 80,
      });
    });

    it('should do nothing with filled in fields', () => {
      const result = prepareFormValues({
        ...publicWarningDetails,
      });

      expect(result).toEqual(publicWarningDetails);
    });
  });

  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should render with default props', () => {
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText(LABEL_PHENOMENON)).toBeTruthy();
    expect(screen.getByText(LABEL_VALID_FROM)).toBeTruthy();
    expect(screen.getByText(LABEL_VALID_UNTIL)).toBeTruthy();
    expect(screen.getByText(LABEL_LEVEL)).toBeTruthy();
    expect(screen.getByText(LABEL_PROBABILITY)).toBeTruthy();
    expect(screen.getByText(BUTTON_CLEAR)).toBeTruthy();
    expect(screen.getByText(BUTTON_SAVE)).toBeTruthy();
    expect(screen.getByText(BUTTON_PUBLISH)).toBeTruthy();
    expect(screen.queryByRole('alert')).toBeFalsy();
  });

  it('should render as saving', () => {
    const props = {
      onSaveForm: jest.fn(),
      formAction: 'saving' as const,
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    expect(
      screen
        .getByRole('button', { name: BUTTON_CLEAR })
        .getAttribute('disabled'),
    ).toBeDefined();

    const saveButton = screen.queryByRole('button', { name: BUTTON_SAVING })!;
    expect(saveButton.getAttribute('disabled')).toBeNull();
    expect(within(saveButton).getByTestId('spinner')).toBeTruthy();

    const publishButton = screen.getByRole('button', { name: BUTTON_PUBLISH });
    expect(publishButton.getAttribute('disabled')).toBeDefined();

    screen
      .getAllByRole('textbox')
      .forEach((textbox) => expect(textbox).toBeDisabled());

    fireEvent.click(saveButton);

    expect(props.onSaveForm).not.toHaveBeenCalled();
  });

  it('should render as publishing', () => {
    const props = {
      onPublishForm: jest.fn(),
      formAction: 'publishing' as const,
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    expect(
      screen
        .getByRole('button', { name: BUTTON_CLEAR })
        .getAttribute('disabled'),
    ).toBeDefined();

    const saveButton = screen.queryByRole('button', { name: BUTTON_SAVE })!;
    expect(saveButton.getAttribute('disabled')).toBeDefined();
    expect(within(saveButton).queryByTestId('spinner')).toBeFalsy();

    const publishButton = screen.getByRole('button', {
      name: BUTTON_PUBLISHING,
    });
    expect(publishButton.getAttribute('disabled')).toBeNull();
    expect(within(publishButton).getByTestId('spinner')).toBeTruthy();

    screen
      .getAllByRole('textbox')
      .forEach((textbox) => expect(textbox).toBeDisabled());

    fireEvent.click(publishButton);
    expect(props.onPublishForm).not.toHaveBeenCalled();
  });

  it('should render as readonly', () => {
    const props = {
      formAction: 'readonly' as const,
      isReadOnly: true,
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.queryByRole('button', { name: BUTTON_SAVING })).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_PUBLISH })).toBeFalsy();
    expect(screen.queryByRole('button', { name: BUTTON_EDIT })).toBeFalsy();

    screen
      .getAllByRole('textbox')
      .forEach((textbox) =>
        expect(textbox.getAttribute('readonly')).toBeDefined(),
      );
  });

  it('should render as error', () => {
    const props = {
      onPublishForm: jest.fn(),
      error: { message: 'test error' },
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText(props.error.message)).toBeTruthy();
    fireEvent.click(screen.getByRole('button', { name: 'CLOSE' }));
    expect(screen.queryByText(props.error.message)).toBeFalsy();
  });

  it('should render object', () => {
    const props = {
      object: {
        id: 'test',
        lastUpdatedTime: '2022-06-01T12:34:27Z',
        objectName: 'my test area',
        scope: 'user',
      } as DrawingListItem,
      publicWarningDetails: {
        geoJSON: testGeoJSON,
      } as PublicWarningDetail,
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText(props.object.objectName)).toBeTruthy();
    expect(
      screen.getByText(formatDate(props.object.lastUpdatedTime)),
    ).toBeTruthy();
    expect(screen.getByRole('button', { name: OBJECTS_MENU })).toBeTruthy();
  });

  it('should render with default form properties if none passed', () => {
    jest.useFakeTimers().setSystemTime(new Date('2023-12-20T14:00:00Z'));

    render(
      <WarningsThemeProvider>
        <PublicWarningsForm />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText(LABEL_PHENOMENON)).toBeTruthy();
    expect(screen.getByText(NO_OBJECT)).toBeTruthy();
    expect(screen.getByText(LABEL_LEVEL)).toBeTruthy();
    expect(screen.getByText('30 %')).toBeTruthy();
    expect(
      screen
        .getByRole('textbox', {
          name: 'Valid from',
        })
        .getAttribute('value') || '',
    ).toEqual('20/12/2023 14:15');
    expect(
      screen
        .getByRole('textbox', {
          name: 'Valid until',
        })
        .getAttribute('value') || '',
    ).toEqual('20/12/2023 20:15');
  });

  it('should render with form properties if passed', () => {
    const props = {
      object: {
        id: '923723984872338768743',
        objectName: 'Drawing object 101',
        lastUpdatedTime: '2022-06-01T12:34:27Z',
        scope: 'user',
        geoJSON: testGeoJSON,
      } as DrawingListItem,
      publicWarningDetails: {
        id: '923723984872338768743',
        phenomenon: 'coastalEvent',
        validFrom: '2022-06-01T15:00:00Z',
        validUntil: '2022-06-01T18:00:00Z',
        level: 'extreme',
        probability: 80,
        descriptionOriginal:
          'Some pretty intense coastal weather is coming our way',
        descriptionTranslation: 'And this would be the translation',
        geoJSON: testGeoJSON,
      },
    };

    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByText('Coastal event')).toBeTruthy();
    expect(screen.getByText('Drawing object 101')).toBeTruthy();
    expect(
      screen
        .getByRole('textbox', {
          name: 'Valid from',
        })
        .getAttribute('value') || '',
    ).toEqual('01/06/2022 15:00');
    expect(
      screen
        .getByRole('textbox', {
          name: 'Valid until',
        })
        .getAttribute('value') || '',
    ).toEqual('01/06/2022 18:00');

    expect(screen.getByText('Extreme')).toBeTruthy();
    expect(screen.getByText('80 %')).toBeTruthy();
  });

  it('should be able to publish a public warning', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2022-06-01T14:00:00Z'));
    const props = {
      onPublishForm: jest.fn(),
      publicWarningDetails: {
        id: '923723984872338768743',
        phenomenon: 'coastalEvent',
        validFrom: '2022-06-01T15:00:00Z',
        validUntil: '2022-06-01T18:00:00Z',
        level: 'extreme',
        probability: 80,
        descriptionOriginal:
          'Some pretty intense coastal weather is coming our way',
        descriptionTranslation: 'And this would be the translation',
        geoJSON: testGeoJSON,
      },
      object: {
        geoJSON: testGeoJSON,
        id: '923723984872338768743',
        objectName: 'Drawing object 101',
        lastUpdatedTime: '2022-06-01T12:34:27Z',
        scope: 'user',
      } as DrawingListItem,
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    fireEvent.click(screen.getByText(BUTTON_PUBLISH));

    await waitFor(() => {
      expect(props.onPublishForm).toHaveBeenCalledWith({
        ...props.publicWarningDetails,
        geoJSON: props.object.geoJSON,
      });
    });
  });

  it('should be able to save a public warning', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2022-06-01T14:00:00Z'));
    const props = {
      onSaveForm: jest.fn(),
      publicWarningDetails: {
        id: '923723984872338768743',
        phenomenon: 'coastalEvent',
        validFrom: '2022-06-01T15:00:00Z',
        validUntil: '2022-06-01T18:00:00Z',
        level: 'extreme',
        probability: 80,
        descriptionOriginal:
          'Some pretty intense coastal weather is coming our way',
        descriptionTranslation: 'And this would be the translation',
        geoJSON: testGeoJSON,
      },
      object: {
        geoJSON: testGeoJSON,
        id: '923723984872338768743',
        objectName: 'Drawing object 101',
        lastUpdatedTime: '2022-06-01T12:34:27Z',
        scope: 'user',
      } as DrawingListItem,
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    fireEvent.click(screen.getByText(BUTTON_SAVE));

    await waitFor(() => {
      expect(props.onSaveForm).toHaveBeenCalledWith({
        ...props.publicWarningDetails,
        geoJSON: props.object.geoJSON,
      });
    });
  });

  it('should be able to clear the form', async () => {
    const props = {
      publicWarningDetails: {
        id: '',
        phenomenon: 'coastalEvent',
        validFrom: '2022-06-01T15:00:00Z',
        validUntil: '2022-06-01T18:00:00Z',
        level: 'extreme',
        probability: 80,
        descriptionOriginal:
          'Some pretty intense coastal weather is coming our way',
        descriptionTranslation: 'And this would be the translation',
        objectId: '923723984872338768743',
        geoJSON: testGeoJSON,
      },
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );
    const phenomenon = screen.getByLabelText('Phenomenon');
    const validFrom = screen.getByRole('textbox', { name: 'Valid from' });
    const validUntil = screen.getByRole('textbox', { name: 'Valid until' });
    const level = screen.getByLabelText('Level');
    const probability = screen.getByLabelText('Probability');
    const descriptionOriginal = screen.getByRole('tabpanel', {
      name: 'Description original',
    });

    // test filled in form
    expect(phenomenon.textContent).toEqual(warningPhenomena[8].title);
    expect(validFrom.getAttribute('value')!).toEqual(
      dateUtils.dateToString(
        new Date(props.publicWarningDetails.validFrom),
        DATE_FORMAT,
        true,
      ),
    );
    expect(validUntil.getAttribute('value')!).toEqual(
      dateUtils.dateToString(
        new Date(props.publicWarningDetails.validUntil),
        DATE_FORMAT,
        true,
      ),
    );

    expect(level.textContent!.trim()).toEqual(levelOptions[2].title);
    expect(probability.textContent).toEqual(
      `${props.publicWarningDetails.probability} %`,
    );
    expect(descriptionOriginal.textContent).toEqual(
      `Dutch (selected)${props.publicWarningDetails.descriptionOriginal}`,
    );

    // click clear
    fireEvent.click(screen.getByText(BUTTON_CLEAR));

    const expectedResult = getEmptyPublicWarning();
    const getFieldValue = (textContent: Node['textContent']): string =>
      textContent!.replace(/\u200B/g, '');

    // test form values
    expect(getFieldValue(phenomenon.textContent)).toEqual(
      expectedResult.phenomenon,
    );
    expect(validFrom.getAttribute('value')!).toEqual(
      dateUtils.dateToString(
        new Date(expectedResult.validFrom),
        DATE_FORMAT,
        true,
      ),
    );
    expect(validUntil.getAttribute('value')!).toEqual(
      dateUtils.dateToString(
        new Date(expectedResult.validUntil),
        DATE_FORMAT,
        true,
      ),
    );

    expect(getFieldValue(level.textContent)).toEqual('');
    expect(probability.textContent).toEqual(`${expectedResult.probability} %`);
    expect(descriptionOriginal.textContent).toEqual(`Dutch (selected)`);
  });

  it('should show required field errors on publishing an empty form and focus first error field', async () => {
    const props = {
      onPublishForm: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    expect(
      screen.queryByText(LABEL_AREA)!.getAttribute('aria-errormessage'),
    ).toBeNull();

    fireEvent.click(screen.getByText(BUTTON_PUBLISH));
    expect(props.onPublishForm).not.toHaveBeenCalled();
    await waitFor(() =>
      expect(screen.getAllByText(errorMessages.required)).toHaveLength(2),
    );
    const phenomenon = screen.getByLabelText(LABEL_PHENOMENON);
    expect(phenomenon.matches(':focus')).toBeTruthy();

    expect(
      screen.getByText(LABEL_AREA).getAttribute('aria-errormessage'),
    ).toEqual(errorMessages.required);
  });

  it('should not show required field errors on saving an empty form', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2024-06-01T14:00:00Z'));
    const props = {
      onSaveForm: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    fireEvent.click(screen.getByText(BUTTON_SAVE));
    await waitFor(() =>
      expect(props.onSaveForm).toHaveBeenCalledWith(
        prepareFormValues(getEmptyPublicWarning()),
      ),
    );
    expect(screen.queryByText(errorMessages.required)).toBeFalsy();
  });

  it('should show errors', async () => {
    const props = {
      error: { message: fakePostError },
      onSaveForm: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByRole('alert')).toBeTruthy();
    expect(screen.getByRole('alert').classList).not.toContain(
      'MuiAlert-standardWarning',
    );
    const titleError1 = screen.getByText('body -> warningDetail -> phenomenon');
    const messageError1 = screen.getByText(
      "value is not a valid enumeration member; permitted: 'wind', 'fog', 'thunderstorm', 'snowIce', 'rain', 'highTemp', 'funnelCloud', 'lowTemp', 'coastalEvent' (type=type_error.enum; enum_values=[<PhenomenonEnum.WIND: 'wind'>, <PhenomenonEnum.FOG: 'fog'>, <PhenomenonEnum.THUNDERSTORM: 'thunderstorm'>, <PhenomenonEnum.SNOW_ICE: 'snowIce'>, <PhenomenonEnum.RAIN: 'rain'>, <PhenomenonEnum.HIGH_TEMP: 'highTemp'>, <PhenomenonEnum.FUNNEL_CLD: 'funnelCloud'>, <PhenomenonEnum.LOW_TEMP: 'lowTemp'>, <PhenomenonEnum.COASTAL_EVENT: 'coastalEvent'>])",
    );

    expect(titleError1).toBeTruthy();
    expect(titleError1.tagName).toEqual('I');

    expect(messageError1).toBeTruthy();
    expect(messageError1.tagName).toEqual('P');

    const titleError2 = screen.getByText('body -> warningDetail -> level');
    const messageError2 = screen.getByText(
      "value is not a valid enumeration member; permitted: 'moderate', 'severe', 'extreme' (type=type_error.enum; enum_values=[<LevelEnum.MODERATE: 'moderate'>, <LevelEnum.SEVERE: 'severe'>, <LevelEnum.EXTREME: 'extreme'>])",
    );
    expect(titleError2).toBeTruthy();
    expect(titleError2.tagName).toEqual('I');

    expect(messageError2).toBeTruthy();
    expect(messageError2.tagName).toEqual('P');

    fireEvent.click(screen.getByRole('button', { name: /CLOSE/i }));

    expect(screen.queryByRole('alert')).toBeFalsy();
  });

  it('should show errors as warning', async () => {
    const props = {
      error: { message: fakePostError, severity: 'warning' as const },
      onSaveForm: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    expect(screen.getByRole('alert')).toBeTruthy();
    expect(screen.getByRole('alert').classList).toContain(
      'MuiAlert-standardWarning',
    );
    const titleError1 = screen.getByText('body -> warningDetail -> phenomenon');
    const messageError1 = screen.getByText(
      "value is not a valid enumeration member; permitted: 'wind', 'fog', 'thunderstorm', 'snowIce', 'rain', 'highTemp', 'funnelCloud', 'lowTemp', 'coastalEvent' (type=type_error.enum; enum_values=[<PhenomenonEnum.WIND: 'wind'>, <PhenomenonEnum.FOG: 'fog'>, <PhenomenonEnum.THUNDERSTORM: 'thunderstorm'>, <PhenomenonEnum.SNOW_ICE: 'snowIce'>, <PhenomenonEnum.RAIN: 'rain'>, <PhenomenonEnum.HIGH_TEMP: 'highTemp'>, <PhenomenonEnum.FUNNEL_CLD: 'funnelCloud'>, <PhenomenonEnum.LOW_TEMP: 'lowTemp'>, <PhenomenonEnum.COASTAL_EVENT: 'coastalEvent'>])",
    );

    expect(titleError1).toBeTruthy();
    expect(titleError1.tagName).toEqual('I');

    expect(messageError1).toBeTruthy();
    expect(messageError1.tagName).toEqual('P');

    const titleError2 = screen.getByText('body -> warningDetail -> level');
    const messageError2 = screen.getByText(
      "value is not a valid enumeration member; permitted: 'moderate', 'severe', 'extreme' (type=type_error.enum; enum_values=[<LevelEnum.MODERATE: 'moderate'>, <LevelEnum.SEVERE: 'severe'>, <LevelEnum.EXTREME: 'extreme'>])",
    );
    expect(titleError2).toBeTruthy();
    expect(titleError2.tagName).toEqual('I');

    expect(messageError2).toBeTruthy();
    expect(messageError2.tagName).toEqual('P');

    fireEvent.click(screen.getByRole('button', { name: /CLOSE/i }));

    expect(screen.queryByRole('alert')).toBeFalsy();
  });

  it('should show required field error on valid from field when empty', async () => {
    const props = {
      onPublishForm: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    const validFrom = screen.getByRole('textbox', {
      name: 'Valid from',
    });
    fireEvent.change(validFrom, {
      target: { value: '' },
    });
    await waitFor(() =>
      expect(validFrom.getAttribute('value') || '').toEqual(''),
    );

    await waitFor(() =>
      expect(screen.getAllByText(errorMessages.required)).toHaveLength(1),
    );
    expect(validFrom.getAttribute('aria-invalid') === 'true').toBeTruthy();
  });

  it('should show required field error on valid until field when empty', async () => {
    const props = {
      onPublishForm: jest.fn(),
    };
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...props} />
      </WarningsThemeProvider>,
    );

    const validUntil = screen.getByRole('textbox', {
      name: 'Valid until',
    });
    fireEvent.change(validUntil, {
      target: { value: '' },
    });
    await waitFor(() =>
      expect(validUntil.getAttribute('value') || '').toEqual(''),
    );

    await waitFor(() =>
      expect(screen.getAllByText(errorMessages.required)).toHaveLength(1),
    );
    expect(validUntil.getAttribute('aria-invalid') === 'true').toBeTruthy();
  });

  it('should show error on valid from field when before current time', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2023-12-20T14:00:00Z'));
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm />
      </WarningsThemeProvider>,
    );

    const validFrom = screen.getByRole('textbox', {
      name: 'Valid from',
    });
    fireEvent.change(validFrom, {
      target: { value: '20/12/2023 13:59' },
    });
    await waitFor(() =>
      expect(validFrom.getAttribute('value') || '').toEqual('20/12/2023 13:59'),
    );

    await screen.findByText(VALID_FROM_ERROR_BEFORE_CURRENT);
    expect(validFrom.getAttribute('aria-invalid') === 'true').toBeTruthy();
  });

  it('should show error on valid from field when too far after current time', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2023-12-20T14:00:00Z'));
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm />
      </WarningsThemeProvider>,
    );

    const validFrom = screen.getByRole('textbox', {
      name: 'Valid from',
    });
    fireEvent.change(validFrom, {
      target: { value: '27/12/2023 14:01' },
    });
    await waitFor(() =>
      expect(validFrom.getAttribute('value') || '').toEqual('27/12/2023 14:01'),
    );

    await screen.findByText(VALID_FROM_ERROR_AFTER_CURRENT);
    expect(validFrom.getAttribute('aria-invalid') === 'true').toBeTruthy();
  });

  it('should show error on valid until field when before current time', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2023-12-20T14:00:00Z'));
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm />
      </WarningsThemeProvider>,
    );

    const validUntil = screen.getByRole('textbox', {
      name: 'Valid until',
    });
    fireEvent.change(validUntil, {
      target: { value: '20/12/2023 13:59' },
    });
    await waitFor(() =>
      expect(validUntil.getAttribute('value') || '').toEqual(
        '20/12/2023 13:59',
      ),
    );

    await screen.findByText(VALID_UNTIL_ERROR_BEFORE_CURRENT);
    expect(validUntil.getAttribute('aria-invalid') === 'true').toBeTruthy();
  });

  it('should show error on valid until field when too far after current time', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2023-12-20T04:00:00Z'));
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm />
      </WarningsThemeProvider>,
    );

    const validUntil = screen.getByRole('textbox', {
      name: 'Valid until',
    });
    fireEvent.change(validUntil, {
      target: { value: '27/12/2023 16:01' },
    });
    await waitFor(() =>
      expect(validUntil.getAttribute('value') || '').toEqual(
        '27/12/2023 16:01',
      ),
    );

    await screen.findByText(VALID_UNTIL_ERROR_AFTER_CURRENT);
    expect(validUntil.getAttribute('aria-invalid') === 'true').toBeTruthy();
  });

  it('should show error on valid until field when less than one hour after valid from time', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2023-12-20T04:00:00Z'));
    render(
      <WarningsThemeProvider>
        <PublicWarningsForm />
      </WarningsThemeProvider>,
    );

    const validFrom = screen.getByRole('textbox', {
      name: 'Valid from',
    });
    expect(validFrom.getAttribute('value') || '').toEqual('20/12/2023 04:15');
    const validUntil = screen.getByRole('textbox', {
      name: 'Valid until',
    });
    fireEvent.change(validUntil, {
      target: { value: '20/12/2023 05:14' },
    });
    await waitFor(() =>
      expect(validUntil.getAttribute('value') || '').toEqual(
        '20/12/2023 05:14',
      ),
    );

    await screen.findByText(VALID_UNTIL_ERROR_AFTER_VALID_FROM);
    expect(validUntil.getAttribute('aria-invalid') === 'true').toBeTruthy();

    fireEvent.change(validFrom, {
      target: { value: '20/12/2023 04:14' },
    });

    await waitFor(() =>
      expect(validFrom.getAttribute('value') || '').toEqual('20/12/2023 04:14'),
    );

    await waitFor(() =>
      expect(
        screen.queryByText(VALID_UNTIL_ERROR_AFTER_VALID_FROM),
      ).toBeFalsy(),
    );
    expect(validUntil.getAttribute('aria-invalid') === 'true').toBeFalsy();
  });

  it('should call onFormDirty and reset isDirty after successfull request', async () => {
    const testProps = {
      onFormDirty: jest.fn(),
    };

    const { rerender } = render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...testProps} />
      </WarningsThemeProvider>,
    );

    const user = userEvent.setup({});

    fireEvent.mouseDown(
      screen.getByRole('button', { name: 'Probability 30 %' }),
    );
    const menuItem = await screen.findByText('50 %');
    fireEvent.click(menuItem);

    expect(testProps.onFormDirty).toHaveBeenCalledWith(true);
    expect(testProps.onFormDirty).toHaveBeenCalledTimes(1);

    user.click(screen.getByRole('button', { name: BUTTON_SAVE }));
    // trigger request by setting formAction saving
    rerender(
      <WarningsThemeProvider>
        <PublicWarningsForm {...{ ...testProps, formAction: 'saving' }} />
      </WarningsThemeProvider>,
    );
    // trigger success by setting formAction
    rerender(
      <WarningsThemeProvider>
        <PublicWarningsForm {...{ ...testProps, formAction: '' }} />
      </WarningsThemeProvider>,
    );

    await waitFor(() =>
      expect(testProps.onFormDirty).toHaveBeenCalledWith(false),
    );
    expect(testProps.onFormDirty).toHaveBeenCalledTimes(2);
  });

  it('should call onFormDirty and not reset after unsuccessfull request', async () => {
    const testProps = {
      onFormDirty: jest.fn(),
    };

    const { rerender } = render(
      <WarningsThemeProvider>
        <PublicWarningsForm {...testProps} />
      </WarningsThemeProvider>,
    );

    const user = userEvent.setup({});

    fireEvent.mouseDown(
      screen.getByRole('button', { name: 'Probability 30 %' }),
    );
    const menuItem = await screen.findByText('50 %');
    fireEvent.click(menuItem);

    expect(testProps.onFormDirty).toHaveBeenCalledWith(true);
    expect(testProps.onFormDirty).toHaveBeenCalledTimes(1);

    user.click(screen.getByRole('button', { name: BUTTON_SAVE }));
    // trigger request by setting formAction saving
    rerender(
      <WarningsThemeProvider>
        <PublicWarningsForm {...{ ...testProps, formAction: 'saving' }} />
      </WarningsThemeProvider>,
    );
    // trigger error
    rerender(
      <WarningsThemeProvider>
        <PublicWarningsForm
          {...{
            ...testProps,
            error: { message: 'something went wrong' },
            formAction: '',
          }}
        />
      </WarningsThemeProvider>,
    );

    expect(testProps.onFormDirty).toHaveBeenCalledTimes(1);
  });
});
