/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { Paper } from '@mui/material';
import * as React from 'react';
import { darkTheme } from '@opengeoweb/theme';
import { PublicWarningsForm } from './PublicWarningsForm';
import { WarningsThemeProvider } from '../Providers/Providers';
import { DrawingListItem } from '../../store/drawings/types';
import { PublicWarningDetail } from '../../store/publicWarningForm/types';
import { fakePostError, testGeoJSON } from '../../storybookUtils/testUtils';

export default {
  title: 'components/PublicWarningsForm',
};

const object: DrawingListItem = {
  id: '923723984872338768743',
  objectName: 'Drawing object 101',
  lastUpdatedTime: '2022-06-01T12:34:27Z',
  scope: 'user',
  geoJSON: testGeoJSON,
};
const publicWarningDetails: PublicWarningDetail = {
  id: '923723984872338768743',
  phenomenon: 'coastalEvent',
  validFrom: '2022-06-01T15:00:00Z',
  validUntil: '2022-06-01T18:00:00Z',
  level: 'extreme',
  probability: 80,
  descriptionOriginal: 'Some pretty intense coastal weather is coming our way',
  descriptionTranslation: 'And this would be the translation',
  geoJSON: testGeoJSON,
};

const baseProps = {
  onPublishForm: (formValues: PublicWarningDetail): void => {
    // eslint-disable-next-line no-console
    console.log('publish form with values', formValues);
  },
  onSaveForm: (formValues: PublicWarningDetail): void => {
    // eslint-disable-next-line no-console
    console.log('save form with values', formValues);
  },
};

const props = {
  object,
  publicWarningDetails,
  ...baseProps,
};

export const PublicWarningsFormDemo = (): React.ReactElement => {
  return (
    <WarningsThemeProvider>
      <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
        <PublicWarningsForm {...props} />
      </Paper>
    </WarningsThemeProvider>
  );
};
PublicWarningsFormDemo.storyName = 'PublicWarningsForm light';

export const PublicWarningsFormDemoDark = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
        <PublicWarningsForm {...props} />
      </Paper>
    </WarningsThemeProvider>
  );
};
PublicWarningsFormDemoDark.storyName = 'PublicWarningsForm dark';

export const PublicWarningsEmptyFormDemo = (): React.ReactElement => {
  return (
    <WarningsThemeProvider>
      <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
        <PublicWarningsForm {...baseProps} />
      </Paper>
    </WarningsThemeProvider>
  );
};
PublicWarningsEmptyFormDemo.storyName = 'PublicWarningsForm empty light';

export const PublicWarningsEmptyFormDemoDark = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
        <PublicWarningsForm {...baseProps} />
      </Paper>
    </WarningsThemeProvider>
  );
};
PublicWarningsEmptyFormDemoDark.storyName = 'PublicWarningsForm empty dark';

export const PublicWarningsSavingFormDemo = (): React.ReactElement => {
  return (
    <WarningsThemeProvider>
      <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
        <PublicWarningsForm {...baseProps} formAction="saving" />
      </Paper>
    </WarningsThemeProvider>
  );
};

export const PublicWarningsSavingFormDemoDark = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
        <PublicWarningsForm {...baseProps} formAction="saving" />
      </Paper>
    </WarningsThemeProvider>
  );
};

export const PublicWarningsErrorFormDemo = (): React.ReactElement => (
  <WarningsThemeProvider>
    <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
      <PublicWarningsForm {...baseProps} error={{ message: fakePostError }} />
    </Paper>
  </WarningsThemeProvider>
);

export const PublicWarningsErrorFormDemoDark = (): React.ReactElement => (
  <WarningsThemeProvider theme={darkTheme}>
    <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
      <PublicWarningsForm {...baseProps} error={{ message: fakePostError }} />
    </Paper>
  </WarningsThemeProvider>
);

export const PublicWarningsReadOnlyFormDemo = (): React.ReactElement => (
  <WarningsThemeProvider>
    <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
      <PublicWarningsForm {...props} formAction="readonly" />
    </Paper>
  </WarningsThemeProvider>
);

export const PublicWarningsReadOnlyFormDemoDark = (): React.ReactElement => (
  <WarningsThemeProvider theme={darkTheme}>
    <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
      <PublicWarningsForm {...props} formAction="readonly" />
    </Paper>
  </WarningsThemeProvider>
);

export const PublicWarningsDisabledFormDemo = (): React.ReactElement => (
  <WarningsThemeProvider>
    <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
      <PublicWarningsForm {...props} formAction="publishing" />
    </Paper>
  </WarningsThemeProvider>
);

export const PublicWarningsDisabledFormDemoDark = (): React.ReactElement => (
  <WarningsThemeProvider theme={darkTheme}>
    <Paper sx={{ maxWidth: 1200, height: 'calc(100vh)' }}>
      <PublicWarningsForm {...props} formAction="publishing" />
    </Paper>
  </WarningsThemeProvider>
);
