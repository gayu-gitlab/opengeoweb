/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { configureStore } from '@reduxjs/toolkit';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { Provider } from 'react-redux';
import { ThemeProvider } from '@opengeoweb/theme';
import { emptyGeoJSON } from '@opengeoweb/webmap-react';
import { PublicWarningsFormConnect } from './PublicWarningsFormConnect';

import {
  TAKEOVER_MESSAGE,
  publicWarningFormActions,
  reducer as publicWarningReducer,
} from '../../store/publicWarningForm/reducer';
import { NO_OBJECT, OBJECTS_MENU, formatDate } from './AreaField/AreaField';
import { DrawingListItem } from '../../store/drawings/types';
import {
  BUTTON_PUBLISH,
  BUTTON_PUBLISHING,
  BUTTON_SAVE,
  BUTTON_SAVING,
} from './PublicWarningsForm';
import {
  PublicWarning,
  PublicWarningDetail,
} from '../../store/publicWarningForm/types';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { MOCK_USERNAME } from '../../utils/fakeApi';
import { publicWarningActions } from '../../store/publicWarnings/reducer';

describe('components/PublicWarningsFormConnect', () => {
  it('should render with default props', () => {
    const store = configureStore({
      reducer: {
        publicWarningForm: publicWarningReducer,
      },
    });

    render(
      <Provider store={store}>
        <ThemeProvider>
          <PublicWarningsFormConnect />
        </ThemeProvider>
      </Provider>,
    );

    expect(store.getState().publicWarningForm.object).toBeUndefined();
    expect(screen.getByText(NO_OBJECT)).toBeTruthy();
    expect(screen.queryByRole('button', { name: OBJECTS_MENU })).toBeFalsy();
  });

  it('should render prefilled object values', () => {
    const store = configureStore({
      reducer: {
        publicWarningForm: publicWarningReducer,
      },
    });
    const testObject: DrawingListItem = {
      id: 'test',
      lastUpdatedTime: '2022-06-01T12:34:27Z',
      objectName: 'my test area',
      scope: 'user',
      geoJSON: emptyGeoJSON,
    };
    const testPublicWarningDetail: PublicWarningDetail = {
      id: '923723984872338768743',
      phenomenon: 'coastalEvent',
      validFrom: '2022-06-01T15:00:00Z',
      validUntil: '2022-06-01T18:00:00Z',
      level: 'extreme',
      probability: 80,
      descriptionOriginal:
        'Some pretty intense coastal weather is coming our way',
      descriptionTranslation: 'And this would be the translation',
      geoJSON: emptyGeoJSON,
    };

    store.dispatch(
      publicWarningFormActions.setFormValues({
        object: testObject,
        publicWarning: {
          warningDetail: testPublicWarningDetail,
          status: 'DRAFT',
        },
      }),
    );

    render(
      <Provider store={store}>
        <ThemeProvider>
          <PublicWarningsFormConnect />
        </ThemeProvider>
      </Provider>,
    );

    expect(store.getState().publicWarningForm.object).toEqual(testObject);
    expect(
      store.getState().publicWarningForm.publicWarning?.warningDetail,
    ).toEqual(testPublicWarningDetail);
    expect(screen.getByText(testObject.objectName)).toBeTruthy();
    expect(
      screen.getByText(formatDate(testObject.lastUpdatedTime)),
    ).toBeTruthy();
    expect(screen.getByRole('button', { name: OBJECTS_MENU })).toBeTruthy();
  });

  it('should publish a form', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2022-06-01T12:00:00Z'));
    const store = configureStore({
      reducer: {
        publicWarningForm: publicWarningReducer,
      },
    });
    const testObject: DrawingListItem = {
      id: 'test',
      lastUpdatedTime: '2022-06-01T12:34:27Z',
      objectName: 'my test area',
      scope: 'user',
      geoJSON: emptyGeoJSON,
    };
    const testPublicWarningDetail: PublicWarningDetail = {
      id: '923723984872338768743',
      phenomenon: 'coastalEvent',
      validFrom: '2022-06-01T15:00:00Z',
      validUntil: '2022-06-01T18:00:00Z',
      level: 'extreme',
      probability: 80,
      descriptionOriginal:
        'Some pretty intense coastal weather is coming our way',
      descriptionTranslation: 'And this would be the translation',
      geoJSON: emptyGeoJSON,
    };

    store.dispatch(
      publicWarningFormActions.setFormValues({
        object: testObject,
        publicWarning: {
          warningDetail: testPublicWarningDetail,
          status: 'DRAFT',
        },
      }),
    );

    render(
      <Provider store={store}>
        <ThemeProvider>
          <PublicWarningsFormConnect />
        </ThemeProvider>
      </Provider>,
    );
    expect(store.getState().publicWarningForm.formState).toBeUndefined();

    const publishButton = screen.getByRole('button', {
      name: BUTTON_PUBLISH,
    });

    fireEvent.click(publishButton);

    await waitFor(() => {
      expect(store.getState().publicWarningForm.formState).toEqual(
        'publishing',
      );
    });

    expect(
      screen.getByRole('button', {
        name: BUTTON_PUBLISHING,
      }),
    ).toBeTruthy();

    await waitFor(() => {
      expect(
        screen.queryByRole('dialog', {
          name: 'Public warning',
        }),
      ).toBeFalsy();
    });

    jest.useRealTimers();
  });

  it('should save a form', async () => {
    jest.useFakeTimers().setSystemTime(new Date('2022-06-01T12:00:00Z'));
    const store = configureStore({
      reducer: {
        publicWarningForm: publicWarningReducer,
      },
    });
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    store.addEggs = jest.fn();

    const testObject: DrawingListItem = {
      id: 'test',
      lastUpdatedTime: '2022-06-01T12:34:27Z',
      objectName: 'my test area',
      scope: 'user',
      geoJSON: emptyGeoJSON,
    };
    const testPublicWarningDetail: PublicWarningDetail = {
      id: '923723984872338768743',
      phenomenon: 'coastalEvent',
      validFrom: '2022-06-01T15:00:00Z',
      validUntil: '2022-06-01T18:00:00Z',
      level: 'extreme',
      probability: 80,
      descriptionOriginal:
        'Some pretty intense coastal weather is coming our way',
      descriptionTranslation: 'And this would be the translation',
      geoJSON: emptyGeoJSON,
    };

    store.dispatch(
      publicWarningFormActions.setFormValues({
        object: testObject,
        publicWarning: {
          warningDetail: testPublicWarningDetail,
          status: 'DRAFT',
          editor: MOCK_USERNAME,
        },
      }),
    );

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormConnect />
      </WarningsThemeStoreProvider>,
    );
    expect(store.getState().publicWarningForm.formState).toBeUndefined();

    const publishButton = screen.getByRole('button', {
      name: BUTTON_SAVE,
    });

    fireEvent.click(publishButton);

    await waitFor(() => {
      expect(store.getState().publicWarningForm.formState).toEqual('saving');
    });
    expect(
      screen.queryByRole('button', {
        name: BUTTON_SAVE,
      }),
    ).toBeFalsy();
    expect(
      screen.getByRole('button', {
        name: BUTTON_SAVING,
      }),
    ).toBeTruthy();
    await waitFor(() => {
      expect(store.getState().publicWarningForm.publicWarning?.editor).toEqual(
        MOCK_USERNAME,
      );
    });
    jest.useRealTimers();
  });

  it('should show error', async () => {
    const store = configureStore({
      reducer: {
        publicWarningForm: publicWarningReducer,
      },
    });
    const testObject: DrawingListItem = {
      id: 'test',
      lastUpdatedTime: '2022-06-01T12:34:27Z',
      objectName: 'my test area',
      scope: 'user',
      geoJSON: emptyGeoJSON,
    };

    store.dispatch(
      publicWarningFormActions.setFormValues({
        object: testObject,
      }),
    );
    const testError = 'something went wrong';
    store.dispatch(
      publicWarningFormActions.setFormError({ message: testError }),
    );

    render(
      <Provider store={store}>
        <ThemeProvider>
          <PublicWarningsFormConnect />
        </ThemeProvider>
      </Provider>,
    );
    expect(screen.getByText(testError)).toBeTruthy();
  });

  it('should render readonly if you are not the editor', () => {
    const store = configureStore({
      reducer: {
        publicWarningForm: publicWarningReducer,
      },
    });
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    store.addEggs = jest.fn();

    const testObject: DrawingListItem = {
      id: 'test',
      lastUpdatedTime: '2022-06-01T12:34:27Z',
      objectName: 'my test area',
      scope: 'user',
      geoJSON: emptyGeoJSON,
    };
    const testPublicWarningDetail: PublicWarningDetail = {
      id: '923723984872338768743',
      phenomenon: 'coastalEvent',
      validFrom: '2022-06-01T15:00:00Z',
      validUntil: '2022-06-01T18:00:00Z',
      level: 'extreme',
      probability: 80,
      descriptionOriginal:
        'Some pretty intense coastal weather is coming our way',
      descriptionTranslation: 'And this would be the translation',
      geoJSON: emptyGeoJSON,
    };

    store.dispatch(
      publicWarningFormActions.setFormValues({
        object: testObject,
        publicWarning: {
          warningDetail: testPublicWarningDetail,
          status: 'DRAFT',
          editor: 'phil.collins',
        },
      }),
    );

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(screen.queryByRole('button', { name: BUTTON_SAVE })).toBeFalsy();
  });
  it('should render in edit mode if you are creating a new warning (you are not yet set as editor)', () => {
    const store = configureStore({
      reducer: {
        publicWarningForm: publicWarningReducer,
      },
    });
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    store.addEggs = jest.fn();

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(screen.getByRole('button', { name: BUTTON_SAVE })).toBeTruthy();
  });

  it('should show a warning when other user takes over', async () => {
    const store = configureStore({
      reducer: {
        publicWarningForm: publicWarningReducer,
      },
    });
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    store.addEggs = jest.fn();

    const testObject: DrawingListItem = {
      id: 'test',
      lastUpdatedTime: '2022-06-01T12:34:27Z',
      objectName: 'my test area',
      scope: 'user',
      geoJSON: emptyGeoJSON,
    };
    const testPublicWarningDetail: PublicWarningDetail = {
      id: '923723984872338768743',
      phenomenon: 'coastalEvent',
      validFrom: '2022-06-01T15:00:00Z',
      validUntil: '2022-06-01T18:00:00Z',
      level: 'extreme',
      probability: 80,
      descriptionOriginal:
        'Some pretty intense coastal weather is coming our way',
      descriptionTranslation: 'And this would be the translation',
      geoJSON: emptyGeoJSON,
    };

    const testPublicWarning: PublicWarning = {
      id: testPublicWarningDetail.id,
      warningDetail: testPublicWarningDetail,
      status: 'DRAFT',
      editor: MOCK_USERNAME,
    };

    store.dispatch(
      publicWarningFormActions.openPublicWarningFormDialog({
        object: testObject,
        publicWarning: testPublicWarning,
      }),
    );

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormConnect />
      </WarningsThemeStoreProvider>,
    );

    expect(screen.queryByText(TAKEOVER_MESSAGE)).toBeFalsy();
    expect(store.getState().publicWarningForm.error).toBeUndefined();

    await waitFor(() => {
      store.dispatch(
        publicWarningActions.fetchWarningsSuccess({
          warnings: [
            {
              ...testPublicWarning,
              editor: 'take.over',
            },
          ],
          lastUpdatedTime: '10:13',
        }),
      );
    });

    expect(store.getState().publicWarningForm.error).toEqual({
      message: TAKEOVER_MESSAGE,
      severity: 'warning',
    });

    expect(screen.getByText(TAKEOVER_MESSAGE)).toBeTruthy();
  });
});
