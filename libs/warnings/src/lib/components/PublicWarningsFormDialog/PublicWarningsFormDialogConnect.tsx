/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { useSetupDialog, uiTypes, CoreAppStore } from '@opengeoweb/store';
import { useAuthenticationContext } from '@opengeoweb/authentication';
import { Position, useConfirmationDialog } from '@opengeoweb/shared';
import { useDispatch, useSelector } from 'react-redux';
import { PublicWarningsFormDialog } from './PublicWarningsFormDialog';
import { publicWarningDialogType } from '../../store/publicWarningForm/utils';
import { PublicWarningsFormConnect } from '../PublicWarningsForm';
import {
  getPublicWarningEditor,
  getPublicWarningFormState,
  getPublicWarningStatus,
  getSelectedPublicIsFormDirty,
  getSelectedPublicWarningId,
} from '../../store/publicWarningForm/selectors';
import { publicWarningFormActions } from '../../store/publicWarningForm/reducer';
import { WarningModuleStore } from '../../store/config';
import { getWarningsApi } from '../../utils/api';

export const warningFormConfirmationOptions = {
  cancelLabel: 'Cancel',
  title: 'Close public warning',
  description: 'There are changes in the form, are you sure you want to close?',
  confirmLabel: 'Close',
  catchOnCancel: true,
};

export const isAlreadyEdited = (
  publicWarningEditor: string | undefined,
  username: string | undefined,
): boolean => {
  return (
    publicWarningEditor !== undefined &&
    publicWarningEditor !== '' &&
    publicWarningEditor !== null &&
    publicWarningEditor !== username
  );
};

export const PublicWarningsFormDialogConnect: React.FC<{
  bounds?: string;
  source?: uiTypes.Source;
  startPosition?: Position;
}> = ({ bounds, source = 'app', startPosition }) => {
  const warningsApi = getWarningsApi();
  const {
    dialogOrder,
    setDialogOrder,
    isDialogOpen,
    uiSource,
    onCloseDialog,
    uiIsLoading,
  } = useSetupDialog(publicWarningDialogType, source);
  const dispatch = useDispatch();
  const confirmDialog = useConfirmationDialog();

  const formAction = useSelector((store: CoreAppStore) =>
    getPublicWarningFormState(store),
  );

  const isFormDirty = useSelector((store: CoreAppStore) =>
    getSelectedPublicIsFormDirty(store),
  );

  const publicWarningStatus = useSelector((store: CoreAppStore) =>
    getPublicWarningStatus(store),
  );

  const selectedWarningId = useSelector((store: CoreAppStore) =>
    getSelectedPublicWarningId(store),
  );

  const publicWarningEditor = useSelector((store: WarningModuleStore) =>
    getPublicWarningEditor(store),
  );

  const { auth } = useAuthenticationContext();
  const isEditor = publicWarningEditor === auth?.username || false;

  const getLatestEditor = async (): Promise<string> => {
    const latestWarnings = await warningsApi.getWarnings();
    const latestWarning = latestWarnings.data.find(
      (warning) => warning.id === selectedWarningId,
    )!;
    return latestWarning.editor || '';
  };

  const setIsEditor = async (isReadOnly: boolean): Promise<boolean> => {
    const username = auth?.username || '';

    // make sure to get the latest changes if you are editing an existing warning
    const latestEditor = selectedWarningId ? await getLatestEditor() : '';

    if (
      !selectedWarningId ||
      (!isReadOnly && username === latestEditor) ||
      (isReadOnly && latestEditor !== username)
    ) {
      // currently editing a new warning or
      // correct editor is already set or
      // someone else is editor so only have to set to readonly mode
      // No need for a BE call
      dispatch(
        publicWarningFormActions.setFormValues({
          formState: isReadOnly ? 'readonly' : '',
        }),
      );
      return true;
    }

    // We are taking over from another user, first confirm this is what they want
    if (!isReadOnly && isAlreadyEdited(latestEditor, username)) {
      await confirmDialog({
        title: 'Switch to edit mode',
        description:
          'This warning is already in edit mode by another user and important information could get lost in the switching process.',
        confirmLabel: 'Edit mode',
      });
    }
    dispatch(
      publicWarningFormActions.toggleEditorWarning({
        warningId: selectedWarningId,
        isEditor: !isReadOnly,
        username,
      }),
    );
    return true;
  };

  const onChangeFormMode = (isReadOnly: boolean): void => {
    if (isReadOnly && isFormDirty) {
      confirmDialog({
        title: 'Whoops',
        description:
          'Are you sure you want to switch to view mode? Your unsaved changes will be lost.',
        confirmLabel: 'Switch mode',
      }).then(async () => {
        await setIsEditor(isReadOnly);
      });
    } else {
      setIsEditor(isReadOnly);
    }
  };

  const closeDialog = async (): Promise<void> => {
    if (isFormDirty) {
      const mayProceed = await confirmDialog(warningFormConfirmationOptions)
        .then(() => true)
        .catch(() => false);

      if (!mayProceed) {
        return;
      }
    }
    onCloseDialog();
  };

  if (!isDialogOpen) {
    return null;
  }
  // Readonly also in case you are not creating a new warning and you are not set as the editor
  const isReadOnly =
    formAction === 'readonly' ||
    (!isEditor && publicWarningStatus !== undefined);
  // Only show toggle if in draft status
  const shouldHideFormToggleMode = publicWarningStatus !== 'DRAFT';

  return (
    <PublicWarningsFormDialog
      isOpen={isDialogOpen}
      onClose={closeDialog}
      order={dialogOrder}
      onMouseDown={setDialogOrder}
      source={uiSource}
      bounds={bounds}
      startPosition={startPosition}
      isLoading={uiIsLoading}
      isReadOnly={isReadOnly}
      onChangeFormMode={onChangeFormMode}
      shouldHideFormMode={shouldHideFormToggleMode}
      publicWarningEditor={publicWarningEditor}
    >
      <PublicWarningsFormConnect />
    </PublicWarningsFormDialog>
  );
};
