/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';

import {
  mapActions,
  uiActions,
  uiReducer,
  webmapReducer,
} from '@opengeoweb/store';
import {
  fireEvent,
  render,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import { configureStore } from '@reduxjs/toolkit';
import createSagaMiddleware from 'redux-saga';
import { snackbarSaga } from '@opengeoweb/snackbar';
import { emptyGeoJSON } from '@opengeoweb/webmap-react';
import { PublicWarningsFormDialogConnect } from './PublicWarningsFormDialogConnect';
import { DIALOG_TITLE } from './PublicWarningsFormDialog';
import { publicWarningDialogType } from '../../store/publicWarningForm/utils';
import {
  publicWarningFormActions,
  reducer as publicWarningReducer,
} from '../../store/publicWarningForm/reducer';
import publicWarningsSaga from '../../store/publicWarnings/sagas';
import publicWarningSagas from '../../store/publicWarningForm/sagas';
import { WarningsThemeStoreProvider } from '../Providers/Providers';
import { DrawingListItem } from '../../store/drawings/types';
import { PublicWarningDetail } from '../../store/publicWarningForm/types';
import { MOCK_USERNAME } from '../../utils/fakeApi';

describe('components/PublicWarningsFormDialogConnect', () => {
  const testObject: DrawingListItem = {
    id: 'test',
    lastUpdatedTime: '2022-06-01T12:34:27Z',
    objectName: 'my test area',
    scope: 'user',
    geoJSON: emptyGeoJSON,
  };

  const sagaMiddleware = createSagaMiddleware();
  const store = configureStore({
    reducer: {
      ui: uiReducer,
      webmap: webmapReducer,
      publicWarningForm: publicWarningReducer,
    },
    middleware: (getDefaultMiddleware) =>
      getDefaultMiddleware().concat(sagaMiddleware),
  });

  sagaMiddleware.run(snackbarSaga);
  sagaMiddleware.run(publicWarningSagas);
  sagaMiddleware.run(publicWarningsSaga);

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  store.addEggs = jest.fn();

  it('should render with default props', async () => {
    const props = {
      source: 'app' as const,
      startPosition: { top: 600, left: 800 },
    };
    const mapId = 'mapId1';

    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormDialogConnect {...props} />
      </WarningsThemeStoreProvider>,
    );
    expect(screen.queryByText(DIALOG_TITLE)).toBeFalsy();

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();
    await waitFor(() => {
      // activate it
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: publicWarningDialogType,
          mapId,
          setOpen: true,
          source: props.source,
        }),
      );
    });
    expect(screen.getByText(DIALOG_TITLE)).toBeTruthy();

    const dialog = screen.getByRole('dialog');

    expect(getComputedStyle(dialog!).top).toEqual(
      `${props.startPosition.top}px`,
    );
    expect(getComputedStyle(dialog!).left).toEqual(
      `${props.startPosition.left}px`,
    );

    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeTruthy();

    expect(screen.queryByTestId('loading-bar')).toBeFalsy();

    // close it
    fireEvent.click(screen.getByRole('button', { name: /Close/i }));
    expect(
      store.getState().ui.dialogs[publicWarningDialogType]?.isOpen,
    ).toBeFalsy();
  });

  it('should show as loading', async () => {
    const props = {
      source: 'app' as const,
      startPosition: { top: 600, left: 800 },
    };
    const mapId = 'mapId1';

    store.dispatch(mapActions.registerMap({ mapId }));

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormDialogConnect {...props} />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      // activate it
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: publicWarningDialogType,
          mapId,
          setOpen: true,
          source: props.source,
        }),
      );
      store.dispatch(
        uiActions.toggleIsLoadingDialog({
          type: publicWarningDialogType,
          isLoading: true,
        }),
      );
    });
    expect(screen.getByTestId('loading-bar')).toBeTruthy();
  });

  it('should show confirmation if you have got unsaved changes and set to view mode', async () => {
    const props = {
      source: 'app' as const,
      startPosition: { top: 600, left: 800 },
    };
    const mapId = 'mapId1';

    store.dispatch(mapActions.registerMap({ mapId }));

    const testPublicWarningDetail: PublicWarningDetail = {
      id: '923723984872338768743',
      phenomenon: 'coastalEvent',
      validFrom: '2022-06-01T15:00:00Z',
      validUntil: '2022-06-01T18:00:00Z',
      level: 'extreme',
      probability: 80,
      descriptionOriginal:
        'Some pretty intense coastal weather is coming our way',
      descriptionTranslation: 'And this would be the translation',
      geoJSON: emptyGeoJSON,
    };

    store.dispatch(
      publicWarningFormActions.setFormValues({
        object: testObject,
        publicWarning: {
          warningDetail: testPublicWarningDetail,
          status: 'DRAFT',
          editor: MOCK_USERNAME,
        },
        formState: '',
      }),
    );
    store.dispatch(
      publicWarningFormActions.setFormDirty({
        isFormDirty: true,
      }),
    );

    render(
      <WarningsThemeStoreProvider store={store}>
        <PublicWarningsFormDialogConnect {...props} />
      </WarningsThemeStoreProvider>,
    );

    await waitFor(() => {
      // activate it
      store.dispatch(
        uiActions.setActiveMapIdForDialog({
          type: publicWarningDialogType,
          mapId,
          setOpen: true,
          source: props.source,
        }),
      );
      store.dispatch(
        uiActions.toggleIsLoadingDialog({
          type: publicWarningDialogType,
          isLoading: true,
        }),
      );
    });

    expect(store.getState().publicWarningForm.object).toEqual(testObject);
    expect(
      store.getState().publicWarningForm.publicWarning?.warningDetail,
    ).toEqual(testPublicWarningDetail);
    expect(screen.getByText(testObject.objectName)).toBeTruthy();

    const mode = within(screen.getByLabelText('Switch mode')).getByRole(
      'checkbox',
    );
    fireEvent.click(mode);

    expect(
      screen.getByText(
        'Are you sure you want to switch to view mode? Your unsaved changes will be lost.',
      ),
    ).toBeTruthy();
  });
});
