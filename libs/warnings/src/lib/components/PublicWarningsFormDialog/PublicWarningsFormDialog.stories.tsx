/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { Box } from '@mui/material';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { PublicWarningsFormDialog } from './PublicWarningsFormDialog';
import { WarningsThemeProvider } from '../Providers/Providers';
import { PublicWarningsForm } from '../PublicWarningsForm';
import { DrawingListItem } from '../../store/drawings/types';
import { PublicWarningDetail } from '../../store/publicWarningForm/types';
import { fakePostError, testGeoJSON } from '../../storybookUtils/testUtils';
import { MOCK_USERNAME } from '../../utils/fakeApi';
import { TAKEOVER_MESSAGE } from '../../store/publicWarningForm/reducer';

export default {
  title: 'components/PublicWarningsFormDialog',
};

const props = {
  isOpen: true,
  publicWarningEditor: MOCK_USERNAME,
  onClose: (): void => {},
};

const object: DrawingListItem = {
  id: '923723984872338768743',
  objectName: 'Drawing object 101',
  lastUpdatedTime: '2022-06-01T12:34:27Z',
  scope: 'user',
  geoJSON: testGeoJSON,
};
const publicWarningDetails: PublicWarningDetail = {
  id: '923723984872338768743',
  phenomenon: 'coastalEvent',
  validFrom: '2022-06-01T15:00:00Z',
  validUntil: '2022-06-01T18:00:00Z',
  level: 'extreme',
  probability: 80,
  descriptionOriginal: 'Some pretty intense coastal weather is coming our way',
  descriptionTranslation: 'And this would be the translation',
  geoJSON: testGeoJSON,
};

const formProps = {
  object,
  publicWarningDetails,
  isReadOnly: false,
  onPublishForm: (formValues: PublicWarningDetail): void => {
    // eslint-disable-next-line no-console
    console.log('publish form with values', formValues);
  },
  onSaveForm: (formValues: PublicWarningDetail): void => {
    // eslint-disable-next-line no-console
    console.log('save form with values', formValues);
  },
};

export const PublicWarningsFormDialogLight = (): React.ReactElement => {
  return (
    <Box sx={{ width: '800px', height: '1000px' }}>
      <WarningsThemeProvider theme={lightTheme}>
        <PublicWarningsFormDialog
          {...props}
          size={{ width: 320, height: 1000 }}
        >
          <PublicWarningsForm {...formProps} />
        </PublicWarningsFormDialog>
      </WarningsThemeProvider>
    </Box>
  );
};

PublicWarningsFormDialogLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/651aa14b3c962721814061e4/version/651c3bb654916d54e604b10a',
    },
  ],
};

PublicWarningsFormDialogLight.storyName =
  'PublicWarningsFormDialog light (takeSnapshot)';

export const PublicWarningsFormDialogDark = (): React.ReactElement => {
  return (
    <Box sx={{ width: '800px', height: '1000px' }}>
      <WarningsThemeProvider theme={darkTheme}>
        <PublicWarningsFormDialog
          {...props}
          size={{ width: 320, height: 1000 }}
        >
          <PublicWarningsForm {...formProps} />
        </PublicWarningsFormDialog>
      </WarningsThemeProvider>
    </Box>
  );
};

PublicWarningsFormDialogDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/651c3bc8617fc7550c8d796f/version/651c3bc8617fc7550c8d7970',
    },
  ],
};
PublicWarningsFormDialogDark.storyName =
  'PublicWarningsFormDialog dark (takeSnapshot)';

export const PublicWarningsFormDialogLightBig = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '900px', height: '800px' }}>
        <PublicWarningsFormDialog {...props} size={{ width: 800, height: 800 }}>
          <PublicWarningsForm {...formProps} />
        </PublicWarningsFormDialog>
      </Box>
    </WarningsThemeProvider>
  );
};
PublicWarningsFormDialogLightBig.storyName =
  'PublicWarningsFormDialog big light (takeSnapshot)';

export const PublicWarningsDarkBig = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Box sx={{ width: '900px', height: '800px' }}>
        <PublicWarningsFormDialog {...props} size={{ width: 800, height: 800 }}>
          <PublicWarningsForm {...formProps} />
        </PublicWarningsFormDialog>
      </Box>
    </WarningsThemeProvider>
  );
};
PublicWarningsDarkBig.storyName =
  'PublicWarningsFormDialog big dark (takeSnapshot)';

export const PublicWarningsFormDialogLightBigError = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '900px', height: '800px' }}>
        <PublicWarningsFormDialog {...props} size={{ width: 800, height: 800 }}>
          <PublicWarningsForm
            {...formProps}
            error={{ message: fakePostError }}
          />
        </PublicWarningsFormDialog>
      </Box>
    </WarningsThemeProvider>
  );
};
PublicWarningsFormDialogLightBigError.storyName =
  'PublicWarningsFormDialog error big light (takeSnapshot)';
export const PublicWarningsFormDialogDarkBigError = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Box sx={{ width: '900px', height: '800px' }}>
        <PublicWarningsFormDialog {...props} size={{ width: 800, height: 800 }}>
          <PublicWarningsForm
            {...formProps}
            error={{ message: fakePostError }}
          />
        </PublicWarningsFormDialog>
      </Box>
    </WarningsThemeProvider>
  );
};
PublicWarningsFormDialogDarkBigError.storyName =
  'PublicWarningsFormDialog error big dark (takeSnapshot)';

export const PublicWarningsFormDialogLightReadOnly = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={lightTheme}>
      <Box sx={{ width: '900px', height: '800px' }}>
        <PublicWarningsFormDialog
          {...props}
          shouldHideFormMode
          size={{ width: 800, height: 800 }}
        >
          <PublicWarningsForm {...formProps} isReadOnly />
        </PublicWarningsFormDialog>
      </Box>
    </WarningsThemeProvider>
  );
};
PublicWarningsFormDialogLightReadOnly.storyName =
  'PublicWarningsFormDialog readonly big light (takeSnapshot)';
export const PublicWarningsFormDialogDarkReadOnly = (): React.ReactElement => {
  return (
    <WarningsThemeProvider theme={darkTheme}>
      <Box sx={{ width: '900px', height: '800px' }}>
        <PublicWarningsFormDialog
          {...props}
          shouldHideFormMode
          size={{ width: 800, height: 800 }}
        >
          <PublicWarningsForm {...formProps} isReadOnly />
        </PublicWarningsFormDialog>
      </Box>
    </WarningsThemeProvider>
  );
};
PublicWarningsFormDialogDarkReadOnly.storyName =
  'PublicWarningsFormDialog readonly big dark (takeSnapshot)';

export const PublicWarningsFormDialogLightTakeoverWarning =
  (): React.ReactElement => {
    return (
      <WarningsThemeProvider theme={lightTheme}>
        <Box sx={{ width: '900px', height: '800px' }}>
          <PublicWarningsFormDialog
            {...props}
            shouldHideFormMode
            size={{ width: 800, height: 800 }}
          >
            <PublicWarningsForm
              {...formProps}
              isReadOnly
              error={{ message: TAKEOVER_MESSAGE, severity: 'warning' }}
            />
          </PublicWarningsFormDialog>
        </Box>
      </WarningsThemeProvider>
    );
  };
PublicWarningsFormDialogLightTakeoverWarning.storyName =
  'PublicWarningsFormDialog takeover light (takeSnapshot)';
export const PublicWarningsFormDialogDarkTakeoverWarning =
  (): React.ReactElement => {
    return (
      <WarningsThemeProvider theme={darkTheme}>
        <Box sx={{ width: '900px', height: '800px' }}>
          <PublicWarningsFormDialog
            {...props}
            shouldHideFormMode
            size={{ width: 800, height: 800 }}
          >
            <PublicWarningsForm
              {...formProps}
              isReadOnly
              error={{ message: TAKEOVER_MESSAGE, severity: 'warning' }}
            />
          </PublicWarningsFormDialog>
        </Box>
      </WarningsThemeProvider>
    );
  };
PublicWarningsFormDialogDarkTakeoverWarning.storyName =
  'PublicWarningsFormDialog takeover dark (takeSnapshot)';
