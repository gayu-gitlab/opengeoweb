/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
export const DATE_FORMAT = 'dd/MM/yyyy HH:mm';
export const DATE_FORMAT_UTC = "yyyy-MM-dd'T'HH:mm:ss'Z'";
export const DATE_FORMAT_HEADER = 'EEEE, MMMM dd';
export const DATE_FORMAT_DATE_FIELDS = 'DD/MM/YYYY HH:mm'; // TODO: should be 'ddd DD MMM HH:mm' https://gitlab.com/opengeoweb/opengeoweb/-/issues/4484
export const DATE_FORMAT_LASTUPDATEDTIME = 'HH:mm';
