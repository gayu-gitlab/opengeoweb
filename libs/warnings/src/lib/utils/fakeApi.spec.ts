/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as utils from '@opengeoweb/api';
import { createFakeApiInstance } from '@opengeoweb/api';
import {
  createApi,
  ERROR_NOT_FOUND,
  extractDrawingDetailsFromJSON,
} from './fakeApi';
import { DrawingDetails, DrawingFromBE } from '../store/drawings/types';
import { drawingsListFullContent } from './fakeApi/index';
import { testGeoJSON } from '../storybookUtils/testUtils';
import { PublicWarning } from '../store/publicWarningForm/types';

describe('src/utils/fakeApi', () => {
  const fakeAxiosInstance = createFakeApiInstance();

  describe('createApi', () => {
    it('should contain all api calls', async () => {
      const api = createApi();
      expect(api.getDrawings).toBeTruthy();
      expect(api.getDrawingDetails).toBeTruthy();
      expect(api.updateDrawing).toBeTruthy();
      expect(api.saveDrawingAs).toBeTruthy();
      expect(api.deleteDrawing).toBeTruthy();
    });

    it('should call getDrawings', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi();

      await api.getDrawings();
      expect(spy).toHaveBeenCalledWith('/drawings');
    });

    it('should call with the right params for getDrawingDetails', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi();

      const drawingId = '34536467568';
      await api.getDrawingDetails(drawingId);
      expect(spy).toHaveBeenCalledWith(`/drawings/${drawingId}`);
    });

    it('should return error for getDrawingDetails when id can not be found', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const api = createApi();

      const drawingId = 'non-existing';

      await expect(async () =>
        api.getDrawingDetails(drawingId),
      ).rejects.toThrow(ERROR_NOT_FOUND);
    });

    it('should call with the right params for updateDrawing', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi();

      const drawingDetails: DrawingFromBE = {
        id: '345346456345',
        objectName: 'Object 88',
        lastUpdatedTime: '2022-05-26T12:34:27Z',
        scope: 'user',
        username: 'Michael',
        geoJSON: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                fill: '#ff7800',
                'fill-opacity': 0.2,
                stroke: '#ff7800',
                'stroke-width': 2,
                'stroke-opacity': 1,
                selectionType: 'poly',
              },
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [
                    [10.481821619038993, 58.535200764136974],
                    [8.640758193817074, 60.346399887508504],
                    [13.391889613744592, 67.5974266962731],
                    [16.658292464944765, 66.50937700061121],
                    [10.481821619038993, 58.535200764136974],
                  ],
                ],
              },
            },
          ],
        },
      };
      await api.updateDrawing(drawingDetails.id!, drawingDetails);
      expect(spy).toHaveBeenCalledWith(
        `/drawings/${drawingDetails.id}/`,
        drawingDetails,
      );
    });

    it('should call with the right params for saveDrawingAs', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi();

      const drawingDetails: DrawingDetails = {
        objectName: 'Object 88',
        scope: 'user',
        username: 'Michael',
        geoJSON: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                fill: '#ff7800',
                'fill-opacity': 0.2,
                stroke: '#ff7800',
                'stroke-width': 2,
                'stroke-opacity': 1,
                selectionType: 'poly',
              },
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [
                    [10.481821619038993, 58.535200764136974],
                    [8.640758193817074, 60.346399887508504],
                    [13.391889613744592, 67.5974266962731],
                    [16.658292464944765, 66.50937700061121],
                    [10.481821619038993, 58.535200764136974],
                  ],
                ],
              },
            },
          ],
        },
      };
      await api.saveDrawingAs(drawingDetails);
      expect(spy).toHaveBeenCalledWith(`/drawings/`, drawingDetails);
    });

    it('should call with the right params for deleteDrawing', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi();
      const drawingId = 'test-1';
      await api.deleteDrawing(drawingId);
      expect(spy).toHaveBeenCalledWith(`/drawings/${drawingId}?delete=true`);
    });

    it('should call with the right params for getWarnings', async () => {
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi();

      await api.getWarnings();
      expect(spy).toHaveBeenCalledWith(`/warnings/`);
    });

    it('should call with the right params for saveWarningAs', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi();

      const testPublicWarning: PublicWarning = {
        warningDetail: {
          id: '923723984872338768743',
          phenomenon: 'coastalEvent',
          validFrom: '2022-06-01T15:00:00Z',
          validUntil: '2022-06-01T18:00:00Z',
          level: 'extreme',
          probability: 80,
          descriptionOriginal:
            'Some pretty intense coastal weather is coming our way',
          descriptionTranslation: 'And this would be the translation',
          geoJSON: testGeoJSON,
        },
        status: 'DRAFT',
      };

      await api.saveWarningAs(testPublicWarning);
      expect(spy).toHaveBeenCalledWith(`/warnings/`, testPublicWarning);
    });

    it('should call with the right params for updateWarning', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi();

      const testPublicWarning: PublicWarning = {
        warningDetail: {
          id: '923723984872338768743',
          phenomenon: 'coastalEvent',
          validFrom: '2022-06-01T15:00:00Z',
          validUntil: '2022-06-01T18:00:00Z',
          level: 'extreme',
          probability: 80,
          descriptionOriginal:
            'Some pretty intense coastal weather is coming our way',
          descriptionTranslation: 'And this would be the translation',
          geoJSON: testGeoJSON,
        },
        status: 'DRAFT',
      };

      await api.updateWarning(
        testPublicWarning.warningDetail.id,
        testPublicWarning,
      );
      expect(spy).toHaveBeenCalledWith(
        `/warnings/${testPublicWarning.warningDetail.id}/`,
        testPublicWarning,
      );
    });
    it('should call with the right params for deleteWarning', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi();
      const warningId = 'test-1';
      await api.deleteWarning(warningId);
      expect(spy).toHaveBeenCalledWith(`/warnings/${warningId}?delete=true`);
    });

    it('should call with the right params for toggleEditorWarning', async () => {
      jest.spyOn(console, 'log').mockImplementation();
      jest
        .spyOn(utils, 'createFakeApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi();
      const warningId = 'test-1';
      await api.toggleEditorWarning(warningId, true);
      expect(spy).toHaveBeenCalledWith(`/warnings/${warningId}?editor=true`);
    });
  });
});

describe('extractDrawingDetailsFromJSON', () => {
  it('should return the relevant drawingdetails', () => {
    expect(extractDrawingDetailsFromJSON('923723984872338768743')).toBe(
      drawingsListFullContent[0],
    );
    expect(extractDrawingDetailsFromJSON('345346456345')).toBe(
      drawingsListFullContent[2],
    );
    expect(extractDrawingDetailsFromJSON('34536467568')).toBe(
      drawingsListFullContent[drawingsListFullContent.length - 2],
    );
    expect(extractDrawingDetailsFromJSON('NOTPRESENT')).toBe(undefined);
  });
});
