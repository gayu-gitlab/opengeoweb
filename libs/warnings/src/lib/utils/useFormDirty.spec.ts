/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { renderHook } from '@testing-library/react';
import { useFormDirty } from './useFormDirty';

describe('useFormDirty', () => {
  it('should trigger onFormDirty when isDirty changes', () => {
    const onFormDirtyMock = jest.fn();

    const { rerender } = renderHook(
      ({ isDirty }) =>
        useFormDirty({
          isDirty,
          isLoading: false,
          error: undefined,
          onFormDirty: onFormDirtyMock,
          onSuccess: jest.fn(),
        }),
      {
        initialProps: { isDirty: false },
      },
    );

    rerender({ isDirty: true });

    expect(onFormDirtyMock).toHaveBeenCalledWith(true);
  });

  it('should trigger onSuccess when isLoading becomes false and there is no error', () => {
    const onSuccessMock = jest.fn();

    const { rerender } = renderHook(
      ({ isLoading, error }) =>
        useFormDirty({
          isDirty: false,
          isLoading,
          error,
          onFormDirty: jest.fn(),
          onSuccess: onSuccessMock,
        }),
      {
        initialProps: { isLoading: true, error: undefined },
      },
    );

    rerender({ isLoading: false, error: undefined });

    expect(onSuccessMock).toHaveBeenCalled();
  });

  it('should not trigger onSuccess when isLoading becomes false but there is an error', () => {
    const onSuccessMock = jest.fn();

    const { rerender } = renderHook(
      ({ isLoading, error }) =>
        useFormDirty({
          isDirty: false,
          isLoading,
          error,
          onFormDirty: jest.fn(),
          onSuccess: onSuccessMock,
        }),
      {
        initialProps: { isLoading: true, error: 'Some error' },
      },
    );

    rerender({ isLoading: false, error: 'Some error' });

    expect(onSuccessMock).not.toHaveBeenCalled();
  });
});
