/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import type {
  DrawingFromBE,
  DrawingListItem,
} from '../../store/drawings/types';
import type { PublicWarning } from '../../store/publicWarningForm/types';
import drawingListJSON from './drawingsList.json';
import drawingsListFullContentJSON from './drawingsListFullContent.json';
import warningListJSON from './warningList.json';

const drawingList = drawingListJSON as DrawingListItem[];
const drawingsListFullContent = drawingsListFullContentJSON as DrawingFromBE[];
const warningList = warningListJSON as unknown as PublicWarning[];

export { drawingList, drawingsListFullContent, warningList };
