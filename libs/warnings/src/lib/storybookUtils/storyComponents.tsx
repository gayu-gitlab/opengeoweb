/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { useDispatch } from 'react-redux';
import { List, ListItemButton, ListItemText } from '@mui/material';
import { MapViewConnect } from '@opengeoweb/core';
import {
  getSingularDrawtoolDrawLayerId,
  mapStoreActions,
  uiActions,
  uiTypes,
} from '@opengeoweb/store';
import { MapControls, emptyGeoJSON } from '@opengeoweb/webmap-react';
import { LayerType } from '@opengeoweb/webmap';
import { AppLayout, AppHeader } from '@opengeoweb/shared';
import { useDefaultMapSettings } from './defaultStorySettings';
import {
  ObjectManagerConnect,
  ObjectManagerMapButtonConnect,
} from '../components/ObjectManager';
import {
  DrawingToolConnect,
  DrawingToolMapButtonConnect,
} from '../components/DrawingTool';
import { PublicWarningsFormDialogConnect } from '../components/PublicWarningsFormDialog';
import { publicWarningFormActions } from '../store/publicWarningForm/reducer';

export const AppHamburgerMenu: React.FC = (): React.ReactElement => {
  const dispatch = useDispatch();

  return (
    <List>
      <ListItemButton
        onClick={(): void => {
          dispatch(publicWarningFormActions.openPublicWarningFormDialog({}));
        }}
      >
        <ListItemText sx={{ fontWeight: 500 }} primary="Open public warnings" />
      </ListItemButton>
    </List>
  );
};

export const MapWithWarnings: React.FC<{
  mapId: string;
  dialogType?: uiTypes.DialogType;
}> = ({ mapId, dialogType = uiTypes.DialogTypes.ObjectManager }) => {
  useDefaultMapSettings({
    mapId,
  });
  const dispatch = useDispatch();
  React.useEffect(() => {
    // add draw layer for drawingtool
    dispatch(
      mapStoreActions.addLayer({
        mapId,
        layer: {
          // empty geoJSON
          geojson: emptyGeoJSON,
          layerType: LayerType.featureLayer,
        },
        layerId: getSingularDrawtoolDrawLayerId(mapId),
        origin: 'storyComponents/MapWithWarnings',
      }),
    );

    if (dialogType !== uiTypes.DialogTypes.DrawingTool) {
      dispatch(
        uiActions.setActiveMapIdForDialog({
          type: dialogType,
          mapId,
          setOpen: true,
          source: 'app',
        }),
      );
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <AppLayout header={<AppHeader menu={<AppHamburgerMenu />} />}>
      <ObjectManagerConnect />
      <MapControls>
        <ObjectManagerMapButtonConnect mapId={mapId} />
        <DrawingToolMapButtonConnect mapId={mapId} />
      </MapControls>
      <MapViewConnect mapId={mapId} />
      <DrawingToolConnect mapId={mapId} bounds="parent" />
      <PublicWarningsFormDialogConnect />
    </AppLayout>
  );
};
