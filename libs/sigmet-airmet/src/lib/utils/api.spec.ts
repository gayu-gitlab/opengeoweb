/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as utils from '@opengeoweb/api';
import { CreateApiProps, createFakeApiInstance } from '@opengeoweb/api';
import {
  Airmet,
  AirmetFromFrontend,
  Sigmet,
  SigmetFromFrontend,
} from '../types';
import { createApi } from './api';
import { fakeAirmetList } from './mockdata/fakeAirmetList';
import { fakeSigmetList } from './mockdata/fakeSigmetList';

describe('src/utils/api', () => {
  const fakeAxiosInstance = createFakeApiInstance();
  const fakeApiParams: CreateApiProps = {
    config: {
      baseURL: 'fakeURL',
      appURL: 'fakeUrl',
      authTokenURL: 'anotherFakeUrl',
      authClientId: 'fakeauthClientId',
    },
    auth: {
      username: 'Michael Jackson',
      token: '1223344',
      refresh_token: '33455214',
    },
    onSetAuth: jest.fn(),
  };

  const sigmetConfigKey = 'sigmet.json';
  const airmetConfigKey = 'airmet.json';

  describe('createApi', () => {
    it('should contain all api calls', async () => {
      const api = createApi(fakeApiParams);
      expect(api.getSigmetConfiguration).toBeTruthy();
      expect(api.getSigmetList).toBeTruthy();
      expect(api.postSigmet).toBeTruthy();
      expect(api.getSigmetTAC).toBeTruthy();
      expect(api.getAirmetConfiguration).toBeTruthy();
      expect(api.getAirmetTAC).toBeTruthy();
      expect(api.getAirmetList).toBeTruthy();
      expect(api.postAirmet).toBeTruthy();
    });

    it('should call with the right params for getAirmetConfiguration', async () => {
      jest
        .spyOn(utils, 'createNonAuthApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      await api.getAirmetConfiguration(airmetConfigKey);
      expect(spy).toHaveBeenCalledWith(`/assets/${airmetConfigKey}`);
    });

    it('should call with the right params for getSigmetConfiguration', async () => {
      jest
        .spyOn(utils, 'createNonAuthApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      await api.getSigmetConfiguration(sigmetConfigKey);
      expect(spy).toHaveBeenCalledWith(`/assets/${sigmetConfigKey}`);
    });

    it('should call backend when no productConfigKey provided for getAirmetConfiguration', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      await api.getAirmetConfiguration();
      expect(spy).toHaveBeenCalledWith(`/airmet-config`);
    });

    it('should call backend when no productConfigKey provided for getSigmetConfiguration', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      await api.getSigmetConfiguration();
      expect(spy).toHaveBeenCalledWith(`/sigmet-config`);
    });

    it('should call with the right params for getSigmetList', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      await api.getSigmetList();
      expect(spy).toHaveBeenCalledWith('/sigmetlist');
    });
    it('should call with the right params for postSigmet', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(fakeApiParams);

      const params: SigmetFromFrontend = {
        changeStatusTo: 'CANCELLED',
        sigmet: fakeSigmetList[0].sigmet,
      };
      await api.postSigmet(params);
      expect(spy).toHaveBeenCalledWith('/sigmet', {
        ...params,
      });
    });
    it('should call with the right params for getSigmetTAC', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(fakeApiParams);

      const params = fakeSigmetList[0].sigmet as Sigmet;
      await api.getSigmetTAC(params);
      expect(spy).toHaveBeenCalledWith('/sigmet2tac', {
        ...params,
      });
    });
    it('should call with the right params for getAirmetTAC', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'post');
      const api = createApi(fakeApiParams);

      const params = fakeAirmetList[0].airmet as Airmet;
      await api.getAirmetTAC(params);
      expect(spy).toHaveBeenCalledWith('/airmet2tac', {
        ...params,
      });
    });
    it('should call with the right params for getAirmetList', async () => {
      jest
        .spyOn(utils, 'createApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      await api.getAirmetList();
      expect(spy).toHaveBeenCalledWith('/airmetlist');
    });
  });
  it('should call with the right params for postAirmet', async () => {
    jest
      .spyOn(utils, 'createApiInstance')
      .mockReturnValueOnce(fakeAxiosInstance);
    const spy = jest.spyOn(fakeAxiosInstance, 'post');
    const api = createApi(fakeApiParams);

    const params: AirmetFromFrontend = {
      changeStatusTo: 'CANCELLED',
      airmet: fakeAirmetList[0].airmet,
    };
    await api.postAirmet(params);
    expect(spy).toHaveBeenCalledWith('/airmet', {
      ...params,
    });
  });
});
