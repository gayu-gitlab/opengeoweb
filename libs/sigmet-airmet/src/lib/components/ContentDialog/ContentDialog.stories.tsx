/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Typography, Button, Box } from '@mui/material';
import { ThemeWrapper } from '@opengeoweb/theme';
import ContentDialog from './ContentDialog';
import { getProductFormLifecycleButtons } from '../ProductForms/utils';
import { fakeAirmetList } from '../../utils/mockdata/fakeAirmetList';
import { ProductCanbe } from '../../types';
import { StoryWrapperFakeApi } from '../../utils/testUtils';

export default { title: 'components/Content Dialog' };

export const ContentDialogDemo = (): React.ReactElement => {
  const [open, setOpen] = React.useState(false);
  const handleToggleDialog = (): void => {
    setOpen(!open);
  };

  return (
    <ThemeWrapper>
      <Button variant="outlined" onClick={handleToggleDialog}>
        Open dialog
      </Button>
      <ContentDialog
        open={open}
        toggleDialogStatus={handleToggleDialog}
        title="Example"
        options={
          <Box sx={{ button: { minWidth: 96, marginLeft: 1 } }}>
            <Button variant="flat">Yes</Button>
            <Button variant="tertiary">No</Button>
            <Button variant="primary">Maybe</Button>
          </Box>
        }
      >
        <Typography variant="subtitle1">Hello World</Typography>
        <Typography variant="body1">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin
          tincidunt feugiat est nec porttitor. Sed quis nulla commodo, fringilla
          mi fringilla, hendrerit justo. Curabitur et felis non dolor congue
          tincidunt. Nunc elementum scelerisque dictum. Sed blandit malesuada
          sem quis rhoncus. Lorem ipsum dolor sit amet, consectetur adipiscing
          elit. Nullam tincidunt tellus et sagittis dapibus. Cras sed vehicula
          quam, volutpat lobortis massa. Etiam quis elementum orci. Ut iaculis
          posuere bibendum. Nam eu sapien et metus sagittis sagittis id et dui.
          Integer vitae lacinia nulla, et laoreet libero. Duis sit amet quam
          luctus, dapibus odio luctus, pharetra metus. Quisque accumsan, lectus
          nec pharetra mollis, neque neque malesuada quam, dictum dapibus purus
          metus in quam. Vestibulum facilisis ornare tortor, quis accumsan nibh
          rutrum vel. Mauris rhoncus est id eros volutpat maximus. Nunc leo
          enim, semper et neque nec, pellentesque faucibus lorem. Sed pretium
          condimentum ipsum, id commodo purus viverra sit amet. Integer
          elementum velit non elit pellentesque lacinia. Nullam accumsan
          efficitur turpis, eget suscipit purus placerat sed. Ut vestibulum
          tempus vehicula. Quisque eu lectus sit amet libero feugiat
          ullamcorper. Aenean egestas, nulla vel dapibus porta, dolor purus
          pellentesque nisi, vitae iaculis dui nunc accumsan metus. Donec
          efficitur viverra nibh at porttitor. Pellentesque diam metus, mollis
          in placerat nec, auctor vitae est. Sed condimentum efficitur risus id
          vehicula. Nam luctus lacus nec urna varius dignissim.
        </Typography>
      </ContentDialog>
    </ThemeWrapper>
  );
};

interface DialogDemoProps {
  canbe: ProductCanbe[];
  title: string;
  isDarkTheme?: boolean;
}

const DialogDemo: React.FC<DialogDemoProps> = ({
  canbe,
  title,
  isDarkTheme,
}: DialogDemoProps) => (
  <StoryWrapperFakeApi isDarkTheme={isDarkTheme}>
    <ContentDialog
      open
      toggleDialogStatus={(): void => {
        /* Do nothing */
      }}
      title={`Example ${title}`}
      options={getProductFormLifecycleButtons({
        canBe: canbe,
        onButtonPress: () => {},
      })}
    >
      <Box
        sx={{
          minWidth: 1024,
        }}
      >
        <Typography variant="h5">{title}</Typography>
      </Box>
    </ContentDialog>
  </StoryWrapperFakeApi>
);

const parametersLightTheme = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a4934891039316017011cd/version/63453b21d90f056999965f74',
    },
  ],
};

const parametersDarkTheme = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/61a4935476031e13fb0f3ab6/version/63453b3bb0834e6a236fa210',
    },
  ],
};

// New
export const NewDialogDemoLight = (): React.ReactElement => (
  <DialogDemo canbe={fakeAirmetList[0].canbe} title="New" />
);

NewDialogDemoLight.storyName = 'Content dialog new light theme (takeSnapshot)';
NewDialogDemoLight.parameters = parametersLightTheme;

export const NewDialogDemoDark = (): React.ReactElement => (
  <DialogDemo isDarkTheme canbe={fakeAirmetList[0].canbe} title="New" />
);

NewDialogDemoDark.storyName = 'Content dialog new dark theme (takeSnapshot)';
NewDialogDemoDark.parameters = parametersDarkTheme;

// View
export const ViewDialogDemoLight = (): React.ReactElement => (
  <DialogDemo canbe={fakeAirmetList[2].canbe} title="View" />
);

ViewDialogDemoLight.storyName =
  'Content dialog view light theme (takeSnapshot)';
ViewDialogDemoLight.parameters = parametersLightTheme;

export const ViewDialogDemoDark = (): React.ReactElement => (
  <DialogDemo isDarkTheme canbe={fakeAirmetList[2].canbe} title="View" />
);

ViewDialogDemoDark.storyName = 'Content dialog view dark theme (takeSnapshot)';
ViewDialogDemoDark.parameters = parametersDarkTheme;

// Expired
export const ExpiredDialogDemoLight = (): React.ReactElement => (
  <DialogDemo canbe={fakeAirmetList[7].canbe} title="Expired" />
);

ExpiredDialogDemoLight.storyName =
  'Content dialog expired view light theme (takeSnapshot)';
ExpiredDialogDemoLight.parameters = parametersLightTheme;

export const ExpiredDialogDemoDark = (): React.ReactElement => (
  <DialogDemo isDarkTheme canbe={fakeAirmetList[7].canbe} title="Expired" />
);

ExpiredDialogDemoDark.storyName =
  'Content dialog expired view dark theme (takeSnapshot)';
ExpiredDialogDemoDark.parameters = parametersDarkTheme;

// Edit
export const EditDialogDemoLight = (): React.ReactElement => (
  <DialogDemo canbe={fakeAirmetList[0].canbe} title="Edit" />
);

EditDialogDemoLight.storyName =
  'Content dialog edit view light theme (takeSnapshot)';
EditDialogDemoLight.parameters = parametersLightTheme;

export const EditDialogDemoDark = (): React.ReactElement => (
  <DialogDemo isDarkTheme canbe={fakeAirmetList[0].canbe} title="Edit" />
);

EditDialogDemoDark.storyName =
  'Content dialog edit view dark theme (takeSnapshot)';
EditDialogDemoDark.parameters = parametersDarkTheme;
