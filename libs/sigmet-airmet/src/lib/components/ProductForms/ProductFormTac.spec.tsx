/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';

import { render, waitFor, renderHook, screen } from '@testing-library/react';

import ProductFormTAC, {
  isLevelFieldsComplete,
  isSurfaceVisComplete,
  noTAC,
  shouldRetrieveTAC,
  useTAC,
  isChangeComplete,
} from './ProductFormTac';
import {
  fakeShortTestSigmet,
  fakeShortVaTestSigmet,
  fakeSigmetList,
} from '../../utils/mockdata/fakeSigmetList';
import { TestWrapper } from '../../utils/testUtils';
import {
  Airmet,
  AirmetPhenomena,
  Sigmet,
  SigmetPhenomena,
  Change,
  VisibilityCause,
} from '../../types';
import {
  fakeAirmetList,
  fakeCancelAirmet,
} from '../../utils/mockdata/fakeAirmetList';

describe('components/ProductForms/ProductFormTac', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });
  afterEach(() => {
    jest.clearAllTimers();
    jest.useRealTimers();
  });

  describe('useTAC', () => {
    it('should retrieve TAC and refetch TAC', async () => {
      const testSigmet = fakeSigmetList[0].sigmet as Sigmet;
      // test with fakeRequest; mimic the backend with only returning phenomenon
      const fakeTACRequest = (data: Sigmet): Promise<{ data: string }> =>
        new Promise((resolve) => {
          resolve({ data: data.phenomenon });
        });

      const { result } = renderHook(() => useTAC(testSigmet, fakeTACRequest));

      await waitFor(() => expect(result.current[0]).toEqual(noTAC));
      const newResponse = 'test';

      await result.current[1](() => ({
        ...testSigmet,
        phenomenon: newResponse,
      }));
      // fix for lodash debounce test problem https://github.com/facebook/jest/issues/3465
      jest.runOnlyPendingTimers();
      await waitFor(() => expect(result.current[0]).toEqual(newResponse));
    });
  });

  it('should not retreive TAC when missing values in Sigmet', async () => {
    const { validDateStart, phenomenon, ...testSigmet } = fakeSigmetList[0]
      .sigmet as Sigmet;

    // test with fakeRequest; mimic the backend with only returning phenomenon
    const fakeTACRequest = (data: Sigmet): Promise<{ data: string }> =>
      new Promise((resolve) => {
        resolve({ data: data.phenomenon });
      });

    const { result } = renderHook(() =>
      useTAC(testSigmet as Sigmet, fakeTACRequest),
    );

    await waitFor(() => expect(result.current[0]).toEqual(noTAC));
    // update with invalid value
    const newResponse = null;
    await result.current[1]({ ...testSigmet, phenomenon: newResponse });

    await waitFor(() => expect(result.current[0]).toEqual(noTAC));
  });

  it('should show error message when error occurs', async () => {
    const testSigmet = fakeSigmetList[0].sigmet as Sigmet;

    const fakeTACRequest = (): Promise<{ data: string }> =>
      new Promise((_, reject) => {
        reject(new Error('server error'));
      });

    const { result } = renderHook(() =>
      useTAC(testSigmet as Sigmet, fakeTACRequest),
    );

    await waitFor(() => expect(result.current[0]).toEqual(noTAC));
  });

  it('should not retrieve TAC and refetch TAC when form has errors', async () => {
    const testSigmet = fakeSigmetList[0].sigmet as Sigmet;
    // test with fakeRequest; mimic the backend with only returning phenomenon
    const fakeTACRequest = (data: Sigmet): Promise<{ data: string }> =>
      new Promise((resolve) => {
        resolve({ data: data.phenomenon });
      });

    const { result } = renderHook(() =>
      useTAC(testSigmet, fakeTACRequest, () => true),
    );

    await waitFor(() => expect(result.current[0]).toEqual(noTAC));
    const newResponse = 'test';

    await result.current[1](() => ({
      ...testSigmet,
      phenomenon: newResponse,
    }));
    // fix for lodash debounce test problem https://github.com/facebook/jest/issues/3465
    jest.runOnlyPendingTimers();
    await waitFor(() => expect(result.current[0]).toEqual(noTAC));
  });
});

describe('components/ProductForms/ProductFormTac', () => {
  it('shows the returned TAC once TAC is returned from BE', async () => {
    const props = {
      tac: noTAC,
    };

    render(
      <TestWrapper>
        <ProductFormTAC {...props} />
      </TestWrapper>,
    );
    expect(screen.getByTestId('productform-tac-message').textContent).toEqual(
      'Missing data: no TAC can be generated',
    );
  });
  it('should return true always for a cancel sigmet', async () => {
    expect(shouldRetrieveTAC(fakeCancelAirmet)).toBe(true);
    expect(shouldRetrieveTAC(fakeSigmetList[3].sigmet)).toBe(true);
  });
  // Minimally required: phenomenon, isObservationOrForecast, validDateStart, validDateEnd, startGeometry, startGeometryIntersect, movementType, change
  it('should return true for sigmet/airmet if all minimal required fields are present', async () => {
    expect(shouldRetrieveTAC(fakeAirmetList[0].airmet)).toBe(true);
    expect(shouldRetrieveTAC(fakeSigmetList[0].sigmet)).toBe(true);
    expect(shouldRetrieveTAC(fakeAirmetList[1].airmet)).toBe(true);
    expect(shouldRetrieveTAC(fakeSigmetList[1].sigmet)).toBe(true);
    expect(shouldRetrieveTAC(fakeAirmetList[4].airmet)).toBe(true);
    expect(shouldRetrieveTAC(fakeSigmetList[4].sigmet)).toBe(true);
    const { phenomenon: unusedPhenom, ...incompleteAirmet } = fakeAirmetList[0]
      .airmet as Airmet;
    expect(shouldRetrieveTAC(incompleteAirmet as Airmet)).toBe(false);
    const { validDateStart, ...incompleteSigmet } = fakeSigmetList[0]
      .sigmet as Sigmet;
    expect(shouldRetrieveTAC(incompleteSigmet as Sigmet)).toBe(false);
    const { isObservationOrForecast, ...incompleteAirmet2 } = fakeAirmetList[1]
      .airmet as Airmet;
    expect(shouldRetrieveTAC(incompleteAirmet2 as Airmet)).toBe(false);
    const { validDateEnd, ...incompleteSigmet2 } = fakeSigmetList[1]
      .sigmet as Sigmet;
    expect(shouldRetrieveTAC(incompleteSigmet2 as Sigmet)).toBe(false);
    const { change, ...incompleteAirmet3 } = fakeAirmetList[1].airmet as Airmet;
    expect(shouldRetrieveTAC(incompleteAirmet3 as Airmet)).toBe(false);
    const { movementType, ...incompleteSigmet3 } = fakeSigmetList[1]
      .sigmet as Sigmet;
    expect(shouldRetrieveTAC(incompleteSigmet3 as Sigmet)).toBe(false);
  });

  it('should return true for short test sigmet if all required fields are present', async () => {
    expect(shouldRetrieveTAC(fakeShortTestSigmet.sigmet)).toBe(true);
    expect(shouldRetrieveTAC(fakeShortVaTestSigmet.sigmet)).toBe(true);
    const { phenomenon, ...incompleteShortTestSigmet } =
      fakeShortTestSigmet.sigmet as Sigmet;
    expect(shouldRetrieveTAC(incompleteShortTestSigmet as Sigmet)).toBe(false);
  });

  describe('isLevelFieldsComplete', () => {
    it('should return false if not all required levels fields are filled in', () => {
      // For overcast cloud AIRMETs and broken cloud AIRMETs should return false if cloudlevels are missing
      expect(
        isLevelFieldsComplete({
          phenomenon: 'OVC_CLD' as AirmetPhenomena,
        } as unknown as Airmet),
      ).toBeFalsy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'BKN_CLD' as AirmetPhenomena,
        } as unknown as Airmet),
      ).toBeFalsy();

      expect(
        isLevelFieldsComplete({
          phenomenon: 'OVC_CLD' as AirmetPhenomena,
          cloudLevelInfoMode: 'BETW',
          cloudLowerLevel: { value: '300' },
          cloudLevel: { value: null },
        } as unknown as Airmet),
      ).toBeFalsy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'BKN_CLD' as AirmetPhenomena,
          cloudLevelInfoMode: 'BETW',
          cloudLowerLevel: { value: '300' },
          cloudLevel: { value: null },
        } as unknown as Airmet),
      ).toBeFalsy();

      expect(
        isLevelFieldsComplete({
          phenomenon: 'OVC_CLD' as AirmetPhenomena,
          cloudLevelInfoMode: 'BETW_ABV',
          cloudLevel: { value: null },
        } as unknown as Airmet),
      ).toBeFalsy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'BKN_CLD' as AirmetPhenomena,
          cloudLevelInfoMode: 'BETW_ABV',
          cloudLevel: { value: '500' },
        } as unknown as Airmet),
      ).toBeFalsy();

      expect(
        isLevelFieldsComplete({
          phenomenon: 'OVC_CLD' as AirmetPhenomena,
          cloudLevelInfoMode: 'BETW_SFC',
        } as unknown as Airmet),
      ).toBeFalsy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'BKN_CLD' as AirmetPhenomena,
          cloudLevelInfoMode: 'BETW_SFC_ABV',
        } as unknown as Airmet),
      ).toBeFalsy();

      // No levelInfoMode should return false
      expect(
        isLevelFieldsComplete({
          phenomenon: 'EMBD_TSGR' as SigmetPhenomena,
        } as Sigmet),
      ).toBeFalsy();
      // A levelInfoMode but no corresponding level value should return false
      expect(isLevelFieldsComplete({} as Sigmet)).toBeFalsy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'EMBD_TSGR' as SigmetPhenomena,
          levelInfoMode: 'AT',
        } as Sigmet),
      ).toBeFalsy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'EMBD_TSGR' as SigmetPhenomena,
          levelInfoMode: 'ABV',
        } as Sigmet),
      ).toBeFalsy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'EMBD_TSGR' as SigmetPhenomena,
          levelInfoMode: 'BETW',
        } as Sigmet),
      ).toBeFalsy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'EMBD_TSGR' as SigmetPhenomena,
          levelInfoMode: 'BETW_SFC',
        } as Sigmet),
      ).toBeFalsy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'EMBD_TSGR' as SigmetPhenomena,
          levelInfoMode: 'TOPS',
        } as Sigmet),
      ).toBeFalsy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'EMBD_TSGR' as SigmetPhenomena,
          levelInfoMode: 'TOPS_ABV',
        } as Sigmet),
      ).toBeFalsy();
      // For Between with no upper or lower level it should return false
      expect(
        isLevelFieldsComplete({
          phenomenon: 'EMBD_TSGR' as SigmetPhenomena,
          levelInfoMode: 'BETW',
          level: { value: '500' },
        } as unknown as Sigmet),
      ).toBeFalsy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'EMBD_TSGR' as SigmetPhenomena,
          levelInfoMode: 'BETW',
          lowerLevel: { value: '500' },
        } as unknown as Sigmet),
      ).toBeFalsy();
    });
    it('should return true if all required levels fields are filled in', () => {
      // For AIRMET phenomena SFC_VIS and SFC_WIND should always return true
      expect(
        isLevelFieldsComplete({
          phenomenon: 'SFC_VIS' as AirmetPhenomena,
        } as Airmet),
      ).toBeTruthy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'SFC_WIND' as AirmetPhenomena,
        } as Airmet),
      ).toBeTruthy();

      expect(
        isLevelFieldsComplete({
          phenomenon: 'OVC_CLD' as AirmetPhenomena,
          cloudLevelInfoMode: 'BETW',
          cloudLowerLevel: { value: '300' },
          cloudLevel: { value: '500' },
        } as unknown as Airmet),
      ).toBeTruthy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'BKN_CLD' as AirmetPhenomena,
          cloudLevelInfoMode: 'BETW',
          cloudLowerLevel: { value: '300' },
          cloudLevel: { value: '500' },
        } as unknown as Airmet),
      ).toBeTruthy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'OVC_CLD' as AirmetPhenomena,
          cloudLevelInfoMode: 'BETW_ABV',
          cloudLowerLevel: { value: '300' },
          cloudLevel: { value: '500' },
        } as unknown as Airmet),
      ).toBeTruthy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'BKN_CLD' as AirmetPhenomena,
          cloudLevelInfoMode: 'BETW_ABV',
          cloudLowerLevel: { value: '300' },
          cloudLevel: { value: '500' },
        } as unknown as Airmet),
      ).toBeTruthy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'OVC_CLD' as AirmetPhenomena,
          cloudLevelInfoMode: 'BETW_SFC_ABV',
          cloudLevel: { value: '500' },
        } as unknown as Airmet),
      ).toBeTruthy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'BKN_CLD' as AirmetPhenomena,
          cloudLevelInfoMode: 'BETW_SFC',
          cloudLevel: { value: '500' },
        } as unknown as Airmet),
      ).toBeTruthy();

      expect(
        isLevelFieldsComplete({
          phenomenon: 'EMBD_TSGR' as SigmetPhenomena,
          levelInfoMode: 'BETW',
          lowerLevel: { value: '300' },
          level: { value: '500' },
        } as unknown as Sigmet),
      ).toBeTruthy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'EMBD_TSGR' as SigmetPhenomena,
          levelInfoMode: 'AT',
          level: { value: '500' },
        } as unknown as Sigmet),
      ).toBeTruthy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'EMBD_TSGR' as SigmetPhenomena,
          levelInfoMode: 'ABV',
          level: { value: '500' },
        } as unknown as Sigmet),
      ).toBeTruthy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'EMBD_TSGR' as SigmetPhenomena,
          levelInfoMode: 'BETW_SFC',
          level: { value: '500' },
        } as unknown as Sigmet),
      ).toBeTruthy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'EMBD_TSGR' as SigmetPhenomena,
          levelInfoMode: 'TOPS',
          level: { value: '500' },
        } as unknown as Sigmet),
      ).toBeTruthy();
      expect(
        isLevelFieldsComplete({
          phenomenon: 'EMBD_TSGR' as SigmetPhenomena,
          levelInfoMode: 'TOPS_ABV',
          level: { value: '500' },
        } as unknown as Sigmet),
      ).toBeTruthy();
    });
  });

  describe('isChangeComplete', () => {
    it('should return false if change in intensity value is empty', () => {
      expect(
        isChangeComplete({
          phenomenon: 'EMBD_TS' as SigmetPhenomena,
          change: '' as Change,
        } as Sigmet),
      ).toBeFalsy();
      expect(
        isChangeComplete({
          phenomenon: 'SFC_VIS' as AirmetPhenomena,
          change: '' as Change,
        } as Airmet),
      ).toBeFalsy();
    });
    it('should return true if change in intensity value is given', () => {
      expect(
        isChangeComplete({
          phenomenon: 'VA_CLD' as SigmetPhenomena,
          change: 'WKN' as Change,
        } as Sigmet),
      ).toBeTruthy();
      expect(
        isChangeComplete({
          phenomenon: 'SFC_VIS' as AirmetPhenomena,
          change: 'NC' as Change,
        } as Airmet),
      ).toBeTruthy();
    });
    it('should return true for a sigmet with volcanic ash cloud if change is omitted', () => {
      expect(
        isChangeComplete(
          {
            phenomenon: 'VA_CLD' as SigmetPhenomena,
            change: '' as Change,
          } as Sigmet,
          true,
        ),
      ).toBeTruthy();
    });
    it('should return true for a sigmet with radioactive cloud if change is omitted', () => {
      expect(
        isChangeComplete(
          {
            phenomenon: 'RDOACT_CLD' as SigmetPhenomena,
            change: '' as Change,
          } as Sigmet,
          true,
        ),
      ).toBeTruthy();
    });
  });

  describe('isSurfaceVisComplete', () => {
    it('should return false if not all required surface visibility fields are filled in', () => {
      expect(
        isSurfaceVisComplete({
          phenomenon: 'SFC_VIS' as AirmetPhenomena,
        } as unknown as Airmet),
      ).toBeFalsy();
      expect(
        isSurfaceVisComplete({
          phenomenon: 'SFC_VIS' as AirmetPhenomena,
          visibilityCause: '' as VisibilityCause,
        } as unknown as Airmet),
      ).toBeFalsy();
    });
    it('should return true if all required surface visibility fields are filled in', () => {
      expect(
        isSurfaceVisComplete({
          phenomenon: 'SFC_VIS' as AirmetPhenomena,
          visibilityCause: 'DZ' as VisibilityCause,
        } as unknown as Airmet),
      ).toBeTruthy();
    });
  });
});
