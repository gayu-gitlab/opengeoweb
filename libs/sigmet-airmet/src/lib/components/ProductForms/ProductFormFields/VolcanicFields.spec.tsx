/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
  errorMessages,
} from '@opengeoweb/form-fields';

import VolcanicFields from './VolcanicFields';

describe('components/ProductForms/ProductFormFields/VolcanicFields', () => {
  it('should not show any errors when entering a valid name and coordinates', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <VolcanicFields
          isDisabled={false}
          isReadOnly={false}
          helperText="Optional"
        />
      </ReactHookFormProvider>,
    );

    const volcanoName = screen.getByRole('textbox', { name: 'Volcano name' });
    const latitude = screen.getByRole('textbox', { name: 'Latitude' });
    const longitude = screen.getByRole('textbox', { name: 'Longitude' });
    fireEvent.change(volcanoName, { target: { value: 'Etna' } });
    fireEvent.change(latitude, { target: { value: '5' } });
    fireEvent.change(longitude, { target: { value: '-5' } });

    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);
    });
  });

  it('should show the helpertext', () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            vaSigmetVolcanoName: '',
            vaSigmetVolcanoCoordinates: {
              latitude: '',
              longitude: '',
            },
          },
        }}
      >
        <VolcanicFields
          isDisabled={false}
          isReadOnly={false}
          helperText="Optional"
        />
      </ReactHookFormProvider>,
    );

    expect(screen.getAllByText('Optional').length).toEqual(1);
  });

  it('should show the volcanic fields as disabled', () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            vaSigmetVolcanoName: 'ETNA',
            vaSigmetVolcanoCoordinates: {
              latitude: '63.62',
              longitude: '-19.61',
            },
          },
        }}
      >
        <VolcanicFields isDisabled isReadOnly={false} helperText="Optional" />
      </ReactHookFormProvider>,
    );
    const volcanoName = screen.getByRole('textbox', { name: 'Volcano name' });
    expect(volcanoName.getAttribute('value')).toEqual('ETNA');
    expect(volcanoName.getAttribute('class')).toContain('Mui-disabled');

    const latitude = screen.getByRole('textbox', { name: 'Latitude' });
    expect(latitude.getAttribute('value')).toEqual('63.62');
    expect(latitude.getAttribute('class')).toContain('Mui-disabled');

    const longitude = screen.getByRole('textbox', { name: 'Longitude' });
    expect(longitude!.getAttribute('value')).toEqual('-19.61');
    expect(longitude!.getAttribute('class')).toContain('Mui-disabled');
  });

  it('should show the volcanic fields when the fields are filled in and form is readonly', async () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            vaSigmetVolcanoName: 'ETNA',
            vaSigmetVolcanoCoordinates: {
              latitude: '63.62',
              longitude: '-19.61',
            },
          },
        }}
      >
        <VolcanicFields isDisabled isReadOnly helperText="Optional" />
      </ReactHookFormProvider>,
    );

    expect(await screen.findByText('Volcano name')).toBeTruthy();
    expect(await screen.findByText('Latitude')).toBeTruthy();
    expect(await screen.findByText('Longitude')).toBeTruthy();
  });

  it('should not show the volcanic fields when they are not filled in and form is readonly', () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            vaSigmetVolcanoName: '',
            vaSigmetVolcanoCoordinates: {
              latitude: '',
              longitude: '',
            },
          },
        }}
      >
        <VolcanicFields isDisabled isReadOnly helperText="Optional" />
      </ReactHookFormProvider>,
    );

    expect(screen.queryByRole('textbox', { name: 'Volcano name' })).toBeFalsy();
    expect(screen.queryByRole('textbox', { name: 'Latitude' })).toBeFalsy();
    expect(screen.queryByRole('textbox', { name: 'Longitude' })).toBeFalsy();
  });

  it('should show the volcano name in uppercase', async () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            vaSigmetVolcanoName: '',
            vaSigmetVolcanoCoordinates: {
              latitude: '',
              longitude: '',
            },
          },
        }}
      >
        <VolcanicFields
          isDisabled={false}
          isReadOnly={false}
          helperText="Optional"
        />
      </ReactHookFormProvider>,
    );

    const volcanoName = screen.getByRole('textbox', { name: 'Volcano name' });
    // fireEvent of simulating user typing the volcano name in lowercase
    fireEvent.change(volcanoName, { target: { value: 'Etna' } });
    // expected result with volcano name in uppercase
    await waitFor(() =>
      expect(volcanoName.getAttribute('value')).toEqual('ETNA'),
    );
  });

  it('should show an error message for longitude when only latitude is filled in', async () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            vaSigmetVolcanoName: 'ETNA',
            vaSigmetVolcanoCoordinates: {
              latitude: '',
              longitude: '',
            },
          },
        }}
      >
        <VolcanicFields
          isDisabled={false}
          isReadOnly={false}
          helperText="Optional"
        />
      </ReactHookFormProvider>,
    );

    const latitudeInput = screen.getByRole('textbox', { name: 'Latitude' });

    fireEvent.change(latitudeInput, { target: { value: 5 } });

    const longitudeInput = screen.getByRole('textbox', { name: 'Longitude' });

    await waitFor(() =>
      expect(
        longitudeInput.getAttribute('aria-invalid') === 'true',
      ).toBeTruthy(),
    );

    expect(screen.getByRole('alert').textContent).toEqual(
      'This field is required',
    );
  });

  it('should show an error message for latitude when only longitude is filled in', async () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            vaSigmetVolcanoName: 'ETNA',
            vaSigmetVolcanoCoordinates: {
              latitude: '',
              longitude: '',
            },
          },
        }}
      >
        <VolcanicFields
          isDisabled={false}
          isReadOnly={false}
          helperText="Optional"
        />
      </ReactHookFormProvider>,
    );

    const longitudeInput = screen.getByRole('textbox', { name: 'Longitude' });

    fireEvent.change(longitudeInput, { target: { value: -5 } });

    const latitudeInput = screen.getByRole('textbox', { name: 'Latitude' });

    await waitFor(() =>
      expect(
        latitudeInput.getAttribute('aria-invalid') === 'true',
      ).toBeTruthy(),
    );

    expect(screen.getByRole('alert').textContent).toEqual(
      'This field is required',
    );
  });

  it('should show an error message for latitude when entering an invalid value', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <VolcanicFields
          isDisabled={false}
          isReadOnly={false}
          helperText="Optional"
        />
      </ReactHookFormProvider>,
    );

    const latitudeInput = screen.getByRole('textbox', { name: 'Latitude' });

    fireEvent.change(latitudeInput, { target: { value: 91 } });

    await waitFor(() =>
      expect(
        latitudeInput.getAttribute('aria-invalid') === 'true',
      ).toBeTruthy(),
    );

    expect(screen.getByText(errorMessages.isLatitude)).toBeTruthy();
  });

  it('should show an error message for longitude when entering an invalid value', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <VolcanicFields
          isDisabled={false}
          isReadOnly={false}
          helperText="Optional"
        />
      </ReactHookFormProvider>,
    );

    const longitudeInput = screen.getByRole('textbox', { name: 'Longitude' });

    fireEvent.change(longitudeInput, { target: { value: 181 } });

    await waitFor(() =>
      expect(
        longitudeInput.getAttribute('aria-invalid') === 'true',
      ).toBeTruthy(),
    );

    expect(screen.getByText(errorMessages.isLongitude)).toBeTruthy();
  });

  it('should be allowed to enter coordinates with decimals', async () => {
    render(
      <ReactHookFormProvider
        options={{
          ...defaultFormOptions,
        }}
      >
        <VolcanicFields
          isDisabled={false}
          isReadOnly={false}
          helperText="Optional"
        />
      </ReactHookFormProvider>,
    );

    const latitudeInput = screen.getByRole('textbox', { name: 'Latitude' });
    const longitudeInput = screen.getByRole('textbox', { name: 'Longitude' });

    fireEvent.change(latitudeInput, { target: { value: 53.48 } });
    fireEvent.change(longitudeInput, { target: { value: 5.21 } });

    await waitFor(() => {
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);
    });
  });
});
