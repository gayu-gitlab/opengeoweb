/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';
import { Button } from '@mui/material';
import { useFormContext } from 'react-hook-form';

import SelectFIR, { getFirLocationDefaultValues } from './SelectFIR';
import { sigmetConfig } from '../../../utils/config';
import { getFir } from '../utils';
import { AirmetConfig, SigmetConfig } from '../../../types';

const testFirArea = getFir(sigmetConfig);

describe('components/ProductForms/ProductFormFields/SelectFIR', () => {
  describe('getFirLocationDefaultValues', () => {
    it('should return default values for fir location', () => {
      const mockConfig: AirmetConfig = {
        location_indicator_mwo: 'EHDB',
        active_firs: ['EHAA', 'EBBU'],
        valid_from_delay_minutes: 30,
        default_validity_minutes: 90,
        fir_areas: {
          EHAA: {
            fir_name: 'AMSTERDAM FIR',
            fir_location: testFirArea,
            location_indicator_atsr: 'EHAA',
            location_indicator_atsu: 'EHAA',
            max_hours_of_validity: 4,
            hours_before_validity: 4,
            cloud_lower_level_min: {
              FT: 100,
            },
            cloud_lower_level_max: {
              FT: 900,
            },
            cloud_level_min: {
              FT: 100,
            },
            cloud_level_max: {
              FT: 9900,
            },
            wind_speed_min: {
              KT: 31,
            },
            wind_speed_max: {
              KT: 199,
            },
            visibility_max: 4900,
            visibility_min: 0,
            level_min: {
              FT: 100,
              FL: 50,
            },
            level_max: {
              FT: 4900,
              FL: 100,
            },
            movement_min: {
              KT: 5,
              KMH: 10,
            },
            movement_max: {
              KT: 150,
              KMH: 99,
            },
            movement_rounding_kt: 5,
            movement_rounding_kmh: 10,
            units: [],
          },
          EBBU: {
            fir_name: 'BRUSSEL FIR',
            fir_location: testFirArea,
            location_indicator_atsr: 'EBBU',
            location_indicator_atsu: 'EBBU',
            max_hours_of_validity: 4,
            hours_before_validity: 4,
            cloud_lower_level_min: {
              FT: 100,
            },
            cloud_lower_level_max: {
              FT: 900,
            },
            cloud_level_min: {
              FT: 100,
            },
            cloud_level_max: {
              FT: 9900,
            },
            wind_speed_min: {
              KT: 31,
            },
            wind_speed_max: {
              KT: 199,
            },
            visibility_max: 4900,
            visibility_min: 0,
            level_min: {
              FT: 100,
              FL: 50,
            },
            level_max: {
              FT: 4900,
              FL: 100,
            },
            movement_min: {
              KT: 5,
              KMH: 10,
            },
            movement_max: {
              KT: 150,
              KMH: 99,
            },
            movement_rounding_kt: 5,
            movement_rounding_kmh: 10,
            units: [],
          },
        },
      };
      const result = getFirLocationDefaultValues(mockConfig);
      expect(result.locationIndicatorATSR).toEqual(
        mockConfig.fir_areas.EHAA.location_indicator_atsr,
      );
      expect(result.locationIndicatorATSU).toEqual(
        mockConfig.fir_areas.EHAA.location_indicator_atsu,
      );
      expect(result.locationIndicatorMWO).toEqual(
        mockConfig.location_indicator_mwo,
      );
      expect(result.firName).toEqual(mockConfig.fir_areas.EHAA.fir_name);
      expect(result.firGeometry).toEqual(getFir(mockConfig));
    });

    it('should return the correct current fir', () => {
      const mockConfig: AirmetConfig = {
        location_indicator_mwo: 'EHDB',
        active_firs: ['EHAA', 'EBBU'],
        valid_from_delay_minutes: 30,
        default_validity_minutes: 90,
        fir_areas: {
          EHAA: {
            fir_name: 'AMSTERDAM FIR',
            fir_location: testFirArea,
            location_indicator_atsr: 'EHAA',
            location_indicator_atsu: 'EHAA',
            max_hours_of_validity: 4,
            hours_before_validity: 4,
            units: [],
          },
          EBBU: {
            fir_name: 'BRUSSEL FIR',
            fir_location: testFirArea,
            location_indicator_atsr: 'EBBU',
            location_indicator_atsu: 'EBBU',
            max_hours_of_validity: 4,
            hours_before_validity: 4,
            units: [],
          },
        },
      };

      const resultWithoutCurrentFIR = getFirLocationDefaultValues(mockConfig);
      expect(resultWithoutCurrentFIR.locationIndicatorATSR).toEqual(
        mockConfig.fir_areas.EHAA.location_indicator_atsu,
      );
      expect(resultWithoutCurrentFIR.locationIndicatorATSU).toEqual(
        mockConfig.fir_areas.EHAA.location_indicator_atsu,
      );
      expect(resultWithoutCurrentFIR.locationIndicatorMWO).toEqual(
        mockConfig.location_indicator_mwo,
      );
      expect(resultWithoutCurrentFIR.firName).toEqual(
        mockConfig.fir_areas.EHAA.fir_name,
      );
      expect(resultWithoutCurrentFIR.firGeometry).toEqual(
        mockConfig.fir_areas.EHAA.fir_location,
      );

      const resultWithCurrentFIR = getFirLocationDefaultValues(
        mockConfig,
        'EBBU',
      );
      expect(resultWithCurrentFIR.locationIndicatorATSR).toEqual(
        mockConfig.fir_areas.EBBU.location_indicator_atsu,
      );
      expect(resultWithCurrentFIR.locationIndicatorATSU).toEqual(
        mockConfig.fir_areas.EBBU.location_indicator_atsu,
      );
      expect(resultWithCurrentFIR.locationIndicatorMWO).toEqual(
        mockConfig.location_indicator_mwo,
      );
      expect(resultWithCurrentFIR.firName).toEqual(
        mockConfig.fir_areas.EBBU.fir_name,
      );
      expect(resultWithCurrentFIR.firGeometry).toEqual(
        mockConfig.fir_areas.EBBU.fir_location,
      );
    });
  });

  it('should be possible to select a FIR', async () => {
    const props = {
      productType: 'sigmet' as const,
      productConfig: sigmetConfig,
      isDisabled: false,
      onChange: jest.fn(),
    };
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            firName: 'ROME FIR',
          },
        }}
      >
        <SelectFIR {...props} />
      </ReactHookFormProvider>,
    );
    expect(props.onChange).not.toHaveBeenCalled();
    expect(await screen.findByText('Select FIR')).toBeTruthy();
    fireEvent.mouseDown(screen.getByRole('button'));
    const menuItem = await screen.findAllByText('BRUSSEL FIR');
    fireEvent.click(menuItem[0]);
    await waitFor(() => {
      expect(screen.getByRole('button').textContent).toEqual('BRUSSEL FIR');
    });
    expect(props.onChange).toHaveBeenCalled();
  });
  it('should render correct defaultValues', async () => {
    const testConfig: SigmetConfig = {
      location_indicator_mwo: 'EHDB',
      fir_areas: {
        EHAA: {
          fir_name: 'AMSTERDAM FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EHAA',
          location_indicator_atsu: 'EHAA',
          area_preset: 'NL_FIR',
          max_hours_of_validity: 4,
          hours_before_validity: 4,
          tc_max_hours_of_validity: 6,
          tc_hours_before_validity: 12,
          va_max_hours_of_validity: 6,
          va_hours_before_validity: 12,
          adjacent_firs: ['EKDK', 'EDWW', 'EDGG', 'EBBU', 'EGTT', 'EGPX'],
          level_min: {
            FT: 100,
            FL: 50,
          },
          level_max: {
            FT: 4900,
            FL: 650,
          },
          movement_min: {
            KT: 5,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          units: [
            {
              unit_type: 'level_unit',
              allowed_units: ['FT', 'FL'],
            },
            {
              unit_type: 'movement_unit',
              allowed_units: ['KT'],
            },
          ],
        },
      },
      valid_from_delay_minutes: 30,
      default_validity_minutes: 90,
      active_firs: ['EHAA'],
    };
    render(
      <ReactHookFormProvider>
        <SelectFIR
          productType="sigmet"
          productConfig={testConfig}
          isDisabled={false}
        />
      </ReactHookFormProvider>,
    );
    await screen.findByText('Select FIR');
    await screen.findByText(testConfig.fir_areas.EHAA.fir_name);
    expect(
      screen.getByTestId('locationIndicatorATSU').getAttribute('value'),
    ).toEqual(testConfig.fir_areas.EHAA.location_indicator_atsu);
    expect(
      screen.getByTestId('locationIndicatorMWO').getAttribute('value'),
    ).toEqual(testConfig.location_indicator_mwo);
    expect(screen.getByTestId('firName').getAttribute('value')).toEqual(
      testConfig.fir_areas.EHAA.fir_name,
    );
    expect(screen.getByTestId('firGeometry').getAttribute('value')).toEqual(
      getFir(testConfig).toString(),
    );
  });
  it('should show the selectFIR as disabled', () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            firName: 'AMSTERDAM FIR',
          },
        }}
      >
        <SelectFIR
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled
        />
      </ReactHookFormProvider>,
    );
    const fir = screen.getByRole('button', {
      name: 'Select FIR AMSTERDAM FIR',
    });
    expect(fir.textContent).toEqual('AMSTERDAM FIR');
    expect(fir.getAttribute('aria-disabled') === 'true').toBeTruthy();
  });
  it('should show the selectFIR as readonly', async () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            firName: 'AMSTERDAM FIR',
          },
        }}
      >
        <SelectFIR
          productType="sigmet"
          productConfig={sigmetConfig}
          isDisabled
          isReadOnly
        />
      </ReactHookFormProvider>,
    );
    expect(await screen.findByText('FIR')).toBeTruthy();
  });
  it('should update hidden fields when changing FIR for sigmet', async () => {
    const mockConfig: SigmetConfig = {
      location_indicator_mwo: 'EHDB',
      active_firs: ['EHAA', 'EBBU'],
      fir_areas: {
        EHAA: {
          fir_name: 'AMSTERDAM FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EHAA',
          location_indicator_atsu: 'EHAA',
          max_hours_of_validity: 4,
          hours_before_validity: 4,
          tc_max_hours_of_validity: 12,
          tc_hours_before_validity: 13,
          va_max_hours_of_validity: 2,
          va_hours_before_validity: 1,
          area_preset: 'FIR',
          adjacent_firs: ['HIP', 'KNEE', 'ALPHA', 'BRAVO', 'CHARLIE'],
          level_min: {
            FT: 100,
            FL: 50,
          },
          level_max: {
            FT: 4900,
            FL: 100,
          },
          movement_min: {
            KT: 5,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          units: [],
        },
        EBBU: {
          fir_name: 'BRUSSEL FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EBBU',
          location_indicator_atsu: 'EBBU',
          max_hours_of_validity: 4,
          hours_before_validity: 4,
          tc_max_hours_of_validity: 12,
          tc_hours_before_validity: 13,
          va_max_hours_of_validity: 2,
          va_hours_before_validity: 1,
          area_preset: 'OTHER_FIR_NO_UNITS',
          adjacent_firs: ['HIP', 'KNEE', 'ALPHA', 'BRAVO', 'CHARLIE', 'BUH'],
          level_min: {
            FT: 100,
            FL: 50,
          },
          level_max: {
            FT: 4900,
            FL: 100,
          },
          movement_min: {
            KT: 5,
            KMH: 10,
          },
          movement_max: {
            KT: 150,
            KMH: 99,
          },
          movement_rounding_kt: 5,
          movement_rounding_kmh: 10,
          units: [],
        },
      },
      valid_from_delay_minutes: 30,
      default_validity_minutes: 90,
    };
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      const [result, setResult] = React.useState<Record<
        string,
        unknown
      > | null>(null);
      return (
        <>
          <SelectFIR
            productType="sigmet"
            productConfig={mockConfig}
            isDisabled={false}
          />
          <Button
            onClick={(): void => {
              handleSubmit((formvalues) => setResult(formvalues))();
            }}
          >
            Validate
          </Button>
          {result && <div data-testid="result">{JSON.stringify(result)}</div>}
        </>
      );
    };
    render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );
    // check the default values
    fireEvent.click(screen.getByText('Validate'));
    await waitFor(() => {
      expect(screen.getByTestId('result').textContent).toEqual(
        '{"locationIndicatorATSR":"EHAA","locationIndicatorATSU":"EHAA","locationIndicatorMWO":"EHDB","firName":"AMSTERDAM FIR","firGeometry":{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"Polygon","coordinates":[[[5,55],[4.331914,55.332644],[3.368817,55.764314],[2.761908,54.379261],[3.15576,52.913554],[2.000002,51.500002],[3.370001,51.369722],[3.370527,51.36867],[3.362223,51.320002],[3.36389,51.313608],[3.373613,51.309999],[3.952501,51.214441],[4.397501,51.452776],[5.078611,51.391665],[5.848333,51.139444],[5.651667,50.824717],[6.011797,50.757273],[5.934168,51.036386],[6.222223,51.361666],[5.94639,51.811663],[6.405001,51.830828],[7.053095,52.237764],[7.031389,52.268885],[7.063612,52.346109],[7.065557,52.385828],[7.133055,52.888887],[7.14218,52.898244],[7.191667,53.3],[6.5,53.666667],[6.500002,55.000002],[5,55]]]},"properties":{"selectionType":"fir"}}]},"IS_DRAFT":false}',
      );
    });
    fireEvent.mouseDown(screen.getByText('AMSTERDAM FIR'));
    const menuItem = await screen.findByText('BRUSSEL FIR');
    fireEvent.click(menuItem);
    await screen.findByText('BRUSSEL FIR');
    // check that all the form fields are updated
    fireEvent.click(screen.getByText('Validate'));
    await waitFor(() => {
      expect(screen.getByTestId('result').textContent).toEqual(
        '{"locationIndicatorATSR":"EBBU","locationIndicatorATSU":"EBBU","locationIndicatorMWO":"EHDB","firName":"BRUSSEL FIR","firGeometry":{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"Polygon","coordinates":[[[5,55],[4.331914,55.332644],[3.368817,55.764314],[2.761908,54.379261],[3.15576,52.913554],[2.000002,51.500002],[3.370001,51.369722],[3.370527,51.36867],[3.362223,51.320002],[3.36389,51.313608],[3.373613,51.309999],[3.952501,51.214441],[4.397501,51.452776],[5.078611,51.391665],[5.848333,51.139444],[5.651667,50.824717],[6.011797,50.757273],[5.934168,51.036386],[6.222223,51.361666],[5.94639,51.811663],[6.405001,51.830828],[7.053095,52.237764],[7.031389,52.268885],[7.063612,52.346109],[7.065557,52.385828],[7.133055,52.888887],[7.14218,52.898244],[7.191667,53.3],[6.5,53.666667],[6.500002,55.000002],[5,55]]]},"properties":{"selectionType":"fir"}}]},"IS_DRAFT":false}',
      );
    });
  });

  it('should update hidden fields when changing FIR for airmet', async () => {
    const mockConfig: AirmetConfig = {
      location_indicator_mwo: 'EHDB',
      active_firs: ['EHAA', 'EBBU'],
      fir_areas: {
        EHAA: {
          fir_name: 'AMSTERDAM FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EHAA',
          location_indicator_atsu: 'EHAA',
          max_hours_of_validity: 4,
          hours_before_validity: 4,
          units: [],
        },
        EBBU: {
          fir_name: 'BRUSSEL FIR',
          fir_location: testFirArea,
          location_indicator_atsr: 'EBBU',
          location_indicator_atsu: 'EBBU',
          max_hours_of_validity: 4,
          hours_before_validity: 4,
          units: [],
        },
      },
      valid_from_delay_minutes: 30,
      default_validity_minutes: 90,
    };
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      const [result, setResult] = React.useState<Record<
        string,
        unknown
      > | null>(null);
      return (
        <>
          <SelectFIR
            productType="airmet"
            productConfig={mockConfig}
            isDisabled={false}
          />
          <Button
            onClick={(): void => {
              handleSubmit((formvalues) => setResult(formvalues))();
            }}
          >
            Validate
          </Button>
          {result && <div data-testid="result">{JSON.stringify(result)}</div>}
        </>
      );
    };
    render(
      <ReactHookFormProvider>
        <Wrapper />
      </ReactHookFormProvider>,
    );
    // check the default values
    await screen.findByText(mockConfig.fir_areas.EHAA.fir_name);
    expect(
      screen.getByTestId('locationIndicatorATSU').getAttribute('value'),
    ).toEqual(mockConfig.fir_areas.EHAA.location_indicator_atsu);
    expect(
      screen.getByTestId('locationIndicatorMWO').getAttribute('value'),
    ).toEqual(mockConfig.location_indicator_mwo);
    expect(screen.getByTestId('firName').getAttribute('value')).toEqual(
      mockConfig.fir_areas.EHAA.fir_name,
    );
    expect(screen.getByTestId('firGeometry').getAttribute('value')).toEqual(
      mockConfig.fir_areas.EHAA.fir_location.toString(),
    );

    // change fir
    fireEvent.mouseDown(screen.getByText('AMSTERDAM FIR'));
    const menuItem = await screen.findByText('BRUSSEL FIR');
    fireEvent.click(menuItem);

    await screen.findByText(mockConfig.fir_areas.EBBU.fir_name);

    expect(
      screen.getByTestId('locationIndicatorATSU').getAttribute('value'),
    ).toEqual(mockConfig.fir_areas.EBBU.location_indicator_atsu);
    expect(
      screen.getByTestId('locationIndicatorMWO').getAttribute('value'),
    ).toEqual(mockConfig.location_indicator_mwo);
    expect(screen.getByTestId('firName').getAttribute('value')).toEqual(
      mockConfig.fir_areas.EBBU.fir_name,
    );
    expect(screen.getByTestId('firGeometry').getAttribute('value')).toEqual(
      mockConfig.fir_areas.EBBU.fir_location.toString(),
    );
  });
});
