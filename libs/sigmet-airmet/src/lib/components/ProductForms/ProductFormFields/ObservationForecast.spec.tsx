/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  fireEvent,
  render,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import { ReactHookFormProvider } from '@opengeoweb/form-fields';
import ObservationForecast from './ObservationForecast';

describe('components/ProductForms/ProductFormFields/ObservationForecast', () => {
  it('should be possible to select a observation or forecast', async () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            isObservationOrForecast: 'FCST',
          },
        }}
      >
        <ObservationForecast isDisabled={false} isReadOnly={false} />
      </ReactHookFormProvider>,
    );

    const obsField: HTMLInputElement = within(
      screen.getByTestId('isObservationOrForecast-OBS'),
    ).getByRole('radio');
    const fcstField: HTMLInputElement = within(
      screen.getByTestId('isObservationOrForecast-FCST'),
    ).getByRole('radio');

    expect(obsField.checked).toBeFalsy();
    expect(fcstField.checked).toBeTruthy();

    fireEvent.click(obsField);

    await waitFor(() => {
      expect(obsField.checked).toBeTruthy();
    });
    expect(fcstField.checked).toBeFalsy();
  });

  it('should show the observation or forecast as disabled', () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            isObservationOrForecast: 'FCST',
          },
        }}
      >
        <ObservationForecast isDisabled isReadOnly={false} />
      </ReactHookFormProvider>,
    );
    const obsField = screen.getByTestId('isObservationOrForecast-OBS');
    const fcstField = screen.getByTestId('isObservationOrForecast-FCST');

    expect(obsField.getAttribute('class')).toContain('Mui-disabled');
    expect(fcstField.getAttribute('class')).toContain('Mui-disabled');
  });

  it('should show the observation or forecast as readonly', () => {
    render(
      <ReactHookFormProvider
        options={{
          defaultValues: {
            isObservationOrForecast: 'FCST',
          },
        }}
      >
        <ObservationForecast isDisabled isReadOnly />
      </ReactHookFormProvider>,
    );
    const obsField = screen.queryByTestId('isObservationOrForecast-OBS');
    const fcstField = screen.queryByTestId('isObservationOrForecast-FCST');

    expect(obsField).toBeNull();
    expect(fcstField!.getAttribute('class')).toContain('Mui-disabled');
  });
});
