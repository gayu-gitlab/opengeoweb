/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { Grid } from '@mui/material';
import {
  ReactHookFormRadioGroup,
  useDraftFormHelpers,
} from '@opengeoweb/form-fields';
import { styles } from '../ProductForm.styles';
import { RadioButtonAndLabel } from './RadioButtonAndLabel';
import ProductFormFieldLayout from './ProductFormFieldLayout';
import { FormFieldProps } from '../../../types';
import { useShortTestHelpers } from '../../../hooks/useShortTestHelpers';

const Change: React.FC<FormFieldProps> = ({
  isDisabled,
  isReadOnly,
}: FormFieldProps) => {
  const { isRequired: isRequiredField } = useDraftFormHelpers();
  const { isShortTest } = useShortTestHelpers();
  const isRequired = (value: string): string | boolean =>
    isShortTest() || isRequiredField(value);
  const fieldsDisabled = isDisabled || isShortTest();

  return (
    <ProductFormFieldLayout title="Change" sx={styles.containerItem}>
      <Grid item xs={12}>
        <ReactHookFormRadioGroup
          name="change"
          rules={{ validate: { isRequired } }}
          disabled={fieldsDisabled}
          isReadOnly={isReadOnly}
        >
          <RadioButtonAndLabel
            value="WKN"
            label="Weakening"
            disabled={fieldsDisabled}
            data-testid="change-WKN"
          />
          <RadioButtonAndLabel
            value="NC"
            label="No change"
            disabled={fieldsDisabled}
            data-testid="change-NC"
          />
          <RadioButtonAndLabel
            value="INTSF"
            label="Intensifying"
            disabled={fieldsDisabled}
            data-testid="change-INTSF"
          />
        </ReactHookFormRadioGroup>
      </Grid>
    </ProductFormFieldLayout>
  );
};

export default Change;
