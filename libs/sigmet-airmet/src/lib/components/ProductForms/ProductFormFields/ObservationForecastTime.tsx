/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { useFormContext } from 'react-hook-form';

import {
  ReactHookFormDateTime,
  isBefore,
  isBetween,
  isValidDate,
} from '@opengeoweb/form-fields';

import { dateUtils } from '@opengeoweb/shared';
import ProductFormFieldLayout from './ProductFormFieldLayout';
import { styles } from '../ProductForm.styles';
import { FormFieldProps } from '../../../types';
import { getFieldLabel } from '../utils';
import { useShortTestHelpers } from '../../../hooks/useShortTestHelpers';

export const ForecastTimeValidation =
  'Forecast time should lie between the valid start and valid end time';

interface ObservationForecastTimeProps extends FormFieldProps {
  helperText: string;
}

const goZeroSeconds = (val: string): string => {
  return dateUtils.dateToString(
    dateUtils.set(dateUtils.utc(val), { seconds: 0, milliseconds: 0 }),
  )!;
};

const ObservationForecastTime: React.FC<ObservationForecastTimeProps> = ({
  isDisabled,
  isReadOnly,
  helperText,
  onChange = (): void => {},
}: ObservationForecastTimeProps) => {
  const { watch } = useFormContext();
  const label = getFieldLabel('Date and time', isReadOnly!);

  const { isShortTest } = useShortTestHelpers();
  const fieldsDisabled = isDisabled || isShortTest();
  return !isReadOnly || watch('observationOrForecastTime') !== null ? (
    <ProductFormFieldLayout title="At">
      <ReactHookFormDateTime
        disablePast={watch('isObservationOrForecast') === 'FCST'}
        disableFuture={watch('isObservationOrForecast') === 'OBS'}
        name="observationOrForecastTime"
        label={label}
        rules={{
          validate: {
            isValidDate,

            isBefore: (val): boolean | string =>
              watch('isObservationOrForecast') === 'OBS' && val
                ? isBefore(val, dateUtils.getCurrentTimeAsString()) ||
                  'Observation time cannot be in the future'
                : true,

            isBetween: (val): boolean | string =>
              watch('isObservationOrForecast') === 'FCST' && val
                ? isBetween(
                    goZeroSeconds(val),
                    goZeroSeconds(watch('validDateStart')),
                    goZeroSeconds(watch('validDateEnd')),
                  ) || ForecastTimeValidation
                : true,
          },
        }}
        disabled={fieldsDisabled}
        isReadOnly={isReadOnly}
        helperText={helperText}
        onChange={(): void => onChange()}
        sx={styles.helperText}
        data-testid="obs-fcst-time"
      />
    </ProductFormFieldLayout>
  ) : null;
};

export default ObservationForecastTime;
