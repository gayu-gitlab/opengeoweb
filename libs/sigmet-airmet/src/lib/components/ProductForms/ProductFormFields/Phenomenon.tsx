/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { MenuItem } from '@mui/material';
import { useFormContext } from 'react-hook-form';
import { ReactHookFormSelect } from '@opengeoweb/form-fields';

import {
  AirmetPhenomena,
  FormFieldProps,
  SigmetPhenomena,
} from '../../../types';
import ProductFormFieldLayout from './ProductFormFieldLayout';
import { getFieldLabel, triggerValidations } from '../utils';
import { useShortTestHelpers } from '../../../hooks/useShortTestHelpers';

const Phenomenon: React.FC<FormFieldProps> = ({
  productType,
  isDisabled,
  isReadOnly,
  onChange = (): void => {},
}: FormFieldProps) => {
  const { getValues, setValue, trigger } = useFormContext();
  const { isShortTest } = useShortTestHelpers();
  const isSigmet = productType === 'sigmet';
  const phenomenaList = isSigmet ? SigmetPhenomena : AirmetPhenomena;
  const label = getFieldLabel('Phenomenon', isReadOnly!);
  const fieldsDisabled = isDisabled || isShortTest();

  return (
    <ProductFormFieldLayout title="What">
      <ReactHookFormSelect
        name="phenomenon"
        label={label}
        rules={{ required: true }}
        disabled={fieldsDisabled}
        isReadOnly={isReadOnly}
        size="small"
        data-testid="phenomenon"
        onChange={(): Promise<boolean> | void => {
          if (getValues('movementType') === 'NO_VA_EXP') {
            setValue('movementType', null);
          } else if (getValues('phenomenon') === 'RDOACT_CLD') {
            // By definition, phenomenon 'RDOACT_CLD'shall not ever move but stay stationary
            setValue('movementType', 'STATIONARY');

            // Also by definition, phenomenon 'RDOACT_CLD' is strictly defined to always span from surface to upper level
            // with no lower explicit level values defined, hence level mode shall be set to 'BETW_SFC' instead of 'BETW'.
            setValue('levelInfoMode', 'BETW_SFC');
          }

          triggerValidations(
            ['validDateStart', 'validDateEnd'],
            getValues,
            trigger,
          );
          onChange();
        }}
      >
        {Object.keys(phenomenaList).map((key) => (
          <MenuItem value={key} key={key}>
            {productType === 'sigmet'
              ? (phenomenaList as typeof SigmetPhenomena)[
                  key as keyof typeof SigmetPhenomena
                ]
              : (phenomenaList as typeof AirmetPhenomena)[
                  key as keyof typeof AirmetPhenomena
                ]}
          </MenuItem>
        ))}
      </ReactHookFormSelect>
    </ProductFormFieldLayout>
  );
};

export default Phenomenon;
