/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  fireEvent,
  render,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import {
  ReactHookFormProvider,
  defaultFormOptions,
  errorMessages,
} from '@opengeoweb/form-fields';

import Levels, {
  DEFAULT_ROUNDING_LEVELS_FL,
  DEFAULT_ROUNDING_LEVELS_FT,
  DEFAULT_ROUNDING_LEVELS_M,
  getLevelInvalidStepsForFLUnitMessage,
  getLevelInvalidStepsForFTUnitMessage,
  getLevelInvalidStepsForMUnitMessage,
  invalidLevelUnitCombinationMessage,
  invalidUnitMessage,
  validateLevels,
  validateLevelUnitCombinations,
} from './Levels';
import { LevelUnits } from '../../../types';
import { getMaxLevelValue, getMinLevelValue } from '../utils';
import { airmetConfig, sigmetConfig } from '../../../utils/config';

describe('components/ProductForms/ProductFormFields/Levels', () => {
  const user = userEvent.setup();

  const defaultSigmetFIR =
    sigmetConfig.fir_areas[sigmetConfig.active_firs[0]].location_indicator_atsu;
  const defaultAirmetFIR =
    airmetConfig.fir_areas[airmetConfig.active_firs[0]].location_indicator_atsu;

  describe('AT section', () => {
    it('should show the correct input fields when selecting level AT and TOPS', async () => {
      render(
        <ReactHookFormProvider>
          <Levels
            isDisabled={false}
            isReadOnly={false}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );
      const levelFieldAt: HTMLInputElement = within(
        screen.getByTestId('levels-AT'),
      ).getByRole('radio');

      // AT not selected
      expect(levelFieldAt.checked).toBeFalsy();
      expect(screen.queryByTestId('levels-TOPS')).toBeNull();
      expect(screen.queryByLabelText('Unit')).toBeNull();
      expect(screen.queryByLabelText('Level')).toBeNull();

      // select AT
      fireEvent.click(levelFieldAt);

      await waitFor(() => expect(levelFieldAt.checked).toBeTruthy());

      expect(screen.getByTestId('levels-TOPS')).toBeTruthy();
      expect(screen.getByLabelText('Unit')).toBeTruthy();
      expect(screen.getByLabelText('Level')).toBeTruthy();

      // should autoFocus
      expect(screen.getByLabelText('Level').matches(':focus')).toBeTruthy();

      // select TOPS
      const levelTopField: HTMLInputElement = within(
        screen.getByTestId('levels-TOPS'),
      ).getByRole('checkbox');
      fireEvent.click(levelTopField);
      await waitFor(() => expect(levelTopField.checked).toBeTruthy());
    });

    it('should show the input fields as disabled with correct values', () => {
      const testValues = {
        levelInfoMode: 'AT',
        level: {
          value: '95',
          unit: 'FL' as LevelUnits,
        },
      };
      render(
        <ReactHookFormProvider
          options={{
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled
            isReadOnly={false}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );
      const levelAt: HTMLInputElement = within(
        screen.getByTestId('levels-AT'),
      ).getByRole('radio');
      expect(levelAt.disabled).toBeTruthy();
      expect(levelAt.getAttribute('value')).toEqual(testValues.levelInfoMode);

      const levelUnit = screen.getByLabelText('Unit');
      expect(levelUnit.getAttribute('aria-disabled') === 'true').toBeTruthy();
      expect(levelUnit.textContent).toEqual(testValues.level.unit);

      const levelValue: HTMLInputElement = screen.getByLabelText('Level');
      expect(levelValue.disabled).toBeTruthy();
      expect(levelValue.getAttribute('value')).toEqual(testValues.level.value);
    });

    it('should show the input fields as readOnly with correct values', () => {
      const testValues = {
        levelInfoMode: 'AT',
        level: {
          value: '95',
          unit: 'FL' as LevelUnits,
        },
      };
      render(
        <ReactHookFormProvider
          options={{
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled
            isReadOnly
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );
      const levelAt: HTMLInputElement = within(
        screen.getByTestId('levels-AT'),
      ).getByRole('radio');

      expect(levelAt.disabled).toBeTruthy();
      expect(levelAt.getAttribute('value')).toEqual(testValues.levelInfoMode);

      const levelUnit = screen.getByLabelText('Unit');
      expect(levelUnit.getAttribute('aria-disabled') === 'true').toBeTruthy();
      expect(levelUnit.textContent).toEqual(testValues.level.unit);

      const levelValue: HTMLInputElement = screen.getByLabelText('Level');
      expect(levelValue.disabled).toBeTruthy();
      expect(levelValue.getAttribute('value')).toEqual(testValues.level.value);
    });

    it('should show an error message when level exceeds max', async () => {
      const testValues = {
        levelInfoMode: 'AT',
        level: {
          value: '',
          unit: 'FL' as LevelUnits,
        },
      };
      render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled={false}
            isReadOnly={false}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );

      const levelValueField: HTMLInputElement = screen.getByLabelText('Level');

      fireEvent.change(levelValueField, { target: { value: 1500 } });

      await screen.findByText(
        `The maximum level in FL is ${getMaxLevelValue(
          'FL',
          defaultSigmetFIR,
          sigmetConfig,
        )}`,
      );

      expect(
        levelValueField.getAttribute('aria-invalid') === 'true',
      ).toBeTruthy();

      await user.click(screen.getByLabelText('Unit'));
      await user.click(screen.getByText('ft'));

      await waitFor(() => {
        expect(
          levelValueField.getAttribute('aria-invalid') === 'true',
        ).toBeFalsy();
      });
    });

    it('should show an error message when the level value is too low', async () => {
      const testValues = {
        levelInfoMode: 'AT',
        level: {
          value: '',
          unit: 'FL' as LevelUnits,
        },
      };
      render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled={false}
            isReadOnly={false}
            productType="airmet"
            productConfig={airmetConfig}
          />
        </ReactHookFormProvider>,
      );

      const levelInput: HTMLInputElement = screen.getByLabelText('Level');

      await user.type(levelInput, '0');
      await screen.findByText(
        `The minimum level in FL is ${getMinLevelValue(
          'FL',
          defaultAirmetFIR,
          airmetConfig,
        )}`,
      );
    });
  });

  describe('BETW section', () => {
    it('should show the correct input fields when selecting level BETW and BETW_SFC', async () => {
      render(
        <ReactHookFormProvider>
          <Levels
            isDisabled={false}
            isReadOnly={false}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );
      const levelFieldBetw: HTMLInputElement = within(
        screen.getByTestId('levels-BETW'),
      ).getByRole('radio');

      // BETW not selected
      expect(levelFieldBetw.checked).toBeFalsy();
      expect(screen.queryByTestId('levels-SFC')).toBeNull();
      expect(screen.queryByLabelText('Unit')).toBeNull();
      expect(screen.queryByLabelText('Upper level')).toBeNull();
      expect(screen.queryByLabelText('Lower level')).toBeNull();

      // select BETW
      fireEvent.click(levelFieldBetw);
      await waitFor(() => expect(levelFieldBetw.checked).toBeTruthy());

      expect(screen.getByTestId('levels-SFC')).toBeTruthy();
      expect(screen.getAllByLabelText('Unit').length).toEqual(2);
      expect(screen.getByLabelText('Upper level')).toBeTruthy();
      expect(screen.getByLabelText('Lower level')).toBeTruthy();

      // should autoFocus
      expect(
        screen.getByLabelText('Upper level').matches(':focus'),
      ).toBeTruthy();

      // select BETW_SFC
      const levelSfcField: HTMLInputElement = within(
        screen.getByTestId('levels-SFC'),
      ).getByRole('checkbox');
      fireEvent.click(levelSfcField);
      await waitFor(() => expect(levelSfcField.checked).toBeTruthy());
    });
    it('should show the input fields as disabled with correct values', () => {
      const testValues = {
        levelInfoMode: 'BETW',
        level: {
          value: '75',
          unit: 'FL' as LevelUnits,
        },
        lowerLevel: {
          value: '500',
          unit: 'FL' as LevelUnits,
        },
      };
      render(
        <ReactHookFormProvider
          options={{
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled
            isReadOnly={false}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );

      const levelBetw: HTMLInputElement = within(
        screen.getByTestId('levels-BETW'),
      ).getByRole('radio');
      expect(levelBetw.disabled).toBeTruthy();
      expect(levelBetw.getAttribute('value')).toEqual(testValues.levelInfoMode);

      const levelUnit = screen.getAllByLabelText('Unit')[0];
      expect(levelUnit.getAttribute('aria-disabled') === 'true').toBeTruthy();
      expect(levelUnit.textContent).toEqual(testValues.level.unit);

      const levelValue: HTMLInputElement = screen.getByLabelText('Upper level');
      expect(levelValue.disabled).toBeTruthy();
      expect(levelValue.getAttribute('value')).toEqual(testValues.level.value);

      const lowerLevelUnit = screen.getAllByLabelText('Unit')[1];
      expect(
        lowerLevelUnit.getAttribute('aria-disabled') === 'true',
      ).toBeTruthy();
      expect(lowerLevelUnit.textContent).toEqual(testValues.lowerLevel.unit);

      const lowerLevelValue: HTMLInputElement =
        screen.getByLabelText('Lower level');
      expect(lowerLevelValue.disabled).toBeTruthy();
      expect(lowerLevelValue.getAttribute('value')).toEqual(
        testValues.lowerLevel.value,
      );
    });
    it('should show the input fields as readOnly with correct values', () => {
      const testValues = {
        levelInfoMode: 'BETW',
        level: {
          value: '95',
          unit: 'FL' as LevelUnits,
        },
        lowerLevel: {
          value: '4200',
          unit: 'FT' as LevelUnits,
        },
      };
      render(
        <ReactHookFormProvider
          options={{
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled
            isReadOnly
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );
      const levelBetw: HTMLInputElement = within(
        screen.getByTestId('levels-BETW'),
      ).getByRole('radio');
      expect(levelBetw.disabled).toBeTruthy();
      expect(levelBetw.getAttribute('value')).toEqual(testValues.levelInfoMode);

      const levelUnit = screen.getAllByLabelText('Unit')[0];
      expect(levelUnit.getAttribute('aria-disabled') === 'true').toBeTruthy();
      expect(levelUnit.textContent).toEqual(testValues.level.unit);

      const levelValue: HTMLInputElement = screen.getByLabelText('Upper level');
      expect(levelValue.disabled).toBeTruthy();
      expect(levelValue.getAttribute('value')).toEqual(testValues.level.value);

      const lowerLevelUnit = screen.getAllByLabelText('Unit')[1];
      expect(
        lowerLevelUnit.getAttribute('aria-disabled') === 'true',
      ).toBeTruthy();
      expect(lowerLevelUnit.textContent).toEqual('ft');

      const lowerLevelValue: HTMLInputElement =
        screen.getByLabelText('Lower level');
      expect(lowerLevelValue.disabled).toBeTruthy();
      expect(lowerLevelValue.getAttribute('value')).toEqual(
        testValues.lowerLevel.value,
      );
    });
    it('should show error when switching to a upperlevel unit with a lower max value', async () => {
      const testValues = {
        levelInfoMode: 'BETW',
        level: {
          value: 1000,
          unit: 'FT' as LevelUnits,
        },
        lowerLevel: {
          value: 150,
          unit: 'FT' as LevelUnits,
        },
      };
      render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled={false}
            isReadOnly={false}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );
      const upperLevelUnit = screen.getAllByLabelText('Unit')[0];

      expect(
        screen.queryByLabelText('Upper level')!.getAttribute('aria-invalid') ===
          'true',
      ).toBeFalsy();

      await user.click(upperLevelUnit);
      await user.click(screen.getByText(LevelUnits.FL));
      await screen.findByText(
        `The maximum level in FL is ${getMaxLevelValue(
          'FL',
          defaultSigmetFIR,
          sigmetConfig,
        )}`,
      );

      expect(
        screen.getByLabelText('Upper level').getAttribute('aria-invalid') ===
          'true',
      ).toBeTruthy();
    });
    it('should show error when lowerlevel value is set higher than upper level value', async () => {
      const testValues = {
        levelInfoMode: 'BETW',
        level: {
          value: 1000,
          unit: 'FT' as LevelUnits,
        },
        lowerLevel: {
          value: 500,
          unit: 'FT' as LevelUnits,
        },
      };
      render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled={false}
            isReadOnly={false}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );
      const lowerLevelValueField = screen.getByLabelText('Lower level');
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);

      await user.clear(lowerLevelValueField);
      await user.type(lowerLevelValueField, '4500');
      await waitFor(() => {
        expect(
          screen
            .queryAllByRole('textbox')
            .filter((field) => field.getAttribute('aria-invalid') === 'true'),
        ).toHaveLength(1);
      });
      await screen.findByText(errorMessages.isLevelLower);

      await user.clear(lowerLevelValueField);
      await user.type(lowerLevelValueField, '500');

      await waitFor(() =>
        expect(
          screen
            .queryAllByRole('textbox')
            .filter((field) => field.getAttribute('aria-invalid') === 'true'),
        ).toHaveLength(0),
      );
      expect(screen.queryByText(errorMessages.isLevelLower)).toBeFalsy();
    });
    it('should show error when upperlevel value is set below lowerlevel value', async () => {
      const testValues = {
        levelInfoMode: 'BETW',
        level: {
          value: 1000,
          unit: 'FT' as LevelUnits,
        },
        lowerLevel: {
          value: 500,
          unit: 'FT' as LevelUnits,
        },
      };
      render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled={false}
            isReadOnly={false}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );
      const upperLevelValueField = screen.getByLabelText('Upper level');
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(0);

      await user.clear(upperLevelValueField);
      await user.type(upperLevelValueField, '500');
      await waitFor(() =>
        expect(
          screen
            .queryAllByRole('textbox')
            .filter((field) => field.getAttribute('aria-invalid') === 'true'),
        ).toHaveLength(1),
      );
      await screen.findByText(errorMessages.isLevelLower);

      await user.clear(upperLevelValueField);
      await user.type(upperLevelValueField, '1000');
      await waitFor(() =>
        expect(
          screen
            .queryAllByRole('textbox')
            .filter((field) => field.getAttribute('aria-invalid') === 'true'),
        ).toHaveLength(0),
      );
      expect(screen.queryByText(errorMessages.isLevelLower)).toBeFalsy();
    });
    it('should show error when switching to a lowerlevel unit with a lower max value', async () => {
      const testValues = {
        levelInfoMode: 'BETW',
        level: {
          value: 100,
          unit: 'FL' as LevelUnits,
        },
        lowerLevel: {
          value: 900,
          unit: 'FT' as LevelUnits,
        },
      };
      render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled={false}
            isReadOnly={false}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );
      const lowerLevelUnit = screen.getAllByLabelText('Unit')[1];
      expect(
        screen.queryByLabelText('Lower level')!.getAttribute('aria-invalid') ===
          'true',
      ).toBeFalsy();

      await user.click(lowerLevelUnit);
      await user.click(screen.getAllByText(LevelUnits.FL)[1]);
      await screen.findByText(
        `The maximum level in FL is ${getMaxLevelValue(
          'FL',
          defaultSigmetFIR,
          sigmetConfig,
        )}`,
      );
      expect(
        screen.getByLabelText('Lower level').getAttribute('aria-invalid') ===
          'true',
      ).toBeTruthy();
    });
  });

  describe('ABOVE section', () => {
    it('should show the correct input fields when selecting level ABV and TOPS_ABV', async () => {
      render(
        <ReactHookFormProvider>
          <Levels
            isDisabled={false}
            isReadOnly={false}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );
      const levelFieldAbv: HTMLInputElement = within(
        screen.getByTestId('levels-ABV'),
      ).getByRole('radio');

      // ABV not selected
      expect(levelFieldAbv.checked).toBeFalsy();
      expect(screen.queryByTestId('levels-TOPS_ABV')).toBeNull();
      expect(screen.queryByLabelText('Unit')).toBeNull();
      expect(screen.queryByLabelText('Level')).toBeNull();

      // select ABV
      fireEvent.click(levelFieldAbv);

      await waitFor(() => expect(levelFieldAbv.checked).toBeTruthy());
      expect(screen.getByTestId('levels-TOPS_ABV')).toBeTruthy();
      expect(screen.getByLabelText('Unit')).toBeTruthy();
      expect(screen.getByLabelText('Level')).toBeTruthy();

      // should autoFocus
      expect(screen.getByLabelText('Level').matches(':focus')).toBeTruthy();

      // select TOPS
      const levelTopField: HTMLInputElement = within(
        screen.getByTestId('levels-TOPS_ABV'),
      ).getByRole('checkbox');
      fireEvent.click(levelTopField);
      await waitFor(() => expect(levelTopField.checked).toBeTruthy());
    });
    it('should show the input fields as disabled with correct values', () => {
      const testValues = {
        levelInfoMode: 'ABV',
        level: {
          value: '50',
          unit: 'FL' as LevelUnits,
        },
      };
      render(
        <ReactHookFormProvider
          options={{
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled
            isReadOnly={false}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );

      const levelAbv: HTMLInputElement = within(
        screen.getByTestId('levels-ABV'),
      ).getByRole('radio');
      expect(levelAbv.disabled).toBeTruthy();
      expect(levelAbv.getAttribute('value')).toEqual(testValues.levelInfoMode);

      const levelUnit = screen.getByLabelText('Unit');
      expect(levelUnit.getAttribute('aria-disabled') === 'true').toBeTruthy();
      expect(levelUnit.textContent).toEqual(testValues.level.unit);

      const levelValue: HTMLInputElement = screen.getByLabelText('Level');
      expect(levelValue.disabled).toBeTruthy();
      expect(levelValue.getAttribute('value')).toEqual(testValues.level.value);
    });
    it('should show the input fields as readOnly with correct values', () => {
      const testValues = {
        levelInfoMode: 'ABV',
        level: {
          value: '50',
          unit: 'FL' as LevelUnits,
        },
      };
      render(
        <ReactHookFormProvider
          options={{
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled
            isReadOnly
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );
      const levelAbv: HTMLInputElement = within(
        screen.getByTestId('levels-ABV'),
      ).getByRole('radio');
      expect(levelAbv.disabled).toBeTruthy();
      expect(levelAbv.getAttribute('value')).toEqual(testValues.levelInfoMode);

      const levelUnit = screen.getByLabelText('Unit');
      expect(levelUnit.getAttribute('aria-disabled') === 'true').toBeTruthy();
      expect(levelUnit.textContent).toEqual(testValues.level.unit);

      const levelValue: HTMLInputElement = screen.getByLabelText('Level');
      expect(levelValue.disabled).toBeTruthy();
      expect(levelValue.getAttribute('value')).toEqual(testValues.level.value);
    });
    it('should show an error message when level exceeds max for unit for sigmet', async () => {
      const testValues = {
        levelInfoMode: 'ABV',
        level: {
          value: '',
          unit: 'FL' as LevelUnits,
        },
      };
      render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled={false}
            isReadOnly={false}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );
      const levelValueField = screen.getByLabelText('Level');
      fireEvent.change(levelValueField, { target: { value: 1000 } });
      await screen.findByText(
        `The maximum level in FL is ${getMaxLevelValue(
          'FL',
          defaultSigmetFIR,
          sigmetConfig,
        )}`,
      );
      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(1);

      const levelUnitField = screen.getByLabelText('Unit');
      await user.click(levelUnitField);
      await user.click(screen.getByText(LevelUnits.FT));

      await waitFor(() => {
        expect(
          screen
            .queryAllByRole('textbox')
            .filter((field) => field.getAttribute('aria-invalid') === 'true'),
        ).toHaveLength(0);
      });
    });
    it('should show an error message when level exceeds max for unit for airmet', async () => {
      const testValues = {
        levelInfoMode: 'ABV',
        level: {
          value: '',
          unit: 'FL' as LevelUnits,
        },
      };
      render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled={false}
            isReadOnly={false}
            productType="airmet"
            productConfig={airmetConfig}
          />
        </ReactHookFormProvider>,
      );
      const levelValueField = screen.getByLabelText('Level');
      fireEvent.change(levelValueField, { target: { value: 500 } });
      await screen.findByText(
        `The maximum level in FL is ${getMaxLevelValue(
          'FL',
          defaultAirmetFIR,
          airmetConfig,
        )}`,
      );

      expect(
        screen
          .queryAllByRole('textbox')
          .filter((field) => field.getAttribute('aria-invalid') === 'true'),
      ).toHaveLength(1);
      const levelUnitField = screen.getByLabelText('Unit');
      await user.click(levelUnitField);
      await user.click(screen.getByText(LevelUnits.FT));

      await waitFor(() => {
        expect(
          screen
            .queryAllByRole('textbox')
            .filter((field) => field.getAttribute('aria-invalid') === 'true'),
        ).toHaveLength(0);
      });
    });

    it('should show an error message when the level value is too low', async () => {
      const testValues = {
        levelInfoMode: 'ABV',
        level: {
          value: '',
          unit: 'FL' as LevelUnits,
        },
      };
      render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled={false}
            isReadOnly={false}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );
      const levelInput = screen.getByLabelText('Level');
      fireEvent.change(levelInput, { target: { value: 0 } });
      await screen.findByText(
        `The minimum level in FL is ${getMinLevelValue(
          'FL',
          defaultSigmetFIR,
          sigmetConfig,
        )}`,
      );
    });

    it('should clear out level value when switching between AT and ABOVE', async () => {
      const testValues = {
        levelInfoMode: 'ABV',
        level: {
          value: '50',
          unit: 'FL' as LevelUnits,
        },
      };
      render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled={false}
            isReadOnly={false}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );
      const levelFieldAt: HTMLInputElement = within(
        screen.getByTestId('levels-AT'),
      ).getByRole('radio');
      const levelValue = screen.getByLabelText('Level');

      // AT not selected
      expect(levelFieldAt.checked).toBeFalsy();
      expect(levelValue.getAttribute('value')).toEqual(testValues.level.value);

      // select AT - expect value to have been cleared out for level
      fireEvent.click(levelFieldAt);
      await waitFor(() => expect(levelFieldAt.checked).toBeTruthy());

      // Select newly rendered level field
      const levelValue2 = screen.getByLabelText('Level');
      await waitFor(() =>
        expect(levelValue2.getAttribute('value')).toEqual(''),
      );
    });
    it('should show error when lowerlevel unit not allowed with upper level unit', async () => {
      const testValues = {
        levelInfoMode: 'BETW',
        level: {
          value: 600,
          unit: 'FL' as LevelUnits,
        },
        lowerLevel: {
          value: 500,
          unit: 'FL' as LevelUnits,
        },
      };
      render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled={false}
            isReadOnly={false}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );
      const lowerLevelUnitField = screen.getAllByLabelText('Unit')[1];

      // Upper FL lower FT is allowed
      await user.click(lowerLevelUnitField);
      await user.click(screen.getByText('ft'));
      await waitFor(() => {
        expect(
          screen
            .queryAllByRole('textbox')
            .filter((field) => field.getAttribute('aria-invalid') === 'true'),
        ).toHaveLength(0);
      });
      expect(
        screen.queryByText(invalidLevelUnitCombinationMessage),
      ).toBeFalsy();

      // Upper FL lower FL is allowed
      await user.click(lowerLevelUnitField);
      await user.click(screen.getAllByText('FL')[1]);
      await waitFor(() => {
        expect(
          screen
            .queryAllByRole('textbox')
            .filter((field) => field.getAttribute('aria-invalid') === 'true'),
        ).toHaveLength(0);
      });
      expect(
        screen.queryByText(invalidLevelUnitCombinationMessage),
      ).toBeFalsy();

      // Upper FT lower FL not allowed
      const levelUnitField = screen.getAllByLabelText('Unit')[0];
      await user.click(levelUnitField);
      await user.click(screen.getByText('ft'));

      await waitFor(() => {
        expect(
          screen
            .queryAllByRole('textbox')
            .filter((field) => field.getAttribute('aria-invalid') === 'true'),
        ).toHaveLength(2);
      });
      expect(screen.getByText(invalidLevelUnitCombinationMessage)).toBeTruthy();

      // Upper FL lower FL allowed
      await user.click(levelUnitField);
      await user.click(screen.getAllByText('FL')[1]);
      await waitFor(() => {
        expect(
          screen
            .queryAllByRole('textbox')
            .filter((field) => field.getAttribute('aria-invalid') === 'true'),
        ).toHaveLength(0);
      });
      expect(
        screen.queryByText(invalidLevelUnitCombinationMessage),
      ).toBeFalsy();
    });

    it('should show no error for unit if SFC selected', async () => {
      const testValues = {
        levelInfoMode: 'BETW',
        level: {
          value: 1000,
          unit: 'FL' as LevelUnits,
        },
        lowerLevel: {
          value: 500,
          unit: 'FL' as LevelUnits,
        },
      };
      render(
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: testValues,
          }}
        >
          <Levels
            isDisabled={false}
            isReadOnly={false}
            productType="sigmet"
            productConfig={sigmetConfig}
          />
        </ReactHookFormProvider>,
      );

      // Upper FT lower FL not allowed
      const levelUnitField = screen.getAllByLabelText('Unit')[0];
      await user.click(levelUnitField);
      await user.click(screen.getByText('ft'));
      await screen.findByText(invalidLevelUnitCombinationMessage);

      const levelSFCField: HTMLInputElement = within(
        screen.getByTestId('levels-SFC'),
      ).getByRole('checkbox');
      fireEvent.click(levelSFCField);
      await waitFor(() => {
        expect(levelSFCField.checked).toBeTruthy();
      });
      expect(screen.queryByLabelText('Lower level')).toBeFalsy();
      expect(
        screen.queryByText(invalidLevelUnitCombinationMessage),
      ).toBeFalsy();
    });
  });

  describe('validateLevels', () => {
    const {
      /* eslint-disable @typescript-eslint/naming-convention */
      level_rounding_FL,
      level_rounding_FT,
      level_rounding_M,
      /* eslint-enable @typescript-eslint/naming-convention */
      ...firArea
    } = airmetConfig.fir_areas.EHAA;
    const testConfig = { ...airmetConfig, fir_areas: { EHAA: firArea } };
    it('should return true if no value given', () => {
      expect(validateLevels('', 'FL', sigmetConfig, 'EHAA')).toBeTruthy();
      expect(validateLevels(null!, 'FL', sigmetConfig, 'EHAA')).toBeTruthy();
    });
    it('should return invalid message if invalid unit given', () => {
      expect(validateLevels('20', 'FAKE', sigmetConfig, 'EHAA')).toBe(
        invalidUnitMessage,
      );
    });

    it('should use default values if not given in config', () => {
      expect(
        validateLevels(
          DEFAULT_ROUNDING_LEVELS_FL.toString(),
          'FL',
          testConfig,
          'EHAA',
        ),
      ).toBeTruthy();
      expect(
        validateLevels(
          DEFAULT_ROUNDING_LEVELS_FT.toString(),
          'FT',
          testConfig,
          'EHAA',
        ),
      ).toBeTruthy();
      expect(
        validateLevels(
          DEFAULT_ROUNDING_LEVELS_M.toString(),
          'M',
          testConfig,
          'EHAA',
        ),
      ).toBeTruthy();

      expect(validateLevels('7', 'FL', testConfig, 'EHAA')).toBe(
        getLevelInvalidStepsForFLUnitMessage(),
      );
      expect(validateLevels('120', 'FT', testConfig, 'EHAA')).toBe(
        getLevelInvalidStepsForFTUnitMessage(),
      );
    });

    it('should fall back to use first FIR if passed FIR does not exist in config', () => {
      const testConfig2 = {
        ...airmetConfig,
        fir_areas: {
          EHAA: {
            level_rounding_FL: 80,
            level_rounding_FT: 33,
            level_rounding_M: 10,
            ...firArea,
          },
        },
      };

      expect(validateLevels('160', 'FL', testConfig2, 'TEST')).toBeTruthy();
      expect(validateLevels('66', 'FT', testConfig2, 'TEST')).toBeTruthy();
      expect(validateLevels('40', 'M', testConfig2, 'TEST')).toBeTruthy();

      expect(validateLevels('150', 'FL', testConfig2, 'TEST')).toBe(
        getLevelInvalidStepsForFLUnitMessage(
          testConfig2.fir_areas.EHAA.level_rounding_FL,
        ),
      );
      expect(validateLevels('22', 'FT', testConfig2, 'TEST')).toBe(
        getLevelInvalidStepsForFTUnitMessage(
          testConfig2.fir_areas.EHAA.level_rounding_FT,
        ),
      );
      expect(validateLevels('22', 'M', testConfig2, 'TEST')).toBe(
        getLevelInvalidStepsForMUnitMessage(
          testConfig2.fir_areas.EHAA.level_rounding_M,
        ),
      );
    });

    it('should use config from passed FIR if exists in config', () => {
      const testConfig3 = {
        ...airmetConfig,
        fir_areas: {
          EHAA: airmetConfig.fir_areas.EHAA,
          EBBB: {
            level_rounding_FL: 80,
            level_rounding_FT: 33,
            level_rounding_M: 10,
            ...firArea,
          },
        },
      };

      expect(validateLevels('160', 'FL', testConfig3, 'EBBB')).toBeTruthy();
      expect(validateLevels('66', 'FT', testConfig3, 'EBBB')).toBeTruthy();
      expect(validateLevels('40', 'M', testConfig3, 'EBBB')).toBeTruthy();

      expect(validateLevels('150', 'FL', testConfig3, 'EBBB')).toBe(
        getLevelInvalidStepsForFLUnitMessage(
          testConfig3.fir_areas.EBBB.level_rounding_FL,
        ),
      );
      expect(validateLevels('22', 'FT', testConfig3, 'EBBB')).toBe(
        getLevelInvalidStepsForFTUnitMessage(
          testConfig3.fir_areas.EBBB.level_rounding_FT,
        ),
      );
      expect(validateLevels('22', 'M', testConfig3, 'EBBB')).toBe(
        getLevelInvalidStepsForMUnitMessage(
          testConfig3.fir_areas.EBBB.level_rounding_M,
        ),
      );
    });

    describe('validateLevelUnitCombinations', () => {
      it('should validate correctly allowed combinations of units', () => {
        expect(validateLevelUnitCombinations('FL', 'FL')).toBe(true);
        expect(validateLevelUnitCombinations('FT', 'FT')).toBe(true);
        expect(validateLevelUnitCombinations('M', 'M')).toBe(true);

        expect(validateLevelUnitCombinations('FL', 'M')).toBe(true);
        expect(validateLevelUnitCombinations('FL', 'FT')).toBe(true);

        expect(validateLevelUnitCombinations('FT', 'M')).toBe(
          invalidLevelUnitCombinationMessage,
        );
        expect(validateLevelUnitCombinations('FT', 'FL')).toBe(
          invalidLevelUnitCombinationMessage,
        );
        expect(validateLevelUnitCombinations('M', 'FT')).toBe(
          invalidLevelUnitCombinationMessage,
        );
        expect(validateLevelUnitCombinations('M', 'FL')).toBe(
          invalidLevelUnitCombinationMessage,
        );
      });
      it('should validate correctly combination with SFC', () => {
        expect(validateLevelUnitCombinations('FL', 'SFC')).toBe(true);
        expect(validateLevelUnitCombinations('FT', 'SFC')).toBe(true);
        expect(validateLevelUnitCombinations('M', 'SFC')).toBe(true);
      });
    });
  });
});
