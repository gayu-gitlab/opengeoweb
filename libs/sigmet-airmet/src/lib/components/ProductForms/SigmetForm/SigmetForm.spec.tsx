/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import {
  render,
  fireEvent,
  waitFor,
  screen,
  within,
} from '@testing-library/react';
import {
  ReactHookFormProvider,
  defaultFormOptions,
} from '@opengeoweb/form-fields';
import { dateUtils } from '@opengeoweb/shared';

import { useFormContext } from 'react-hook-form';
import { Button } from '@mui/material';

import SigmetForm from './SigmetForm';
import { fakeSigmetList } from '../../../utils/mockdata/fakeSigmetList';
import { noTAC } from '../ProductFormTac';
import { TestWrapper } from '../../../utils/testUtils';
import {
  CancelSigmet,
  ProductConfig,
  Sigmet,
  SigmetPhenomena,
} from '../../../types';
import {
  exitDrawModeMessage,
  maxFeaturePointsMessage,
} from '../ProductFormFields/StartGeometry';
import { sigmetConfig } from '../../../utils/config';

describe('components/SigmetForm/SigmetForm', () => {
  it('should render successfully', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());
    expect(screen.getByTestId('phenomenon').textContent).toContain(
      'Obscured thunderstorm(s)',
    );
  });
  it('should show no issue date for a new sigmet', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {},
          }}
        >
          <SigmetForm mode="edit" />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(screen.getByText('(Not published)')).toBeTruthy();
  });

  it('should show no issue date for a draft sigmet', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    expect(screen.getByText('(Not published)')).toBeTruthy();
  });

  it('should show issue date for a published sigmet', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[1].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[1].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    expect(
      screen.getByText(
        `${dateUtils.dateToString(
          dateUtils.utc(fakeSigmetList[1].sigmet.issueDate),
          "yyyy/MM/dd HH:mm 'UTC'",
        )}`,
      ),
    ).toBeTruthy();
  });

  it('should show issue date for a expired sigmet', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[4].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[4].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    expect(
      screen.getByText(
        `${dateUtils.dateToString(
          dateUtils.utc(fakeSigmetList[4].sigmet.issueDate),
          "yyyy/MM/dd HH:mm 'UTC'",
        )}`,
      ),
    ).toBeTruthy();
  });

  it('should show issue date for a cancel sigmet', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[3].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialCancelSigmet={fakeSigmetList[3].sigmet as CancelSigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    expect(
      screen.getByText(
        `${dateUtils.dateToString(
          dateUtils.utc(fakeSigmetList[3].sigmet.issueDate),
          "yyyy/MM/dd HH:mm 'UTC'",
        )}`,
      ),
    ).toBeTruthy();
  });

  it('should show the volcano specific fields when phenomenon is Volcanic ash cloud and form is in edit mode', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...fakeSigmetList[1].sigmet,
            },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[1].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    expect(screen.getByTestId('phenomenon').textContent).toEqual(
      'Volcanic ash cloud',
    );

    expect(screen.getByText('Volcano name')).toBeTruthy();
    expect(screen.getByText('Latitude')).toBeTruthy();
    expect(screen.getByText('Longitude')).toBeTruthy();
    expect(screen.getByTestId('movementType-NO_VA_EXP').textContent).toContain(
      'No volcanic ash expected',
    );
  });

  it('should not show the volcano specific fields when phenomenon is other than Volcanic ash cloud and form is in edit mode', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...fakeSigmetList[0].sigmet,
            },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );
    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    expect(screen.getByTestId('phenomenon').textContent).not.toEqual(
      'Volcanic ash cloud',
    );
    expect(screen.queryByText('Volcano name')).toBeFalsy();
    expect(screen.queryByText('Latitude')).toBeFalsy();
    expect(screen.queryByText('Longitude')).toBeFalsy();
    expect(screen.queryByText('No volcanic ash expected')).toBeFalsy();
  });

  it('should not show the helper text on too many intersection points when opening a SIGMET with the FIR selected', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    expect(screen.queryByText(maxFeaturePointsMessage)).toBeFalsy();
  });

  it('should show a helper text when entering draw mode', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    const startGeometrybaseElement = screen.getByTestId('startGeometry');

    const drawBtn = within(
      within(startGeometrybaseElement).getByTestId('drawtools-polygon'),
    ).getByRole('radio');
    fireEvent.click(drawBtn);

    await waitFor(() =>
      expect(startGeometrybaseElement.textContent).toContain(
        exitDrawModeMessage,
      ),
    );
  });

  it('should disable form when entering draw mode', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeSigmetList[0].sigmet },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[0].sigmet as Sigmet}
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    const drawBtn = within(
      screen.getAllByTestId('drawtools-polygon')[0],
    ).getByRole('radio');
    fireEvent.click(drawBtn);

    const inputWrapper = screen.getAllByRole('radio')[0];

    await waitFor(() =>
      expect(inputWrapper.hasAttribute('disabled')).toBeTruthy(),
    );

    // enable form again by switching mode
    const deleteBtn = within(
      screen.getAllByTestId('drawtools-delete')[0],
    ).getByRole('radio');
    fireEvent.click(deleteBtn);

    await waitFor(() =>
      expect(inputWrapper.hasAttribute('disabled')).toBeFalsy(),
    );
  });

  it('should remove the movementType NO_VA_EXP when changing phenomenon', async () => {
    const Wrapper = (): React.ReactElement => {
      const { handleSubmit } = useFormContext();
      return (
        <>
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[1].sigmet as Sigmet}
          />
          <Button
            onClick={(): void => {
              handleSubmit(() => {})();
            }}
          >
            Validate
          </Button>
        </>
      );
    };

    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...fakeSigmetList[1].sigmet,
            },
          }}
        >
          <Wrapper />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    // make sure phenomenon Volcanic ash cloud and movementType No VA expected are set
    expect(screen.getByTestId('phenomenon').textContent).toEqual(
      'Volcanic ash cloud',
    );
    fireEvent.click(screen.getByTestId('movementType-NO_VA_EXP'));
    expect(
      (screen.getByTestId('movementType-NO_VA_EXP').firstChild as HTMLElement)
        .classList,
    ).toContain('Mui-checked');

    // change phenomenon
    fireEvent.mouseDown(
      within(screen.getByTestId('phenomenon')).getByRole('button'),
    );
    const menuItem = await screen.findByText('Radioactive cloud');
    fireEvent.click(menuItem);
    await waitFor(() =>
      expect(screen.getByTestId('phenomenon').textContent).toContain(
        'Radioactive cloud',
      ),
    );

    // check that NO_VA_EXP radiobutton is gone
    await waitFor(() =>
      expect(screen.queryByText('No volcanic ash expected')).toBeFalsy(),
    );

    // check that movementType has a required field error when validating (it should be empty now because NO_VA_EXP was removed)
    fireEvent.click(screen.getByText('Validate'));

    await waitFor(() =>
      expect(screen.getByRole('alert').textContent).toEqual(
        'This field is required',
      ),
    );
  });

  it('should be able to start drawing without provided geometries', async () => {
    const fakeDraftSigmetWithoutStartGeometries = {
      ...fakeSigmetList[0].sigmet,
      startGeometry: null,
      startGeometryIntersect: null,
      endGeometry: null,
      endGeometryIntersect: null,
    };
    expect(fakeDraftSigmetWithoutStartGeometries.startGeometry).toBeNull();
    expect(
      fakeDraftSigmetWithoutStartGeometries.startGeometryIntersect,
    ).toBeNull();
    expect(fakeDraftSigmetWithoutStartGeometries.endGeometry).toBeNull();
    expect(
      fakeDraftSigmetWithoutStartGeometries.endGeometryIntersect,
    ).toBeNull();

    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: { ...fakeDraftSigmetWithoutStartGeometries },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={
              fakeDraftSigmetWithoutStartGeometries as unknown as Sigmet
            }
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    const startGeometrybaseElement = screen.getByTestId('startGeometry');

    const drawBtn = within(
      within(startGeometrybaseElement).getByTestId('drawtools-polygon'),
    ).getByRole('radio');
    fireEvent.click(drawBtn!);

    expect(screen.getByTestId('phenomenon').textContent).toContain(
      'Obscured thunderstorm(s)',
    );
  });

  it('should display change options when phenomenon is volcanic ash cloud and not omitted in config', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...fakeSigmetList[1].sigmet,
            },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[1].sigmet as Sigmet}
            productConfig={
              {
                ...sigmetConfig,
              } as ProductConfig
            }
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(screen.getByTestId('phenomenon').textContent).toEqual(
      'Volcanic ash cloud',
    );

    expect(screen.getByText('Change')).toBeTruthy();
  });

  it('should display change options when phenomenon is radioactive cloud and not omitted in config', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...fakeSigmetList[1].sigmet,
              phenomenon: 'RDOACT_CLD' as SigmetPhenomena,
            },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={{
              ...(fakeSigmetList[1].sigmet as Sigmet),
              phenomenon: 'RDOACT_CLD' as SigmetPhenomena,
            }}
            productConfig={
              {
                ...sigmetConfig,
              } as ProductConfig
            }
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect((await screen.findByTestId('phenomenon')).textContent).toEqual(
      'Radioactive cloud',
    );

    expect(await screen.findByText('Change')).toBeTruthy();
  });

  it('should not display change options when omitted in config and phenomenon is volcanic ash cloud', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...fakeSigmetList[1].sigmet,
            },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={fakeSigmetList[1].sigmet as Sigmet}
            productConfig={
              {
                ...sigmetConfig,
                omit_change_va_and_rdoact_cloud: true,
              } as ProductConfig
            }
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(screen.getByTestId('phenomenon').textContent).toEqual(
      'Volcanic ash cloud',
    );

    await waitFor(() => expect(screen.queryByText('Change')).toBeFalsy());
  });

  it('should not display change options when omitted in config and phenomenon is radioactive cloud', async () => {
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: {
              ...fakeSigmetList[1].sigmet,
              phenomenon: 'RDOACT_CLD' as SigmetPhenomena,
            },
          }}
        >
          <SigmetForm
            mode="edit"
            initialSigmet={{
              ...(fakeSigmetList[1].sigmet as Sigmet),
              phenomenon: 'RDOACT_CLD' as SigmetPhenomena,
            }}
            productConfig={
              {
                ...sigmetConfig,
                omit_change_va_and_rdoact_cloud: true,
              } as ProductConfig
            }
          />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    expect(screen.getByTestId('phenomenon').textContent).toEqual(
      'Radioactive cloud',
    );

    await waitFor(() => expect(screen.queryByText('Change')).toBeFalsy());
  });

  it('should disable fields in short test sigmet', async () => {
    const shortTestSigmet = {
      ...fakeSigmetList[0].sigmet,
      type: 'SHORT_TEST',
    } as Sigmet;
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: shortTestSigmet,
          }}
        >
          <SigmetForm mode="edit" initialSigmet={shortTestSigmet} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    const phenomenon = screen.getByLabelText('Select phenomenon').classList;
    expect(phenomenon).toContain('Mui-disabled');

    const obsFcstTime = within(screen.getByTestId('obs-fcst-time')).getByRole(
      'textbox',
    );
    expect(obsFcstTime.classList).toContain('Mui-disabled');

    screen.getAllByRole('radio').forEach((radio) => {
      expect(radio.hasAttribute('disabled')).toBeTruthy();
    });
  });

  it('should disable fields and not display volcanic fields in short volcanic ash test sigmet', async () => {
    const shortTestSigmet = {
      ...fakeSigmetList[0].sigmet,
      type: 'SHORT_VA_TEST',
      phenomenon: 'VA_CLD' as SigmetPhenomena,
    } as Sigmet;
    render(
      <TestWrapper>
        <ReactHookFormProvider
          options={{
            ...defaultFormOptions,
            defaultValues: shortTestSigmet,
          }}
        >
          <SigmetForm mode="edit" initialSigmet={shortTestSigmet} />
        </ReactHookFormProvider>
      </TestWrapper>,
    );

    await waitFor(() => expect(screen.queryByText(noTAC)).toBeFalsy());

    const phenomenon = screen.getByLabelText('Select phenomenon').classList;
    expect(phenomenon).toContain('Mui-disabled');

    const obsFcstTime = within(screen.getByTestId('obs-fcst-time')).getByRole(
      'textbox',
    );
    expect(obsFcstTime.classList).toContain('Mui-disabled');

    screen.getAllByRole('radio').forEach((radio) => {
      expect(radio.hasAttribute('disabled')).toBeTruthy();
    });

    expect(screen.queryByText('Volcano name')).toBeFalsy();
    expect(screen.queryByText('Latitude')).toBeFalsy();
    expect(screen.queryByText('Longitude')).toBeFalsy();
  });
});
