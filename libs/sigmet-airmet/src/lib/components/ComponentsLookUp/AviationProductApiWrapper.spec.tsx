/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render } from '@testing-library/react';
import { AviationProductApiWrapper } from './AviationProductApiWrapper';
import { ProductType } from '../../types';

describe('AviationProductApiWrapper', () => {
  const config = {
    baseURL: 'https://example.com/api',
    appURL: 'https://example.com',
    authTokenURL: 'https://example.com/auth',
    authClientId: '123456',
  };

  const auth = {
    username: 'test user',
    token: '',
    refresh_token: '',
  };

  const onSetAuth = jest.fn();
  const productConfigKey = 'sigmet.json';
  const productType: ProductType = 'sigmet';

  const props = {
    config,
    auth,
    onSetAuth,
    productConfigKey,
    productType,
  };

  it('renders correctly with provided props', async () => {
    const { container } = render(<AviationProductApiWrapper {...props} />);
    expect(container.firstChild).not.toBeNull();
  });

  it('does not render without config', () => {
    const { container } = render(
      <AviationProductApiWrapper {...props} config={null!} />,
    );
    expect(container.firstChild).toBeNull();
  });
});
