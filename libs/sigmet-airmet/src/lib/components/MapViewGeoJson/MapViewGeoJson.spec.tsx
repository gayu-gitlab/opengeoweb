/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import {
  layerActions,
  mapActions,
  uiActions,
  defaultLayers,
} from '@opengeoweb/store';
import { LayerType, webmapUtils } from '@opengeoweb/webmap';
import { MapViewLayerProps } from '@opengeoweb/webmap-react';
import MapViewGeoJson from './MapViewGeoJson';
import { srsAndBboxDefault } from './constants';
import { TestWrapper } from '../../utils/testUtils';

describe('MapViewGeoJson', () => {
  afterEach(() => {
    webmapUtils.unRegisterAllWMJSLayersAndMaps();
  });
  it('should render successfully with default layers', () => {
    render(
      <TestWrapper>
        <MapViewGeoJson geoJSONLayers={[]} />
      </TestWrapper>,
    );
    const layerList = screen.queryAllByTestId('mapViewLayer');
    expect(layerList[0].textContent).toContain('countryborders');
  });

  it('should set correct default baselayers', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            id: 'main-map',
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson geoJSONLayers={[]} />
      </TestWrapper>,
    );

    const expectedBaseLayers = store
      .getActions()
      .find((action) => action.type === 'layerReducer/setBaseLayers');

    expect(expectedBaseLayers.payload.layers).toEqual([
      {
        ...defaultLayers.overLayer,
        id: expect.stringContaining('countryborders-sigmet-mapid_'),
      },
    ]);
  });

  it('should set correct given baselayers', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            id: 'main-map',
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    const testLayers = [
      {
        id: 'baseLayer-airmet',
        name: 'WorldMap_Light_Grey_Canvas',
        type: 'twms',
        layerType: LayerType.baseLayer,
      },
      {
        id: 'countryborder-airmet',
        name: 'countryborders',
        layerType: LayerType.overLayer,
        format: 'image/png',
        enabled: true,
        service: 'https://geoservices.knmi.nl/wms?DATASET=baselayers&',
      },
      {
        id: 'northseastations-airmet',
        name: 'northseastations',
        layerType: LayerType.overLayer,
        service:
          'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserverdataset=OVL',
        format: 'image/png',
        enabled: true,
      },
      {
        id: 'airports-airmet',
        name: 'airports',
        layerType: LayerType.overLayer,
        service:
          'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserverdataset=OVL',
        format: 'image/png',
        enabled: true,
      },
    ];

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson baseLayers={testLayers} geoJSONLayers={[]} />
      </TestWrapper>,
    );

    const expectedBaseLayers = store
      .getActions()
      .find((action) => action.type === 'layerReducer/setBaseLayers');

    expect(expectedBaseLayers.payload.layers).toEqual(testLayers);
  });

  it('should render layers successfully with fir', () => {
    const testLayers: MapViewLayerProps[] = [
      {
        id: 'geojsonlayer-fir',
        geojson: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [
                    [5, 55],
                    [4.331914, 55.332644],
                    [3.368817, 55.764314],
                    [2.761908, 54.379261],
                    [3.15576, 52.913554],

                    [7.133055, 52.888887],
                    [7.14218, 52.898244],
                    [7.191667, 53.3],
                    [6.5, 53.666667],
                    [6.500002, 55.000002],
                    [5, 55],
                  ],
                ],
              },
              properties: {
                selectionType: 'fir',
                stroke: '#0075a9',
                'stroke-width': 1.5,
                'stroke-opacity': 1,
                fill: '#0075a9',
                'fill-opacity': 0,
              },
            },
          ],
        },
        isInEditMode: false,
      },
      {
        id: 'geojsonlayer-end',
        geojson: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                selectionType: 'point',
                stroke: '#6e1e91',
                'stroke-width': 1.5,
                'stroke-opacity': 1,
                fill: '#6e1e91',
                'fill-opacity': 0.25,
              },
              geometry: {
                type: 'Point',
                coordinates: [5, 51.5],
              },
            },
          ],
        },
        isInEditMode: false,
        drawMode: '',
        selectedFeatureIndex: 0,
      },
      {
        id: 'geojsonlayer-start',
        geojson: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                selectionType: 'box',
                stroke: '#f24a00',
                'stroke-width': 1.5,
                'stroke-opacity': 1,
                fill: '#f24a00',
                'fill-opacity': 0.25,
              },
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [
                    [8.451700286501511, 52.063884260285],
                    [0.4518849127926449, 52.063884260285],
                    [0.4518849127926449, 53.280640580981604],
                    [8.451700286501511, 53.280640580981604],
                    [8.451700286501511, 52.063884260285],
                  ],
                ],
              },
            },
          ],
        },
        isInEditMode: false,
        drawMode: '',
        selectedFeatureIndex: 0,
      },
      {
        id: 'geojsonlayer-intersection-end',
        geojson: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                selectionType: 'point',
                stroke: '#6e1e91',
                'stroke-width': 1.5,
                'stroke-opacity': 1,
                fill: '#6e1e91',
                'fill-opacity': 0.5,
              },
              geometry: {
                type: 'Point',
                coordinates: [5, 51.5],
              },
            },
          ],
        },
        isInEditMode: false,
      },
      {
        id: 'geojsonlayer-intersection-start',
        geojson: {
          type: 'FeatureCollection',
          features: [
            {
              type: 'Feature',
              properties: {
                selectionType: 'poly',
                stroke: '#f24a00',
                'stroke-width': 1.5,
                'stroke-opacity': 1,
                fill: '#f24a00',
                'fill-opacity': 0.5,
              },
              geometry: {
                type: 'Polygon',
                coordinates: [
                  [
                    [6.77617041058054, 52.063884],
                    [2.461047035878412, 52.063884],
                    [3.15576, 52.913554],
                    [3.0571195833792144, 53.280641],
                    [7.189282421218352, 53.280641],
                    [7.14218, 52.898244],
                    [7.133055, 52.888887],
                    [7.065557, 52.385828],
                    [7.063612, 52.346109],
                    [7.031389, 52.268885],
                    [7.053095, 52.237764],
                    [6.77617041058054, 52.063884],
                  ],
                ],
              },
            },
          ],
        },
        isInEditMode: false,
      },
    ];
    render(
      <TestWrapper>
        <MapViewGeoJson geoJSONLayers={testLayers} />
      </TestWrapper>,
    );
    const layerList = screen.queryAllByTestId('mapViewLayer');
    expect(layerList[1].textContent).toContain('geojsonlayer-fir');
    expect(layerList[2].textContent).toContain('geojsonlayer-end');
    expect(layerList[3].textContent).toContain('geojsonlayer-start');
    expect(layerList[4].textContent).toContain('geojsonlayer-intersection-end');
    expect(layerList[5].textContent).toContain(
      'geojsonlayer-intersection-start',
    );
  });

  it('should set layers of main map if they exist', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            id: 'main-map',
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson geoJSONLayers={[]} />
      </TestWrapper>,
    );
    expect(
      store
        .getActions()
        .find((action) => action.type === layerActions.setLayers.type),
    ).toBeTruthy();
  });

  it('should set mapDimensions of the main map if they exist', () => {
    const dimensions = [{ currentTime: new Date().toString(), name: 'time' }];
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            id: 'main-map',
            mapLayers: ['layerid_2'],
            dimensions,
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson geoJSONLayers={[]} />
      </TestWrapper>,
    );

    const expectedAction = store
      .getActions()
      .find((action) => action.type === mapActions.mapChangeDimension.type);
    expect(expectedAction).toBeTruthy();
    expect(expectedAction.payload.dimension).toEqual(dimensions[0]);
  });

  it('should contain the layermanager, legend and multidimensionselect buttons', async () => {
    const dimensions = [{ currentValue: '9000', name: 'elevation' }];
    const mapId = 'main-map';
    const layerId = 'layerid_1';

    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            id: mapId,
            mapLayers: [layerId],
            dimensions,
          },
        },
        allIds: [mapId],
      },
      layers: {
        byId: {
          [layerId]: {
            mapId,
            id: layerId,
            dimensions: [
              {
                name: 'elevation',
                currentValue: '9000',
                units: 'meters',
                values: '1000,5000,9000',
                synced: false,
              },
            ],
          },
        },
        allIds: [layerId],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson geoJSONLayers={[]} />
      </TestWrapper>,
    );
    expect(screen.getByTestId('layerManagerButton')).toBeTruthy();
    expect(screen.getByTestId('open-Legend')).toBeTruthy();

    // cannot check multidimensionselect button because it only shows up after dimension is set, checking the action instead
    const dimensionAction = store
      .getActions()
      .find((action) => action.type === mapActions.mapChangeDimension.type);
    expect(dimensionAction).toBeTruthy();
    expect(dimensionAction.payload.dimension).toEqual(dimensions[0]);
  });

  it('should include docked layer manager with source module', async () => {
    const dimensions = [{ currentValue: '9000', name: 'elevation' }];
    const mapId = 'main-map';
    const layerId = 'layerid_1';

    const mockState = {
      webmap: {
        byId: {
          [mapId]: {
            id: mapId,
            mapLayers: [layerId],
            dimensions,
          },
        },
        allIds: [mapId],
      },
      layers: {
        byId: {
          [layerId]: {
            mapId,
            id: layerId,
            dimensions: [
              {
                name: 'elevation',
                currentValue: '9000',
                units: 'meters',
                values: '1000,5000,9000',
                synced: false,
              },
            ],
          },
        },
        allIds: [layerId],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson geoJSONLayers={[]} />
      </TestWrapper>,
    );

    const expectedActions = uiActions.registerDialog({
      setOpen: false,
      source: 'module',
      type: expect.stringContaining('dockedLayerManager-sigmet-mapid_'),
    });

    expect(store.getActions()).toContainEqual(expectedActions);
  });

  it('should set correct default projection', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            id: 'main-map',
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson geoJSONLayers={[]} />
      </TestWrapper>,
    );

    const expectedAction = {
      payload: {
        bbox: srsAndBboxDefault.bbox,
        mapId: expect.any(String),
        srs: srsAndBboxDefault.srs,
      },
      type: 'mapReducer/setBbox',
    };

    expect(store.getActions()).toContainEqual(expectedAction);
  });

  it('should set correct given projection', () => {
    const mockState = {
      webmap: {
        byId: {
          'main-map': {
            id: 'main-map',
            mapLayers: ['layerid_2'],
          },
        },
        allIds: ['main-map'],
      },
      layers: {
        byId: {
          layerid_2: {
            mapId: 'main-map',
            id: 'layerid_2',
          },
        },
        allIds: ['layerid_2'],
      },
      syncronizationGroupStore: {
        groups: {
          byId: {},
          allIds: [],
        },
        sources: {
          byId: {},
          allIds: [],
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    const testProjection = {
      bbox: {
        left: -811501,
        right: 2738819,
        top: 12688874,
        bottom: 5830186,
      },
      srs: 'EPSG:3857',
    };

    render(
      <TestWrapper store={store}>
        <MapViewGeoJson projection={testProjection} geoJSONLayers={[]} />
      </TestWrapper>,
    );

    const expectedAction = {
      payload: {
        bbox: testProjection.bbox,
        mapId: expect.any(String),
        srs: testProjection.srs,
      },
      type: 'mapReducer/setBbox',
    };

    expect(store.getActions()).toContainEqual(expectedAction);
  });
});
