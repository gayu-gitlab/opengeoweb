/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { CircularProgress } from '@mui/material';
import { Box } from '@mui/system';
import { useApi, useApiContext } from '@opengeoweb/api';
import { AlertBanner } from '@opengeoweb/shared';
import { useAuthenticationContext } from '@opengeoweb/authentication';
import React from 'react';
import { ProductType } from '../../types';
import { SigmetAirmetApi } from '../../utils/api';
import MetInfoWrapper from './MetInfoWrapper';

interface ModuleWrapperProps {
  productType: ProductType;
  productConfigKey?: string;
}

export const ERROR_TITLE =
  'Something went wrong with loading the configuration file';
export const ERROR_AUTH =
  'This feature is protected by authentication. Please log in to proceed';

const DownloadSigmetAirmetConfigWrapper: React.FC<ModuleWrapperProps> = ({
  productType,
  productConfigKey,
}) => {
  const { api } = useApiContext<SigmetAirmetApi>();
  const { isLoggedIn } = useAuthenticationContext();

  const getConfigurationRequest =
    api[
      productType === 'sigmet'
        ? 'getSigmetConfiguration'
        : 'getAirmetConfiguration'
    ];
  const {
    result: productConfig,
    isLoading: isConfigLoading,
    error,
  } = useApi(getConfigurationRequest, productConfigKey);

  if (isConfigLoading) {
    return (
      <Box
        sx={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
      >
        <CircularProgress data-testid="loadingSpinner" />
      </Box>
    );
  }
  if (error) {
    return (
      <AlertBanner
        title={ERROR_TITLE}
        info={!isLoggedIn ? ERROR_AUTH : error.message}
      />
    );
  }
  return (
    <MetInfoWrapper productType={productType} productConfig={productConfig} />
  );
};

export default DownloadSigmetAirmetConfigWrapper;
