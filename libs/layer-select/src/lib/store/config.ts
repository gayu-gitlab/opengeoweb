/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { Egg } from '@redux-eggs/core';
import { Store } from '@reduxjs/toolkit';

import { createStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import { reducer as layerSelect } from './reducer';
import { LayerSelectModuleState } from './types';
import { rootSaga } from './sagas';

// TODO: This fixes typecheck errors but maybe there is a better way to do that
createStore({
  extensions: [getSagaExtension()],
});

export const layerSelectConfig: Egg<Store<LayerSelectModuleState>> = {
  id: 'layerSelect-module',
  reducersMap: {
    layerSelect,
  },
  sagas: [rootSaga],
};
