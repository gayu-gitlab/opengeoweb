/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import { produce } from 'immer';
import { storeTestSettings } from '@opengeoweb/store';
import { initialState } from './reducer';
import * as selectors from './selectors';
import {
  Filter,
  Filters,
  LayerSelectModuleState,
  PopupVariant,
  RootState,
} from './types';

const mockSearchText = 'searchText';
const filter1: Filter = {
  id: 'keyword-keyword1',
  name: 'keyword1',
  amountVisible: 1,
  checked: true,
  type: 'keywords',
};
const filter2: Filter = {
  id: 'keyword-keyword2',
  name: 'keyword2',
  amountVisible: 1,
  checked: true,
  type: 'keywords',
};
const filter3: Filter = {
  id: 'keyword-keyword3',
  name: 'keyword3',
  amountVisible: 1,
  checked: false,
  type: 'keywords',
};
const mockFilters: Filters = {
  entities: {
    'keyword-keyword1': filter1,
    'keyword-keyword2': filter2,
    'keyword-keyword3': filter3,
  },
  ids: ['keyword-keyword1', 'keyword-keyword2', 'keyword-keyword3'],
};
const mockActiveServices = {
  entities: {
    service1: { serviceId: 'service1', enabled: true, filterIds: [] },
    service2: { serviceId: 'service2', enabled: true, filterIds: [] },
    service3: { serviceId: 'service3', enabled: false, filterIds: [] },
  },
  ids: ['service1', 'service2', 'service3'],
};
const mockState: LayerSelectModuleState = {
  layerSelect: {
    ...initialState,
    filters: {
      searchFilter: mockSearchText,
      filters: mockFilters,
      activeServices: mockActiveServices,
    },
    allServicesEnabled: true,
  },
};

describe('store/layerSelect/selectors', () => {
  describe('getSearchFilter', () => {
    it('should return the current search filter', () => {
      expect(selectors.getSearchFilter(mockState)).toBe(mockSearchText);
    });

    it('should return an empty string when the store does not exist', () => {
      expect(selectors.getSearchFilter(null!)).toBe('');
    });

    it('should return empty string when search text is empty string', () => {
      expect(
        selectors.getSearchFilter({
          layerSelect: {
            ...initialState,
            filters: {
              searchFilter: '',
              filters: {
                entities: {},
                ids: [],
              },
              activeServices: {
                entities: {},
                ids: [],
              },
            },
            allServicesEnabled: true,
          },
        }),
      ).toBe('');
    });
  });

  describe('getActiveServices', () => {
    it('should return an empty object when the store does not exist', () => {
      expect(selectors.getActiveServices(null!)).toEqual({});
    });

    it('should return an array of objects of active services', () => {
      expect(selectors.getActiveServices(mockState)).toBe(
        mockActiveServices.entities,
      );
    });
  });

  describe('getActiveServiceById', () => {
    it('should return object of active service', () => {
      expect(selectors.getActiveServiceById(mockState, 'service1')).toBe(
        mockActiveServices.entities['service1'],
      );
    });

    it('should empty object if service not found', () => {
      expect(
        selectors.getActiveServiceById(mockState, 'not-existing-id'),
      ).toBeUndefined();
    });

    it('should empty object if store is not defined ', () => {
      expect(
        selectors.getActiveServiceById({}, 'not-existing-id'),
      ).toBeUndefined();
    });
  });

  describe('getEnabledServiceIds', () => {
    it('should return an array of string of enabled services', () => {
      expect(selectors.getEnabledServiceIds(mockState)).toStrictEqual([
        'service1',
        'service2',
      ]);
    });
  });

  describe('getCheckedFilterIds', () => {
    it('should return an array of string of checked keywords', () => {
      expect(selectors.getCheckedFilterIds(mockState)).toStrictEqual([
        'keyword-keyword1',
        'keyword-keyword2',
      ]);
    });
  });

  describe('getAllFilterIds', () => {
    it('should return an empty array when the store does not exist', () => {
      expect(selectors.getAllFilterIds(null!)).toEqual([]);
    });

    it('should return an array of string of all filters', () => {
      expect(selectors.getAllFilterIds(mockState)).toStrictEqual(
        mockFilters.ids,
      );
    });
  });

  describe('getAllFilters', () => {
    it('should return an empty array when the store does not exist', () => {
      expect(selectors.getAllFilters(null!)).toEqual([]);
    });
    it('should return filters', () => {
      expect(selectors.getAllFilters(mockState)).toEqual([
        filter1,
        filter2,
        filter3,
      ]);
    });
  });

  describe('isAllFiltersChecked', () => {
    it('should return false that indicates that all filters are not checked', () => {
      expect(selectors.isAllFiltersChecked(mockState)).toStrictEqual(false);
    });

    it('should return boolean that indicates that all filters are checked', () => {
      const mockFilters2 = produce(mockFilters, (draft) => {
        draft.entities['keyword-keyword3']!.checked = true;
      });
      const mockState2: LayerSelectModuleState = {
        layerSelect: {
          ...initialState,
          filters: {
            searchFilter: mockSearchText,
            filters: mockFilters2,
            activeServices: mockActiveServices,
          },
          allServicesEnabled: true,
        },
      };
      expect(selectors.isAllFiltersChecked(mockState2)).toEqual(true);
    });
  });

  describe('getFilterById', () => {
    it('should return undefined when the store does not exist', () => {
      expect(selectors.getFilterById(null!, 'keyword1')).toEqual(undefined);
    });

    it('should return undefined if filter not found', () => {
      expect(selectors.getFilterById(mockState, 'keyword4')).toEqual(undefined);
    });
    it('should return a filter', () => {
      expect(selectors.getFilterById(mockState, 'keyword1')).toStrictEqual(
        mockFilters.entities['keyword1'],
      );
    });
  });

  describe('getActiveLayerInfo', () => {
    it('should return the current activeLayerInfo', () => {
      expect(selectors.getActiveLayerInfo(mockState)).toBe(
        mockState.layerSelect!.activeLayerInfo,
      );
    });

    it('should return undefined when the store does not exist', () => {
      expect(selectors.getActiveLayerInfo(null!)).toBe(undefined);
    });
  });
  describe('getFilteredLayers', () => {
    const keyword = `keywords-${
      storeTestSettings.defaultReduxServices['serviceid_1'].layers![0]
        .keywords![0]
    }`;
    const group = `groups-${
      storeTestSettings.defaultReduxServices['serviceid_1'].layers![1].path[0]
    }`;

    const filterFromService: Filter = {
      id: keyword,
      name: 'keyword',
      amountVisible: 1,
      checked: true,
      type: 'keywords',
    };

    const filterNotInSerivce: Filter = {
      id: 'not-in-service',
      name: 'not-in-service',
      amountVisible: 1,
      checked: true,
      type: 'groups',
    };

    const InactivefilterFromService: Filter = {
      id: group,
      name: 'groups',
      amountVisible: 1,
      checked: true,
      type: 'groups',
    };

    const mockFiltersFromService: Filters = {
      entities: {
        'keywords-keyword': filterFromService,
        'groups-not-in-service': filterNotInSerivce,
        'groups-testLayer': InactivefilterFromService,
      },
      ids: [keyword, group, 'groups-not-in-service'],
    };

    const entities = {
      serviceid_1: {
        serviceId: 'serviceid_1',
        enabled: true,
        filterIds: [],
      },
    };

    const mockStateWithService: RootState = {
      layerSelect: {
        ...initialState,
        filters: {
          searchFilter: '',
          filters: mockFiltersFromService,
          activeServices: {
            entities,
            ids: ['serviceid_1'],
          },
        },
        allServicesEnabled: true,
      },
      services: {
        byId: storeTestSettings.defaultReduxServices,
        allIds: ['serviceid_1'],
      },
    };

    it('should return a list containing the mockState layers', () => {
      expect(selectors.getFilteredLayers(mockStateWithService)).toHaveLength(
        mockStateWithService.services!.byId['serviceid_1'].layers!.length,
      );
    });

    it('should return a list containing the name of the searchstring', () => {
      const mockStateWithServiceAndSearch: RootState = {
        layerSelect: {
          ...initialState,
          filters: {
            searchFilter:
              storeTestSettings.defaultReduxServices['serviceid_1'].layers![0]
                .name!,
            filters: mockFiltersFromService,
            activeServices: {
              entities,
              ids: ['serviceid_1'],
            },
          },
          allServicesEnabled: true,
        },
        services: {
          byId: storeTestSettings.defaultReduxServices,
          allIds: ['serviceid_1'],
        },
      };

      const searchedLayers = selectors.getFilteredLayers(
        mockStateWithServiceAndSearch,
      );
      expect(searchedLayers).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            name: mockStateWithServiceAndSearch.services!.byId['serviceid_1']
              .layers![0].name,
          }),
        ]),
      );
      expect(searchedLayers).toHaveLength(1);
    });

    it('should return a list containing one of the mockStateWithService layers with a keyword filter', () => {
      const mockFiltersFromService2 = produce(
        mockFiltersFromService,
        (draft) => {
          draft.entities['groups-testLayer']!.checked = false;
        },
      );

      const mockStateWithService2: RootState = {
        layerSelect: {
          ...initialState,
          filters: {
            searchFilter: '',
            filters: mockFiltersFromService2,
            activeServices: {
              entities,
              ids: ['serviceid_1'],
            },
          },
          allServicesEnabled: true,
        },
        services: {
          byId: storeTestSettings.defaultReduxServices,
          allIds: ['serviceid_1'],
        },
      };
      const filteredKeyword = selectors.getFilteredLayers(
        mockStateWithService2,
      );
      expect(filteredKeyword).toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            keywords: [filterFromService.name],
          }),
        ]),
      );
      expect(filteredKeyword).not.toEqual(
        expect.arrayContaining([
          expect.objectContaining({
            groups: [filterNotInSerivce.name],
          }),
        ]),
      );
      expect(filteredKeyword).toHaveLength(1);
    });
  });

  describe('getServicePopupDetails', () => {
    it('should return the default when the store does not exist', () => {
      expect(selectors.getServicePopupDetails(null!)).toStrictEqual({
        isOpen: false,
        url: '',
        serviceId: '',
        variant: 'add',
      });
    });

    it('should return the servicePopupDetails', () => {
      expect(selectors.getServicePopupDetails(mockState)).toStrictEqual({
        isOpen: false,
        url: '',
        serviceId: '',
        variant: 'add',
      });
      const mockState2 = {
        layerSelect: {
          ...initialState,
          servicePopup: {
            isOpen: false,
            url: 'someURL',
            serviceId: '123',
            variant: 'edit' as PopupVariant,
          },
        },
      };
      expect(selectors.getServicePopupDetails(mockState2)).toStrictEqual({
        isOpen: false,
        url: 'someURL',
        serviceId: '123',
        variant: 'edit',
      });
    });
  });
});
