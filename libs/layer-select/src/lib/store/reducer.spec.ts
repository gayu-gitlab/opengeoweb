/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { AnyAction } from '@reduxjs/toolkit';
import { serviceTypes, serviceActions } from '@opengeoweb/store';
import { reducer, initialState, layerSelectActions } from './reducer';
import { Filter, Filters, LayerSelectStoreType } from './types';
import { getFilterId } from './utils';

const filterId1 = `keywords-keyword1`;
const filterName1 = 'keyword1';
const filter1: Filter = {
  id: filterId1,
  name: filterName1,
  amountVisible: 1,
  checked: true,
  type: 'keywords',
};

const filterId2 = `keywords-keyword2`;
const filterName2 = 'keyword2';
const filter2: Filter = {
  id: filterId2,
  name: filterName2,
  amountVisible: 1,
  checked: true,
  type: 'keywords',
};

const serviceId1 = 'serviceid_1';
const serviceBase1 = {
  serviceId: serviceId1,
  serviceName: 'FMI',
  serviceUrl: 'https://testservice1',
  enabled: true,
  scope: 'system' as serviceTypes.ServiceScope,
};

const serviceId2 = 'serviceid_2';
const serviceBase2 = {
  serviceId: serviceId2,
  serviceName: 'METNO',
  serviceUrl: 'https://testservice2',
  enabled: true,
  scope: 'system' as serviceTypes.ServiceScope,
};

const serviceId3 = 'serviceid_3';
const serviceBase3 = {
  serviceId: serviceId3,
  serviceName: 'KNMI',
  serviceUrl: 'https://testurl3',
  enabled: true,
  scope: 'system' as serviceTypes.ServiceScope,
};
describe('store/layerSelect/reducer', () => {
  it('should return initial state if no state and action passed in', () => {
    expect(reducer(undefined, {} as AnyAction)).toEqual(initialState);
  });

  describe('searchFilter', () => {
    it('should update search filter when search filter is passed in', () => {
      const filterText = 'searchText';
      const action = layerSelectActions.setSearchFilter({ filterText });
      const expectedState: LayerSelectStoreType = {
        ...initialState,
        filters: {
          searchFilter: filterText,
          filters: {
            entities: {},
            ids: [],
          },
          activeServices: {
            entities: {},
            ids: [],
          },
        },
        allServicesEnabled: true,
      };
      expect(reducer(initialState, action)).toEqual(expectedState);
    });
  });

  describe('activeServices', () => {
    const stateAllActive: LayerSelectStoreType = {
      ...initialState,
      filters: {
        searchFilter: '',
        filters: {
          entities: {
            [filterId1]: {
              ...filter1,
              amountVisible: 3,
            },
          },
          ids: [filterId1],
        },
        activeServices: {
          entities: {
            [serviceId1]: {
              ...serviceBase1,
              filterIds: [filterId1],
            },
            [serviceId2]: {
              ...serviceBase2,
              filterIds: [filterId1],
            },
            [serviceId3]: {
              ...serviceBase3,
              filterIds: [filterId1],
            },
          },
          ids: [serviceId1, serviceId2, serviceId3],
        },
      },
      allServicesEnabled: true,
    };
    const stateAllDisabled: LayerSelectStoreType = {
      ...initialState,
      filters: {
        searchFilter: '',
        filters: {
          entities: {
            [filterId1]: {
              id: filterId1,
              name: filterName1,
              amountVisible: 0,
              checked: true,
              type: 'keywords',
            },
          },
          ids: [filterId1],
        },
        activeServices: {
          entities: {
            [serviceId1]: {
              ...serviceBase1,
              filterIds: [filterId1],
              enabled: false,
            },
            [serviceId2]: {
              ...serviceBase2,
              enabled: false,
              filterIds: [filterId1],
            },
            [serviceId3]: {
              ...serviceBase3,
              filterIds: [filterId1],
              enabled: false,
            },
          },
          ids: [serviceId1, serviceId2, serviceId3],
        },
      },
      allServicesEnabled: true,
    };

    it('should add new services to activeServices list and increment the amountVisible for given keywords', () => {
      const action = layerSelectActions.enableActiveService({
        serviceId: serviceId1,
      });
      const expectedState: LayerSelectStoreType = {
        ...initialState,
        filters: {
          searchFilter: '',
          filters: {
            entities: {
              [filterId1]: {
                id: filterId1,
                name: filterName1,
                amountVisible: expect.any(Number),
                checked: true,
                type: 'keywords',
              },
            },
            ids: [filterId1],
          },
          activeServices: {
            entities: {
              [serviceId1]: {
                ...serviceBase1,
                filterIds: [filterId1],
              },
              [serviceId2]: {
                ...serviceBase2,
                filterIds: [filterId1],
                enabled: false,
              },
              [serviceId3]: {
                ...serviceBase3,
                enabled: false,
                filterIds: [filterId1],
              },
            },
            ids: [serviceId1, serviceId2, serviceId3],
          },
        },
        allServicesEnabled: true,
      };
      expect(reducer(stateAllDisabled, action)).toEqual(expectedState);
    });

    it('should disable service in activeServices and decrement amountVisible for given keywords', () => {
      const action = layerSelectActions.disableActiveService({
        serviceId: serviceId1,
      });
      const expectedState: LayerSelectStoreType = {
        ...initialState,
        filters: {
          searchFilter: '',
          filters: {
            entities: {
              [filterId1]: {
                ...filter1,
                amountVisible: expect.any(Number),
              },
            },
            ids: [filterId1],
          },
          activeServices: {
            entities: {
              [serviceId1]: {
                ...serviceBase1,
                enabled: false,
                filterIds: [filterId1],
              },
              [serviceId2]: {
                ...serviceBase2,
                enabled: true,
                filterIds: [filterId1],
              },
              [serviceId3]: {
                ...serviceBase3,
                enabled: true,
                filterIds: [filterId1],
              },
            },
            ids: [serviceId1, serviceId2, serviceId3],
          },
        },
        allServicesEnabled: true,
      };
      expect(reducer(stateAllActive, action)).toEqual(expectedState);
    });

    it('should remove service in activeServices', () => {
      const action = layerSelectActions.layerSelectRemoveService({
        serviceId: serviceId1,
      });
      const expectedState: LayerSelectStoreType = {
        ...initialState,
        filters: {
          searchFilter: '',
          filters: {
            entities: {
              [filterId1]: { ...filter1, amountVisible: expect.any(Number) },
            },
            ids: [filterId1],
          },
          activeServices: {
            entities: {
              [serviceId2]: {
                ...serviceBase2,
                filterIds: [filterId1],
              },
              [serviceId3]: {
                ...serviceBase3,
                enabled: true,
                filterIds: [filterId1],
              },
            },
            ids: [serviceId2, serviceId3],
          },
        },
        allServicesEnabled: true,
      };
      expect(reducer(stateAllActive, action)).toEqual(expectedState);
    });

    describe('toggleFilters and enableOnlyOneFilter', () => {
      const filters: Filters = {
        entities: {
          [filterId1]: filter1,
          [filterId2]: filter2,
        },
        ids: [filterId1, filterId2],
      };
      const state: LayerSelectStoreType = {
        ...initialState,
        filters: {
          searchFilter: '',
          filters,
          activeServices: {
            entities: {},
            ids: [],
          },
        },
        allServicesEnabled: true,
      };
      const state2: LayerSelectStoreType = {
        ...initialState,
        filters: {
          searchFilter: '',
          filters: {
            entities: {
              [filterId1]: {
                ...filter1,
                checked: false,
              },
              [filterId2]: {
                ...filter2,
                checked: false,
              },
            },
            ids: [filterId1, filterId2],
          },
          activeServices: {
            entities: {},
            ids: [],
          },
        },
        allServicesEnabled: true,
      };

      const state3: LayerSelectStoreType = {
        ...initialState,
        filters: {
          searchFilter: '',
          filters: {
            entities: {
              [filterId1]: filter1,
              [filterId2]: {
                ...filter2,
                checked: false,
              },
            },
            ids: [filterId1, filterId2],
          },
          activeServices: {
            entities: {},
            ids: [],
          },
        },
        allServicesEnabled: true,
      };
      it('should toggle listed keywords', () => {
        const action = layerSelectActions.toggleFilter({
          filterIds: [filterId1, filterId2],
        });
        expect(reducer(state, action)).toEqual(state2);
        expect(reducer(state2, action)).toEqual(state);
      });

      it('should enable only clicked keyword when clicked only', () => {
        const action = layerSelectActions.enableOnlyOneFilter({
          filterId: filterId1,
        });
        expect(reducer(state, action)).toEqual(state3);
        expect(reducer(state2, action)).toEqual(state3);
      });
    });
  });

  describe('activeLayerInfo', () => {
    it('should update the activeLayerInfo', () => {
      const layer = {
        name: 'Test layer',
        title: 'Test layer wind precip',
        leaf: true,
        path: ['dummy'],
        serviceName: 'test-service',
      };
      const action = layerSelectActions.setActiveLayerInfo({ layer });
      const expectedState: LayerSelectStoreType = {
        ...initialState,
        activeLayerInfo: layer,
      };
      expect(reducer(initialState, action)).toEqual(expectedState);
    });
  });

  describe('closeServicePopupOpen', () => {
    it('should set isOpen to false', () => {
      const action = layerSelectActions.closeServicePopupOpen();
      const initialOpenState: LayerSelectStoreType = {
        ...initialState,
        servicePopup: {
          variant: 'add',
          isOpen: true,
          url: '',
          serviceId: '',
        },
      };

      const expectedState: LayerSelectStoreType = {
        ...initialState,
      };
      expect(reducer(initialOpenState, action)).toEqual(expectedState);
    });
  });

  describe('toggleServicePopup', () => {
    it('should set the passed in variant and isOpen prop and set the others to empty if not passed in', () => {
      const action = layerSelectActions.toggleServicePopup({
        isOpen: true,
        variant: 'edit',
      });

      const initialOpenState: LayerSelectStoreType = {
        ...initialState,
        servicePopup: {
          variant: 'add',
          isOpen: true,
          url: 'someurl',
          serviceId: '123',
        },
      };

      const expectedState: LayerSelectStoreType = {
        ...initialState,
        servicePopup: {
          isOpen: true,
          variant: 'edit',
          url: '',
          serviceId: '',
        },
      };
      expect(reducer(initialOpenState, action)).toEqual(expectedState);
    });

    it('should set the passed in variant and isOpen prop and adjust others as when passed in', () => {
      const action = layerSelectActions.toggleServicePopup({
        isOpen: true,
        variant: 'edit',
        url: 'newurl',
        serviceId: '456',
      });

      const initialOpenState: LayerSelectStoreType = {
        ...initialState,
        servicePopup: {
          variant: 'add',
          isOpen: true,
          url: 'someurl',
          serviceId: '123',
        },
      };

      const expectedState: LayerSelectStoreType = {
        ...initialState,
        servicePopup: {
          isOpen: true,
          variant: 'edit',
          url: 'newurl',
          serviceId: '456',
        },
      };
      expect(reducer(initialOpenState, action)).toEqual(expectedState);
    });
  });

  describe('serviceActions.serviceSetLayers', () => {
    it('should add group and keywords to a service', () => {
      const testGroups = ['path1', 'path2'];
      const testKeywords = ['keyword1', 'keyword2'];
      const testService = {
        id: 'serviceid_12',
        name: 'msgrt',
        serviceUrl:
          'https://adaguc-server-msg-cpp-portal.pmc.knmi.cloud/wms?DATASET=msgrt&',
        scope: 'user' as const,
        abstract:
          'This service demonstrates how the ADAGUC server can be used to create OGC services.',
        layers: [
          {
            name: 'test-layer-1',
            title: 'test layer 1',
            leaf: true,
            path: testGroups,
            keywords: testKeywords,
          },
        ],
      };

      const result = reducer(
        initialState,
        serviceActions.serviceSetLayers(testService),
      );

      expect(result.filters.activeServices.ids).toEqual([testService.id]);
      expect(result.filters.activeServices.entities).toEqual({
        [testService.id]: {
          serviceId: testService.id,
          enabled: true,
          filterIds: [
            ...testGroups.map((group) => getFilterId('groups', group)),
            ...testKeywords.map((group) => getFilterId('keywords', group)),
          ],
          groups: testGroups,
          keywords: testKeywords,
          scope: testService.scope,
          serviceName: testService.name,
          serviceUrl: testService.serviceUrl,
          abstract: testService.abstract,
          isLoading: false,
        },
      });
    });

    it('should do nothing is isUpdating', () => {
      const testGroups = ['path1', 'path2'];
      const testKeywords = ['keyword1', 'keyword2'];
      const testService = {
        id: 'serviceid_12',
        name: 'msgrt',
        serviceUrl:
          'https://adaguc-server-msg-cpp-portal.pmc.knmi.cloud/wms?DATASET=msgrt&',
        scope: 'user' as const,
        abstract:
          'This service demonstrates how the ADAGUC server can be used to create OGC services.',
        layers: [
          {
            name: 'test-layer-1',
            title: 'test layer 1',
            leaf: true,
            path: testGroups,
            keywords: testKeywords,
          },
        ],
        isUpdating: true,
      };

      const result = reducer(
        initialState,
        serviceActions.serviceSetLayers(testService),
      );

      expect(result).toEqual(initialState);
    });

    it('should not update state when layers are empty', () => {
      const testService = {
        id: 'serviceid_12',
        name: 'msgrt',
        serviceUrl:
          'https://adaguc-server-msg-cpp-portal.pmc.knmi.cloud/wms?DATASET=msgrt&',
        scope: 'user' as const,
        abstract:
          'This service demonstrates how the ADAGUC server can be used to create OGC services.',
        layers: [],
        isUpdating: true,
      };

      const result = reducer(
        initialState,
        serviceActions.serviceSetLayers(testService),
      );

      expect(result).toEqual(initialState);
    });
  });

  describe('serviceActions.fetchInitialServices', () => {
    it('should register all services as filter', () => {
      const testServices = [
        {
          id: 'serviceid_12',
          name: 'msgrt',
          serviceUrl:
            'https://adaguc-server-msg-cpp-portal.pmc.knmi.cloud/wms?DATASET=msgrt&',
          scope: 'user' as const,
          abstract:
            'This service demonstrates how the ADAGUC server can be used to create OGC services.',
        },
        {
          id: 'serviceid_11',
          name: 'DWD GeoServer WMSs',
          serviceUrl: 'https://maps.dwd.de/geoserver/dwd/WX-Produkt/ows?2',
          scope: 'user' as const,
          abstract: 'This is the Web Map Server of DWD.',
        },
      ];

      const result = reducer(
        initialState,
        serviceActions.fetchInitialServices({ services: testServices }),
      );

      const [serviceResult1, serviceResult2] = testServices;

      expect(result.filters.activeServices.ids).toEqual([
        serviceResult1.id,
        serviceResult2.id,
      ]);

      expect(result.filters.activeServices.entities[serviceResult1.id]).toEqual(
        {
          serviceId: serviceResult1.id,
          serviceName: serviceResult1.name,
          serviceUrl: serviceResult1.serviceUrl,
          abstract: serviceResult1.abstract,
          scope: serviceResult1.scope,
          filterIds: [],
          isLoading: true,
          enabled: true,
        },
      );

      expect(result.filters.activeServices.entities[serviceResult2.id]).toEqual(
        {
          serviceId: serviceResult2.id,
          serviceName: serviceResult2.name,
          serviceUrl: serviceResult2.serviceUrl,
          abstract: serviceResult2.abstract,
          scope: serviceResult2.scope,
          filterIds: [],
          isLoading: true,
          enabled: true,
        },
      );
    });

    it('should handle services as filter when store already has services', () => {
      const testServices = [
        {
          id: 'serviceid_12',
          name: 'msgrt',
          serviceUrl:
            'https://adaguc-server-msg-cpp-portal.pmc.knmi.cloud/wms?DATASET=msgrt&',
          scope: 'user' as const,
          abstract:
            'This service demonstrates how the ADAGUC server can be used to create OGC services.',
        },
        {
          id: 'serviceid_11',
          name: 'DWD GeoServer WMSs',
          serviceUrl: 'https://maps.dwd.de/geoserver/dwd/WX-Produkt/ows?2',
          scope: 'user' as const,
          abstract: 'This is the Web Map Server of DWD.',
        },
      ];

      const result = reducer(
        {
          ...initialState,
          filters: {
            searchFilter: '',
            filters: {
              entities: {},
              ids: [],
            },
            activeServices: {
              entities: {
                [testServices[0].id]: {
                  ...testServices[0],
                },
                [testServices[1].id]: {
                  ...testServices[1],
                },
              },
              ids: [testServices[0].id, testServices[1].id],
            },
          },
        },
        serviceActions.fetchInitialServices({ services: testServices }),
      );

      const [serviceResult1, serviceResult2] = testServices;

      expect(result.filters.activeServices.ids).toEqual([
        serviceResult1.id,
        serviceResult2.id,
      ]);

      expect(result.filters.activeServices.entities[serviceResult1.id]).toEqual(
        {
          serviceId: serviceResult1.id,
          serviceName: serviceResult1.name,
          serviceUrl: serviceResult1.serviceUrl,
          abstract: serviceResult1.abstract,
          scope: serviceResult1.scope,
          filterIds: [],
          isLoading: true,
          enabled: true,
        },
      );

      expect(result.filters.activeServices.entities[serviceResult2.id]).toEqual(
        {
          serviceId: serviceResult2.id,
          serviceName: serviceResult2.name,
          serviceUrl: serviceResult2.serviceUrl,
          abstract: serviceResult2.abstract,
          scope: serviceResult2.scope,
          filterIds: [],
          isLoading: true,
          enabled: true,
        },
      );
    });

    it('should handle empty services', () => {
      const testServices: serviceTypes.InitialService[] = [];

      const result = reducer(
        initialState,
        serviceActions.fetchInitialServices({ services: testServices }),
      );

      expect(result.filters.activeServices.ids).toEqual(
        initialState.filters.activeServices.ids,
      );

      expect(result.filters.activeServices.entities).toEqual(
        initialState.filters.activeServices.entities,
      );
    });
  });
});
