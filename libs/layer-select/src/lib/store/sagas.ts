/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { put, select, takeEvery, all } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import { WMLayer } from '@opengeoweb/webmap';
import {
  serviceActions,
  uiActions,
  uiSelectors,
  uiTypes,
} from '@opengeoweb/store';
import { getUserAddedServices, setUserAddedServices } from './localStorage';
import { layerSelectActions, LayerSelectActions } from './reducer';
import {
  ActiveServiceObjectEntities,
  LayerSelectRemoveServicePayload,
} from './types';
import { isUserAddedService } from './utils';
import {
  getActiveLayerInfo,
  getActiveServices,
  getFilteredLayers,
} from './selectors';

export function* layerSelectCloseInfoDialogSaga({
  type,
  payload,
}: ReturnType<
  | typeof uiActions.setToggleOpenDialog
  | typeof uiActions.setActiveMapIdForDialog
  | typeof layerSelectActions.disableActiveService
  | typeof layerSelectActions.toggleFilter
  | typeof layerSelectActions.setSearchFilter
>): SagaIterator {
  switch (type) {
    case uiActions.setToggleOpenDialog.type: {
      const togglePayload = payload as uiTypes.UIToggleDialogPayload;
      if (
        !togglePayload.setOpen &&
        togglePayload.type === uiTypes.DialogTypes.LayerSelect
      ) {
        yield put(
          uiActions.setToggleOpenDialog({
            type: uiTypes.DialogTypes.LayerInfo,
            setOpen: false,
          }),
        );
      }

      break;
    }
    case uiActions.setActiveMapIdForDialog.type: {
      const activeMapIdPayload =
        payload as uiTypes.SetActiveMapIdForDialogPayload;

      if (!activeMapIdPayload.setOpen) {
        const isLayerInfoDialogOpen = yield select(
          uiSelectors.getisDialogOpen,
          uiTypes.DialogTypes.LayerInfo,
        );
        if (isLayerInfoDialogOpen) {
          yield put(
            uiActions.setToggleOpenDialog({
              type: uiTypes.DialogTypes.LayerInfo,
              setOpen: false,
            }),
          );
        }
      }
      break;
    }
    default: {
      const isLayerInfoDialogOpen = yield select(
        uiSelectors.getisDialogOpen,
        uiTypes.DialogTypes.LayerInfo,
      );
      if (isLayerInfoDialogOpen) {
        const filteredLayers = yield select(getFilteredLayers);
        const dialogInfo = yield select(getActiveLayerInfo);
        if (
          !filteredLayers
            .map((layer: WMLayer) => layer.title)
            .includes(dialogInfo.title)
        ) {
          yield put(
            uiActions.setToggleOpenDialog({
              type: uiTypes.DialogTypes.LayerInfo,
              setOpen: false,
            }),
          );
        }
      }
    }
  }
}

export function* layerSelectRemoveServiceSaga(
  capturedAction: LayerSelectActions,
): SagaIterator {
  const removedService =
    capturedAction.payload as LayerSelectRemoveServicePayload;
  const { serviceId, serviceUrl } = removedService;
  const results = yield select(getActiveServices);
  const services = results as ActiveServiceObjectEntities;

  const servicesEnabled = Object.values(services).find(
    (service) => service?.enabled,
  );

  if (servicesEnabled === undefined) {
    yield all(
      Object.entries(services).map(([serviceId]) => {
        return put(
          layerSelectActions.enableActiveService({
            serviceId,
          }),
        );
      }),
    );
  }

  yield put(
    serviceActions.mapStoreRemoveService({
      id: serviceId,
      serviceUrl: serviceUrl!,
    }),
  );
}

export function addServiceToLocalStorageSaga({
  payload,
}: ReturnType<typeof serviceActions.serviceSetLayers>): void {
  const { scope, name, serviceUrl, abstract } = payload;
  if (!isUserAddedService(scope)) {
    return;
  }
  const localStorageServices = getUserAddedServices();

  setUserAddedServices({
    ...localStorageServices,
    [serviceUrl]: {
      name,
      url: serviceUrl,
      abstract,
    },
  });
}
export function removeServiceFromLocalStorageSaga({
  payload,
}: ReturnType<typeof serviceActions.mapStoreRemoveService>): void {
  const { serviceUrl } = payload;
  const localStorageServices = getUserAddedServices();
  if (!localStorageServices[serviceUrl]) {
    return;
  }

  const updatedServices = { ...localStorageServices };
  delete updatedServices[serviceUrl];

  setUserAddedServices(updatedServices);
}

export function* rootSaga(): SagaIterator {
  yield takeEvery(
    layerSelectActions.layerSelectRemoveService.type,
    layerSelectRemoveServiceSaga,
  );
  yield takeEvery(
    serviceActions.serviceSetLayers.type,
    addServiceToLocalStorageSaga,
  );
  yield takeEvery(
    serviceActions.mapStoreRemoveService.type,
    removeServiceFromLocalStorageSaga,
  );
  yield takeEvery(
    [
      uiActions.setToggleOpenDialog.type,
      uiActions.setActiveMapIdForDialog.type,
      layerSelectActions.disableActiveService.type,
      layerSelectActions.toggleFilter.type,
      layerSelectActions.setSearchFilter.type,
    ],
    layerSelectCloseInfoDialogSaga,
  );
}
