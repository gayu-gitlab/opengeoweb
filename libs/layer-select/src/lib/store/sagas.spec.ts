/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { all, put, select, takeEvery } from 'redux-saga/effects';
import {
  serviceActions,
  serviceTypes,
  uiActions,
  uiTypes,
} from '@opengeoweb/store';
import {
  rootSaga,
  layerSelectRemoveServiceSaga,
  addServiceToLocalStorageSaga,
  removeServiceFromLocalStorageSaga,
  layerSelectCloseInfoDialogSaga,
} from './sagas';
import { layerSelectActions } from './reducer';
import { setUserAddedServices } from './localStorage';
import { UserAddedServices } from './types';
import { layerSelectSelectors } from '.';

const { mapStoreRemoveService, serviceSetLayers } = serviceActions;
const { layerSelectRemoveService } = layerSelectActions;

beforeEach(() => {
  window.localStorage.clear();
});

describe('store/layerSelect/sagas', () => {
  describe('rootSaga', () => {
    it('should take action and fire correct sagas', () => {
      const generator = rootSaga();

      expect(generator.next().value).toEqual(
        takeEvery(layerSelectRemoveService.type, layerSelectRemoveServiceSaga),
      );
      expect(generator.next().value).toEqual(
        takeEvery(serviceSetLayers.type, addServiceToLocalStorageSaga),
      );
      expect(generator.next().value).toEqual(
        takeEvery(
          mapStoreRemoveService.type,
          removeServiceFromLocalStorageSaga,
        ),
      );
      expect(generator.next().value).toEqual(
        takeEvery(
          [
            uiActions.setToggleOpenDialog.type,
            uiActions.setActiveMapIdForDialog.type,
            layerSelectActions.disableActiveService.type,
            layerSelectActions.toggleFilter.type,
            layerSelectActions.setSearchFilter.type,
          ],
          layerSelectCloseInfoDialogSaga,
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
  });

  describe('LayerSelectRemoveServiceSaga', () => {
    it('should handle layerSelectRemoveService Saga', () => {});
    const DummyAction = layerSelectRemoveService({
      serviceId: 'serviceId_1',
      serviceUrl: 'https://service1',
    });

    const generator = layerSelectRemoveServiceSaga(DummyAction);
    expect(generator.next().value).toEqual(
      select(layerSelectSelectors.getActiveServices),
    );

    // mock results from yield select
    const servicesFromSelect = {
      serviceid_1: {
        serviceId: 'serviceid_1',
        enabled: false,
        filterIds: [],
        scope: 'system' as serviceTypes.ServiceScope,
        serviceName: 'service1',
        serviceUrl: 'https://service1',
      },
      serviceid_2: {
        serviceId: 'serviceid_2',
        enabled: false,
        filterIds: [],
        scope: 'system' as serviceTypes.ServiceScope,
        serviceName: 'service2',
        serviceUrl: 'https://service2',
      },
    };

    expect(generator.next(servicesFromSelect).value).toEqual(
      all([
        put(
          layerSelectActions.enableActiveService({
            serviceId: servicesFromSelect['serviceid_1'].serviceId,
          }),
        ),
        put(
          layerSelectActions.enableActiveService({
            serviceId: servicesFromSelect['serviceid_2'].serviceId,
          }),
        ),
      ]),
    );

    expect(generator.next().value).toEqual(
      put(
        mapStoreRemoveService({
          id: 'serviceId_1',
          serviceUrl: 'https://service1',
        }),
      ),
    );
    expect(generator.next().done).toBeTruthy();
  });

  describe('addServiceToLocalStorageSaga', () => {
    it('should not write non-user-added service to localstorage', () => {
      jest.spyOn(Object.getPrototypeOf(window.localStorage), 'setItem');
      const actionWithUserAddedService = serviceSetLayers({
        id: 'id_1',
        layers: [
          {
            abstract: '500m CAPPI images from Vihti radar',
            keywords: null!,
            leaf: false,
            name: 'Radar:radar_vihti_dbzh-cappi_3067',
            path: [],
            title: '500m CAPPI images from Vihti radar',
          },
        ],
        name: 'FMI',
        serviceUrl: 'https://openwms.fmi.fi/geoserver/wms?',
        scope: 'system',
      });
      addServiceToLocalStorageSaga(actionWithUserAddedService);
      expect(window.localStorage.setItem).not.toHaveBeenCalled();
    });

    it('should write user-added service to localstorage', () => {
      jest.spyOn(Object.getPrototypeOf(window.localStorage), 'setItem');
      const actionWithUserAddedService = serviceSetLayers({
        id: 'id_1',
        layers: [
          {
            abstract: '500m CAPPI images from Vihti radar',
            keywords: null!,
            leaf: false,
            name: 'Radar:radar_vihti_dbzh-cappi_3067',
            path: [],
            title: '500m CAPPI images from Vihti radar',
          },
        ],
        name: 'FMI',
        serviceUrl: 'https://openwms.fmi.fi/geoserver/wms?',
        scope: 'user',
        abstract: 'test abstract',
      });
      expect(window.localStorage.setItem).not.toHaveBeenCalled();
      addServiceToLocalStorageSaga(actionWithUserAddedService);
      expect(window.localStorage.setItem).toHaveBeenCalled();
      expect(
        JSON.parse(window.localStorage.getItem('userAddedServices')!),
      ).toEqual({
        'https://openwms.fmi.fi/geoserver/wms?': {
          name: 'FMI',
          url: 'https://openwms.fmi.fi/geoserver/wms?',
          abstract: actionWithUserAddedService.payload.abstract,
        },
      });
    });

    it('should correctly write two user-added services to localstorage', () => {
      jest.spyOn(Object.getPrototypeOf(window.localStorage), 'setItem');
      const actionWithUserAddedService1 = serviceSetLayers({
        id: 'id_1',
        layers: [
          {
            abstract: '500m CAPPI images from Vihti radar',
            keywords: null!,
            leaf: false,
            name: 'Radar:radar_vihti_dbzh-cappi_3067',
            path: [],
            title: '500m CAPPI images from Vihti radar',
          },
        ],
        name: 'FMI',
        serviceUrl: 'https://openwms.fmi.fi/geoserver/wms?',
        scope: 'user',
        abstract: 'test abstract',
      });
      const actionWithUserAddedService2 = serviceSetLayers({
        id: 'id_1',
        layers: [],
        name: 'test name',
        serviceUrl: 'https://testservice.fi/wms?',
        scope: 'user',
      });
      addServiceToLocalStorageSaga(actionWithUserAddedService1);
      addServiceToLocalStorageSaga(actionWithUserAddedService2);
      expect(
        JSON.parse(window.localStorage.getItem('userAddedServices')!),
      ).toEqual({
        'https://openwms.fmi.fi/geoserver/wms?': {
          name: 'FMI',
          url: 'https://openwms.fmi.fi/geoserver/wms?',
          scope: 'user',
          abstract: actionWithUserAddedService1.payload.abstract,
        },
        'https://testservice.fi/wms?': {
          name: 'test name',
          url: 'https://testservice.fi/wms?',
        },
      });
    });

    it('should not write duplicate user-added services to localstorage', () => {
      jest.spyOn(Object.getPrototypeOf(window.localStorage), 'setItem');
      const actionWithUserAddedService = serviceSetLayers({
        id: 'id_1',
        layers: [
          {
            abstract: '500m CAPPI images from Vihti radar',
            keywords: null!,
            leaf: false,
            name: 'Radar:radar_vihti_dbzh-cappi_3067',
            path: [],
            title: '500m CAPPI images from Vihti radar',
          },
        ],
        name: 'FMI',
        serviceUrl: 'https://openwms.fmi.fi/geoserver/wms?',
        scope: 'user',
      });
      // call the function twice with the same action
      addServiceToLocalStorageSaga(actionWithUserAddedService);
      addServiceToLocalStorageSaga(actionWithUserAddedService);

      expect(
        JSON.parse(window.localStorage.getItem('userAddedServices')!),
      ).toEqual({
        'https://openwms.fmi.fi/geoserver/wms?': {
          name: 'FMI',
          url: 'https://openwms.fmi.fi/geoserver/wms?',
        },
      });
    });
  });

  describe('deleteServiceFromLocalStorageSaga', () => {
    it('should do nothing when service to delete does not exist', () => {
      const initialUserAddedServices: UserAddedServices = {
        'https://testservice.fi/wms?': {
          name: 'test name',
          url: 'https://testservice.fi/wms?',
        },
      };
      jest.spyOn(Object.getPrototypeOf(window.localStorage), 'setItem');
      setUserAddedServices(initialUserAddedServices);

      const deleteServiceAction = mapStoreRemoveService({
        id: 'id_1',
        serviceUrl: 'https://wrongtestservice.fi/wms?',
      });
      expect(window.localStorage.setItem).toHaveBeenCalledTimes(1);
      removeServiceFromLocalStorageSaga(deleteServiceAction);
      expect(window.localStorage.setItem).toHaveBeenCalledTimes(1);

      expect(
        JSON.parse(window.localStorage.getItem('userAddedServices')!),
      ).toEqual({
        'https://testservice.fi/wms?': {
          name: 'test name',
          url: 'https://testservice.fi/wms?',
        },
      });
    });
    it('should delete service from localstorage', () => {
      const initialUserAddedServices: UserAddedServices = {
        'https://testservice.fi/wms?': {
          name: 'test name',
          url: 'https://testservice.fi/wms?',
        },
        'https://testservice2.fi/wms?': {
          name: 'test name 2',
          url: 'https://testservice2.fi/wms?',
        },
      };
      jest.spyOn(Object.getPrototypeOf(window.localStorage), 'setItem');
      setUserAddedServices(initialUserAddedServices);

      const deleteServiceAction = mapStoreRemoveService({
        id: 'id_1',
        serviceUrl: 'https://testservice.fi/wms?',
      });
      expect(window.localStorage.setItem).toHaveBeenCalledTimes(1);
      removeServiceFromLocalStorageSaga(deleteServiceAction);
      expect(window.localStorage.setItem).toHaveBeenCalledTimes(2);

      expect(
        JSON.parse(window.localStorage.getItem('userAddedServices')!),
      ).toEqual({
        'https://testservice2.fi/wms?': {
          name: 'test name 2',
          url: 'https://testservice2.fi/wms?',
          scope: 'user',
        },
      });
    });
  });

  describe('layerSelectCloseInfoDialogSaga', () => {
    it('should trigger close of layerinfo dialog if layerselect gets closed', () => {
      const action = uiActions.setToggleOpenDialog({
        type: uiTypes.DialogTypes.LayerSelect,
        setOpen: false,
      });

      const generator = layerSelectCloseInfoDialogSaga(action);
      expect(generator.next().value).toEqual(
        put(
          uiActions.setToggleOpenDialog({
            type: uiTypes.DialogTypes.LayerInfo,
            setOpen: false,
          }),
        ),
      );
    });
    it('should trigger close of layerinfo dialog if layerselect gets closed from activemap', () => {
      const action = uiActions.setActiveMapIdForDialog({
        mapId: 'map-1',
        type: uiTypes.DialogTypes.LayerSelect,
        setOpen: false,
      });
      const generator = layerSelectCloseInfoDialogSaga(action);
      const isLayerInfoDialogOpen = true;
      const currentActiveMapId = 'map-1';
      generator.next(isLayerInfoDialogOpen);
      expect(generator.next(currentActiveMapId).value).toEqual(
        put(
          uiActions.setToggleOpenDialog({
            type: uiTypes.DialogTypes.LayerInfo,
            setOpen: false,
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
    it('should be done if the info dialog is not open', () => {
      const action = layerSelectActions.disableActiveService({
        serviceId: 'serviceId1',
      });
      const generator = layerSelectCloseInfoDialogSaga(action);
      const isLayerInfoDialogOpen = false;
      generator.next(isLayerInfoDialogOpen);
      expect(generator.next(isLayerInfoDialogOpen).done).toBeTruthy();
    });
    it('should close the dialog when its not the layerlist', () => {
      const action = layerSelectActions.disableActiveService({
        serviceId: 'serviceId1',
      });
      const generator = layerSelectCloseInfoDialogSaga(action);
      const isLayerInfoDialogOpen = true;
      const filteredLayers = [{ title: 'title' }];
      const dialogInfo = { title: 'title-not-in-layer' };
      generator.next(isLayerInfoDialogOpen);
      expect(generator.next(isLayerInfoDialogOpen).value).toEqual(
        select(layerSelectSelectors.getFilteredLayers),
      );
      expect(generator.next(filteredLayers).value).toEqual(
        select(layerSelectSelectors.getActiveLayerInfo),
      );
      expect(generator.next(dialogInfo).value).toEqual(
        put(
          uiActions.setToggleOpenDialog({
            type: uiTypes.DialogTypes.LayerInfo,
            setOpen: false,
          }),
        ),
      );
      expect(generator.next().done).toBeTruthy();
    });
    it('should not close of dialog when its in the layerlist', () => {
      const action = layerSelectActions.disableActiveService({
        serviceId: 'serviceId1',
      });
      const generator = layerSelectCloseInfoDialogSaga(action);
      const isLayerInfoDialogOpen = true;
      const filteredLayers = [{ title: 'title' }];
      const dialogInfo = { title: 'title' };
      generator.next(isLayerInfoDialogOpen);
      expect(generator.next(isLayerInfoDialogOpen).value).toEqual(
        select(layerSelectSelectors.getFilteredLayers),
      );
      expect(generator.next(filteredLayers).value).toEqual(
        select(layerSelectSelectors.getActiveLayerInfo),
      );
      expect(generator.next(dialogInfo).done).toBeTruthy();
    });
  });
});
