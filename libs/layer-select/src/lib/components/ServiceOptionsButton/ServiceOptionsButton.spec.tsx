/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { lightTheme } from '@opengeoweb/theme';
import userEvent from '@testing-library/user-event';

import { ServiceOptionsButton } from './ServiceOptionsButton';
import { TestThemeStoreProvider } from '../testUtils/TestThemeStoreProvider';

describe('src/components/LayerSelect/ServiceOptionsButton', () => {
  const mockState = {
    layerSelect: {
      filters: {
        activeServices: {
          entities: {
            serviceid_1: {
              serviceName: 'Radar Norway',
              serviceUrl: 'https://halo-wms.met.no/halo/default.map?',
              enabled: true,
              filterIds: [],
            },
            serviceid_2: {
              serviceName: 'Precipitation Radar NL',
              serviceUrl: 'https://geoservices.knmi.nl/wms?dataset=RADAR&',
              enabled: true,
              filterIds: [],
            },
          },
          ids: ['serviceid_1', 'serviceid_2'],
        },
      },
    },
  };
  const store = createMockStoreWithEggs(mockState);
  const user = userEvent.setup();

  it('should render the component', () => {
    render(
      <TestThemeStoreProvider theme={lightTheme} store={store}>
        <ServiceOptionsButton mapId="map1" />,
      </TestThemeStoreProvider>,
    );

    const button = screen.queryByTestId('serviceOptionsButton');
    expect(button).toBeTruthy();
  });

  it('should open and close the service dialog', async () => {
    render(
      <TestThemeStoreProvider theme={lightTheme} store={store}>
        <ServiceOptionsButton mapId="map1" />,
      </TestThemeStoreProvider>,
    );

    await user.click(screen.queryByTestId('serviceOptionsButton')!);
    expect(screen.getByTestId('ServiceDialog')).toBeTruthy();

    await user.keyboard('{Escape}');
    expect(screen.queryByTestId('ServiceDialog')).toBeFalsy();
  });

  it('should open service dialog with working tab behavior', async () => {
    render(
      <TestThemeStoreProvider theme={lightTheme} store={store}>
        <ServiceOptionsButton mapId="map1" />,
      </TestThemeStoreProvider>,
    );

    expect(screen.queryByRole('list')).toBeFalsy();
    fireEvent.click(screen.getByTestId('serviceOptionsButton'));

    expect(screen.getByRole('list')).toBeTruthy();
    expect(screen.getAllByTestId('updateServiceButton')[0]).not.toHaveFocus();

    fireEvent.click(screen.getByTestId('ServiceDialog'));
    await user.tab();

    expect(screen.getByRole('list')).toBeTruthy();
    expect(screen.getAllByTestId('updateServiceButton')[0]).toHaveFocus();
  });
});
