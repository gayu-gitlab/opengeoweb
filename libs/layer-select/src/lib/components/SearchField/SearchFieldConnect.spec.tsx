/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { serviceTypes } from '@opengeoweb/store';

import { SearchFieldConnect } from './SearchFieldConnect';
import { TestThemeStoreProvider } from '../testUtils/TestThemeStoreProvider';
import { layerSelectActions } from '../../store';

describe('components/LayerManager/LayerSelect/SearchField/SearchFieldConnect', () => {
  it('should dispatch action to set the filter', async () => {
    const mockState = {
      layerSelect: {
        filters: {
          searchFilter: '',
          activeServices: {
            entities: {
              serviceid_1: {
                serviceName: 'MET Norway',
                serviceUrl: 'https://halo-wms.met.no/halo/default.map?',
                enabled: true,
                keywordsPerService: [],
                scope: 'system' as serviceTypes.ServiceScope,
              },
            },
            ids: ['serviceid_1'],
          },
          servicePopup: { isOpen: false, variant: 'add' },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    jest.useFakeTimers();

    render(
      <TestThemeStoreProvider store={store}>
        <SearchFieldConnect />
      </TestThemeStoreProvider>,
    );
    const textField = screen.getByRole('textbox');
    fireEvent.change(textField, { target: { value: 'abc' } });

    const expectedAction = layerSelectActions.setSearchFilter({
      filterText: 'abc',
    });

    jest.runOnlyPendingTimers();
    expect(store.getActions()).toEqual([expectedAction]);

    jest.clearAllTimers();
    jest.useRealTimers();
  });

  it('should dispatch action to open Service Popup to save WMS url', async () => {
    const mockState = {
      layerSelect: {
        filters: {
          searchFilter: '',
          activeServices: {
            entities: {
              serviceid_1: {
                serviceName: 'MET Norway',
                serviceUrl: 'https://halo-wms.met.no/halo/default.map?',
                enabled: true,
                keywordsPerService: [],
                scope: 'system' as serviceTypes.ServiceScope,
              },
            },
            ids: ['serviceid_1'],
          },
          servicePopup: { isOpen: false, variant: 'add' },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    jest.useFakeTimers();

    render(
      <TestThemeStoreProvider store={store}>
        <SearchFieldConnect />
      </TestThemeStoreProvider>,
    );
    const textField = screen.getByRole('textbox');
    fireEvent.change(textField, { target: { value: 'https://testService' } });

    jest.runOnlyPendingTimers();

    fireEvent.click(screen.getByText('Save'));

    const expectedAction = [
      layerSelectActions.toggleServicePopup({
        isOpen: true,
        url: 'https://testService',
        variant: 'save',
      }),
    ];
    expect(store.getActions()).toEqual(expectedAction);

    jest.clearAllTimers();
    jest.useRealTimers();
  });
});
