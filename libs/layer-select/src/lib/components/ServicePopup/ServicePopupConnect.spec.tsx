/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2021 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2021 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';

import { ServicePopupConnect } from './ServicePopupConnect';
import { TestThemeStoreProvider } from '../testUtils/TestThemeStoreProvider';
import { layerSelectInitialState, layerSelectActions } from '../../store';

describe('src/components/LayerManager/LayerSelect/ServicePopup/ServicePopupConnect', () => {
  const standardProps = {
    isOpen: true,
    variant: 'add',
    url: '',
    serviceId: '',
  };
  it('should set open the dialog if opened in store', async () => {
    const mockState = {
      layerSelect: {
        ...layerSelectInitialState,
        servicePopup: { ...standardProps },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <TestThemeStoreProvider store={store}>
        <ServicePopupConnect />
      </TestThemeStoreProvider>,
    );

    expect(screen.getByTestId('servicePopup')).toBeTruthy();
  });

  it('should fire action to close popup if pressed close icon', async () => {
    const mockState = {
      layerSelect: {
        ...layerSelectInitialState,
        servicePopup: { ...standardProps },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <TestThemeStoreProvider store={store}>
        <ServicePopupConnect />
      </TestThemeStoreProvider>,
    );

    const expectedAction = [layerSelectActions.closeServicePopupOpen()];
    fireEvent.click(screen.getByTestId('closePopupButtonCross'));
    expect(store.getActions()).toEqual(expectedAction);
  });
});
