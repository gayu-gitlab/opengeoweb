/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import {
  WMJSService,
  getCapabilities,
  mockGetCapabilities,
} from '@opengeoweb/webmap';
import {
  loadWMSService,
  validateServiceName,
  validateServiceUrl,
  VALIDATIONS_NAME_EXISTING,
  VALIDATIONS_SERVICE_EXISTING,
  VALIDATIONS_SERVICE_VALID_URL,
} from './utils';

const mockServiceFromStore = {
  title: 'service test title',
  abstract: 'service Abstract',
};

jest.mock('@opengeoweb/webmap', () => ({
  ...jest.requireActual('@opengeoweb/webmap'),
  WMGetServiceFromStore: (): WMJSService => mockServiceFromStore as WMJSService,
}));

describe('src/lib/components/LayerManager/ServicePopup/ServicePopup', () => {
  jest
    .spyOn(getCapabilities, 'getLayersFromService')
    .mockImplementation(mockGetCapabilities.mockGetLayersFromService);

  jest
    .spyOn(getCapabilities, 'getLayersFlattenedFromService')
    .mockImplementation(
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      mockGetCapabilities.mockGetLayersFlattenedFromService as any,
    );

  describe('validateServiceName', () => {
    it('should validate ServiceName for existing names', () => {
      expect(validateServiceName('test', {})).toBeTruthy();
      expect(
        validateServiceName('testing', { test: { serviceName: 'test' } }),
      ).toBeTruthy();

      expect(
        validateServiceName('test', { test: { serviceName: 'test' } }),
      ).toEqual(VALIDATIONS_NAME_EXISTING);
      expect(
        validateServiceName('Test', { test: { serviceName: 'test' } }),
      ).toEqual(VALIDATIONS_NAME_EXISTING);
    });
    it('should validate but ignore own name', () => {
      expect(
        validateServiceName('test', { test: { serviceName: 'test' } }, 'test'),
      ).toBeTruthy();
      expect(
        validateServiceName('Test', { test: { serviceName: 'test' } }, 'Test'),
      ).toBeTruthy();

      expect(
        validateServiceName(
          'testing',
          { testing: { serviceName: 'testing' } },
          'Test',
        ),
      ).toEqual(VALIDATIONS_NAME_EXISTING);
    });
  });

  describe('validateServiceUrl', () => {
    it('should validate validateServiceUrl URL is valid', () => {
      expect(validateServiceUrl('http://www.test.nl', {})).toBeTruthy();
      expect(validateServiceUrl('https://www.test.nl', {})).toBeTruthy();

      expect(validateServiceUrl('', {})).toEqual(VALIDATIONS_SERVICE_VALID_URL);
      expect(validateServiceUrl(undefined!, {})).toEqual(
        VALIDATIONS_SERVICE_VALID_URL,
      );
      expect(validateServiceUrl('://www.test.nl', {})).toEqual(
        VALIDATIONS_SERVICE_VALID_URL,
      );
      expect(validateServiceUrl('www.test.nl', {})).toEqual(
        VALIDATIONS_SERVICE_VALID_URL,
      );
      expect(validateServiceUrl('test.nl', {})).toEqual(
        VALIDATIONS_SERVICE_VALID_URL,
      );
    });

    it('should validate validateServiceUrl for existing Urls', () => {
      expect(
        validateServiceUrl('http://www.test.nl', {
          test: { serviceUrl: 'https://www.test.nl' },
        }),
      ).toBeTruthy();
      expect(
        validateServiceUrl('http://www.test.nl', {
          test: { serviceUrl: 'http://www.test.nl' },
        }),
      ).toEqual(VALIDATIONS_SERVICE_EXISTING);
      expect(
        validateServiceUrl('https://www.test.nl', {
          test: { serviceUrl: 'https://www.test.nl' },
        }),
      ).toEqual(VALIDATIONS_SERVICE_EXISTING);
    });
  });
  describe('loadWMSService', () => {
    it('should fetch layers', async () => {
      const layers = await loadWMSService(
        mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
        true,
      );
      expect(layers.abstract).toBe('service Abstract');
      expect(layers.layers).toHaveLength(2);
    });

    it('should fetch and return no layers', async () => {
      const layers = await loadWMSService(
        mockGetCapabilities.MOCK_URL_NO_CHILDREN,
        true,
      );
      expect(layers.abstract).toBe('service Abstract');
      expect(layers.layers).toHaveLength(0);
    });

    it('should throw an exception', async () => {
      try {
        await loadWMSService('invalid', true);
      } catch (e) {
        expect(e.message).toBe("Url 'invalid' is not a mock-url.");
      }
    });
  });
});
