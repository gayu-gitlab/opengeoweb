/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { useDispatch } from 'react-redux';

import {
  uiActions,
  uiTypes,
  uiSelectors,
  serviceTypes,
} from '@opengeoweb/store';
import { LayerInfoButton } from './LayerInfoButton';
import {
  useAppSelector,
  layerSelectSelectors,
  layerSelectActions,
} from '../../store';

interface LayerInfoButtonProps {
  isLayerMissing?: boolean;
  layer: serviceTypes.ServiceLayer;
  mapId: string;
  serviceName: string;
  source?: uiTypes.Source;
}

export const LayerInfoButtonConnect: React.FC<LayerInfoButtonProps> = ({
  isLayerMissing,
  layer,
  mapId,
  serviceName,
  source = 'app',
}: LayerInfoButtonProps) => {
  const dispatch = useDispatch();

  const currentActiveMapId = useAppSelector((store) =>
    uiSelectors.getDialogMapId(store, uiTypes.DialogTypes.LayerInfo),
  );

  const isOpenInStore = useAppSelector((store) =>
    uiSelectors.getisDialogOpen(store, uiTypes.DialogTypes.LayerInfo),
  );

  const currentLayerInfo = useAppSelector((store) =>
    layerSelectSelectors.getActiveLayerInfo(store),
  );

  const isActive =
    layer.name === currentLayerInfo?.name &&
    serviceName === currentLayerInfo?.serviceName;

  const toggleInfoDialog = React.useCallback((): void => {
    dispatch(
      uiActions.setActiveMapIdForDialog({
        type: uiTypes.DialogTypes.LayerInfo,
        mapId,
        setOpen: currentActiveMapId !== mapId ? true : !isOpenInStore,
        source,
      }),
    );
  }, [currentActiveMapId, dispatch, isOpenInStore, mapId, source]);

  const setActiveLayerInfo = React.useCallback((): void => {
    dispatch(
      layerSelectActions.setActiveLayerInfo({
        layer: { ...layer, serviceName },
      }),
    );

    dispatch(
      uiActions.orderDialog({
        type: uiTypes.DialogTypes.LayerInfo,
      }),
    );
  }, [dispatch, layer, serviceName]);

  const onClick = (): void => {
    if (isActive) {
      toggleInfoDialog();
    }
    if (!isActive && isOpenInStore) {
      setActiveLayerInfo();
    }
    if (!isActive && !isOpenInStore) {
      setActiveLayerInfo();
      toggleInfoDialog();
    }
  };

  return (
    <LayerInfoButton
      disabled={isLayerMissing}
      isActive={isActive}
      isOpenInStore={isOpenInStore}
      onClick={onClick}
    />
  );
};
