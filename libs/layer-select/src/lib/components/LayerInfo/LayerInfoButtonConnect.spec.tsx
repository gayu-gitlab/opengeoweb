/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { lightTheme } from '@opengeoweb/theme';
import { uiActions, uiTypes } from '@opengeoweb/store';
import { LayerInfoButtonConnect } from './LayerInfoButtonConnect';
import { TestThemeStoreProvider } from '../testUtils/TestThemeStoreProvider';
import { layerSelectActions } from '../../store';

const props = {
  layer: {
    name: 'test',
    title: 'test',
    leaf: true,
    path: [],
  },
  serviceName: 'test-service',
  mapId: '124',
};

describe('src/components/LayerSelect/layerInfo/LayerInfoButtonConnect', () => {
  it('should render the component', () => {
    const store = createMockStoreWithEggs({});

    render(
      <TestThemeStoreProvider theme={lightTheme} store={store}>
        <LayerInfoButtonConnect {...props} />
      </TestThemeStoreProvider>,
    );

    const button = screen.getByRole('button', { name: 'layer info' });
    expect(button).toBeTruthy();
  });

  it('should set active layer and open dialog when dialog is not open yet', () => {
    const mockState = {
      ui: {
        layerInfo: {
          type: uiTypes.DialogTypes.LayerInfo,
          activeMapId: '',
          isOpen: false,
          source: 'app',
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <TestThemeStoreProvider store={store}>
        <LayerInfoButtonConnect {...props} />
      </TestThemeStoreProvider>,
    );
    const button = screen.getByRole('button', { name: 'layer info' });
    expect(button).toBeTruthy();
    fireEvent.click(button);

    const expectedActions = [
      layerSelectActions.setActiveLayerInfo({
        layer: { ...props.layer, serviceName: props.serviceName },
      }),
      uiActions.orderDialog({
        type: uiTypes.DialogTypes.LayerInfo,
      }),
      uiActions.setActiveMapIdForDialog({
        type: uiTypes.DialogTypes.LayerInfo,
        setOpen: true,
        mapId: props.mapId,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should update the active layer info when dialog is already open for another layer', () => {
    const mockState = {
      ui: {
        dialogs: {
          layerInfo: {
            type: uiTypes.DialogTypes.LayerInfo,
            activeMapId: '',
            isOpen: true,
            source: 'app',
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <TestThemeStoreProvider store={store}>
        <LayerInfoButtonConnect {...props} />
      </TestThemeStoreProvider>,
    );

    const button = screen.getByRole('button', { name: 'layer info' });
    expect(button).toBeTruthy();
    fireEvent.click(button);

    const expectedActions = [
      layerSelectActions.setActiveLayerInfo({
        layer: { ...props.layer, serviceName: props.serviceName },
      }),
      uiActions.orderDialog({
        type: uiTypes.DialogTypes.LayerInfo,
      }),
    ];

    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should close the dialog when dialog is open for current layer', () => {
    const mockStateUi = {
      dialogs: {
        layerInfo: {
          type: uiTypes.DialogTypes.LayerInfo,
          activeMapId: props.mapId,
          isOpen: true,
          source: 'app',
        },
      },
    };
    const mockStateLayerSelect = {
      activeLayerInfo: { ...props.layer, serviceName: props.serviceName },
    };
    const mockState = {
      ui: mockStateUi,
      layerSelect: mockStateLayerSelect,
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <TestThemeStoreProvider store={store}>
        <LayerInfoButtonConnect {...props} />
      </TestThemeStoreProvider>,
    );

    const button = screen.getByRole('button', { name: 'layer info' });
    expect(button).toBeTruthy();

    fireEvent.click(button);
    const expectedActions = [
      uiActions.setActiveMapIdForDialog({
        type: uiTypes.DialogTypes.LayerInfo,
        setOpen: false,
        mapId: props.mapId,
        source: 'app',
      }),
    ];

    expect(store.getActions()).toEqual(expectedActions);
  });

  it('should update the active layer info when dialog is open for a layer with the same name but from a different service', () => {
    const mockStateUi = {
      dialogs: {
        layerInfo: {
          type: uiTypes.DialogTypes.LayerInfo,
          activeMapId: '',
          isOpen: true,
          source: 'app',
        },
      },
    };
    const mockStateLayerSelect = {
      activeLayerInfo: { ...props.layer, serviceName: 'different-service' },
    };
    const mockState = {
      ui: mockStateUi,
      layerSelect: mockStateLayerSelect,
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <TestThemeStoreProvider store={store}>
        <LayerInfoButtonConnect {...props} />
      </TestThemeStoreProvider>,
    );

    const button = screen.getByRole('button', { name: 'layer info' });
    expect(button).toBeTruthy();
    fireEvent.click(button);

    const expectedActions = [
      layerSelectActions.setActiveLayerInfo({
        layer: { ...props.layer, serviceName: props.serviceName },
      }),
      uiActions.orderDialog({
        type: uiTypes.DialogTypes.LayerInfo,
      }),
    ];

    expect(store.getActions()).toEqual(expectedActions);
  });
});
