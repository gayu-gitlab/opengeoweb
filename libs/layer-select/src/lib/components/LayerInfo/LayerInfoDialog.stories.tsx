/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { ThemeWrapper, darkTheme, lightTheme } from '@opengeoweb/theme';

import { LayerInfoDialog } from './LayerInfoDialog';

export default {
  title: 'components/LayerSelect/LayerInfo',
};

const props = {
  onClose: (): void => {},
  dialogHeight: 985,
  layer: {
    name: 'hirlam:isobaric:temperature',
    title: 'HIRLAM Temperature Isobaric',
    leaf: true,
    path: ['models', 'hirlam', 'isobaric'],
    keywords: ['model', 'hirlam', 'temperature', 'isobaric'],
    abstract: 'Lorem ipsum dolor sit amet.',
    styles: [
      {
        title: 'countours',
        name: 'external-radiation',
        legendURL: '',
        abstract: 'Ulkoisen säteilyn annosnopeus',
      },
      {
        title: 'isotherms',
        name: 'external-radiation-2',
        legendURL: '',
        abstract: 'Ulkoisen säteilyn annosnopeus 2',
      },
    ],
    serviceName: 'FMI',
    dimensions: [
      {
        name: 'time',
        values: '2022-10-04T00:00:00Z/PT1H',
        units: 'ISO8601',
        currentValue: '2022-10-04T00:00:00Z',
      },
      {
        name: 'reference_time',
        values: '2022-10-05,2022-10-06',
        units: 'ISO8601',
        currentValue: '2022-10-05',
      },
      {
        name: 'modellevel',
        values: '1,2,3,4,5',
        units: '-',
        currentValue: '1',
      },
      {
        name: 'elevation',
        values: '1000,500,100',
        units: 'hPa',
        currentValue: '1000',
      },
      {
        name: 'member',
        values: '1,2,3',
        units: '-',
        currentValue: '1',
      },
    ],
    geographicBoundingBox: {
      north: '64',
      south: '58',
      east: '24',
      west: '-18',
    },
  },
  isOpen: true,
};

export const LayerInfoDialogLight = (): React.ReactElement => {
  return (
    <ThemeWrapper theme={lightTheme}>
      <LayerInfoDialog {...props} />
    </ThemeWrapper>
  );
};
LayerInfoDialogLight.storyName = 'Layer Info Dialog Light (takeSnapshot)';
LayerInfoDialogLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62c695a1ed42f01adee34e63/version/633551ddd63e4c4d715ab03d',
    },
  ],
};

export const LayerInfoDialogDark = (): React.ReactElement => {
  return (
    <ThemeWrapper theme={darkTheme}>
      <LayerInfoDialog {...props} />
    </ThemeWrapper>
  );
};
LayerInfoDialogDark.storyName = 'Layer Info Dialog Dark (takeSnapshot)';
LayerInfoDialogDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62d7b06898ff1c11dbcc0d0a/version/63355206daa88e4eda6803bc',
    },
  ],
};
