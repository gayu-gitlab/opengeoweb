/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { storeTestSettings } from '@opengeoweb/store';
import { LayerListConnect } from './LayerListConnect';
import { TestThemeStoreProvider } from '../testUtils/TestThemeStoreProvider';

const props = {
  mapId: 'map_id',
  layerSelectHeight: 500,
  serviceListHeight: 200,
  layerSelectWidth: 500,
};
describe('src/components/LayerList/LayerListConnect', () => {
  it('should render component', async () => {
    const mockState = {
      services: {
        byId: storeTestSettings.defaultReduxServices,
        allIds: ['serviceid_1'],
      },
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <TestThemeStoreProvider store={store}>
        <LayerListConnect {...props} />
      </TestThemeStoreProvider>,
    );
    expect(screen.getByTestId('layerList')).toBeTruthy();
  });
});
