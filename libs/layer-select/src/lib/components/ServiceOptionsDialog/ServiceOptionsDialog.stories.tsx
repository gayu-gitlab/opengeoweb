/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { darkTheme, lightTheme } from '@opengeoweb/theme';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { layerTypes } from '@opengeoweb/store';

import { ServiceOptionsDialog } from './ServiceOptionsDialog';
import { TestThemeStoreProvider } from '../testUtils/TestThemeStoreProvider';
import { layerSelectTypes } from '../../store';

export default {
  title: 'components/LayerSelect/ServiceOptionsDialog',
};

const services: layerSelectTypes.ActiveServiceObjectEntities = {
  serviceid_1: {
    serviceName: 'MET Norway',
    serviceUrl: 'https://service1',
    enabled: true,
    filterIds: [],
    scope: 'user',
  },
  serviceid_2: {
    serviceName: 'KNMI Radar',
    serviceUrl: 'https://service2',
    enabled: true,
    filterIds: [],
  },
  serviceid_3: {
    serviceName: 'NOAA Tropical Cyclones with long name',
    serviceUrl: 'https://service3',
    enabled: true,
    filterIds: [],
  },
};

const selectedLayers = [] as layerTypes.Layer[];

const activeServices = { entities: services, ids: [] };
const mockState = { layerSelect: { filters: { activeServices } } };
const store = createMockStoreWithEggs(mockState);

export const ServiceOptionsDialogLight = (): React.ReactElement => (
  <TestThemeStoreProvider theme={lightTheme} store={store}>
    <ServiceOptionsDialog services={services} selectedLayers={selectedLayers} />
  </TestThemeStoreProvider>
);

export const ServiceOptionsDialogDark = (): React.ReactElement => (
  <TestThemeStoreProvider theme={darkTheme} store={store}>
    <ServiceOptionsDialog services={services} selectedLayers={selectedLayers} />
  </TestThemeStoreProvider>
);

ServiceOptionsDialogLight.storyName =
  'Service Options Dialog Light Theme (takeSnapshot)';

ServiceOptionsDialogLight.parameters = {
  zeplinLink: [
    {
      name: 'Light theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62c695a1ed42f01adee34e63/version/632b1738c788c83cc9d5da8d',
    },
  ],
};

ServiceOptionsDialogDark.storyName =
  'Service Options Dialog Dark Theme (takeSnapshot)';

ServiceOptionsDialogDark.parameters = {
  zeplinLink: [
    {
      name: 'Dark theme',
      link: 'https://app.zeplin.io/project/5ecf84a3c6ae1047a368f393/screen/62d7b06898ff1c11dbcc0d0a/version/632b1ae593c99c3b8f379504',
    },
  ],
};
