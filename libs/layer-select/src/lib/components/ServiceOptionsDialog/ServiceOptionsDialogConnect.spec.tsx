/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import {
  clearImageCache,
  getCapabilities,
  mockGetCapabilities,
  WMJSService,
} from '@opengeoweb/webmap';
import { serviceActions, serviceTypes } from '@opengeoweb/store';
import { snackbarActions } from '@opengeoweb/snackbar';
import { ServiceOptionsDialogConnect } from './ServiceOptionsDialogConnect';
import { TestThemeStoreProvider } from '../testUtils/TestThemeStoreProvider';
import { layerSelectActions } from '../../store';

const mockServiceFromStore = {
  id: 'serviceid_1',
  title: 'service test title',
  abstract: 'service Abstract',
};

jest.mock('@opengeoweb/webmap', () => ({
  ...jest.requireActual('@opengeoweb/webmap'),
  WMGetServiceFromStore: (): WMJSService => mockServiceFromStore as WMJSService,
  clearImageCache: jest.fn(),
}));

const mockState = {
  layerSelect: {
    filters: {
      activeServices: {
        entities: {
          serviceid_1: {
            serviceName: 'Radar Norway',
            serviceUrl: mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
            enabled: true,
            filterIds: [],
            scope: 'system' as serviceTypes.ServiceScope,
          },
          serviceid_2: {
            serviceName: 'Precipitation Radar NL',
            serviceUrl: mockGetCapabilities.MOCK_URL_INVALID,
            enabled: true,
            filterIds: [],
            scope: 'user' as serviceTypes.ServiceScope,
          },
        },
        ids: ['serviceid_1', 'serviceid_2'],
      },
    },
  },
};
describe('src/lib/components/LayerManager/ServiceOptionsDialogConnect', () => {
  const store = createMockStoreWithEggs(mockState);

  jest
    .spyOn(getCapabilities, 'getLayersFromService')
    .mockImplementation(mockGetCapabilities.mockGetLayersFromService);

  jest
    .spyOn(getCapabilities, 'getLayersFlattenedFromService')
    .mockImplementation(
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      mockGetCapabilities.mockGetLayersFlattenedFromService as any,
    );

  it('should render the component', () => {
    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </TestThemeStoreProvider>,
    );

    expect(screen.getByTestId('ServiceDialog')).toBeTruthy();
  });

  it('should dispatch LayerSelect removeService and openSnackbar action on click', async () => {
    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </TestThemeStoreProvider>,
    );

    const expectedAction = [
      snackbarActions.openSnackbar({
        message: `Service "${mockState.layerSelect.filters.activeServices.entities.serviceid_2.serviceName}" has been deleted`,
      }),
      layerSelectActions.layerSelectRemoveService({
        serviceId: 'serviceid_2',
        serviceUrl: 'https://notawmsservice.nl',
      }),
    ];
    fireEvent.click(screen.getByRole('button', { name: /delete service/i }));
    expect(store.getActions()).toEqual(expectedAction);
  });

  it('should dispatch serviceSetLayers and openSnackbar action on updateServiceButton click', async () => {
    const store = createMockStoreWithEggs(mockState);

    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </TestThemeStoreProvider>,
    );

    const expectedAction = [
      serviceActions.serviceSetLayers({
        id: mockServiceFromStore.id,
        name: mockState.layerSelect.filters.activeServices.entities.serviceid_1
          .serviceName,
        serviceUrl: mockGetCapabilities.MOCK_URL_WITH_CHILDREN,
        abstract: mockServiceFromStore.abstract,
        layers: [
          {
            name: 'RAD_NL25_PCP_CM',
            title: 'RAD_NL25_PCP_CM',
            leaf: true,
            path: [],
            keywords: [],
            abstract: '',
            styles: [],
            dimensions: [],
            geographicBoundingBox: undefined,
          },
          {
            name: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
            title: 'RADNL_OPER_R___25PCPRR_L3_KNMI',
            leaf: true,
            path: [],
            keywords: [],
            abstract: '',
            styles: [],
            dimensions: [],
            geographicBoundingBox: undefined,
          },
        ],
        scope:
          mockState.layerSelect.filters.activeServices.entities.serviceid_1
            .scope,
        isUpdating: true,
      } as serviceTypes.SetLayersForServicePayload),
      snackbarActions.openSnackbar({
        message: `Service "${mockState.layerSelect.filters.activeServices.entities.serviceid_1.serviceName}" has been succesfully updated`,
      }),
    ];

    const buttons = screen.getAllByTestId('updateServiceButton')[1];
    fireEvent.click(buttons);

    await waitFor(() => expect(store.getActions()).toEqual(expectedAction));

    expect(clearImageCache).toHaveBeenCalledTimes(1);
  });

  it('should dispatch openSnackbar action informing about an error on updateServiceButton click', async () => {
    const store = createMockStoreWithEggs(mockState);

    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </TestThemeStoreProvider>,
    );

    const expectedAction = [
      snackbarActions.openSnackbar({
        message:
          'Unable to update service, reason: "Url \'https://notawmsservice.nl\' is not a wms service."',
      }),
    ];

    const buttons = screen.getAllByTestId('updateServiceButton')[0];
    fireEvent.click(buttons);

    await waitFor(() => expect(store.getActions()).toEqual(expectedAction));

    expect(clearImageCache).toHaveBeenCalledTimes(0);
  });

  it('should dispatch trigger toggleServicePopup on add service', async () => {
    const store = createMockStoreWithEggs(mockState);

    render(
      <TestThemeStoreProvider store={store}>
        <ServiceOptionsDialogConnect />
      </TestThemeStoreProvider>,
    );

    fireEvent.click(screen.getByTestId('openAddServiceButton'));

    const expectedAction = [
      layerSelectActions.toggleServicePopup({
        isOpen: true,
        variant: 'add',
      }),
    ];

    expect(store.getActions()).toEqual(expectedAction);
  });
});
