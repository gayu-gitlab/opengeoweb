/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import * as utils from '@opengeoweb/api';
import { CreateApiProps, createFakeApiInstance } from '@opengeoweb/api';
import { createApi } from './api';

describe('src/utils/api', () => {
  const fakeAxiosInstance = createFakeApiInstance();
  const fakeApiParams: CreateApiProps = {
    config: {
      baseURL: 'fakeURL',
    },
  };
  const fakeApiUrl = 'fakealerts.fmi.fi';
  const fakeAlertUrl = 'fakealerts.fmi.fi/alert1';

  describe('createApi', () => {
    it('should contain all api calls', async () => {
      const api = createApi(fakeApiParams);
      expect(api.getAllAlerts).toBeTruthy();
      expect(api.getLastUpdated).toBeTruthy();
    });

    it('should call with the right params for getAllAlerts', async () => {
      jest
        .spyOn(utils, 'createNonAuthApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      await api.getAllAlerts!(fakeApiUrl);
      expect(spy).toHaveBeenCalledWith(`/allalerts?url=${fakeApiUrl}`);
    });

    it('should call with the right params for getFeedLinks', async () => {
      jest
        .spyOn(utils, 'createNonAuthApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      await api.getFeedLinks!(fakeApiUrl);
      expect(spy).toHaveBeenCalledWith(`/feedlinks?url=${fakeApiUrl}`);
    });

    it('should call with the right params for getSingleAlert', async () => {
      jest
        .spyOn(utils, 'createNonAuthApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      await api.getSingleAlert!(fakeAlertUrl);
      expect(spy).toHaveBeenCalledWith(`/singlealert?url=${fakeAlertUrl}`);
    });

    it('should call with the right params for getLastUpdated', async () => {
      jest
        .spyOn(utils, 'createNonAuthApiInstance')
        .mockReturnValueOnce(fakeAxiosInstance);
      const spy = jest.spyOn(fakeAxiosInstance, 'get');
      const api = createApi(fakeApiParams);

      await api.getLastUpdated!(fakeApiUrl);
      expect(spy).toHaveBeenCalledWith(`/poll_feed?url=${fakeApiUrl}`);
    });
  });
});
