/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { feedLanguage } from './feedLanguageUtils';

describe('libs/core/src/lib/utils/DimensionUtils', () => {
  describe('feedLanguage', () => {
    it('should return given language', () => {
      const language = 'fi-FI';
      expect(feedLanguage(language)).toEqual(language);
    });
    it('should return default language if language not given', () => {
      const language = '';
      const defaultLanguage = 'en-US';
      expect(feedLanguage(language)).toEqual(defaultLanguage);
      expect(feedLanguage(undefined)).toEqual(defaultLanguage);
      expect(feedLanguage()).toEqual(defaultLanguage);
    });
  });
});
