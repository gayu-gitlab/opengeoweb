/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import capTrans from '../../../locales/cap.json';

export const CAP_NAMESPACE = 'cap';

export const initCapTestI18n = (): void => {
  i18n.use(initReactI18next).init({
    lng: 'en',
    ns: CAP_NAMESPACE,
    interpolation: {
      escapeValue: false,
    },
    resources: {
      en: {
        cap: {
          ...capTrans.en,
        },
      },
      fi: {
        cap: {
          ...capTrans.fi,
        },
      },
    },
  });
};
