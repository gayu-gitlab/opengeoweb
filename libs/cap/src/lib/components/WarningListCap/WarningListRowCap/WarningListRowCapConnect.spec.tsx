/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';
import { render, waitFor, screen } from '@testing-library/react';
import { createMockStoreWithEggs } from '@opengeoweb/shared';
import { WarningListRowCapConnect } from './WarningListRowCapConnect';
import { CapThemeStoreProvider } from '../../Providers';
import { mockCapWarningPresets } from '../../../utils/mockCapData';
import { mockCapStateWithAlerts } from '../../../store/';

const presetsSwedishLanguagePriority = {
  ...mockCapWarningPresets,
  feeds: [
    {
      feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
      feedType: 'rss',
      languagePriority: 'sv-FI',
    },
  ],
};

const presetsFrenchLanguagePriority = {
  ...mockCapWarningPresets,
  feeds: [
    {
      feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
      feedType: 'rss',
      languagePriority: 'fr-FR',
    },
  ],
};

const presetsNoLanguagePriority = {
  ...mockCapWarningPresets,
  feeds: [
    {
      feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
      feedType: 'rss',
      languagePriority: '',
    },
  ],
};

const props = {
  alertId: '2.49.0.1.246.0.0.2022',
  mockCapWarningPresets,
};

describe('src/components/LayerList/WarningListRowCapConnect', () => {
  it('should render component', async () => {
    const mockState = { capStore: { ...mockCapStateWithAlerts } };
    const store = createMockStoreWithEggs(mockState);
    const { container } = render(
      <CapThemeStoreProvider store={store}>
        <WarningListRowCapConnect
          alertId={props.alertId}
          capWarningPresets={props.mockCapWarningPresets}
        />
      </CapThemeStoreProvider>,
    );
    await waitFor(() => expect(container).toBeTruthy());
  });

  it('should use the language priority defined in the preset if available', async () => {
    const mockState = { capStore: { ...mockCapStateWithAlerts } };
    const store = createMockStoreWithEggs(mockState);
    render(
      <CapThemeStoreProvider store={store}>
        <WarningListRowCapConnect
          alertId={props.alertId}
          capWarningPresets={presetsSwedishLanguagePriority}
        />
      </CapThemeStoreProvider>,
    );
    expect((await screen.findByTestId('event')).textContent).toEqual(
      'Varning för skogsbrand',
    );
  });

  it('should use English when available if warning is not available in given language priority', async () => {
    const mockState = { capStore: { ...mockCapStateWithAlerts } };
    const store = createMockStoreWithEggs(mockState);
    render(
      <CapThemeStoreProvider store={store}>
        <WarningListRowCapConnect
          alertId={props.alertId}
          capWarningPresets={presetsFrenchLanguagePriority}
        />
      </CapThemeStoreProvider>,
    );
    expect((await screen.findByTestId('event')).textContent).toEqual(
      'Forest fire warning',
    );
  });

  it('should use the first language available if the language priority or English cannot be found', async () => {
    const capStoreNoEnglish = { ...mockCapStateWithAlerts };
    capStoreNoEnglish.alerts.entities[
      '2.49.0.1.246.0.0.2022'
    ]!.languages[2].language = 'fr-FR';
    const mockState = { capStore: { ...capStoreNoEnglish } };
    const store = createMockStoreWithEggs(mockState);
    render(
      <CapThemeStoreProvider store={store}>
        <WarningListRowCapConnect
          alertId={props.alertId}
          capWarningPresets={presetsNoLanguagePriority}
        />
      </CapThemeStoreProvider>,
    );
    expect((await screen.findByTestId('event')).textContent).toEqual(
      'Metsäpalovaroitus',
    );
  });
});
