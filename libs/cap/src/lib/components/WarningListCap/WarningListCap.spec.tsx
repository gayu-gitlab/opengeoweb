/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render, screen } from '@testing-library/react';
import { createStore } from '@redux-eggs/redux-toolkit';
import { WarningListCap } from './WarningListCap';
import { mockGeoJson } from '../../utils/mockGeoJson';
import { createApi } from '../../utils/fakeApi';
import { CapApi } from '../../utils/api';
import { CapFeatures } from '../types';
import { mockCapWarningPresets } from '../../utils/mockCapData';
import { CapApiProvider, CapThemeStoreProvider } from '../Providers';

const mockStore = {
  capStore: {
    alerts: {
      ids: ['2.49.0.1.246.0.0.2022'],
      entities: {
        '2.49.0.1.246.0.0.2022': {
          id: '2.49.0.1.246.0.0.2022',
          feedAddress: 'https://alerts.fmi.fi/cap/feed/rss_en-GB.rss',
          severity: 'Extreme',
          certainty: 'Likely',
          onset: '2022-07-07T00:00:00+03:00',
          expires: '2022-07-07T23:53:10+03:00',
          languages: [
            {
              language: 'fi-FI',
              event: 'Metsäpalovaroitus',
              senderName: 'Ilmatieteen laitos',
              headline:
                'Oranssi metsäpalovaroitus: Uusimaa ja Kymenlaakso, to 0.00 - 23.53',
              description:
                'Metsäpalojen vaara on erittäin suuri keskiviikon ja torstain välisestä yöstä torstain ja perjantain väliseen yöhön 30 % todennäköisyydellä.',
            },
            {
              language: 'sv-FI',
              event: 'Varning för skogsbrand',
              senderName: 'Meteorologiska institutet',
              headline:
                'Orange varning för skogsbrand: Nyland och Kymmenedalen, to 0.00 - 23.53',
              description:
                'Risken för skogsbränder är mycket hög på från och med natten mellan onsdag och torsdag fram till natten mellan torsdag och fredag med 30 % sannolikhet.',
            },
            {
              language: 'en-GB',
              event: 'Forest fire warning',
              senderName: 'Finnish Meteorological Institute',
              headline:
                'Orange forest fire warning: Uusimaa and Kymenlaakso, Thu 0.00 - 23.53',
              description:
                'A risk of forest fires is very high on from the night between Wednesday and Thursday to the night between Thursday and Friday with probability of 30 %.',
            },
          ],
        },
      },
    },
  },
};

describe('components/WarningListCap/WarningListCap', () => {
  const createFakeApi = (): CapApi => ({
    ...createApi,
    getAllAlerts: (): Promise<{ data: CapFeatures }> =>
      Promise.resolve({
        data: mockGeoJson,
      }),
  });

  const store = createStore();
  it('should render successfully', async () => {
    render(
      <CapApiProvider createApi={createFakeApi}>
        <CapThemeStoreProvider store={store}>
          <WarningListCap
            alertIds={mockStore.capStore.alerts.ids}
            capWarningPresets={mockCapWarningPresets}
          />
        </CapThemeStoreProvider>
      </CapApiProvider>,
    );

    expect(await screen.findByTestId('warningList')).toBeTruthy();
  });

  it('should show a warning note on list that the list is not affected by time based filtering', async () => {
    render(
      <CapApiProvider createApi={createFakeApi}>
        <CapThemeStoreProvider store={store}>
          <WarningListCap
            alertIds={mockStore.capStore.alerts.ids}
            capWarningPresets={mockCapWarningPresets}
          />
        </CapThemeStoreProvider>
      </CapApiProvider>,
    );

    expect(
      await screen.findByText(
        'Note: Only map is affected by timeslider based filtering',
      ),
    ).toBeTruthy();
  });
});
