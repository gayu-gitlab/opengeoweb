/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { dateUtils } from '@opengeoweb/shared';
import { CustomFeature } from '../types';
import { Task } from './Task';

export const convertSeverityToColorClass = (
  severity: Task['severity'],
): string => {
  switch (severity) {
    case 'Extreme':
      return 'geoweb-red';
    case 'Severe':
      return 'geoweb-orange';
    case 'Moderate':
      return 'geoweb-yellow';
    case 'Minor':
    default:
      return 'geoweb-green';
  }
};

export const getEndDateFromHeadline = (headline: string): Date => {
  const splitHeadline = headline.split(',');
  const timeRange = splitHeadline[splitHeadline.length - 1];
  const splitTimeRange = timeRange.split('to');
  const end = splitTimeRange[splitTimeRange.length - 1].trim();
  const year = dateUtils.createDate().getUTCFullYear();
  const nextYear = year + 1;

  if (!end.includes(year.toString()) && !end.includes(nextYear.toString())) {
    return dateUtils.stringToDate(end, "dd MMMM HH:mm 'UTC.'")!;
  }

  return dateUtils.createDate(end);
};

export const convertFeaturesToTasks = (features: CustomFeature[]): Task[] => {
  const tasks = features.map((feature) => {
    const { languages, identifier, severity, onset } =
      feature.properties.details;
    const { areaDesc, headline, description, senderName, event } = languages[1];
    const task = {
      name: `${areaDesc}, ${event}`,
      title: headline,
      description,
      author: senderName,
      area: areaDesc,
      phenomenon: event,
      severity,
      custom_class: convertSeverityToColorClass(severity as Task['severity']),
      id: identifier,
      start: dateUtils.createDate(onset),
      end: getEndDateFromHeadline(headline),
      progress: 100,
      dependencies: '',
    };
    return task;
  });
  return tasks as unknown as Task[];
};
