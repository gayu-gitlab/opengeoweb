/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React, { createRef } from 'react';

import { Box } from '@mui/material';
import Gantt from './Gantt';
import { Task } from './Task';
import { ViewMode } from './ViewMode';

export const renderDate = (date: Date): string =>
  `${date.getUTCDate()}-${date.getUTCMonth() + 1}-${date.getUTCFullYear()}`;

export const renderTime = (date: Date): string =>
  `${String(date.getUTCHours()).padStart(2, '0')}:${String(
    date.getUTCMinutes(),
  ).padStart(2, '0')}`;

interface GanttReactWrapperProps {
  tasks: Task[];
  viewMode?: ViewMode;
  onTasksChange?: (tasks: Task[]) => void;
  onClick?: (task: Task) => void;
  onDateChange?: (task: Task, start: Date, end: Date) => void;
  onProgressChange?: (task: Task, progress: number) => void;
  onViewChange?: (mode: ViewMode) => void;
}

interface FrappeGanttState {
  viewMode: ViewMode;
  tasks: Task[];
}

const frappeGanttDefaultProps = {
  viewMode: ViewMode.Day,
  onTasksChange: (): void => {},
  onClick: (): void => {},
  onDateChange: (): void => {},
  onProgressChange: (): void => {},
  onViewChange: (): void => {},
};

export class GanttReactWrapper extends React.Component<
  GanttReactWrapperProps,
  FrappeGanttState
> {
  private _target = createRef<HTMLDivElement>();
  private _svg = createRef<SVGSVGElement>();
  private _gantt: Gantt = null!;

  static defaultProps = frappeGanttDefaultProps;

  constructor(props: GanttReactWrapperProps) {
    super(props);

    this.state = {
      viewMode: null!,
      tasks: props.tasks,
    };
  }

  static getDerivedStateFromProps(
    nextProps: GanttReactWrapperProps,
  ): FrappeGanttState {
    return {
      viewMode: nextProps.viewMode!,
      tasks: nextProps.tasks.map((t) => new Task(t)),
    };
  }

  componentDidMount(): void {
    const { tasks, viewMode } = this.state;
    const {
      onClick,
      onViewChange,
      onProgressChange,
      onTasksChange,
      onDateChange,
    } = this.props;

    this._gantt = new Gantt(this._svg.current!, tasks, {
      on_click: (task: Task): void => {
        onClick!(task);
      },
      on_view_change: onViewChange,
      on_progress_change: (task: Task, progress: number): void => {
        onProgressChange!(task, progress);
        onTasksChange!(tasks);
      },
      on_date_change: (task: Task, start: Date, end: Date): void => {
        onDateChange!(task, start, end);
        onTasksChange!(tasks);
      },
      view_modes: ['Hour', 'Quarter Day', 'Half Day', 'Day', 'Week', 'Month'],
      custom_popup_html(task): string {
        return `
        <div class="details-container">
          <h3>${task.name}</h3>
          <p>Phenomenon: ${task.phenomenon}</p>
          <p>Area: ${task.area}</p>
          <p>${(task.severity && `Severity: ${task.severity}`) || ''}</p>
          <p>Start: ${renderDate(task._start)} ${renderTime(
          task._start,
        )} UTC</p>
          <p>End: ${renderDate(task._end)} ${renderTime(task._end)} UTC</p>
        </div>
        `;
      },
    });
    if (this._gantt) {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      this._gantt.change_view_mode(viewMode);
    }
    const midOfSvg = this._svg.current!.clientWidth * 0.5;
    this._target.current!.scrollLeft = midOfSvg;
  }

  componentDidUpdate(): void {
    if (this._gantt) {
      const { tasks, viewMode } = this.state;
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      this._gantt.refresh(tasks);
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      this._gantt.change_view_mode(viewMode);
    }
  }

  render(): React.ReactElement {
    return (
      <Box
        ref={this._target}
        sx={{
          height: '100%',
          overflow: 'scroll',
          ' .geoweb-red .bar-progress': {
            fill: 'red',
          },
          ' .geoweb-orange .bar-progress': {
            fill: 'orange',
          },
          ' .geoweb-yellow .bar-progress': {
            fill: 'yellow',
          },
          ' .geoweb-green .bar-progress': {
            fill: 'green',
          },
          '.gantt .bar-label': {
            fill: 'black',
          },
          '.gantt-container': {
            height: '100%',
          },
          // tooltip styling
          '.gantt-container .popup-wrapper': {
            padding: 1,
            minWidth: 200,
            backgroundColor: 'geowebColors.background.surface',
            boxShadow: 4,
          },
          '.details-container': {
            backgroundColor: 'geowebColors.background.surface',
            color: 'geowebColors.typographyAndIcons.text',
          },
          '.gantt-container .popup-wrapper .pointer': {
            margin: '3px 0px 0 -3px',
            borderTopColor: 'geowebColors.background.surface',
          },
        }}
      >
        <svg
          ref={this._svg}
          width="100%"
          height="100%"
          xmlns="http://www.w3.org/2000/svg"
          xmlnsXlink="http://www.w3.org/1999/xlink"
        />
      </Box>
    );
  }
}
