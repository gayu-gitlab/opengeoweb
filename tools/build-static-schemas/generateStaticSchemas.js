const fs = require('fs');
const path = require('path');
const Ajv = require('ajv');
const standaloneCode = require('ajv/dist/standalone').default;
const geojsonSchema = require('./geojson.schema.json');
const sigmetSchema = require('./sigmet.schema.json');
const airmetSchema = require('./airmet.schema.json');
const firSchema = require('./firArea.schema.json');
const ajv = new Ajv({
  allErrors: true,
  schemas: [firSchema, geojsonSchema],
  code: { source: true },
});
require('ajv-merge-patch')(ajv);

// Generate sigmet schema
const sigmetValidator = ajv.compile(sigmetSchema);
const sigmetValidatorCode = standaloneCode(ajv, sigmetValidator);
fs.writeFileSync(
  path.join(
    __dirname,
    '../../libs/sigmet-airmet/src/lib/utils/config/sigmetSchemaValidator.js',
  ),
  sigmetValidatorCode,
);

// Generate airmet schema
const airmetValidator = ajv.compile(airmetSchema);
const airmetValidatorCode = standaloneCode(ajv, airmetValidator);
fs.writeFileSync(
  path.join(
    __dirname,
    '../../libs/sigmet-airmet/src/lib/utils/config/airmetSchemaValidator.js',
  ),
  airmetValidatorCode,
);
