# Create pre-compiled validators based on json schemas using Ajv

Goal is to pre-compile json schemas because using the Ajv library directly in the geoweb bundle causes issues with CSP settings. See: https://ajv.js.org/security.html#content-security-policy for details.

With Ajv, it is possible to pre-compile the schemas to static validators:

- https://ajv.js.org/standalone.html

# Generate static schemas:

In the tools/build-static-schemas folder do:

```
npm ci
node generateStaticSchemas.js

```

After that, go back to the main directory and run prettier on the new files:

```
cd ../../
nx format:write "--base=remotes/origin/master"
```

Based on:

- build-static-schemas/geojson.schema.json
- build-static-schemas/firArea.schema.json
- build-static-schemas/airmet.schema.json
- build-static-schemas/sigmet.schema.json

it will update:

- libs/sigmet-airmet/src/lib/utils/config/sigmetSchemaValidator.js
- libs/sigmet-airmet/src/lib/utils/config/airmetSchemaValidator.js

These schemas are then used in libs/sigmet-airmet/src/lib/utils/config/config.ts to validate the configuration files.
