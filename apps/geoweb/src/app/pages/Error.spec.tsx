/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { render, screen } from '@testing-library/react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Error from './Error';

describe('pages/error', () => {
  it('should render correctly', async () => {
    const { baseElement } = render(
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Error />} />
        </Routes>
      </BrowserRouter>,
    );
    expect(baseElement).toBeTruthy();
  });

  it('should show woops something wrong message if no error passed', async () => {
    const { baseElement } = render(
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<Error />} />
        </Routes>
      </BrowserRouter>,
    );

    expect(baseElement).toBeTruthy();
    expect(
      await screen.findByText(
        'Woops, something has gone wrong. Please try again.',
      ),
    ).toBeTruthy();
    expect(await screen.findByText('Login Page')).toBeTruthy();
  });

  it('should show error message', async () => {
    const error = { message: 'Error message', stack: 'error stack' };

    const { baseElement } = render(
      <BrowserRouter>
        <Routes>
          <Route
            path="/"
            element={<Error location={{ state: { error } } as History} />}
          />
        </Routes>
      </BrowserRouter>,
    );

    expect(baseElement).toBeTruthy();
    expect(
      await screen.findByText(
        'Woops, an error has occurred. Please try again.',
      ),
    ).toBeTruthy();
    expect(await screen.findByText('Error message')).toBeTruthy();
    expect(await screen.findByText('error stack')).toBeTruthy();
  });
});
