/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';

import { Route } from 'react-router-dom';

import {
  fireEvent,
  render,
  screen,
  waitFor,
  within,
} from '@testing-library/react';
import { AuthenticationProvider } from '@opengeoweb/authentication';
import {
  actions as workspaceActions,
  emptyMapWorkspace,
  workspaceListActions,
  viewPresetActions,
  workspaceRoutes,
  workspaceReducer,
} from '@opengeoweb/workspace';
import {
  routerActions,
  syncGroupsReducer,
  uiActions,
  uiTypes,
} from '@opengeoweb/store';
import {
  ConfigType,
  createMockStoreWithEggs,
  createToolkitMockStoreWithEggs,
} from '@opengeoweb/shared';
import {
  timeSeriesActions,
  emptyTimeSeriesPreset,
} from '@opengeoweb/timeseries';
import { TestWrapper } from '../../components/Providers';
import { WorkspaceLayout } from './WorkspaceLayout';

describe('geoweb/src/app/pages/workspace/WorkspaceLayout', () => {
  // mock any api requests
  jest
    .spyOn(window, 'XMLHttpRequest')
    .mockImplementation(() => null as unknown as XMLHttpRequest);

  it('should not render object manager while initial presets have not been loaded', async () => {
    const initialWorkspaceState = {
      id: '',
      title: 'Empty Map test',
      views: {
        allIds: [],
        byId: {},
      },
      mosaicNode: 'test',
      isFetching: false,
      syncGroups: [],
    };
    const mockState = {
      workspace: initialWorkspaceState,
      syncronizationGroupStore: {
        sources: {
          byId: {},
          allIds: [],
        },
        groups: {
          byId: {},
          allIds: [],
        },
        viewState: {},
      },
    };

    const newMockState = {
      reducer: {
        workspace: workspaceReducer,
        syncronizationGroupStore: syncGroupsReducer,
      },
      preloadedState: {
        syncronizationGroupStore: mockState.syncronizationGroupStore,
        workspace: mockState.workspace,
      },
    };

    const store = createToolkitMockStoreWithEggs(newMockState);
    const dispatchSpy = jest.spyOn(store, 'dispatch');

    render(
      <TestWrapper store={store}>
        <Route path="/" element={<WorkspaceLayout />} />
      </TestWrapper>,
    );

    expect(screen.queryByTestId('appLayout')).toBeFalsy();
    expect(dispatchSpy).not.toHaveBeenCalled();
    await waitFor(() => {
      expect(screen.getByTestId('appLayout')).toBeTruthy();
    });
    // render all correct modules
    expect(dispatchSpy).toHaveBeenCalledWith(
      workspaceListActions.fetchWorkspaceList({}),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      viewPresetActions.fetchInitialViewPresets(),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'syncGroups',
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'timeSeriesManager',
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'timeSeriesSelect',
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'keywordFilter',
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'layerInfo',
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'layerSelect',
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'layerManager',
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      viewPresetActions.registerViewPreset({
        panelId: 'test',
        viewPresetId: '',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      workspaceActions.changePreset({
        workspacePreset: emptyMapWorkspace,
      }),
    );

    expect(dispatchSpy).toHaveBeenCalledWith(
      routerActions.navigateToUrl({ url: workspaceRoutes.root }),
    );

    expect(dispatchSpy).not.toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'objectManager',
        setOpen: false,
        source: 'app',
      }),
    );
  });

  it('render object manager and public warnings when GW_DRAWINGS_BASE_URL is given', async () => {
    const initialWorkspaceState = {
      id: '',
      title: 'Empty Map test',
      views: {
        allIds: [],
        byId: {},
      },
      mosaicNode: 'test',
      isFetching: false,
      syncGroups: [],
    };
    const mockState = {
      workspace: initialWorkspaceState,
      syncronizationGroupStore: {
        sources: {
          byId: {},
          allIds: [],
        },
        groups: {
          byId: {},
          allIds: [],
        },
        viewState: {},
      },
    };

    const newMockState = {
      reducer: {
        workspace: workspaceReducer,
        syncronizationGroupStore: syncGroupsReducer,
      },
      preloadedState: {
        syncronizationGroupStore: mockState.syncronizationGroupStore,
        workspace: mockState.workspace,
      },
    };
    const store = createToolkitMockStoreWithEggs(newMockState);
    const dispatchSpy = jest.spyOn(store, 'dispatch');

    render(
      <TestWrapper store={store}>
        <Route
          path="/"
          element={
            <WorkspaceLayout
              config={{
                GW_DRAWINGS_BASE_URL: 'some url',
              }}
            />
          }
        />
      </TestWrapper>,
    );

    expect(screen.queryByTestId('appLayout')).toBeFalsy();
    await waitFor(() => {
      expect(screen.getByTestId('appLayout')).toBeTruthy();
    });
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: 'objectManager',
        setOpen: false,
        source: 'app',
      }),
    );
    expect(dispatchSpy).toHaveBeenCalledWith(
      uiActions.registerDialog({
        type: uiTypes.DialogTypes.PublicWarnings,
        setOpen: false,
        source: 'app',
      }),
    );
  });

  it('should render with default props and show title in tab', async () => {
    const initialWorkspaceState = {
      id: '',
      title: 'Empty Map test',
      views: {
        allIds: [],
        byId: {},
      },
      mosaicNode: 'test',
      isFetching: false,
      syncGroups: [],
    };
    const mockState = {
      workspace: initialWorkspaceState,
      syncronizationGroupStore: {
        sources: {
          byId: {},
          allIds: [],
        },
        groups: {
          byId: {},
          allIds: [],
        },
        viewState: {},
      },
    };
    const store = createMockStoreWithEggs(mockState);

    render(
      <TestWrapper store={store}>
        <Route path="/" element={<WorkspaceLayout />} />
      </TestWrapper>,
    );

    await screen.findByTestId('workspace');
    expect(screen.getByText('Empty Map test')).toBeTruthy();

    // render all correct modules
    expect(store.getActions()).toEqual([
      workspaceListActions.fetchWorkspaceList({}),
      viewPresetActions.fetchInitialViewPresets(),
      uiActions.registerDialog({
        type: 'syncGroups',
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: 'timeSeriesSelect',
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: 'timeseriesInfo',
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: 'timeSeriesManager',
        setOpen: false,
        source: 'app',
      }),
      timeSeriesActions.addServices({
        timeSeriesServices: emptyTimeSeriesPreset,
      }),
      uiActions.registerDialog({
        type: 'keywordFilter',
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: 'layerInfo',
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: 'layerSelect',
        setOpen: false,
        source: 'app',
      }),
      uiActions.registerDialog({
        type: 'layerManager',
        setOpen: false,
        source: 'app',
      }),
      viewPresetActions.registerViewPreset({
        panelId: 'test',
        viewPresetId: '',
      }),
      workspaceActions.changePreset({
        workspacePreset: emptyMapWorkspace,
      }),

      routerActions.navigateToUrl({ url: workspaceRoutes.root }),
    ]);
  });

  it('should fetch correct workspace with workspaceId ', async () => {
    const mockState = {
      workspace: {
        id: '',
        title: 'Empty Map test',
        views: {
          allIds: [],
          byId: {},
        },
        mosaicNode: '',
        isFetching: false,
        syncGroups: [],
      },
      syncronizationGroupStore: {
        sources: {
          byId: {},
          allIds: [],
        },
        groups: {
          byId: {},
          allIds: [],
        },
        viewState: {},
      },
    };
    const store = createMockStoreWithEggs(mockState);
    const props = {
      workspaceId: '12345',
    };

    render(
      <TestWrapper store={store}>
        <Route path="/" element={<WorkspaceLayout {...props} />} />
      </TestWrapper>,
    );

    await screen.findByTestId('workspace');

    await waitFor(() => {
      const expectedActionResult = store
        .getActions()
        .filter((action) => action.type === 'workspace/fetchWorkspace')[0];
      expect(expectedActionResult).toEqual(
        workspaceActions.fetchWorkspace({ workspaceId: props.workspaceId }),
      );
    });
  });

  it('should dispatch an action to open Sync Groups', async () => {
    const mockState = {
      workspace: {
        id: '',
        title: 'Empty Map test',
        views: {
          allIds: [],
          byId: {},
        },
        mosaicNode: '',
        isFetching: false,
        syncGroups: [],
      },
      syncronizationGroupStore: {
        sources: {
          byId: {},
          allIds: [],
        },
        groups: {
          byId: {},
          allIds: [],
        },
        viewState: {
          timeslider: {
            groups: [],
            sourcesById: [],
          },
          zoompane: {
            groups: [],
            sourcesById: [],
          },
          level: {
            groups: [],
            sourcesById: [],
          },
        },
      },
    };
    const store = createMockStoreWithEggs(mockState);
    render(
      <TestWrapper store={store}>
        <Route path="/" element={<WorkspaceLayout />} />
      </TestWrapper>,
    );

    await screen.findByTestId('appLayout');

    await screen.findByTestId('menuButton');
    fireEvent.click(screen.getByTestId('menuButton'));
    fireEvent.click(screen.getByText('Sync Groups'));

    const expectedAction = uiActions.setToggleOpenDialog({
      type: uiTypes.DialogTypes.SyncGroups,
      setOpen: true,
    });

    await waitFor(() => {
      expect(store.getActions()).toContainEqual(expectedAction);
    });
  });

  it('should open and close all modules', async () => {
    const testConfig: ConfigType = {
      GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
      GW_FEATURE_MODULE_SPACE_WEATHER: true,
      GW_SW_BASE_URL: 'spaceweather',
      GW_SIGMET_BASE_URL: 'sigmet',
      GW_AIRMET_BASE_URL: 'airmet',
      GW_TAF_BASE_URL: 'taf',
      GW_PRESET_BACKEND_URL: 'presets',
    };

    const mockState = {
      workspace: {
        id: '',
        title: 'Empty Map test',
        views: {
          allIds: [],
          byId: {},
        },
        mosaicNode: '',
        isFetching: false,
        syncGroups: [],
      },
      syncronizationGroupStore: {
        sources: {
          byId: {},
          allIds: [],
        },
        groups: {
          byId: {},
          allIds: [],
        },
        viewState: {},
      },
    };
    const store = createMockStoreWithEggs(mockState);
    const props = {
      workspaceId: '12345',
      config: testConfig,
    };

    render(
      <AuthenticationProvider>
        <TestWrapper store={store}>
          <Route path="/" element={<WorkspaceLayout {...props} />} />
        </TestWrapper>
      </AuthenticationProvider>,
    );

    await screen.findByTestId('appLayout');

    fireEvent.click(screen.getByTestId('menuButton'));
    fireEvent.click(screen.getByText('Space Weather Forecast'));
    await waitFor(() => {
      expect(screen.getByTestId('SpaceWeatherModule')).toBeTruthy();
    });

    // closing the modules
    fireEvent.click(
      within(screen.getByTestId('SpaceWeatherModule')).getByTestId('closeBtn'),
    );
    await waitFor(() => {
      expect(screen.queryByText('Space Weather Forecast')).toBeFalsy();
    });
  });
});
