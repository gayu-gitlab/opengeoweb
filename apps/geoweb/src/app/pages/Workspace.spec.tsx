/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import { Route, useLocation } from 'react-router-dom';

import {
  AuthenticationProvider,
  getCurrentUrlLocation,
  getSessionStorageProvider,
} from '@opengeoweb/authentication';
import { ConfigType, createMockStoreWithEggs } from '@opengeoweb/shared';
import Workspace from './Workspace';
import { TestWrapper } from '../components/Providers';
import { AppStore } from '../store';
import { createFakeApi } from '../utils/api';

describe('Workspace', () => {
  const testConfig: ConfigType = {
    GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
    GW_FEATURE_MODULE_SPACE_WEATHER: true,
    GW_SW_BASE_URL: 'spaceweather',
    GW_SIGMET_BASE_URL: 'sigmet',
    GW_AIRMET_BASE_URL: 'airmet',
    GW_TAF_BASE_URL: 'taf',
    GW_PRESET_BACKEND_URL: 'presets',
  };

  const storedWindowLocation = window.location;

  afterEach(() => {
    window.location = storedWindowLocation;
  });

  const mockState: AppStore = {
    syncronizationGroupStore: {
      sources: {
        byId: {},
        allIds: [],
      },
      groups: {
        byId: {},
        allIds: [],
      },
      viewState: {
        timeslider: {
          groups: [],
          sourcesById: [],
        },
        zoompane: {
          groups: [],
          sourcesById: [],
        },
        level: {
          groups: [],
          sourcesById: [],
        },
      },
    },
  };

  it('should render correctly', async () => {
    const store = createMockStoreWithEggs(mockState);
    const props = {
      onSetAuth: jest.fn(),
      isLoggedIn: true,
      onLogin: jest.fn(),
      auth: null,
      sessionStorageProvider: getSessionStorageProvider(),
    };

    render(
      <AuthenticationProvider value={props}>
        <TestWrapper store={store}>
          <Route
            path="/"
            element={
              <Workspace config={testConfig} createApi={createFakeApi} />
            }
          />
        </TestWrapper>
      </AuthenticationProvider>,
    );
    expect(await screen.findByTestId('appLayout')).toBeTruthy();
  });

  it('should redirect to login if authenticated and not logged in', async () => {
    const store = createMockStoreWithEggs(mockState);
    const sessionStorageProvider = getSessionStorageProvider();
    const props = {
      onSetAuth: jest.fn(),
      isLoggedIn: false,
      onLogin: jest.fn(),
      auth: null,
      sessionStorageProvider,
    };

    const testCallbackUrl = 'http://localhost/test/123';

    global.window = Object.create(window);
    Object.defineProperty(window, 'location', {
      value: {
        href: testCallbackUrl,
      },
      writable: true,
    });

    const TestLogin: React.FC = () => {
      const { state } = useLocation();
      return (
        <>
          <div>Redirected to /login</div>
          <div>Redirect url: {state.url}</div>
        </>
      );
    };

    // mock session storage
    jest.spyOn(Storage.prototype, 'getItem').mockReturnValue('true');
    render(
      <AuthenticationProvider value={props}>
        <TestWrapper store={store}>
          <Route path="/" element={<Workspace config={testConfig} />} />
          <Route path="/login" element={<TestLogin />} />
        </TestWrapper>
      </AuthenticationProvider>,
    );

    expect(await screen.findByText('Redirected to /login')).toBeTruthy();
    expect(
      await screen.findByText(
        `Redirect url: ${getCurrentUrlLocation(
          testCallbackUrl,
          testConfig.GW_APP_URL!,
        )}`,
      ),
    ).toBeTruthy();

    expect(screen.queryByTestId('workspace')).toBeFalsy();
  });

  it('should redirect to login if not authenticated and has GW_FEATURE_FORCE_AUTHENTICATION set', async () => {
    const store = createMockStoreWithEggs(mockState);
    const sessionStorageProvider = getSessionStorageProvider();
    const testConfigWithForceAuth: ConfigType = {
      GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
      GW_SW_BASE_URL: 'spaceweather',
      GW_SIGMET_BASE_URL: 'sigmet',
      GW_AIRMET_BASE_URL: 'airmet',
      GW_TAF_BASE_URL: 'taf',
      GW_PRESET_BACKEND_URL: 'presets',
      GW_FEATURE_FORCE_AUTHENTICATION: true,
    };

    const props = {
      onSetAuth: jest.fn(),
      isLoggedIn: false,
      onLogin: jest.fn(),
      auth: {
        username: 'Michael Jackson',
        token: '1223344',
        refresh_token: '33455214',
      },
      sessionStorageProvider,
    };
    const testCallbackUrl = 'http://localhost/test/123';

    global.window = Object.create(window);
    Object.defineProperty(window, 'location', {
      value: {
        href: testCallbackUrl,
      },
      writable: true,
    });

    const TestLogin: React.FC = () => {
      const { state } = useLocation();
      return (
        <>
          <div>Redirected to /login</div>
          <div>Redirect url: {state.url}</div>
        </>
      );
    };

    // mock session storage
    jest.spyOn(Storage.prototype, 'getItem').mockReturnValue('false');
    render(
      <AuthenticationProvider value={props}>
        <TestWrapper store={store}>
          <Route
            path="/"
            element={<Workspace config={testConfigWithForceAuth} />}
          />
          <Route path="/login" element={<TestLogin />} />
        </TestWrapper>
      </AuthenticationProvider>,
    );

    expect(await screen.findByText('Redirected to /login')).toBeTruthy();
    expect(
      await screen.findByText(
        `Redirect url: ${getCurrentUrlLocation(
          testCallbackUrl,
          testConfigWithForceAuth.GW_APP_URL!,
        )}`,
      ),
    ).toBeTruthy();

    expect(screen.queryByTestId('workspace')).toBeFalsy();
  });

  it('should not redirect to login if logged in', async () => {
    const store = createMockStoreWithEggs(mockState);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    delete window.location;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.location = new URL('http://localhost/');

    const props = {
      onSetAuth: jest.fn(),
      isLoggedIn: true,
      onLogin: jest.fn(),
      auth: null,
      sessionStorageProvider: getSessionStorageProvider(),
    };

    // mock session storage
    jest.spyOn(Storage.prototype, 'getItem').mockReturnValue('true');

    render(
      <AuthenticationProvider value={props}>
        <TestWrapper store={store}>
          <Route
            path="/"
            element={
              <Workspace config={testConfig} createApi={createFakeApi} />
            }
          />
          <Route path="/login" element={<div>Redirected to /login</div>} />
        </TestWrapper>
      </AuthenticationProvider>,
    );

    await waitFor(() => {
      expect(screen.queryByText('Redirected to /login')).toBeFalsy();
    });

    expect(await screen.findByTestId('workspace')).toBeTruthy();
  });
});
