/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import {
  fireEvent,
  render,
  screen,
  waitFor,
  within,
} from '@testing-library/react';
import React from 'react';
import configureStore from 'redux-mock-store';

import {
  AuthenticationProvider,
  getSessionStorageProvider,
} from '@opengeoweb/authentication';
import { uiActions, uiTypes } from '@opengeoweb/store';
import {
  WorkspacePreset,
  emptyMapWorkspace,
  actions as workspaceActions,
  workspaceRoutes,
} from '@opengeoweb/workspace';
import { Route } from 'react-router-dom';

import * as utils from '@opengeoweb/api';
import { createFakeApiInstance } from '@opengeoweb/api';
import { ConfigType, createMockStoreWithEggs } from '@opengeoweb/shared';
import { TestWrapper } from '../components/Providers';
import { AppStore } from '../store';
import { createFakeApi } from '../utils/api';
import Home from './Home';

describe('Home', () => {
  const testConfig: ConfigType = {
    GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
    GW_SW_BASE_URL: 'spaceweather',
    GW_SIGMET_BASE_URL: 'sigmet',
    GW_AIRMET_BASE_URL: 'airmet',
    GW_TAF_BASE_URL: 'taf',
    GW_PRESET_BACKEND_URL: 'presets',
    GW_FEATURE_MODULE_SIGMET_CONFIGURATION: '/sigmetConfiguration',
    GW_FEATURE_MODULE_AIRMET_CONFIGURATION: '/airmetConfiguration',
  };

  const storedWindowLocation = window.location;

  afterEach(() => {
    window.location = storedWindowLocation;
  });

  const mockState: AppStore = {
    syncronizationGroupStore: {
      sources: {
        byId: {},
        allIds: [],
      },
      groups: {
        byId: {},
        allIds: [],
      },
      viewState: {
        timeslider: {
          groups: [],
          sourcesById: [],
        },
        zoompane: {
          groups: [],
          sourcesById: [],
        },
        level: {
          groups: [],
          sourcesById: [],
        },
      },
    },
  };

  it('should not render homepage when no config is loading', async () => {
    const props = {
      config: null!,
      auth: null,
      onSetAuth: jest.fn(),
    };
    const mockStore = configureStore();
    const store = mockStore(mockState);
    render(
      <TestWrapper store={store}>
        <Route
          path={workspaceRoutes.root}
          element={<Home {...props} createApi={createFakeApi} />}
        />
      </TestWrapper>,
    );
    await waitFor(() => expect(screen.queryByTestId('appLayout')).toBeFalsy());
  });

  it('should render successfully with default empty preset', async () => {
    const store = createMockStoreWithEggs(mockState);

    render(
      <TestWrapper store={store}>
        <Route
          path={workspaceRoutes.root}
          element={<Home config={testConfig} createApi={createFakeApi} />}
        />
      </TestWrapper>,
    );
    await waitFor(() => {
      expect(screen.getByTestId('WorkspacePage')).toBeTruthy();
    });

    const expectedAction = workspaceActions.changePreset({
      workspacePreset: emptyMapWorkspace as WorkspacePreset,
    });

    await waitFor(() => {
      expect(store.getActions()).toContainEqual(expectedAction);
    });
  });

  it('should dispatch an action to open Sync Groups', async () => {
    const store = createMockStoreWithEggs(mockState);
    render(
      <TestWrapper store={store}>
        <Route
          path={workspaceRoutes.root}
          element={<Home config={testConfig} createApi={createFakeApi} />}
        />
      </TestWrapper>,
    );

    await screen.findByTestId('menuButton');
    fireEvent.click(screen.getByTestId('menuButton'));
    fireEvent.click(screen.getByText('Sync Groups'));

    const expectedAction = uiActions.setToggleOpenDialog({
      type: uiTypes.DialogTypes.SyncGroups,
      setOpen: true,
    });

    await waitFor(() => {
      expect(store.getActions()).toContainEqual(expectedAction);
    });
  });

  it('should open and close all modules', async () => {
    const fakeAxiosInstance = createFakeApiInstance();
    jest
      .spyOn(utils, 'createNonAuthApiInstance')
      .mockReturnValue(fakeAxiosInstance);
    const store = createMockStoreWithEggs(mockState);

    render(
      <AuthenticationProvider>
        <TestWrapper store={store}>
          <Route
            path={workspaceRoutes.root}
            element={
              <Home
                config={{
                  GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
                  GW_SW_BASE_URL: 'spaceweather',
                  GW_SIGMET_BASE_URL: 'sigmet',
                  GW_AIRMET_BASE_URL: 'airmet',
                  GW_FEATURE_MODULE_AIRMET_CONFIGURATION:
                    'knmi.airmetConfig.json',
                  GW_FEATURE_MODULE_SIGMET_CONFIGURATION:
                    'knmi.sigmetConfig.json',
                  GW_FEATURE_MODULE_SPACE_WEATHER: true,
                }}
                createApi={createFakeApi}
              />
            }
          />
        </TestWrapper>
      </AuthenticationProvider>,
    );

    await screen.findByTestId('menuButton');
    fireEvent.click(screen.getByTestId('menuButton'));
    fireEvent.click(screen.getByText('Space Weather Forecast'));
    await waitFor(() => {
      expect(screen.getByTestId('SpaceWeatherModule')).toBeTruthy();
    });

    // closing the modules
    fireEvent.click(
      within(screen.getByTestId('SpaceWeatherModule')).getByTestId('closeBtn'),
    );
    await waitFor(() => {
      expect(screen.queryByText('Space Weather Forecast')).toBeFalsy();
    });
  });

  it('should show the viewpreset button', async () => {
    const newMockState: AppStore = {
      ...mockState,
      workspace: {
        id: 'screenConfigA',
        title: 'Radar',
        views: {
          byId: {
            screenA: {
              title: 'Precipitation Radar',
              componentType: 'Map',
              initialProps: {
                mapPreset: {
                  layers: [
                    {
                      service:
                        'https://adaguc-server-geoweb.geoweb.knmi.cloud/adagucserver?dataset=RADAR',
                      name: 'precipitation',
                      format: 'image/png',
                      enabled: true,
                      style: 'radar/nearest',
                      id: 'test_layerid_active',
                      layerType: 'mapLayer',
                    },
                  ],
                  activeLayerId: 'test_layerid_active',
                },
                syncGroupsIds: ['Area_A', 'Time_A'],
              },
            },
          },
          allIds: ['screenA'],
        },
        mosaicNode: 'screenA',
      },
      ui: {
        order: [uiTypes.DialogTypes.LayerManager],
        dialogs: {
          layerManager: {
            activeMapId: 'screenA',
            isOpen: true,
            type: uiTypes.DialogTypes.LayerManager,
            source: 'app',
          },
        },
      },
      viewPresets: {
        entities: {},
        ids: [],
      },
    };
    const store = createMockStoreWithEggs(newMockState);

    render(
      <TestWrapper store={store}>
        <Route
          path={workspaceRoutes.root}
          element={<Home config={testConfig} createApi={createFakeApi} />}
        />
      </TestWrapper>,
    );
    expect(await screen.findByTestId('layerManagerButton')).toBeTruthy();
    expect(await screen.findAllByTestId('layerManagerWindow')).toHaveLength(1);
    expect(
      await screen.findAllByTestId('viewpreset-options-toolbutton'),
    ).toBeTruthy();
  });

  it('should redirect to login if authenticated and not logged in', async () => {
    const store = createMockStoreWithEggs(mockState);
    const props = {
      onSetAuth: jest.fn(),
      isLoggedIn: false,
      onLogin: jest.fn(),
      auth: null,
      sessionStorageProvider: getSessionStorageProvider(),
    };

    // mock session storage
    jest.spyOn(Storage.prototype, 'getItem').mockReturnValue('true');

    render(
      <AuthenticationProvider value={props}>
        <TestWrapper store={store}>
          <Route
            path={workspaceRoutes.root}
            element={<Home config={testConfig} createApi={createFakeApi} />}
          />
          <Route path="/login" element={<div>Redirected to /login</div>} />
        </TestWrapper>
      </AuthenticationProvider>,
    );
    expect(await screen.findByText('Redirected to /login')).toBeTruthy();

    expect(screen.queryByText('GeoWeb')).toBeFalsy();
  });

  it('should not redirect to login if logged in', async () => {
    const store = createMockStoreWithEggs(mockState);
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    delete window.location;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    window.location = new URL('http://localhost/');

    const props = {
      onSetAuth: jest.fn(),
      isLoggedIn: true,
      onLogin: jest.fn(),
      auth: null,
      sessionStorageProvider: getSessionStorageProvider(),
    };

    // mock session storage
    jest.spyOn(Storage.prototype, 'getItem').mockReturnValue('true');

    render(
      <AuthenticationProvider value={props}>
        <TestWrapper store={store}>
          <Route
            path={workspaceRoutes.root}
            element={<Home config={testConfig} createApi={createFakeApi} />}
          />
          <Route path="/login" element={<div>Redirected to /login</div>} />
        </TestWrapper>
      </AuthenticationProvider>,
    );
    await waitFor(() => {
      expect(screen.queryByText('Redirected to /login')).toBeFalsy();
    });

    expect(await screen.findByText('GeoWeb')).toBeTruthy();
  });
});
