/* eslint-disable @typescript-eslint/ban-ts-comment */
/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import { CapModule, capModuleConfig } from '@opengeoweb/cap';
import { layerSelectConfig, layerSelectTypes } from '@opengeoweb/layer-select';
import {
  coreModuleConfig,
  CoreAppStore,
  serviceTypes,
} from '@opengeoweb/store';
import {
  timeSeriesModuleConfig,
  TimeSeriesModuleState,
} from '@opengeoweb/timeseries';
import { drawingModuleConfig } from '@opengeoweb/warnings';
import {
  WorkspaceModuleStore,
  workspaceModuleConfig,
} from '@opengeoweb/workspace';
import { createStore } from '@redux-eggs/redux-toolkit';
import { getSagaExtension } from '@redux-eggs/saga-extension';
import { DevToolsEnhancerOptions } from '@reduxjs/toolkit';
import { produce } from 'immer';

const appStoreModules = [
  ...coreModuleConfig,
  layerSelectConfig,
  workspaceModuleConfig,
  timeSeriesModuleConfig,
  capModuleConfig,
  drawingModuleConfig,
];

export interface AppStore
  extends WorkspaceModuleStore,
    CoreAppStore,
    layerSelectTypes.LayerSelectModuleState,
    TimeSeriesModuleState,
    CapModule {}

const devToolsStateSanitizer: DevToolsEnhancerOptions = {
  stateSanitizer: (state) => {
    return produce(state, (draftState) => {
      // @ts-ignore
      if (state['services']) {
        // @ts-ignore
        Object.values(state['services'].byId).forEach(
          (service: serviceTypes.SetLayersForServicePayload) => {
            // @ts-ignore
            draftState['services'].byId[service.id].layers =
              service.layers.length;
          },
        );
      }
    });
  },
};
const store = createStore({
  extensions: [getSagaExtension({})],
  defaultMiddlewareOptions: {
    serializableCheck: { warnAfter: 128 },
    immutableCheck: { warnAfter: 128 },
  },
  devTools:
    process.env.NODE_ENV === 'development' ? devToolsStateSanitizer : false,
});

export { store, appStoreModules };
