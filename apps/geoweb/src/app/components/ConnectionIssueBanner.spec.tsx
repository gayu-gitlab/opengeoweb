/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import { fireEvent, render, waitFor, screen } from '@testing-library/react';
import * as apiModule from '@opengeoweb/api';
import { AuthenticationConfig } from '@opengeoweb/authentication';
import { Credentials } from '@opengeoweb/api';
import { ConnectionIssueBanner } from './ConnectionIssueBanner';

describe('components/ConnectionIssueBanner', () => {
  const auth: Credentials = {
    expires_at: 1640011500,
    keep_session_alive_at: 1640008860,
    refresh_token: '',
    token: 'test',
    username: 'max.verstappen',
    has_connection_issue: true,
  };

  const authConfig: AuthenticationConfig = {
    GW_APP_URL: '',
    GW_AUTH_TOKEN_URL: '',
    GW_AUTH_CLIENT_ID: '',
    GW_AUTH_LOGIN_URL: '',
    GW_AUTH_LOGOUT_URL: '',
  };

  it('should not do anything if there is no connection error', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    const { container } = render(
      <Provider store={store}>
        <ConnectionIssueBanner />
      </Provider>,
    );
    const { childElementCount } = container;
    await waitFor(() => {
      expect(childElementCount).toEqual(0);
    });
  });

  it('should show the alertbanner and not go away when there are connection issues after clicking try again', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    const onSetAuth = jest.fn();
    render(
      <Provider store={store}>
        <ConnectionIssueBanner
          auth={auth}
          onSetAuth={onSetAuth}
          authConfig={authConfig}
        />
      </Provider>,
    );
    expect(await screen.findByTestId('alert-banner')).toBeTruthy();
    expect(screen.getByRole('button')).toBeTruthy();

    const mock = jest.spyOn(apiModule, 'refreshAccessTokenAndSetAuthContext');

    mock.mockImplementationOnce(({ onSetAuth }) => {
      onSetAuth!({
        ...auth,
        has_connection_issue: true,
      });
      return Promise.resolve();
    });

    /* Click button try again */
    fireEvent.click(screen.queryByText('Try again'.toUpperCase())!);
    expect(onSetAuth).toHaveBeenCalledTimes(2);
    expect(onSetAuth).toHaveBeenCalledWith({
      expires_at: 1640011500,
      has_connection_issue: false,
      keep_session_alive_at: 1640008860,
      refresh_token: '',
      token: 'test',
      username: 'max.verstappen',
    });
    expect(onSetAuth).toHaveBeenCalledWith({
      expires_at: 1640011500,
      has_connection_issue: true,
      keep_session_alive_at: 1640008860,
      refresh_token: '',
      token: 'test',
      username: 'max.verstappen',
    });
  });

  it('should call snackbar action after clicking try again when there are no connection issues anymore', async () => {
    const mockStore = configureStore();
    const store = mockStore();
    const onSetAuth = jest.fn();
    render(
      <Provider store={store}>
        <ConnectionIssueBanner
          auth={auth}
          onSetAuth={onSetAuth}
          authConfig={authConfig}
        />
      </Provider>,
    );
    expect(await screen.findByTestId('alert-banner')).toBeTruthy();
    expect(screen.getByRole('button')).toBeTruthy();

    const mock = jest.spyOn(apiModule, 'refreshAccessTokenAndSetAuthContext');

    mock.mockImplementationOnce(({ onSetAuth }) => {
      onSetAuth!({
        ...auth,
        has_connection_issue: false,
      });
      return Promise.resolve();
    });

    /* Click button try again */
    fireEvent.click(screen.queryByText('Try again'.toUpperCase())!);
    expect(onSetAuth).toHaveBeenCalledTimes(2);
    expect(onSetAuth).toHaveBeenCalledWith({
      expires_at: 1640011500,
      has_connection_issue: false,
      keep_session_alive_at: 1640008860,
      refresh_token: '',
      token: 'test',
      username: 'max.verstappen',
    });
    expect(onSetAuth).toHaveBeenCalledWith({
      expires_at: 1640011500,
      has_connection_issue: false,
      keep_session_alive_at: 1640008860,
      refresh_token: '',
      token: 'test',
      username: 'max.verstappen',
    });

    expect(store.getActions()).toEqual([
      {
        payload: { message: 'Your connection is restored' },
        type: 'snackbar/openSnackbar',
      },
    ]);
  });
});
