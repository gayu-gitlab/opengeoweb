/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import * as React from 'react';
import { Link, Typography } from '@mui/material';
import { CustomDialog } from '@opengeoweb/shared';
import image from '../../assets/firefox_container_queries.png';

interface MessageProps {
  isFirefox?: boolean;
}

const Message: React.FC<MessageProps> = ({
  isFirefox = false,
}: MessageProps) => (
  <>
    <Typography>
      This browser is not compatible with GeoWeb functionalities.
    </Typography>
    <br />
    {isFirefox ? (
      <Typography data-testid="firefoxMessage">
        For optimal experience, you can enable container queries by following
        these steps:
        <br />
        <br />
        1. Open a new tab in this browser and in the address bar type
        &quot;about:config&quot;
        <br />
        2. Press the confirm button when asked if you are sure to continue
        <br />
        3. Search for &quot;container-queries&quot;
        <br />
        4. Set it to true by clicking the switch button on the right
        <br />
        <img
          alt="Firefox container queries configuration"
          src={image}
          width="528"
        />
        <br />
        5. Now refresh your GeoWeb application
      </Typography>
    ) : (
      <Typography>
        For optimal experience, please upgrade to the latest version of{' '}
        <Link href="https://www.google.com/chrome/" target="_blank">
          Chrome
        </Link>
        ,{' '}
        <Link href="https://support.apple.com/downloads/safari" target="_blank">
          Safari
        </Link>{' '}
        or{' '}
        <Link href="https://www.microsoft.com/edge" target="_blank">
          Microsoft Edge
        </Link>
        .
      </Typography>
    )}
  </>
);

const BrowserIssueDialog: React.FC = () => {
  const hasContainerQueriesSupport =
    'container' in document.documentElement.style;
  const isFirefox =
    window.navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
  const [open, setOpen] = React.useState<boolean>(!hasContainerQueriesSupport);

  if (hasContainerQueriesSupport) {
    return null;
  }

  return (
    <CustomDialog
      title="Browser issue"
      open={open}
      onClose={(): void => {
        setOpen(false);
      }}
    >
      <Message isFirefox={isFirefox} />
    </CustomDialog>
  );
};

export default BrowserIssueDialog;
