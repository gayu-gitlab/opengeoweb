/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import React from 'react';
import { fireEvent, screen, render, waitFor } from '@testing-library/react';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { ConfigType } from '@opengeoweb/shared';
import UserMenu from './UserMenu';
import { TestWrapper } from '../utils/mockdata';
import * as api from '../utils/api';
import { createFakeApi } from '../utils/api';

const mockUseNavigate = jest.fn();

jest.mock('react-router-dom', () => ({
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  ...(jest.requireActual('react-router-dom') as any),
  useNavigate: (): jest.Mock => mockUseNavigate,
}));

describe('/components/UserMenu', () => {
  const testConfig: ConfigType = {
    GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
    GW_FEATURE_APP_TITLE: 'TestTitle',
    GW_FEATURE_FORCE_AUTHENTICATION: false,
    GW_FEATURE_MODULE_SPACE_WEATHER: false,
    GW_FEATURE_MENU_FEEDBACK: true,
    GW_FEATURE_MENU_USER_DOCUMENTATION_URL: 'https://testurl.com',
    GW_FEATURE_MENU_INFO: true,
    GW_FEATURE_MENU_VERSION: true,
  };

  it('should show the BE version info', async () => {
    const props = {
      userName: 'daan.storm',
      userRole: 'Meteorologist',
      config: testConfig,
    };
    render(
      <TestWrapper>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<UserMenu {...props} />} />
          </Routes>
        </BrowserRouter>
      </TestWrapper>,
    );

    fireEvent.click(screen.getByTestId('userButton'));
    await waitFor(() => {
      expect(screen.getByTestId('be-version-menu').textContent).toContain(
        'version 1.0.1',
      );
    });
  });

  it('should show the user menu', async () => {
    const props = {
      userName: 'daan.storm',
      userRole: 'Meteorologist',
      config: testConfig,
    };
    render(
      <TestWrapper>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<UserMenu {...props} />} />
          </Routes>
        </BrowserRouter>
      </TestWrapper>,
    );

    fireEvent.click(screen.getByTestId('userButton'));
    // Wait for the version to have loaded
    await waitFor(() => {
      expect(screen.getByTestId('be-version-menu').textContent).toContain(
        'version 1.0.1',
      );
    });

    expect(screen.getByTestId('userMenu')).toBeTruthy();
    expect(screen.getByText(props.userName)).toBeTruthy();
    expect(screen.getByText(props.userRole)).toBeTruthy();
  });

  it('should show the *user* documentation link item in menu', async () => {
    const props = {
      userName: 'daan.storm',
      userRole: 'Meteorologist',
      config: testConfig,
    };
    render(
      <TestWrapper>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<UserMenu {...props} />} />
          </Routes>
        </BrowserRouter>
      </TestWrapper>,
    );

    fireEvent.click(screen.getByTestId('userButton'));
    await waitFor(() => {
      expect(screen.getByText('User documentation')).toBeTruthy();
    });
  });

  it('should show the *project* documentation dialog in menu', async () => {
    const props = {
      userName: 'daan.storm',
      userRole: 'Meteorologist',
      config: testConfig,
    };
    render(
      <TestWrapper>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<UserMenu {...props} />} />
          </Routes>
        </BrowserRouter>
      </TestWrapper>,
    );

    fireEvent.click(screen.getByTestId('userButton'));
    await waitFor(() => {
      expect(screen.getByText('Project documentation')).toBeTruthy();
    });
  });

  it('should toggle the menu', async () => {
    const props = {
      userName: 'daan.storm',
      userRole: 'Meteorologist',
      config: testConfig,
    };
    render(
      <TestWrapper>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<UserMenu {...props} />} />
          </Routes>
        </BrowserRouter>
      </TestWrapper>,
    );

    expect(screen.queryByTestId('userMenu')).toBeFalsy();
    fireEvent.click(screen.getByTestId('userButton'));
    // Wait for the version to have loaded
    await waitFor(() => {
      expect(screen.getByTestId('be-version-menu').textContent).toContain(
        'version 1.0.1',
      );
    });

    expect(screen.getByTestId('userMenu')).toBeTruthy();
    fireEvent.click(screen.getByTestId('userButton'));
    expect(screen.queryByTestId('userMenu')).toBeFalsy();
  });

  it('should show the loading message when BE version is still loading', async () => {
    jest.spyOn(api, 'createFakeApi').mockReturnValueOnce({
      ...createFakeApi(),
      getBeVersion: (): Promise<{ data: { version: string } }> =>
        Promise.resolve({ data: null! }),
    });
    const props = {
      userName: 'daan.storm',
      userRole: 'Meteorologist',
      config: testConfig,
    };
    render(
      <TestWrapper>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<UserMenu {...props} />} />
          </Routes>
        </BrowserRouter>
      </TestWrapper>,
    );

    fireEvent.click(screen.getByTestId('userButton'));
    await waitFor(() => {
      expect(screen.getByTestId('be-version-menu').textContent).toContain(
        'Loading...',
      );
    });
  });

  it('should change theming', async () => {
    const props = {
      userName: 'daan.storm',
      userRole: 'Meteorologist',
      config: testConfig,
    };
    render(
      <TestWrapper>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<UserMenu {...props} />} />
          </Routes>
        </BrowserRouter>
      </TestWrapper>,
    );

    fireEvent.click(screen.getByTestId('userButton'));

    const lightButton: HTMLInputElement = screen.getByRole('radio', {
      name: /Light/i,
    });
    const darkButton: HTMLInputElement = screen.getByRole('radio', {
      name: /Dark/i,
    });

    expect(lightButton.checked).toBe(true);
    expect(darkButton.checked).toBe(false);

    expect(
      window.getComputedStyle(screen.getAllByRole('menuitem')[0])[
        'border-bottom' as keyof CSSStyleDeclaration
      ],
    ).toEqual('1px solid rgba(0, 0, 0, 0.12)');

    // toggle to dark
    fireEvent.click(darkButton);
    await waitFor(() => {
      expect(lightButton.checked).toBe(false);
    });
    expect(darkButton.checked).toBe(true);

    expect(
      window.getComputedStyle(screen.getAllByRole('menuitem')[0])[
        'border-bottom' as keyof CSSStyleDeclaration
      ],
    ).toEqual('1px solid rgba(255, 255, 255, 0.12)');
  });

  it('should show the error message when BE version failed to load', async () => {
    jest.spyOn(api, 'createFakeApi').mockReturnValueOnce({
      ...createFakeApi(),
      getBeVersion: (): Promise<{ data: { version: string } }> =>
        Promise.reject(new Error('version error')),
    });

    const props = {
      userName: 'daan.storm',
      userRole: 'Meteorologist',
      config: testConfig,
    };
    render(
      <TestWrapper>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<UserMenu {...props} />} />
          </Routes>
        </BrowserRouter>
      </TestWrapper>,
    );

    fireEvent.click(screen.getByTestId('userButton'));
    await waitFor(() => {
      expect(screen.getByTestId('be-version-menu').textContent).toContain(
        'Loading failed: version error',
      );
    });
  });

  it('should load version and open the feedback page in a new tab', async () => {
    const storedWindowOpen = window.open;
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    delete window.open;
    window.open = jest.fn();

    const props = {
      userName: 'daan.storm',
      userRole: 'Meteorologist',
      config: testConfig,
    };
    render(
      <TestWrapper>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<UserMenu {...props} />} />
          </Routes>
        </BrowserRouter>
      </TestWrapper>,
    );

    fireEvent.click(screen.getByTestId('userButton'));
    // Wait for the version to have loaded
    await waitFor(() => {
      expect(screen.getByTestId('be-version-menu').textContent).toContain(
        'version 1.0.1',
      );
    });

    fireEvent.click(screen.getByTestId('open-feedback'));
    await waitFor(() =>
      expect(window.open).toHaveBeenCalledWith(
        'http://confluence.knmi.nl/display/GW/Feedback',
        '_blank',
      ),
    );
    window.open = storedWindowOpen;
  });

  it('should open and close the release notes', async () => {
    const props = {
      userName: 'daan.storm',
      userRole: 'Meteorologist',
      config: testConfig,
    };
    render(
      <TestWrapper>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<UserMenu {...props} />} />
          </Routes>
        </BrowserRouter>
      </TestWrapper>,
    );

    expect(screen.queryByTestId('userMenu')).toBeFalsy();
    fireEvent.click(screen.getByTestId('userButton'));
    // Wait for the version to have loaded
    await waitFor(() => {
      expect(screen.getByTestId('be-version-menu').textContent).toContain(
        'version 1.0.1',
      );
    });

    expect(screen.getByTestId('userMenu')).toBeTruthy();
    fireEvent.click(screen.getByTestId('be-version-menu'));
    expect(screen.queryByTestId('userMenu')).toBeFalsy();
    expect(screen.getByTestId('releaseNotes')).toBeTruthy();
    fireEvent.click(screen.getByTestId('closeReleaseNotes'));
    expect(screen.queryByTestId('releaseNotes')).toBeFalsy();
    expect(screen.queryByTestId('userMenu')).toBeFalsy();
  });

  it('should show logout option when authentication configured and user is logged in', async () => {
    const props = {
      userName: 'daan.storm@gmail.com',
      userRole: 'Meteorologist',
      isAuthConfigured: true,
    };
    render(
      <TestWrapper>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<UserMenu {...props} />} />
          </Routes>
        </BrowserRouter>
      </TestWrapper>,
    );

    expect(screen.queryByTestId('userMenu')).toBeFalsy();
    fireEvent.click(screen.getByTestId('userButton'));
    await waitFor(() => {
      expect(screen.getByTestId('userMenu')).toBeTruthy();
    });
    expect(screen.getByTestId('logoutButton')).toBeTruthy();
  });

  it('should show login option when authentication is configured and user is not logged in', async () => {
    const props = {
      userName: 'Guest',
      isAuthConfigured: true,
    };
    render(
      <TestWrapper>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<UserMenu {...props} />} />
          </Routes>
        </BrowserRouter>
      </TestWrapper>,
    );

    fireEvent.click(screen.getByTestId('userButton'));
    expect(await screen.findByTestId('userMenu')).toBeTruthy();
    expect(await screen.findByTestId('loginButton')).toBeTruthy();
  });

  it('should not show logout/login option when no authentication configured', async () => {
    const props = {
      userName: 'daan.storm@gmail.com',
      userRole: 'Meteorologist',
    };
    render(
      <TestWrapper>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<UserMenu {...props} />} />
          </Routes>
        </BrowserRouter>
      </TestWrapper>,
    );

    expect(screen.queryByTestId('userMenu')).toBeFalsy();
    fireEvent.click(screen.getByTestId('userButton'));
    await waitFor(() => {
      expect(screen.getByTestId('userMenu')).toBeTruthy();
    });
    expect(screen.queryByTestId('logoutButton')).toBeFalsy();
    expect(screen.queryByTestId('loginButton')).toBeFalsy();
  });

  it('should handle logout', async () => {
    const props = {
      userName: 'daan.storm@gmail.com',
      userRole: 'Meteorologist',
      isAuthConfigured: true,
    };
    render(
      <TestWrapper>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<UserMenu {...props} />} />
          </Routes>
        </BrowserRouter>
      </TestWrapper>,
    );

    expect(screen.queryByTestId('userMenu')).toBeFalsy();
    fireEvent.click(screen.getByTestId('userButton'));
    await waitFor(() => {
      expect(screen.getByTestId('userMenu')).toBeTruthy();
    });
    expect(screen.getByTestId('logoutButton')).toBeTruthy();
    fireEvent.click(screen.getByTestId('logoutButton'));

    expect(mockUseNavigate).toHaveBeenLastCalledWith('/logout');
  });

  it('should handle login', async () => {
    const props = {
      userName: 'Guest',
      isAuthConfigured: true,
    };

    render(
      <TestWrapper>
        <BrowserRouter>
          <Routes>
            <Route path="/" element={<UserMenu {...props} />} />
          </Routes>
        </BrowserRouter>
      </TestWrapper>,
    );

    expect(screen.queryByTestId('userMenu')).toBeFalsy();
    fireEvent.click(screen.getByTestId('userButton'));
    await waitFor(() => {
      expect(screen.getByTestId('userMenu')).toBeTruthy();
    });
    expect(screen.getByTestId('loginButton')).toBeTruthy();
    fireEvent.click(screen.getByTestId('loginButton'));

    expect(mockUseNavigate).toHaveBeenLastCalledWith('/login');
  });
});
