/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { Provider } from 'react-redux';
import { AnyAction, Store } from '@reduxjs/toolkit';
import { MemoryRouter, Routes } from 'react-router-dom';
import { ApiProvider } from '@opengeoweb/api';
import {
  ThemeProviderProps,
  ThemeTypes,
  ThemeWrapper,
} from '@opengeoweb/theme';
import { AppWrapperConnect } from '@opengeoweb/core';
import { SnackbarWrapperConnect } from '@opengeoweb/snackbar';
import { withEggs, ConfirmationServiceProvider } from '@opengeoweb/shared';
import { createFakeApi } from '../../utils/api';
import { appStoreModules } from '../../store';

const ThemeWrapperWithModules: React.FC<ThemeProviderProps> = withEggs(
  appStoreModules,
)(({ theme, children, defaultThemeName }: ThemeProviderProps) => (
  <ThemeWrapper theme={theme} defaultThemeName={defaultThemeName}>
    {children}
  </ThemeWrapper>
));

export const AppWrapper: React.FC<{
  children: React.ReactNode;
  store: Store<unknown, AnyAction>;
  defaultThemeName?: ThemeTypes;
}> = ({ children, store, defaultThemeName }) => (
  <Provider store={store}>
    <ThemeWrapperWithModules defaultThemeName={defaultThemeName}>
      <AppWrapperConnect>
        <SnackbarWrapperConnect>
          <ConfirmationServiceProvider>
            {children as React.ReactElement}
          </ConfirmationServiceProvider>
        </SnackbarWrapperConnect>
      </AppWrapperConnect>
    </ThemeWrapperWithModules>
  </Provider>
);

interface TestWrapperProps {
  store: Store<unknown, AnyAction>;
  children: React.ReactNode;
}

export const TestWrapper: React.FC<TestWrapperProps> = ({
  store,
  children,
}: TestWrapperProps) => {
  return (
    <ApiProvider createApi={createFakeApi}>
      <ThemeWrapper>
        <Provider store={store}>
          <MemoryRouter>
            <Routes>{children}</Routes>
          </MemoryRouter>
        </Provider>
      </ThemeWrapper>
    </ApiProvider>
  );
};
