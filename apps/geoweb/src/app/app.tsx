/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import React from 'react';
import { BrowserRouter, Routes as ReactRoutes, Route } from 'react-router-dom';
import { RouterWrapperConnect } from '@opengeoweb/core';

import './app.css';

import { AlertBanner, ConfigType } from '@opengeoweb/shared';
import {
  AuthenticationProvider,
  getAuthConfig,
  AuthenticationConfig,
  Login,
  Logout,
  Code,
  getSessionStorageProvider,
  getRandomString,
  getCodeChallenge,
  RequireAuth,
} from '@opengeoweb/authentication';
import { workspaceRoutes } from '@opengeoweb/workspace';

import { ThemeWrapper } from '@opengeoweb/theme';

import { isArray } from 'lodash';
import { CreateApiProps } from '@opengeoweb/api';
import { useTranslation } from 'react-i18next';
import ErrorPage from './pages/Error';
import NotFound from './pages/404';
import Workspace from './pages/Workspace';
import {
  useConfig,
  isValidConfig,
  isValidConfigWithAuthentication,
  sortErrors,
  ValidationError,
} from './utils/loadConfig';
import { store } from './store';
import { AppApi, createApi as createRealApi } from './utils/api';
import Home from './pages/Home';
import { AppWrapper } from './components/Providers';

interface RoutesProps {
  forceAuth?: boolean;
  config?: ConfigType;
  createApi?: (props: CreateApiProps) => AppApi;
}

const Routes: React.FC<RoutesProps> = ({
  forceAuth = false,
  config,
  createApi = createRealApi,
}: RoutesProps) => {
  return (
    <BrowserRouter>
      <RouterWrapperConnect>
        <ReactRoutes>
          <Route path={workspaceRoutes.root}>
            <Route
              index
              element={
                forceAuth ? (
                  <RequireAuth>
                    <Home config={config} createApi={createApi} />
                  </RequireAuth>
                ) : (
                  <Home config={config} createApi={createApi} />
                )
              }
            />

            <Route
              path={workspaceRoutes.workspace}
              element={<Workspace config={config} createApi={createApi} />}
            />

            <Route
              path={workspaceRoutes.workspaceDetail}
              element={<Workspace config={config} createApi={createApi} />}
            />

            <Route path="/login" element={<Login />} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/code" element={<Code />} />
            <Route path="/error" element={<ErrorPage />} />
            <Route path="/*" element={<NotFound />} />
          </Route>
        </ReactRoutes>
      </RouterWrapperConnect>
    </BrowserRouter>
  );
};

interface AppProps {
  createApi?: (props: CreateApiProps) => AppApi;
}

const App: React.FC<AppProps> = ({ createApi = createRealApi }: AppProps) => {
  const [configObject] = useConfig<ConfigType>('config.json');
  document.title = configObject?.GW_FEATURE_APP_TITLE || 'GeoWeb';

  const { i18n } = useTranslation();
  if (configObject?.GW_LANGUAGE && i18n.language !== configObject.GW_LANGUAGE) {
    i18n.changeLanguage(configObject?.GW_LANGUAGE);
  }

  React.useEffect(() => {
    const sessionStorageProvider = getSessionStorageProvider();

    if (!sessionStorageProvider.getOauthState()) {
      const oauthState = getRandomString();
      sessionStorageProvider.setOauthState(oauthState);
    }

    if (
      !sessionStorageProvider.getOauthCodeVerifier() ||
      !sessionStorageProvider.getOauthCodeChallenge()
    ) {
      const codeVerifier = getRandomString();
      getCodeChallenge(codeVerifier).then((codeChallenge) => {
        // setting both codeChallenge and codeVerifier at the same time since they are related
        sessionStorageProvider.setOauthCodeVerifier(codeVerifier);
        sessionStorageProvider.setOauthCodeChallenge(codeChallenge);
      });
    }
  }, []);

  // if config is null, it has not yet loaded.
  if (!configObject) {
    return null;
  }

  // validates config.json. If there are any empty or non existing keys, it will show an error
  const validateRequiredFields = isValidConfig(configObject);
  if (validateRequiredFields !== true && isArray(validateRequiredFields)) {
    const errors = sortErrors(validateRequiredFields);

    return (
      <ThemeWrapper>
        <AlertBanner
          severity="error"
          title="Configuration (config.json) is not valid and has the following errors:"
          info={
            <>
              {Object.keys(errors).map((key) => (
                <p key={key}>
                  <strong>{key}: </strong>
                  {errors[key as keyof typeof errors]!.map(
                    (error: ValidationError) => error.key,
                  ).join(', ')}
                </p>
              ))}
            </>
          }
        />
      </ThemeWrapper>
    );
  }

  // validates config.json for valid auth keys. If it's not valid, it will show the default application without errors
  if (isValidConfigWithAuthentication(configObject) !== true) {
    return (
      <AppWrapper
        store={store}
        defaultThemeName={configObject?.GW_DEFAULT_THEME}
      >
        <Routes config={configObject} createApi={createApi} />
      </AppWrapper>
    );
  }
  // renders the validated homepage
  return (
    <AuthenticationProvider
      configURLS={getAuthConfig(configObject as AuthenticationConfig)}
    >
      <AppWrapper
        store={store}
        defaultThemeName={configObject?.GW_DEFAULT_THEME}
      >
        <Routes
          config={configObject}
          forceAuth={configObject?.GW_FEATURE_FORCE_AUTHENTICATION}
          createApi={createApi}
        />
      </AppWrapper>
    </AuthenticationProvider>
  );
};

export default App;
