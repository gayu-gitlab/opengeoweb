/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */
import { renderHook, waitFor } from '@testing-library/react';

import { ConfigType } from '@opengeoweb/shared';

import {
  getNonExistingKeyWarning,
  isValidConfig,
  isValidConfigWithAuthentication,
  sortErrors,
  useConfig,
  ValidationType,
} from './loadConfig';

describe('utils/loadConfig/isValidConfig', () => {
  const storedFetch = global['fetch'];
  beforeEach(() => {
    jest.spyOn(console, 'warn').mockImplementation(() => {});
  });
  afterEach(() => {
    global['fetch'] = storedFetch;
  });

  it('should return true when config is valid', () => {
    const validConfig = {
      GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
    };
    expect({}).toBeTruthy();
    expect(isValidConfig(validConfig)).toBeTruthy();
    expect({
      GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
    }).toBeTruthy();
  });

  it('should return list of errors when config is invalid', () => {
    expect(isValidConfig({} as ConfigType)).toBeTruthy();

    // empty keys
    expect(
      isValidConfig({
        GW_INITIAL_PRESETS_FILENAME: '',
      }),
    ).toEqual([
      { key: 'GW_INITIAL_PRESETS_FILENAME', errorType: ValidationType.empty },
    ]);
  });

  it('should return list of mixed errors when config is invalid', () => {
    expect(isValidConfig({} as ConfigType)).toBeTruthy();

    expect(
      isValidConfig({
        NON_EXISTING_KEY: 'testing',
        GW_AUTH_LOGIN_URL: '',
      } as unknown as ConfigType),
    ).toEqual([{ key: 'GW_AUTH_LOGIN_URL', errorType: ValidationType.empty }]);
  });

  it('should trigger warnings for non existing keys', () => {
    const spy = jest.spyOn(console, 'warn');

    const testConfigWithNonexistingKeys = {
      NON_EXISTING_KEY: 'testing',
      NON_EXISTING_KEY_TWO: false,
      NON_EXISTING_KEY_THREE: false,
    } as ConfigType;

    isValidConfig(testConfigWithNonexistingKeys);

    Object.keys(testConfigWithNonexistingKeys).forEach((key, index) => {
      expect(spy).toHaveBeenNthCalledWith(
        index + 1,
        getNonExistingKeyWarning(key),
      );
    });
  });

  it('should return true when authentication config is valid', () => {
    const validConfig = {
      GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
      GW_AUTH_LOGIN_URL: 'http://fakeurl',
      GW_AUTH_LOGOUT_URL: 'http://fakeurl',
      GW_AUTH_TOKEN_URL: 'http://fakeurl',
      GW_AUTH_CLIENT_ID: '123456',
      GW_APP_URL: 'http://fakeurl',
      GW_FEATURE_APP_TITLE: 'title',
    };
    expect(isValidConfigWithAuthentication(validConfig)).toBeTruthy();
    expect(isValidConfig(validConfig)).toBeTruthy();
  });
});

describe('sortErrors', () => {
  it('should return undefined if no errors ', () => {
    const result = sortErrors([]);

    expect(result[ValidationType.missing]).toBeUndefined();
    expect(result[ValidationType.nonExist]).toBeUndefined();
    expect(result[ValidationType.empty]).toBeUndefined();
  });
  it('should sort errors on type', () => {
    const testErrors = [
      { key: 'GW_AUTH_TOKEN_URL', errorType: ValidationType.missing },
      { key: 'GW_INITIAL_PRESETS_FILENAME', errorType: ValidationType.missing },
      { key: 'GW_AUTH_LOGIN_URL', errorType: ValidationType.empty },
      { key: 'GW_AUTH_LOGOUT_URL', errorType: ValidationType.empty },
    ];

    const result = sortErrors(testErrors);

    expect(result[ValidationType.missing]).toEqual([
      testErrors[0],
      testErrors[1],
    ]);

    expect(result[ValidationType.empty]).toEqual([
      testErrors[2],
      testErrors[3],
    ]);
  });
});

describe('useConfig', () => {
  it('should load file from assets', async () => {
    const validConfig = {
      GW_INITIAL_PRESETS_FILENAME: 'initialPresets.json',
      GW_AUTH_LOGIN_URL: 'http://fakeurl',
      GW_AUTH_LOGOUT_URL: 'http://fakeurl',
      GW_AUTH_TOKEN_URL: 'http://fakeurl',
      GW_AUTH_CLIENT_ID: '123456',
      GW_APP_URL: 'http://fakeurl',
      GW_FEATURE_APP_TITLE: 'title',
    };

    const mockFetch = jest.fn().mockImplementation(() =>
      Promise.resolve({
        json: () => Promise.resolve(validConfig),
      }),
    );
    global['fetch'] = mockFetch;

    const { result } = renderHook(() => useConfig<ConfigType>('config.json'));
    await waitFor(() => {
      const [config] = result.current;
      expect(config).toEqual(validConfig);
    });
    expect(mockFetch).toHaveBeenCalledWith('./assets/config.json', {
      credentials: 'include',
    });
  });

  it('should log a info and return null when the config has not yet loaded and it does not exist', async () => {
    const spy = jest.spyOn(global.console, 'info').mockImplementation();
    global['fetch'] = jest.fn().mockImplementation(() => Promise.reject);

    const { result } = renderHook(() => useConfig<ConfigType>('config.json'));
    const [config] = result.current;
    expect(config).toEqual(null);
    await waitFor(() =>
      expect(spy).toHaveBeenCalledWith(
        'Unable to load configuration file (config.json)',
      ),
    );
  });

  it('should log a info and return an empty object if the config does note exist', async () => {
    const spy = jest.spyOn(global.console, 'info').mockImplementation();
    global['fetch'] = jest.fn().mockImplementation(() => Promise.reject);

    const { result } = renderHook(() => useConfig<ConfigType>('config.json'));

    await waitFor(() => {
      const [config] = result.current;
      expect(config).toEqual({});
    });
    expect(spy).toHaveBeenCalledWith(
      'Unable to load configuration file (config.json)',
    );
  });
});
