/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

describe('TimeSeriesManager', () => {
  it('should drag and drop plots', () => {
    cy.intercept('GET', '**/opendata.fmi.fi/**', {});
    cy.mockExampleConfigWithBackendUrl();
    cy.mockFetchViewpresetList();
    cy.mockWorkspaces();
    cy.visit('/');
    cy.wait('@workspaceList');
    cy.findByTestId('workspaceMenuButton').click();

    cy.findByText('Timeseries').click();
    cy.findByRole('button', { name: 'TimeSeries Manager' }).click();

    cy.findAllByText(/Plot [0-9]/).as('plotTitles');
    cy.get('@plotTitles').eq(0).contains('Plot 1');
    cy.get('@plotTitles').eq(1).contains('Plot 2');

    cy.get('button[data-testid=dragHandleButton_0').dragTo(
      'button[data-testid=dragHandleButton_1',
    );

    cy.get('@plotTitles').eq(0).contains('Plot 2');
    cy.get('@plotTitles').eq(1).contains('Plot 1');

    cy.get('[id=Plot_2]').contains('Bar');
    cy.get('[id=Plot_2]').dragTo('[id=Plot_1]');
    cy.get('[id=Plot_1]').contains('Bar');
  });
});
