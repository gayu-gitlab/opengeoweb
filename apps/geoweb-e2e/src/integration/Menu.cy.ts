/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

describe('Menu', () => {
  it('should not show menu items for features that are not active', () => {
    cy.mockExampleConfigNoFeatures();
    cy.visit('/');
    // Open the menu
    cy.findByTestId('menuButton').click();
    cy.findByText('Space Weather Forecast').should('not.exist');
  });

  it('should show the Spaceweather module when GW_FEATURE_MODULE_SPACE_WEATHER is on', () => {
    cy.mockExampleConfig();
    cy.mockCognitoLoginSuccess();
    cy.findByTestId('menuButton').click();
    cy.findByText('Space Weather Forecast').should('be.visible');
    cy.findByText('Space Weather Forecast').click();

    cy.mockCognitoLogoutSuccess();
  });

  it('should open and close the Space Weather Forecast dialog', () => {
    cy.mockExampleConfig();
    cy.mockCognitoLoginSuccess();

    // Open the menu
    cy.findByTestId('menuButton').click();
    // Open the Space Weather Forecast
    cy.findByText('Space Weather Forecast').click();
    // Check that menu is closed
    cy.findByText('Presets').should('not.exist');
    // Check that the Space Weather dialog is shown
    cy.findByTestId('SpaceWeatherModule').should('be.visible');
    // Close the dialog
    cy.findByTestId('SpaceWeatherModule').findByTestId('closeBtn').click();
    // Check that the Space Weather dialog is closed
    cy.findByTestId('SpaceWeatherModule').should('not.exist');
    cy.mockCognitoLogoutSuccess();
  });

  it('should open the TAF module', () => {
    cy.mockExampleConfig();
    cy.mockWorkspaces();
    cy.mockCognitoLoginSuccess();
    cy.mockFetchTAFList();
    cy.wait('@workspaceList');

    // Open the menu
    cy.findByTestId('menuButton').click();
    // Check that Aviation list is shown
    cy.findByText('Aviation').should('be.visible');
    // Open the TAF module
    cy.findAllByText('TAF').eq(1).click();
    // Check that menu is closed
    cy.findByText('Presets').should('not.exist');
    // Check that the TAF module is shown
    cy.wait('@taflist');
    cy.get('#tafmodule').should('be.visible');
    cy.mockCognitoLogoutSuccess();
  });

  it('should open the Sigmet module', () => {
    cy.mockExampleConfig();
    cy.mockExampleConfigWithSigmet();
    cy.mockWorkspaces();
    cy.mockCognitoLoginSuccess();
    cy.mockFetchSIGMETList();
    cy.wait('@workspaceList');

    // Open the menu
    cy.findByTestId('menuButton').click();
    // Check that Aviation list is shown
    cy.findByText('Aviation').should('be.visible');
    // Open the TAF module
    cy.findByText('SIGMET').click();
    // Check that menu is closed
    cy.findByText('Presets').should('not.exist');

    cy.wait('@sigmetlist');
    cy.findByTestId('productListItem').should('be.visible');
    cy.mockCognitoLogoutSuccess();
  });

  it('should open the Airmet module', () => {
    cy.mockExampleConfig();
    cy.mockExampleConfigWithAirmet();
    cy.mockWorkspaces();
    cy.mockCognitoLoginSuccess();
    cy.mockFetchAIRMETList();
    cy.wait('@workspaceList');

    // Open the menu
    cy.findByTestId('menuButton').click();
    // Check that Aviation list is shown
    cy.findByText('Aviation').should('be.visible');
    // Open the TAF module
    cy.findByText('AIRMET').click();
    // Check that menu is closed
    cy.findByText('Presets').should('not.exist');

    cy.wait('@airmetlist');
    cy.findByTestId('productListItem').should('be.visible');
    cy.mockCognitoLogoutSuccess();
  });
});
