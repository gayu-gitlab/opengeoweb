/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

describe('Warnings', () => {
  beforeEach(() => {
    cy.mockWarnings();
    cy.mockWorkspaces();
    cy.mockExampleConfig();
    cy.mockCognitoLoginSuccess();
    // close layer manager
    cy.findByLabelText('Layer Manager').click();
  });

  it('should share an object to a public warning', () => {
    // open object manager
    cy.findByLabelText('Object Manager Button for 1_screen').click();
    cy.wait('@objectList');
    // click share button
    cy.findAllByLabelText('Share').first().click();
    cy.findByText('PUBLIC WARNING').click();
    // Public Warning form should open
    cy.findByText('Public Warning').should('be.visible');
    // Correct object should be loaded
    cy.findByText('Area')
      .next()
      .findByText('Drawing object 101')
      .should('be.visible');
  });

  it('should save a new object', () => {
    // open drawing toolbox
    cy.findByLabelText('Drawing Toolbox').click();
    cy.findByText('Drawing Toolbox 1_screen').should('be.visible');
    // draw a polygon
    cy.findByLabelText('Polygon').click();
    cy.findByLabelText('map').click(650, 400);
    cy.findByLabelText('map').click(660, 450);
    cy.findByLabelText('map').click(500, 500);
    cy.findByTestId('drawtools-polygon').type('{esc}');
    // Change object name
    cy.findByRole('textbox', { name: 'Object name' }).clear();
    cy.findByRole('textbox', { name: 'Object name' }).type('New object');
    // save object
    cy.findByText('Save new object').click();
    cy.wait('@saveNewObject');
    // button text should be updated
    cy.findByText('Update object').should('be.visible');
    // snackbar should be visible
    cy.findByText('Object New object has been saved!').should('be.visible');
  });

  it('should update an object', () => {
    // open object manager
    cy.findByLabelText('Object Manager Button for 1_screen').click();
    cy.wait('@objectList');
    // click edit button
    cy.findAllByLabelText('Object options').first().click();
    cy.findByText('Edit').click();
    cy.wait('@getObjectDetail');
    // object manager should close
    cy.findAllByLabelText('Object options').should('have.length', 0);
    // drawing toolbox should open for selected object
    cy.findByText('Drawing Toolbox 1_screen').should('be.visible');
    cy.findByRole('textbox', { name: 'Object name' }).should(
      'have.value',
      'Drawing object 101',
    );
    // update opbject
    cy.findByText('Update object').click();
    cy.wait('@updateObject');
    // snackbar should be visible
    cy.findByText('Object Drawing object 101 has been updated!').should(
      'be.visible',
    );
  });

  it('should delete an object', () => {
    // open object manager
    cy.findByLabelText('Object Manager Button for 1_screen').click();
    cy.wait('@objectList');
    // click delete button
    cy.findAllByLabelText('Object options').first().click();
    cy.findByText('Delete').click();
    // confirm
    cy.findByText('Delete').click();
    cy.wait('@deleteObject');
    cy.wait('@objectList');
    // snackbar should be visible
    cy.findByText('Drawing object 101 has been deleted').should('be.visible');
  });

  it('should publish a public warning', () => {
    cy.findByTestId('workspaceMenuButton').click();
    cy.findByText('Workspace menu').should('be.visible');

    cy.wait('@workspaceList');

    // open warning list workspace
    cy.findByText('Warning list').click();
    cy.wait('@getWarnings');

    // open object manager
    cy.findByLabelText('Object Manager Button for 2_screen').click();
    cy.wait('@objectList');
    // click share button
    cy.findAllByLabelText('Share').first().click();
    cy.findByText('PUBLIC WARNING').click();
    // Public Warning form should open
    cy.findByText('Public Warning').should('be.visible');
    // Correct object should be loaded
    cy.findByText('Area')
      .next()
      .findByText('Drawing object 101')
      .should('be.visible');

    // fill in form
    cy.findByLabelText('Phenomenon').click();
    cy.findByText('Rain').click();
    cy.findByLabelText('Level').click();
    cy.findByText('Extreme').click();

    // click publish
    cy.findByText('Publish').click();
    cy.wait('@saveWarningAs');
    // snackbar should show
    cy.findByText('Public warning has succesfully been published!').should(
      'be.visible',
    );
    // form should be hidden after publish
    cy.findByText('Public Warning').should('not.exist');
  });

  it('should save a draft public warning', () => {
    cy.findByTestId('workspaceMenuButton').click();
    cy.findByText('Workspace menu').should('be.visible');

    cy.wait('@workspaceList');

    // open warning list workspace
    cy.findByText('Warning list').click();
    cy.wait('@getWarnings');

    // create new warning
    cy.findByText('Create a warning').click();

    // Public Warning form should open
    cy.findByText('Public Warning').should('be.visible');

    // fill in form
    cy.findByLabelText('Phenomenon').click();
    cy.findByText('Rain').click();

    // click save
    cy.findByText('Save').click();
    cy.wait('@saveWarningAs');
    // snackbar should show
    cy.findByText('Public warning has succesfully been saved!').should(
      'be.visible',
    );
    // list should be refreshed
    cy.wait('@getWarnings');
  });

  it('should delete a draft public warning', () => {
    cy.findByTestId('workspaceMenuButton').click();
    cy.findByText('Workspace menu').should('be.visible');

    cy.wait('@workspaceList');

    // open warning list workspace
    cy.findByText('Warning list').click();
    cy.wait('@getWarnings');

    // delete the first draft
    cy.findByLabelText('warninglist DRAFT')
      .findAllByLabelText('Options')
      .first()
      .click();
    cy.findByText('Delete').click();
    cy.findByText('Delete').click();

    cy.wait('@deleteWarning');
    // snackbar should show
    cy.findByText('Warning has been deleted').should('be.visible');
    // list should be refreshed
    cy.wait('@getWarnings');
  });
});
