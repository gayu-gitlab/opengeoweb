/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

import { SigmetConfig } from '@opengeoweb/sigmet-airmet';
import {
  getDayFromDate,
  getHoursFromDate,
  getLaterDate,
  getMonthFromDate,
} from '../support/utils';

describe('Sigmet', () => {
  beforeEach(() => {
    cy.mockRadarGetCapabilities();
    cy.mockExampleConfigWithSigmet();
    cy.mockWorkspaces();

    cy.mockCognitoLoginSuccess();
    cy.findByTestId('workspaceMenuButton').click();
  });

  afterEach(() => {
    cy.mockCognitoLogoutSuccess();
  });

  it('should load the given sigmet configuration in the edit form', () => {
    cy.wait('@workspaceList');
    cy.mockFetchSIGMETList();

    // open sigmet module
    cy.findByText('Sigmet').click();
    // Check that menu is closed
    cy.findByText('Workspace menu').should('not.be.visible');
    cy.findByTestId('productListCreateButton').click();
    const currentTime = new Date().toISOString();

    // confirm that SIGMET type selection defaults to 'Normal'
    cy.get('#mui-component-select-type').then(() => {
      cy.findByText(`Normal`).should('be.visible');
    });

    // choose type
    cy.get('#mui-component-select-type').click();
    cy.findByText('Test').click();

    // choose phenomenon
    cy.get('#mui-component-select-phenomenon').click();
    cy.findByText('Severe icing').click();

    cy.intercept('POST', /\/sigmet/).as('mockSave');
    cy.findByTestId('productform-dialog-draft').click();

    cy.fixture('mockExample.sigmetConfig.json').then(
      (sigmetConfig: SigmetConfig) => {
        const firConfig = sigmetConfig.fir_areas[sigmetConfig.active_firs[0]];
        cy.wait('@mockSave').then((interception) => {
          assert.isNotNull(interception.request.body);
          const { sigmet } = interception.request.body;
          // check fir info
          assert.equal(
            sigmet.locationIndicatorMWO,
            sigmetConfig.location_indicator_mwo,
          );
          assert.equal(
            sigmet.locationIndicatorATSU,
            firConfig.location_indicator_atsu,
          );
          assert.equal(
            sigmet.locationIndicatorATSR,
            firConfig.location_indicator_atsr,
          );
          assert.equal(sigmet.firName, firConfig.fir_name);
        });

        // check valid_from_delay_minutes
        cy.get('input[name="validDateStart"]').then(($date) => {
          const defaultDate = $date.attr('value')!;
          const expectedStartDate = getLaterDate(
            currentTime,
            sigmetConfig.valid_from_delay_minutes,
            'minutes',
            false,
            true,
          );
          assert.equal(defaultDate, expectedStartDate);
        });

        // check default_validity_minutes
        cy.get('input[name="validDateEnd"]').then(($date) => {
          const defaultDate = $date.attr('value')!;
          const expectedEndDate = getLaterDate(
            currentTime,
            sigmetConfig.valid_from_delay_minutes +
              sigmetConfig.default_validity_minutes,
            'minutes',
            false,
            true,
          );
          assert.equal(defaultDate, expectedEndDate);
        });

        // check max_hours_of_validity
        cy.get('input[name="validDateEnd"]').then(($date) => {
          const oldDate = $date.attr('value')!;

          const newDate = getLaterDate(
            oldDate,
            firConfig.max_hours_of_validity + 1,
          );

          // Set new date
          // Move cursor right to select month and set new month (applicable if we go past midnight at end of month)
          cy.get('[name="validDateEnd"]').type('{rightArrow}');
          cy.get('[name="validDateEnd"]').type(getMonthFromDate(newDate));
          // Set new day (applicable if we go past midnight)
          cy.get('[name="validDateEnd"]').type(getDayFromDate(newDate));
          // Set new hours
          cy.get('[name="validDateEnd"]').type(getHoursFromDate(newDate));
          cy.findByText(
            `Valid until time can be no more than ${firConfig.max_hours_of_validity} hours after Valid from time`,
          ).should('exist');
          // Re-enter the old date again
          // Select month again to revert back to old month the cursor automatically moves right to next field
          cy.get('[name="validDateEnd"]').type('{leftArrow}');
          cy.get('[name="validDateEnd"]').type('{leftArrow}');
          cy.get('[name="validDateEnd"]').type('{leftArrow}');
          cy.get('[name="validDateEnd"]').type(
            getMonthFromDate(oldDate, false),
          );
          cy.get('[name="validDateEnd"]').type(getDayFromDate(oldDate, false));
          cy.get('[name="validDateEnd"]').type(
            getHoursFromDate(oldDate, false),
          );
        });
        // check hours_before_validity
        cy.get('input[name="validDateStart"]').then(($date) => {
          const oldDate = $date.attr('value')!;
          const newDate = getLaterDate(
            oldDate,
            firConfig.hours_before_validity + 1,
          );
          // Set new date
          // Move cursor right to select month and set new month (applicable if we go past midnight at end of month)
          cy.get('[name="validDateStart"]').type('{rightArrow}');
          cy.get('[name="validDateStart"]').type(getMonthFromDate(newDate));
          // Set new day (applicable if we go past midnight)
          cy.get('[name="validDateStart"]').type(getDayFromDate(newDate));
          // Set new hours
          cy.get('[name="validDateStart"]').type(getHoursFromDate(newDate));

          cy.findByText(
            `Valid from time can be no more than ${firConfig.hours_before_validity} hours after current time`,
          ).should('exist');

          // Re-enter the old date again
          // Select month again to revert back to old month
          // The cursor automatically moves right to next field
          cy.get('[name="validDateStart"]').type('{leftArrow}');
          cy.get('[name="validDateStart"]').type('{leftArrow}');
          cy.get('[name="validDateStart"]').type('{leftArrow}');
          cy.get('[name="validDateStart"]').type(
            getMonthFromDate(oldDate, false),
          );
          cy.get('[name="validDateStart"]').type(
            getDayFromDate(oldDate, false),
          );
          cy.get('[name="validDateStart"]').type(
            getHoursFromDate(oldDate, false),
          );
        });

        // check level_unit
        cy.get('input[value=AT]').click();
        cy.get('[id="mui-component-select-level.unit"]').click();
        const levelUnits = firConfig.units.find(
          (unitConfig) => unitConfig.unit_type === 'level_unit',
        )!.allowed_units;
        cy.get('[id="menu-level.unit"] li').then(($items) => {
          assert.equal($items.length, levelUnits.length);
          $items.map((index, item) =>
            assert.equal(item.textContent!.toUpperCase(), levelUnits[index]),
          );
          $items.first().trigger('click');
        });

        // check level_min
        cy.get('[name="level.value"]').clear();
        cy.get('[name="level.value"]').type('0');
        const levelMin1 =
          firConfig.level_min![
            levelUnits[0] as keyof typeof firConfig.level_min
          ];
        const levelMin2 =
          firConfig.level_min![
            levelUnits[1] as keyof typeof firConfig.level_max
          ];
        cy.findByText(
          `The minimum level in ${levelUnits[0].toLowerCase()} is ${levelMin1}`,
        ).should('exist');
        cy.get('[id="mui-component-select-level.unit"]').click();
        cy.get('[id="menu-level.unit"] li').then(($items) => {
          $items.last().trigger('click');
          cy.findByText(
            `The minimum level in ${levelUnits[1]} is ${levelMin2}`,
          ).should('exist');
        });

        // check level_max
        cy.get('[name="level.value"]').clear();
        cy.get('[name="level.value"]').type('5000');
        const levelMax1 =
          firConfig.level_max![
            levelUnits[0] as keyof typeof firConfig.level_max
          ];
        const levelMax2 =
          firConfig.level_max![
            levelUnits[1] as keyof typeof firConfig.level_max
          ];
        cy.findByText(
          `The maximum level in ${levelUnits[1]} is ${levelMax2}`,
        ).should('exist');
        cy.get('[id="mui-component-select-level.unit"]').click();
        cy.get('[id="menu-level.unit"] li').then(($items) => {
          $items.first().trigger('click');
          cy.findByText(
            `The maximum level in ${levelUnits[0].toLowerCase()} is ${levelMax1}`,
          ).should('exist');
        });

        // check level_rounding_FT
        cy.get('[name="level.value"]').clear();
        cy.get('[name="level.value"]').type('173');
        const levelRoundingFT = firConfig.level_rounding_FT;
        cy.findByText(
          `Levels must be rounded to the nearest ${levelRoundingFT}ft`,
        ).should('exist');

        // check level_rounding_FL
        const levelRoundingFL = firConfig.level_rounding_FL;
        cy.get('[id="mui-component-select-level.unit"]').click();
        cy.get('[id="menu-level.unit"] li').then(($items) => {
          $items.last().trigger('click');
          cy.findByText(
            `Levels must be rounded to the nearest ${levelRoundingFL}`,
          ).should('exist');
        });

        // check movement_unit
        cy.get('input[value=MOVEMENT]').click();
        cy.get('[id="mui-component-select-movementUnit"]').click();
        const movementUnits = firConfig.units.find(
          (unitConfig) => unitConfig.unit_type === 'movement_unit',
        )!.allowed_units;
        cy.get('[id="menu-movementUnit"] li').then(($items) => {
          assert.equal($items.length, movementUnits.length);
          $items.map((index, item) =>
            assert.equal(item.textContent!.toUpperCase(), movementUnits[index]),
          );
          $items.first().trigger('click');
        });

        // check movement_min
        cy.get('[name="movementSpeed"]').clear();
        cy.get('[name="movementSpeed"]').type('0');
        const movementMin =
          firConfig.movement_min![
            movementUnits[0] as keyof typeof firConfig.movement_min
          ];
        const movementMin2 =
          firConfig.movement_min![
            movementUnits[1] as keyof typeof firConfig.movement_min
          ];
        cy.findByText(
          `The minimum level in ${movementUnits[0].toLowerCase()} is ${movementMin}`,
        ).should('exist');
        cy.get('[id="mui-component-select-movementUnit"]').click();
        cy.get('[id="menu-movementUnit"] li').then(($items) => {
          $items.last().trigger('click');
          cy.findByText(
            `The minimum level in ${movementUnits[1].toLowerCase()} is ${movementMin2}`,
          ).should('exist');
        });

        // check movement_max
        cy.get('[name="movementSpeed"]').clear();
        cy.get('[name="movementSpeed"]').type('200');
        const movementMax =
          firConfig.movement_max![
            movementUnits[0] as keyof typeof firConfig.movement_max
          ];
        const movementMax2 =
          firConfig.movement_max![
            movementUnits[1] as keyof typeof firConfig.movement_max
          ];
        cy.findByText(
          `The maximum level in ${movementUnits[1].toLowerCase()} is ${movementMax2}`,
        ).should('exist');
        cy.get('[id="mui-component-select-movementUnit"]').click();
        cy.get('[id="menu-movementUnit"] li').then(($items) => {
          $items.first().trigger('click');
          cy.findByText(
            `The maximum level in ${movementUnits[0].toLowerCase()} is ${movementMax}`,
          ).should('exist');
        });

        // check movement_rounding_kt
        cy.get('[name="movementSpeed"]').clear();
        cy.get('[name="movementSpeed"]').type('21');
        const movementRoundingKT = firConfig.movement_rounding_kt;
        cy.findByText(
          `Speed should be rounded to the nearest ${movementRoundingKT}kt`,
        ).should('exist');

        // check movement_rounding_kmh
        const movementRoundingKMH = firConfig.movement_rounding_kmh;
        cy.get('[id="mui-component-select-movementUnit"]').click();
        cy.get('[id="menu-movementUnit"] li').then(($items) => {
          $items.last().trigger('click');
          cy.findByText(
            `Speed should be rounded to the nearest ${movementRoundingKMH}kmh`,
          ).should('exist');
        });

        // Tropical cyclone: phenomenon not configurable yet

        // Volcanic ash cloud
        cy.get('#mui-component-select-phenomenon').click();
        cy.findByText('Volcanic ash cloud').click();

        // check va_hours_before_validity
        cy.get('input[name="validDateStart"]').then(($date) => {
          const oldDate = $date.attr('value')!;
          const newDate = getLaterDate(
            oldDate,
            firConfig.va_hours_before_validity + 1,
          );

          // Set new date
          // Move cursor right to select month and set new month (applicable if we go past midnight at end of month)
          cy.get('[name="validDateStart"]').type('{leftArrow}');
          cy.get('[name="validDateStart"]').type('{leftArrow}');
          cy.get('[name="validDateStart"]').type('{leftArrow}');
          cy.get('[name="validDateStart"]').type(getMonthFromDate(newDate));
          // Set new day (applicable if we go past midnight)
          cy.get('[name="validDateStart"]').type(getDayFromDate(newDate));
          // Set new hours
          cy.get('[name="validDateStart"]').type(getHoursFromDate(newDate));
          cy.findByText(
            'Valid until time has to be after Valid from time',
          ).should('exist');
          cy.findByText(
            `Valid from time can be no more than ${firConfig.va_hours_before_validity} hours after current time`,
          ).should('exist');
          // Re-enter the old date again
          // Select month again to revert back to old month
          // The cursor automatically moves right to next field
          cy.get('[name="validDateStart"]').type('{leftArrow}');
          cy.get('[name="validDateStart"]').type('{leftArrow}');
          cy.get('[name="validDateStart"]').type('{leftArrow}');
          cy.get('[name="validDateStart"]').type(
            getMonthFromDate(oldDate, false),
          );
          cy.get('[name="validDateStart"]').type(
            getDayFromDate(oldDate, false),
          );
          cy.get('[name="validDateStart"]').type(
            getHoursFromDate(oldDate, false),
          );
        });

        // check va_max_hours_of_validity
        cy.get('input[name="validDateEnd"]').then(($date) => {
          const oldDate = $date.attr('value')!;
          const newDate = getLaterDate(
            oldDate,
            firConfig.va_max_hours_of_validity + 1,
          );
          // Set new date
          // Move cursor right to select month and set new month (applicable if we go past midnight at end of month)
          cy.get('[name="validDateEnd"]').type('{leftArrow}');
          cy.get('[name="validDateEnd"]').type('{leftArrow}');
          cy.get('[name="validDateEnd"]').type('{leftArrow}');
          cy.get('[name="validDateEnd"]').type(getMonthFromDate(newDate));
          // Set new day (applicable if we go past midnight)
          cy.get('[name="validDateEnd"]').type(getDayFromDate(newDate));
          // Set new hours
          cy.get('[name="validDateEnd"]').type(getHoursFromDate(newDate));
          cy.findByText(
            `Valid until time can be no more than ${firConfig.va_max_hours_of_validity} hours after Valid from time`,
          ).should('exist');
          // Re-enter the old date again
          // Select month again to revert back to old month
          // The cursor automatically moves right to next field
          cy.get('[name="validDateEnd"]').type('{leftArrow}');
          cy.get('[name="validDateEnd"]').type('{leftArrow}');
          cy.get('[name="validDateEnd"]').type('{leftArrow}');
          cy.get('[name="validDateEnd"]').type(
            getMonthFromDate(oldDate, false),
          );
          cy.get('[name="validDateEnd"]').type(getDayFromDate(oldDate, false));
          cy.get('[name="validDateEnd"]').type(
            getHoursFromDate(oldDate, false),
          );
        });
      },
    );

    // close the sigmet module
    cy.findByText('BACK').click();
    cy.findByText('Discard and close').click();
    cy.findByTestId('close-btn').click();
  });

  it('should show adjacent_firs from sigmet configuration when cancelling Volcanic ash cloud', () => {
    cy.wait('@workspaceList');

    // mock sigmet list with published VA_CLD
    cy.mockFetchSIGMETList();

    // open sigmet module
    cy.findByText('Sigmet').click();
    // Check that menu is closed
    cy.findByText('Workspace menu').should('not.be.visible');
    cy.wait('@sigmetlist');

    // open published VA_CLD SIGMET
    cy.findByTestId('productListItem').click();
    // cancel it
    cy.findByText('Cancel').click();

    // check adjacent_firs
    cy.fixture('mockExample.sigmetConfig.json').then((sigmetConfig) => {
      const firConfig = sigmetConfig.fir_areas[sigmetConfig.active_firs[0]];
      cy.get('[name="vaSigmetMoveToFIR"]').then(($items) => {
        const { adjacent_firs: adjacentFirs } = firConfig;
        assert.equal($items.length, adjacentFirs.length + 1);
        $items.map((index, item) =>
          assert.equal(
            item.getAttribute('value'),
            adjacentFirs[index] || 'UNKNOWN',
          ),
        );
        cy.findByTestId('confirmationDialog-cancel').click();
      });
    });

    // close the sigmet module
    cy.findByText('BACK').click();
    cy.findByTestId('close-btn').click();
  });

  it('should load the correct configuration when changing the selected FIR', () => {
    cy.wait('@workspaceList');
    cy.mockFetchSIGMETList();

    // open sigmet module
    cy.findByText('Sigmet').click();
    // Check that menu is closed
    cy.findByText('Workspace menu').should('not.be.visible');
    cy.findByTestId('productListCreateButton').click();

    // choose type
    cy.get('#mui-component-select-type').click();
    cy.findByText('Test').click();

    // choose phenomenon
    cy.get('#mui-component-select-phenomenon').click();
    cy.findByText('Severe icing').click();

    // choose other FIR
    cy.get('#mui-component-select-locationIndicatorATSR').click();
    cy.findByText('BRUSSEL FIR').click();

    cy.intercept('POST', /\/sigmet/).as('mockSave');
    cy.findByTestId('productform-dialog-draft').click();

    cy.fixture('mockExample.sigmetConfig.json').then(
      (sigmetConfig: SigmetConfig) => {
        const firConfig = sigmetConfig.fir_areas[sigmetConfig.active_firs[1]];
        cy.wait('@mockSave').then((interception) => {
          assert.isNotNull(interception.request.body);
          const { sigmet } = interception.request.body;
          // check fir info
          assert.equal(
            sigmet.locationIndicatorMWO,
            sigmetConfig.location_indicator_mwo,
          );
          assert.equal(
            sigmet.locationIndicatorATSU,
            firConfig.location_indicator_atsu,
          );
          assert.equal(
            sigmet.locationIndicatorATSR,
            firConfig.location_indicator_atsr,
          );
          assert.equal(sigmet.firName, firConfig.fir_name);
        });

        // check level_unit
        cy.get('input[value=AT]').click();
        cy.get('[id="mui-component-select-level.unit"]').click();
        const levelUnits = firConfig.units.find(
          (unitConfig) => unitConfig.unit_type === 'level_unit',
        )!.allowed_units;
        cy.get('[id="menu-level.unit"] li').then(($items) => {
          assert.equal($items.length, levelUnits.length);
          $items.map((index, item) =>
            assert.equal(item.textContent!.toUpperCase(), levelUnits[index]),
          );
          $items.first().trigger('click');
        });

        // check movement_unit
        cy.get('input[value=MOVEMENT]').click();
        cy.get('[id="mui-component-select-movementUnit"]').click();
        const movementUnits = firConfig.units.find(
          (unitConfig) => unitConfig.unit_type === 'movement_unit',
        )!.allowed_units;
        cy.get('[id="menu-movementUnit"] li').then(($items) => {
          assert.equal($items.length, movementUnits.length);
          $items.map((index, item) =>
            assert.equal(item.textContent!.toUpperCase(), movementUnits[index]),
          );
          $items.first().trigger('click');
        });
      },
    );

    // close the sigmet module
    cy.findByText('BACK').click();
    cy.findByText('Discard and close').click();
    cy.findByTestId('close-btn').click();
  });
});
