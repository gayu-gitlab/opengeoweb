/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */

const convertToUTCString = (date: string): string => {
  const year = Number(date.slice(0, 4));
  const month = Number(date.slice(5, 7));
  const day = Number(date.slice(8, 10));
  const hours = Number(date.slice(11, 13));
  const minutes = Number(date.slice(14, 16));

  return new Date(Date.UTC(year, month - 1, day, hours, minutes)).toISOString();
};

/** helper function to convert date to a later time */
export const getLaterDate = (
  date: string,
  amountToAdd: number,
  unitOfTime: 'hours' | 'minutes' = 'hours',
  convertToUTC = true, // If input date string is in format "YYYY/MM/DD HH:mm"
  timepickerFormat = false, // If output needs to be in datepicker format "YYYY/MM/DD HH:mm"
): string => {
  const currentDateUTC = convertToUTC ? convertToUTCString(date) : date;
  const currentDate = new Date(currentDateUTC);

  const newDate =
    unitOfTime === 'hours'
      ? new Date(date).setUTCHours(currentDate.getUTCHours() + amountToAdd)
      : new Date(date).setUTCMinutes(currentDate.getUTCMinutes() + amountToAdd);

  const dateString = new Date(newDate).toISOString();

  if (!timepickerFormat) {
    return dateString;
  }

  // Construct 'yyyy/MM/dd HH:mm' format
  return `${new Date(newDate).getUTCFullYear()}/${getMonthFromDate(
    dateString,
  )}/${getDayFromDate(dateString)} ${getHoursFromDate(
    dateString,
  )}:${getMinutesFromDate(dateString)}`;
};

export const getMinutesFromDate = (date: string): string => {
  const newDate = new Date(date).getUTCMinutes();
  return newDate < 10 ? `0${newDate.toString()}` : newDate.toString();
};

// Pass passedUTC = false in case you are passing date string formatted as used in the date/time picker ('yyyy/MM/dd HH:mm')
export const getHoursFromDate = (date: string, passedUTC = true): string => {
  const newDate = passedUTC
    ? new Date(date).getUTCHours()
    : new Date(date).getHours();
  return newDate < 10 ? `0${newDate.toString()}` : newDate.toString();
};

// Pass passedUTC = false in case you are passing date string formatted as used in the date/time picker ('yyyy/MM/dd HH:mm')
export const getDayFromDate = (date: string, passedUTC = true): string => {
  const newDate = passedUTC
    ? new Date(date).getUTCDate()
    : new Date(date).getDate();
  return newDate < 10 ? `0${newDate.toString()}` : newDate.toString();
};

// Pass passedUTC = false in case you are passing date string formatted as used in the date/time picker ('yyyy/MM/dd HH:mm')
export const getMonthFromDate = (date: string, passedUTC = true): string => {
  const newDate = passedUTC
    ? new Date(date).getUTCMonth() + 1
    : new Date(date).getMonth() + 1;
  return newDate < 10 ? `0${newDate.toString()}` : newDate.toString();
};
