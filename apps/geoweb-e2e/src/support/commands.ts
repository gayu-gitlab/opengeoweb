/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2023 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2023 - Finnish Meteorological Institute (FMI)
 * */
import '@testing-library/cypress/add-commands';

Cypress.Commands.add('mockLogin', (fixtureName = '', mockFailure = false) => {
  cy.mockGetVersion();

  if (mockFailure) {
    cy.intercept('POST', '**/token', { statusCode: 403 }).as('token');
  } else {
    cy.fixture(fixtureName).then((json) => {
      cy.intercept('POST', '**/token', {
        statusCode: 200,
        body: json,
      }).as('token');
    });
  }

  let redirectedToLogin = false;
  cy.on('url:changed', (url) => {
    if (!redirectedToLogin && url.includes('/login')) {
      redirectedToLogin = true;
    }
  });

  cy.visit('/').then(() => {
    cy.wait('@exampleConfig').then((interception) => {
      assert.isNotNull(interception.response!.body);
      // eslint-disable-next-line @typescript-eslint/naming-convention
      const { oauth_state } = window.sessionStorage;
      cy.url()
        // test will redirect to error page because we use a non existing login url
        .should('contain', 'error')
        .then(() => {
          // before the error page we should have been redirected to /login
          expect(redirectedToLogin).to.equal(true);
        });
      cy.visit(`/code?state=${oauth_state}&code=dummyCodeToMockLogin`).then(
        () => {
          cy.wait('@token');
          if (!mockFailure) {
            // User should be at the homepage
            cy.url().should('eq', 'http://localhost:5400/');
          }
        },
      );
    });
  });
});

Cypress.Commands.add('mockCognitoLoginSuccess', () => {
  cy.mockLogin('mockLoginResponseCognito.json');
});

Cypress.Commands.add('mockGitlabLoginSuccess', () => {
  cy.mockExampleConfigGitlabLogin();
  cy.mockLogin('mockLoginResponseGitlab.json');
});

Cypress.Commands.add('mockLoginFailure', () => {
  cy.mockExampleConfig();
  cy.mockLogin('', true);
});

Cypress.Commands.add('mockCognitoLogoutSuccess', () => {
  cy.intercept('GET', /\/logout/, {
    statusCode: 302,
    headers: {
      location: 'http://localhost:5400/login',
    },
  }).as('logout');

  cy.findByTestId('userButton').click({ force: true });
  cy.findByText('Logout').click({ force: true });
  cy.wait('@logout');
});

Cypress.Commands.add('mockFetchTAFList', () => {
  cy.fixture('mockTAFList').then((json) => {
    cy.intercept('GET', /\/aviation\/taflist/, {
      statusCode: 200,
      body: json,
    }).as('taflist');
  });

  cy.intercept('POST', /\/aviation\/taf/, {
    statusCode: 200,
  }).as('postTaf');
});

Cypress.Commands.add('mockGetTac', () => {
  cy.fixture('mockTAC').then((text) => {
    cy.intercept('POST', /\/aviation\/taf2tac/, {
      statusCode: 200,
      body: text,
    }).as('getTac');
  });
});

Cypress.Commands.add('mockFetchSIGMETList', () => {
  cy.fixture('mockSIGMETList.json').then((json) => {
    cy.intercept('GET', /\/aviation\/sigmetlist/, {
      statusCode: 200,
      body: json,
    }).as('sigmetlist');
  });
});

Cypress.Commands.add('mockFetchAIRMETList', () => {
  cy.fixture('mockAIRMETList.json').then((json) => {
    cy.intercept('GET', /\/aviation\/airmetlist/, {
      statusCode: 200,
      body: json,
    }).as('airmetlist');
  });
});

Cypress.Commands.add(
  'dragTo',
  { prevSubject: true },
  (subject, dropSelector) => {
    const DELAY_INTERVAL_MS = 10;

    cy.wrap(subject).trigger('pointerdown', {
      button: 0,
    });
    cy.wrap(subject).trigger('dragstart');
    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.wrap(subject).wait(DELAY_INTERVAL_MS);
    cy.get(dropSelector).trigger('dragover', 'bottom');
    // eslint-disable-next-line cypress/no-unnecessary-waiting
    cy.get(dropSelector).wait(DELAY_INTERVAL_MS);
    cy.get(dropSelector).trigger('drop');
    cy.get(dropSelector).trigger('mouseup');
  },
);

Cypress.Commands.add('mockRadarGetCapabilities', () => {
  cy.fixture('mockMetNoGetCapabilities.xml').then((data) => {
    cy.intercept(
      'GET',
      /halo-wms.met.no\/halo\/default.map\?service=WMS&request=GetCapabilities/,
      {
        statusCode: 200,
        body: data,
        headers: {
          'Content-Type': 'text/xml',
        },
      },
    ).as('getCapabilitiesMetNO');
  });

  cy.fixture('mockKNMIGetCapabilities.xml').then((data) => {
    cy.intercept(
      'GET',
      /geoservices.knmi.nl\/wms\?dataset=RADAR&service=WMS&request=GetCapabilities/,
      {
        statusCode: 200,
        body: data,
        headers: {
          'Content-Type': 'text/xml',
        },
      },
    ).as('getCapabilitiesKNMI');
  });

  cy.fixture('mockFMIGetCapabilities.xml').then((data) => {
    cy.intercept(
      'GET',
      /openwms.fmi.fi\/geoserver\/wms\?service=WMS&request=GetCapabilities/,
      {
        statusCode: 200,
        body: data,
        headers: {
          'Content-Type': 'text/xml',
        },
      },
    ).as('getCapabilitiesFMI');
  });
});

Cypress.Commands.add('waitOnRadarGetCapabilities', () => {
  cy.wait('@getCapabilitiesMetNO');
  cy.wait('@getCapabilitiesFMI');
  cy.wait('@getCapabilitiesKNMI');
});

Cypress.Commands.add('mockGetVersion', () => {
  cy.intercept('GET', /\/infra\/version/, {
    statusCode: 200,
    body: { version: 'tst' },
  }).as('getVersion');
});

Cypress.Commands.add('mockExampleConfig', () => {
  cy.intercept('GET', '/assets/config.json', {
    fixture: 'mockExampleConfigCognitoLogin.json',
  }).as('exampleConfig');
});

Cypress.Commands.add('mockExampleConfigGitlabLogin', () => {
  cy.intercept('GET', '/assets/config.json', {
    fixture: 'mockExampleConfigGitlabLogin.json',
  }).as('exampleConfig');
});

Cypress.Commands.add('mockExampleConfigNoFeatures', () => {
  cy.intercept('GET', '/assets/config.json', {
    fixture: 'mockExampleConfigNoFeatures.json',
  }).as('exampleConfigNoFeatures');
});

Cypress.Commands.add('mockExampleConfigWithSigmet', () => {
  cy.intercept('GET', '/assets/config.json', {
    fixture: 'mockExampleConfigWithSigmet.json',
  }).as('exampleConfig');
  cy.intercept('GET', '/assets/mockExample.sigmetConfig.json', {
    fixture: 'mockExample.sigmetConfig.json',
  }).as('mockSigmetProductConfig');
});

Cypress.Commands.add('mockExampleConfigWithAirmet', () => {
  cy.intercept('GET', '/assets/config.json', {
    fixture: 'mockExampleConfigWithAirmet.json',
  }).as('exampleConfig');
  cy.intercept('GET', '/assets/mockExample.airmetConfig.json', {
    fixture: 'mockExample.airmetConfig.json',
  }).as('mockAirmetProductConfig');
});

Cypress.Commands.add('mockExampleConfigWithBackendUrl', () => {
  cy.intercept('GET', '/assets/config.json', {
    fixture: 'mockExampleConfigWithBackendUrl.json',
  }).as('exampleConfig');
});

Cypress.Commands.add('mockFetchViewpresetList', () => {
  cy.intercept('GET', '/presets/viewpreset', {
    fixture: 'mockViewpresetList.json',
  }).as('viewpresetlist');
  cy.fixture('mockViewpresetList').then((json) => {
    cy.intercept('GET', '/presets/viewpreset?scope=user', {
      statusCode: 200,
      body: [json[1]],
    }).as('presetlist-user');
  });
  cy.fixture('mockViewpresetList').then((json) => {
    cy.intercept('GET', '/presets/viewpreset?scope=system', {
      statusCode: 200,
      body: [json[0], json[2], json[3]],
    }).as('presetlist-system');
  });
  cy.fixture('mockViewpresetList').then((json) => {
    cy.intercept('GET', '/presets/viewpreset?scope=system&search=rad', {
      statusCode: 200,
      body: [json[2], json[3]],
    }).as('presetlist-search');
  });
  cy.fixture('mockViewpresetList').then((json) => {
    cy.intercept('GET', '/presets/viewpreset?scope=system&search=Advanced', {
      statusCode: 200,
      body: [json[3]],
    }).as('presetlist-searchadvanced');
  });
});

Cypress.Commands.add('mockFetchViewpreset', () => {
  cy.fixture('mockViewpreset').then((json) => {
    cy.intercept('GET', /\/NLwarnings/, {
      statusCode: 200,
      body: json,
    }).as('viewpreset');
  });
});

Cypress.Commands.add('mockDeleteViewpreset', () => {
  cy.intercept('DELETE', /\/NLwarnings/, {
    statusCode: 204,
  }).as('deleteViewpreset');
});

Cypress.Commands.add('mockSaveViewpresetAs', () => {
  cy.intercept('POST', /\/viewpreset/, {
    statusCode: 200,
  }).as('saveViewpresetAs');
});

Cypress.Commands.add('mockSaveViewpreset', () => {
  cy.intercept('PUT', /\/NLwarnings/, {
    statusCode: 201,
  }).as('saveViewpreset');
});

Cypress.Commands.add('mockViewpresets', () => {
  cy.mockFetchViewpresetList();
  cy.mockFetchViewpreset();
  cy.mockDeleteViewpreset();
  cy.mockSaveViewpresetAs();
  cy.mockSaveViewpreset();
});

Cypress.Commands.add('mockFetchSIGMETList', () => {
  cy.fixture('mockSIGMETList').then((json) => {
    cy.intercept('GET', /\/sigmetlist/, {
      statusCode: 200,
      body: json,
    }).as('sigmetlist');
  });
});

const resizeObserverLoopErrRe = /^[^(ResizeObserver loop limit exceeded)]/;

Cypress.on('uncaught:exception', (err) => {
  // catches resizeObserver error https://stackoverflow.com/questions/49384120/resizeobserver-loop-limit-exceeded
  // returning false here prevents Cypress from failing the test
  if (resizeObserverLoopErrRe.test(err.message)) {
    return false;
  }
  // sometimes a service won't load
  // returning false here prevents cypress from failing the test
  if (err.message.includes('No Capability element found in service')) {
    return false;
  }
  // we still want to ensure there are no other unexpected
  // errors, so we let them fail the test
  return true;
});

Cypress.Commands.add('mockWorkspaces', () => {
  cy.intercept('GET', '/presets/workspacepreset?scope=user,system', {
    fixture: 'workspace/mockWorkspacePresetsList.json',
  }).as('workspaceList');
  cy.intercept('POST', '/presets/workspacepreset', {
    fixture: 'workspace/mockWorkspacePresetsList.json',
  }).as('workspacePost');

  // taf
  cy.intercept('GET', '/presets/viewpreset/screenConfigTaf', {
    fixture: 'workspace/mockViewpresetTaf.json',
  }).as('viewpresetTaf');
  cy.intercept('GET', '/presets/workspacepreset/screenConfigTaf', {
    fixture: 'workspace/mockWorkspaceTaf.json',
  }).as('workspaceTaf');
  // airmet
  cy.intercept('GET', '/presets/viewpreset/screenConfigAirmet', {
    fixture: 'workspace/mockViewpresetAirmet.json',
  }).as('viewpresetAirmet');
  cy.intercept('GET', '/presets/workspacepreset/screenConfigAirmet', {
    fixture: 'workspace/mockWorkspaceAirmet.json',
  }).as('workspaceAirmet');
  // sigmet
  cy.intercept('GET', '/presets/viewpreset/screenConfigSigmet', {
    fixture: 'workspace/mockViewpresetSigmet.json',
  }).as('viewpresetSigmet');
  cy.intercept('GET', '/presets/workspacepreset/screenConfigSigmet', {
    fixture: 'workspace/mockWorkspaceSigmet.json',
  }).as('workspaceSigmet');

  // radar
  cy.intercept('GET', '/presets/viewpreset/customRadar', {
    fixture: 'workspace/mockViewpresetRadar.json',
  }).as('viewpresetGet');
  cy.intercept('GET', '/presets/workspacepreset/customRadar', {
    fixture: 'workspace/mockWorkspaceRadar.json',
  }).as('workspaceGet');
  cy.intercept('PUT', '/presets/workspacepreset/customRadar', {
    fixture: 'workspace/mockViewpresetRadar.json',
  }).as('workspacePut');
  cy.intercept('DELETE', '/presets/workspacepreset/customRadar', {
    fixture: 'workspace/mockViewpresetRadar.json',
  }).as('workspaceDelete');

  // timeseries
  cy.intercept('GET', '/presets/workspacepreset/defaultTimeseriesPreset', {
    fixture: 'workspace/mockWorkspaceTimeseries.json',
  }).as('workspaceGet');
  cy.intercept('GET', '/presets/viewpreset/Timeseries', {
    fixture: 'workspace/mockViewpresetTimeseries.json',
  }).as('viewpresetGet');

  // warnings
  cy.intercept('GET', '/presets/viewpreset/warninglist', {
    fixture: 'workspace/mockViewpresetWarnings.json',
  }).as('viewpresetWarnings');
  cy.intercept('GET', '/presets/workspacepreset/workspaceWarnings', {
    fixture: 'workspace/mockWorkspaceWarnings.json',
  }).as('workspaceWarnings');
});

Cypress.Commands.add('mockWarnings', () => {
  cy.intercept('GET', 'warningsurl/drawings', {
    fixture: 'warnings/mockObjectList.json',
  }).as('objectList');
  cy.intercept('POST', 'warningsurl/drawings/', {
    statusCode: 200,
  }).as('saveNewObject');
  cy.intercept('GET', 'warningsurl/drawings/923723984872338768743', {
    fixture: 'warnings/mockObjectDetail.json',
  }).as('getObjectDetail');
  cy.intercept('POST', 'warningsurl/drawings/923723984872338768743', {
    statusCode: 201,
  }).as('updateObject');
  cy.intercept(
    'POST',
    'warningsurl/drawings/923723984872338768743?delete=true',
    {
      statusCode: 201,
    },
  ).as('deleteObject');
  cy.intercept('GET', 'warningsurl/warnings', {
    fixture: 'warnings/mockWarningList.json',
  }).as('getWarnings');
  cy.intercept('POST', /warningsurl\/warnings\/.+\?delete=true/, {
    statusCode: 201,
  }).as('deleteWarning');
  cy.intercept('POST', /warningsurl\/warnings\/.+\//, {
    statusCode: 201,
  }).as('updateWarning');
  cy.intercept('POST', 'warningsurl/warnings/', {
    statusCode: 200,
  }).as('saveWarningAs');
});

beforeEach(() => {
  cy.intercept('GET', /.*GetCapabilities/, { log: false });
});
